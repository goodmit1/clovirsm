package com.clovirsm.hv.cloud.azure;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AzLinuxVMAPITest extends AzAPITest {
    @BeforeEach
    public void init(){
        from = "/subscriptions/dadd60d0-9dfa-40cd-af23-8d658046ec7e/resourceGroups/test1/providers/Microsoft.Compute/galleries/gallery/images/linux-image/versions/1.0.0";
        vmName="clovir-linux";
        specType = "Standard_A2_v2";
        addUserShellFile = "UserAdd.sh";;
        dataDiskSize = 10;
        osDiskSize = 30;
        isLinux = true;
        new_specType = "Standard_A1_v2";
        diskType = "Standard_LRS";
    }
@Override
    protected VMInfo getVMInfo(String vmName) throws Exception {
        VMInfo vmInfo = new VMInfo( );
        vmInfo.setVM_NM( vmName);
        vmInfo.setRSC_GROUP_NM(rscGroup);
        return api.vmState( new HVParam(  connInfo , vmInfo));

    }
     

    

}