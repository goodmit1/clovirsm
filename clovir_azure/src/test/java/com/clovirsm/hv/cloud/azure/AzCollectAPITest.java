package com.clovirsm.hv.cloud.azure;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.HVParam;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class AzCollectAPITest extends AzAPITest {
    @Test
    public void listHVObj() throws Exception {

        List<Map> list  = api.listObject(new HVParam(connInfo));


        Set types = CommonUtil.getDistinctValues(list,"OBJ_TYPE_NM");
        System.out.println(types);
        System.out.println(list);
        Assert.assertNotNull(list);
        Assert.assertNotEquals(list.size(),0);
        Assert.assertEquals(types.size(),4); // Datastore, Template, InstanceType,
        int instanceTypeCnt = 0;
        for(Map m:list){
            if("instanceType".equals(m.get("OBJ_TYPE_NM"))) {
                instanceTypeCnt++;
                String prop = (String) m.get("OBJ_PROP");
                JSONObject json  =new JSONObject( prop );
                System.out.println("cpu:" + json.get(HypervisorAPI.PARAM_CPU) + ",memory:" + json.get(HypervisorAPI.PARAM_MEM));

            }
        }
        Assert.assertNotEquals(instanceTypeCnt, 0);
    }
}
