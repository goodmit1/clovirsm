package com.clovirsm.hv.cloud.azure;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMTestCommon;
import org.junit.jupiter.api.BeforeEach;

import java.util.HashMap;


public class AzAPITest extends VMTestCommon {


    @BeforeEach
    public void login() throws Exception {

        String clientId = "20f45be5-6e7a-4a0c-a6e9-8f38b899ac4a";
        String tenantId = "d4071445-af81-4dca-bf8c-13f992263aaa";
        String clientSecret = "-h26-pknUYSaBq1g-HnZ8EHTO6_O~Mu6gG";
        String subscriptionId = "dadd60d0-9dfa-40cd-af23-8d658046ec7e";

        String region = "koreacentral";


        connInfo = new HashMap();
        connInfo.put("tenantId", tenantId);
        connInfo.put("subscriptionId", subscriptionId);

        connInfo.put(HypervisorAPI.PARAM_RSC_GROUP_NM, rscGroup);
        connInfo.put(HypervisorAPI.PARAM_URL, region);
        connInfo.put(HypervisorAPI.PARAM_USERID, clientId);
        connInfo.put(HypervisorAPI.PARAM_PWD, clientSecret);

        api = new AzureAPI() ;
    }
}
