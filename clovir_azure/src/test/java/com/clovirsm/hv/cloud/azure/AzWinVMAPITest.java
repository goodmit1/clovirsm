package com.clovirsm.hv.cloud.azure;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AzWinVMAPITest extends AzLinuxVMAPITest {
    @BeforeEach
    public void init(){
        super.init();
        from = "/subscriptions/dadd60d0-9dfa-40cd-af23-8d658046ec7e/resourceGroups/test1/providers/Microsoft.Compute/galleries/gallery/images/windows/versions/1.0.1";
        vmName="clovirsm-win";

        addUserShellFile = "UserAdd.ps1";
        osDiskSize = 127;
        isLinux = false;
    }


}
