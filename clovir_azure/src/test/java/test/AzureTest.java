package test;

import com.clovirsm.hv.CommonUtil;
import com.microsoft.azure.AzureEnvironment;
import com.microsoft.azure.AzureResponseBuilder;
import com.microsoft.azure.PagedList;
import com.microsoft.azure.credentials.ApplicationTokenCredentials;
import com.microsoft.azure.management.Azure;
import com.microsoft.azure.management.compute.*;
import com.microsoft.azure.management.network.Network;
import com.microsoft.azure.management.network.NetworkInterface;
import com.microsoft.azure.management.network.NetworkSecurityRule;
import com.microsoft.azure.management.network.SecurityRuleProtocol;
import com.microsoft.azure.management.resources.Deployment;
import com.microsoft.azure.management.resources.DeploymentMode;
import com.microsoft.azure.management.resources.ResourceGroup;
import com.microsoft.azure.management.resources.fluentcore.arm.Region;
import com.microsoft.azure.management.resources.implementation.ResourceManager;
import com.microsoft.azure.serializer.AzureJacksonAdapter;
import com.microsoft.rest.LogLevel;
import com.microsoft.rest.RestClient;
import com.microsoft.rest.ServiceCallback;
import com.microsoft.rest.ServiceFuture;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author : kimjinwon
 * @version : 1.0.0
 * @package : test.clovirsm
 * @name : AzureTest.java
 * @date : 2021-05-28 오후 2:48
 * @modifyed :
 **/
@Ignore
public class AzureTest {
    private final String clientId = "20f45be5-6e7a-4a0c-a6e9-8f38b899ac4a";
    private final String tenantId = "d4071445-af81-4dca-bf8c-13f992263aaa";
    private final String clientSecret = "-h26-pknUYSaBq1g-HnZ8EHTO6_O~Mu6gG";
    private final String subscriptionId = "dadd60d0-9dfa-40cd-af23-8d658046ec7e";

    private Azure azure;
    private ApplicationTokenCredentials credentials;
    @Before
    public void azureConnect() {
        try {
            credentials = new ApplicationTokenCredentials(clientId, tenantId, clientSecret, AzureEnvironment.AZURE);
            RestClient restClient = new RestClient.Builder()
                    .withBaseUrl(AzureEnvironment.AZURE, AzureEnvironment.Endpoint.RESOURCE_MANAGER)
                    .withSerializerAdapter(new AzureJacksonAdapter())
                    .withResponseBuilderFactory(new AzureResponseBuilder.Factory())
                    .withLogLevel(LogLevel.BODY_AND_HEADERS)
                    .withCredentials(credentials).build();
            azure = Azure.authenticate(restClient, credentials.domain(), subscriptionId).withDefaultSubscription();

            // Print selected subscription
            System.out.println("Selected subscription: " + azure.subscriptionId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Ignore
    public void createResourceGroup() {
        ResourceGroup resourceGroup = azure.resourceGroups().define("test4")
                .withRegion(Region.KOREA_CENTRAL)
                .create();
    }

    @Test
    @Ignore
    public void createInstance() {
        final String userName = "test123";
        final String sshKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC4FB0oahfZ2MgzgIQvNMBJmtiU0MDWq9J6ixylkcmBwcGrxGA7OrR/ZxqLADXAW/bAYFyoNPeu7rijcX/b7ZdlXQYINqUzIXpXvOJ/rimbkDElXknQdG8D+XHZjLgSprkhLfWLeEzJgmIkil3LuSfKzG5BitvKxMsRObDYMSwwxHS35UaHGK6CYYvVQsnmj89v6bfDGS6lRepBQ4J10mQ6W/eb3ywnvj3to/DpB4/nWuCh1FcnmmkXOaMSOhl1qIRInxTo5R/uHMKSc8o1fntJGyBIoeL6TqgDvNKgWeEp9XX/wSO5K3j+EhLC2Y8VI8OIVjAtCpujIR40Tb5mPTlVQNW7psoe6X+3FD9QakPeECQB5MaqNJ/P9OrQYoyzf3VHRKCyChmp9mzRAzw9RGt1wxCeE2R2DAG6qKWCt7GjUrVBgnFBw5GsWMGUroFil7bYmFeMXsghhZBlQLD91vdVk5LYmaGQPy7ckVXl94I6px9nIAQrAWgQuoy5tAgrOhvSdq7UliNDvrcj7zaNbWlLuTG7R4aC5XdLtwJkN5dAgDsvlXIX9seEbGbS4kHOdUqmLlCF9tS0hKzK11J1fnqTuTMrmAb4QO7FJAka6rajs2QLpqlZh0SBojSiHQmVUJvZDhyEBn3Yz0V71DsS4nzmcCh0HM8AWmgJdNxSpxs/3w== sjhello@DESKTOP-FSSOOA1 " + userName;
        try {
            VirtualMachine linuxVM = azure.virtualMachines().define("VM03")
                    .withRegion(Region.KOREA_CENTRAL)
                    .withNewResourceGroup("test4")
                    .withNewPrimaryNetwork("0.0.0.0/24")
                    .withPrimaryPrivateIPAddressDynamic()
                    .withoutPrimaryPublicIPAddress()
                    .withPopularLinuxImage(KnownLinuxVirtualMachineImage.UBUNTU_SERVER_16_04_LTS)
                    .withRootUsername(userName)
                    .withSsh(sshKey)
                    .withSize(VirtualMachineSizeTypes.STANDARD_B1S)
                    .create();




        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Ignore
    public void describeResourceGroup() {
        ResourceGroup resourceGroups = azure.resourceGroups().getByName("test6");
    /*    for (ResourceGroup resourceGroup : resourceGroups) {
            System.out.println("resourceGroup.id = " + resourceGroup.id());
            System.out.println("resourceGroup.name = " + resourceGroup.name());
            System.out.println("resourceGroup.key = " + resourceGroup.key());
            System.out.println("");
        }*/
        Assert.assertNull(resourceGroups);

    }

    @Test
    @Ignore
    public void deleteResourceGroup() {
        azure.resourceGroups().beginDeleteByName("test4");
    }

    @Test
    @Ignore
    public void describeInstance() {
        PagedList<VirtualMachine> list = azure.virtualMachines().listByResourceGroup("test");

        for (VirtualMachine vm : list) {
            System.out.println("vm = " + vm.id());
            System.out.println("name: " + vm.name());
            System.out.println("vmid: " + vm.vmId());
            System.out.println("resourceGroupName: " + vm.resourceGroupName());
            System.out.println("osDiskSize: " + vm.osDiskSize());
            System.out.println("regionName: " + vm.regionName());
            System.out.println("public Ip: " + vm.networkInterfaceIds());
            System.out.println("power status: " + vm.powerState());
            System.out.println("size: " + vm.size());
            System.out.println();
        }
    }

    @Test
    @Ignore
    public void deleteInstance() {
        azure.virtualMachines().deleteById("/subscriptions/87b91ada-7690-45d6-8d39-d40fb83e5b82/resourceGroups/Test02/providers/Microsoft.Compute/virtualMachines/testLinuxVM2");
    }

    @Test
    @Ignore
    @DisplayName("Network 가져오기")
    public void getVirtualMachineSizeTypes() {
        Collection<VirtualMachineSizeTypes> virtualMachineSizeTypes = VirtualMachineSizeTypes.values();
        for (VirtualMachineSizeTypes virtualMachineSizeTypes1 : virtualMachineSizeTypes) {
            System.out.println("virtualMachineSizeTypes1 = " + virtualMachineSizeTypes1);
        }
    }
    @Ignore
    @Test
    public void getImageGallery() {
        PagedList<GalleryImage> galleryImages = azure.galleryImages().listByGallery("test1", "gallery");
        for (GalleryImage galleryImage : galleryImages) {
            long count = galleryImage.listVersions().size();
            GalleryImageVersion galleryImageVersion = galleryImage.listVersions().stream().skip(count - 1).findFirst().get();
            System.out.println("galleryImageVersion = " + galleryImageVersion.id());
        }
    }
    @Test
    @Ignore
    @DisplayName("Instance 상태 확인")
    public void getInstanceDescribe(){
     VirtualMachine virtualMachine = azure.virtualMachines().getByResourceGroup("CU_RSC","nullWAS002D");
     String status = virtualMachine.instanceView().statuses().stream()
             .filter(instanceViewStatus -> instanceViewStatus.code().contains("PowerState"))
             .findFirst()
             .map(InstanceViewStatus::code)
             .map(String::toString)
             .orElseThrow(IllegalArgumentException::new);
     System.out.println("status = " + status);
        for (String network : virtualMachine.networkInterfaceIds()){
            NetworkInterface byId = azure.networkInterfaces().getById(network);
            System.out.println("byId.id() = " + byId.primaryIPConfiguration().getPublicIPAddress().ipAddress());
        }
        List<DiskInstanceView> disks = virtualMachine.instanceView().disks();
        disks.stream().forEach(diskInstanceView -> System.out.println("diskInstanceView.name() = " + diskInstanceView.name()));
        System.out.println("virtualMachine.type() = " + virtualMachine.size());
     }


    @Test
    @Ignore
    @DisplayName("Network 가져오기")
    public void getNetwork(){
        PagedList<Network> networks = azure.networks().listByResourceGroup("test");
        System.out.println("networks = " + networks.stream().findFirst().orElseThrow(IllegalArgumentException::new));
    }

    @Test
    public void deleteTemplate() throws Exception{
        azure.deployments().deleteByResourceGroup("syk-test","clovirsm-win");

    }

    @Test
    public void createByTemplate() throws  Exception {
        ResourceManager azureResourceManager = ResourceManager
                .configure()
                .authenticate(credentials)
                .withSubscription(subscriptionId);
        String deploymentName="VM01-deployment";
        String string = CommonUtil.readStream2String( Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("azureCreateWindowTemplate.json")), "utf-8");
        byte[] buf = string.getBytes();
        JSONObject json = new JSONObject( new String(buf, "utf-8"));
        JSONObject param = new JSONObject();
       // param.put("VM_NM", (new JSONObject()).put("value", "clovirsm-win"));
        param.put("ADMIN_ID", (new JSONObject()).put("value", "clovirsm"));
        param.put("ADMIN_PWD", (new JSONObject()).put("value", "3NzaC1yc2EAA!@"));
        param.put("ADMIN_SSH_KEY", (new JSONObject()).put("value", "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC1YghtLabUH973f6ZZnUUq3oH6f7BsTXwo9rDIJsUU2zCns+RcuuuEdTbz8Rf1i9MZPJ09WjDrIiVfFXiPN5qCirsRCbP30VpJPFDZXUncw6Raxhgbqw0zwR3naeQkKwrk1opRpQzvHxQKvxhJ/yKa/hP56eue+UvKsykcxQ65z1iiQblyONtis5/VV1ak7NDe/5UxgS1o7OtuNWC6rN3Q1y/z60tGzu6I1lj9hVNXq2AgYyeha0MGqIBHCgIUal+l6oE8qoYJ6B38IZVEz3dv6NQ3O2I5ryZmMs49H9M3IkeJIMdtCQhOp8ngpH8WP933wnRrmonXc8mYRAEmuYpgDNm3uB+xTS+/pt7Tln5hrWPgKmYrX80ddLOXbo2FuZBbVdWnFbyeQ1er5qt08dtlYSFCyHnBfd5XqEQdt2JC6qVV7bh61nOoAZtiL+I46vaVtgRwgW8iD9lD5NmyaRCv1uVkFsKOaD43NXtIaRoThQlt8wGJFXOA9kWBxWkt87E="));
        final String[] errMsg = {null};
        ServiceFuture<Deployment> deployment = azure.deployments().define(deploymentName)
                .withExistingResourceGroup("syk-test1")
                .withTemplate(json.toString())
                .withParameters(param.toString())
                .withMode(DeploymentMode.INCREMENTAL)
                .createAsync(new ServiceCallback<Deployment>() {
                    @Override
                    public void failure(Throwable throwable) {
                        System.out.println(throwable.getMessage());
                        errMsg[0] = throwable.getMessage();
                    }

                    @Override
                    public void success(Deployment deployment) {

                    }
                });
        try {
            deployment.get(500, TimeUnit.MILLISECONDS);
        }
        catch (TimeoutException e){

        }
        System.out.println("ssss" + errMsg[0]);
    }

    @Test
    public void securityGroupUpdate() {
        azure.networkSecurityGroups().getByResourceGroup("CU_RSC", "nullWEB002D_NSG")
                .update()
                .defineRule("FW_221.147.80.129:80")
                .allowInbound()
                .fromAddress("221.147.80.129")
                .fromAnyPort().toAnyAddress().toPort(80).withProtocol(SecurityRuleProtocol.TCP).withPriority(200).attach()
        .apply();
    }
    
    @Test
    public void securityGroupDescribe() {
        Map<String, NetworkSecurityRule> stringNetworkSecurityRuleMap = azure.networkSecurityGroups().getByResourceGroup("CU_RSC", "nullWEB002D_NSG")
                .securityRules();

        System.out.println("stringNetworkSecurityRuleMap.size() = " + stringNetworkSecurityRuleMap.size());
    }

    @Test
    public void instancesDNSDescribe() {
        String s = azure.publicIPAddresses().getByResourceGroup("CU_RSC", "nullWAS003P_IP").leafDomainLabel();

        System.out.println("s = " + s);
    }


    @Test
    public void getImageGallery1() {
        GalleryImage galleryImage = azure.galleryImages().getByGallery("test1", "gallery", "windows");
        if(galleryImage != null){
            System.out.println(galleryImage.osType().name());
        }
        else{
            System.out.println("null");
        }
    }
    @Test
    public void getImages() {
        PagedList<VirtualMachineImage> images = azure.virtualMachineImages().listByRegion(Region.KOREA_CENTRAL);
       // images.loadAll();
       images.forEach(s -> {
           System.out.println(s.toString() + ":" + s.id());
       });
    }
    @Test
    public void getImage(){
        String id = "/Subscriptions/dadd60d0-9dfa-40cd-af23-8d658046ec7e/Providers/Microsoft.Compute/Locations/KoreaCentral/Publishers/%s/ArtifactTypes/VMImage/Offers/%s/Skus/%s/Versions";
        id = String.format(id, "canonical","0001-com-ubuntu-server-focal","20_04-lts");
        VirtualMachineImage image = azure.virtualMachineImages().getImage(Region.KOREA_CENTRAL, "canonical", "0001-com-ubuntu-server-focal", "20_04-lts", "latest");
        System.out.println(image.id());
        System.out.println(image.osDiskImage().operatingSystem().toString());
    }
    @Test
    public void getDeployemnt(){
        Deployment deployment = azure.deployments().getByResourceGroup("syk-test", "clovir-linux");
        System.out.println(deployment.exportTemplate().templateAsJson());
        System.out.println(deployment.parameters());
    }
    @Test
    @Ignore
    public void runCmd() {
        List<String> script = new ArrayList<>();

        script.add("/Users/user-profile.ps1 clovirsm Qwer4321!@# 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC1YghtLabUH973f6ZZnUUq3oH6f7BsTXwo9rDIJsUU2zCns+RcuuuEdTbz8Rf1i9MZPJ09WjDrIiVfFXiPN5qCirsRCbP30VpJPFDZXUncw6Raxhgbqw0zwR3naeQkKwrk1opRpQzvHxQKvxhJ/yKa/hP56eue+UvKsykcxQ65z1iiQblyONtis5/VV1ak7NDe/5UxgS1o7OtuNWC6rN3Q1y/z60tGzu6I1lj9hVNXq2AgYyeha0MGqIBHCgIUal+l6oE8qoYJ6B38IZVEz3dv6NQ3O2I5ryZmMs49H9M3IkeJIMdtCQhOp8ngpH8WP933wnRrmonXc8mYRAEmuYpgDNm3uB+xTS+/pt7Tln5hrWPgKmYrX80ddLOXbo2FuZBbVdWnFbyeQ1er5qt08dtlYSFCyHnBfd5XqEQdt2JC6qVV7bh61nOoAZtiL+I46vaVtgRwgW8iD9lD5NmyaRCv1uVkFsKOaD43NXtIaRoThQlt8wGJFXOA9kWBxWkt87E='");

        List<RunCommandInputParameter> param = new ArrayList<>();
        RunCommandResult res = azure.virtualMachines().getByResourceGroup("syk-test", "clovirsm-win")
                .runPowerShellScript("syk-test", "clovirsm-win", script, param);
        res.value().forEach(s ->{
            System.out.println(s.message());
        });
    }
}