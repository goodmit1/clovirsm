package com.clovirsm.hv.cloud.azure;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.PublicHVAction;
import com.microsoft.azure.management.Azure;


public abstract class AzureCommon extends PublicHVAction {


    public static final String NIC = "_NIC";
    public static final String SECURITY_GROUP = "_NSG";
    public static final String IP = "_IP";




    protected Azure client;


    public AzureCommon(IConnection conn) {
        super(conn);
         if (conn != null) {
            this.client = ((AzureConnection)  conn).getClient();
        }
    }
    protected   String getConnectionClassName(){
        return AzureConnection.class.getName();
    }

    public AzureConnection getAzureConnection(){
        return (AzureConnection)super.getConnect();
    }


}
