package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.management.compute.RunCommandResult;

import java.util.List;
import java.util.Map;

public class AZRunCmd extends AzureCommon {

    public AZRunCmd(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }


    public String run(VMInfo vmInfo, List script, List params){
        RunCommandResult res = super.client.virtualMachines().getByResourceGroup(vmInfo.getRSC_GROUP_NM(), vmInfo.getVM_NM())
                .runPowerShellScript(vmInfo.getRSC_GROUP_NM(), vmInfo.getVM_NM(), script, params);
        StringBuffer msg = new StringBuffer();
        res.value().forEach(s ->{
            System.out.println(s.message());
            msg.append(s.message() + "\n");
        });
        return msg.toString();

    }
}
