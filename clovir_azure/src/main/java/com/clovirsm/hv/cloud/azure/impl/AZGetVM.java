package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.*;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import com.microsoft.azure.PagedList;
import com.microsoft.azure.management.compute.*;
import com.microsoft.azure.management.network.NetworkInterface;
import com.microsoft.azure.management.network.NetworkSecurityGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AZGetVM extends AzureCommon {
    public AZGetVM(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws  Exception {

            state(  param.getVmInfo(), (VMInfo) result);


        return null;
    }

    VirtualMachine virtualMachine;
    protected void state(VMInfo param, VMInfo result) throws NotFoundException {
        virtualMachine = null;
        VirtualMachine virtualMachine = getVMRef(param);
        setParameter(  virtualMachine, result  );
    }
    private  VirtualMachine getVMRef(VMInfo vmInfo) throws NotFoundException {
        if(virtualMachine == null ) {
            virtualMachine = client.virtualMachines()
                    .getByResourceGroup(  vmInfo.getRSC_GROUP_NM(), vmInfo.getVM_NM());
        }
        if(virtualMachine == null){
            throw new NotFoundException("VM " +vmInfo.getVM_NM() );
        }
        return virtualMachine;
    }
    protected void setParameter(  VirtualMachine virtualMachine, VMInfo result  ) {

        //String osDiskName = getOsDisk(virtualMachine);

        result.setVM_NM(  virtualMachine.name());

        result.setSPEC_TYPE(  virtualMachine.size().toString());
        result.setRSC_GROUP_NM(virtualMachine.resourceGroupName());
        result.setGUEST_NM( getGuestNm(virtualMachine) ) ;
        result.setPUBLIC_IP(  getDNS(virtualMachine));
        setDisk(virtualMachine,     result);
        setSpecInfo( result, virtualMachine);
        result.setVM_HV_ID(virtualMachine.name());
        if(virtualMachine.powerState() != null) result.setRUN_CD(virtualMachine.powerState().equals(PowerState.RUNNING) ?  HypervisorAPI.RUN_CD_RUN : HypervisorAPI.RUN_CD_STOP);
        result.setIP_LIST( getIpAddress(virtualMachine));
    }
    private void setDisk(VirtualMachine virtualMachine,    VMInfo result) {
        if(virtualMachine.instanceView() == null) return;
        List<DiskInfo> diskList = new ArrayList();
        List<DiskInstanceView> disks = virtualMachine.instanceView().disks();

        int  total = 0;
        for (DiskInstanceView disk : disks) {
            DiskInfo diskParam = new DiskInfo();

            Disk diskInfo = client.disks().getByResourceGroup(virtualMachine.resourceGroupName(), disk.name());
            diskParam.setDISK_NM( disk.name());
            diskParam.setDISK_PATH(diskInfo.id() );
            diskParam.setDISK_SIZE( diskInfo.sizeInGB());
            diskParam.setDS_NM( diskInfo.sku().toString() );
            total += diskInfo.sizeInGB();
            diskList.add(diskParam);

        }
        result.setDISK_LIST( diskList);
        result.setDISK_SIZE(total);

    }
    private void setSpecInfo(VMInfo result , VirtualMachine virtualMachine){
        VirtualMachineSizeTypes spec = virtualMachine.size();
        PagedList<ComputeSku> listSkus = client.computeSkus().listbyRegionAndResourceType(super.getAzureConnection().currentRegion, ComputeResourceType.VIRTUALMACHINES);
        listSkus.loadAll();
        List<ResourceSkuCapabilities> capaList = listSkus.stream().filter(s -> s.name().toString().equals(spec.toString())).map(s -> s.capabilities()).findFirst().get();
        result.setCPU( AZListObject.getCPU(capaList));
        result.setRAM_SIZE( AZListObject.getRAM(capaList));
    }
    private String getGuestNm (VirtualMachine virtualMachine){
        return virtualMachine.osType().toString();
    }
    private String getOsDisk(VirtualMachine virtualMachine) {
        String[] osDisks = virtualMachine.osDiskId().split("/");
        return osDisks[osDisks.length - 1];
    }



    private List<IPInfo> getIpAddress(VirtualMachine virtualMachine) {
        List<IPInfo> ipList = new ArrayList<>( );

        for (String network : virtualMachine.networkInterfaceIds()) {
            NetworkInterface byId = client.networkInterfaces().getById(network);
            IPInfo ipInfo = new IPInfo();
            ipInfo.ip = byId.primaryPrivateIP();
            ipInfo.macAddress = byId.macAddress();
            ipInfo.nicId = byId.hashCode();

            ipList.add(ipInfo);
        }
        return ipList;
    }

    private String getDNS(VirtualMachine virtualMachine) {
        String dnsLabel = virtualMachine.getPrimaryPublicIPAddress().leafDomainLabel();
        String regionName = virtualMachine.getPrimaryPublicIPAddress().regionName();

        return dnsLabel + "." + regionName + ".cloudapp.azure.com";
    }

    public boolean isWindow(VMInfo info) throws NotFoundException {
        VirtualMachine virtualMachine = getVMRef(info);
        return "WINDOWS".equals(getGuestNm(virtualMachine).toUpperCase());
    }
    public NetworkSecurityGroup getSecurityGroup(VMInfo info) throws NotFoundException {
        VirtualMachine virtualMachine = getVMRef(info);
        return virtualMachine.getPrimaryNetworkInterface().getNetworkSecurityGroup();

    }
}
