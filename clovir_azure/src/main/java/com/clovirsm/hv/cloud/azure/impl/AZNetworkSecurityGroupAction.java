package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.management.network.NetworkSecurityGroup;
import com.microsoft.azure.management.network.SecurityRuleProtocol;
import com.microsoft.azure.management.resources.fluentcore.arm.Region;

import java.util.Map;

public class AZNetworkSecurityGroupAction extends AzureCommon {
    NetworkSecurityGroup networkSecurityGroup = null;
    public AZNetworkSecurityGroupAction(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }


    public NetworkSecurityGroup getNetworkSecurityGroup() {
        return networkSecurityGroup;
    }

    public void newCreateSecurityGroup(Map param){
        networkSecurityGroup = client.networkSecurityGroups().define(param.get("VM_NM") + SECURITY_GROUP)
                .withRegion(Region.fromName((String) param.get("CONN_URL")))
                .withExistingResourceGroup((String) param.get(HypervisorAPI.PARAM_RSC_GROUP_NM))
                .defineRule("SSH")
                .allowInbound()
                .fromAnyAddress()
                .fromAnyPort()
                .toAnyAddress()
                .toPort(22)
                .withProtocol(SecurityRuleProtocol.fromString("Tcp"))
                .attach()
                .create();
    }
}
