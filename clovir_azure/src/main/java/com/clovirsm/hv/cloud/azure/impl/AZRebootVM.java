package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;

import java.util.Map;

public class AZRebootVM extends AzureCommon {
    public AZRebootVM(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        try {
            doAction( param.getVmInfo());
        } catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(MSG_INSTANCE_REBOOT_ERROR);
        }
        return null;
    }
    protected void doAction(VMInfo vmInfo){
        client.virtualMachines().getByResourceGroup(vmInfo.getRSC_GROUP_NM(), vmInfo.getVM_NM()).restart();
    }
}
