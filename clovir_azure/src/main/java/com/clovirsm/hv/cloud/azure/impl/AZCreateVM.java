package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.management.resources.Deployment;
import com.microsoft.azure.management.resources.DeploymentMode;
import com.microsoft.azure.management.resources.ResourceGroup;
import com.microsoft.azure.management.resources.fluentcore.arm.Region;
import com.microsoft.azure.management.resources.fluentcore.model.Creatable;
import com.microsoft.rest.ServiceCallback;
import com.microsoft.rest.ServiceFuture;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AZCreateVM extends AzureCommon {
    ServiceCallback<Deployment> serviceCallback = new AZInstanceCallback();

    public AZCreateVM(IConnection conn) {
        super(conn);
    }

    /**
     * @method :
     * @author : kimjinwon
     * @Param :
     * VM_NM : VM name*
     * CONN_URL : Azure 리전
     * ETC_CD : Team ETC_CD로 ResourceGroup의 Name명
     * PUBLIC_KEY : Default SSH KEY
     * SPEC_TYPE : VM 스팩 설정
     * @comment :Instance 생성
     * 생성 시  Resource Group 체크(없을 시 생성) -> 공용IP 생성 -> 가상 네트워크체크(없을 시 생성) -> 네트워크 인터페이스 생성 ->  Asyc로 VM Create
     ***/
    @Override
    public String run1(HVParam  paramMap, Map result) throws Exception {
        try {
            VMInfo param =  paramMap.getVmInfo();
            createResourceGroupNotExist(param);
            super.chkVMSetting(param);
            if (!isWindow(param.getFROM())) {
                return createVM(param, "azureCreateLinuxTemplate.json");
            } else  {
               return createVM(param, "azureCreateWindowTemplate.json");
            }


        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException(MSG_INSTANCE_CREATE_ERROR + e);
        }
    }
    private boolean isWindow(String imgId) throws Exception {
        AZCheckTemplatePath chkTemplate = new AZCheckTemplatePath(this.getConnect());
        VMInfo tmplInfo = chkTemplate.run(imgId);
        return tmplInfo.isWindows();

      }
    private void createResourceGroupNotExist(VMInfo param){
        ResourceGroup rscGroup = client.resourceGroups().getByName(param.getRSC_GROUP_NM());
        if(rscGroup == null){

            List<Creatable<ResourceGroup>> req;
            client.resourceGroups().define(param.getRSC_GROUP_NM())
                    .withRegion(getAzureConnection().currentRegion)
                    .create();


        }

    }
    /**
     * @method : createWindow
     * @author : kimjinwon
     * @Param : param // 설정값
     * @comment : 사용자에게 받은 템플릿이 Windows일 경우 Windows로 배포
     ***/
    private String createVM(VMInfo param, String templatePath) throws  Exception {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(templatePath);
        String jsonStr = CommonUtil.readStream2String(stream, "utf-8");

        JSONObject json = new JSONObject(jsonStr);
        JSONObject jsonParam = getJsonObjectParam(param);

        ServiceFuture<Deployment> res = client.deployments().define(param.getVM_NM())
                .withExistingResourceGroup(param.getRSC_GROUP_NM())
                .withTemplate(json.toString())
                .withParameters(jsonParam.toString())
                .withMode(DeploymentMode.INCREMENTAL)
                .createAsync(serviceCallback);
        try {
            //배포가 만들어지기 전에 에러가 발생한 경우를 위해 만든 로직임
            res.get(500, TimeUnit.MILLISECONDS);
        }catch(TimeoutException ignore){

        }
        return res.get().id();
        //System.out.println(res.get().provisioningState());
    }




    private JSONObject getJsonObjectParam(VMInfo param) {
        JSONObject jsonParam = new JSONObject();
        jsonParam.put("vmNm", (new JSONObject()).put("value", param.getVM_NM()));
        jsonParam.put("adminSshKey", (new JSONObject()).put("value", param.getOS_ADMIN_SSH_KEY()));
        jsonParam.put("location", (new JSONObject()).put("value", Region.findByLabelOrName(super.getAzureConnection().getURL())));
        jsonParam.put("specType", (new JSONObject()).put("value", param.getSPEC_TYPE()));
        jsonParam.put("tmplPath", (new JSONObject()).put("value", param.getFROM()));
        jsonParam.put("defaultIp", (new JSONObject()).put("value", param.getDEFAULT_IP()));
        jsonParam.put("diskList", (new JSONObject()).put("value",  param.getDISK_LIST()));
        //jsonParam.put("diskType", (new JSONObject()).put("value", param.getDISK_LIST().get(0).getDATASTORE_NM_LIST()[0]));
        jsonParam.put("adminId", (new JSONObject()).put("value", param.getOS_ADMIN_ID()));
        jsonParam.put("adminPwd", (new JSONObject()).put("value", param.getOS_ADMIN_PWD()));

        return jsonParam;
    }
    private String getWinCreateSSHScript(VMInfo vmInfo){
        String script =  "/Users/user-profile.ps1 ${ADMIN_ID} ${ADMIN_PWD} \"${ADMIN_SSH_KEY}\"";
        return CommonUtil.fillContentByVar(script, vmInfo);
    }

}
