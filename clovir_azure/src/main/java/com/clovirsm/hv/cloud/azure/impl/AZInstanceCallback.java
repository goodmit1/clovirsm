package com.clovirsm.hv.cloud.azure.impl;

import com.microsoft.azure.management.resources.Deployment;
import com.microsoft.rest.ServiceCallback;

public class AZInstanceCallback implements ServiceCallback<Deployment> {
    @Override
    public void failure(Throwable throwable) {
        System.out.println("throwable = " + throwable);
    }

    @Override
    public void success(Deployment deployment) {
        
    }
}
