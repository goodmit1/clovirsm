package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.rest.ServiceCallback;

import java.util.Map;

public class AZPublicIpDeleteAction extends AzureCommon {

    public AZPublicIpDeleteAction(IConnection conn) {
        super(conn);
    }

    protected void delete(  String id) {
        System.out.println("delete public ip"  + id);
        client.publicIPAddresses().deleteByIdAsync(  id,
                new ServiceCallback<Void>() {
                    @Override
                    public void failure(Throwable throwable) {
                        throwable.printStackTrace();
                        throw new RuntimeException("PublicIP 삭제 중 오류가 발생했습니다.");
                    }

                    @Override
                    public void success(Void aVoid) {

                    }
                });
    }
    @Override
    public String run1(HVParam param, Map result) throws Exception {

        return null;
    }
}