package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;

import java.util.Map;

public class AZPowerOffVM extends AzureCommon {
    public AZPowerOffVM(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        try {
            doAction(   param.getVmInfo() );
            return null;
        } catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(MSG_INSTANCE_STOP_ERROR);
        }

    }
    protected void doAction(VMInfo vmInfo){
        client.virtualMachines().getByResourceGroup(vmInfo.getRSC_GROUP_NM(), vmInfo.getVM_NM()).powerOff();
    }
}
