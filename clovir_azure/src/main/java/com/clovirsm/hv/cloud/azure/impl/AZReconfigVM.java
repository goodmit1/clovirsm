package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IAsyncAfter;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.management.compute.*;
import com.microsoft.azure.management.resources.Deployment;
import com.microsoft.azure.management.resources.DeploymentMode;
import com.microsoft.rest.ServiceCallback;
import com.microsoft.rest.ServiceFuture;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AZReconfigVM extends AzureCommon {
    public AZReconfigVM(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam  param, Map result) throws Exception {
        try {

            runInThread(param);
            return "" + System.nanoTime();

        } catch (IllegalArgumentException e){
            e.printStackTrace();
            throw new IllegalArgumentException();
        }  catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(MSG_INSTANCE_RESOURCE_ERROR);
        }

    }
    public void runInThread(HVParam param) {
        Thread thread = new Thread(() -> {
            try {
                doAction(param.getVmInfo());
                if (param.getAfter() != null && param.getAfter() instanceof IAsyncAfter) {
                    ((IAsyncAfter) param.getAfter()).setDone(true);
                }
                getConnect().disconnect();

            } catch (Exception e) {
                if (param.getAfter() != null && param.getAfter() instanceof IAsyncAfter) {
                    ((IAsyncAfter) param.getAfter()).setError(e);
                }
                e.printStackTrace();


            }
        });
        thread.start();

    }
    protected void doAction(VMInfo newVmInfo) throws Exception{
        VirtualMachine vm = client.virtualMachines().getByResourceGroup(newVmInfo.getRSC_GROUP_NM(), newVmInfo.getVM_NM());
        //vm.powerOff();
        vm.deallocate();
        this.changeInstanceType(vm, newVmInfo);
        this.changeDisk(vm, newVmInfo.getDISK_LIST());
        vm.startAsync();

    }
    protected String doActionByTemplate(VMInfo newVmInfo) throws Exception{
        AZPowerOffVM powerOffAction = new AZPowerOffVM(this.getConnect());
        powerOffAction.doAction(newVmInfo);
        Deployment deployment = client.deployments().getByResourceGroup(newVmInfo.getRSC_GROUP_NM(), newVmInfo.getVM_NM());
        String json = deployment.exportTemplate().templateAsJson();
        Map params = (Map)deployment.parameters();
        JSONObject jsonParam = new JSONObject(params);
        jsonParam.keySet().forEach( key->{
            jsonParam.getJSONObject((String) key).remove("type");
        });
        newVmInfo.getDISK_LIST().forEach( diskInfo -> {
            if(diskInfo.getDATASTORE_NM_LIST() == null){
                diskInfo.setDATASTORE_NM_LIST(diskInfo.getDS_NM());
            }
        });
        newVmInfo.getDISK_LIST().remove(0);
        jsonParam.put("specType", (new JSONObject()).put("value", newVmInfo.getSPEC_TYPE()));
        jsonParam.put("diskList", (new JSONObject()).put("value", newVmInfo.getDISK_LIST()));
        System.out.println(json);
        System.out.println(jsonParam);

        ServiceCallback<Deployment> serviceCallback = new AZInstanceCallback();
        ServiceFuture<Deployment> res =  client.deployments().define(newVmInfo.getVM_NM())
                .withExistingResourceGroup(newVmInfo.getRSC_GROUP_NM())
                .withTemplate(json)
                .withParameters(jsonParam.toString())
                .withMode(DeploymentMode.INCREMENTAL)
                .createAsync(serviceCallback);
        try {
            res.get(500, TimeUnit.MILLISECONDS);
        }catch(TimeoutException ignore){

        }
        return newVmInfo.getVM_NM();
    }
    private void changeInstanceType(VirtualMachine virtualMachine, VMInfo vmInfo) {
        if (!virtualMachine.size().toString().equals(vmInfo.getSPEC_TYPE())) {
            virtualMachine.update()
                    .withSize(VirtualMachineSizeTypes.fromString((String) vmInfo.get("SPEC_TYPE")))
                    .apply();
        }
    }

    private void changeDisk(VirtualMachine virtualMachine, List<DiskInfo> dataDisk) {
        int idx=0;
        for (DiskInfo m : dataDisk) {
            if(m.isNew()){
                createDisk(virtualMachine, idx, m.getDATASTORE_NM_LIST()[0], m.getDISK_SIZE());
            }
            else{
                updateDisk(m.getDISK_PATH(), m.getDISK_SIZE());
            }
            idx++;

        }
    }

    private void createDisk(VirtualMachine virtualMachine, int index, String diskType, int changeDiskSize) {
        virtualMachine.update()
                .withNewDataDisk(changeDiskSize
                        , index
                        , CachingTypes.READ_WRITE
                        , StorageAccountTypes.fromString( diskType))
                .apply();
    }

    private void updateDisk( String diskId, int changeDiskSize) {
        Disk disk = client.disks()
                .getById(diskId);

        if(disk.sizeInGB() != changeDiskSize) {
            if (changeDiskSize < disk.sizeInGB()) {
                throw new IllegalArgumentException("현재 DISK 값보다 낮은 DISK 값을 선택할 수 없습니다.");
            }

            disk.update()
                    .withSizeInGB(changeDiskSize)
                    .apply();

        }
    }


}
