package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.management.compute.GalleryImage;

import java.util.Map;

public class AZCheckTemplatePath extends AzureCommon {
    public AZCheckTemplatePath(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam  param, Map result) throws Exception {

        return null;
    }

    public VMInfo run(String imgId) throws Exception {
        Map<String, String> imgInfo = getInfoFromUrl(imgId);
        VMInfo result = new VMInfo();


        GalleryImage image = client.galleryImages().getByGallery(imgInfo.get("resourcegroups"), imgInfo.get("galleries"), imgInfo.get("images"));
        String osName = image.osType().name();
        result.setGUEST_NM(osName);



        //result.setDISK_SIZE(image.getVersion(imgInfo.get("versions")).storageProfile().osDiskImage().sizeInGB());
        return result;
    }
}
