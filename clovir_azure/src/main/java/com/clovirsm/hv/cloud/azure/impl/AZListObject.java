package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.PagedList;
import com.microsoft.azure.management.compute.*;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AZListObject extends AzureCommon {
    public AZListObject(IConnection conn) {
        super(conn);
    }
    @Override
    public String run1(HVParam  param, Map result) throws Exception {
        List list = new ArrayList();
        String parent =  super.getAzureConnection().currentRegion.name();
        add(list, parent, super.getAzureConnection().currentRegion.label(), OBJ_TYPE_REGIONS, null);
        addChild(list, parent );
        result.put(HypervisorAPI.PARAM_LIST, list);
        return parent;
    }
    private void add(List result,String id, String name, String type, String parent, JSONObject prop)
    {
        Map map = new HashMap();
        map.put("OBJ_ID", id);
        map.put("OBJ_NM", name);
        map.put("OBJ_TYPE_NM", type);
        map.put("PARENT_OBJ_NM", parent);
        if(prop !=null){
            map.put("OBJ_PROP", prop.toString());
        }
        result.add(map);
    }
    private void add(List result,String id, String name, String type, String parent )
    {
        add(result, id, name, type, parent, null);
    }

    private void addChild(List list, String parent ) throws Exception {
        getInstanceType(list, parent);
        getTemplate(list, parent );
        getDiskType(list, parent);
    }

    private void getTemplate(List list, String parent ) throws Exception {


        PagedList<Gallery> galleries = client.galleries().list();
        galleries.loadAll();
        for (Gallery gallery : galleries) {
            PagedList<GalleryImage> galleryImages = client.galleryImages().listByGallery(gallery.resourceGroupName(),gallery.name());
            galleryImages.loadAll();
            for (GalleryImage galleryImage : galleryImages) {
                long count = galleryImage.listVersions().size();
                if(count==0) continue;
                GalleryImageVersion galleryImageVersion = galleryImage.listVersions().stream().skip(count - 1).findFirst().orElseThrow(NullPointerException::new);
                add(list, galleryImageVersion.id(), galleryImage.name(), OBJ_TYPE_TEMPLATE, parent);
            }
        }

    }

    private void getInstanceType(List list, String parent) {
        PagedList<ComputeSku> skus = client.computeSkus().listbyRegionAndResourceType( getAzureConnection().currentRegion, ComputeResourceType.VIRTUALMACHINES);
        skus.loadAll();
        for(ComputeSku sku:skus) {
            try {
                add(list, sku.name().toString(), getInstanceName(sku), OBJ_TYPE_INSTANCE_TYPE, parent, getInstanceProp(sku));

            } catch (java.lang.NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private String getInstanceName(ComputeSku machineType){
        return machineType.name().toString()  ;
    }
    private JSONObject getInstanceProp(ComputeSku machineType) {
        JSONObject json = new JSONObject();
        json.put(HypervisorAPI.PARAM_CPU, getCPU(machineType.capabilities()));
        json.put(HypervisorAPI.PARAM_MEM,  getRAM(machineType.capabilities() ))  ;

        return json;
    }
    public static int getCPU(List<ResourceSkuCapabilities> capabilities){
        return CommonUtil.getInt(getCapaValue(capabilities, "vCPUs"));
    }
    public static int getRAM(List<ResourceSkuCapabilities> capabilities){
        return  CommonUtil.getInt(getCapaValue(capabilities, "MemoryGB"));
    }
    public static String getCapaValue(List<ResourceSkuCapabilities> capabilities, String field){
        return capabilities.stream().filter( c-> field.equals( c.name())).map( c-> c.value()).findFirst().orElse(null );

    }
    private void getDiskType(List list, String parent ) {
        PagedList<ComputeSku> skus = client.computeSkus().listbyRegionAndResourceType(getAzureConnection().currentRegion, ComputeResourceType.DISKS);
        skus.loadAll();
        for(ComputeSku sku:skus){
            add(list, sku.name().toString(), sku.name().toString(), OBJ_TYPE_DISK_TYPE, parent, getDiskTypeProp(sku.capabilities()));
        }

    }
    private JSONObject getDiskTypeProp(List<ResourceSkuCapabilities> capabilities)   {
        JSONObject result = new JSONObject();
        result.put("min", getCapaValue(capabilities,"MinSizeGiB"  ));
        result.put("max", getCapaValue(capabilities,"MaxSizeGiB"  ));
        return result;
    }
}
