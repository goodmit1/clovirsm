package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.rest.ServiceCallback;

import java.util.Map;

public class AZDiskDeleteAction extends AzureCommon {
    public AZDiskDeleteAction(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }

    protected void delete(  String diskId){
        System.out.println("delete disk "  + diskId);
        client.disks().deleteByIdAsync(diskId, new ServiceCallback<Void>() {
            @Override
            public void failure(Throwable throwable) {
                throwable.printStackTrace();
                throw new RuntimeException("DISK 삭제 중 오류가 발생했습니다.");
            }
            @Override
            public void success(Void aVoid) {
            }
        });
    }

}
