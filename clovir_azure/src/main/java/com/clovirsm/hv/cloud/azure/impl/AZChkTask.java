package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.PagedList;
import com.microsoft.azure.management.compute.VirtualMachine;
import com.microsoft.azure.management.resources.Deployment;
import com.microsoft.azure.management.resources.DeploymentOperation;
import com.microsoft.azure.management.resources.StatusMessage;

import java.util.Map;

public class AZChkTask extends AzureCommon {

    private VirtualMachine virtualMachine;

    public AZChkTask(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }


    public boolean run(  String taskId) throws Exception {
        Boolean resultTF = false;

        Deployment deployment = client.deployments().getById(taskId);
        /*PagedList<DeploymentOperation> operations = deployment.deploymentOperations().list();
        for (DeploymentOperation operation : operations) {
            if (operation.targetResource() != null) {
                System.out.println("operation.statusCode() = " + operation.statusCode());
                resultTF = operation.statusCode().equals("OK") || operation.statusCode().equals("Created");
                if (!resultTF)
                    break;
            }
        }*/
        if(deployment == null){
            throw new Exception("deployment "  + taskId + " not found");
        }
        //System.out.println(deployment.provisioningState());
        if("Failed".equals(deployment.provisioningState())){
            throw new Exception( getErrMsg(deployment));
        }
        else if("Succeeded".equals(deployment.provisioningState())){
            resultTF = true;
        }
        return resultTF;
    }
    protected String getErrMsg(Deployment deployment){
        PagedList<DeploymentOperation> operations = deployment.deploymentOperations().list();
        for (DeploymentOperation operation : operations) {
            if (operation.targetResource() != null) {

                boolean resultTF = operation.statusCode().equals("OK") || operation.statusCode().equals("Created");
                if (!resultTF){
                    return  ((StatusMessage)operation.statusMessage()).error().message();

                }

            }
        }
        return null;
    }


    /*
    public void checkInstanceState(Map param, Map result){
        try {
            virtualMachine = client.virtualMachines()
                    .getByResourceGroup((String) param.get(HypervisorAPI.PARAM_RSC_GROUP_NM), (String) param.get(HypervisorAPI.PARAM_VM_NM));
            String status = virtualMachine.instanceView().statuses().stream()
                    .filter(instanceViewStatus -> instanceViewStatus.code().contains("PowerState"))
                    .findFirst()
                    .map(InstanceViewStatus::code)
                    .map(String::toString)
                    .orElse("stop");
            result.put("instances", virtualMachine);
            if(status.contains("running"))
                result.put("RESULT", true);
            else
                result.put("RESULT", false);

        } catch (NullPointerException e){
            throw new NullPointerException("값이 없습니다");
        }
    }*/
}