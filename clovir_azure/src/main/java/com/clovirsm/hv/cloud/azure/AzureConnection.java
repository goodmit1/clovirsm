package com.clovirsm.hv.cloud.azure;

import com.clovirsm.hv.IConnection;
import com.microsoft.azure.AzureEnvironment;
import com.microsoft.azure.AzureResponseBuilder;
import com.microsoft.azure.credentials.ApplicationTokenCredentials;
import com.microsoft.azure.management.Azure;
import com.microsoft.azure.management.resources.fluentcore.arm.Region;
import com.microsoft.azure.serializer.AzureJacksonAdapter;
import com.microsoft.rest.LogLevel;
import com.microsoft.rest.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class AzureConnection implements IConnection {
    public Logger logger = LoggerFactory.getLogger(this.getClass());
    public Region currentRegion;
    protected Azure client = null;
    private String url;

    public Azure getClient() {
        return client;
    }


    @Override
    public String getURL() {

        return url;
    }


    @Override
    public void connect(String url, String userId, String pwd, Map prop) throws Exception {
        this.currentRegion = com.microsoft.azure.management.resources.fluentcore.arm.Region.fromName(url);


        String tenantId = (String) prop.get("tenantId");
        String subscriptionId =  (String) prop.get("subscriptionId");
        ApplicationTokenCredentials credentials = new ApplicationTokenCredentials(userId, tenantId, pwd, AzureEnvironment.AZURE);
        RestClient restClient = new RestClient.Builder()
                .withBaseUrl(AzureEnvironment.AZURE, AzureEnvironment.Endpoint.RESOURCE_MANAGER)
                .withSerializerAdapter(new AzureJacksonAdapter())
                .withResponseBuilderFactory(new AzureResponseBuilder.Factory())
                .withReadTimeout(150, TimeUnit.SECONDS)
                .withLogLevel(LogLevel.NONE)
                .withCredentials(credentials).build();
        client = Azure.authenticate(restClient, credentials.domain(), subscriptionId).withDefaultSubscription();
        this.url = url;
    }


    @Override
    public void disconnect() {
        // TODO Auto-generated method stub
        client = null;
    }


    @Override
    public boolean isConnected() {
        // TODO Auto-generated method stub
        return client != null;
    }


}
