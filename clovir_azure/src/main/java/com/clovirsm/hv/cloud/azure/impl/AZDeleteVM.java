package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.management.compute.VirtualMachine;
import com.microsoft.azure.management.compute.VirtualMachineDataDisk;
import com.microsoft.rest.ServiceCallback;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class AZDeleteVM extends AzureCommon {
    public AZDeleteVM(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam paramMap, Map result) throws Exception {
        VMInfo param =   paramMap.getVmInfo();
        try {

            VirtualMachine vm = client.virtualMachines().getByResourceGroup(param.getRSC_GROUP_NM(), param.getVM_NM());
            if(vm == null){
                System.out.println("VM not found");
                return null;
            }
            List<String> nicIds = vm.networkInterfaceIds();
            String osDiskId = vm.osDiskId();
            Collection<VirtualMachineDataDisk> disks = vm.dataDisks().values();


            client.virtualMachines().deleteByResourceGroupAsync(param.getRSC_GROUP_NM(), param.getVM_NM(), new ServiceCallback<Void>() {

                @Override
                public void failure(Throwable throwable) {
                    throwable.printStackTrace();
                    throw new RuntimeException("VM 삭제 중 오류가 발생했습니다.");
                }

                @Override
                public void success(Void aVoid) {
                    AZNetworkInterfaceDeleteAction azNetworkInterfaceDeleteAction = new AZNetworkInterfaceDeleteAction(getAzureConnection());
                    try {
                        for (String nicId : nicIds) {
                            System.out.println("delete Nic "  + nicId);
                            azNetworkInterfaceDeleteAction.delete( nicId);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    AZDiskDeleteAction azDiskDeleteAction = new AZDiskDeleteAction(getAzureConnection());
                    try{
                        azDiskDeleteAction.delete(osDiskId);
                        for(VirtualMachineDataDisk disk:disks) {

                            azDiskDeleteAction.delete(disk.id());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println("delete deployment "  +  param.getVM_NM());
                    client.deployments().deleteByResourceGroup(param.getRSC_GROUP_NM(), param.getVM_NM());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("VM 삭제 중 오류가 발생했습니다.");
        }
        return param.getVM_NM();
    }
    /*
    protected void deleteByTemplate(VMInfo vmInfo){
        Deployment deployment = client.deployments().getByResourceGroup(vmInfo.getRSC_GROUP_NM(), vmInfo.getVM_NM());
        if(deployment != null){
            List<ResourceReference> resources = deployment.outputResources();
            for(ResourceReference rsc : resources){
                GenericResource obj = client.genericResources().getById(rsc.id());
                if(obj.name().startsWith(vmInfo.getVM_NM())){
                    client.
                }
            }
        }
    }*/
}
