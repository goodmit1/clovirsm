package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.management.network.NetworkSecurityGroup;
import com.microsoft.azure.management.network.SecurityRuleProtocol;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AZCreateSecurityRule extends AzureCommon {
    public AZCreateSecurityRule(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        AZGetVM vm = new AZGetVM(this.getConnect());
        NetworkSecurityGroup securityGroup = vm.getSecurityGroup(param.getVmInfo());
        doAction(param.getVmInfo().getRSC_GROUP_NM(), securityGroup, param.getFwInfo().getProtocol(), param.getFwInfo().getIps(),   param.getFwInfo().getPorts());
        return null;
    }

    protected String getFWName(String ip){
        return   ip;
    }
    public String doAction(String rscGroup, NetworkSecurityGroup securityGroup, String protocol, String[] ips, String[] ports) throws Exception {
        try {

            int priority = securityGroup.securityRules().size() + 1;

            for (String port : ports) {
                String[] newIps = null;
                boolean isCreate = false;
                if(securityGroup.securityRules().get(port) != null){
                    newIps = putIps(securityGroup.securityRules().get(port).sourceAddressPrefixes(), ips);
                    isCreate = newIps.length != ips.length;
                }
                else{
                    newIps = ips;
                    isCreate = true;
                }
                if(isCreate) {
                    client.networkSecurityGroups().getByResourceGroup(rscGroup, securityGroup.name())
                            .update()
                            .defineRule(port)
                            .allowInbound()
                            .fromAddresses(newIps)
                            .fromAnyPort()
                            .toAnyAddress()
                            .toPortRanges(port)
                            .withProtocol(SecurityRuleProtocol.fromString(protocol))
                            .withPriority(priority++ * 100)
                            .attach()
                            .apply();
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Network Security Group 업데이트에 실패했습니다.");
        }
        return null;
    }

    private String[] putIps(List<String> oldSourceIps, String[] ips) {
        List<String> newIps = new ArrayList<>();
        newIps.addAll(oldSourceIps);
       for(String ip : ips ){
           boolean isFind = oldSourceIps.stream().filter(i -> isInRange(i, ip)).findFirst().orElse(null) != null;
           if(!isFind){
               newIps.add(ip);
           }
       }
       return newIps.stream().toArray(String[]::new);
    }


}
