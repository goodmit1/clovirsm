package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.microsoft.azure.management.network.NetworkSecurityGroup;
import com.microsoft.azure.management.network.SecurityRuleProtocol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class AZDeleteSecurityRule extends AZCreateSecurityRule {
    public AZDeleteSecurityRule(IConnection conn) {
        super(conn);
    }

    @Override


    public String doAction(String rscGroup, NetworkSecurityGroup securityGroup, String protocol, String[] ips, String[] ports) throws Exception {
        try {

            for(String port:ports) {
                String[] newIps = removeIps(securityGroup.securityRules().get(port).sourceAddressPrefixes(), ips);
                /* rule delete */
                if(newIps.length==0) {
                    client.networkSecurityGroups().getById(securityGroup.id())
                            .update().withoutRule(port).apply();
                }
                else {
                    client.networkSecurityGroups().getByResourceGroup(rscGroup, securityGroup.name())
                            .update()
                            .defineRule(port)
                            .allowInbound()
                            .fromAddresses(newIps)
                            .fromAnyPort()
                            .toAnyAddress()
                            .toPortRanges(port)
                            .withProtocol(SecurityRuleProtocol.fromString(protocol))
                            .attach()
                            .apply();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Network Security Group 삭제에 실패했습니다.");
        }
        return null;
    }
    protected String[] removeIps(List<String> oldSourceIps, String[] ips) {
        List<String> newIps = new ArrayList<>();
        List<String> ipsArr = Arrays.asList(ips);
        for(String oldIp : oldSourceIps ){
            boolean isFind =ipsArr.stream().filter(ip -> isInRange(oldIp, ip)).findFirst().orElse(null) != null;
            if(!isFind){
                newIps.add(oldIp);
            }
        }
        return newIps.stream().toArray(String[]::new);
    }
}
