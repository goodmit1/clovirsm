package com.clovirsm.hv.cloud.azure.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.azure.AzureCommon;
import com.clovirsm.hv.obj.HVParam;
import com.microsoft.azure.management.network.NetworkInterface;
import com.microsoft.azure.management.network.NetworkSecurityGroup;
import com.microsoft.rest.ServiceCallback;

import java.util.Map;

public class AZNetworkInterfaceDeleteAction extends AzureCommon {
    public AZNetworkInterfaceDeleteAction(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }

    protected void delete(String id){
        NetworkInterface nic = client.networkInterfaces().getById(id);
        NetworkSecurityGroup nsgroup = nic.getNetworkSecurityGroup();
        String publicIp = nic.primaryIPConfiguration().publicIPAddressId();
        client.networkInterfaces().deleteByIdAsync(id,
                new ServiceCallback<Void>() {
                    @Override
                    public void failure(Throwable throwable) {
                        throwable.printStackTrace();
                        throw new RuntimeException("networkInterface 삭제 중 오류가 발생했습니다.");
                    }
                    @Override
                    public void success(Void aVoid) {
                        AZPublicIpDeleteAction azPublicIpDeleteAction = new AZPublicIpDeleteAction(getAzureConnection());
                        AZDeleteSecurityGroup azDeleteSecurityRule = new AZDeleteSecurityGroup(getAzureConnection());
                        try {
                            azPublicIpDeleteAction.delete(  publicIp);
                            azDeleteSecurityRule.delete( nsgroup.id());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

}
