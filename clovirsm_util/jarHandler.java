import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class jarHandler {
    private static List old_file_names = new ArrayList();
    private static List old_file_sizes = new ArrayList();
    private static List new_file_names = new ArrayList();
    private static List new_file_sizes = new ArrayList();
    private static List classes_file_RealPath = new ArrayList();
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        // jar : .jar 파일이 저장되어 있는 폴더위치 / classes : classes의 경로
        String old_path = "C:\\Users\\kimo9\\Desktop\\굿모닝아이텍\\test";    // (구)버전
        String new_path = "C:\\Users\\kimo9\\Desktop\\굿모닝아이텍\\test2";   // (신)버전

        System.out.print("검색 타입을 선택하세요. ( 1: .jar / 2: classes / -1: 종료) : ");
        int searchType = sc.nextInt();

        if( searchType == 1 ) {
            // jar 비교
            jarCompare(old_path, new_path);
        }else if( searchType == 2) {
            // classes 비교
            classesCompare(old_path, new_path);
        }else {
            // Type Error
            System.out.println("[ERROR] 검색 타입 오류");
        }

    }

    // .jar 파일 경로 추출
    private static List jarPath(String path) {
        File dir = new File(path);
        File[] files = dir.listFiles();
        List paths = new ArrayList();

        for(int i = 0; i < files.length; i++) {
            if(files[i].getName().endsWith(".jar")) {
                paths.add(files[i].getPath());
            }
        }

        Collections.sort(paths);

        return paths;
    }

    // jar 로직
    private static void jarCompare(String old_path, String new_path) throws IOException {
        List old_paths = jarPath(old_path);
        List new_paths = jarPath(new_path);

        if(old_paths.size() != new_paths.size()) {
            System.out.println("(구)버전 : "+old_paths.size()+" | (신)버전 : "+new_paths.size());
            System.out.println("[ERROR] : 비교 대상 파일 수 오류");
            return ;
        }

        System.out.println("\n탐색된 jar 파일 수 : "+new_paths.size());
        for(int i = 0; i < new_paths.size(); i++) {
            System.out.println("\n(구)버전 : "+old_paths.get(i));
            System.out.println("(신)버전 : "+new_paths.get(i));
            System.out.println("===================================================================================");
            JarFile old_jarClass = new JarFile( (String) old_paths.get(i));
            JarFile new_jarClass = new JarFile( (String) new_paths.get(i));

            // file 정보 출력
            jarClassExtraction(old_jarClass, old_file_names, old_file_sizes);
            jarClassExtraction(new_jarClass, new_file_names, new_file_sizes);

            fileCompare(old_file_names, old_file_sizes, new_file_names, new_file_sizes);
            System.out.println("===================================================================================");
        }
    }

    // classes 로직
    private static void classesCompare(String old_path, String new_path) throws IOException {
        directoryPath(old_path, old_file_names, old_file_sizes, null);
        directoryPath(new_path, new_file_names, new_file_sizes, classes_file_RealPath);

        System.out.println("\n(구)버전 : "+old_path);
        System.out.println("(신)버전 : "+new_path);
        System.out.println("===================================================================================");
        fileCompare(old_file_names, old_file_sizes, new_file_names, new_file_sizes);
        System.out.println("===================================================================================");

    }

    // classes directory 파일 추출 함수
    private static void directoryPath(String classesPath, List file_names, List file_sizes, List classes_file_RealPath) throws IOException {
        SimpleFileVisitor<Path> file = new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) throws IOException {
                if(classes_file_RealPath != null) {
                    classes_file_RealPath.add(filePath.toRealPath());
                }
                file_names.add(filePath.getFileName());
                file_sizes.add(attrs.size());
                return FileVisitResult.CONTINUE;
            }
        };
        Files.walkFileTree(Paths.get(classesPath), file);
    }

    // 파일 비교 함수
    private static void fileCompare(List old_files, List old_sizes, List new_files, List new_sizes) {
        int changes = 0;
        int news = 0;
        List newsList = new ArrayList();
        List changeList = new ArrayList();

        for(int i = 0; i < new_files.size(); i++) {
            int cnt = 0;
            for(int j = 0; j < old_files.size(); j++) {
                if(new_files.get(i).equals(old_files.get(j))) {
                    if(!new_sizes.get(i).equals(old_sizes.get(j))) {
                        changes++;
                        if(classes_file_RealPath.size() > 0) {
                            changeList.add(classes_file_RealPath.get(i));
                        }else {
                            changeList.add(new_files.get(i));
                        }
                    }
                    cnt++;
                }
            }

            if(cnt == 0) {
                news++;
                if(classes_file_RealPath.size() > 0) {
                    changeList.add(classes_file_RealPath.get(i));
                }else {
                    changeList.add(new_files.get(i));
                }
            }
        }

        System.out.println("기존 클래스 변경 사항 : "+changes);
        for(int i = 0; i < changes; i++) {
            System.out.println(changeList.get(i));
        }

        System.out.println("\n새로 생성된 클래스 파일 : "+news);
        for(int i = 0; i < news; i++) {
            System.out.println(newsList.get(i));
        }
    }

    // jar 내부 파일 추출 함수
    private static void jarClassExtraction(JarFile jar, List file_names, List file_sizes) {
        for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements();) {
            JarEntry entry = entries.nextElement();
            String file = entry.getName();
            long filesize = entry.getSize();

            if(!entry.isDirectory()) {
                file_names.add(file);
                file_sizes.add(filesize);
            }
        }
    }

}
