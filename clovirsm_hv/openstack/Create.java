import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openstack4j.api.Builders;
import org.openstack4j.api.OSClient;
import org.openstack4j.model.compute.Flavor;
import org.openstack4j.model.compute.Image;
import org.openstack4j.model.compute.Server;
import org.openstack4j.model.compute.ServerCreate;
import org.openstack4j.model.network.Port;
import org.openstack4j.model.network.Subnet;
import org.openstack4j.openstack.OSFactory;

public class Create extends Common {
	public void run()
	{
		connect();
		Port port = os.networking().port().create(Builders.port()
	              .name("port-1")
	              .networkId(super.getNWId("syk"))
	              .build());
		 
	 
		ServerCreate sc = Builders.server().name("syk").flavor(super.getFlavorId("m1.small"))
				.image(super.getImageId("ubuntu15.10"))
				.addAdminPass("test123")
				.addNetworkPort(port.getId())
				//.addSecurityGroup(super.getSecurityGroupId("jclouds-test"))
				.addSecurityGroup("jclouds-test")
				.build();
		 
		Server server = os.compute().servers().boot(sc);
	}
	public void list()
	{
		List<? extends Server> servers = os.compute().servers().list();
		
	}
	public static void main(String[] args)
	{
		Create c = new Create();
		c.run();
		 
	}
}
