package com.clovirsm.hv.util;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Map;

public class SshUtils {

    /**
     * @method : createSsh
     * @author : kimjinwon
     * @Param :
     * @comment : JSch 라이브러리를 사용하여 개인키 /
     ***/
    public Map createSsh() throws JSchException {

        Map<String, String> privateKey = new HashMap<>();

        ByteArrayOutputStream bytePrivateKey = new ByteArrayOutputStream();// 개인키
        ByteArrayOutputStream bytePublicKey = new ByteArrayOutputStream();// 공개키

        JSch jsch = new JSch();
        String comment = "";
        KeyPair kpair = KeyPair.genKeyPair(jsch, 2, 2048);
        kpair.writePrivateKey(bytePrivateKey);
        kpair.writePublicKey(bytePublicKey, comment);
        privateKey.put("PRIVATE_KEY", bytePrivateKey.toString());
        kpair.dispose();

        return privateKey;
    }

    /**
     * @method : createPublicKey
     * @author : kimjinwon
     * @Param :
     * privateKey : 사용자 개인키
     * name : 사용자 아이디
     * @comment : 개인키를 공개키로 만드는 Method
     ***/
    public String createPublicKey(String privateKey, String name) {
        String sshKey = "";

        try {
            PrivateKey prbKey = CryptoUtils.convertPemToPrivateKey(privateKey);
            RSAPublicKey pubKey = CryptoUtils.convertPrivateKeyToRsaPublicKey(prbKey);
            sshKey = CryptoUtils.encodeAsOpenSSH(pubKey, name);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sshKey;
    }

    public String changeDbPrbKeyToPrbKey(String key) {
        key = key.replace("-----BEGIN RSA PRIVATE KEY-----", "")
                .replace("-----END RSA PRIVATE KEY-----", "")
                .replace(" ", "\n");


        return "-----BEGIN RSA PRIVATE KEY-----" + key + "-----END RSA PRIVATE KEY-----";
    }

    public String changePrbKeyToPubKey(String privateKey) throws IOException, GeneralSecurityException {
        PrivateKey prbKey = CryptoUtils.convertPemToPrivateKey(privateKey);
        RSAPublicKey pubKey = CryptoUtils.convertPrivateKeyToRsaPublicKey(prbKey);
        String rsaPubKey = CryptoUtils.encodeAsOpenSSH(pubKey, null);
        return changePubKeyToSshPubKey(rsaPubKey);
    }

    public String changePubKeyToSshPubKey(String rsaPubKey) {
        String[] keyParts = rsaPubKey.split("\\s"); // header + content + comment
        if (keyParts.length < 2 || !keyParts[0].equals("ssh-rsa"))
            throw new IllegalArgumentException("The key " + rsaPubKey + " is not a properly formatted RSA key.");
        StringBuilder sb = new StringBuilder("---- BEGIN SSH2 PUBLIC KEY ----\n");
        final int rowLength = 70;
        final String keyContent = keyParts[1];
        for (int i = 0; i * rowLength < keyContent.length(); i++) {
            sb.append(keyContent, i * rowLength, Math.min((i + 1) * rowLength, keyContent.length())).append('\n');
        }
        sb.append("---- END SSH2 PUBLIC KEY ----");
        return sb.toString();
    }
}
