package com.clovirsm.hv.util;


import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;


public class CryptoUtils {


    public static String encodeAsOpenSSH(RSAPublicKey key, String subject) {
        return encodeAsOpenSSH(key) + " " + subject;
    }

    public static String encodeAsOpenSSH(RSAPublicKey key) {
        byte[] keyBlob = keyBlob(key.getPublicExponent(), key.getModulus());
        byte[] encodedByteArray = Base64.getEncoder().encode(keyBlob);
        String encodedString = new String(encodedByteArray);
        return "ssh-rsa " + encodedString;
    }

    private static byte[] keyBlob(BigInteger publicExponent, BigInteger modulus) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            writeLengthFirst("ssh-rsa".getBytes(), out);
            writeLengthFirst(publicExponent.toByteArray(), out);
            writeLengthFirst(modulus.toByteArray(), out);
            return out.toByteArray();
        } catch (IOException e) {
            System.out.println("Failed");
            e.printStackTrace();
        }
        return null;
    }

    private static void writeLengthFirst(byte[] array, ByteArrayOutputStream out) throws IOException {
        out.write((array.length >>> 24) & 0xFF);
        out.write((array.length >>> 16) & 0xFF);
        out.write((array.length >>> 8) & 0xFF);
        out.write((array.length >>> 0) & 0xFF);
        if (array.length == 1 && array[0] == (byte) 0x00)
            out.write(new byte[0]);
        else
            out.write(array);
    }

    public static RSAPublicKey convertPrivateKeyToRsaPublicKey(PrivateKey privateKey) {
        RSAPrivateCrtKey privk = (RSAPrivateCrtKey) privateKey;
        RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
        KeyFactory keyFactory;
        PublicKey myPublicKey = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
            myPublicKey = keyFactory.generatePublic(publicKeySpec);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return (RSAPublicKey) myPublicKey;
    }

    public static PrivateKey convertPemToPrivateKey(String key) throws IOException, GeneralSecurityException {
        PrivateKey privateKey = null;
        try {
            key = key.replaceAll("\\n", "").replace("-----BEGIN RSA PRIVATE KEY-----", "")
                    .replace("-----END RSA PRIVATE KEY-----", "").replaceAll("\\s", "");
            byte[] privKeyByteArray = Base64.getDecoder().decode(key.getBytes());

            ASN1Sequence seq = ASN1Sequence.getInstance(privKeyByteArray);
            RSAPrivateKey bcPrivateKey = RSAPrivateKey.getInstance(seq);
            JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
            AlgorithmIdentifier algId = new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE);
            privateKey = converter
                    .getPrivateKey(new PrivateKeyInfo(algId, bcPrivateKey));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return privateKey;

    }
}