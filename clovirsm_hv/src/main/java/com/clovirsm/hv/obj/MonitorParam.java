package com.clovirsm.hv.obj;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MonitorParam extends HashMap {

    boolean max = false;
    public MonitorParam(){
        super();
    }

    public MonitorParam(Map m) {
        super();
        super.putAll(m);
    }

    public boolean isInnerHour(){
        return NumberUtil.getBoolean( get("INNER_HOUR"),false);
    }

    public void setInnerHour(boolean innerHour){
        put("INNER_HOUR" ,innerHour);
    }

    public String getInterval(){
        return (String) get(VMWareCommon.PARAM_INTERVAL);
    }

    public void setInterval(String i){
        put(VMWareCommon.PARAM_INTERVAL ,i);
    }

    public String getPeriod() {
        return (String)  get(VMWareAPI.PARAM_PERIOD);
    }

    public void setPeriod(String p) {
        put(VMWareAPI.PARAM_PERIOD, p);
    }

    public String getTargetObjType(){
        return (String)  get( HypervisorAPI.PARAM_OBJECT_TYPE);
    }

    public void setTargetObjType(String type){
        put( HypervisorAPI.PARAM_OBJECT_TYPE, type);
    }

    public String getTargetObjNm(){
        return (String)  get( HypervisorAPI.PARAM_OBJECT_NM);
    }

    public void setTargetObjNm(String type){
        put( HypervisorAPI.PARAM_OBJECT_NM, type);
    }

    public String getCategory() {
        return (String)  get(VMWareAPI.PARAM_CATEGORY);
    }

    public void setCategory(String category) {
        put(VMWareAPI.PARAM_CATEGORY, category);
    }

    public Date getStartTime() {
       return  (Date)get(VMWareAPI.PARAM_START_DT);
    }

    public void setStartTime(Date startTime) {
        put(VMWareAPI.PARAM_START_DT, startTime);
    }

    public Date getFinishTime() {
        return (Date)get(VMWareAPI.PARAM_FINISH_DT);
    }

    public void setFinishTime(Date finishTime) {
        put(VMWareAPI.PARAM_FINISH_DT, finishTime);
    }

    public int getPageNo() {
        return CommonUtil.getInt(  get(VMWareAPI.PARAM_PAGE_NO));
    }

    public void setPageNo(int pageNo) {
        put(VMWareAPI.PARAM_PAGE_NO, pageNo);
    }

    public int getPageSize() {
        return CommonUtil.getInt(  get(VMWareAPI.PARAM_PAGE_SIZE));
    }

    public void setPageSize(int pageSize) {
        put(VMWareAPI.PARAM_PAGE_SIZE,pageSize);
    }

    public String getCLUSTER() {
        return (String)super.get(VMWareAPI.PARAM_CLUSTER);
    }
    public void setCLUSTER(String s) {
        super.put(VMWareAPI.PARAM_CLUSTER, s);
    }
    public boolean includeMax() {
        return max;
    }
    public void setIncludeMax(boolean b){
        max = b;

    }
}
