package com.clovirsm.hv.obj;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IAfterProcess;

import java.util.Map;

public class HVParam {
    Map connParam;
    IAfterProcess after;
    VMInfo vmInfo;
    DiskInfo diskInfo;
    MonitorParam monitorParam;

    public FWInfo getFwInfo() {
        return fwInfo;
    }

    public void setFwInfo(FWInfo fwInfo) {
        this.fwInfo = fwInfo;
    }

    FWInfo fwInfo;

    public HVParam(Map connParam ){
        this(connParam, null, null);
    }

    public HVParam(Map connParam, VMInfo vmInfo){
        this(connParam, vmInfo, null);
    }
    public HVParam(Map connParam, VMInfo vmInfo, DiskInfo diskInfo){
        this(connParam, vmInfo, diskInfo, null);
    }

    public HVParam(Map connParam, VMInfo vmInfo, DiskInfo diskInfo, IAfterProcess after){
        this.connParam = connParam;
        this.vmInfo = vmInfo;
        this.diskInfo = diskInfo;
        this.after = after;
    }

    public MonitorParam getMonitorParam() {
        return monitorParam;
    }

    public void setMonitorParam(MonitorParam monitorParam) {
        this.monitorParam = monitorParam;
    }

    public IAfterProcess getAfter() {
        return after;
    }

    public void setAfter(IAfterProcess after) {
        this.after = after;
    }

    public Map getConnParam() {
        return connParam;
    }

    public void setConnParam(Map connParam) {
        this.connParam = connParam;
    }

    public VMInfo getVmInfo() {
        return vmInfo;
    }

    public void setVmInfo(VMInfo vmInfo) {
        this.vmInfo = vmInfo;
    }

    public DiskInfo getDiskInfo() {
        return diskInfo;
    }

    public void setDiskInfo(DiskInfo diskInfo) {
        this.diskInfo = diskInfo;
    }
}
