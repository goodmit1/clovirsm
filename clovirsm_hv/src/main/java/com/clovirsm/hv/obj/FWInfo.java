package com.clovirsm.hv.obj;

public class FWInfo  {
    String[] ips;
    String[] ports;
    String protocol;
    public static final String PROTOCOL_TCP="tcp";

    public static final String PROTOCOL_UDP="udp";
    public String getProtocol() {
        return protocol==null?"tcp":protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }



    public FWInfo(String protocol, String[] ips , String[] ports){
        this.ips = ips;
        this.protocol = protocol;
        this.ports = ports;
    }
    public String[] getIps() {
        return ips;
    }

    public void setIps(String[] ips) {
        this.ips = ips;
    }

    public String[] getPorts() {
        return ports;
    }

    public void setPorts(String[] ports) {
        this.ports = ports;
    }



}
