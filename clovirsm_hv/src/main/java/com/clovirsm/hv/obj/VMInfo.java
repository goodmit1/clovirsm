package com.clovirsm.hv.obj;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VMInfo extends HashMap {

    public VMInfo(){
        super();
    }
    public VMInfo(String vmNm){
        this.setVM_NM(vmNm);
    }
    public VMInfo(String vmNm, String vmId){
        this.setVM_NM(vmNm);
        this.setVM_HV_ID(vmId);
    }
    public VMInfo(Map orgParam){
        super.putAll(orgParam);
    }
    public String getVM_NM(){
        return (String)super.get(HypervisorAPI.PARAM_VM_NM);
    }
    public void setVM_NM(String vmNm){
        super.put(HypervisorAPI.PARAM_VM_NM, vmNm);
    }

    public String getRUN_CD(){
        return (String)super.get(HypervisorAPI.PARAM_RUN_CD);
    }
    public void setRUN_CD(String cd){
        super.put(HypervisorAPI.PARAM_RUN_CD, cd);
    }

    public int getCPU(){
        return (int)super.get(HypervisorAPI.PARAM_CPU);
    }
    public void setCPU(int cpu){
        super.put(HypervisorAPI.PARAM_CPU, cpu);
    }
    public int getRAM_SIZE(){
        return (int)super.get(HypervisorAPI.PARAM_MEM);
    }
    public void setRAM_SIZE(int mem){
        super.put(HypervisorAPI.PARAM_MEM, mem);
    }
    public int getDISK_SIZE(){
        return (int)super.get(HypervisorAPI.PARAM_DISK);
    }
    public void setDISK_SIZE(int i) {
        super.put(HypervisorAPI.PARAM_DISK,  i);
    }



    public String getSPEC_TYPE(){
        return (String)super.get(HypervisorAPI.PARAM_SPEC_TYPE);
    }
    public void setSPEC_TYPE(String SPEC_TYPE){
        super.put(HypervisorAPI.PARAM_SPEC_TYPE, SPEC_TYPE );
    }
    public String getFROM(){
        return (String)super.get(HypervisorAPI.PARAM_FROM);
    }
    public void setFROM(String FROM){
        super.put(HypervisorAPI.PARAM_FROM, FROM);
    }

    /** os disk 's diskType ( 사양에 포함된 디스크 종류 ) **/
    public String[] getDATASTORE_NM_LIST() {
        return (String[])super.get(VMWareAPI.PARAM_DATASTORE_NM_LIST);
    }
    public void setDATASTORE_NM_LIST(String[] list) {
        super.put(VMWareAPI.PARAM_DATASTORE_NM_LIST, list);
    }
    public String getFOLDER_NM() {
        return (String)super.get(VMWareAPI.PARAM_FOLDER_NM);
    }


    public List<DiskInfo> getDISK_LIST() {
        return (List<DiskInfo>)super.get(VMWareAPI.PARAM_DISK_LIST);
    }

    public void setDISK_LIST(List<DiskInfo> diskList) {
         super.put(VMWareAPI.PARAM_DISK_LIST, diskList);
    }
    public String getCLUSTER() {
        return (String)super.get(VMWareAPI.PARAM_CLUSTER);
    }
    public void setCLUSTER(String s) {
        super.put(VMWareAPI.PARAM_CLUSTER, s);
    }

    /**
     * clovirsm의 VM내 어드민 계정의 비밀번호
     * @return
     */
    public String getOS_ADMIN_PWD() {
        return (String)super.get("ADMIN_PWD");
    }
    public void setOS_ADMIN_PWD(String s) {
        super.put("ADMIN_PWD", s);
    }
    /**
     * VM ip 목록
     * @return
     */
    public List<IPInfo> getIP_LIST() {
        return ( List<IPInfo>)super.get(VMWareAPI.PARAM_IP_LIST);
    }
    public void setIP_LIST(List<IPInfo> s) {
        super.put(VMWareAPI.PARAM_IP_LIST, s);
    }
    /**
     * clovirsm 서버 아이피, 모든 VM의 22 포트를 방화벽에서 열기 위해 필요함.
     * @return
     */
    public String getDEFAULT_IP() {
        return (String)super.get("DEFAULT_IP");
    }
    public void setDEFAULT_IP(String s) {
        super.put("DEFAULT_IP", s);
    }

    /**
     * clovirsm의 VM내 어드민 계정의 아이디
     * @return
     */
    public String getOS_ADMIN_ID() {
        return (String)super.get("ADMIN_ID");
    }
    public void setOS_ADMIN_ID(String id) {
        super.put("ADMIN_ID", id);
    }

    /**
     * 서버 생성 후 PowerOn여부
     * @return
     */
    public boolean isPOWER_ON() {
        return "Y".equals(super.get("POWER_ON_YN"));
    }

    public void setPOWER_ON(boolean on){
        super.put("POWER_ON", on ? "Y":"N");
    }
    public String getPURPOSE() {
        return (String)super.get("PURPOSE");
    }

    /**
     * clovirsm의 VM내 어드민 계정의 SSH 퍼블릭 키
     * @return
     */
    public void setOS_ADMIN_SSH_KEY(String s) {
        super.put("ADMIN_SSH_KEY", s);
    }

    public String getOS_ADMIN_SSH_KEY( ) {
        return (String)super.get("ADMIN_SSH_KEY");
    }

    /**
     * VM id
     * @param id
     */
    public void setVM_HV_ID(String id) {
        super.put("VM_HV_ID", id);
    }
    public String getVM_HV_ID(){
        return (String)super.get("VM_HV_ID");
    }


    /**
     * os name
     * @param guestNm
     */
    public void setGUEST_NM(String guestNm) {super.put(HypervisorAPI.PARAM_GUEST_NM, guestNm);  }
    public String getGUEST_NM( ){
        return (String)super.get(HypervisorAPI.PARAM_GUEST_NM);
    }
    public boolean isWindows(){
        return "WINDOWS".equals(getGUEST_NM().toUpperCase());
    }
    public void setCMT(String cmt) {super.put(HypervisorAPI.PARAM_CMT, cmt);  }
    public String getCMT( ){
        return (String)super.get(HypervisorAPI.PARAM_CMT);
    }


    public void setPUBLIC_IP(String ip) {super.put(HypervisorAPI.PARAM_PUBLIC_IP, ip);  }
    public String getPUBLIC_IP( ){
        return (String)super.get(HypervisorAPI.PARAM_PUBLIC_IP);
    }


    /**
     * vmware resourcePool, azure resource group
     * @param nm
     */
    public void setRSC_GROUP_NM(String nm) {super.put(HypervisorAPI.PARAM_RSC_GROUP_NM, nm);  }
    public String getRSC_GROUP_NM( ){
        return (String)super.get(HypervisorAPI.PARAM_RSC_GROUP_NM);
    }

    public String getVM_ID( ){
        return (String)super.get("VM_ID");
    }

    public void setVM_ID(Object vm_id) {super.put("VM_ID", vm_id);  }

    public void setFOLDER_NM(  String folder) {
        super.put("FOLDER_NM", folder);
    }
    public String getREAL_DC_NM(){
        return (String) super.get("REAL_DC_NM");
    }
    public void setREAL_DC_NM(String dcNm){
        super.put("REAL_DC_NM", dcNm);
    }

    public String getDC_ID(){
        return (String) super.get("DC_ID");
    }
    public void setDC_ID(String id){
        super.put("DC_ID", id);
    }

    public int getGPU_SIZE(){
        return NumberUtil.getInt( get("GPU_SIZE"));
    }
    public void setGPU_SIZE(int size){
        super.put("GPU_SIZE", size);
    }

    public String getGPU_MODEL(){
        return (String) super.get("GPU_MODEL");
    }
    public void setGPU_MODEL(String model){
        super.put("GPU_MODEL", model);
    }

    public String getHOST_NM(){
        return (String) super.get("HOST_NM");
    }
    public void setHOST_NM(String name){
        super.put("HOST_NM", name);
    }

    public String getFT_YN(){
        return (String) super.get("FT_YN");
    }
    public void setFT_YN(String yn){
        super.put("FT_YN", yn);
    }


}
