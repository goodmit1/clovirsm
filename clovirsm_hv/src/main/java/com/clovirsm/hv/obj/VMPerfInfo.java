package com.clovirsm.hv.obj;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.NumberUtil;

import java.util.Map;

public class VMPerfInfo extends VMInfo{
    public VMPerfInfo(){
        super();
    }
    public VMPerfInfo(Map map){
       super(map);
    }

    public void setCPU_USAGE(float usage){
        super.put("CPU_USAGE", usage);
    }
    public float getCPU_USAGE( ){
       return NumberUtil.getFloat( super.get("CPU_USAGE" ));
    }
    public void setMEM_USAGE(float usage){
        super.put("MEM_USAGE", usage);
    }
    public float getMEM_USAGE(float usage){
       return NumberUtil.getFloat( super.get("MEM_USAGE" ));
    }
    public int getUSE_DISK_SIZE(){
        return NumberUtil.getInt( get("USE_DISK_SIZE"));
    }
    public void setUSE_DISK_SIZE(int size){
        super.put("USE_DISK_SIZE", size);
    }

    public void setDISK_USAGE(float usage){
        super.put("DISK_USAGE", usage);
    }
    public float getDISK_USAGE(float usage){
        return NumberUtil.getFloat( super.get("DISK_USAGE" ));
    }

    public void setNET_USAGE(float net_usage) {
        super.put("NET_USAGE", net_usage);
    }
    public float getNET_USAGE(float usage){
        return NumberUtil.getFloat( super.get("NET_USAGE" ));
    }
    public String getGPU_MODEL(){
        return (String) super.get("GPU_MODEL");
    }

    public void setGPU_MODEL(String model){
        super.put("GPU_MODEL", model);
    }

    public void setREG_DT(String model){
        super.put("REG_DT", model);
    }
    public String getREG_DT(){
        return (String) super.get("REG_DT");
    }
    public void setREG_TM(String model){
        super.put("REG_TM", model);
    }
    public String getREG_TM(){
        return (String) super.get("REG_TM");
    }
}
