package com.clovirsm.hv;

import com.clovirsm.hv.vmware.VMWareAPI;

import java.util.HashMap;
import java.util.Map;

public class DiskInfo extends HashMap {

    public DiskInfo(){
        super();
    }
    public DiskInfo(String diskType, int diskSize){
        super();
        setDATASTORE_NM_LIST(diskType);
        setDISK_SIZE(diskSize);
    }

    public DiskInfo(Map m) {
        super();
        putAll(m);
    }

    public int getDISK_SIZE() {
        return CommonUtil.getInt(super.get("DISK_SIZE"));
    }

    public void setDISK_SIZE(int DISK_SIZE) {
        put("DISK_SIZE",DISK_SIZE);
    }

    public String getDISK_PATH() {
        return (String) super.get("DISK_PATH");
    }

    public boolean isNew(){
        return getDISK_PATH()==null;
    }
    public void setDISK_PATH(String DISK_PATH) {
        put("DISK_PATH",DISK_PATH);
    }

    public String getDISK_NM() {
        return (String) super.get("DISK_NM");
    }

    public void setDISK_NM(String DISK_NM) {
        put("DISK_NM",DISK_NM);
    }

    public String getDS_NM() {
        return (String) super.get("DS_NM");
    }

    //  public cloud disk type, vmware datastore name
    public void setDS_NM(String DS_NM) {
        put("DS_NM",DS_NM);
    }

    //  public cloud disk type, vmware datastore name  for create or update
    public String[] getDATASTORE_NM_LIST() {
        return (String[])super.get(VMWareAPI.PARAM_DATASTORE_NM_LIST);
    }
    public void setDATASTORE_NM_LIST(String... list) {
        super.put(VMWareAPI.PARAM_DATASTORE_NM_LIST, list);
    }


    /**
     *  disk id in clovirsm
     * @return
     */
    public String getDS_ID() {
        return (String) super.get("DS_ID");
    }

    //  public cloud disk type, vmware datastore name
    public void setDS_ID(String DS_ID) {
        put("DS_ID",DS_ID);
    }

}
