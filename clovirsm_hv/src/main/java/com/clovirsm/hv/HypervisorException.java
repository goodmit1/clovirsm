package com.clovirsm.hv;

public class HypervisorException extends Exception{
 
	public static final String  PROCESS_FAIL="err.PROCESS_FAIL";
	public static final String  LACK_OF_SIZE="err.LACK_OF_SIZE";
	public static final String DS_LACK_OF_SIZE = "err.DS_LACK_OF_SIZE";
	String[] args ;
	public HypervisorException(String... args)
	{
		super(args[0]);
		this.args = args;

	}
	public String[] getMsgParams()
	{
		return this.args;
	}
}
