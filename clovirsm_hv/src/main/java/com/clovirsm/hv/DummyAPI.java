package com.clovirsm.hv;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;

import java.util.List;
import java.util.Map;

public class DummyAPI implements HypervisorAPI{

	@Override
	public Map deleteDisk(HVParam param) throws Exception {
		return null;
	}

	@Override
	public void onFinishCreateVM(HVParam param) throws Exception {

	}

	@Override
	public Map renameSnapshot(HVParam param, String oldName, String newName) throws Exception {
		return null;
	}

	@Override
	public Map revertVM(HVParam param, String snapshot_nm) throws Exception {
		return null;
	}

	@Override
	public Map mountDisk(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map createVM(HVParam param, boolean isRedeploy) throws Exception {
		return null;
	}

	@Override
	public Map moveVMFolder(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map reconfigVM(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map powerOpVM(HVParam param, String op) throws Exception {
		return null;
	}

	@Override
	public Map deleteSnapshot(HVParam param, String snapshot_nm) throws Exception {
		return null;
	}

	@Override
	public void deleteImage(HVParam param) throws Exception {

	}

	@Override
	public Map deleteVM(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map openConsole(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map createSnapshot(HVParam param, String snapshotNm) throws Exception {
		return null;
	}

	@Override
	public void renameVM(HVParam param, String newName) throws Exception {

	}

	@Override
	public void renameImg(HVParam param, String newName) throws Exception {

	}

	@Override
	public Map unmount(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMPerf(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getPerf(String type, HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map addNic(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map deleteNic(HVParam param, int nicId) throws Exception {
		return null;
	}

	@Override
	public int getFirstNicId() throws Exception {
		return 0;
	}

	@Override
	public Map listPerfHistory(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map listEvent(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map listAlarm(HVParam param) throws Exception {
		return null;
	}

	@Override
	public List listObject(HVParam param) throws Exception {
		return null;
	}

	@Override
	public String getHVObjType(String standardType) {
		return null;
	}

	@Override
	public String getStandardObjType(String objType) {
		return null;
	}

	@Override
	public VMInfo chkTemplatePath(HVParam param, String tmplPath) throws Exception {
		return null;
	}

	@Override
	public Map getDSAttribute(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getHostAttribute(HVParam param) throws Exception {
		return null;
	}

	@Override
	public String[] getPerfHistoryTypes(String objType) throws Exception {
		return new String[0];
	}

	@Override
	public List<Map> listVMDisk(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map addDisk(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map createImage(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Float getSnapshotSize(HVParam param, String snapshotName) throws Exception {
		return null;
	}

	@Override
	public VMInfo vmState(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMPerfListInDC(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMListInDC(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map listAllAlarm(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map guestRun(HVParam param, String cmd, String cmdParam) throws Exception {
		return null;
	}

	@Override
	public void guestUpload(HVParam param, String svrPath, String localPath) throws Exception {

	}

	@Override
	public void guestDownload(HVParam param, String svrPath, String localPath) throws Exception {

	}

	@Override
	public Map listLicense(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMIpListInDC(HVParam param) throws Exception {
		return null;
	}

	@Override
	public void connectTest(Map param) throws Exception {

	}

	@Override
	public boolean chkTask(HVParam param, String taskId) throws Exception {
		return false;
	}

	@Override
	public Map listAllVMMaxPerf(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMTags(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map createFw(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map deleteFwSourceIp(HVParam param) throws Exception {
		return null;
	}
}
