package com.clovirsm.hv;

public interface IAsyncAfter extends IAfterProcess{
    void setDone(boolean b);

    void setError(Throwable errMsg);
}
