package com.clovirsm.hv;

import com.clovirsm.hv.obj.HVParam;

import java.util.HashMap;
import java.util.Map;

public abstract class PublicCloudAPI extends CommonAPI{
    protected Map doProcess(HVParam param, Class... classList) throws Exception {
        IConnection connectionMgr = null;

        try {
            connectionMgr = this.connect(param.getConnParam());
            Map result = new HashMap();


            for(int i = 0; i < classList.length; i++) {
                Class cls = classList[i];
                PublicHVAction action = (PublicHVAction)cls.getConstructor(IConnection.class).newInstance(connectionMgr);
                action.run(param, result );
            }


            return result;
        } finally {
           disconnect(connectionMgr);


        }
    }


}
