package com.clovirsm.hv;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.util.SubnetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public abstract class PublicHVAction  {
    private final IConnection conn;
    public static final String OBJ_TYPE_REGIONS="Regions";
    public static final String OBJ_TYPE_KEYPAIR="Keypair";
    public static final String SEARCH_KEYPAIR="SearchKeypair";
    public static final String OBJ_TYPE_KEYPAIR_SECRET="PRIVATE_KEY";
    public static final String OBJ_TYPE_SECURITY_GROUP="VM_NM";
    public static final String OBJ_TYPE_INSTANCE_TYPE="instanceType";
    public static final String OBJ_TYPE_DISK_TYPE="Datastore";

    public static final String OBJ_TYPE_SECURITY_GROUP_IPS="ips";
    public static final String OBJ_TYPE_SECURITY_GROUP_PORTS="ports";
    public static final String OBJ_TYPE_TEMPLATE="Template";
    public static final String MSG_INSTANCE_CREATE_ERROR = "서버를 생성 중 오류가 발생하였습니다.";
    public static final String MSG_INSTANCE_SEARCH_ERROR = "서버를 찾을 수 없습니다.";
    public static final String MSG_INSTANCE_BOOT_ERROR = "VM 실행중 오류가 발생하였습니다.";
    public static final String MSG_INSTANCE_STOP_ERROR = "VM 중지중 오류가 발생하였습니다.";
    public static final String MSG_INSTANCE_REBOOT_ERROR = "VM 재실행중 오류가 발생하였습니다.";
    public static final String MSG_INSTANCE_RESOURCE_ERROR = "VM 자원 변경 중 오류가 발생하였습니다.";
    public static final String MSG_NETWORK_FIND_ERROR = "Network를 찾을 수 없습니다.";
    public PublicHVAction(IConnection m) {
        this.conn = m;

    }
    public IConnection getConnect(){
        return conn;
    }
    public void run(HVParam param, Map result ) throws Exception
    {
        String taskId = this.run1(param, result);
        result.put(HypervisorAPI.PARAM_TASK_ID, taskId);
        if(param.getAfter()  != null && taskId != null)
        {
            param.getAfter().startTask(taskId);
        }
    }

    protected  abstract  String getConnectionClassName();
    public IConnection connect(Map param) throws Exception
    {
        IConnection connection  = (IConnection)ConnectionPool.getInstance().getConnection(getConnectionClassName(), (String)param.get(HypervisorAPI.PARAM_URL), (String)param.get(HypervisorAPI.PARAM_USERID), (String)param.get(HypervisorAPI.PARAM_PWD), param  );

        return connection ;
    }

    public static String getWinStartScript(VMInfo vmInfo){
        String script = "/Users/user-profile.ps1 ${ADMIN_ID} ${ADMIN_PWD} \"${ADMIN_SSH_KEY}\"";
        return CommonUtil.fillContentByVar(script, vmInfo);
    }
    public abstract String run1(HVParam param, Map result) throws Exception;

    protected void chkVMSetting(VMInfo vmInfo) throws Exception {
        if(vmInfo.getVM_NM() == null) {
            throw new Exception("VM Name is null");
        }
        if(vmInfo.getSPEC_TYPE()== null) {
            throw new Exception("Spec is null");
        }
        if(vmInfo.getFROM() == null) {
            throw new Exception("Image is null");
        }
        if(vmInfo.getDATASTORE_NM_LIST()==null || vmInfo.getDATASTORE_NM_LIST().length==0){
            throw new Exception("Disk Type not mapping");
        }

    }

    protected Map<String, String> getInfoFromUrl(String url ){
        //  from = "/subscriptions/dadd60d0-9dfa-40cd-af23-8d658046ec7e/resourceGroups/test1/providers/Microsoft.Compute/galleries/gallery/images/windows/versions/0.0.1";
        String[] arr = url.split("/");
        Map<String, String> result = new HashMap<>();
        for(int idx = 1; idx < arr.length; idx++){
            result.put(arr[idx++].toLowerCase(), arr[idx]);
        }
        return result;
    }
    public static String getCIDR(String ip) {
        ip = ip.trim();
        if("".equals(ip)){
            return null;
        }
        if(ip.indexOf("/")>0){
            return ip;
        }
        else{
            return ip+"/32";
        }
    }
    protected boolean isInRange(String ipRange,  String ip){
        if(ipRange.equals(ip)) {
            return true;
        }
        else if(ipRange.indexOf("/")<0){
            return false;
        }
        SubnetUtils utils = new SubnetUtils(ipRange);
        utils.setInclusiveHostCount(true);
        return utils.getInfo().isInRange(ip);
    }
    public Logger logger = LoggerFactory.getLogger(this.getClass());
}
