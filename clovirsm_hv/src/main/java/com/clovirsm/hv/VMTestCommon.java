package com.clovirsm.hv;

import com.clovirsm.hv.obj.FWInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public abstract class VMTestCommon {
    protected Map connInfo = new HashMap();
    protected HypervisorAPI api;
    protected String vmName="clovir-linux";
    protected String from = "/subscriptions/dadd60d0-9dfa-40cd-af23-8d658046ec7e/resourceGroups/test1/providers/Microsoft.Compute/galleries/gallery/images/linux-image/versions/1.0.0";
    protected boolean isLinux = true;
    protected String adminPrivateKey="-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEAyKYTlWOuUqGFZtzB1IkwUj4zn5RV7AlErKXMurGEDprOjA0S\n" +
            "S8Usk4VersyKt+5pVSmiCevskPUgjphHhnvYgNgX8ZcGECc9t+l3/c1hryXsv/rV\n" +
            "El2b8BJmnDA3xXSaT/n9thegf3Ag+liAiyS3+CkpmAJsBV+4TNlfYS7znOuk/CRt\n" +
            "lesYstnn8Od4XE2DPGfx2SNr1+c3AvqaSc+tiIaK1ZDtBEgaSLNk8n8DCQrtC3CC\n" +
            "xL3WEbybfI66BqXuDS5Xu3jSzFDbPNgMHAF2kkA7NjFlhiZKyueV9Ma9gIrJQNMn\n" +
            "OCw9mTUACbxa+yu7dPnOxjcyytnabMqd/ksYeQIDAQABAoIBAEAqTAz0CaVHHgAd\n" +
            "DKdJD/jdB2k66JdJB3smsmfpqM961h6Xd4hfj7fFaNgBdUMIrQqc+8KsfcSFf1na\n" +
            "fNUW+c+KS23o2/mCcwxDTyoM6fY27JUgQ89RvDJzz/iWut8cQNTenzICMENQulsU\n" +
            "vlbA8Uwc7b482AqH58o7oXLUrvc5GqdMvjCCz8coXr7qxK5pndo6FqxrBYe2L8Yc\n" +
            "M1DTBXTkatLXbKdOaYGJSUN/kReLg++I550h36xJlFOBcQeSsnaQ2/CmAe5O1yON\n" +
            "hRJAAyMllzif6lnLQTICL7KqkeVvbUdTnC59YnrpkL+4g6cplNRnyGu4HDq/oOLX\n" +
            "oZGL4ZkCgYEA+lToPFrG1+/mW6KgIvtwMsNdOwOVrT+pKcd2jqR7OfsDFgGK9vuZ\n" +
            "chXH36EPyhQI+FJqkY8zIG29gZYdGDZFoW4t5jOCUQgvC4FrACWPbv5fqbr09QuN\n" +
            "P88WgLX9AuIua+UprNKZ/k2gYA0zN3XUYu5rA+KEyoTEAoSkl/2giB8CgYEAzTEs\n" +
            "V/Yde329aaxKXiJekboM4KCeNflnOOKK8RP+KW8NEq2kGfYUAaMXjtYNw8cxvzup\n" +
            "4Gzn8sMUkUaBNjZyf8L+wGvFhJ7eAET1L7sRWAkIoPFdYh4ixB8smcQ+qukAjHlV\n" +
            "WQyBZzv+aQDxKyWcMGl6lDi7CBuHw5mTcIbRLGcCgYEAsMC24bau49Vt75HaRBkJ\n" +
            "Va5/66VST+u9Q9Skr38PyxajTSx1tIJeDCG46PvOLD8NPljjCjf0P5d80Tzu+iOH\n" +
            "8r3bBsmj4BdJMHSG2qmqpxJQ9YUeZM9tQnq0Rtk49opBgMlkLbTeKqMg8/EeErDN\n" +
            "77SsNyf/3wbzZ1vE49SfPr0CgYAoz839FNOaW1EQMpd8D6P1oiariubM33U7aDuw\n" +
            "ZYSY4yCbhXPtQfeKt9CYtVR5iyYG3iwUmjSsIUb2+in7CWtOpA8dYJIhpaOMNCr8\n" +
            "Ol3hv7I6qKclO884aObrEpBQWBWm2v8xvf4C9XjNk8VkCo/9RQ1/a6UvoKIpMmOT\n" +
            "7wytowKBgF6iGFlz8ghQZdkOwR/Qm8IF/+eXhBd/UokXpKstx6ueYLZZX3KiygF5\n" +
            "aJLa5TwEwfCahmwemMXr8mNXDLiruFFeYmIkBD37AhgksqMfDej+krqC0XQ3Dhu0\n" +
            "uGYux/+wQmSpKGZb8f1Bj92sP00gkV+N19oE2z9crXas5G8KCy5t\n" +
            "-----END RSA PRIVATE KEY-----";
    protected int sshPort = 22;


    protected int dataDiskSize = 10;
    protected int osDiskSize = 30;
    protected String adminId = "clovirsm";
    protected String adminPublicKey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIphOVY65SoYVm3MHUiTBSPjOflFXsCUSspcy6sYQOms6MDRJLxSyThV6uzIq37mlVKaIJ6+yQ9SCOmEeGe9iA2BfxlwYQJz236Xf9zWGvJey/+tUSXZvwEmacMDfFdJpP+f22F6B/cCD6WICLJLf4KSmYAmwFX7hM2V9hLvOc66T8JG2V6xiy2efw53hcTYM8Z/HZI2vX5zcC+ppJz62IhorVkO0ESBpIs2TyfwMJCu0LcILEvdYRvJt8jroGpe4NLle7eNLMUNs82AwcAXaSQDs2MWWGJkrK55X0xr2AislA0yc4LD2ZNQAJvFr7K7t0+c7GNzLK2dpsyp3+Sxh5";
    protected String addUserShellFile = "UserAdd.sh";
    protected String adminPwd = "QWER43212!@#$";
    protected String specType = "Standard_A2_v2";
    protected String new_specType = "Standard_A1_v2";
    protected String diskType = "Standard_LRS";
    protected String defaultIp = "221.147.80.129";
    protected int cpu = 2;
    protected int mem = 4;
    protected int new_cpu = 1;
    protected int new_mem = 2;
    protected String rscGroup = "syk-test";
    protected String newUserId = "test10";
    //String rscGroup;
    public abstract  void login() throws Exception;

    @Order(1)
    @Test
    public void createVM() throws Exception {

        createVM(false);

    }
    protected void createVM(boolean recreate) throws Exception {
        VMInfo param = new VMInfo( );
        param.setVM_NM(vmName);
        param.setDEFAULT_IP(defaultIp);
        List<DiskInfo> diskInfo = new ArrayList();
        diskInfo.add(new DiskInfo(diskType, dataDiskSize));
        param.setDISK_LIST(diskInfo);
        param.setSPEC_TYPE(specType);
        param.setFROM(  from );
        param.setOS_ADMIN_PWD(adminPwd);
        param.setOS_ADMIN_SSH_KEY(adminPublicKey);
        param.setOS_ADMIN_ID(adminId);
        param.setRSC_GROUP_NM(rscGroup);
        param.setDATASTORE_NM_LIST(new String[]{diskType});
        Map result = api.createVM( new HVParam(  connInfo, param), recreate);
        String taskId = (String)result.get(HypervisorAPI.PARAM_TASK_ID);
        assertNotNull(taskId);
        System.out.println(taskId);
        blockUntilComplete(taskId);
    }
    @Order(2)
    @Test
    public void onFinishCreateVM() throws Exception {
        // VM생성 이후 Azure windows는 portal용 ssh 아이디 생성함.
        // gcp는 clovirsm이 22번 포트로 접속하기 위해 방화벽 추가
        VMInfo param = new VMInfo( );
        param.setVM_NM(vmName);
        param.setRSC_GROUP_NM(rscGroup);
        param.setDEFAULT_IP(defaultIp);
        param.setOS_ADMIN_PWD(adminPwd);
        param.setOS_ADMIN_SSH_KEY(adminPublicKey);
        param.setOS_ADMIN_ID(adminId);
        api.onFinishCreateVM( new HVParam(  connInfo, param ));

    }
    @Order(3)
    @Test
    public void createVMExistError() throws Exception {

           assertThrows(AlreadyExistException.class, ()->  createVM(false));

    }
    
    //재배포
    @Order(3)
    @Test
    public void reCreateVM() throws Exception {

        assertDoesNotThrow(  ()->  createVM(true));

    }
    @Order(10)
    @Test
    public void getVMInfo() throws Exception{

        VMInfo result = this.getVMInfo(vmName);
        System.out.println(result);
        assertNotNull(result.getVM_HV_ID());
        assertEquals(result.getRUN_CD(), "R", "RUN_CD");

        assertNotEquals(isLinux, result.isWindows());
        assertEquals(result.getCPU(), cpu, "CPU_CNT");
        assertEquals(result.getRAM_SIZE(), mem, "RAM_SIZE");
        assertEquals( result.getDISK_SIZE(), osDiskSize+dataDiskSize, "DISK_SIZE");
        assertEquals((result.getIP_LIST()).size(), 1);
        assertEquals(result.getSPEC_TYPE(), specType);
        assertEquals(( result.getDISK_LIST()).size(), 2);
        assertEquals(result.getDISK_LIST().get(1).get("DISK_SIZE"), dataDiskSize);
    }

    protected VMInfo getVMInfo(String vmName) throws Exception {
        VMInfo vmInfo = new VMInfo( );
        vmInfo.setVM_NM( vmName);
        return api.vmState( new HVParam(  connInfo , vmInfo));

    }
    @Order(20)
    @Test
    public void sshConnection() throws Exception{
        addOsUser(vmName, newUserId, "Qwer43211YQam", adminPublicKey);
    }
    protected void addOsUser(String vmName, String id, String pwd, String sshKey) throws Exception{

        String basePath = this.getClass().getResource("/").toString()  + "../../../clovirsm_jsp/src/main/resources/shell/" + addUserShellFile;

        InputStream in =new URL(basePath).openStream();


        String cmd = CommonUtil.readStream2String(in, "utf-8");
        in.close();
        Map param = new HashMap();
        param.put("USER_NAME", id);
        param.put("PUBLIC_KEY",  sshKey);
        param.put("USER_PASSWORD", pwd);
        cmd = CommonUtil.fillContentByVar(cmd, param);
        VMInfo result = this.getVMInfo(vmName);
        String host =  result.getPUBLIC_IP();

        PublicKeySshSession sshSession = new PublicKeySshSession.Builder(host, adminId, sshPort, adminPrivateKey.getBytes(StandardCharsets.UTF_8)).build();
        sshSession.execute(cmd, false);
        sshSession.disconnect();
        chkSsh( newUserId );

    }
    protected void chkSsh( String id ) throws Exception {
        VMInfo vmInfo = this.getVMInfo(vmName);

        String host = vmInfo.getPUBLIC_IP();
        if(isLinux){
            chkCreateUserLinux(host, id);
        }
        else{
            chkCreateUserWin(host, id);
        }
    }
    protected void chkCreateUserLinux(String host, String id) throws Exception {
        PublicKeySshSession sshSession = new PublicKeySshSession.Builder(host, id, sshPort, adminPrivateKey.getBytes(StandardCharsets.UTF_8)).build();
        sshSession.execute("pwd", false);
        sshSession.disconnect();
    }
    protected void chkCreateUserWin(String host, String id) throws Exception {
        PublicKeySshSession sshSession = new PublicKeySshSession.Builder(host, adminId, sshPort, adminPrivateKey.getBytes(StandardCharsets.UTF_8)).build();
        String userInfo = sshSession.execute("net user " + id, false);
        System.out.println("UserInfo" + userInfo);
        // remote desktop 에 계정이 있는 지 확인
        sshSession.disconnect();
        Assertions.assertTrue(userInfo.indexOf(id)>0);
    }
    @Order(30)
    @Test
    public void powerOff() throws Exception {
        VMInfo vmInfo = new VMInfo(vmName);
        vmInfo.setRSC_GROUP_NM(rscGroup);

        api.powerOpVM( new HVParam(  connInfo , vmInfo), HypervisorAPI.POWER_OFF);
        Thread.sleep(5000);
        VMInfo result = api.vmState( new HVParam(  connInfo,  vmInfo));
        assertEquals("S", result.getRUN_CD());
    }
    @Order(31)
    @Test
    public void powerOn() throws Exception {
        VMInfo vmInfo = new VMInfo(vmName);
        vmInfo.setRSC_GROUP_NM(rscGroup);
        api.powerOpVM( new HVParam(  connInfo,  vmInfo), HypervisorAPI.POWER_ON);

        while(true){
            Thread.sleep(10000);
            vmInfo = this.getVMInfo(vmName);
            if(vmInfo.getRUN_CD().equals("R")){
                break;
            }
        }

    }
    @Order(32)
    @Test
    public void chkTemplate() throws Exception {
        VMInfo template = api.chkTemplatePath( new HVParam(  connInfo ), from);
        //assertEquals(osDiskSize, template.getDISK_SIZE());
        assertNotEquals(isLinux, template.isWindows());

    }
    @Order(40)
    @Test
    public void removeFwSourceIp() throws Exception {
        HVParam param = new HVParam(connInfo);
        VMInfo vmInfo = new VMInfo(vmName);
        vmInfo.setRSC_GROUP_NM(rscGroup);
        param.setVmInfo(vmInfo);
        param.setFwInfo(new FWInfo(FWInfo.PROTOCOL_TCP,new String[]{defaultIp}, new String[]{"" + sshPort}));
        api.deleteFwSourceIp(param);
        Thread.sleep(10000);
        assertThrows(Exception.class, ()->  this.chkSsh(adminId) );


    }
    @Order(41)
    @Test
    public void createFW() throws Exception {
        createFW(defaultIp, new String[]{"" + sshPort, "8080"});
        Thread.sleep(10000);
        this.chkSsh(adminId);
        createFW(defaultIp, new String[]{"" + sshPort, "8080"});
        Thread.sleep(3000);
        createFW("10.11.2.1", new String[]{"" + sshPort });
    }
    protected  void createFW(String ip, String[] ports) throws Exception {
        HVParam param = new HVParam(connInfo);
        VMInfo vmInfo = new VMInfo(vmName);
        vmInfo.setRSC_GROUP_NM(rscGroup);
        param.setVmInfo(vmInfo);
        param.setFwInfo(new FWInfo(FWInfo.PROTOCOL_TCP,new String[]{ip}, ports));

        api.createFw(param);
    }
    @Order(50)
    @Test
    public void reconfigVM() throws Exception {
        ReconfigAfter after = new ReconfigAfter();
        VMInfo vmOldInfo = this.getVMInfo(vmName);

        List<DiskInfo> diskList = vmOldInfo.getDISK_LIST();
        diskList.get(1).setDISK_SIZE(dataDiskSize*2);
        diskList.add( new DiskInfo(diskType, dataDiskSize));




        vmOldInfo.setSPEC_TYPE(new_specType);


        Map result = api.reconfigVM( new HVParam(  connInfo,  vmOldInfo, null, after));
        String taskId = (String)result.get(HypervisorAPI.PARAM_TASK_ID);
        while(true){
            Thread.sleep(500);
            if(after.isDone()){
                break;
            }
        }
        assertNull(after.errMsg);
        VMInfo newVMInfo = this.getVMInfo(vmName);
        assertEquals(osDiskSize + dataDiskSize*3, newVMInfo.getDISK_SIZE());
        assertEquals(3, newVMInfo.getDISK_LIST().size());
        assertEquals(new_cpu, newVMInfo.getCPU());
        assertEquals(new_mem, newVMInfo.getRAM_SIZE());
    }

    @Order(99)
    @Test
    public void deleteVM() throws Exception {
        VMInfo vmInfo = new VMInfo(   );
        vmInfo.setRSC_GROUP_NM(rscGroup);
        vmInfo.setVM_NM( vmName);
        api.deleteVM( new HVParam(  connInfo,  vmInfo));
        //TODO Azure는 VM삭제 후 다른 것들이 삭제 되어야 하는데 테스트 경우 충분히 waiting을 하지 않아서 일부 덜 삭제 될 수 있음.

        try {
            while (true) {
                Thread.sleep(60000);
                getVMInfo(vmName);

            }
        }
        catch(NotFoundException e)
        {

        }

    }

    protected boolean chkTask(String task) throws Exception {

        return api.chkTask( new HVParam(  connInfo ), task);


    }

    public   void blockUntilComplete(String task) throws Exception {
        while(true){
            boolean isFinish = chkTask(task);
            if(isFinish){
                break;
            }
            Thread.sleep(10000);
        }
    }

    class ReconfigAfter implements IAsyncAfter  {
        boolean isDone = false;
        Throwable errMsg = null;
        String taskId ;

        public boolean isDone(){
            return isDone;
        }

        @Override
        public void setDone(boolean b) {
            if(b){
                try {
                    onAfterSuccess(taskId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            isDone = b;

        }

        @Override
        public void setError(Throwable errMsg) {
            this.errMsg = errMsg;
            try {
                onAfterFail(taskId, errMsg);
            } catch (Exception e) {
                e.printStackTrace();
            }
            isDone = true;

        }

        @Override
        public void onAfterSuccess(String s) throws Exception {
            System.out.println("success");
        }

        @Override
        public Map getParam() {
            return null;
        }

        @Override
        public void chkState()  {

        }

        @Override
        public void setTask(String s) {
            this.taskId = s;
            System.out.println("set Task " +  s);
        }

        @Override
        public void startTask(String s) {
            System.out.println("start Task " +  s);
        }

        @Override
        public void onAfterFail(String s, Throwable throwable) throws Exception {
            System.out.println("fail " + s);
            if(throwable != null) System.out.println(throwable.getMessage());
        }
    }
}
