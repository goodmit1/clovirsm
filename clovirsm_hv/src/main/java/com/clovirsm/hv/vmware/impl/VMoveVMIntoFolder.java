package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Arrays;
import java.util.Map;

public class VMoveVMIntoFolder extends com.clovirsm.hv.vmware.CommonAction {

	public VMoveVMIntoFolder(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception {
		ManagedObjectReference vm = super.getVM(param.getVmInfo());
		ManagedObjectReference folder = null;
		try {
			folder = super.findObjectFromRoot(VMWareAPI.OBJ_TYPE_FOLDER,  param.getVmInfo().getFOLDER_NM());
		}
		catch(NotFoundException e) {
			VCreateFolder createFolder = new VCreateFolder(this.connectionMng);
			folder = createFolder.makeFolder(super.getDataCenter(dc),  param.getVmInfo().getFOLDER_NM());
		}
		if(folder != null && vm != null) {
			  ManagedObjectReference taskmor =
		               vimPort.moveIntoFolderTask(folder, Arrays.asList(vm));
		}
		return null;
	}

}
