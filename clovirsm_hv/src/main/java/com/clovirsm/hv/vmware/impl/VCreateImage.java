package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VwareVMInfo;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;
/**
 * 이미지 추가
 * @author 윤경
 *
 */
public class VCreateImage extends VCreateVM{

	public VCreateImage(ConnectionMng m) {
		super(m);

	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam paramMap, Map result)
			throws Exception {
		this.param = new VwareVMInfo( paramMap.getVmInfo());
		if(param.get(HypervisorAPI.PARAM_IMAGE_NM) != null){
			param.setVM_NM((String)param.get(HypervisorAPI.PARAM_IMAGE_NM));
		}
		if(param.get(VMWareAPI.PARAM_IMG_DATASTORE_NM) != null){
			param.setDATASTORE_NM_LIST((String[])param.get(VMWareAPI.PARAM_IMG_DATASTORE_NM));
		}


		return this.createVM(true, result, param);

	}
}
