package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Map;

public class VGuestFileUpload extends com.clovirsm.hv.vmware.CommonAction{
	protected NamePasswordAuthentication auth = new NamePasswordAuthentication();
	public VGuestFileUpload(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception {
		return null;
	}


	public Map run(VMInfo vmInfo, String toPath, String fromPath)  throws Exception {
		ManagedObjectReference vm = super.getVM(vmInfo.getVM_NM());
	 
	 
		auth.setUsername(vmInfo.getOS_ADMIN_ID());
		auth.setPassword(vmInfo.getOS_ADMIN_PWD());

		long permission = 0640L;
		try
		{
			permission = NumberUtil.getLong(vmInfo.get("FILE_PERMISSION"));
		}
		catch(Exception ignore)
		{
			
		}
		uploadFile(   vm, fromPath, toPath, permission);
		return null; 
	}
	public void uploadFile(ManagedObjectReference vmRef, String filePathLocal, String filePathInGuest, long permission) throws Exception
	  {
	    File file = new File(filePathLocal);

	    if (file.isDirectory())
	    {
	      throw new IllegalArgumentException("Local file path points to a directory: " + filePathLocal);
	    }

	    long fileSize = file.length();

	    FileInputStream in = new FileInputStream(filePathLocal);
	    uploadFromStream(vmRef, in, fileSize, filePathInGuest, file.lastModified(), true, permission);
	    in.close();
	  }

	  public void uploadFromStream(ManagedObjectReference vmRef,    InputStream in, long size, String filePathInGuest, long modifyTime, boolean overwrite, long permission) throws  Exception 
	  {
	    GuestFileAttributes guestFileAttr = null;

	    if(isWindow(vmRef))
	    {
	      GuestWindowsFileAttributes winFileAttr = new GuestWindowsFileAttributes();
	      guestFileAttr = winFileAttr;
	      if(filePathInGuest.indexOf(":")<0) filePathInGuest = "c:" + filePathInGuest;
	      filePathInGuest = filePathInGuest.replaceAll("/", "\\\\");
			
	    }
	    else
	    {
	      GuestPosixFileAttributes posixFileAttributes = new GuestPosixFileAttributes();
	      
	      posixFileAttributes.setPermissions(permission);
	      guestFileAttr = posixFileAttributes;
	    }

	    guestFileAttr.setAccessTime(toXmlCal(new Date()));
 
	    guestFileAttr.setModificationTime(toXmlCal(new Date(modifyTime)));
	    ManagedObjectReference mor = (ManagedObjectReference)super.getProp(super.connectionMng.getServiceContent().getGuestOperationsManager(), "fileManager");
		
	    String upUrlStr = super.connectionMng.getVimPort().initiateFileTransferToGuest(mor, vmRef,auth, filePathInGuest, guestFileAttr, size, overwrite);

	    uploadData(upUrlStr, in, size, filePathInGuest);
	  }
	 
	  private void uploadData(String urlString, InputStream in, long size, String filePath) throws IOException
	  {
	    HttpURLConnection conn = (HttpURLConnection)new URL(urlString).openConnection();
	    conn.setDoInput(true);
	    conn.setDoOutput(true);
		conn.setConnectTimeout(10000);
	    conn.setRequestProperty("Content-Type", "application/octet-stream");
	    conn.setRequestMethod("PUT");
	    conn.setRequestProperty("Content-Length", Long.toString(size));
	    
	    OutputStream out = conn.getOutputStream();
	    CommonUtil.readStream2Stream(in, out);
	    out.close();

	    if (200 != conn.getResponseCode())
	    {
	    	InputStream stream = conn.getErrorStream();
	    	
	    	ByteArrayOutputStream out1 = new ByteArrayOutputStream();
	    	CommonUtil.readStream2Stream(stream, out1);
			 
	    	throw new IOException("File upload is not successful"  +new String(out1.toByteArray()) );
	    }
	    conn.disconnect();
	  }


}
