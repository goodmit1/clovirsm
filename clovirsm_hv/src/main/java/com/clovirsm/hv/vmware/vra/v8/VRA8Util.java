package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.HVProperty;

public class VRA8Util {
	public static Map<String, String> getFieldNameMap(String kubun){
		Map result = new HashMap();
		try {
			String t;
			t = HVProperty.getInstance().getProperty("vra.include.param."+kubun);
			if(t != null) {
				String[] arr = t.split(",");
				for(String a:arr) {
					String[] arr1 = a.split("=");
					result.put(arr1[0], arr1[1]);
				}
				 
			}
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
		return result;
	}
	public static Map<String, List<String>> getSWInfo(JSONObject json, JSONObject dataJson) throws Exception {
		JSONObject resources = json.getJSONObject("resources");
		Map result = new HashMap();
		Set<String> keys= resources.keySet();
		String delim = HVProperty.getInstance().getProperty("vra_app_delim_str", ",") ;
		List<String> sw = new ArrayList();
		for(String key:keys) {
			JSONObject resource = resources.getJSONObject(key);
			if("Cloud.Ansible".equals(resource.getString("type"))){
				JSONObject properties = resource.getJSONObject("properties");
				String host = getResourceName(properties.getString("host"));
				JSONArray provision =  getJSONArray(properties.getJSONObject("playbooks"),"provision");
				
				for(int i=0; i < provision.length(); i++) {
					List sw1= getAppFromAnsible(delim, provision.getString(i), dataJson);
					if(sw1 != null) sw.addAll(sw1);
				}
				result.put(host,  sw);
			}
			else if(((String)resource.get("type")).matches("Cloud.*.Machine")) {
				JSONObject properties = resource.getJSONObject("properties");
				if(properties.has("application")) {
					try { 
						List sw1= getApp(  properties.getString("application"), delim, dataJson);
						if(sw1 != null) sw.addAll(sw1);
					}
					catch(Exception ignore) {
						System.out.println(ignore.getMessage());
					}
					 
				}
				result.put(key,  sw);
			}
		}
		return result;
	}
	public static Map getSWInfo4Deploy(JSONObject json, JSONObject dataJson) throws Exception {
		JSONArray resources = json.getJSONArray("resources");
		Map result = new HashMap();
		//String delim = HVProperty.getInstance().getProperty("vra_app_delim_str", "?tasks") ;
		String delim = HVProperty.getInstance().getProperty("vra_app_delim_str", ",") ;
		for(int j=0; j < resources.length(); j++) {
			List<String> sw = new ArrayList();
			JSONObject resource = resources.getJSONObject(j);
			if("Cloud.Ansible".equals(resource.getString("type"))){
				JSONObject properties = resource.getJSONObject("properties");
				String host =  properties.getJSONObject("host").getString("resourceName");
				JSONArray provision =  getJSONArray(properties.getJSONObject("playbooks"),"provision");
				
				for(int i=0; i < provision.length(); i++) {
					List sw1= getAppFromAnsible(delim, provision.getString(i), dataJson);
					if(sw1 != null) sw.addAll(sw1);
				}
				result.put(host,  sw);
			}
			else if(((String)resource.get("type")).matches("Cloud.*.Machine")) {
				JSONObject properties = resource.getJSONObject("properties");
				if(properties.has("application")) {
					 
					List sw1= getApp(  properties.getString("application"), delim, dataJson);
					if(sw1 != null) sw.addAll(sw1);
					 
				}
				result.put(properties.getString("resourceName"),  sw);
			}
		}
		return result;
	}
	static JSONArray getJSONArray(JSONObject json, String key) {
		Object o = json.get(key);
		if(o instanceof JSONArray) {
			return (JSONArray)o;
		}
		else {
			JSONArray arr = new JSONArray();
			arr.put(o);
			return arr;
		}
	}
	public static String getFileNameOnly(String str) {
		int pos = str.lastIndexOf("/");
		int pos1 = str.lastIndexOf(".");
		if(pos<0 && pos1<0) {
			return str;
		}
		if(pos1<0) {
			pos1 = str.length();
		}
		return str.substring(pos+1, pos1);
	}
	public static List getAppFromAnsible(String startStr, String str, JSONObject dataJson) throws Exception {
		
		int pos = str.lastIndexOf(startStr);
		 
		if(pos<0) return null;
		str = str.substring(pos);
		if(str.indexOf("${")>0 && dataJson != null) {
			YamlCalc calc = new YamlCalc(dataJson);
			str = (String) calc.cal(str);
		}
		String[] arr = str.split("/");
		List result = new ArrayList();
		for(String arr1 : arr) {
			int pos1 = arr1.indexOf(".yaml");
			if(pos1>0) {
				result.add(arr1.substring(0, pos1));
			}
		}
		return result;
		
		 
	}
public static List getApp(  Object str1, String delim, JSONObject dataJson) throws Exception {
	List result = new ArrayList();
	if(str1 instanceof String) {
		
		String str = (String)str1;
		if(str.indexOf("${")>=0 && dataJson != null) {
			YamlCalc calc = new YamlCalc(dataJson);
			str = (String) calc.cal(str);
		}
		String[] arr = str.split(delim);
		
		for(String arr1 : arr) {
			 
				result.add( arr1  );
			 
		}
		
	}
	else if(str1 instanceof JSONArray) {
		JSONArray str = (JSONArray)str1;
		for(int i=0; i < str.length(); i++) {
			result.add( str.getString(i)  );
		}
	}
	return result;	 
}
public static String getResourceName(String host) {
		if(host.indexOf("{resource.")>0) {
			String str =  getInnerStr(host,"{resource.", ".");
			if(str.endsWith("]")){
				int pos = str.indexOf("[");
				return str.substring(0, pos);
			}
			else {
				return str;
			}
		}
		if(host.indexOf("{resource[\"")>0) {
			return  getInnerStr(host,"\"", "\"");
		}
		return null;
	}
	public static String getInnerStr(String val, String start, String end) {
		int pos = val.indexOf(start);
		if(pos>=0) {
			int pos1 = val.indexOf( end, pos+start.length());
			if(pos1>0) {
				return val.substring(pos +  start.length() , pos1);
			}
		}
		return null;
	}
}
