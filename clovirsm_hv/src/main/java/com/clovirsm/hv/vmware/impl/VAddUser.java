package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.security.credstore.CredentialStore;
import com.vmware.security.credstore.CredentialStoreFactory;
import com.vmware.vim25.AlreadyExistsFaultMsg;
import com.vmware.vim25.HostAccountSpec;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;

public class VAddUser extends CommonAction{

	public VAddUser(ConnectionMng m) {
		super(m);
	 
	}
	
	 String getServerName() {
	       
	            String urlString =  super.connectionMng.getUrl().toString();
	            if (urlString.indexOf("https://") != -1) {
	                int sind = 8;
	                int lind = urlString.indexOf("/sdk");
	                return urlString.substring(sind, lind);
	            } else if (urlString.indexOf("http://") != -1) {
	                int sind = 7;
	                int lind = urlString.indexOf("/sdk");
	                return urlString.substring(sind, lind);
	            } else {
	                return urlString;
	            }
	        
	    }

	protected void createUser(  String userName, String password) throws Exception {
		
		ManagedObjectReference hostLocalAccountManager = super.connectionMng.getServiceContent().getAccountManager();
		 
		
		 
		try
		{
			HostAccountSpec hostAccountSpec = new HostAccountSpec();
			hostAccountSpec.setId(userName);
			hostAccountSpec.setPassword(password);
			hostAccountSpec.setDescription("LMS User");
			vimPort.createUser(hostLocalAccountManager,
					hostAccountSpec);
			
		
		}
		catch(AlreadyExistsFaultMsg ignore)
		{
			
		}
		CredentialStore csObj = CredentialStoreFactory.getCredentialStore();
		csObj.addPassword(getServerName(), userName, password.toCharArray());
		System.out.println("Successfully created user and populated the "
				+ "credential store");
	
	}
	 
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception {
		String userName =  VMWareAPI.GUEST_USER_ID_PREFIX + param.getVmInfo().getOS_ADMIN_ID();
		String pwd = param.getVmInfo().getOS_ADMIN_PWD();
	 
		this.createUser(   userName, pwd);
		return null;
	}


}
