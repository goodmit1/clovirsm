package com.clovirsm.hv.vmware.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.vmware.VCRestCommon;
import com.clovirsm.hv.vmware.VCRestConnection;

public class GetVMTags extends VCRestCommon {

	public GetVMTags(VCRestConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result) throws Exception {
		JSONObject categoryList = client.getJSON("/com/vmware/cis/tagging/category");
		JSONArray list = categoryList.getJSONArray("value");
		Map categoryMap = new HashMap();
		for(int i=0; i < list.length(); i++) {
			
			JSONObject category =  client.getJSON("/com/vmware/cis/tagging/category/id:" + list.getString(i));
			categoryMap.put(list.getString(i), category.getJSONObject("value").getString("name"));
		}
		JSONObject tagList = client.getJSON("/com/vmware/cis/tagging/tag");
		list = tagList.getJSONArray("value");
		List resultList = new ArrayList(); 
		for(int i=0; i < list.length(); i++) {
				
				JSONObject category =  client.getJSON("/com/vmware/cis/tagging/tag/id:" + list.getString(i));
				JSONObject objs = (JSONObject)client.post("/com/vmware/cis/tagging/tag-association?~action=list-attached-objects-on-tags", "{ \"tag_ids\": [ \"" + list.getString(i)  + "\" ] }"  );
				JSONArray values =  objs.getJSONArray("value");
				if(values.length()==0)  continue;
				
				JSONArray objList = values.getJSONObject(0).getJSONArray("object_ids");
				 
				for(int j=0; j< objList.length(); j++) {
					JSONObject obj1 = objList.getJSONObject(j);
					if(obj1.getString("type").equals("VirtualMachine")) {
						Map m = new HashMap();
						m.put("TAG_NAME", categoryMap.get(category.getJSONObject("value").getString("category_id")));
						m.put("TAG_VAL",  category.getJSONObject("value").getString("name"));
						m.put("VM_HV_ID",  obj1.getString("id"));
						resultList.add(m);
					}
				}
				
				 
		}
		result.put("LIST", resultList);
		
	}

}
