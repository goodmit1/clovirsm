package com.clovirsm.hv.vmware;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public   class GetDFResult {
	
	public List<Map> getData(String filePath) throws Exception{
		FileReader reader = new FileReader(filePath);
		try {
			BufferedReader breader = new BufferedReader(reader);
			breader.readLine();
			String line ;
			List result = new ArrayList();
			while((line = breader.readLine() ) != null) {
				if(line.indexOf(":")>0) {
					String[] arr = line.split(" ");
					Map m = new HashMap();
					m.put("NAS_PATH", arr[0]);
					m.put("TOTAL", arr[1]);
					m.put("USED", arr[2]);
					result.add(m);
				}
			}
			return result;
		}
		finally {
			reader.close();
		}
	}
	public static void main(String[] args) {
		GetDFResult r = new GetDFResult();
		try {
			System.out.println(r.getData("c:/temp/GIT-Mobis-Portal-df.txt"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}