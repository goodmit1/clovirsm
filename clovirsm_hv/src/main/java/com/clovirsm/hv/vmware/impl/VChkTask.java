package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.TaskInfo;
import com.vmware.vim25.TaskInfoState;

import java.util.Map;

public class VChkTask  extends CommonAction{

	public VChkTask(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception {
		return null;
	}


	public boolean run(String taskId) throws Exception {
		ManagedObjectReference task  = this.makeManagedObjectReference("Task", taskId);
		TaskInfo taskInfo = null;
		try
		{
			taskInfo = (TaskInfo)getProp(task, "info");
		}
		catch(Exception e)
		{
			System.out.println("task " + taskId + " is deleted");

			return true;
		}

		if(taskInfo == null)
		{
			throw new Exception(taskId + " not found");
		}

		if(taskInfo.getState().equals(TaskInfoState.SUCCESS))
		{
			return true;
		}
		else if(taskInfo.getState().equals(TaskInfoState.ERROR))
		{
			throw new Exception(taskInfo.getError().getLocalizedMessage());
		}
		else
		{
			return false;
		}
    }
}
