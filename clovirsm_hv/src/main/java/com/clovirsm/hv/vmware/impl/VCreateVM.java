package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.*;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.clovirsm.hv.vmware.VwareVMInfo;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * VM 생성
 * @author 윤경
 *
 */
public class VCreateVM extends com.clovirsm.hv.vmware.CommonAction{


	public VCreateVM(ConnectionMng m) {
		super(m);

	}

	VwareVMInfo param;

	@Override
	protected ManagedObjectReference run1(String dc, HVParam paramMap, Map result)
			throws Exception {
		this.param = new VwareVMInfo(paramMap.getVmInfo());

		return this.createVM(false,result, param);

	}
	protected VirtualDeviceConfigSpec setNetwork(  String portgroupName, IPInfo ipInfo) throws Exception
	{
		ManagedObjectReference dvspgMor =	findObjectFromRoot(
				VMWareAPI.OBJ_TYPE_DVP,portgroupName);
		return setDVPNetwork(dvspgMor, ipInfo);
	}
	protected VirtualDeviceConfigSpec setStandardNetwork( ManagedObjectReference  vmRef, ManagedObjectReference dvspgMor, IPInfo ipInfo ) throws Exception
	{
		VirtualDeviceConfigSpec nicSpec = new VirtualDeviceConfigSpec();
		VirtualEthernetCard nic = null;
		List<VirtualDevice> hardwares = super.getHardwareDevice(vmRef);
		for (VirtualDevice device : hardwares) {
			if (device instanceof VirtualEthernetCard) {
					nic = (VirtualEthernetCard) device;
					break;
			}
		}
		
		if(nic == null)
		{
			nic = new VirtualVmxnet3();
			nic.setKey(ipInfo.nicId);

		}
		else
		{
			nicSpec.setOperation(VirtualDeviceConfigSpecOperation.EDIT);	
		}
		if(ipInfo.macAddress != null && !"".equals(ipInfo.macAddress)){
			nic.setAddressType("manual");
			nic.setMacAddress(ipInfo.macAddress);
		}
		else{
			nic.setAddressType("assigned");
		}
		
		// VirtualEthernetCard nic = new VirtualPCNet32();
		//
		String netName = (String) getProp(dvspgMor,"name");
		VirtualEthernetCardNetworkBackingInfo nicBacking = new VirtualEthernetCardNetworkBackingInfo();
	    VirtualDeviceConnectInfo info = new VirtualDeviceConnectInfo();
	    info.setConnected(true);
	    info.setStartConnected(true);
	    info.setAllowGuestControl(true);
	    nic.setConnectable(info);
	    nicBacking.setDeviceName(netName);
	    nicBacking.setUseAutoDetect(true);
	    nicBacking.setNetwork(dvspgMor);
	 
		nic.setBacking(nicBacking);

		nicSpec.setDevice(nic);
		return nicSpec;

		 
	}
	
	protected VirtualDeviceConfigSpec setDVPNetwork( ManagedObjectReference dvspgMor, IPInfo ipInfo ) throws Exception
	{

		VirtualDeviceConfigSpec nicSpec = new VirtualDeviceConfigSpec();

		nicSpec.setOperation(VirtualDeviceConfigSpecOperation.EDIT);
		// VirtualEthernetCard nic = new VirtualPCNet32();
		VirtualEthernetCard nic = new VirtualVmxnet3();

		


		DVPortgroupConfigInfo portgroupInfo = (DVPortgroupConfigInfo) getProp(dvspgMor,"config");
		if(portgroupInfo == null){
			return null;
		}
		ManagedObjectReference dvs = portgroupInfo.getDistributedVirtualSwitch();
		VMwareDVSConfigInfo switchInfo = (VMwareDVSConfigInfo) getProp(dvs,"config");
		if (switchInfo != null ) {


			DistributedVirtualSwitchPortConnection vspc= new DistributedVirtualSwitchPortConnection();
			vspc.setSwitchUuid(switchInfo.getUuid());
			vspc.setPortgroupKey(portgroupInfo.getKey());
			VirtualEthernetCardDistributedVirtualPortBackingInfo nicBacking = new VirtualEthernetCardDistributedVirtualPortBackingInfo();
			nicBacking.setPort(vspc);

			// nicBacking.setDeviceName(networkName);
			nic.setKey(ipInfo.nicId);
			if(ipInfo.macAddress != null && !"".equals(ipInfo.macAddress)){
				nic.setAddressType("manual");
				nic.setMacAddress(ipInfo.macAddress);
			}
			else{
				nic.setAddressType("assigned");
			}
			nic.setBacking(nicBacking);

			nicSpec.setDevice(nic);
			return nicSpec;

		}

		return null;

	}
	protected VirtualDeviceConfigSpec createVirtualDisk(ManagedObjectReference vmRef, Map m, String[] dsNames,
             int diskSizeGB, Set dsInHost) throws Exception {

		 
			 
			 
			 String diskName = createDisk.getDiskName(dsInHost, dsNames, param.getVM_NM(),  diskSizeGB);
			 String diskmode = "persistent";
			 VirtualDeviceConfigSpec vdcs = createDisk.getDiskDeviceConfigSpec(vmRef, diskName, diskSizeGB, diskmode);
			 m.put("DISK_PATH", diskName);
			 
		 	 return vdcs;
		 
	 
	}
	public static void setLinuxNetwork(int idx, VirtualMachineCloneSpec cloneSpec, String hostName, IPInfo ipInfo)
	{
		CustomizationSpec spinfo = new CustomizationSpec();
		customizeNetwork(idx, spinfo, ipInfo);
		CustomizationLinuxPrep idsettings  = new CustomizationLinuxPrep();
		CustomizationFixedName fixedName = new CustomizationFixedName();
	    fixedName.setName(hostName);

	    idsettings.setDomain("local.com");
		idsettings.setHostName(fixedName );
		spinfo.setIdentity(idsettings);
		CustomizationGlobalIPSettings ipset = new CustomizationGlobalIPSettings();

		spinfo.setGlobalIPSettings(ipset );
		cloneSpec.setCustomization(spinfo );
	}


	protected void setCustomizationSpecInfo(int idx, boolean isWin, VirtualMachineCloneSpec cloneSpec, CustomizationSpecItem cspec  ,String hostName,   String adminPwd ) throws Exception
	{


			if(!isWin && hostName != null)
			{
				CustomizationFixedName fixedName = new CustomizationFixedName();
			    fixedName.setName(hostName);
			    CustomizationLinuxPrep customizationLinuxPrep =(CustomizationLinuxPrep) cspec.getSpec().getIdentity();
			    customizationLinuxPrep.setHostName(fixedName);
				
			}
			else if(isWin && adminPwd != null){
				CustomizationSysprep sysprep = (CustomizationSysprep) cspec.getSpec().getIdentity();
				CustomizationPassword password = new CustomizationPassword();
				password.setPlainText(true);
				password.setValue(adminPwd);

				sysprep.getGuiUnattended().setPassword(password);
			}
			/*
			 * CustomizationWinOptions options = new CustomizationWinOptions();
			 * options.setChangeSID(true); options.setDeleteAccounts(true);
			 * cspec.getSpec().setOptions(options);
			 */

			cloneSpec.setCustomization(cspec.getSpec());

		 


	}


	private static void customizeNetwork(int idx , CustomizationSpec cspec, IPInfo ipInfo)
	{
		CustomizationIPSettings cip = new CustomizationIPSettings();
		if(ipInfo.ip==null || "".equals(ipInfo.ip))
		{
			CustomizationDhcpIpGenerator dhcp = new CustomizationDhcpIpGenerator();
			cip.setIp(dhcp);
		}
		else
		{
			CustomizationFixedIp fip = new CustomizationFixedIp();
			fip.setIpAddress(ipInfo.ip);
			cip.setIp(fip);
			cip.setSubnetMask(ipInfo.subnetMask);
			cip.getGateway().add(ipInfo.gateway);

		}
		
		List<CustomizationAdapterMapping> list = cspec.getNicSettingMap();
		if(list.size()<=idx)
		{
			List dnsServerList =    list.get(list.size()-1).getAdapter().getDnsServerList();
			if(dnsServerList !=null && dnsServerList.size()>0) {
				cip.getDnsServerList().addAll(dnsServerList);
			}
			CustomizationAdapterMapping mapping = new CustomizationAdapterMapping();
			mapping.setAdapter(cip);
			list.add(mapping);
		}
		else
		{
			List dnsServerList =    list.get(idx).getAdapter().getDnsServerList();

			if(dnsServerList !=null && dnsServerList.size()>0) {
				cip.getDnsServerList().addAll(dnsServerList);
			}
			list.get(idx).setAdapter(cip);

			if(ipInfo.macAddress != null) {
				list.get(idx).setMacAddress(ipInfo.macAddress);
			}
		}

	}

	VCreateDisk createDisk ;

	protected ManagedObjectReference createVM(boolean isTemplate, Map result, VwareVMInfo param) throws Exception
	{
		ManagedObjectReference vmRef  ;
		if(param.getFROM().startsWith("vm-")) {
			vmRef = new ManagedObjectReference();
			vmRef.setType("VirtualMachine");
			vmRef.setValue(param.getFROM());
		}
		else{
			vmRef = super.getVM(  param.getFROM());
		}
		if (vmRef == null) {
			throw new NotFoundException( "Template", param.getFROM());

		}

		ManagedObjectReference dcM = super.getDataCenter(param.getREAL_DC_NM());
		ManagedObjectReference vmFolderRef;
		if(param.getFOLDER_NM() != null && !"".equals(param.getFOLDER_NM()))
		{	
			VCreateFolder folder = new VCreateFolder(this.connectionMng);
	
			vmFolderRef = folder.makeFolder(dcM, param.getFOLDER_NM());
		}
		else
		{
			vmFolderRef = (ManagedObjectReference) super.getProp(vmRef, "parent");
		}

		VirtualMachineCloneSpec cloneSpec = new VirtualMachineCloneSpec();

		cloneSpec.setTemplate(isTemplate);

		VirtualMachineRelocateSpec relocSpec = new VirtualMachineRelocateSpec();
		ManagedObjectReference resourcepoolmor  ;
		ManagedObjectReference host = null;
		if(param.getHOST_HV_ID() != null && !"".equals(param.getHOST_HV_ID())) // host 셋팅
		{
			host = makeManagedObjectReference(VMWareCommon.OBJ_TYPE_HOST, param.getHOST_HV_ID());
			relocSpec.setHost(host);
			
			resourcepoolmor = super.findObject((ManagedObjectReference)super.getProp(host, "parent"), "ResourcePool", "Resources");
			
			
		}
		else if(param.getCLUSTER() != null && !"".equals(param.getCLUSTER())){
			ArrayOfManagedObjectReference vmhosts = (ArrayOfManagedObjectReference) super.getProp(super.getCluster(param.getREAL_DC_NM(),  param.getCLUSTER()), "host");
			host = vmhosts.getManagedObjectReference().get(0);
			relocSpec.setHost(host);
			resourcepoolmor = super.findObject((ManagedObjectReference)super.getProp(host, "parent"), "ResourcePool", "Resources");
		}
		else{
			String resourcePool = param.getRSC_GROUP_NM();
			if(resourcePool== null || "".equals(resourcePool))
			{
				resourcePool="Resources";

			}
			resourcepoolmor = super.findObject(dcM, "ResourcePool", resourcePool);
			 
		}
		relocSpec.setPool(resourcepoolmor);
		if(host == null)
		{
			host = (ManagedObjectReference)getProp(vmRef,"runtime.host");
		}
		Set dsInHost = super.getDsInHost(host);

		String[] dsNames = param.getDATASTORE_NM_LIST();
		if(dsNames != null && dsNames.length>0)
		{
			String dsNm = getAvailableDs(dsInHost, dsNames, param.getDISK_SIZE());
			result.put(VMWareAPI.PARAM_DATASTORE_NM, dsNm);
			if(dsNm != null)
			{
				ManagedObjectReference datastoreRef = findObjectFromRoot(VMWareCommon.OBJ_TYPE_DS, dsNm);
				relocSpec.setDatastore(datastoreRef);
			}
		}
		else{

				throw new Exception("DS_NM is empty");


		}
		cloneSpec.setLocation(relocSpec);
		
		 
		if(!isTemplate)
		{
			List<IPInfo> ipList =  param.getIP_LIST();

			boolean isWin  = isWindow(vmRef);
			int idx=0;
			CustomizationSpecItem cspec  ;
			ManagedObjectReference cpm = super.connectionMng.getServiceContent()
					.getCustomizationSpecManager();
			if(isWin)
			{
				cspec = vimPort.getCustomizationSpec(cpm,
						param.getWINDOWS_CSI());

				
			}
			else
			{
				cspec = vimPort.getCustomizationSpec(cpm,
						param.getLINUX_CSI());
			}
			setCustomizationSpecInfo(idx,isWin,cloneSpec, cspec, param.getVM_NM(),  param.getOS_ADMIN_PWD() );
			ArrayList deviceConfigSpec = new ArrayList<VirtualDeviceConfigSpec>();
			if(ipList != null) {
				for (IPInfo ipInfo : ipList) {
					if(ipInfo == null) {
						continue;
					}
					String ip = ipInfo.ip;
					String gateway = ipInfo.gateway;
					String subnet = ipInfo.subnetMask;


					System.out.println(ip + ":" + gateway + ":" + subnet + ":" + ipInfo.network);


					if (cspec != null) {
						customizeNetwork(idx, cspec.getSpec(), ipInfo);

					} else {
						setLinuxNetwork(idx, cloneSpec, param.getVM_NM(), ipInfo);
					}
					if (ipInfo.network != null) {
						//cspec.getSpec().getNicSettingMap().remove(cspec.getSpec().getNicSettingMap().size()-1);

						if (ipInfo.network.startsWith("network-")) {
							ManagedObjectReference nt = super.makeManagedObjectReference(VMWareAPI.OBJ_TYPE_NETWORK, ipInfo.network);
							deviceConfigSpec.add(this.setStandardNetwork(vmRef, nt, ipInfo));
						} else if(ipInfo.network.startsWith("dvportgroup-")){
							ManagedObjectReference nt = super.makeManagedObjectReference(VMWareAPI.OBJ_TYPE_DVP, ipInfo.network);
							deviceConfigSpec.add(this.setDVPNetwork(nt, ipInfo));
						} else {
							try{
								ManagedObjectReference nt = super.findObjectFromRoot(VMWareAPI.OBJ_TYPE_NETWORK, ipInfo.network);
								deviceConfigSpec.add(this.setStandardNetwork(vmRef, nt, ipInfo));
							}
							catch(NotFoundException  e){
								try {
									ManagedObjectReference nt = super.findObjectFromRoot(VMWareAPI.OBJ_TYPE_DVP, ipInfo.network);
									deviceConfigSpec.add(this.setDVPNetwork(nt, ipInfo));
								}
								catch (NotFoundException e1){
									throw new Exception(ipInfo.network + " Network not found");
								}
							}
						}

					}
					idx++;

				}
				if (cspec != null) {
					int diff = cspec.getSpec().getNicSettingMap().size() - ipList.size();
					for (int i = 0; i < diff; i++) {
						cspec.getSpec().getNicSettingMap().remove(cspec.getSpec().getNicSettingMap().size() - 1);
					}
				}
			}
			VirtualMachineConfigSpec spec = new VirtualMachineConfigSpec();
			

		

			cloneSpec.setPowerOn(param.isPOWER_ON());

			spec.setNumCPUs(param.getCPU());
			spec.setMemoryMB(1024L * param.getRAM_SIZE()  );

			if(param.getPORT_GROUP_NM() != null && !"".equals(param.getPORT_GROUP_NM()) && ipList != null && ipList.size()>0)
			{
				VirtualDeviceConfigSpec nicSpec = setNetwork(  param.getPORT_GROUP_NM(), ipList.get(0) );
				deviceConfigSpec.add(nicSpec);
			}
			//String diskPath = getVMDiskFileName(dsNm, vmName);
			//List<VirtualDevice> hardwares = super.getHardwareDevice(vmRef);
			List<DiskInfo> dataDisk =  param.getDISK_LIST();
			if(dataDisk != null) {
				createDisk = new VCreateDisk(this.connectionMng);

				for (Map m : dataDisk) {

					VirtualDeviceConfigSpec diskSpec = createVirtualDisk(vmRef, m, dsNames, NumberUtil.getInt(m.get("DISK_SIZE")), dsInHost);
					if (diskSpec != null) deviceConfigSpec.add(diskSpec);
				}
			}
			if(deviceConfigSpec.size()>0)
			{
				spec.getDeviceChange().addAll(deviceConfigSpec);
			}
			if(param.getPURPOSE() != null) spec.setAnnotation(param.getPURPOSE());
			cloneSpec.setConfig(spec );
		}


		ManagedObjectReference cloneTask =
				vimPort.cloneVMTask(vmRef, vmFolderRef, param.getVM_NM(), cloneSpec);
		result.put(HypervisorAPI.PARAM_TASK_ID, cloneTask.getValue());
		return cloneTask;

	}

}

