package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.security.credstore.CredentialStore;
import com.vmware.security.credstore.CredentialStoreFactory;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;

public class VDeleteUser  extends CommonAction{

	public VDeleteUser(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception {
		ManagedObjectReference hostLocalAccountManager = null;
		String hostName = (String)param.getConnParam().get(VMWareAPI.PARAM_HOST_NM);
		ManagedObjectReference host = (ManagedObjectReference)super.findObjectFromRoot("HostSystem", hostName);
		hostLocalAccountManager = (ManagedObjectReference)super.getProp(host, "configManager.accountManager");
		
		String userName = VMWareAPI.GUEST_USER_ID_PREFIX +  param.getConnParam().get(VMWareAPI.PARAM_GUEST_USERID);
		vimPort.removeUser(hostLocalAccountManager, userName);
		CredentialStore csObj = CredentialStoreFactory.getCredentialStore();
		csObj.removePassword(hostName, userName);
		return null;
	}

}
