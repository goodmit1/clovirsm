package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;

/**
 * 폴더 등 기타 오브젝트 삭제
 * @author 윤경
 *
 */
public class VDeleteObject  extends com.clovirsm.hv.vmware.CommonAction{

	public VDeleteObject(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {

		return null;
	}
	
	protected void deleteObject(ManagedObjectReference obj) throws Exception
	{
	 
		ManagedObjectReference taskmor = vimPort.destroyTask(obj);
		getTaskResultAfterDone(taskmor, "Delete " + obj.getType() + ":" + obj.getValue());
	}

    public void run(String type, String name) throws Exception {
		deleteObject(super.getObject(type, name));
    }
}
