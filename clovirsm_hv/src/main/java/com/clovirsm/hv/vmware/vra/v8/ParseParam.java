package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.yaml.snakeyaml.Yaml;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HVProperty; 

public class ParseParam {
	Map fieldMap = null;
	public ParseParam() {
	
		fieldMap = VRA8Util.getFieldNameMap("A");
		
	}
	public Map<String,Map> run(String yamlString) throws  Exception {
		 
		Yaml yaml= new Yaml();
	    Map map = (Map) yaml.load(yamlString); 
	    return run(map);
	}
	
	
	
	boolean hasMe(String prop, String key) {
		String pattern = ".*\\$\\{.*resource(\\[(\"|')|\\.)" + key + "(('|\")\\]|\\.|\\[).*";
		
		return prop.matches(pattern); 
	}
	protected String getProvisionChk(String fieldName, String key, JSONObject input, JSONObject prop) throws Exception {
		if(fieldName == null) return null;
		String pattern = HVProperty.getInstance().getProperty( "vra_app_field_pattern",".*tasks\\.yaml\\?tasks=.*");
		if(fieldName.equals("provision")) {
			Object provisionO = prop.getJSONObject("playbooks").get("provision");
			if("array".equals( input.getString("type")) && provisionO instanceof String){ // provision의 값이 array인 경우
				return "application";
			}
			
			if(provisionO instanceof JSONArray) {
				JSONArray provision  = (JSONArray)provisionO;
				for(int i=0; i < provision.length(); i++) {
					if(provision.getString(i).startsWith("${input." + key) || provision.getString(i).matches(pattern + "input\\." + key + ".*") ) {
						return "application";
					}
			
				}
			}
			 
			 
			
			return "";
		}
		return fieldName;
	}
	JSONObject json;
	
	public JSONObject getJSON() {
		return json;
	}
	private int getVMCount(   Map<String,Map> steps, ArrayList<String[]> target) {
		int result = 0;
		for(String[] a:target) {
			if(((String)steps.get(a[0]).get("TAGS")).startsWith("type:VM")) {
				result++;
			}
		}
		return result;
	}
	private String[] getVMCountByInput(   Map<String,Map> steps, String name, List<String[]> target) throws Exception {
		 
		for(Object o:steps.keySet()) {
			if(((String)steps.get(o).get("TAGS")).startsWith("type:VM")  ) {
				for(String[] a:target) {
					if(((String)steps.get(o).get("STEP_NM")).equals(a[0])) {
						return a;
					}
				}
			}
		}
		return null;
	}
	public Map<String,Map> run(Map map) throws  Exception {
		
	    json = new JSONObject(map);
	    Map<String,List<String>> swMap = VRA8Util.getSWInfo(new org.json.JSONObject(json.toString()), null);
	    Map<String,Map> steps = new LinkedHashMap();
	    if(!json.has("inputs")) {
	    	return steps;
	    }
		JSONObject inputs = json.getJSONObject("inputs");
		JSONObject resources = json.getJSONObject("resources");
		
		Map<String,String> resourcesProp = new LinkedHashMap();
		Iterator keys = resources.keys(); 
		while(keys.hasNext()) {
			String key = (String) keys.next();
			JSONObject resource = (JSONObject) resources.get((String)key);
			String tags =  getTagName(resource.getJSONObject("properties"));
			if(inputs.has("_" + key + "_param")) {
				List<String> sws = swMap.get(key);
				if(sws != null && sws.size()>0)
				{
					tags +=",app:" + CommonUtil.join( sws, "|") + ",appName:" + key;
				}
			}
			if(((String)resource.get("type")).matches("Cloud.*.Machine")) {// VM 외부에서 VM에 관련 된 것 ...
				tags =  "type:VM"   + tags;
			}
			steps.put((String)key, getStepMap(  (String)key,  tags ));
			
			resourcesProp.put((String)key, resource.get("properties").toString());
		}
		steps.put("ETC", getStepMap(  "ETC",""));
		Iterator it = inputs.keys();
		while(it.hasNext()) {
			String key = (String)it.next();
			if(key.startsWith("_")) continue;
			ArrayList<String[]> target = new ArrayList();
			for(String key1:resourcesProp.keySet()) {
				String prop = resourcesProp.get(key1);
				JSONObject propJson = new JSONObject(prop);
				String fieldName = getProvisionChk(getFieldName(propJson, prop, (String) key), key, inputs.getJSONObject(key), propJson);
				
				if(fieldName != null) {
					target.add(new String[] {key1, fieldName});
				}
			}
			JSONObject input = inputs.getJSONObject((String)key);
			input.put("name", key);
			if(target.size()>0) { // input 이 사용된 target
				int cnt = getVMCount(steps, target);
				if(cnt>1) {
					input.put("fieldName", target.get(0)[1]);
					((JSONArray)steps.get("ETC").get("INPUTS")).put(input);
				}
				else {
					if(target.size()==1) {
						String[] a = target.get(0);
						input.put("fieldName", a[1]);
						String type = (String)fieldMap.get(a[1]);
						if(type != null) {
							input.put("type", type);
						}
						putNotExistJSON((JSONArray)steps.get(a[0]).get("INPUTS"), input);
					}
					else {
						String[] a = getVMCountByInput(steps, key, target);
						if(a != null) {
							input.put("fieldName", a[1]);
							String type = (String)fieldMap.get(a[1]);
							if(type != null) {
								input.put("type", type);
							}
							putNotExistJSON((JSONArray)steps.get(a[0]).get("INPUTS"), input);
							 
						}
					}
					
					
				}
				
				 
			}
			
		}
		it = resources.keys();
		while(it.hasNext()) {
			String key = (String)it.next();
			JSONObject resource = (JSONObject) resources.get((String)key);
			if(!((String)resource.get("type")).matches("Cloud.*.Machine")) {// VM 외부에서 VM에 관련 된 것 ...
				if(((String)resource.get("type")).matches("Cloud.Ansible")) {// ansible
					String host = resource.getJSONObject("properties").getString("host");
					String vmName = VRA8Util.getResourceName(host );
					if(vmName != null) {
						putAllJSONArray((JSONArray)steps.get(vmName).get("INPUTS"),(JSONArray)steps.get(key).get("INPUTS"));
						steps.remove(key);
					}
						
				}
				else
				{
					ArrayList target = new ArrayList();
					for(String key1:resourcesProp.keySet()) {
						if(key1.equals(key)) continue;
						String prop = resourcesProp.get(key1);
						if(hasMe(prop , key  )) {
							target.add(key1);
						}
						 
					}
					if( target.size()==1){
						Map targetObj = steps.get(target.get(0));
						if(targetObj != null) {
							putAllJSONArray((JSONArray)targetObj.get("INPUTS"),(JSONArray)steps.get(key).get("INPUTS"));
							steps.remove(key);
						}
					}
				}
				
				if(steps.get(key) != null)	{
					putAllJSONArray((JSONArray)steps.get("ETC").get("INPUTS"),(JSONArray)steps.get(key).get("INPUTS"));
					steps.remove(key);
					
				} 
				
			}
			else if(resource.getJSONObject("properties").has("count")) {
				String count = resource.getJSONObject("properties").getString("count");
				chkVMCount(count, resource, steps, key);
				
				 
			}
		}
		
		Set<String> stepKeys = new HashSet();
		stepKeys.addAll(steps.keySet());
		for(String k:stepKeys) {
			boolean hasApp =  ((String)((Map)steps.get(k)).get("TAGS")).indexOf("app:")>=0;
			if(!hasApp && ( (JSONArray)steps.get(k).get("INPUTS")).length()==0) {
				steps.remove(k);
			}
		}
		 
		System.out.println(steps.keySet() + ":" + steps);
		return steps;
	 
	}
	private void chkVMCount(String count, JSONObject resource, Map<String,Map> steps, String key) throws Exception {
		if(count.startsWith(("${length"))){
			int max = 1;
			int min = 1;
			int def = 1;
			try {
				max = resource.getJSONObject("properties").getInt("count_max");
			}
			catch(Exception ignore) {
				
			}
			try {
				min = resource.getJSONObject("properties").getInt("count_min");
				def = min;
			}
			catch(Exception ignore) {
				
			}
			try {
				def = resource.getJSONObject("properties").getInt("count_def");
			}
			catch(Exception ignore) {
				
			}
			if(max>min) {
				((JSONArray)steps.get(key).get("INPUTS")).put(countInput("_count_" + key,min, max, def));
				resource.getJSONObject("properties").put("count", "${input._count_" + key + "}");
			}
			else {
				resource.getJSONObject("properties").put("count", min);
			}
			
		}
	}
	private JSONObject countInput(String name, int min, int max, int def) throws Exception {
		JSONObject json = new JSONObject("{\"default\":1,\"fieldName\":\"count\",\"name\":\"count\",\"maximum\":7,\"description\":\"서버 갯수\",\"type\":\"integer\",\"title\":\"서버 갯수\",\"minimum\":1}");
		json.put("default", def);
		json.put("name", name);
		json.put("maximum", max);
		json.put("minimum", min);
		return json;
	}
	public   void putAllJSONArray(JSONArray target, JSONArray src) throws  Exception {
		if(src == null) return;
		for(int i=0; i < src.length(); i++) {
			if(!existName(target, src.getJSONObject(i).getString("name"))) {
				target.put(src.get(i));
			}
		}
		 
	}
	public   void putNotExistJSON(JSONArray target, JSONObject src) throws  Exception {
		if(src == null) return;
		 
		if(!existName(target, src.getString("name"))) {
			target.put(src);
		}
		 
		 
	}
	private boolean existName(JSONArray target, Object val) throws Exception {
		for(int i=0; i < target.length(); i++) {
			if(target.getJSONObject(i).getString("name").equals(val)) {
				return true;
			}
		}
		return false;
	}
	private int getStartQuot(String prop) {
		int pos = prop.lastIndexOf("\"");
		if(pos>0 ){
			if( prop.charAt(pos-1)=='\\') {
				return getStartQuot(prop.substring(0, pos-1));
			}
			else {
				return pos;
			}
				
		}
		return -1;
				
	}
	private String getFieldName(JSONObject obj, String prop, int pos) throws Exception {
		Iterator<String> it = obj.keys();
		String result=null;
		while(it.hasNext()) {
			String k = it.next();
			int pos1 = prop.indexOf("\"" + k + "\":");
			if(pos1>0 && pos1<pos) {
				result = k;
			}
			else {
				break;
			}
		}
		if(result != null && obj.get(result) instanceof JSONObject ) {
			return getFieldName(obj.getJSONObject(result), prop, pos);
		}
		return result;
	}
	private String getFieldName(JSONObject projJson, String prop, String key) throws Exception  {
		
		String fieldName = getFieldNameByKeyword(projJson, prop, "${input." + key + "}");
		if(fieldName == null) {
			 fieldName = getFieldNameByKeyword(projJson, prop, "input." + key  );
		}
		
		return fieldName;
	}
	private String getFieldNameByKeyword(JSONObject projJson, String prop, String keyword) throws Exception  {
		int pos = prop.indexOf(keyword );
		if(pos>0) {
			return getFieldName(projJson, prop, pos);
			 
			
		}
		return null;
	}
	private Map getStepMap(  String key, String tags) {
		Map m = new HashMap();
		 
		m.put("STEP_NM", key);
		m.put("TAGS", tags);
		m.put("INPUTS", new JSONArray());
		return m;
	}
	private String getTagName(JSONObject json) throws Exception {
		if(json.has("tags")) {
			JSONArray tags = json.getJSONArray("tags");
			StringBuffer sb = new StringBuffer();
			for(int i=0; i <tags.length(); i++) {
				String key = tags.getJSONObject(i).getString("key");
				String value = tags.getJSONObject(i).getString("value");
				if(!value.startsWith("${")) {
					sb.append(","  + key + ":" + value);
				}
			}
			return sb.toString();
		}
		return "";
	}
	
}
