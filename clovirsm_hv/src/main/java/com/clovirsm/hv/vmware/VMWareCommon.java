package com.clovirsm.hv.vmware;

import java.util.Map;

import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IConnection;
import com.vmware.connection.ConnectionMng;

public class VMWareCommon {
	/** company datacenter property **/
	public static final String PARAM_CLUSTER = "CLUSTER";
	public static final String PARAM_PORT_GROUP_NM = "DVPG";
	public static final String PARAM_VLAN_ID = "VLAN";

	public static final Object PARAM_FOLDER_NM = "FOLDER";



	/** company datacenter property **/
	public static final String PARAM_VIRTUALSWITCH_ID = "virtualswitch_id";
	public static final Object PARAM_IMG_DATASTORE_NM = "img_ds_nm";
	public static final Object PARAM_CLUSTER_LIST = "clusterList";

	public static final Object PARAM_WINDOWS_CSI = "windows_csi";
	public static final Object PARAM_LINUX_CSI = "linux_csi";

	public static final String OBJ_TYPE_VM = "VirtualMachine";
	public static final String OBJ_TYPE_DVP = "DistributedVirtualPortgroup";
	public static final String OBJ_TYPE_DVS = "VmwareDistributedVirtualSwitch";

	public static final String OBJ_TYPE_NETWORK="Network";

	public static final String OBJ_TYPE_RP = "ResourcePool";
	public static final String OBJ_TYPE_FOLDER = "Folder";

	public static final String PARAM_PLUS_CPU = "plus_cpu"; //resourcepool 계산용
	public static final Object PARAM_INTERVAL = "interval";
	public static final String OBJ_TYPE_DC = "Datacenter";
	public static final String OBJ_TYPE_CLUSTER = "ClusterComputeResource";
	public static final String OBJ_TYPE_HOST = "HostSystem";

	public static final String OBJ_TYPE_DS = "Datastore";
	public static final String OBJ_TYPE_CSI = "CustomizationSpecInfo";
	public static final Object PARAM_DC_OBJ_ID = "DC_OBJ_ID";


	protected void disconnectNew(ConnectionMng conn)
	{
		try {

			if(conn != null) conn.disconnect();;
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	protected void disconnect(IConnection conn)
	{
		try {

			if(conn != null) ConnectionPool.getInstance().release((IConnection)conn);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	protected void disconnect(VCRestConnection conn)
	{
		try {

			if(conn != null) ConnectionPool.getInstance().release((IConnection)conn);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	public VMWareConnection connect(Map param) throws Exception
	{
		int initPoolSize = 0;
		try{
			initPoolSize = Integer.parseInt(param.get(HypervisorAPI.PARAM_INIT_POOL_SIZE).toString());
		}
		catch(Exception ignore){

		}
		int maxPoolSize = 5;
		try{
			maxPoolSize = Integer.parseInt(param.get(HypervisorAPI.PARAM_MAX_POOL_SIZE).toString());
		}
		catch(Exception ignore){

		}
		VMWareConnection connection  = (VMWareConnection)ConnectionPool.getInstance().getConnection(VMWareConnection.class.getName(), (String)param.get(HypervisorAPI.PARAM_URL), (String)param.get(HypervisorAPI.PARAM_USERID), (String)param.get(HypervisorAPI.PARAM_PWD), param, maxPoolSize, initPoolSize  );


		return connection ;
	}
	public VCRestConnection connectRest(Map param) throws Exception
	{
		VCRestConnection connection  = (VCRestConnection)ConnectionPool.getInstance().getConnection(VCRestConnection.class.getName(), (String)param.get(HypervisorAPI.PARAM_URL), (String)param.get(HypervisorAPI.PARAM_USERID), (String)param.get(HypervisorAPI.PARAM_PWD), param );

		return connection ;
	}
	protected ConnectionMng connectNewGuest(Map param) throws Exception
	{
		VMWareConnection connection  = (VMWareConnection)ConnectionPool.getInstance().getNewConnection(VMWareConnection.class.getName(), (String)param.get(HypervisorAPI.PARAM_URL), (String)param.get(HypervisorAPI.PARAM_GUEST_USERID), (String)param.get(HypervisorAPI.PARAM_GUEST_PWD), param);

		return connection ;
	}
	protected ConnectionMng connectHostNewGuest(Map param) throws Exception
	{
		VMWareConnection connection  = new  VMWareConnection();
		connection.setHostConnection(true);
		connection.connect((String)param.get(HypervisorAPI.PARAM_URL), (String)param.get(HypervisorAPI.PARAM_GUEST_USERID), (String)param.get(HypervisorAPI.PARAM_GUEST_PWD));
		return connection ;
	}
	public ConnectionMng connectHostNew(Map param) throws Exception
	{
		VMWareConnection connection  = new  VMWareConnection();
		connection.setHostConnection(true);
		connection.connect((String)param.get(HypervisorAPI.PARAM_URL), (String)param.get(HypervisorAPI.PARAM_USERID), (String)param.get(HypervisorAPI.PARAM_PWD));
		return connection ;
	}
}
