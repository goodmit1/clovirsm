package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;

/**
 * 스냅샷 삭제
 * @author 윤경
 *
 */
public class VDeleteSnapShot  extends VCreateSnapshot{

	public VDeleteSnapShot(ConnectionMng m) {
		super(m);
		 
	}
	 
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		 

		return null;
	}
	public Object run(HVParam param, String snpName) throws Exception {
		deleteSnapshot(super.getVM(param.getVmInfo()),snpName);
		return null;
	}
	protected void deleteSnapshot(ManagedObjectReference vmRef, String snapshotname)  
	{
		 
		try {
			 
			ManagedObjectReference snapmor = getSnapshotReference(vmRef,  snapshotname);
		    if (snapmor != null) {
		        ManagedObjectReference taskMor =
		                  vimPort.removeSnapshotTask(snapmor, true, true);
		        //getTaskResultAfterDone(taskMor, "DeleteSnapshot:" + vmName + "," + snapshotname);
		    }
		} catch (Exception e) {
		 
			e.printStackTrace();
		}
	 
		
	}

}
