package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 이벤트 목록
 * @author 윤경
 *
 */
public class VListEvent extends com.clovirsm.hv.vmware.CommonAction {

	public VListEvent(ConnectionMng m) {
		super(m);
		 
	}



	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		ManagedObjectReference ref = super.getObject(param.getMonitorParam().getTargetObjType(), param.getMonitorParam().getTargetObjNm());
		 

		List<Map> list = createEventHistoryCollector(ref, param.getMonitorParam());
		result.put(VMWareAPI.PARAM_LIST, list);
		return null;

	}
	
	List<Map> createEventHistoryCollector(ManagedObjectReference vmRef, MonitorParam monitorParam) throws Exception
	{
		if(monitorParam.getPageNo()<0) monitorParam.setPageNo(1);
		if(monitorParam.getPageSize()<0) monitorParam.setPageSize( 100) ;

		ManagedObjectReference eventManagerRef = super.connectionMng.getServiceContent().getEventManager();
		EventFilterSpecByEntity entitySpec = new EventFilterSpecByEntity();
		entitySpec.setEntity(vmRef);
		entitySpec.setRecursion(EventFilterSpecRecursionOption.ALL);
		EventFilterSpec eventFilter = new EventFilterSpec();
		eventFilter.setEntity(entitySpec);
		if(monitorParam.getCategory() != null)
		{
			eventFilter.getCategory().add(monitorParam.getCategory());
		}
		
		if(monitorParam.getStartTime()  != null || monitorParam.getFinishTime()  != null)
		{
			EventFilterSpecByTime timeFilter = new EventFilterSpecByTime();
			if(monitorParam.getStartTime()  != null) timeFilter.setBeginTime(toXmlCal(monitorParam.getStartTime() ));
			if(monitorParam.getFinishTime() != null) timeFilter.setEndTime(toXmlCal(monitorParam.getFinishTime()));
			eventFilter.setTime(timeFilter );
		}
		
		ManagedObjectReference eventHistoryCollectorRef = vimPort.createCollectorForEvents(eventManagerRef, eventFilter);
		vimPort.setCollectorPageSize(eventHistoryCollectorRef, monitorParam.getPageSize()* monitorParam.getPageNo());
		ArrayOfEvent o = (ArrayOfEvent)getProp(eventHistoryCollectorRef,"latestPage");
		List<Map> result = new ArrayList();
		if(o == null) return result;
		List<Event>list1 = o.getEvent();
		int startNo=(monitorParam.getPageNo()-1)*monitorParam.getPageSize();
		
		ArrayOfEventDescriptionEventDetail eventDesc = (ArrayOfEventDescriptionEventDetail)getProp(super.connectionMng.getServiceContent().getEventManager(),"description.eventInfo");
		List<EventDescriptionEventDetail> eventInfo = eventDesc.getEventDescriptionEventDetail();

		for(int i = startNo; i < list1.size(); i++)
		{
			Event e = list1.get(i);
			Map m = new HashMap();
			m.put(VMWareAPI.PARAM_COMMENT, e.getFullFormattedMessage());
			XMLGregorianCalendar t = e.getCreatedTime();

			m.put(VMWareAPI.PARAM_CRE_TIME, t.toGregorianCalendar().getTime());
			m.put(VMWareAPI.PARAM_CATEGORY,this.getSeverity(e.getClass().getName(), eventInfo));
			m.put(VMWareAPI.PARAM_TARGET, getTarget(e));
			result.add(m);
		}
		return result;

	}


	String getTarget(Event e)
	{
		if(e.getVm() != null)
		{
			return "VM:" + e.getVm().getName();
		}
		else if(e.getDs() != null)
		{
			return "DS:" + e.getDs().getName();
		}
		else if(e.getHost() != null)
		{
			return "Host:" + e.getHost().getName();
		}
		return "";
	}
	private String getSeverity(String eventKey, List<EventDescriptionEventDetail>eventInfo) {
		// strip com.vmware.vim25. from eventKey
		int beginIndex = 17; // skip 17 chars
		String eventName = eventKey.substring(beginIndex);
		for (EventDescriptionEventDetail info: eventInfo) {
			if (info.getKey().equals(eventName))
				return info.getCategory();
		}
		return "Unknown";
	}

}
