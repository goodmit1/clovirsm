package com.clovirsm.hv.vmware.vrops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class GetMetricKeys extends VROpsCommon{

	public GetMetricKeys(VROpsConnection conn) {
		super(conn);
		 
	}

	@Override
	public Object run(Map param) throws Exception {
		  Map allMetrics = (Map) param.get("allMetrics");
		  String name = (String) param.get("name");
		  String vmId = (String) param.get("vmId");
		String vmResId = getVMResourceId(client, name, vmId);
		if(vmResId == null) {
			return null;
		}
		JSONObject statKeys = (JSONObject) client.get("/resources/" + vmResId + "/statkeys");
		JSONArray keys = statKeys.getJSONObject("ops:stat-keys").getJSONArray("ops:stat-key");
		List result1 = new ArrayList();
		for(int i=0; i < keys.length(); i++) {
			String key = keys.getJSONObject(i).getString("ops:key");
			if(allMetrics.containsKey(key))	{
				result1.add(  allMetrics.get(key));
			}
		}
		Map result = new HashMap();
		result.put("resourceId", vmResId);
		result.put("metrics", result1);
		return result;
	}

}
