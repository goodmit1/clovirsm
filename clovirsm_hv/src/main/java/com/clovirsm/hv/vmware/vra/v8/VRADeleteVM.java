package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.vra.VRACommon;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VRADeleteVM extends VRACommon{
	JSONObject json;
	String requestId;
	 
	public VRADeleteVM(VRAConnection conn) {
		super(conn);
		 
	}
	
	 
	protected String getResourceId( String name, boolean isId) {
		JSONArray resources = json.getJSONArray("resources");
		for(int i=0; i< resources.length(); i++) {
			if( resources.getJSONObject(i).has("properties")) {
				JSONObject property = resources.getJSONObject(i).getJSONObject("properties");
				 
					if(name.equals(property.getString("resourceName"))){
						if(isId) {
							return resources.getJSONObject(i).getString("id");
						}
						else {
							return property.getString("resourceId");
						}
						
					}
				 
			}
		}
		return null;
	}
	protected void remove ( String name ) throws Exception{
		//{"actionId":"Cloud.vSphere.Machine.Remove.Disk","targetId":"542f5fe2-2638-4622-a2bf-4804aac3732f","inputs":{"diskId":"/resources/disks/671bb1417f99447559e81693ce2e8"}}
				JSONObject param = new JSONObject();
				param.put("actionId", "Cloud.vSphere.Machine.Delete");
				String vmId =  getResourceId(name, true);
				if(vmId == null) {
					return;
				}
				param.put("targetId",vmId);
				remove (   param,   vmId  ) ;
				
	}
	protected void remove ( JSONObject param, String vmId  ) throws Exception{
		try
		{
			JSONObject result = (JSONObject)super.client.post("/deployment/api/deployments/" + requestId + "/resources/" + vmId + "/requests", param.toString());
			System.out.println(result); 
		}
		catch(Exception e) {
			JSONObject error = new JSONObject( e.getMessage());
			if(error.getInt("errorCode") == 20009) {
				Thread.sleep(10000);
				remove(param, vmId);
			}
			else {
				throw e;
			}
		}
	}
	@Override
	public void run(Map param, Map result) throws Exception {
		 requestId = (String)param.get("VRA_REQUEST_ID");
		 try
		 {
			 json = super.client.getJSON("/deployment/api/deployments/" + requestId + "?expandProject=true&expandResources=true&expandLastRequest=true");
			 remove((String)param.get("VM_NM"));
		 }
		 catch(Exception e) {
			 if(e.getMessage().indexOf("404") < 0) {
				 throw e;
			 }
			 result.put("NOT_FOUND", "Y");
		 }
	}
}
