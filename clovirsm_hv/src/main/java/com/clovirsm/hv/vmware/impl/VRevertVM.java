package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;

/**
 * 스냅샷에서 복구
 * @author 윤경
 *
 */
public class VRevertVM extends VDeleteSnapShot{

	public VRevertVM(ConnectionMng m) {
		super(m);
		 
	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		return null;
		
	}
	public void run(VMInfo vmInfo, String snapshotName)
			throws Exception {

		  revertSnapshot(super.getVM(vmInfo),snapshotName);

	}
	protected ManagedObjectReference revertSnapshot(ManagedObjectReference vmRef, String snapshotname) throws Exception
	{
		 
	 
		ManagedObjectReference snapmor = getSnapshotReference(vmRef,  snapshotname);
	    if (snapmor != null) {
	    	ManagedObjectReference taskMor =
                    vimPort.revertToSnapshotTask(snapmor, null, true);
	        return taskMor;
	  		
	    }
	    return null;
	}
}
