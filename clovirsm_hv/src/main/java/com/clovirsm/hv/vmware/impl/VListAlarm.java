package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.AlarmInfo;
import com.vmware.vim25.AlarmState;
import com.vmware.vim25.ArrayOfAlarmState;
import com.vmware.vim25.ManagedObjectReference;
/**
 * 정의된 알람 목록
 * @author 윤경
 *
 */
public class VListAlarm extends VListTriggeredAlarm {

	public VListAlarm(ConnectionMng m) {
		super(m);
		 
	}
	@Override
	protected void listConfigIssue(List<Map> result, ManagedObjectReference mor) throws Exception
	{
		
	}
	
	@Override
	protected  ArrayOfAlarmState listAlarmState(ManagedObjectReference mor) throws Exception
	{
		return (ArrayOfAlarmState) super.getProp(mor, "declaredAlarmState");
	}

	 
}
