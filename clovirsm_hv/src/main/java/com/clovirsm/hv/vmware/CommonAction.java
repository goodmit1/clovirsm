/*
 * ******************************************************
 * Copyright VMware, Inc. 2010-2012.  All Rights Reserved.
 * ******************************************************
 *
 * DISCLAIMER. THIS PROGRAM IS PROVIDED TO YOU "AS IS" WITHOUT
 * WARRANTIES OR CONDITIONS # OF ANY KIND, WHETHER ORAL OR WRITTEN,
 * EXPRESS OR IMPLIED. THE AUTHOR SPECIFICALLY # DISCLAIMS ANY IMPLIED
 * WARRANTIES OR CONDITIONS OF MERCHANTABILITY, SATISFACTORY # QUALITY,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
 */

package com.clovirsm.hv.vmware;

import com.clovirsm.hv.*;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;

/**
 *  
 */
public abstract class CommonAction {

	protected ConnectionMng connectionMng;
	protected VimPortType vimPort;
	
	/**
	 * 오브젝트 reference
	 * @param dc
	 * @param param SVC_CD, (SVC_NM | FOLDER_NM, VM_ID)
	 * @return
	 * @throws Exception
	 */
	protected ManagedObjectReference getObject(String dc, Map param) throws Exception
	{
		if(VMWareCommon.OBJ_TYPE_VM.equals(param.get(VMWareAPI.PARAM_OBJECT_TYPE)))
		{
			return getVM( param);
		}
		else
		{
			return findObjectFromRoot((String)param.get(VMWareAPI.PARAM_OBJECT_TYPE), (String)param.get(VMWareAPI.PARAM_OBJECT_NM));
		}
	}
	protected ManagedObjectReference getObject(  String type, String name) throws Exception
	{
		return findObjectFromRoot(type, name);

	}
	 public Map<ManagedObjectReference, Map<String, Object>> inContainerByType(
	            ManagedObjectReference container, String morefType,
	            String[] morefProperties ) throws  Exception {
	    	Map<String, ManagedObjectReference> temp =  inContainerByType(container,
	    			morefType );
	    	if(temp == null || temp.size()==0) return new HashMap();
	    	List<ManagedObjectReference> entityMors = new ArrayList();
			entityMors.addAll(temp.values());
			return connectionMng.getMOREF().entityProps(entityMors, morefProperties);

	    }
	protected ManagedObjectReference makeManagedObjectReference(String type, String id)
	{
		ManagedObjectReference obj = new ManagedObjectReference();
		obj.setType(type);
		obj.setValue(id);
		return obj;
	}

	protected XMLGregorianCalendar toXmlCal(Date date) throws Exception
	{
		GregorianCalendar gcal = new GregorianCalendar();
		gcal.setTimeInMillis(date.getTime());
	    return DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
	}
	public CommonAction(ConnectionMng m) {
		this.connectionMng = m;
		vimPort = connectionMng.getVimPort();
	}
	 
	protected String getVMDiskFileName(String dataStoreName, String vmName)
	{
		return  "[" + dataStoreName + "] " + vmName + "/" + vmName + System.currentTimeMillis() + ".vmdk";

	}
	
	/**
	 * after가 있다면 Async로 동작 
	 * @param param
	 * @param result
	 * @param after
	 * @throws Exception
	 */
	public void run(HVParam param, Map result, IAfterProcess after) throws Exception {

		 
		String dc = (String)param.getConnParam().get(VMWareAPI.PARAM_REAL_DC_NM);

		try {
			ManagedObjectReference task = run1(dc,  param, result);
			if(result != null && after!= null) after.getParam().putAll(result);
			if(task != null)
			{
				this.getTaskResultAfterDone(task, null, after);

			}
		}
		catch(Exception e) {
			if(e.getMessage() != null && e.getMessage().indexOf("session")>0) {
				connectionMng.disconnect();
			}
			throw e;
		}
	}
	protected Set getDsInHost( ManagedObjectReference host) throws Exception
	{
		Set availDsInHost = new HashSet();
		if(host != null)
		{
			List<ManagedObjectReference> datastores = ((ArrayOfManagedObjectReference)this.getProp(host, "datastore")).getManagedObjectReference();
			for(ManagedObjectReference m : datastores)
			{
				availDsInHost.add(m.getValue());
			}
		}
		return availDsInHost;
	}
	
	/**
	 * dsNames 중에서 사이즈여유가 있는 데이터 스토어 정보

	 * @return
	 * @throws Exception
	 */
	protected String getAvailableDs( Set availDsInHost , String[] dsIds_org, int diskSizeGB) throws  Exception
	{

		float rate = HVProperty.getInstance().getInt("vm.ds_size_rate", 100)/100; 
		for(String ds : dsIds_org)
		{
			String newDs = ds;
			if(!ds.startsWith("datastore-")){
				ManagedObjectReference dsRef = this.getObject("Datastore", ds);
				if(dsRef != null){
					newDs = dsRef.getValue();
				}
			}
			if(!availDsInHost.contains(newDs)) {
				System.out.println("getAvailableDs==" + ds + " not in " + availDsInHost);
				continue;
			}
			if(getDatastoreNameWithFreeSpace(newDs,  (int)(diskSizeGB*rate)))
			{
				if(ds.startsWith("datastore-")) {
					ManagedObjectReference datastoreRef = makeManagedObjectReference(VMWareCommon.OBJ_TYPE_DS, newDs);
					return (String) this.getProp(datastoreRef, "name");
				}
				else{
					return ds;
				}
				
			}
			else {
				System.out.println("getAvailableDs==" + ds + " not in " + availDsInHost);
			}
		}
		throw new HypervisorException(HypervisorException.DS_LACK_OF_SIZE,   "" + diskSizeGB + "GB" );
	}
	
	/**
	 * 데이터 스토어에 사이즈 여유가 있는지 여부
	 * @param storeNm
	 * @param minFreeSpaceGB
	 * @return
	 * @throws Exception
	 */
	boolean getDatastoreNameWithFreeSpace(String storeNm, int minFreeSpaceGB)
			throws Exception {
		ManagedObjectReference datastoreRef =  makeManagedObjectReference(VMWareCommon.OBJ_TYPE_DS, storeNm);
		DatastoreSummary ds =  (DatastoreSummary) getProp(datastoreRef, "summary");
		if(ds.getFreeSpace() <  1024l * 1024 * 1024 * minFreeSpaceGB )
		{
			System.out.println("getDatastoreNameWithFreeSpace = " + storeNm + "(" + ds.getFreeSpace()/(1024l * 1024 * 1024 ) + "GB" + ") <  " + minFreeSpaceGB + ")")  ;
			return false;
		}
		return true;
	}

	public  void run(HVParam param, Map result) throws Exception {
		this.run(param, result, null);
	}

	/**
	public class OnAfterThread extends Thread
	{
		ManagedObjectReference task;
		IAfterProcess after;
		public OnAfterThread(ManagedObjectReference task, IAfterProcess after)
		{
			this.task = task;
			this.after = after;
		}
		public void run()
		{
			try {
				getTaskResultAfterDone(task, null, after);
			} catch (Exception e) {

				e.printStackTrace();
			}
			finally
			{
				if(connectionMng != null)
					try {
						ConnectionPool.getInstance().release((IConnection)connectionMng);
					} catch (Exception e) {
						 
						e.printStackTrace();
					}
			}
		}
	}
	*/

	protected abstract ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception;

	/**
	 * DC안에 objType에 해당하는 오브젝트 목록
	 * @param dcmor
	 * @param objType
	 * @return
	 * @throws Exception
	 */
	protected Map<String, ManagedObjectReference> findObjectList(ManagedObjectReference dcmor,
			String objType ) throws Exception {
		Map<String, ManagedObjectReference> list = connectionMng.getMOREF()
				.inContainerByType(dcmor, objType) ;
		if (list != null) {
			return list ;
		} else {
			return null;
		}
	}
	protected Map<ManagedObjectReference,Map<String,Object>> findObjectList(ManagedObjectReference container, String morefType, String[] prop) throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
        return connectionMng.getMOREF().inContainerByType(container,morefType,prop,new RetrieveOptions());
    }
	/**
	 * dc안에서 objType과 objName으로 찾기
	 * @param dcmor
	 * @param objType
	 * @param objName
	 * @return
	 * @throws Exception
	 */
	protected ManagedObjectReference findObject(ManagedObjectReference dcmor,
			String objType, String objName) throws Exception {
		ManagedObjectReference ds = connectionMng.getMOREF()
				.inContainerByType(dcmor, objType).get(objName);
		if (ds != null) {
			return ds;
		} else {
			throw new NotFoundException(objType , objName);
		}
	}

	protected ManagedObjectReference getDataCenter(String dc) throws Exception {
		return findObjectFromRoot(VMWareCommon.OBJ_TYPE_DC, dc);

	}
	protected ManagedObjectReference getVM(String name) throws Exception {
		return findObjectFromRoot( VMWareCommon.OBJ_TYPE_VM, name);
	}
	protected ManagedObjectReference getVM(Map param) throws Exception {
		if(param.get(HypervisorAPI.PARAM_VM_HV_ID) != null && !"".equals(param.get(HypervisorAPI.PARAM_VM_HV_ID)))
		{
			ManagedObjectReference vm = new ManagedObjectReference();
			vm.setType("VirtualMachine");
			vm.setValue((String)param.get(HypervisorAPI.PARAM_VM_HV_ID));
			return vm;
		}
		return  findObjectFromRoot( VMWareCommon.OBJ_TYPE_VM, (String)param.get(VMWareAPI.PARAM_VM_NM));
	}
	protected ManagedObjectReference getVM1(String dc, String folderNm,String vmName) throws Exception {
		 
		ManagedObjectReference vm = null;
		if(folderNm == null || folderNm.equals(""))
		{
			vm = findObjectFromRoot( VMWareCommon.OBJ_TYPE_VM, vmName);
		}
		else
		{
			String pathInfo = "//" + dc + "//vm//" + folderNm + "//" + vmName;
			if(dc == null)
			{
				pathInfo = "//vm//" + folderNm + "//" + vmName;
			}
			System.out.println(pathInfo);
			vm = findByPath(pathInfo);
			if(vm==null)
			{
				vm = findObjectFromRoot( VMWareCommon.OBJ_TYPE_VM, vmName);
			}
		}
		if(vm == null)
		{
			throw new NotFoundException( "VM" , vmName);
		}
		return vm;

	}
	protected void getTaskResultAfterDone(ManagedObjectReference task, String  msg) throws Exception
	{
		
		  this.getTaskResultAfterDone(task, msg, null);
	}
	
	protected void getTaskResultAfterDone(ManagedObjectReference task, String  msg, IAfterProcess after) throws Exception
	{
		  getTaskResultAfterDone(false,task, msg, after );
	}
	
	
	 
	/**
	 * task가 null이 아니고 after 클래스가 있다면 사후 작업 수행
	 * @param task
	 * @param msg
	 * @param after
	 * @return
	 * @throws Exception
	 */
	protected void getTaskResultAfterDone(boolean isAlreadyStarted, ManagedObjectReference task, String  msg, IAfterProcess after)
			throws Exception {
		if(task == null)
		{
			return ;
		}
		if(!isAlreadyStarted && after != null)
		{
			after.startTask(task.getValue());
		}
		if(after == null) {
			Thread.sleep(1000);
			TaskInfo taskInfo =  (TaskInfo) this.getProp(task, "info");
			LocalizedMethodFault error = taskInfo.getError();
			if(error != null) {
			 
				throw  getException(error);
			}
		}
		 
	}
	private Exception getException(Object o){
		if(o instanceof LocalizedMethodFault )
		{
			return new Exception(
					((LocalizedMethodFault) o).getLocalizedMessage());
		}
		else 
		{
			return (Exception)o;
		}
	}
	
	/**
	 * 전체에서 type에 해당하는 오브젝트 목록
	 * @param type
	 * @return
	 * @throws Exception
	 */
	protected Map<String, ManagedObjectReference> findObjectListFromRoot(String type ) throws Exception {
		Map<String, ManagedObjectReference> dcmor = inContainerByType(type);
		if (dcmor != null) {
			return dcmor ;
		} else {
			return null;
		}
	}
	
	/**
	 * 전체에서 type과 name이 일치하는 오브젝트
	 * @param type
	 * @param name
	 * @return
	 * @throws Exception
	 */
	protected ManagedObjectReference findObjectFromRoot(String type, String name) throws Exception {
		if(type.equals("Folder"))
		{
			int pos = name.lastIndexOf("/");
			if(pos>0)
			{
				name = name.substring(pos+1);
			}
		}
		ManagedObjectReference dcmor = inContainerByType(type).get(name);
		if (dcmor != null) {
			return dcmor;
		} else {
			System.out.println("NOT FOUND " + type + "," + name);
			throw new NotFoundException( type,name);
		}
	}
	protected Map<String,ManagedObjectReference > inContainerByType(ManagedObjectReference container, String type) throws Exception{
		if(VMWareCommon.OBJ_TYPE_VM.equals( type )) {
			return inContainerVMOnlyMaster(container);
		}
		return connectionMng
		.getMOREF()
		.inContainerByType(
				container,
				type);
	}
	protected Map<String,ManagedObjectReference > inContainerByType(String type) throws Exception{
		return inContainerByType(connectionMng.getServiceContent().getRootFolder(), type);
	}
	protected Map inContainerVMOnlyMaster( ManagedObjectReference container ) throws Exception{
		 RetrieveResult vmList = connectionMng
			.getMOREF()
			.containerViewByType(container, VMWareCommon.OBJ_TYPE_VM, new RetrieveOptions(), new String[] { "name","config.ftInfo.role"});
		 return toMapOnlyMasterVM(vmList);
	}
	protected Map<String, ManagedObjectReference> toMapOnlyMasterVM(RetrieveResult rslts) throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
        final Map<String, ManagedObjectReference> tgtMoref = new HashMap<String, ManagedObjectReference>();
        String token = null;
        token = populateOnlyMasterVM(rslts, tgtMoref);

        while ( token != null && !token.isEmpty() ) {
            // fetch results based on new token
            rslts = vimPort.continueRetrievePropertiesEx(
            		connectionMng.getServiceContent().getPropertyCollector(), token);

            token = populateOnlyMasterVM(rslts, tgtMoref);
        }

        return tgtMoref;
    }
	
	protected   String populateOnlyMasterVM(final RetrieveResult rslts, final Map<String, ManagedObjectReference> tgtMoref) {
        String token = null;
        if (rslts != null) {
            token = rslts.getToken();
            for(ObjectContent oc : rslts.getObjects()) {
                ManagedObjectReference mr = oc.getObj();
                 
                List<DynamicProperty> dps = oc.getPropSet();
                if (dps != null) {
                	 String vmname= null;
        			 Object role = null;
        			 for(DynamicProperty p : dps) {
        				 if("name".equals( p.getName())){
        					 vmname = (String)p.getVal();
        				 }
        				 else {
        					 role = p.getVal();
        				 }
        			 }
        			 if(role==null || NumberUtil.getInt(role) == 1) {
        				 tgtMoref.put(vmname, mr);
        			 }
                }
               
            }
        }
        return token;
    }
	protected boolean isWindow(ManagedObjectReference vmRef) throws Exception
	{
		String os = (String)getProp(vmRef, "config.guestId");
		if(os.toLowerCase().startsWith("win"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * type 속성 정보
	 * @param _this
	 * @param type
	 * @return
	 * @throws Exception
	 */
	protected  Object getProp(ManagedObjectReference _this, String type) throws Exception
	{
		
		return connectionMng.getMOREF().entityProps(_this,
				new String[]{type}).get(type);
	}
	/**
	 * types에 해당하는 속성 정보 목록
	 * @param _this
	 * @param types
	 * @return
	 * @throws Exception
	 */
	protected  Map<String,Object> getProps(ManagedObjectReference _this, String... types) throws Exception
	{
		return connectionMng.getMOREF().entityProps(_this,
				types);
	}
	
	/**
	 * path경로로 오브젝트 찾아 return
	 * @param pathName
	 * @return
	 * @throws Exception
	 */
	protected ManagedObjectReference findByPath(String pathName) throws Exception
	{
		
		  return vimPort.findByInventoryPath(connectionMng.getServiceContent().getSearchIndex(),
				  pathName);
	}
	
	/**
	 * 랜카드 목록 
	 * @param list
	 * @return
	 */
	protected VirtualEthernetCard getNetworkDevice( List<VirtualDevice> list )
	{
		 
		for(VirtualDevice d : list)
		{
			if(d instanceof VirtualEthernetCard)
			{
				return (VirtualEthernetCard)d;
			}
		}
		return null;
	}
	
	/**
	 * 디스크 중 첫번째
	 * @param list
	 * @return
	 */
	protected VirtualDisk getDisk( List<VirtualDevice> list , boolean isFirst)
	{
		 
		VirtualDisk result = null;
		for(VirtualDevice d : list)
		{
			if(d instanceof VirtualDisk)
			{
				result= (VirtualDisk)d;
				if(isFirst) break;
			}
		}
		return result;
	}
	
	protected float getDiskTotalGB( List<VirtualDevice> list)
	{
		long result = 0;
		for(VirtualDevice d : list)
		{
			if(d instanceof VirtualDisk)
			{
				result +=  ((VirtualDisk)d).getCapacityInKB();
				
			}
		}
		return result/1024/1024;
	}
	/**
	 * 디스크 목록
	 * @param list
	 * @return
	 */
	protected List<VirtualDisk> getDisks( List<VirtualDevice> list )
	{
		 
		List result = new ArrayList();
		for(VirtualDevice d : list)
		{
			if(d instanceof VirtualDisk)
			{
				result.add(d);
			}
		}
		return result;
	}
	
	/**
	 * 하드웨어 목록
	 * @param vmMoref
	 * @return
	 * @throws Exception
	 */
	protected List<VirtualDevice> getHardwareDevice(ManagedObjectReference vmMoref) throws Exception
	{
		VirtualMachineConfigInfo vmConfigInfo =
                (VirtualMachineConfigInfo) getProp(vmMoref, "config");
       
       return         vmConfigInfo.getHardware().getDevice();
        
	}
	/**
	 * 클러스터 reference
	 * @param dc
	 * @param cluster
	 * @return
	 * @throws Exception
	 */
	protected ManagedObjectReference getCluster(String dc, String cluster)
			throws Exception {

		ManagedObjectReference dcmor = getDataCenter(dc);
		ManagedObjectReference ds = connectionMng.getMOREF()
				.inContainerByType(dcmor, VMWareCommon.OBJ_TYPE_CLUSTER)
				.get(cluster);
		if (ds == null) {
			throw new NotFoundException( "Cluster",cluster);
		}
		return ds;

	}
	protected static String getIP4(List<String> ips)
	{
		 
		for(String ip:ips)
		{
			if(ip.indexOf(".") > 0)
			{
				return ip;
				
			}
		}
		
		return "";
	}
	
}
