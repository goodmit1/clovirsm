package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;
/**
 * VM에 종속되지 않는 디스크 삭제
 * @author 윤경
 *
 */
public class VDeleteDisk  extends com.clovirsm.hv.vmware.CommonAction{

	public VDeleteDisk(ConnectionMng m) {
		super(m);
	 
	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		String path = (String)param.getDiskInfo().getDISK_PATH( );
		deleteDisk(dc, path);
		return null;
	}

	public void run(ManagedObjectReference dcRef, VMInfo vmInfo, DiskInfo diskInfo) throws Exception{
		super.connectionMng.getVimPort().deleteVirtualDiskTask(this.connectionMng.getServiceContent().getVirtualDiskManager(), diskInfo.getDISK_PATH(), dcRef);

	}
	protected void deleteDisk(String dc, String path) throws Exception
	{
		ManagedObjectReference dcRef = super.getDataCenter(dc);
		super.connectionMng.getVimPort().deleteVirtualDiskTask(this.connectionMng.getServiceContent().getVirtualDiskManager(), path, dcRef);
	}

}
