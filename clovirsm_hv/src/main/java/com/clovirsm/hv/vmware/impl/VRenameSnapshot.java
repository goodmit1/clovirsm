package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;

/**
 * 스냅 샷 이름 변경
 * @author 윤경
 *
 */
public class VRenameSnapshot extends VDeleteSnapShot{

	public VRenameSnapshot(ConnectionMng m) {
		super(m);
		 
	}
	public void run(VMInfo vmInfo, String oldName, String newName) throws Exception{
		renameSnapshot(super.getVM(vmInfo), oldName, newName);

	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		 
			return null;
	}
	
	protected void renameSnapshot(ManagedObjectReference vmRef, String snapshotname, String newName) throws Exception
	{
		 
	 
		ManagedObjectReference snapmor = getSnapshotReference(vmRef,  snapshotname);
	    if (snapmor != null) {
	    	vimPort.renameSnapshot(snapmor, newName, "");
	        
	  		
	    }
	}
}
