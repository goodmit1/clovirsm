package com.clovirsm.hv.vmware.vni;

import java.util.HashMap;
import java.util.Map;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.IAfterProcess;

public class VNIAPI extends CommonAPI{
	
	
	
	protected String getConnectionName() {
		return VNIConnection.class.getName();
	}
	 
	public static String getHVType() {
	 
		return "V.VRNI";
	}
	 
	protected Map doProcess(IAfterProcess after, Map param,  Class... classList) throws Exception
	{	
		VNIConnection connectionMgr = null;
		try
		{
			connectionMgr = (VNIConnection) connect(param);
			 
			Map result = new HashMap();
			for(Class cls : classList)
			{
				VNICommon action = (VNICommon)cls.getConstructor(VNIConnection.class).newInstance(connectionMgr);
			 	action.run(param, result, after);
			 	
			}
			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
	public Map getFlow(Map param) throws Exception{
		 
			return this.doProcess(null, param, VNIGetFlows.class);
			
		 
	}
	 
	public Map listHostNode(Map param) throws Exception{
		 
		return this.doProcess(null, param, GetVNIHostList.class);
		
	 
}
 
	 
}
