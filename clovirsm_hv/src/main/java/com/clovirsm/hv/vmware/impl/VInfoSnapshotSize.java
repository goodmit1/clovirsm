package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 스냅샷 사이즈 가져오기
 * @author 윤경
 *
 */
public class VInfoSnapshotSize extends VCreateSnapshot {

	public VInfoSnapshotSize(ConnectionMng m) {
		super(m);
		 
	}
	public Object run(HVParam param, String snapshotName) throws Exception {
		return getSize(super.getVM(param.getVmInfo()), snapshotName);
	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {

		return null;
		
	}
	private float getSize(ManagedObjectReference vmMor, String snapName) throws Exception
	{
		ManagedObjectReference sMor = getSnapshotReference(vmMor, snapName);
		String snapKey = sMor.getValue();
		 
		VirtualMachineFileLayoutEx o = (VirtualMachineFileLayoutEx)getProp(vmMor, "layoutEx" );
		
		List<VirtualMachineFileLayoutExFileInfo> files = o.getFile();
		Map sizeMap = new HashMap();
    	Map nameMap = new HashMap();
    	for(VirtualMachineFileLayoutExFileInfo i : files)
    	{
    		//System.out.println(i.getType() + ":"+ i.getKey() + ":" + i.getName() + ":" + i.getSize());
    		sizeMap.put(i.getKey(), i.getSize());
    		
    		if(i.getName().indexOf("0000")>0)
    		{
    			nameMap.put(i.getKey(), i.getName());
	    		 
    		}
    	
    	}
    	List<VirtualMachineFileLayoutExSnapshotLayout> sList = o.getSnapshot();
    	int idx=0;
    	for(VirtualMachineFileLayoutExSnapshotLayout s : sList)
    	{
    		
    		if(s.getKey().getValue().equals(snapKey))
    		{
	    		long size = (long)sizeMap.get(s.getDataKey());
	    		List<VirtualMachineFileLayoutExDiskLayout> diskList = s.getDisk();
	    		for(VirtualMachineFileLayoutExDiskLayout l : diskList)
	    		{
	    			List<VirtualMachineFileLayoutExDiskUnit> chains = l.getChain();
	    			for(VirtualMachineFileLayoutExDiskUnit c : chains)
	    			{
	    				 
	    				List<Integer> keys = c.getFileKey();
	    				for(Integer k : keys)
	    				{
	    					if(idx==0)
	    					{
	    						size += (long)sizeMap.get(k);
	    					}
	    					else
	    					{
		    					String name = (String)nameMap.get(k);
		    					if(name != null)
		    					{
			    					String postFix = padding("" + idx,6);
			    					if(name.indexOf(postFix  )>0)
			    					{
			    						size += (long)sizeMap.get(k);
			    					}
		    					}
	    					}
	    				}
	    			}
	    			
	    		}
	    		return 1.0f * size/1024/1024/1024;
    		}
    		idx++;
    		 
    	}
		throw new NotFoundException( "size of snapshot"); 
	}
	private String padding(String num, int len)
    {
    	String result = "";
    	for(int i = num.length(); i < len ; i++)
    	{
    		result += "0";
    	}
    	return result + num;
    }
}
