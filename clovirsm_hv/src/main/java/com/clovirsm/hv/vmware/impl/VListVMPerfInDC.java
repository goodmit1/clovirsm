package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.obj.VMPerfInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.*;

/**
 * 데이터 센터 내 호스트 속성 목록
 * @author 윤경
 *
 */
public class VListVMPerfInDC extends VListHostAttribute {
	Set<String> excludePath ;
	public VListVMPerfInDC(ConnectionMng m) {
		super(m);
		try {
			String[] path= HVProperty.getInstance().getStrings("disk_exclude_path");
			if(path != null)
			{
				excludePath = new HashSet();
				for(String p:path)
				{
					excludePath.add(p);
				}
			}
		} catch (Exception e) {
		 
			e.printStackTrace();
		}
	}
	@Override
	protected boolean isTarget(Map<String, Object> attribute) {
		Object role = attribute.get("config.ftInfo.role");
		if(role == null || NumberUtil.getInt(role)==1) return true; 
		return false;
	}
	@Override
	protected Map newAttr(){
		return new VMPerfInfo();
	}
	protected void addDiskInfo(ManagedObjectReference vm, Map newAttr ) throws Exception
	{
		VMPerfInfo perfInfo = (VMPerfInfo)newAttr;
		perfInfo.setVM_HV_ID(vm.getValue());
		perfInfo.setVM_NM((String)newAttr.get("NAME"));
		 VirtualMachinePowerState powerState = (VirtualMachinePowerState)newAttr.remove("POWERSTATE");
		 if(powerState != null)
		 {
			 perfInfo.setRUN_CD( powerState.value().equals("poweredOn") ? VMWareAPI.RUN_CD_RUN : VMWareAPI.RUN_CD_STOP);
		 }
		 ArrayOfVirtualDevice hdeviceList = (ArrayOfVirtualDevice)newAttr.remove("DEVICE");
		 long total = 0; 
		 int gpuSize = 0;
		 if(hdeviceList != null)
		 {	 
			 List<VirtualDevice> deviceList =  hdeviceList.getVirtualDevice();
			
	         for (VirtualDevice device : deviceList) {
	         
	        	 if(device instanceof VirtualDisk)
	        	 {
	        		 
	                total +=((VirtualDisk)device).getCapacityInKB();
	                 
	        	 }
	        	 else if(device instanceof VirtualPCIPassthrough ) {
	        		VirtualPCIPassthroughVmiopBackingInfo backing = (VirtualPCIPassthroughVmiopBackingInfo)((VirtualPCIPassthrough)device).getBacking();
	 				String summary = backing.getVgpu();
	 				//grid_p40-12q
	 				int pos = summary.lastIndexOf("grid_");
	 				if(pos>=0) {
	 					int pos1 = summary.lastIndexOf("-");
	 					String gmodel = summary.substring(pos+5, pos1);
	 					String q = summary.substring(pos1+1, summary.length()-1);
						perfInfo.setGPU_MODEL(   gmodel);
	 					gpuSize += Integer.parseInt( q) ;
	 				}
	 			}
	         }
		 }
		perfInfo.setGPU_SIZE(gpuSize);

         GuestInfo guest = (GuestInfo)newAttr.remove("GUEST");
		  List<GuestDiskInfo> diskList = guest.getDisk();
		  long gtotal = 0;
		  long free = 0;
		  for(GuestDiskInfo info : diskList)
		  {
			  //System.out.println(newAttr.get("NAME") + "'s disk="+ info.getDiskPath() + ":"  + info.getFreeSpace() + "/" + info.getCapacity());
			  if(excludePath != null  && excludePath.contains(info.getDiskPath())) continue;	
			  gtotal += info.getCapacity();
			  free += info.getFreeSpace();
		  }
		  long used = gtotal - free;
		  //newAttr.put("CPUMHZ", 1.0 * NumberUtil.getInt(newAttr.get("cpu_usagemhz")) / NumberUtil.getInt(newAttr.get("cpu_usage")) * 100 );
		  
		  
		  ManagedObjectReference host = (ManagedObjectReference)newAttr.remove("HOST");
		  if(host != null) {
			  perfInfo.setHOST_NM((String) super.getProp(host, "name"));
		  }
		  if(newAttr.get("ROLE") != null){
		  	perfInfo.setFT_YN("Y");
		  }
		  perfInfo.setDISK_SIZE((int) (1.0f * total/1024/1024));
		  perfInfo.setUSE_DISK_SIZE((int) (1.0f * used/1024/1024/1024));
		  //newAttr.put("ipAddress", guest.getIpAddress());
		  perfInfo.setGUEST_NM(  guest.getGuestFullName());
		 
		  if(total>0)
		  {
			  perfInfo.setDISK_USAGE(  1.0f * used / (total*1024) * 100 );
		  }

		  perfInfo.setCPU(NumberUtil.getInt(newAttr.get("NUMCPU"), 0));
          perfInfo.setRAM_SIZE((int) (NumberUtil.getInt(newAttr.get("MEMORYSIZEMB"), 0)/1024));

		 
	}
	
	protected void addPerfInfo(MonitorParam param, ManagedObjectReference host, Map newAttr) {
		super.addPerfInfo(param, host, newAttr);
		 
		try {
			addDiskInfo(host, newAttr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		  
	  }
	protected ManagedObjectReference getParent(String dc, String cluster) throws Exception
	{
		if(cluster == null)
		{
			return super.getDataCenter(dc);
		}
		return super.getCluster(dc, cluster);
	}
	 
	protected String getObjType()
	{
		return VMWareCommon.OBJ_TYPE_VM;
	}
	protected String[] attributes()
	{
		return new String[] {
				"summary.config.name" ,"summary.config.template", // template 여부
				"summary.config.memorySizeMB",
				"summary.config.numCpu" , "guest", "runtime.powerState", "runtime.host", "config.hardware.device", "config.ftInfo.role"
		};
	}
}
