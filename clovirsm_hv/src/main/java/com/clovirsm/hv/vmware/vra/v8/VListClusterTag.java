package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VListClusterTag  extends VRACommon8{

	public VListClusterTag(VRAConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result) throws Exception {
		JSONObject json = conn.getRestClient().getJSON("/provisioning/uerp/provisioning/mgmt/compute?expand&$filter=((type%20eq%20%27ZONE%27)%20or%20(type%20eq%20%27VM_HOST%27))");
		System.out.println(json.toString());
		JSONObject documents = json.getJSONObject("documents");
		Iterator it = documents.keys();
		List resultList = new ArrayList();
		while(it.hasNext()) {
			String key = (String) it.next();
			JSONObject document = documents.getJSONObject(key);
			JSONArray tags = document.getJSONArray("tags");
			
			String name = document.getString("name");
			
			String hostName = document.getJSONArray("endpoints").getJSONObject(0).getJSONObject("endpointProperties").getString("hostName");
			Map m = new HashMap();
			m.put("NAME", name);
			String datacenter = document.getString("regionId");
			int pos = datacenter.indexOf(":");
			m.put("DC_HV_ID", datacenter.substring(pos+1));
			if(tags.length()>0) {
				String tagKey = tags.getJSONObject(0).getString("key");
				String tagValue = tags.getJSONObject(0).getString("value");
				m.put("TAG", tagKey + ("".equals(tagValue)?"":":" + tagValue));
			}
			m.put("TYPE", document.getString("type"));
			m.put("DC_IP", hostName);
			resultList.add(m);
		}
		result.put(HypervisorAPI.PARAM_LIST, resultList);
		System.out.println(result);
		
	}

}
