package com.clovirsm.hv.vmware.nsx.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.nsx.NSXCommon;

public class VListEdge extends NSXCommon{

	public VListEdge(RestClient client) {
		super(client);
		 
	}

	@Override
	public void run(Map param, Map result, IAfterProcess after) throws Exception {
		 JSONObject json = super.get("/4.0/edges");
		 System.out.println(json);
		 JSONObject pagedEdgeList = json.getJSONObject("pagedEdgeList");
		 JSONObject edgePage = pagedEdgeList.getJSONObject("edgePage");
		 List list = new ArrayList();
		 if(edgePage.has("edgeSummary"))
		 {
			 JSONArray edgeSummaryList = edgePage.getJSONArray("edgeSummary");
			
			 for(int i=0; i < edgeSummaryList.length(); i++)
			 {
				 JSONObject edgeSummary = edgeSummaryList.getJSONObject(i);
				 Map m = new HashMap();
				 m.put("id", edgeSummary.getString("objectId"));
				 m.put("name", edgeSummary.getString("name"));
				 list.add(m);
				 
			 }
		 }
		 result.put(HypervisorAPI.PARAM_LIST, list);

		
	}

	public static void main(String[] args)
	{
		try {
		RestClient client = new RestClient("https://172.16.33.40","administrator@vsphere.local","VMware1!");
		
		//VListEdge edge = new VListEdge(client);
		VListTransportzone edge = new VListTransportzone(client);
		Map param = new HashMap();
		Map result = new HashMap();
		
			edge.run(param, result, null);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
}
