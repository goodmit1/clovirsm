package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.PerfCompositeMetric;
import com.vmware.vim25.PerfEntityMetricBase;
import com.vmware.vim25.PerfQuerySpec;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VGetAllVMMaxPerf extends VGetPerf{

	public VGetAllVMMaxPerf(ConnectionMng m) {
		super(m);
		 
	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		 
	 
		result.putAll((Map) counterInfo(super.getDataCenter(dc),  param.getMonitorParam()));
		return null;
		
	}
	protected Object counterInfo(ManagedObjectReference dcmor , MonitorParam param) throws Exception {
		Map<String,ManagedObjectReference > hosts =  connectionMng.getMOREF()
				.inContainerByType(dcmor, "HostSystem");
		PerfQuerySpec qSpec = new PerfQuerySpec();
		Date startDate =  param.getStartTime();
		Date finishDate = param.getFinishTime();
		qSpec.setStartTime(toXmlCal(startDate));
		qSpec.setEndTime(toXmlCal(finishDate));
		qSpec.setIntervalId(86400);
		Map<String, Map> result = new HashMap();
		Map<Integer, String> perfMap = getPerfIds(qSpec, com.clovirsm.hv.vmware.VMWareCommon.OBJ_TYPE_VM, (String[])param.get("PERF_IDS"));
		for(ManagedObjectReference host : hosts.values()) {
			qSpec.setEntity(host);
			ManagedObjectReference perfManager = super.connectionMng.getServiceContent().getPerfManager();
			PerfCompositeMetric pv = vimPort.queryPerfComposite(perfManager, qSpec);
			if(pv != null)
			{
			    
			    List<PerfEntityMetricBase> pembs = pv.getChildEntity();
			    System.out.println(pembs.size());
			    for(int i=0; pembs!=null && i< pembs.size(); i++)
			    {
			    	String vmId = pembs.get(i).getEntity().getValue();
			    	Map data = result.get(vmId);
			    	if(data == null) {
			    		data = new HashMap();
			    		result.put(vmId, data);
			    	}
			    	data.putAll((Map)this.displayValues(null, pembs.get(i), perfMap));
			    }
			}
		}
		return result;
				
	}
}
