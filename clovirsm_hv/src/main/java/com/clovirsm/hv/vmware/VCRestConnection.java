package com.clovirsm.hv.vmware;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.vra.VRAConnection;

// Vcenter rest client
public class VCRestConnection extends VRAConnection{
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		int pos = url.lastIndexOf("/");
		url = url.substring(0,  pos) + "/rest";
		
		client = new RestClient(url, userId,pwd);
		client.setMimeType("application/json");
		JSONObject res = (JSONObject) client.post("/com/vmware/cis/session", "{}");
		client.setAuthStr(null);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+ 30);
		expire =  cal.getTime() ;

		Map  header = new HashMap();
		header.put("vmware-api-session-id", res.getString("value"));
		client.setHeader(header);
	}
}
