package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfManagedObjectReference;
import com.vmware.vim25.ArrayOfVirtualDevice;
import com.vmware.vim25.ManagedObjectReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 템플릿 명 존재 여부 체크
 *
 * @author 윤경
 */
public class VChkTemplatePath extends com.clovirsm.hv.vmware.CommonAction {
    //Set<String> excludePath ;
    public VChkTemplatePath(ConnectionMng m) {
        super(m);
		/*try {
			String[] path= HVProperty.getInstance().getStrings("disk_exclude_path");
			if(path != null)
			{
				excludePath = new HashSet();
				for(String p:path)
				{
					excludePath.add(p);
				}
			}
		} catch (Exception e) {
		 
			e.printStackTrace();
		}*/
    }

    @Override
    protected ManagedObjectReference run1(String dc, HVParam param,
                                          Map result) throws Exception {
        return null;
    }

    public VMInfo run(String path1) throws Exception {
        VMInfo result = new VMInfo();
        ManagedObjectReference obj = super.getVM(path1);
        if (obj == null) {
            throw new NotFoundException(path1);
        }
        Map propValues = super.getProps(obj, "summary.config.guestFullName", "summary.config.numCpu", "summary.config.memorySizeMB", "config.hardware.device", "environmentBrowser", "runtime.host", "network");

        result.setGUEST_NM((String) propValues.get("summary.config.guestFullName"));
        result.setCPU(CommonUtil.getInt(propValues.get("summary.config.numCpu")));
        ManagedObjectReference host = (ManagedObjectReference) propValues.get("runtime.host");
        if (host != null) {
            result.put("HOST_HV_ID", host.getValue());
        }
        result.setRAM_SIZE(NumberUtil.getInt(propValues.get("summary.config.memorySizeMB")) / 1024);
        result.setDISK_SIZE((int) (VGetVM.getDiskSize((ArrayOfVirtualDevice) propValues.get("config.hardware.device")) / 1024 / 1024));
        List<ManagedObjectReference> vmNetwork = ((ArrayOfManagedObjectReference) propValues.get("network")).getManagedObjectReference();

        List network = new ArrayList();
        for (ManagedObjectReference n : vmNetwork) {

            network.add(n.getValue());
        }
        result.put("NETWORK", network);
        return result;
    }


}
