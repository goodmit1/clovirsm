package com.clovirsm.hv.vmware.vrops;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.YamlUtil;
import com.clovirsm.hv.vmware.log.VLogConnection;
import com.clovirsm.hv.vmware.vni.VNIConnection;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VROpsAPI extends CommonAPI {
	public static String getHVType() {
		 
		return "V.VROPS";
	}
	 
	 
	protected  String getConnectionName() {
		return VROpsConnection.class.getName();
	}
	protected Object doProcess(  Map param,  Class  cls) throws Exception
	{	
		VROpsConnection conn = null;
		try
		{
			//setLoginInfo(param);
			conn = (VROpsConnection) connect(param);
			Map result = new HashMap();
			 
			VROpsCommon action = (VROpsCommon)cls.getConstructor(VROpsConnection.class).newInstance(conn);
			return action.run(param);
			 
		}
		finally
		{
			disconnect(conn);
		}
	}
	public JSONArray getAllDCHeathInfo(  Map param) throws Exception {
		return (JSONArray) this.doProcess(param, GetAllDCHealth.class);
	}
	 
	
	
	
	public Map getResourceMetricKeys( Map param, Map allMetrics, String name, String vmId) throws Exception{
		 
		param.put("allMetrics",allMetrics);
		param.put("name",name);
		param.put("vmId",vmId);
		return (Map) this.doProcess(param, GetMetricKeys.class);
	}
	
	public Map getMetricData(Map param , String resId, String key, String intervalType, long begin, long end ) throws Exception {
		 
		 
		param.put("resId",resId);
		param.put("key",key);
		param.put("intervalType",intervalType);
		param.put("begin",begin);
		param.put("end",end);
		return (Map) this.doProcess(param, GetMetricData.class);
		
		
	}
	public Map listHostNode(Map param) throws Exception
	{
		 return (Map)doProcess(    param,  GetVROpsHostList.class );
	}
	public List<Map> getUnderOverSize(Map param, int day) throws Exception {
		
		 
		param.put("DAY", day);
		return (List<Map>) this.doProcess(param, GetUnderOverSizeVM.class);
			
		
		
	}


 

}
