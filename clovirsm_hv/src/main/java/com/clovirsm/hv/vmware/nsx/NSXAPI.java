package com.clovirsm.hv.vmware.nsx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareConnection;
import com.clovirsm.hv.vmware.log.VLogConnection;
import com.clovirsm.hv.vmware.nsx.impl.VCreateLogicalSwitch;
import com.clovirsm.hv.vmware.vrops.VROpsConnection;
import com.vmware.connection.ConnectionMng;

public class NSXAPI extends CommonAPI {

	public static String getHVType()
	{
		return "V.NSX";
	}
	protected  String getConnectionName() {
		return NSXConnection.class.getName();
	}
	 
	 
	public Map createLogicalSwitch(Map param) throws Exception
	{
		NSXConnection conn = (NSXConnection) connect(param);
		try {
			RestClient client = conn.getRestClient();
			VCreateLogicalSwitch action = new VCreateLogicalSwitch(client);
			Map result = new HashMap();
			action.run(param, result, null);
			return result;
		}
		finally {
			this.disconnect(conn);
		}
		
	}
	public Map listHostNode(Map param) throws Exception
	{
		NSXConnection conn = (NSXConnection) connect(param);
		try {
			JSONObject list = conn.getRestClient().getJSON("/api/v1/transport-nodes");
			JSONArray results = list.getJSONArray("results");
			List serverlist = new ArrayList();
			for(int i=0;  i < results.length(); i++) {
				JSONObject node_deployment_info = results.getJSONObject(i).getJSONObject("node_deployment_info");
				 
				if(node_deployment_info.has("fqdn")) {
					serverlist.add( node_deployment_info.getString("fqdn"));
				}
			}
			Map result = new HashMap();
			result.put(VMWareAPI.PARAM_LIST, serverlist);
			return result;
		}
		finally {
			this.disconnect(conn);
		}
		
	}
	public Map listDC(Map param) throws Exception
	{
		NSXConnection conn = (NSXConnection) connect(param);
		try {
			JSONObject list = conn.getRestClient().getJSON("/api/v1/fabric/compute-managers");
			JSONArray results = list.getJSONArray("results");
			List serverlist = new ArrayList();
			for(int i=0;  i < results.length(); i++) {
				serverlist.add(results.getJSONObject(i).getString("server"));
			}
			Map result = new HashMap();
			result.put(VMWareAPI.PARAM_LIST, serverlist);
			return result;
			
		}
		finally {
			this.disconnect(conn);
		}
		
	}
	 
}
