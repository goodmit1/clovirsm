package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VGuestRun extends com.clovirsm.hv.vmware.CommonAction{

	public VGuestRun(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception {
		return null;
	}
	public Map run(VMInfo vmInfo , String cmd, String args) throws Exception {
		Map result = new HashMap();
		this.runCmd(super.getVM(vmInfo.getVM_NM()), vmInfo.getOS_ADMIN_ID(), vmInfo.getOS_ADMIN_PWD(), cmd, args, result);
		return result;

	}
	protected Integer runCmd (ManagedObjectReference vm, String id, String pwd, String path, String args, Map result) throws Exception{
		NamePasswordAuthentication auth = new NamePasswordAuthentication();
		 
		auth.setUsername(id);
		auth.setPassword(pwd);
		GuestProgramSpec prog = new GuestProgramSpec();
		prog.setProgramPath(path);
		prog.setArguments(args);
		ManagedObjectReference mor = (ManagedObjectReference)super.getProp(super.connectionMng.getServiceContent().getGuestOperationsManager(), "processManager");
		
		long pid = super.vimPort.startProgramInGuest(mor, vm, auth, prog );
		List<Long> pids = new ArrayList();
		pids.add(pid);
		
		Integer exitCode = getExitCode(0 , mor, vm, auth, pids);
		if(result != null) {
			result.put("PID", pid);
			result.put("EXIT_CODE", exitCode);
		}
	
		return exitCode;
		
	}
	protected Integer getExitCode( int idx, ManagedObjectReference mor,  ManagedObjectReference vm, GuestAuthentication auth,List<Long> pids) throws Exception
	{
		if(idx>5) return null;
		Thread.sleep(1000); 
		List<GuestProcessInfo> pInfoList = super.vimPort.listProcessesInGuest(mor, vm, auth, pids );
		if(pInfoList.size()>0)
		{
			 Integer exitCode = pInfoList.get(0).getExitCode();
			 if(exitCode == null)
			 {
				 return getExitCode(++idx, mor, vm, auth, pids);
			 }
			 else
			 {
				 return exitCode;
			 }
		}
		return null;
	}

}
