package com.clovirsm.hv.vmware.vra.v8;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.json.JSONObject;

public class YamlCalc { 
	ScriptEngine js;
	String inputVar;
	static String  fun= null;
	public void init() throws Exception {
	 
		InputStream in = (this.getClass().getResourceAsStream("/vra8Js.js"));
		byte[] buf = new byte[1024];
		int len = 0;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		while((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		fun = new String(out.toByteArray());
	}
	public YamlCalc(JSONObject input) throws Exception{
		
		 
		js = new ScriptEngineManager().getEngineByName("javascript");
		if(fun==null) {
			init();
		}
		js.eval(fun);
		inputVar = "var input = " + input.toString() + ";"  ;
	}
	public  Object cal(String str ) throws Exception{
		int pos = str.indexOf("${");
		int pos1 = str.indexOf( "}", pos);
		String proc = str.substring(pos+2, pos1);
		
		return   js.eval(inputVar + proc) ;
	}

}
