package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.*;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.*;

/**
 * 디스크 추가
 * @author 윤경
 *
 */
public class VCreateDisk extends com.clovirsm.hv.vmware.CommonAction{



	public VCreateDisk(ConnectionMng m) {
		super(m);

	}
	public void run(ManagedObjectReference dcRef, VMInfo vmInfo, DiskInfo diskInfo) throws Exception{

	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		int sizeGB = CommonUtil.getInt(param.getDiskInfo().getDISK_SIZE()) ; //GB
		String vmName = param.getVmInfo().getVM_NM();
		ManagedObjectReference vmRef = super.getVM(param.getVmInfo().getVM_NM());
		
		ManagedObjectReference host = (ManagedObjectReference)getProp(vmRef,"runtime.host");
		String diskName = getDiskName(super.getDsInHost(host), (String[])param.getDiskInfo().getDATASTORE_NM_LIST() , vmName,sizeGB);
		result.put(VMWareAPI.PARAM_DISK_PATH, diskName);
		
		return doAction(vmRef,   diskName , sizeGB );
		
	}
	protected String getDiskName(Set availDSInHost, String[] dsNames, String vmNm, int sizeGB) throws Exception
	{
		 
		String dsNm = getAvailableDs(availDSInHost, dsNames, sizeGB);
		 
		return getVMDiskFileName(dsNm , vmNm);
		
	}
	 
	protected boolean isWaiting()
	{
		return false;
	}
	protected ManagedObjectReference doAction(ManagedObjectReference vmRef,   String diskName, int diskSizeGB ) throws Exception
	{
		String diskmode = "persistent";
		VirtualMachineConfigSpec vmConfigSpec = new VirtualMachineConfigSpec();
	 
		VirtualDeviceConfigSpec vdiskSpec = getDiskDeviceConfigSpec( vmRef,   diskName, diskSizeGB, diskmode);
		if (vdiskSpec != null) {
			List<VirtualDeviceConfigSpec> vdiskSpecArray =
					new ArrayList<VirtualDeviceConfigSpec>();
			vdiskSpecArray.add(vdiskSpec);
			vmConfigSpec.getDeviceChange().addAll(vdiskSpecArray);
			ManagedObjectReference tmor =
					vimPort.reconfigVMTask(vmRef, vmConfigSpec);
			if(!isWaiting())
			{
				return tmor;
			}
			else
			{
				super.getTaskResultAfterDone(tmor, this.getClass().getName() + "," + diskName);
				return null;
			}
		} else {
			return null;
		}
	} 
	List<Integer[]> getControllerKey(ManagedObjectReference vmMor)
			throws Exception {
		List<Integer[]> retVal = new ArrayList<Integer[]>();

		List<VirtualDevice> listvd =
				((ArrayOfVirtualDevice) getProp(vmMor, "config.hardware.device")).getVirtualDevice();

		Map<Integer, VirtualDevice> deviceMap =
				new HashMap<Integer, VirtualDevice>();
		for (VirtualDevice virtualDevice : listvd) {
			deviceMap.put(virtualDevice.getKey(), virtualDevice);
		}
		boolean found = false;
		for (VirtualDevice virtualDevice : listvd) {
			if (virtualDevice instanceof VirtualSCSIController) {
				VirtualSCSIController vscsic =
						(VirtualSCSIController) virtualDevice;
				int[] slots = new int[16];
				slots[7] = 1;
				List<Integer> devicelist = vscsic.getDevice();
				for (Integer deviceKey : devicelist) {
					if (deviceMap.get(deviceKey).getUnitNumber() != null) {
						slots[deviceMap.get(deviceKey).getUnitNumber()] = 1;
					}
				}
				for (int i = 0; i < slots.length; i++) {
					if (slots[i] != 1) {
						retVal.add(new Integer[]{vscsic.getKey(), i});
						 
						found = true;
						 
					}
				}
				if (found) {
					break;
				}
			}
		}

		if (!found) {
			throw new HypervisorException(HypervisorException.PROCESS_FAIL,
					"The SCSI controller on the vm has maxed out its "
							+ "capacity. Please add an additional SCSI controller");
		}
		return retVal;
	}
	protected VirtualDeviceConfigSpec getDiskDeviceConfigSpec(  ManagedObjectReference virtualMachine,   String diskName, int diskSizeGB, String diskmode) throws Exception {
		return getDiskDeviceConfigSpec(true,  virtualMachine,    diskName,diskSizeGB,diskmode );
	}
	 
    
	protected void setDiskMode(String diskType, String diskmode, VirtualDiskFlatVer2BackingInfo backinginfo)
	{
		switch (diskType) {
       
        case "thick":
            backinginfo
                    .setDiskMode(VirtualDiskMode.INDEPENDENT_PERSISTENT.value());
            backinginfo.setThinProvisioned(Boolean.FALSE);
            backinginfo.setEagerlyScrub(Boolean.FALSE);
             
            break;
      
        case "thin":
            
            backinginfo.setDiskMode(diskmode);
            backinginfo.setThinProvisioned(Boolean.TRUE);
            backinginfo.setEagerlyScrub(Boolean.FALSE);
           
            break;
        
        case "eagerzeroed":
        	backinginfo.setDiskMode(diskmode);
            backinginfo.setThinProvisioned(Boolean.FALSE);
            backinginfo.setEagerlyScrub(Boolean.TRUE);
            
            break;
        default:
       
            backinginfo
                    .setDiskMode(diskType);
            backinginfo.setThinProvisioned(Boolean.FALSE);
            backinginfo.setEagerlyScrub(Boolean.FALSE);
             
          
      
            break;
    }

	}
	public VirtualDisk getDiskByPath(List<VirtualDisk> disks,String path)
	{
		for(VirtualDisk d:disks)
		{
			VirtualDeviceFileBackingInfo backingInfo = (VirtualDeviceFileBackingInfo)d.getBacking();
			if(path.equals(backingInfo.getFileName()))
			{
				return d;
			}
		}
		return null;
	}
	
	List<Integer[]> getControllerKeyReturnArr = null;
	int diskCnt = 1;
	protected VirtualDeviceConfigSpec getDiskDeviceConfigSpec(boolean isNew,  ManagedObjectReference virtualMachine,   String diskName, int diskSizeGB, String diskmode) throws Exception {
		VirtualDeviceConfigSpec diskSpec = new VirtualDeviceConfigSpec();


		VirtualDisk disk = new VirtualDisk();
		VirtualDiskFlatVer2BackingInfo diskfileBacking =
				new VirtualDiskFlatVer2BackingInfo();
		 

		int ckey = 0;
		int unitNumber = 0;
		if(getControllerKeyReturnArr == null)
		{
			getControllerKeyReturnArr =	getControllerKey(virtualMachine);
		}
		if (!getControllerKeyReturnArr.isEmpty()) {
			Integer[] t = getControllerKeyReturnArr.remove(0);
			ckey = t[0];
			unitNumber = t[1];
		}
	
		diskfileBacking.setFileName(diskName);
		String diskType = HVProperty.getInstance().getProperty("vmware.disk_type", "thin");
		if(diskmode != null) setDiskMode(diskType, diskmode, diskfileBacking);
		
		disk.setControllerKey(ckey);
		disk.setUnitNumber(unitNumber);
		disk.setBacking(diskfileBacking);
		long size = 1024l * 1024 * diskSizeGB;
		disk.setCapacityInKB(size);
		disk.setKey(-1 * diskCnt++);

		diskSpec.setOperation(VirtualDeviceConfigSpecOperation.ADD);
		if(isNew) diskSpec.setFileOperation(VirtualDeviceConfigSpecFileOperation.CREATE);
		diskSpec.setDevice(disk);
		return diskSpec;
	}
	 

}
