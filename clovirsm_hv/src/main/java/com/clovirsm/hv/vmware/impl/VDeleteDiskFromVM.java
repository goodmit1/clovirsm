package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.List;
import java.util.Map;

/**
 * VM에 속한 디스크 삭제
 * @author 윤경
 *
 */
public class VDeleteDiskFromVM extends VCreateDisk{

	public VDeleteDiskFromVM(ConnectionMng m) {
		super(m);
	 
	}
	protected boolean isWaiting()
	{
		return true;
	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {

		String diskName =  param.getDiskInfo().getDISK_PATH();
		doAction(super.getVM(param.getVmInfo()),  diskName , -1  );
		return null; 
	}
	private boolean isSameName(String str1, String str2)
	{
		if(str1.equals(str2)) return true;
		int pos1 = str1.lastIndexOf("/");
		int pos2 = str2.lastIndexOf("/");
		return str1.substring(pos1).equals(str2.substring(pos2));
	}
	@Override
	protected VirtualDeviceConfigSpec getDiskDeviceConfigSpec(boolean isDestory,   ManagedObjectReference virtualMachine,   String diskName, int diskSizeByte, String diskmode) throws Exception {
		 VirtualDisk disk = null;
         List<VirtualDevice> deviceList =  ((ArrayOfVirtualDevice) getProp(virtualMachine,"config.hardware.device" )).getVirtualDevice();
         for (VirtualDevice device : deviceList) {
         
         	
             if (device instanceof VirtualDisk) {
            	 VirtualDeviceFileBackingInfo backingInfo = (VirtualDeviceFileBackingInfo)device.getBacking();
             	System.out.println(device.getDeviceInfo().getLabel() + ":" + backingInfo.getFileName());  
                 if (isSameName(diskName,backingInfo.getFileName())) {
                     disk = (VirtualDisk) device;
                     break;
                 }
             }
         }
         if (disk != null) {
        	 VirtualDeviceConfigSpec diskSpec = new VirtualDeviceConfigSpec();
             diskSpec.setOperation(VirtualDeviceConfigSpecOperation.REMOVE);
             if(isDestory) diskSpec.setFileOperation(VirtualDeviceConfigSpecFileOperation.DESTROY);
             diskSpec.setDevice(disk);
             return diskSpec;
         } else {
             System.out.println("No device found " + diskName);
             return null;
         }
	}
}
