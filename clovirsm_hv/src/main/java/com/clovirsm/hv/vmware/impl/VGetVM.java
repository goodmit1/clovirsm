package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.*;

/**
 * VM의 powerState와 IP목록 가져오기
 * @author 윤경
 *
 */
public class VGetVM extends com.clovirsm.hv.vmware.CommonAction
{
	Set excludePath = null;
	public VGetVM(ConnectionMng m) {
		super(m);

	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		ManagedObjectReference vm = super.getVM(param.getVmInfo());
		state(vm, (VMInfo)result);
		stateFull(vm,  (VMInfo)result);

		return null;

	}
	protected void state(ManagedObjectReference vmRef ,   VMInfo result) throws Exception
	{
		result.setVM_HV_ID(vmRef.getValue());
		Map<String, Object> propValues = super.getProps(vmRef, "runtime.powerState","summary.config.guestFullName", "guest.guestFullName",   "guest.net", "runtime.host");
		VirtualMachinePowerState  powerState = (VirtualMachinePowerState) propValues.get("runtime.powerState" );

		if(powerState != null)
		{
			result.setRUN_CD(powerState.value().equals("poweredOn") ? VMWareAPI.RUN_CD_RUN : VMWareAPI.RUN_CD_STOP);

		}
		String guestNm  = (String)propValues.get("guest.guestFullName");
		if(guestNm == null || "".equals(guestNm)) {
			guestNm= (String)propValues.get("summary.config.guestFullName" );
		}
		result.setGUEST_NM(guestNm );
		ManagedObjectReference host = (ManagedObjectReference)propValues.get("runtime.host");
		if(host!=null)
		{
			result.put("CPU_MHZ", super.getProp(host, "summary.hardware.cpuMhz"));
			result.put("HOST_HV_ID", host.getValue());
		}
		ArrayOfGuestNicInfo nicInfos = (ArrayOfGuestNicInfo)propValues.get("guest.net");
		result.setIP_LIST( getIPInfo(nicInfos));



	}
	protected void stateFull(ManagedObjectReference vmRef ,  VMInfo result) throws Exception
	{
		Map<String, Object> propValues = super.getProps(vmRef, "summary.config.annotation" , "summary.config.memorySizeMB",
				"summary.config.numCpu" , "config.hardware.device","guest.disk");
		if(propValues.size()==0) return;
		result.setCPU( NumberUtil.getInt( propValues.get("summary.config.numCpu")));
		result.setRAM_SIZE(  NumberUtil.getInt(propValues.get("summary.config.memorySizeMB"))/1024  );
		result.setDISK_SIZE((int) (this.getDiskSize((ArrayOfVirtualDevice) propValues.get("config.hardware.device"))/1024/1024));
		result.setCMT((String)propValues.get("summary.config.annotation"));
		result.put("DS_NM", getDSName(vmRef));
		getDiskList(result, (ArrayOfVirtualDevice) propValues.get("config.hardware.device"),(ArrayOfGuestDiskInfo) propValues.get("guest.disk")) ;
	}
	
	protected   String getDSName(ManagedObjectReference vm) throws Exception
	{
		ArrayOfManagedObjectReference  o = (ArrayOfManagedObjectReference ) getProp(vm, "datastore");
		List<ManagedObjectReference> dlist = o.getManagedObjectReference() ;
		
		if(dlist.size()>0)
		{
			return (String)super.getProp(dlist.get(0), "name");
		}
		return null;
	}
	protected   List<IPInfo> getIPInfo(ArrayOfGuestNicInfo nicInfos )
	{
		List<GuestNicInfo> list = nicInfos.getGuestNicInfo();
		List<IPInfo> ipInfos = new ArrayList<IPInfo>();
		Set unique = new HashSet();
		for(GuestNicInfo m : list)
		{
			IPInfo ipInfo = new IPInfo();
			List<String> ips = m.getIpAddress();
			//System.out.println(m.getDeviceConfigId() + ":" + ips);
			String ip = getIP4(ips);
			if(!ip.equals("") || !unique.contains(m.getDeviceConfigId()) )
			{
				ipInfo.macAddress = m.getMacAddress();
				ipInfo.nicId = m.getDeviceConfigId();
				ipInfo.ip = ip;

				ipInfos.add(ipInfo);
				unique.add(m.getDeviceConfigId() );
			}
		}
		return ipInfos;
	}

	
	/**
	 * KB로 resturn
	 * @param list1
	 * @return
	 */
	public static   long getDiskSize(ArrayOfVirtualDevice list1)
	{
		long total = 0;
		List<VirtualDevice> deviceList = list1.getVirtualDevice();
		for(VirtualDevice d : deviceList)
		{
			if(d instanceof VirtualDisk)
			{
				VirtualDisk disk = (VirtualDisk)d;
				total += disk.getCapacityInKB();
			}

		}
		return total;
	}

	public     void getDiskList (Map info, ArrayOfVirtualDevice list1, ArrayOfGuestDiskInfo diskInfos)
	{
		List<Map> result = new ArrayList();
		List<VirtualDevice> deviceList = list1.getVirtualDevice();
		 
        long used = getUsedDiskSize(  diskInfos);
        boolean isFirst = true; 
        int gpuSize = 0;
		for(VirtualDevice d : deviceList)
		{
			if(d instanceof VirtualDisk)
			{
				DiskInfo map = new DiskInfo();
				VirtualDisk disk = (VirtualDisk)d;
				VirtualDeviceFileBackingInfo backingInfo = (VirtualDeviceFileBackingInfo)disk.getBacking();
				map.setDISK_PATH( backingInfo.getFileName());
				
				
				long diskSize = Math.round(1.0* disk.getCapacityInKB()/1024/1024);
				map.setDISK_SIZE((int) diskSize);
				if(diskMountNameSize.size()>0) {
					map.setDISK_NM(    diskMountNameSize.remove(0));
				}
				 
				result.add(map);
			}
			else if(d instanceof VirtualPCIPassthrough ) {
				VirtualPCIPassthroughVmiopBackingInfo backing = (VirtualPCIPassthroughVmiopBackingInfo)((VirtualPCIPassthrough)d).getBacking();
				String summary = backing.getVgpu();
				//grid_p40-12q
				int pos = summary.lastIndexOf("grid_");
				if(pos>=0) {
					int pos1 = summary.lastIndexOf("-");
					String gmodel = summary.substring(pos+5, pos1);
					String q = summary.substring(pos1+1, summary.length()-1);
					info.put("GPU_MODEL", gmodel);
					gpuSize += Integer.parseInt( q) ;
				}
			}

		}
		info.put("GPU_SIZE", gpuSize);
		info.put("DISK_LIST", result);
	}

	public   Set getExcludePath()
	{
		if(excludePath == null)
		{
			excludePath = new HashSet();
			try {
				String[] path= HVProperty.getInstance().getStrings("disk_exclude_path");
				if(path != null)
				{
	
					for(String p:path)
					{
						excludePath.add(p);
					}
	
				}
			} catch (Exception e) {
	
				e.printStackTrace();
			}
		}
		return excludePath;
	}
	List<String> diskMountNameSize ;
	protected   long getUsedDiskSize(  ArrayOfGuestDiskInfo diskList1)
	{

		Set excludePath = getExcludePath();
		
		long gtotal = 0;
		long free = 0;
		List<GuestDiskInfo> diskList = diskList1.getGuestDiskInfo();
		diskMountNameSize = new ArrayList();
		for(GuestDiskInfo info : diskList)
		{
			//System.out.println(newAttr.get("NAME") + "'s disk="+ info.getDiskPath() + ":"  + info.getFreeSpace() + "/" + info.getCapacity());
			if(excludePath != null  && excludePath.contains(info.getDiskPath())) continue;	
			diskMountNameSize.add(info.getDiskPath());
			gtotal += info.getCapacity();
			free += info.getFreeSpace();
		}
		return  gtotal - free;
	}


}
