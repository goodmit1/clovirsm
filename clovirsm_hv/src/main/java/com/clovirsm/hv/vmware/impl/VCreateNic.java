package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 랜카드 추가
 * @author 윤경
 *
 */
public class VCreateNic extends CommonAction{

	public VCreateNic(ConnectionMng m) {
		super(m);

	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param,
                                          Map result) throws Exception {
		ManagedObjectReference vmRef = super.getVM(param.getVmInfo().getVM_NM());
		  int nextKey = createNic(vmRef
				  , (String)param.getVmInfo().get(VMWareAPI.PARAM_PORT_GROUP_NM)
				  );
		  result.put(VMWareAPI.PARAM_NIC_ID, nextKey);
		  return null;
	}
	private int getNextKey(ManagedObjectReference vmRef ) throws Exception
	{
		List<VirtualDevice> deviceList =  super.getHardwareDevice(vmRef);
		int maxKey = 0;
		for (VirtualDevice device : deviceList) {
			if (device instanceof VirtualEthernetCard) {
				int key = device.getKey();
				if(maxKey<key)
				{
					maxKey = key;
				}
				 
			}
		}
		return maxKey + 1;
	}
	private int createNic(ManagedObjectReference vmRef , String portgroupName  ) throws Exception
	{
		 
		VirtualDeviceConfigSpec nicSpec = new VirtualDeviceConfigSpec();
		VirtualEthernetCard nic = new VirtualVmxnet3();

		ManagedObjectReference dvspgMor =
				findObjectFromRoot( 
						VMWareAPI.OBJ_TYPE_DVP,portgroupName);


		DVPortgroupConfigInfo portgroupInfo = (DVPortgroupConfigInfo) getProp(dvspgMor,"config");
		ManagedObjectReference dvs = portgroupInfo.getDistributedVirtualSwitch();
		VMwareDVSConfigInfo switchInfo = (VMwareDVSConfigInfo) getProp(dvs,"config");
		if (switchInfo != null && portgroupInfo != null) {


			DistributedVirtualSwitchPortConnection vspc= new DistributedVirtualSwitchPortConnection();
			vspc.setSwitchUuid(switchInfo.getUuid());
			vspc.setPortgroupKey(portgroupInfo.getKey());
 
			VirtualEthernetCardDistributedVirtualPortBackingInfo nicBacking = new VirtualEthernetCardDistributedVirtualPortBackingInfo();
			nicBacking.setPort(vspc);

			 
			int nextKey = getNextKey( vmRef );
			nic.setKey(nextKey);
			nic.setAddressType("assigned");
			nic.setBacking(nicBacking);
			  nicSpec.setOperation(VirtualDeviceConfigSpecOperation.ADD);
			nicSpec.setDevice(nic);
			VirtualMachineConfigSpec vmConfigSpec = new VirtualMachineConfigSpec();

			List<VirtualDeviceConfigSpec> nicSpecArray =
					new ArrayList<VirtualDeviceConfigSpec>();
			nicSpecArray.add(nicSpec);
			vmConfigSpec.getDeviceChange().addAll(nicSpecArray);
			
			ManagedObjectReference task =
					vimPort.reconfigVMTask(vmRef, vmConfigSpec);
			getTaskResultAfterDone(task, "Add NIC:" + vmRef.getValue());
			return nextKey;
			 

		}
		else
		{
			throw new Exception("Switch not found");
		}
		
	}
	

}
