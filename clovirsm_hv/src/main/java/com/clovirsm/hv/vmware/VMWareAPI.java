package com.clovirsm.hv.vmware;

import com.clovirsm.hv.*;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.vmware.impl.*;
import com.clovirsm.hv.vmware.log.LogInsightAPI;
import com.clovirsm.hv.vmware.nsx.NSXAPI;
import com.clovirsm.hv.vmware.rest.GetVMTags;
import com.clovirsm.hv.vmware.sddc.SDDCAPI;
import com.clovirsm.hv.vmware.vni.VNIAPI;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.hv.vmware.vrops.VROpsAPI;
import com.vmware.connection.ConnectionMng;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VMWareAPI extends VMWareCommon implements HypervisorAPI{
	public static String GUEST_USER_ID_PREFIX="user_";


	public IAPI getAPI(String kubun) throws Exception {
		//VRNI,		VROPS,		VRLI
		if(kubun.equals("VRA")) {
			return getVRA();
		}
		if(kubun.equals("VRNI")) {
			return getVNI();
		}
		if(kubun.equals("VROPS")) {
			return getVROps();
		}
		if(kubun.equals("NSX")) {
			return getNSX();
		}
		if(kubun.equals("VRLI")) {
			return getVLI();
		}
		if(kubun.equals("VRLI")) {
			return getVLI();
		}
		if(kubun.indexOf("DB")>=0) {
			return new DBPoolAPI();
		}
		return new CommonAPI();

	}
	public LogInsightAPI getVLI() throws Exception {
		return (LogInsightAPI)HVProperty.getInstance().getOtherAPI(LogInsightAPI.getHVType(), LogInsightAPI.class);
	}
	public VRAAPI getVRA() throws Exception
	{
		return (VRAAPI)HVProperty.getInstance().getOtherAPI(VRAAPI.getHVType(), VRAAPI.class);
	}
	public VNIAPI getVNI() throws Exception
	{
		return (VNIAPI)HVProperty.getInstance().getOtherAPI(VNIAPI.getHVType(), VNIAPI.class);
	}
	public VROpsAPI getVROps() throws Exception
	{
		return (VROpsAPI)HVProperty.getInstance().getOtherAPI(VROpsAPI.getHVType(),VROpsAPI.class);
	}
	public NSXAPI getNSX() throws Exception
	{
		return (NSXAPI)HVProperty.getInstance().getOtherAPI(NSXAPI.getHVType(), NSXAPI.class);
	}

	@Override
	public Map getVMTags(HVParam param) throws Exception
	{
		VCRestConnection conn = null;
		try	{
			conn = connectRest(param.getConnParam());
			Map result = new HashMap();
			GetVMTags tags = new GetVMTags(conn);
			tags.run(param.getVmInfo(), result);
			return result;
		}
		finally {
			disconnect(conn);
		}

	}
	@Override
	public boolean chkTask(HVParam param, String taskId) throws Exception
	{
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param.getConnParam());
			VChkTask action = new VChkTask(connectionMgr);
			return action.run(taskId);
		}
		finally
		{
			disconnect(connectionMgr);
		}


	}

	@Override
	public Map deleteVM(HVParam param) throws Exception
	{

		return doProcess( param, VPowerOffVM.class,VDeleteObject.class);

	}
	@Override
	public void deleteImage(HVParam param) throws Exception
	{

		 deleteObject(VMWareCommon.OBJ_TYPE_VM, param.getVmInfo().getVM_NM() ,param.getConnParam());

	}
	@Override
	public Float getSnapshotSize(HVParam param, String snapshotName) throws Exception
	{
		VMWareConnection connectionMgr = null;

		try {
			connectionMgr = connect(param.getConnParam());
			VInfoSnapshotSize action = new VInfoSnapshotSize(connectionMgr);
			return (float)action.run(param , snapshotName);

		}
		finally {
			disconnect(connectionMgr);
		}


	}

	@Override
	public Map deleteSnapshot(HVParam param, String snapshot_nm) throws Exception
	{
		VMWareConnection connectionMgr = null;

		try {
			connectionMgr = connect(param.getConnParam());
			VDeleteSnapShot action = new VDeleteSnapShot(connectionMgr);
			action.run(param , snapshot_nm);
			return null;
		}
		finally {
			disconnect(connectionMgr);
		}

	}

	protected void deleteObject(String type, String name, Map param) throws Exception
	{
		ConnectionMng connectionMgr = null;
		try
		{
			connectionMgr = connectNewGuest(param);

			VDeleteObject action = new VDeleteObject(connectionMgr);

			action.run(type, name);

		}
		catch(Exception e)
		{
			disconnectNew(connectionMgr);
			throw e;
		}


	}
	@Override
	public Map powerOpVM(HVParam param, String op) throws Exception
	{
		Class cls = null;
		if(op.equals(POWER_ON))
		{
			cls = VPowerOnVM.class;
		}
		else if(op.equals(POWER_OFF))
		{
			cls = VPowerOffVM.class;
		}
		else if(op.equals(POWER_REBOOT))
		{
			cls = VRebootVM.class;
		}
		return doProcess( param,  cls);

	}
	@Override
	public Map openConsole(HVParam param) throws Exception
	{
		return doProcess(    param,   VOpenConsole.class);

	}

	protected Map doProcess(  HVParam param,   Class... classList) throws Exception
	{
		IConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param.getConnParam());

			Map result = new HashMap();
			for(Class cls : classList)
			{
				CommonAction action = (CommonAction)cls.getConstructor(ConnectionMng.class).newInstance(connectionMgr);
				if(param.getAfter() == null)
				{
					action.run(param, result);
				}
				else
				{
					action.run(param, result, param.getAfter());
				}
			}
			return result;
		}
		finally
		{
			if(param.getAfter() == null) disconnect(connectionMgr);
		}
	}




	@Override
	public Map reconfigVM(HVParam param) throws Exception
	{
		 return doProcess(param,  VReconfigVM.class);


	}

	@Override
	public List listVMDisk(HVParam param ) throws Exception
	{
		Map result = doProcess(param,   VListVMDisk.class);
		return (List)result.get(PARAM_LIST);

	}
	public List listAllPerfMetricInfo(HVParam param ) throws Exception
	{
		Map result = doProcess(param,   VGetAllPerfMetricInfo.class);
		return (List)result.get(PARAM_LIST);

	}
	@Override
	public Map createVM(HVParam param, boolean isRedeploy) throws Exception
	{
		return doProcess(  param, VCreateVM.class);



	}
	/*@Override
	public Map createVMFromTemplate(IBeforeAfter before, boolean isDCFirst, Map param) throws Exception
	{
		param.put(PARAM_FROM, "//" + param.get(PARAM_REAL_DC_NM) + "//vm//" + param.get(PARAM_FOLDER_NM) + "//" + param.get(PARAM_IMAGE_NM)) ;
		return createVM(before, isDCFirst,  param);
	}*/
	@Override
	public Map createImage(HVParam param) throws Exception
	{
		//param.put(PARAM_FROM,   info.getVM_NM()) ;
		return doProcess(param,VCreateImage.class);

	}
	@Override
	public Map addDisk(HVParam param) throws Exception
	{

		return doProcess(param, VCreateDisk.class);

	}
	@Override
	public Map mountDisk(HVParam param) throws Exception
	{
		return doProcess( param, VMountDisk.class);

	}

	@Override
	public Map revertVM(HVParam param, String snapshot_nm) throws Exception
	{
		return doProcess(param,VRevertVM.class);

	}
	@Override
	public Map renameSnapshot(HVParam param, String oldName, String newName) throws Exception
	{
		VMWareConnection connectionMgr = null;
		VMInfo result = new VMInfo();
		try
		{
			connectionMgr = connect(param.getConnParam());
			VRenameSnapshot vmaction = new VRenameSnapshot(connectionMgr);

			vmaction.run(param.getVmInfo(), oldName, newName);


			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}

	}
	@Override
	public VMInfo vmState(HVParam param) throws Exception
	{
		VMWareConnection connectionMgr = null;
		VMInfo result = new VMInfo();
		try
		{
			connectionMgr = connect(param.getConnParam());
			VGetVM vmaction = new VGetVM(connectionMgr);

			vmaction.run(param, result);


			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
	@Override
	public Map deleteDisk(HVParam param) throws Exception
	{
		Class cls = null;
		if(param.getVmInfo() == null)
		{
			cls = VDeleteDisk.class;
		}
		else
		{
			cls = VDeleteDiskFromVM.class;

		}
		return doProcess(  param,  cls);
	}

	@Override
	public void onFinishCreateVM(HVParam param) throws Exception {

	}

	@Override
	public Map createSnapshot(HVParam param, String snapshotNm) throws Exception {
		VMWareConnection connectionMgr = null;

		try {
			connectionMgr = connect(param.getConnParam());
			VCreateSnapshot action = new VCreateSnapshot(connectionMgr);
			action.run(param , snapshotNm);
			return null;
		}
		finally {
			disconnect(connectionMgr);
		}


	}
	@Override
	public Map addNic(HVParam param) throws Exception {

		return doProcess( param,  VCreateNic.class);
	}
	@Override
	public Map deleteNic(HVParam param, int nicId) throws Exception {

		VMWareConnection connectionMgr = null;

		try {
			connectionMgr = connect(param.getConnParam());
			VDeleteNic action = new VDeleteNic(connectionMgr);
			action.run(param , nicId);
			return null;
		}
		finally {
			disconnect(connectionMgr);
		}

	}
	@Override
	public void renameVM(HVParam param, String newName) throws Exception {
		VMWareConnection connectionMgr = null;

		try {
			connectionMgr = connect(param.getConnParam());
			VRename action = new VRename(connectionMgr);
			action.run(VMWareCommon.OBJ_TYPE_VM, param.getVmInfo().getVM_NM(), newName);
		}
		finally {
			disconnect(connectionMgr);
		}
	}

	@Override
	public void renameImg(HVParam param, String newName) throws Exception {
		this.renameVM(param, newName);
	}

	/*public Map copyVM(Map param ) throws Exception {
		param.put(PARAM_FROM, "//" + param.get(PARAM_REAL_DC_NM) + "//vm//" + param.get(PARAM_FOLDER_NM) + "//" + param.get(PARAM_IMAGE_NM)) ;
		return createVM(null, false, param);

	}*/
	@Override
	public Map unmount(HVParam param) throws Exception {
		return this.doProcess(param,   VUnMountDisk.class);
	}
	@Override
	public Map getVMPerf( HVParam param) throws Exception
	{
		return getPerf(VMWareCommon.OBJ_TYPE_VM, param);
	}
	@Override
	public Map getPerf(String type, HVParam param) throws Exception
	{
		String className = "com.clovirsm.hv.vmware.impl.VGetPerf" + type;

		return this.doProcess(param,   Class.forName(className));
	}

	@Override
	public List listObject(HVParam param ) throws Exception
	{

		Map result =  this.doProcess(param,   VListObject.class);
		return (List)result.get(HypervisorAPI.PARAM_LIST);
	}
	@Override
	public Map listAllVMMaxPerf( HVParam param) throws Exception
	{

		return this.doProcess(param,  VGetAllVMMaxPerf.class);
	}
	@Override
	public Map listPerfHistory( HVParam param) throws Exception
	{
		String type = getHVObjType(param.getMonitorParam().getTargetObjType());
		param.getMonitorParam().setTargetObjType( type);
		return this.doProcess(param,   VListPerfHistory.class);
	}
	@Override
	public int getFirstNicId() throws Exception {
		return HVProperty.getInstance().getInt("vmware_firstNic_id", 4000 );
	}
	@Override
	public Map listEvent(   HVParam param) throws Exception {

		String type = getHVObjType(param.getMonitorParam().getTargetObjType());
		param.getMonitorParam().setTargetObjType( type);
		return this.doProcess(param,   VListEvent.class);
	}
	@Override
	public Map listAlarm( HVParam param) throws Exception {


		String type = getHVObjType(param.getMonitorParam().getTargetObjType());
		param.getMonitorParam().setTargetObjType( type);
		return this.doProcess(param,   VListTriggeredAlarm.class);
	}

	@Override
	public Map listAllAlarm( HVParam param) throws Exception {
		String type =  VMWareCommon.OBJ_TYPE_DC;
		param.getMonitorParam().setTargetObjType(type);
		param.getMonitorParam().setTargetObjNm((String)param.getConnParam().get("REAL_DC_NM"));

		return this.doProcess(param,   VListTriggeredAlarm.class);
	}
	@Override
	public String[] getPerfHistoryTypes(String objType) throws Exception
	{
		String type =  getHVObjType(objType);
		return   HVProperty.getInstance().getStrings("vmware.perf.obj.history." + type);
	}
	@Override
	public String getHVObjType(String standardType) {
		switch(standardType)
		{
			case HypervisorAPI.OBJ_TYPE_DC:
				return VMWareCommon.OBJ_TYPE_DC;
			case HypervisorAPI.OBJ_TYPE_DS:
				return VMWareCommon.OBJ_TYPE_DS;
			case HypervisorAPI.OBJ_TYPE_CLUSTER:
				return VMWareCommon.OBJ_TYPE_CLUSTER;
			case HypervisorAPI.OBJ_TYPE_HOST:
				return VMWareCommon.OBJ_TYPE_HOST;
			case HypervisorAPI.OBJ_TYPE_VM:
				return VMWareCommon.OBJ_TYPE_VM;
			case HypervisorAPI.OBJ_TYPE_RP:
				return VMWareCommon.OBJ_TYPE_RP;
		}
		return standardType;
	}
	@Override
	public String getStandardObjType(String objType) {
		switch(objType)
		{
			case VMWareCommon.OBJ_TYPE_DC:
				return HypervisorAPI.OBJ_TYPE_DC;
			case VMWareCommon.OBJ_TYPE_DS:
				return HypervisorAPI.OBJ_TYPE_DS;
			case VMWareCommon.OBJ_TYPE_CLUSTER:
				return HypervisorAPI.OBJ_TYPE_CLUSTER;
			case VMWareCommon.OBJ_TYPE_HOST:
				return HypervisorAPI.OBJ_TYPE_HOST;
			case VMWareCommon.OBJ_TYPE_VM:
				return HypervisorAPI.OBJ_TYPE_VM;
			case VMWareCommon.OBJ_TYPE_RP:
				return HypervisorAPI.OBJ_TYPE_RP;
		}
		return objType;
	}
	@Override
	public VMInfo chkTemplatePath(HVParam param, String tmplPath) throws Exception {
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param.getConnParam());
			VChkTemplatePath action = new VChkTemplatePath(connectionMgr);
			return action.run(tmplPath);

		}
		finally
		{
			 disconnect(connectionMgr);
		}


	}
	@Override
	public Map getDSAttribute(HVParam param) throws Exception
	{
		return this.doProcess(param,   VListDSAttribute.class);

	}
	@Override
	public Map getVMPerfListInDC(HVParam param) throws Exception
	{
		return this.doProcess(param,  VListVMPerfInDC.class);

	}
	@Override
	public Map getVMIpListInDC(HVParam param) throws Exception
	{
		return this.doProcess(param,  VListVMIpInDC.class);

	}
	@Override
	public Map getHostAttribute(HVParam param) throws Exception
	{
		return this.doProcess(param,   VListHostAttribute.class);

	}
	@Override
	public Map listLicense(HVParam param) throws Exception
	{
		return this.doProcess(param,   VListLicense.class);

	}

	@Override
	public Map getVMListInDC(HVParam param) throws Exception {
		return this.doProcess(param,  VListVMInDC.class);
	}
	@Override
	public Map guestRun(HVParam param, String cmd, String cmdParam) throws Exception {
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param.getConnParam());
			VGuestRun vm = new VGuestRun(connectionMgr) ;

			return vm.run(param.getVmInfo(), cmd, cmdParam );

		}
		finally
		{
			disconnect(connectionMgr);
		}

	}
	@Override
	public void guestUpload(HVParam param, String svrPath, String localPath) throws Exception {
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param.getConnParam());
			VGuestFileUpload vm = new VGuestFileUpload(connectionMgr) ;

			vm.run(param.getVmInfo(), svrPath, localPath );
		}
		finally
		{
			disconnect(connectionMgr);
		}

	}
	@Override
	public void guestDownload(HVParam param, String svrPath, String localPath) throws Exception {
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param.getConnParam());
			VGuestFileDownload vm = new VGuestFileDownload(connectionMgr) ;

			vm.run(param.getVmInfo(), svrPath, localPath);
		}
		finally
		{
			disconnect(connectionMgr);
		}

	}
	@Override
	public void connectTest(Map  param) throws Exception {
		VMWareConnection connectionMgr   = new VMWareConnection();
		try {
			connectionMgr.connect((String)param.get(HypervisorAPI.PARAM_URL), (String)param.get(HypervisorAPI.PARAM_USERID), (String)param.get(HypervisorAPI.PARAM_PWD));
		}
		finally{
			disconnect(connectionMgr);
		}
	}
	@Override
	public Map moveVMFolder(HVParam param) throws Exception {
		 return this.doProcess(param,   VMoveVMIntoFolder.class);

	}
	public SDDCAPI getSDDC() throws Exception {
		return (SDDCAPI)HVProperty.getInstance().getOtherAPI(SDDCAPI.getHVType(), SDDCAPI.class);
	}
	public LogInsightAPI getVRLI() throws Exception {
		return (LogInsightAPI)HVProperty.getInstance().getOtherAPI(LogInsightAPI.getHVType(), LogInsightAPI.class);
	}
	@Override
	public Map createFw(HVParam param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map deleteFwSourceIp(HVParam param) throws Exception {
		return null;
	}

}
