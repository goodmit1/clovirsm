package com.clovirsm.hv.vmware.vni;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.SessionRestClient;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class VNISessionConnection implements IConnection{
	protected String url;
	String userId;
	String pwd;
	 
	protected SessionRestClient client;
	protected Date expire;
	protected String token;
	protected boolean connect = false;
	public SessionRestClient getRestClient()
	{
		return client;
	}
	 
	@Override
	public String getURL() {
		return url;
	}

 
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		 
		client = new SessionRestClient(url);
		client.setMimeType("application/json");
		 
		JSONObject json = new JSONObject();
		json.put("username",userId);
		json.put("password", pwd);
		json.put("domain", prop.get("domain")); 
		//json.put("domain","localdomain");
		JSONObject result = (JSONObject)client.post("/api/auth/login", json.toString());

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+ 30);
		expire =  cal.getTime() ;
		
		token = result.getString("csrfToken");
		Map header = new HashMap();
		header.put("x-vrni-csrf-token", token);
		client.setHeader(header);
		connect = true;
	}

	 

	@Override
	public boolean isConnected() {
		return client == null || expire.compareTo(new Date())>0;
		
	}

	 
	@Override
	public void disconnect() {
		client = null;
		
	}
}
