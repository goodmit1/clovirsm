package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.*;

/**
 * 성능 정보 가져오기
 * @author 윤경
 *
 */
public class VGetPerf extends VListPerfHistory{

	public VGetPerf(ConnectionMng m) {
		super(m);
		 
	}
	protected Object counterInfo(ManagedObjectReference vmRef ,  MonitorParam param) throws Exception {
		try {

			Map result = (Map) super.counterInfo(vmRef,   param);
			addDiskInfo(vmRef, param, result);
			addSpecInfo(vmRef, param,  result);
			return result;
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
				
	}
 
	protected void addSpecInfo(ManagedObjectReference vmRef, MonitorParam param, Map result) throws Exception{
		 
		
	}
	protected void addDiskInfo(ManagedObjectReference vmRef, MonitorParam param, Map result) throws Exception {
		 
		
	}
	protected String[] getPerfTypes(String objType) throws Exception 
	{
		String[] ids = HVProperty.getInstance().getStrings("vmware.perf.obj." + objType);
		if(ids==null)
		{
			throw new Exception(objType + "'s perf not found");
		}
		return ids;
	}
	@Override
	protected void setQSpeckExtraInfo(PerfQuerySpec qSpec) {
	 
		qSpec.setMaxSample(1);
	}

	/*
	@Override
	void setTimeInterval(PerfQuerySpec qSpec, String periodKubun) throws Exception
	{
		ManagedObjectReference perfManager = super.connectionMng.getServiceContent().getPerfManager();
		PerfProviderSummary perfProviderSummary = vimPort
				.queryPerfProviderSummary(perfManager, qSpec.getEntity());
		
		if (!perfProviderSummary.isCurrentSupported()) {
			qSpec.setIntervalId(300);
		} else {
			qSpec.setIntervalId(perfProviderSummary.getRefreshRate());
		}
		qSpec.setMaxSample(1);
	}
	*/
	private boolean isHourBetween(Date date, Date fromDate, Date toDate)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Calendar fromCal = Calendar.getInstance();
		fromCal.setTime(fromDate);
		Calendar toCal = Calendar.getInstance();
		toCal.setTime(toDate);
		return (cal.get(Calendar.HOUR_OF_DAY) > fromCal.get(Calendar.HOUR_OF_DAY)
				|| (cal.get(Calendar.HOUR_OF_DAY) == fromCal.get(Calendar.HOUR_OF_DAY)
					&& cal.get(Calendar.MINUTE)  >= fromCal.get(Calendar.MINUTE) ))
				&& (cal.get(Calendar.HOUR_OF_DAY)<toCal.get(Calendar.HOUR_OF_DAY)
				|| ((cal.get(Calendar.HOUR_OF_DAY)==toCal.get(Calendar.HOUR_OF_DAY)
					&& cal.get(Calendar.MINUTE)<=toCal.get(Calendar.MINUTE) )));
	}
	protected boolean isTargetTime(MonitorParam param , long date)
	{
		
		if(!param.isInnerHour() || param.getStartTime()==null || param.getFinishTime()==null)
		{
			return true;
		}
		else
		{
			return isHourBetween(new Date(date), param.getStartTime(), param.getFinishTime());
		}
	}
	@Override
	protected Object displayValues(MonitorParam param,List<PerfEntityMetricBase> values,Map<Integer, String> perfMap  ) {
		Map result = new HashMap();
		for(PerfEntityMetricBase v:values) {
			result.putAll((Map)displayValues(param, v, perfMap));
		}
		return result;
	}
	
	protected Object displayValues( MonitorParam param,  PerfEntityMetricBase metric,Map<Integer, String> perfMap  ) {
		Map result = new HashMap();
		 
			List<PerfMetricSeries> listperfmetser = ((PerfEntityMetric) metric
					 ).getValue();
			List<PerfSampleInfo> listperfsinfo = ((PerfEntityMetric) metric
					 ).getSampleInfo();
			if (listperfsinfo == null || listperfsinfo.size() == 0) {
				 
				return result;
			}
			result.put(VMWareAPI.PARAM_CRE_TIME,  listperfsinfo.get(0).getTimestamp().toGregorianCalendar().getTimeInMillis());
			for (int vi = 0; vi < listperfmetser.size(); ++vi) {

				if (listperfmetser.get(vi) instanceof PerfMetricIntSeries) {
					PerfMetricIntSeries val = (PerfMetricIntSeries) listperfmetser.get(vi);
					List<Long> listlongs = val.getValue();
					long total = 0;
					double max = 0;
					int count = 0;
					for(int j=0; j<listlongs.size(); j++)
					{
						long date = listperfsinfo.get(j).getTimestamp().toGregorianCalendar().getTimeInMillis();
						if(isTargetTime( param, date ))
						{	
							//System.out.println(new Date(date));
							long v = listlongs.get(j);
							if(v>max) max = v;
							total += v;
							count++;
						}
					}
					if(count==0) continue;		
					String metricName = perfMap.get(val.getId().getCounterId());
					
					double lval = 1.0 * total / count;
					
					if(divideTarget.contains(metricName))
					{
						lval= lval / 100 ;
						max = 1.0 * max / 100;
					}
					System.out.println(metricName  + "=" + listlongs.size()+ ":"+ total + "/" + count + "," + max);
					result.put(metricName.toUpperCase(), lval);
					result.put(metricName.toUpperCase() + "_MAX", max);
				}
			}
		 
		return result;
	}
}
