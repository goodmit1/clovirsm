package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 분산 포트 그룹 추가
 * @author 윤경
 *
 */
public class VCreateDVPortGroup extends com.clovirsm.hv.vmware.CommonAction{

	public VCreateDVPortGroup(ConnectionMng m) {
		super(m);

	}
	

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception {

		int vlanId = (int)param.getVmInfo().get(VMWareAPI.PARAM_VLAN_ID);
		String portName = (String)param.getVmInfo().get(VMWareAPI.PARAM_PORT_GROUP_NM);
		String cluster = (String) param.getVmInfo().get(VMWareAPI.PARAM_CLUSTER);
		addDVPortGroup(dc,cluster,(String)param.getVmInfo().get(VMWareAPI.PARAM_VIRTUALSWITCH_ID),portName,
				vlanId,
				result);
		return null;
	}



	protected void addDVPortGroup(String dcName, String cluster, String virtualswitchid, String portgroupname , int vlanId, Map result ) throws Exception
	{
		ManagedObjectReference dvsMor = findObjectFromRoot(VMWareAPI.OBJ_TYPE_DVS,virtualswitchid);

		if (dvsMor != null) {
			DVPortgroupConfigSpec portGroupConfigSpec =
					new DVPortgroupConfigSpec();
			VMwareDVSPortgroupPolicy policy = new VMwareDVSPortgroupPolicy();
			policy.setVlanOverrideAllowed(true);
			portGroupConfigSpec.setPolicy(policy);
			portGroupConfigSpec.setName(portgroupname);
			portGroupConfigSpec.setNumPorts(16);
			VmwareDistributedVirtualSwitchVlanIdSpec vlanspec = new VmwareDistributedVirtualSwitchVlanIdSpec();
			vlanspec.setVlanId(vlanId);
			vlanspec.setInherited(false);
			VMwareDVSPortSetting portSetting = new VMwareDVSPortSetting();
			portSetting.setVlan(vlanspec);
			portGroupConfigSpec.setDefaultPortConfig(portSetting);
			portGroupConfigSpec.setType("earlyBinding");

			List<DVPortgroupConfigSpec> listDVSPortConfigSpec =
					new ArrayList<DVPortgroupConfigSpec>();
			listDVSPortConfigSpec.add(portGroupConfigSpec);

			ManagedObjectReference taskmor =
					vimPort.addDVPortgroupTask(dvsMor, listDVSPortConfigSpec);

			getTaskResultAfterDone(taskmor, "CreateDVPortGroup:" + portgroupname);
		}

	}


}
