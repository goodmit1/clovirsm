package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;

/**
 * VM 시작
 * @author 윤경
 *
 */
public class VPowerOnVM extends com.clovirsm.hv.vmware.CommonAction {

	public VPowerOnVM(ConnectionMng m) {
		super(m);

	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {
		ManagedObjectReference vmMor = powerop(super.getVM(param.getVmInfo()), param.getVmInfo());
		onAfter(vmMor, result); 
		return null;

	}
	protected void onAfter(ManagedObjectReference vmMor,Map result) throws Exception
	{ 
	}
	protected ManagedObjectReference powerop(ManagedObjectReference vmMor, Map param) throws Exception {
	 
		if (vmMor != null) {
			param.put("VM_HV_ID", vmMor.getValue());
			 
			ManagedObjectReference taskmor =  process(vmMor);
			if (waiting && taskmor != null  ) {

				try
				{
					getTaskResultAfterDone(taskmor, "power operation : " + vmMor.getValue());
				}
				catch(Exception ignore)
				{
					//
				}
				 
			}
			
		}
		return vmMor;
	}

	 

	protected ManagedObjectReference process(ManagedObjectReference vmMor)
			throws Exception {
		return vimPort.powerOnVMTask(vmMor, null);
	}
	boolean waiting = true;
	public void setWaiting(boolean b) {
		this.waiting = b;
		
	}
}
