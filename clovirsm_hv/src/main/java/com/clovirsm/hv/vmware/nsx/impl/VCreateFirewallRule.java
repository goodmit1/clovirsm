package com.clovirsm.hv.vmware.nsx.impl;

import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.ParamObj;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.clovirsm.hv.vmware.nsx.NSXCommon;

public class VCreateFirewallRule extends NSXCommon {

	public VCreateFirewallRule(RestClient client) {
		super(client);
		 
	}

	@Override
	public void run(Map param, Map result, IAfterProcess after) throws Exception {
		
		/*<rule disabled="false" logged="false">
<name>AddRuleTest</name>
<action>allow</action>
<notes></notes>
<appliedToList>
<appliedTo>
<value>datacenter-26</value>
<type>Datacenter</type>
</appliedTo>
</appliedToList>
<sectionId>2</sectionId>
<sources excluded="true">
<source>
<value>datacenter-26</value>
<type>Datacenter</type>
</source>
</sources>
<services>
<service>
<value>application-216</value>
</service>
</services>
</rule>



<rule disabled=”false” logged=”true”>
<name>Test-Rule</name>
<action>ALLOW</action>
<sources excluded=”false”>
<source>
<value>10.112.1.1</value>
<type>Ipv4Address</type>
<isValid>true</isValid>
</source>
</sources>
<services>
<service>
<destinationPort>80</destinationPort>
<protocol>6</protocol>
<subProtocol>6</subProtocol>
</service>
</services>
</rule>
*/
		String sectionId = (String)param.get("fw_sectionId");
		ParamObj rule = new ParamObj("rule");
		rule.addAttr("disabled","false");
		rule.addAttr("logged","false");
		rule.addChild(new ParamObj("name", (String)param.get(HypervisorAPI.PARAM_OBJECT_NM)));
		rule.addChild(new ParamObj("action","allow"));

		ParamObj appliedToList = new ParamObj("appliedToList");
		rule.addChild(appliedToList);		
		ParamObj appliedTo = new ParamObj("appliedTo");
		appliedToList.addChild(appliedTo);
		appliedTo.addChild(new ParamObj("value",(String)param.get(VMWareCommon.PARAM_DC_OBJ_ID)));
		appliedTo.addChild(new ParamObj("type","Datacenter"));
	 
	 
		rule.addChild(new ParamObj("sectionId", sectionId));
		
		ParamObj sources = new ParamObj("sources");
		rule.addChild(sources);
		ParamObj source = new ParamObj("source");
		sources.addChild(source);
		source.addChild(new ParamObj("value", ""));
		source.addChild(new ParamObj("type", ""));
		
		ParamObj services = new ParamObj("services");
		rule.addChild(services);
		ParamObj service = new ParamObj("service");
		services.addChild(service);
		service.addChild(new ParamObj("value", ""));
		
		Object response = super.post("/4.0/firewall/globalroot-0/config/layer3sections/"+sectionId+"/rules", rule);
		
		
	}


}
