package com.clovirsm.hv.vmware.vra.v8;

import com.clovirsm.hv.vmware.vra.VRACommon;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public abstract class VRACommon8 extends VRACommon{

	public VRACommon8(VRAConnection conn) {
		super(conn);
		 
	}
	protected String apiVer() {
		return "2019-01-15";
	}

}
