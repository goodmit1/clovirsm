package com.clovirsm.hv.vmware;


import com.clovirsm.hv.obj.VMInfo;

import java.util.Map;

// vmware vm obj for create
public class VwareVMInfo extends VMInfo {

    public VwareVMInfo(Map paramMap) {
        super(paramMap);
    }

    public String getWINDOWS_CSI() {
        return (String) super.get(VMWareAPI.PARAM_WINDOWS_CSI);
    }
    public String getLINUX_CSI() {
        return (String) super.get(VMWareAPI.PARAM_LINUX_CSI);
    }

    public String getPORT_GROUP_NM() {
        return (String) super.get(VMWareAPI.PARAM_PORT_GROUP_NM);
    }

    public String getHOST_HV_ID() {
        return (String) super.get("HOST_HV_ID");
    }




}
