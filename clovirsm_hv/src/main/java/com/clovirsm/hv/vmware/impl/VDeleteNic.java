package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 랜카드 삭제
 * @author 윤경
 *
 */
public class VDeleteNic extends CommonAction{

	public VDeleteNic(ConnectionMng m) {
		super(m);

	}
	@Override
	protected ManagedObjectReference run1(String dc, HVParam param,
                                          Map result) throws Exception {

		return null;
	}
	public void run(HVParam param, int nicId) throws Exception {
		deleteNic(super.getVM(param.getVmInfo())
				, nicId);
	}
	private void deleteNic(ManagedObjectReference vmRef, int confKey) throws Exception
	{
		 
		VirtualDeviceConfigSpec nicSpec = new VirtualDeviceConfigSpec();
		VirtualEthernetCard nic = null;
		nicSpec.setOperation(VirtualDeviceConfigSpecOperation.REMOVE);
		List<VirtualDevice> hardwares = super.getHardwareDevice(vmRef);
		for (VirtualDevice device : hardwares) {
			if (device instanceof VirtualEthernetCard) {
				if (confKey == device.getKey()) {
					nic = (VirtualEthernetCard) device;
					break;
				}
			}
		}
		if (nic != null) {
			nicSpec.setDevice(nic);
			VirtualMachineConfigSpec vmConfigSpec = new VirtualMachineConfigSpec();

			List<VirtualDeviceConfigSpec> nicSpecArray =
					new ArrayList<VirtualDeviceConfigSpec>();
			nicSpecArray.add(nicSpec);
			vmConfigSpec.getDeviceChange().addAll(nicSpecArray);
			ManagedObjectReference task =
					vimPort.reconfigVMTask(vmRef, vmConfigSpec);
			getTaskResultAfterDone(task, "Delete NIC:" + vmRef.getValue() + "'" + confKey);
		} else {
			 

		}



		



	}
	

}
