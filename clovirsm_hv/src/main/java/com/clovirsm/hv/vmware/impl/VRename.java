package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

import java.util.Map;

/**
 * 이름 변경
 * @author 윤경
 *
 */
public class VRename  extends com.clovirsm.hv.vmware.CommonAction{

	public VRename(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result) throws Exception {
		return null;
	}


	public void run(String type, String oldName, String newName) throws Exception
	{

		rename(super.getObject(type, oldName), newName);
	}
	protected void rename(ManagedObjectReference obj, String newName) throws Exception
	{
		 
		ManagedObjectReference taskmor = vimPort.renameTask(obj, newName);
		getTaskResultAfterDone(taskmor,"Rename " + obj.getType()  +":" + newName);
	}


}
