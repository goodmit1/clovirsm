package com.clovirsm.hv.vmware.vni;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.SessionRestClient;

public class VNIConnection implements IConnection{
	protected String url;
	String userId;
	String pwd;
	 
	protected  RestClient client;
	protected Date expire;
	protected String token;
	protected boolean connect = false;
	public  RestClient getRestClient()
	{
		return client;
	}
	 
	@Override
	public String getURL() {
		return url;
	}

 
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		 
		client = new  RestClient(url);
		client.setMimeType("application/json");
		 
		JSONObject json = new JSONObject();
		json.put("username",userId);
		json.put("password", pwd);
		json.put("domain", prop.get("domain")); 
		JSONObject domain = new JSONObject();
		domain.put("domain_type","LOCAL");
		json.put("domain", domain);
		JSONObject res = (JSONObject)client.post("/api/ni/auth/token", json.toString());
		try {
			expire = new Date(res.getLong("expiry"));
		}
		catch(Exception e) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+ 30);
			expire =  cal.getTime() ;
		}
		
		
	 
		Map header = new HashMap();
		header.put("Authorization","NetworkInsight " +  res.getString("token"));
		client.setHeader(header);
		connect = true;
	}

	 

	@Override
	public boolean isConnected() {
		return client == null || expire.compareTo(new Date())>0;
		
	}

	 
	@Override
	public void disconnect() {
		client = null;
		
	}
}
