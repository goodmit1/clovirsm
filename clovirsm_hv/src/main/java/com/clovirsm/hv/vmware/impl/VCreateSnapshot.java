package com.clovirsm.hv.vmware.impl;

import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.obj.HVParam;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VirtualMachineSnapshotInfo;
import com.vmware.vim25.VirtualMachineSnapshotTree;

import java.util.List;
import java.util.Map;
/**
 * 스냅샷 추가
 * @author 윤경
 *
 */
public class VCreateSnapshot extends com.clovirsm.hv.vmware.CommonAction{

	public VCreateSnapshot(ConnectionMng m) {
		super(m);

	}

	@Override
	protected ManagedObjectReference run1(String dc, HVParam param, Map result)
			throws Exception {

		return null;
		
	}
	public Object run(HVParam param, String snapshotName) throws Exception {
		createSnapshot(  super.getVM(param.getVmInfo()), snapshotName, "");
		return null;
	}
	ManagedObjectReference traverseSnapshotInTree(
			List<VirtualMachineSnapshotTree> snapTree, String findName ) {
		ManagedObjectReference snapmor = null;
		if (snapTree == null) {
			return snapmor;
		}
		for (VirtualMachineSnapshotTree node : snapTree) {

			if (findName != null && node.getName().equalsIgnoreCase(findName)) {
				return node.getSnapshot();
			} else {
				List<VirtualMachineSnapshotTree> listvmst =
						node.getChildSnapshotList();
				List<VirtualMachineSnapshotTree> childTree = listvmst;
				snapmor = traverseSnapshotInTree(childTree, findName );
			}
		}
		return snapmor;
	}
	ManagedObjectReference getSnapshotReference(
			ManagedObjectReference vmmor,   String snapName) throws Exception{
		VirtualMachineSnapshotInfo snapInfo =
				(VirtualMachineSnapshotInfo) getProp(vmmor, "snapshot");
		ManagedObjectReference snapmor = null;
		if (snapInfo != null) {
			List<VirtualMachineSnapshotTree> listvmst =
					snapInfo.getRootSnapshotList();
			snapmor = traverseSnapshotInTree(listvmst, snapName );
			if (snapmor == null) {
				throw new NotFoundException( "snapshot:" + snapName);
			}
		} else {
			throw new NotFoundException( "snapshot:" + snapName);
		}
		return snapmor;
	}
	protected ManagedObjectReference createSnapshot(  ManagedObjectReference vmRef , String snapshotname, String description) throws Exception
	{
	 
	
		ManagedObjectReference taskMor =
				vimPort.createSnapshotTask(vmRef, snapshotname, description, false,
						false);
		return taskMor;
		 

	}
	
}
