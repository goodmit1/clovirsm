package test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Ignore;
import org.junit.Test;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.SessionRestClient;
import com.clovirsm.hv.vmware.vni.GetVNIHostList;
import com.clovirsm.hv.vmware.vni.VNIConnection;
import com.clovirsm.hv.vmware.vni.VNIGetFlows;

@Ignore
public class VRNI {

	@Test
	public void login() throws Exception {
		RestClient client = new RestClient("https://10.50.253.205/api/ni");
		client.setMimeType("application/json");
		JSONObject json = new JSONObject();
		json.put("username","admin@local");
		json.put("password","VMware1!");
		JSONObject domain = new JSONObject();
		domain.put("domain_type","LOCAL");
		json.put("domain", domain);
		JSONObject res = (JSONObject)client.post("/auth/token", json.toString());
		System.out.println("token: " + res.getString("token"));
		System.out.println(new Date(res.getLong("expiry")));
		Map header = new HashMap();
		header.put("Authorization","NetworkInsight " +  res.getString("token"));
		client.setHeader(header);
		//System.out.println(client.get("/api/ni/entities/vms?size=10"));
		header.put("x-vrni-csrf-token",  res.getString("token"));
		
		System.out.println(client.getJSON("/entities/hosts?size=10000"));
		//System.out.println(client.get("/api/search/query?searchString=home&timestamp=1575006509050&includeObjects=false&includeFacets=true&includeMetrics=false&includeEvents=false&startIndex=0&maxItemCount=10&dateTimeZone=%2B09%3A00&sourceString=USER&includeModelKeyOnly=false"));
	}
	@Test
	public void test() throws Exception {
		RestClient client = new RestClient("https://10.50.253.205");
		Map header = new HashMap();
		header.put("x-vrni-csrf-token", "r3Ob7W+uqlw2zFNm38kfyQ==");
		header.put("Authorization","NetworkInsight " + "r3Ob7W+uqlw2zFNm38kfyQ==");
		client.setHeader(header);
		System.out.println(client.get("/api/model/descriptions"));
		 
	}
	@Test
	public void getHosts() throws Exception {
		VNIConnection conn = new VNIConnection();
		Map param = new HashMap();
		param.put("domain", "localdomain");
		conn.connect("https://10.50.253.205", "admin@local", "VMware1!", param);
		GetVNIHostList host = new GetVNIHostList(conn);
		host.run(param, param);
		System.out.println(param);
	}
		
	@Test
	public void getFlow() throws Exception {
		VNIConnection conn = new VNIConnection();
		Map param = new HashMap();
		param.put("domain", "localdomain");
		conn.connect("https://vrni.goodmit.tech.kr", "admin@local", "VMware1!", param);
		VNIGetFlows flows = new VNIGetFlows(conn);
		param.put("VM_NM", "Cloud_vSphere_Machine_1-mcm760-122021388818");
		flows.run(param, param);
		System.out.println(param);
		Map flow = new HashMap();
		flow.putAll((Map)param.get("FLOW"));
		
		param.put("VM_NM", "VM1-SHK-databaseserver");
		//flows.run(param, param);
		
		//flow.putAll((Map)param.get("FLOW"));
		Iterator values = flow.values().iterator();
		while(values.hasNext()) {
			Object[] v =(Object[]) values.next();
			System.out.println("[\"" + v[0] + "\",\"" + v[1] + "\"," + v[2] + "],");
			
		}
		 
	}
	
	@Test
	public void parse() {
		try {
				/*
				/api/search/query
				searchString: plan vm where name like 'VM1-SHK-windowserver' in last 24 hours
				timestamp: 1574994330595
				timeRangeString:  at Now 
				includeObjects: false
				includeFacets: true
				includeMetrics: false
				includeEvents: false
				startIndex: 0
				maxItemCount: 10
				dateTimeZone: +09:00
				sourceString: USER
				includeModelKeyOnly: false" */
			JSONObject json = new JSONObject(new JSONTokener( new FileInputStream("C:\\workspace_clovir\\clovirsm_hv\\src\\test\\clovirsm\\vrni.json")));
			JSONObject applets = json.getJSONArray("applets").getJSONObject(0);
			JSONArray payload = applets.getJSONArray("applets").getJSONObject(2).getJSONArray("payload");
			for(int i=0; i < payload.length(); i++) {
				JSONArray fields = payload.getJSONObject(i).getJSONArray("fields");
				for(int j=0; j < fields.length(); j++) {
					JSONObject field = fields.getJSONObject(j);
					System.out.println(field.getString("name") + ":" + field.get("value"));
				}
			}
			JSONArray entities = applets.getJSONArray("applets").getJSONObject(0).getJSONArray("entities");
			entities.getJSONObject(0).getString("modelKey");
			json = new JSONObject(new JSONTokener( new FileInputStream("C:\\workspace_clovir\\clovirsm_hv\\src\\test\\clovirsm\\vrni_graph.json")));
			JSONArray  nodes = json.getJSONObject("graph").getJSONArray("nodes");
			Map nodeMap = new HashMap();
			for(int i=0; i < nodes.length(); i++) {
				JSONObject node = nodes.getJSONObject(i);
				nodeMap.put(node.get("id"),	node.getJSONObject("data").getString("name"));
			}
			JSONArray  edges = json.getJSONObject("graph").getJSONArray("edges");
			Map flowMap = new HashMap();
			for(int i=0; i < edges.length(); i++) {
				JSONObject edge = edges.getJSONObject(i);
				flowMap.put(nodeMap.get(edge.get("fromNodeId")),	nodeMap.get(edge.get("toNodeId")));
			}	
			System.out.println(flowMap);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
