CREATE OR REPLACE FUNCTION datediff( date1 TIMESTAMP,  date2 TIMESTAMP) RETURN NUMBER 
 IS
 	 
BEGIN
	if date2 IS NULL OR  date1 IS null  THEN
		RETURN -1;
	END IF;
	 
	RETURN CAST(date1 AS DATE) -  CAST(date2 AS DATE)   ;
 
END;

CREATE OR REPLACE FUNCTION fn_img_nm_by_lvl(  id1  varchar2, lvl int ) 
 RETURN varchar2  
 IS 
result1 VARCHAR(200) DEFAULT '';
max_lvl int ;

BEGIN
  
 SELECT max(LEVEL )  INTO max_lvl FROM NC_IMG_CATEGORY a 
			START WITH a.CATEGORY_ID=id1 CONNECT BY PRIOR a.PAR_CATEGORY_ID=a.CATEGORY_ID;

 SELECT CATEGORY_NM INTO result1
 	FROM (SELECT max_lvl+1-LEVEL AS level1 ,  a.CATEGORY_NM   FROM NC_IMG_CATEGORY a 
				START WITH 	a.CATEGORY_ID=id1 CONNECT BY PRIOR a.PAR_CATEGORY_ID=a.CATEGORY_ID
		) a WHERE 	level1 = lvl;				
RETURN result1;
END;

CREATE OR REPLACE FUNCTION fn_parent_dept_id_by_lvl(  team_cd1  varchar2, lvl int ) 
 RETURN varchar2  
 IS 
result1 VARCHAR(200) DEFAULT '';
max_lvl int ;

BEGIN
  
 SELECT max(LEVEL )  INTO max_lvl FROM FM_TEAM a 
			START WITH a.TEAM_CD=team_cd1 CONNECT BY PRIOR a.PARENT_CD=a.TEAM_CD;

 SELECT TEAM_CD INTO result1
 	FROM (SELECT max_lvl+2-LEVEL AS level1 ,  a.TEAM_CD   FROM FM_TEAM a 
					START WITH a.TEAM_CD=team_cd1 CONNECT BY PRIOR a.PARENT_CD=a.TEAM_CD
		) a WHERE 	level1 = lvl;				
RETURN result1;
END;

CREATE OR REPLACE FUNCTION GET_ALARM(DC_ID1 varchar2, OBJ_ID varchar2) RETURN varchar2 
IS 
	result1 varchar2(200) DEFAULT '';	
BEGIN
	SELECT LISTAGG(ALARM_NM, ',')  WITHIN GROUP(ORDER BY CRE_TIME) INTO result1 FROM NC_HV_ALARM  WHERE DC_ID=DC_ID1 AND TARGET_ID=OBJ_ID;
    RETURN result1;
END ;

CREATE OR REPLACE FUNCTION get_currentTimeMillis 
 RETURN NUMBER  
 IS
 
BEGIN
  
RETURN  to_number(sysdate - to_date('01-01-1970','dd-MM-YYYY')) * (24 * 60 * 60 * 1000) ;

END;

CREATE OR REPLACE FUNCTION get_ddic_nm(DD_ID1 varchar2, DD_VALUE1 VARCHAR2, LANG1 VARCHAR2, KO_NM VARCHAR2) 
RETURN  varchar2 
IS 
result1 varchar2(200) DEFAULT KO_NM;
BEGIN
	
if LANG1 IS NULL OR LANG1='ko' THEN
	if KO_NM is null then
	 	
		SELECT KO_DD_DESC INTO result1 FROM FM_DDIC WHERE DD_ID=DD_ID1 AND DD_VALUE = DD_VALUE1;
	end if;	
ELSE
	SELECT NM INTO result1 FROM FM_NL WHERE TABLE_NM='FM_DDIC' AND FIELD_NM = 'DD_VALUE' AND ID=concat(concat(DD_ID1, '.'), DD_VALUE1) AND LANG_TYPE=LANG1;
end if;
return result1;
END;

CREATE OR REPLACE FUNCTION get_img_category_ids(  CATEGORY_ID1  varchar2) 
 RETURN varchar2  
 IS result1 VARCHAR(200) DEFAULT ''; 
BEGIN
  
SELECT substr(CATEGORY_NMS,2) INTO result1 FROM (SELECT CATEGORY_ID, SYS_CONNECT_BY_PATH(CATEGORY_ID, ',') "CATEGORY_NMS"
	   FROM NC_IMG_CATEGORY
	   START WITH PAR_CATEGORY_ID = 0 
	   CONNECT BY PRIOR CATEGORY_ID = PAR_CATEGORY_ID
   ) a 
   WHERE CATEGORY_ID=CATEGORY_ID1;
RETURN result1;
END;

CREATE OR REPLACE FUNCTION get_img_category_nm(  CATEGORY_ID1  varchar2) 
 RETURN varchar2  
 IS result1 VARCHAR(200) DEFAULT ''; 
BEGIN
  
SELECT substr(CATEGORY_NMS,2) INTO result1 FROM (SELECT CATEGORY_ID, SYS_CONNECT_BY_PATH(CATEGORY_NM, '>') "CATEGORY_NMS"
	   FROM NC_IMG_CATEGORY
	   START WITH PAR_CATEGORY_ID = 0 
	   CONNECT BY PRIOR CATEGORY_ID = PAR_CATEGORY_ID
   ) a 
   WHERE CATEGORY_ID=CATEGORY_ID1;
RETURN result1;
END;

CREATE OR REPLACE FUNCTION get_next_approver ( Id1  decimal) RETURN varchar2 
 
IS
 
result1 VARCHAR2(200) DEFAULT '';
nm VARCHAR2(50);
CURSOR c1  IS
     SELECT USER_NAME
     FROM FM_USER a, NC_REQ_NEXT_APPROVER b
     WHERE a.USER_ID=b.APPROVER and b.REQ_ID=Id1 ; 
BEGIN
 




   FOR item IN c1
   LOOP
   	 result1  := concat (result1 , ',');
  	 result1 := CONCAT(result1,item.USER_NAME);
   END LOOP ;
  
   IF length(result1)>0 THEN 
   	result1 := substr(result1,2);
   END IF;
   
   RETURN result1;
END;

CREATE OR REPLACE FUNCTION get_org_nm(  team_cd1  varchar2) 
 RETURN varchar2  
 IS result1 VARCHAR(200) DEFAULT ''; 
BEGIN
  
select comp_nm into result1 from FM_COMPANY a, FM_TEAM b where a.COMP_ID=b.COMP_ID and b.TEAM_CD=team_cd1 ;
RETURN result1;
END;

CREATE OR REPLACE FUNCTION get_spec_nm( specId VARCHAR2, cpuCnt INTEGER, ramSize integer,   diskSize INTEGER,  unit VARCHAR2) RETURN  VARCHAR2 
 IS
 	result1 VARCHAR2(200) DEFAULT ''; 
BEGIN
	
	IF specId = '0' THEN
		return  concat(concat(concat(concat(concat(cpuCnt,  'vCore, '), ramSize) , 'GB RAM, '), CASE WHEN mod(diskSize, 1024)=0 THEN  concat(TO_char(diskSize/1024,'fm999,999'), 'TB ') ELSE concat(TO_char(diskSize,'fm999,999,999'),'GB ') end),'Disk');
	ELSE 	
		SELECT concat(concat(concat(concat(concat(concat(concat(SPEC_NM, '('), CPU_CNT), 'vCore, '), RAM_SIZE), 'GB RAM, '), 
		CASE WHEN diskSize>0 AND mod(diskSize, 1024)=0 THEN  concat(concat(concat( TO_char(diskSize/1024,'fm999,999'), 'TB ') ,DISK_TYPE_NM ),'Disk')
		when diskSize>0 THEN concat(concat(concat(TO_char(diskSize,'fm999,999,999'),'GB '),DISK_TYPE_NM ),'Disk') ELSE concat(NC_VM_SPEC.DD_FEE,'원/day')  END), ')')
		INTO result1 FROM NC_VM_SPEC,  NC_DISK_TYPE d  
		WHERE d.DISK_TYPE_ID=NC_VM_SPEC.DISK_TYPE_ID  AND SPEC_ID=specId;
	END IF;
	RETURN result1;

 
END;

CREATE OR REPLACE FUNCTION get_template_nm(os_id1 varchar2, from_id1 VARCHAR2) 
RETURN  varchar2 
IS 
result1 varchar2(200) DEFAULT '';
BEGIN
	
if from_id1 IS NULL THEN
	IF os_id1 is NULL THEN result1 := '';
	ELSE SELECT NC_OS_TYPE.OS_NM  into result1 FROM NC_OS_TYPE WHERE NC_OS_TYPE.OS_ID = os_id1;
	END IF;
ELSif from_id1 IS not NULL THEN
	IF SUBSTR(from_id1, 1, 1) = 'S' THEN SELECT NC_VM.VM_NM into result1 FROM NC_VM WHERE NC_VM.VM_ID = from_id1;
	ELSIF SUBSTR(from_id1, 1, 1) = 'G' THEN SELECT NC_IMG.IMG_NM into result1 FROM NC_IMG WHERE NC_IMG.IMG_ID = from_id1;
	END IF;
end if;
return result1;
END;

CREATE OR REPLACE FUNCTION get_vm_dd_fee( ID1 VARCHAR2 , DC_ID1 VARCHAR2 , CPU_CNT1 int, RAM_SIZE1 int,DISK_TYPE_ID1 varchar2, DISK_SIZE1 int    , SPEC_ID1 VARCHAR2 ) RETURN  int   
IS 
DISK_FEE int default 0;
SPEC_FEE int default 0;
EXTRA_DISK_SIZE int default DISK_SIZE1;
BEGIN
	
	 
  IF SPEC_ID1 = '0' THEN
  	
  	SELECT (SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD='C') * CPU_CNT1 + (SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD='M') *RAM_SIZE1+ (select DD_FEE from NC_DISK_TYPE b  where  b.DISK_TYPE_ID=NVL(DISK_TYPE_ID1,'1')) * DISK_SIZE1 INTO  SPEC_FEE FROM DUAL;
	RETURN SPEC_FEE;
  ELSIF  SPEC_ID1  is not null  then 
	select b.DD_FEE  , a.DD_FEE  , DISK_SIZE1 -  a.DISK_SIZE  
		into DISK_FEE, SPEC_FEE,EXTRA_DISK_SIZE from NC_VM_SPEC a, NC_DISK_TYPE b  where SPEC_ID=SPEC_ID1 and a.DISK_TYPE_ID=b.DISK_TYPE_ID	;
  else
  	select DD_FEE into DISK_FEE from NC_DISK_TYPE b, NC_DC a where a.IMG_DISK_TYPE_ID=b.DISK_TYPE_ID and a.DC_ID = DC_ID1;
  end if;	
  return SPEC_FEE + (EXTRA_DISK_SIZE*DISK_FEE);
  
  
END;

CREATE OR REPLACE FUNCTION get_vm_ip(  dc_id1  varchar2, vm_nm1 varchar2, vm_id1 varchar2) 
 RETURN varchar2  
 IS
 result1 VARCHAR(200) DEFAULT ''; 
 dc_id2 varchar(20) DEFAULT dc_id1;
 vm_nm2 varchar(200) DEFAULT vm_nm1;
BEGIN
  
if vm_nm1 is   null then 
	select VM_NM , DC_ID into vm_nm2, dc_id2 from NC_VM where VM_ID= vm_id1;
end if;

select   LISTAGG( IP, ',')  WITHIN GROUP(ORDER BY IP)   into result1 from NC_IP where DC_ID=dc_id2 and VM_NM = vm_nm2 ;
  
RETURN result1;
END;
