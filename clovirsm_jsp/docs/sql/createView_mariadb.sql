create or replace view v_nc_vm as select
    nc_vm.VM_ID as VM_ID,
    nc_vm.VM_NM as VM_NM,
    nc_vm.VM_NM as OLD_VM_NM,
    nc_vm.DC_ID as DC_ID,
    nc_vm.TEAM_CD as TEAM_CD,
    nc_vm.CMT as CMT,
    nc_vm.RUN_CD as RUN_CD,
    r.CUD_CD  as CUD_CD,
    r.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vm.INS_PGM as INS_PGM,
    nc_vm.INS_TMS as INS_TMS,
    nc_vm.INS_IP as INS_IP,
    nc_vm.INS_ID as INS_ID,
    nc_vm.FEE_TYPE_CD as FEE_TYPE_CD,
    nc_vm.OS_HH_FEE as OS_HH_FEE,
    nc_vm.SPEC_HH_FEE as SPEC_HH_FEE,
    nc_vm.OS_DD_FEE as OS_DD_FEE,
    nc_vm.SPEC_DD_FEE as SPEC_DD_FEE,
    nc_vm.SPEC_ID as SPEC_ID,
    nc_vm.SPEC_ID as OLD_SPEC_ID,
    nc_vm.OS_ID as OS_ID,
    nc_vm.OS_MM_FEE as OS_MM_FEE,
    nc_vm.SPEC_MM_FEE as SPEC_MM_FEE,
    nc_vm.FROM_ID as FROM_ID,
    nc_vm.DISK_TYPE_ID as DISK_TYPE_ID,
    nc_vm.DISK_TYPE_ID as OLD_DISK_TYPE_ID,
    get_template_nm(
        nc_vm.OS_ID,
        nc_vm.FROM_ID
    ) as TMPL_NM,
    nc_vm.CPU_CNT as OLD_CPU_CNT,
    nc_vm.RAM_SIZE as OLD_RAM_SIZE,
    nc_vm.DISK_SIZE as OLD_DISK_SIZE,
    'G' as OLD_DISK_UNIT,
    nc_vm.CPU_CNT as CPU_CNT,
    nc_vm.RAM_SIZE as RAM_SIZE,
    nc_vm.DISK_SIZE as DISK_SIZE,
    'G' as DISK_UNIT,
    nc_vm.STOP_DD_FEE as STOP_DD_FEE,
    nc_vm.STOP_HH_FEE as STOP_HH_FEE,
    nc_vm.STOP_MM_FEE as STOP_MM_FEE,
    nc_vm.FAIL_MSG as FAIL_MSG,
    nc_vm.INS_DT as INS_DT,
    nc_vm.VM_HV_ID as VM_HV_ID,
    nc_vm.LAST_USE_TMS as LAST_USE_TMS,
    nc_vm.PURPOSE as PURPOSE,
    nc_vm.GUEST_NM as GUEST_NM,
    nc_vm.CATEGORY as CATEGORY,
    nc_vm.P_KUBUN as P_KUBUN,
    nc_vm.USE_MM as USE_MM,
    nc_vm.NAS as NAS,
    nc_vm.DEL_YN as DEL_YN
from
    NC_VM nc_vm
  		left outer join NC_VM_REQ r on (nc_vm.VM_ID=r.VM_ID and r.APPR_STATUS_CD in ('R','W'));
  		
  		
  		 create or replace view v_nc_fw as 
select
    fw.APPR_STATUS_CD as APPR_STATUS_CD,
    fw.TASK_STATUS_CD as TASK_STATUS_CD,
    fw.CIDR as CIDR,
    fw.PORT as PORT,
    fw.VM_USER_YN as VM_USER_YN,
    fw.OLD_PORT as OLD_PORT,
    fw.OLD_VM_USER_YN as OLD_VM_USER_YN,
    fw.FW_USER_ID as FW_USER_ID,
    fw.VM_ID as VM_ID,
    fw.FW_ID as FW_ID,
    fw.CUD_CD as CUD_CD,
    fw.INS_DT as INS_DT,
    fw.INS_ID as INS_ID,
    fw.FAIL_MSG as FAIL_MSG,
    fw.SERVER_IP as SERVER_IP,
    fw.CMT as CMT,
    fw.USE_MM as USE_MM,
    fw.EXPIRE_DT as EXPIRE_DT,
    t.TEAM_CD as TEAM_CD,
    t.TEAM_NM as TEAM_NM,
    u.POSITION as POSITION,
    u.USER_NAME as USER_NAME,
    u.LOGIN_ID as LOGIN_ID,
    t2.TEAM_CD as INS_TEAM_CD,
    t2.TEAM_NM as INS_TEAM_NM,
    u2.POSITION as INS_POSITION,
    u2.USER_NAME as INS_NAME,
    u2.LOGIN_ID as INS_LOGIN_ID
from
    (
        (
            (
                (
                    (
                        (
                            select
                               r.APPR_STATUS_CD as APPR_STATUS_CD,
                               nc_fw.TASK_STATUS_CD as TASK_STATUS_CD,
                               nc_fw.CIDR as CIDR,
                               nc_fw.PORT as PORT,
                               nc_fw.VM_USER_YN as VM_USER_YN,
                               nc_fw.PORT as OLD_PORT,
                               nc_fw.VM_USER_YN as OLD_VM_USER_YN,
                               nc_fw.FW_USER_ID as FW_USER_ID,
                               nc_fw.VM_ID as VM_ID,
                               nc_fw.FW_ID as FW_ID,
                               r.CUD_CD as CUD_CD,
                               nc_fw.INS_DT,
                               nc_vm.INS_ID  as INS_ID,
                               nc_fw.FAIL_MSG as FAIL_MSG,
                                get_vm_ip(
                                    nc_vm.DC_ID,
                                    nc_vm.VM_NM,
                                   nc_fw.VM_ID
                                ) as SERVER_IP,
                               nc_fw.CMT as CMT,
                               nc_fw.USE_MM as USE_MM,
                                (
                                    case
                                        when isnull(
                                           nc_fw.USE_MM
                                        ) then null
                                        else(
                                           nc_fw.INS_TMS + interval nc_fw.USE_MM month
                                        )
                                    end
                                ) as EXPIRE_DT
                            from
                               nc_fw
                               left outer join nc_vm on (nc_fw.VM_ID=nc_vm.VM_ID)
                               left outer join nc_fw_req r on (nc_fw.FW_ID=r.FW_ID and r.APPR_STATUS_CD in ('R','W'))
                            where
								nc_fw.DEL_YN = 'N'
                                
                        )
                
                    ) fw
                left join fm_user u on
                    (
                        (
                            u.USER_ID = fw.FW_USER_ID
                        )
                    )
                )
            left join fm_user u2 on
                (
                    (
                        u2.USER_ID = fw.INS_ID
                    )
                )
            )
        left join fm_team t on
            (
                (
                    t.TEAM_CD = u.TEAM_CD
                )
            )
        )
    left join fm_team t2 on
        (
            (
                t2.TEAM_CD = u2.TEAM_CD
            )
        )
    );
    
    
    create or replace view v_nc_work as
      select
    a.VOLUME_ID as SVC_ID,
    'V' as SVC_CD,
    concat (concat( concat(a.CLAIM_NM,'('), concat(SIZE, SIZE_UNIT)), ')')  as SVC_NM,
    a.CUD_CD as CUD_CD,
    a.APPR_STATUS_CD as APPR_STATUS_CD,
    a.INS_ID as INS_ID,
    a.INS_DT as INS_DT,
    0 as FEE,
    'D' as FEE_TYPE_CD
from
    CM_APP_VOLUME_REQ a
    left outer join CM_APP_REQ r on (a.APP_ID=r.APP_ID and a.INS_DT=r.INS_DT)
    where   a.APPR_STATUS_CD = 'W' and r.APP_ID is null 
    union all
    select
    CM_APP_REQ.APP_ID as SVC_ID,
    'A' as SVC_CD,
    CM_APP_REQ.APP_NM as SVC_NM,
    CM_APP_REQ.CUD_CD as CUD_CD,
    CM_APP_REQ.APPR_STATUS_CD as APPR_STATUS_CD,
    CM_APP_REQ.INS_ID as INS_ID,
    CM_APP_REQ.INS_DT as INS_DT,
    0 as FEE,
    'D' as FEE_TYPE_CD
from
    CM_APP_REQ where   APPR_STATUS_CD = 'W'
union all select
    NC_VM_REQ.VM_ID as SVC_ID,
    'S' as SVC_CD,
    NC_VM_REQ.VM_NM as SVC_NM,
    NC_VM_REQ.CUD_CD as CUD_CD,
    NC_VM_REQ.APPR_STATUS_CD as APPR_STATUS_CD,
    NC_VM_REQ.INS_ID as INS_ID,
    NC_VM_REQ.INS_DT as INS_DT,
    (
        case
            when(
                NC_VM_REQ.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                NC_VM_REQ.VM_ID,
                NC_VM_REQ.DC_ID,
                NC_VM_REQ.CPU_CNT,
                NC_VM_REQ.RAM_SIZE,
                NC_VM_REQ.DISK_TYPE_ID,
                NC_VM_REQ.DISK_SIZE,
                NC_VM_REQ.SPEC_ID
            )
        end
    ) as FEE,
    NC_VM_REQ.FEE_TYPE_CD as FEE_TYPE_CD
from
    NC_VM_REQ where   APPR_STATUS_CD = 'W'
union all select
    nc_img_req.IMG_ID as SVC_ID,
    'G' as SVC_CD,
    nc_img_req.IMG_NM as SVC_NM,
    nc_img_req.CUD_CD as CUD_CD,
    nc_img_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_img_req.INS_ID as INS_ID,
    nc_img_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_img_req.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                nc_img_req.IMG_ID,
                nc_img_req.DC_ID,
                0,
                0,
                (
                    select
                        b.IMG_DISK_TYPE_ID
                    from
                        nc_dc b
                    where
                        (
                            b.DC_ID = nc_img_req.DC_ID
                        )
                ),
                nc_img_req.DISK_SIZE,
                null
            )
        end
    ) as FEE,
    nc_img_req.FEE_TYPE_CD as FEE_TYPE_CD
from
    nc_img_req where   APPR_STATUS_CD = 'W'
union all select
    nc_fw_req.FW_ID as SVC_ID,
    'F' as SVC_CD,
    concat( concat( concat( concat( CIDR, '->' ),  VM_NM), ':' ), ifnull( nc_fw_req.PORT, '' )) as SVC_NM,
    nc_fw_req.CUD_CD as CUD_CD,
    nc_fw_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_fw_req.INS_ID as INS_ID,
    nc_fw_req.INS_DT as INS_DT,
    0 as FEE,
    'F' as FEE_TYPE_CD
from
    nc_fw_req,nc_vm
    	 
where
   nc_fw_req.VM_ID=nc_vm.VM_ID and    APPR_STATUS_CD = 'W'
union all select
    nc_vra_catalogreq_req.CATALOGREQ_ID as SVC_ID,
    'C' as SVC_CD,
    concat( concat( r.CATALOG_NM, '/' ), nc_vra_catalogreq_req.PURPOSE ) as SVC_NM,
    nc_vra_catalogreq_req.CUD_CD as CUD_CD,
    nc_vra_catalogreq_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vra_catalogreq_req.INS_ID as INS_ID,
    nc_vra_catalogreq_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_vra_catalogreq_req.CUD_CD = 'D'
            ) then 0
            else nc_vra_catalogreq_req.PREDICT_DD_FEE
        end
    ) as FEE,
    'D' as D
from
    (
        nc_vra_catalogreq_req
    join nc_vra_catalog r
    )
where
    (
        r.CATALOG_ID = nc_vra_catalogreq_req.CATALOG_ID
    ) and     APPR_STATUS_CD = 'W';
  		
    
 create or replace view v_nc_req_detail as  
 select
    a.VOLUME_ID as SVC_ID,
    'V' as SVC_CD,
    concat (concat( concat(a.CLAIM_NM,'('), concat(SIZE, SIZE_UNIT)), ')')  as SVC_NM,
    a.CUD_CD as CUD_CD,
    a.APPR_STATUS_CD as APPR_STATUS_CD,
    a.INS_ID as INS_ID,
    a.INS_DT as INS_DT,
    0 as FEE,
    'D' as FEE_TYPE_CD
from
    CM_APP_VOLUME_REQ a
    left outer join CM_APP_REQ r on (a.APP_ID=r.APP_ID and a.INS_DT=r.INS_DT)
    where    r.APP_ID is null 
 union all   
 select
    CM_APP_REQ.APP_ID as SVC_ID,
    'A' as SVC_CD,
    CM_APP_REQ.APP_NM as SVC_NM,
    CM_APP_REQ.CUD_CD as CUD_CD,
    CM_APP_REQ.APPR_STATUS_CD as APPR_STATUS_CD,
    CM_APP_REQ.INS_ID as INS_ID,
    CM_APP_REQ.INS_DT as INS_DT,
    0 as FEE,
    'D' as FEE_TYPE_CD
from
    CM_APP_REQ
union all 
select
    NC_VM_REQ.VM_ID as SVC_ID,
    'S' as SVC_CD,
    NC_VM_REQ.VM_NM as SVC_NM,
    NC_VM_REQ.CUD_CD as CUD_CD,
    NC_VM_REQ.APPR_STATUS_CD as APPR_STATUS_CD,
    NC_VM_REQ.INS_ID as INS_ID,
    NC_VM_REQ.INS_DT as INS_DT,
    (
        case
            when(
                NC_VM_REQ.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                NC_VM_REQ.VM_ID,
                NC_VM_REQ.DC_ID,
                NC_VM_REQ.CPU_CNT,
                NC_VM_REQ.RAM_SIZE,
                NC_VM_REQ.DISK_TYPE_ID,
                NC_VM_REQ.DISK_SIZE,
                NC_VM_REQ.SPEC_ID
            )
        end
    ) as FEE,
    NC_VM_REQ.FEE_TYPE_CD as FEE_TYPE_CD
from
    NC_VM_REQ
union all select
    nc_img_req.IMG_ID as SVC_ID,
    'G' as SVC_CD,
    nc_img_req.IMG_NM as SVC_NM,
    nc_img_req.CUD_CD as CUD_CD,
    nc_img_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_img_req.INS_ID as INS_ID,
    nc_img_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_img_req.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                nc_img_req.IMG_ID,
                nc_img_req.DC_ID,
                0,
                0,
                (
                    select
                        b.IMG_DISK_TYPE_ID
                    from
                        nc_dc b
                    where
                        (
                            b.DC_ID = nc_img_req.DC_ID
                        )
                ),
                nc_img_req.DISK_SIZE,
                null
            )
        end
    ) as FEE,
    nc_img_req.FEE_TYPE_CD as FEE_TYPE_CD
from
    nc_img_req
union all select
    nc_fw_req.FW_ID as SVC_ID,
    'F' as SVC_CD,
    concat(
        concat(
            concat(
                concat(
                    nc_fw_req.CIDR,
                    '->'
                ),
                VM_NM 
            ),
            ':'
        ),
        ifnull(
            nc_fw_req.PORT,
            ''
        )
    ) as SVC_NM,
    nc_fw_req.CUD_CD as CUD_CD,
    nc_fw_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_fw_req.INS_ID as INS_ID,
    nc_fw_req.INS_DT as INS_DT,
    0 as FEE,
    'F' as FEE_TYPE_CD
from
    nc_fw_req, nc_vm
where 	nc_vm.VM_ID = nc_fw_req.VM_ID
        
union all select
    nc_vra_catalogreq_req.CATALOGREQ_ID as SVC_ID,
    'C' as SVC_CD,
    concat(
        concat(
            r.CATALOG_NM,
            '/'
        ),
        nc_vra_catalogreq_req.PURPOSE
    ) as SVC_NM,
    nc_vra_catalogreq_req.CUD_CD as CUD_CD,
    nc_vra_catalogreq_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vra_catalogreq_req.INS_ID as INS_ID,
    nc_vra_catalogreq_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_vra_catalogreq_req.CUD_CD = 'D'
            ) then 0
            else nc_vra_catalogreq_req.PREDICT_DD_FEE
        end
    ) as FEE,
    'D' as D
from
    (
        nc_vra_catalogreq_req
    join nc_vra_catalog r
    )
where
    (
        r.CATALOG_ID = nc_vra_catalogreq_req.CATALOG_ID
    )
    ;
    
    
    create or replace view v_nc_img as select
    nc_img.IMG_ID as IMG_ID,
    nc_img.IMG_NM as IMG_NM,
    nc_img.CMT as CMT,
    nc_img.DISK_SIZE as DISK_SIZE,
    nc_img.FEE_TYPE_CD as FEE_TYPE_CD,
    nc_img.HH_FEE as HH_FEE,
    nc_img.DD_FEE as DD_FEE,
    nc_img.MM_FEE as MM_FEE,
    nc_img.FROM_ID as FROM_ID,
    nc_img.DEL_YN as DEL_YN,
    nc_img.TEAM_CD as TEAM_CD,
    nc_img.DC_ID as DC_ID,
    nc_img.OS_ID as OS_ID,
    nc_img.INS_ID as INS_ID,
    r.CUD_CD as CUD_CD,
    r.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_img.TASK_STATUS_CD as TASK_STATUS_CD,
    nc_img.FAIL_MSG as FAIL_MSG,
    nc_img.USE_MM as USE_MM,
    nc_img.INS_DT as INS_DT,
    nc_img.CATEGORY as CATEGORY,
    nc_img.P_KUBUN as P_KUBUN,
    nc_img.PURPOSE as PURPOSE,
    nc_img.SPEC_ID as SPEC_ID
from
    nc_img
    	left outer join nc_img_req r on (nc_img.IMG_ID = r.IMG_ID and r.APPR_STATUS_CD in ('W','R'));
 