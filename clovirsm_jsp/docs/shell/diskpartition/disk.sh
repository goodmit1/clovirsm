#!/bin/bash
## diskinfos parameters must be contain the mountpoint and disk size information
## ex) orahome=100,tibadm=200(mountpoint=disksize,...)
diskinfos=$1
array_diskinfos=$(echo $diskinfos | tr "," "\n")
for diskinfo in $array_diskinfos
do
  input_mountpoint_tmp=""
  input_disksize=""
  input_mountpoint_tmp=$(echo $diskinfo | cut -f1 -d=)
  input_disksize=$(echo $diskinfo | cut -f2 -d=)
  if [[ $input_mountpoint_tmp == "" ]]
    then
      mkdir "/data"
      input_mountpoint="/data"
    else
     if [[ $input_mountpoint_tmp == /* ]]
     then
       input_mountpoint=$input_mountpoint_tmp
     else
       input_mountpoint="/"$input_mountpoint_tmp
     fi
     mkdir $input_mountpoint
   fi
   if [[ $input_mountpoint != "/" ]]
   then
     chmod -R 777 $input_mountpoint
   fi
   lsblk -io KNAME | grep sd | cut -c 1-3 | sort | uniq > /tmp/all_devices.txt
   blkid | grep sd | cut -c 6-8 | sort | uniq > /tmp/ex_devices.txt
   diff /tmp/all_devices.txt /tmp/ex_devices.txt | grep sd | cut -c 3-5 | sort | uniq > /tmp/new_devices.txt
   lsblk -io KNAME,SIZE | grep $input_disksize | cut -c 1-3 | sort > /tmp/size_matched_devices.txt
   size_matched_new_devices=$(grep -wFf /tmp/new_devices.txt /tmp/size_matched_devices.txt)
   size_matched_new_device=$(echo $size_matched_new_devices | cut -f1 -d' ')
   new_fs="/dev/"$size_matched_new_device
   parted -s $new_fs mklabel gpt mkpart primary 1 100%
   mkfs -F -t ext4 $new_fs"1"
   mount $new_fs"1" $input_mountpoint
   /usr/bin/cp -f /etc/fstab /etc/fstab.org
   echo "$new_fs"1"      $input_mountpoint  ext4      defaults,nofail   0       2" >> /etc/fstab
   df -h
   file -s $new_fs"1"
 done
 rm -rf /tmp/all_devices.txt
 rm -rf /tmp/ex_devices.txt
 rm -rf /tmp/new_devices.txt
 rm -rf /tmp/size_matched_devices.txt
