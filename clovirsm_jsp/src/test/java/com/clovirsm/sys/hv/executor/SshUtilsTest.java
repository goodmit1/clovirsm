package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.util.SshUtils;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class SshUtilsTest {
    SshUtils sshUtils;

    @BeforeEach
    public void beforeSshUtils() {
        sshUtils = new SshUtils();
    }

    @Test
    void createSsh() throws JSchException {
        //GIVE
        Map<String, String> sshKey = new HashMap();
        //WHEN
        sshKey = sshUtils.createSsh();

        //THEN
        assertThat(sshKey.get("PRIVATE_KEY"), notNullValue());
    }

    @Test
    public void changePrivateKeyToPublicKey() {
        //GIVE

        String userName = "clovir";
        String privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIIEowIBAAKCAQEAq1m93EVU4vcI1msVYRM93AuKamT0iMzvtrgRwUs9j7O25ZQ0\n" +
                "XAz6tHqW5AhY0xUxEATztULVckKFe3eKCCRXklmUYVI6h69SQpJ0/OqIqgPCaLd+\n" +
                "4umV/VWRvjyIWbCY9lh96cxTB2O1JIOTsXBC9UAdDFQ+zRTN12Xcmfioro6bPzBN\n" +
                "BC4N7YT+ABVpn5xoeXJSPjLMSb8nQH4dSQT42qulebURHEe3tPvyEz/K0UBmm3B4\n" +
                "BUwXu2bHnDLFrBHimOdpg13F5WfbNNK//sfnxGYJ9gQohk3jXY4qF14F3UfRCOfN\n" +
                "FzSWusJ+YmoJc6XntIgOb9hRRUJtKELwCE9bPQIDAQABAoIBAEPlxp7u9ikNfHoC\n" +
                "arFRv5RfGlT4dhFzK5W6sIZoexdjPhkQtgK3vewgG4R2bkQORKe1XqC3v/6Lr3ry\n" +
                "Lkb1p6G9T1eVDs5PQYhRonvJr6e236VA+M2Pg7ij3KGGOP1ZRfNvs00egtQNxWyX\n" +
                "msGX22B1LE2DYHNBSCOPIG/+oVCxWXfKvSRuUliwlqIP8SB2GPxdgdHipKmijqsb\n" +
                "ygrDnqRLi6d5O2oa6xJBQoO6jPEXrqGhhck8xnZVTkW/R1rJaNKu1EvgFbzaYe4S\n" +
                "yQ6JZVV8cwy2v70b0CKsMVQH6rTphHT4hRXDhVlVeozmcpV2Y3YXvn4+Oo+EZNK3\n" +
                "cFR4T4ECgYEA1rwKS7SitLzzSWFlBWBR+oJ0oH868vhbLi2rENwnNK5EMFggxCf0\n" +
                "HhC5+y/8WHl+OxSpkiImZ/SCFwrDzW9nR6b/DJDMj+Ny/Lzb7fopSH+oag15CHLm\n" +
                "tTE6kcoJWv1AsHgYB0BqKKjHkFHY9HSquo3YufoHAvZ2ntZhr9+8+J0CgYEAzEdo\n" +
                "UeW+pCs6EMjvMcfXG3qogB4Nj3q+BZ23wqcwYaT+ey8JVbR0MtJrQu6HyPAs3+7V\n" +
                "40R4A1JV8PWdLDUMcGCqKO4lqj4q3OgUt9eVkGJdqZjap4jiV7gtbNOVSeULFiGV\n" +
                "xzdh5UYDDdK6ZEi39L/Qyq+tKKw9Wxb04xnJ2yECgYEAhRimVQhQdTBMp2YLxIpQ\n" +
                "JbKocTaLJkBT28qIpnmef2OodkKZhJuzDtOVQyEaCwi6+gmzlRU/st11qKyOco4j\n" +
                "eOEEYxh5ApKbGhZBRz7wSbR+gcV6CIeXgiVSRTzyEYi5iOfXFN+TLUJ393cTv2l0\n" +
                "zQ7Qj21u3SJAofEFOYq3SW0CgYB1TVfhF11Gpi/crZV0731dIS0bDKbayi/Nx8ha\n" +
                "tWi52WXsVJE2rr2+5UeEkrZUM16BZINb0thbh+8jw9ZyKTAwY8GyaTYlsTcpzPcA\n" +
                "roIK/AEuv0BnzdmzZ2ZmRIVIfobEOZrpX6sOXgqo6GXx6jQADhmWGhUD8wv0f5U1\n" +
                "x3w7IQKBgDQchLjtD95obZ0fNgVYU7STz/pFwY2R8djvj3V3hKO/nHkdPRQbsmx9\n" +
                "64isFSCR1k0/ELTJDqvWEJA5FD6aYO8lPyA5SW4iaTjIC3FfRkFQvbg7V7gRFsbC\n" +
                "gaJx70343W0pqbMZ0zAwe9QE8/sdDYrmP0DYqUznaZ+0wAIM/fZ9\n" +
                "-----END RSA PRIVATE KEY-----\n";
        //WHEN
        String publicKey = sshUtils.createPublicKey(privateKey, userName);
        System.out.println("publicKey = " + publicKey);
        //THEN
        assertThat(publicKey, containsString("rsa"));
    }
}