package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.CommonUtil;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertThat;

class ShellAddUserTest {

    ShellAddUser shellAddUser = null;

    @BeforeEach
    void beforeShellAddUser() {
        shellAddUser = new ShellAddUser();
    }


    @Test
    public void changeLinuxParam() throws IOException {
        //GIVE
        String inputCmd = CommonUtil.readStream2String( Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("shell/UserAdd.sh")), "utf-8");

        //WHEN
        Map<String, Object> cmdParam = new HashMap<>();
        cmdParam.put("USER_NAME", "fliconz");
        cmdParam.put("PUBLIC_KEY", "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCPlkNZlSUUTV7GO8BcVIVUqD0znrYegU0z\n" +
                "qxl2NuqqHXUsIE2b1wRHr5vE4mWI+jB//qsJUbOx/Coid3etuXeE6x3FsxlLiovx\n" +
                "NBR6iEhwEAdazMgVwRmJV83lFvdA6G7s/7Yx931w5a9bt/8/McisFGhlueOG0xDr\n" +
                "RBQtMROhK8P70FF26DEes3h5maZE5fmEz8nH/00GEH/W8B9fBsaT70xG787sHZd5\n" +
                "BZMQiHike+KsfaJKdhEHj747RDwuoZ8lJJtKke/iX0hywODPNsdhDvzLR6bfVC8d\n" +
                "Tn0czgnTUrtO4lnmWwWNmLVLI9jDzUR3Jt4V8VrZpp1CM/2Ai7sf fliconz");
        String shellCMD = CommonUtil.fillContentByVar(inputCmd, cmdParam);

        //THEN
        assertThat(shellCMD, containsString("fliconz"));
        assertThat(shellCMD, containsString("ssh-rsa"));
    }
}