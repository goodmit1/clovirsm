package fliconz_admin_jsp_sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.RestClient;
 

public class SearchTest {

	@org.junit.Test
	public void vmList() {
		RestClient client = new RestClient("http://10.150.0.4:9200");
		client.setMimeType("application/json");
		JSONObject json = new JSONObject();
		json.put("size", 10000);
		List list = new ArrayList();
		try {
			JSONObject result = (JSONObject)client.post("/vmlist/_search", json.toString());
			JSONArray hits = result.getJSONObject("hits").getJSONArray("hits");
			for(int i=0; i < hits.length(); i++) {
				JSONObject source = hits.getJSONObject(i).getJSONObject("_source");
				Map map = CommonUtil.converMap(source );
				list.add(map);
			}
			System.out.println(list);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
}
