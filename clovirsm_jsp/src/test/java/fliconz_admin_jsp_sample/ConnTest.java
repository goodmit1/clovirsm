package fliconz_admin_jsp_sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class ConnTest {

	public static void main(String[] args) {
		ConnTest test = new ConnTest();
		test.test();
	}
	
	public void test() {
		  CloseableHttpClient httpClient = HttpClients.createDefault();

		  HttpGet httpGet = new HttpGet("http://localhost/login/sso.jsp");
		try {
			CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
			httpGet = new HttpGet("http://localhost//admin/notice/index.jsp");
			httpResponse = httpClient.execute(httpGet);
			Header[] headers = httpResponse.getAllHeaders();
			for(Header header:headers) {
				System.out.println(header.getName() + ":" + header.getValue());
			}
			System.out.println(httpResponse.getEntity().getContentLength());
			InputStream  content =       httpResponse.getEntity().getContent();
	        byte[] buf = new byte[1024];
	        int idx = 0;
	        int total = 0;
	        while((idx = content.read(buf)) >=0){
	        	total += idx;
	        	System.out.println(total + "," + idx);
	        }
	        /*
	        BufferedReader reader = new BufferedReader(new InputStreamReader(
	                httpResponse.getEntity().getContent()));
	 
	        String inputLine;
	        StringBuffer response = new StringBuffer();
	 
	        while ((inputLine = reader.readLine()) != null) {
	            response.append(inputLine);
	        }
	        
	        reader.close();
	 
	        //Print result
	        System.out.println(response.toString());
	        */
	        httpClient.close();
 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	        


		  
	}
}
