package com.clovirsm.site;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.clovirsm.service.site.SiteService;
import com.clovirsm.service.workflow.ApprovalService;
import com.clovirsm.service.workflow.DefaultNextApprover;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.security.UserVO;

public class GMobisNextApprover extends DefaultNextApprover{
	@Autowired ApprovalService service;
	@Override
	public void onAfterDeny(Map param) throws Exception {
		setInfo(param);
		System.out.println(param);
		service.sendMail(param.get("INS_ID"), "mail_title_req_deny", "req_deny", param);
		 
	}
	@Override
	public void onAfterDeploy(Map param,  List<Map> details)  throws Exception {
		String[] steps = super.getSteps();
		if(TextHelper.indexOf(steps, DefaultNextApprover.ROLE_POTAL_MANAGER)<0) {
			setInfo(param);
			 
			System.out.println(param);
			List<Object> adminIds = super.getMemberByRole(DefaultNextApprover.ROLE_POTAL_MANAGER, param);
			for(Object id : adminIds) {
				if(UserVO.getUser().getUserId().equals(id)) continue;
				service.sendMail(id, "mail_title_deploy", "deploy", param);
			}
		}
	}
 
	
	private void setInfo(Map param) throws Exception {
		if(param.get("REQ_NM")==null) {
			Map info = service.getInfo(param);
			info.put("REQ_USER_NAME", info.get("USER_NAME"));
			param.putAll(info);
		}else if(param.get("REQ_USER_NAME")==null){
			param.put("REQ_USER_NAME", param.get("_USER_NM_"));
		}
	}
}
