package com.clovirsm.site.hynix;

import java.sql.Connection;
import java.util.Map;

import com.clovirsm.service.site.GroupService;
import com.clovirsm.site.mobis.ConnectionFactory;

public class DeptUserCopy extends com.clovirsm.site.mobis.DeptUserCopy{
	protected Connection getConnection(GroupService groupService, Map param) throws Exception {
		 
		param.put("CONN_TYPE", "USER_DB");
		Map connInfo = groupService.selectOneByQueryKey("com.clovirsm.site","getUserDBInfo", param);
		return ConnectionFactory.connection(  (String)connInfo.get("CONN_URL"), (String)connInfo.get("CONN_USERID"), (String)connInfo.get("CONN_PWD"));
//
	}
}
