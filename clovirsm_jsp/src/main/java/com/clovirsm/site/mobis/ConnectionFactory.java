package com.clovirsm.site.mobis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.clovirsm.common.ClassHelper;
import com.clovirsm.hv.DBConnection;

public class ConnectionFactory {

    public static Connection connection( String url, String id, String pwd ) throws  Exception{
    	return DBConnection.getDBConnection(url, id, pwd);
    }

    public static void main(String[] args) {
    	String type = null;
    	String url = "jdbc:mysql://10.150.0.4:3306/clovircm?useUnicode=true&amp;characterEncoding=utf8&amp;serverTimezone=UTC&amp;autoReconnect=true&amp;";
    	int pos = url.indexOf(":", 6);
    	type = url.substring("jdbc:".length(), pos);
    	System.out.println(type);
    }
}
