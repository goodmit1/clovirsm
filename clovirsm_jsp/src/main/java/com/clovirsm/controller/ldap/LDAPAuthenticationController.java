package com.clovirsm.controller.ldap;

import com.clovirsm.service.site.GMobisLDAPService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.SSOHelper;
import jdk.nashorn.api.scripting.ScriptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

@RestController
@RequestMapping(value = "/ldap")
public class LDAPAuthenticationController extends DefaultController {

    @Autowired
    GMobisLDAPService ldapService;

    @Override
    protected DefaultService getService() {
        return null;
    }

    @RequestMapping(value = "/login")
    public void login(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map requestParam = ControllerUtil.getParam(request);
        Map responseParam = ldapService.getLDAPResponse(requestParam);

        sendRedirect(request, response, responseParam);
    }

    private void sendRedirect(HttpServletRequest request, HttpServletResponse response, Map responseParam) throws Exception {
        if (responseParam.isEmpty()){
            response.sendError(500,"Users not registered with LDAP");
        }
        ldapService.close();
        SSOHelper.ssoLogin(responseParam.get("LOGIN_ID").toString(), "", request, response);
    }
}
