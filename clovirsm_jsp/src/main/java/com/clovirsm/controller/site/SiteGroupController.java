package com.clovirsm.controller.site;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

 
import com.clovirsm.service.site.GroupService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/site_group" )
public class SiteGroupController  extends DefaultController {

	@Autowired GroupService service;
	@Override
	protected GroupService getService() {
		 
		return service;
	}

}
