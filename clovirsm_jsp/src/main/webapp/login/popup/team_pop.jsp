<%--
  Created by IntelliJ IDEA.
  User: kimhk
  Date: 2021-06-10
  Time: 오전 11:25
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<style>
    .team_dialog {
        width: 800px;
    }
    .modal-footer-button {
        width: 90px;
        height: 25px;
        font-size: 13px;
        outline: none;
        border: none;
        border-radius: 4px;
        color: white;
        background-color: #00AC4F;
        cursor: pointer;
    }
    .tree-button-group {
        float: right;
    }
</style>

<div class="modal fade" id="team_modal" tabindex="-1" aria-labelledby="teamModalLabel" aria-hidden="true">
    <div class="modal-dialog team_dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="teamModalLabel"><spring:message code="label_select_team" text="부서 선택"/></h2>
                <div style="border-bottom: #BEBEBE solid 1px; margin: 10px 0 0 0;"></div>
            </div>
            <div class="modal-body" style="padding-top: 0;">
                <div class="tree-button-group">
                    <a href="#" onclick="searchTree()"><i class="fa fa-search"></i></a>
                    <a href="#" onclick="expendAllTree()"><i class="fa fa-plus-square-o"></i></a>
                    <a href="#" onclick="closeAllTree()"><i class="fa fa-minus-square-o"></i></a>
                </div>
                <div id="jstree" class="tree"></div>
            </div>
            <div class="modal-footer">
                <input type="button" class="modal-footer-button" value="<spring:message code="no_select" text="선택없음"/>" onclick="closeModal()" >
                <input type="button" class="modal-footer-button" value="<spring:message code="btn_reload" text="새로고침"/>" onclick='refreshTree();' >
            </div>
        </div>
    </div>
</div>

<script>
    var data = JSON.stringify({ _COMP_ID_: "102216547406718", USE_YN: "Y" });
    var teamData = new Array();
    $.ajax({
        type: 'POST',
        url: '/api/team/list?USE_YN=Y',
        data: data,
        async: false,
        beforeSend : function(xhr){
            xhr.setRequestHeader("Content-type","application/json;charset=UTF-8");
        },
        success: function (data) {
            $.each(data.list, function (idx, item){
                teamData[idx] = {
                    'id': item.TEAM_CD,
                    'parent': item.PARENT_CD === "0" ? "#" : item.PARENT_CD,
                    'text': item.TEAM_NM,
                }
            })
        }
    });
    $("#jstree").jstree({
        "ui": { "theme_name": "checkbox" },
        "core": {
            "data": teamData,
            "themes": { "icons": false },
            "initially_open": ["allNode"],
        },
        "plugins": ["search", "checkbox", "ui", "themes"],
        "checkbox": { three_state: false },
        "search": {
          "show_only_matches": true,
          "show_only_matches_children": true,
        },
    }).bind("select_node.jstree", function (event, data) {
        var { text, id } = data.instance.get_node(data.selected);
        closeModal();
        $("#team").val(text);
        $("#TEAM_CD").val(id);
    });

    function closeModal() {
        $("#team_modal").modal("hide");
    }

    function expendAllTree() {
        $("#jstree").jstree("open_all");
    }

    function closeAllTree() {
        $("#jstree").jstree("close_all");
    }

    function searchTree() {
        var input = prompt(msg_search_keyword, "");
        $("#jstree").jstree("search", input);
    }

    function refreshTree() {
        $('#jstree').jstree("deselect_all");
    }


</script>
