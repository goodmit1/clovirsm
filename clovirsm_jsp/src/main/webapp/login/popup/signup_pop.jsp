<%--
  Created by IntelliJ IDEA.
  User: kimhk
  Date: 2021-06-08
  Time: 오전 8:31
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--사용자 등록: ID, 이름, PW, 이메일, 부서--%>
<style>
    .modal-dialog {
        width: 450px;
    }

    .sign-item {
        margin-bottom: 15px;
    }

    .form-label {
        font-weight: bold;
    }

    h2 {
        font-weight: bold;
    }

    .submit-block .submit-button {
        width: 100%;
        height: 30px;
        display: flex;
        align-content: center;
        justify-content: center;
        flex-wrap: wrap;
        font-size: 16px;
        outline: none;
        border: none;
        border-radius: 4px;
        color: white;
        background-color: #00AC4F;
        font-weight: 600;
        cursor: pointer;
        padding-left: 1rem;
        padding-right: 1rem;
    }

    .pwd-valid {
        margin: 5px 0 0 0;
        color: #EA4C46;
        visibility: hidden;
        background-color: white;
    }

    label[required]:after {
        content: " *";
        color: red;
    }

    .input-box {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .team-button {
        width: 34px;
        height: 34px;
        border: 1px solid #ccc;
        border-bottom-right-radius: 4px;
        border-top-right-radius: 4px;
    }

    .team-input {
        border-right: none;
        border-bottom-right-radius: 0;
        border-top-right-radius: 0;
    }
</style>
<div class="modal fade" id="signin_Modal" tabindex="-1" aria-labelledby="sigiInModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="sigiInModalLabel"><spring:message code="label_signup" text="회원 가입"/></h2>
                <div style="border-bottom: #BEBEBE solid 1px; margin: 10px 0 0 0;"></div>
            </div>
            <div class="modal-body">
                <!-- Login -->
                <form action="/api/user/save" id="signin-form" method="POST">
                    <div class="mb-3 sign-item">
                        <label for="name" class="form-label" required><spring:message code="label_userName"
                                                                                      text="이름"/></label>
                        <input type="text" class="form-control" name="USER_NAME" id="name"
                               placeholder="<spring:message code="label_userName" text="이름"/>">
                    </div>
                    <div class="mb-3 sign-item">
                        <label for="userId" class="form-label" required><spring:message code="label_userId"
                                                                                        text="아이디"/></label>
                        <input type="text" class="form-control" name="LOGIN_ID" id="userId"
                               placeholder="<spring:message code="label_userId" text="아이디"/>">
                    </div>
                    <div class="mb-3 sign-item">
                        <label for="email" class="form-label"><spring:message code="label_notice_receive_email" text="이메일"/></label>
                        <input type="email" class="form-control" id="email" name="EMAIL" aria-describedby="emailHelp"
                               placeholder="<spring:message code="label_notice_receive_email" text="이메일"/>">
                    </div>
                    <div class="mb-3 sign-item">
                        <label for="position" class="form-label"><spring:message code="position" text="직위"/></label>
                        <input type="text" class="form-control" id="position" name="POSITION"
                               placeholder="<spring:message code="position" text="직위"/>">
                    </div>
                    <div class="mb-3 sign-item">
                        <label for="team" class="form-label"><spring:message code="FM_TEAM_TEAM_CD" text="팀"/></label>
                        <div class="input-box">
                            <input type="text" class="form-control team-input" id="team" onclick="openTeamModal()"
                                   name="TEAM_NM" placeholder="<spring:message code="FM_TEAM_TEAM_CD" text="팀"/>">
                            <input type="hidden" id="TEAM_CD" name="TEAM_CD" value=""/>
                            <button type="button" class="team-button" onclick="openTeamModal()">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <div class="mb-3 sign-item">
                        <label for="password" class="form-label" required><spring:message code="label_password"
                                                                                          text="비밀번호"/></label>
                        <input type="password" class="form-control" id="password" name="PASSWORD"
                               placeholder="<spring:message code="label_password" text="비밀번호"/>">
                    </div>
                    <div class="mb-3 sign-item">
                        <label for="Re-password" class="form-label" required><spring:message
                                code="label_password_confirm" text="비밀번호 확인"/></label>
                        <input type="password" class="form-control" id="Re-password"
                               placeholder="<spring:message code="label_password_confirm" text="비밀번호 확인"/>">
                        <div class="pwd-valid"><spring:message code="msg_password_chk_fail"
                                                               text="비밀번호가 맞지 않습니다."/></div>
                    </div>
                    <input type="hidden" value="G" name="USE_YN"/>
                    <input type="hidden" value="18" name="USER_TYPE"/>
                </form>
            </div>
            <div class="modal-footer">
                <div class="submit-block">
                    <button class="submit-button" type="button" onclick="handledSubmit()"><spring:message
                            code="label_sign_up" text="회원가입"/></button>
                </div>
            </div>
        </div>
        <script>
            var oldVar;
            $("#Re-password").on('propertychange change keyup paste input', function () {   // 비밀번호 비교하기
                var currentVal = $(this).val();
                if (currentVal === oldVar) return;
                oldVar = currentVal;

                var pwdValue = $("#password").val();
                var rePwdValue = $("#Re-password").val();
                if (pwdValue !== rePwdValue) {
                    $(".pwd-valid").css("visibility", "visible");
                } else {
                    $(".pwd-valid").css("visibility", "hidden");
                }
            })

            function openTeamModal() {
                $("#team_modal").modal('show');
                $('#jstree').jstree("deselect_all");
            }

            function handledSubmit() {  // 폼 유효성 검사 후 제출
                var formData = $("#signin-form").serializeObject();
                var {USER_NAME, LOGIN_ID, EMAIL, PASSWORD, USE_YN, USER_TYPE} = formData;
                if (!USER_NAME || !LOGIN_ID || !PASSWORD) {
                    alert("<spring:message code="msg_jsf_necessary_check" text="필수 값을 체크해주세요."/>");
                } else if (PASSWORD !== $("#Re-password").val()) {
                    alert("<spring:message code="msg_password_chk_fail" text="비밀번호가 맞지 않습니다."/>")
                } else {
                    post("/api/user/list/list_FM_USER_VALID/", formData, function (data) {
                        if(data.length == 0) {
                            post("/api/user/save", formData, (data1) => {
                                if(data1.result == 'ok') {
                                    alert("가입이 완료되었습니다. 관리자의 승인을 기다려주세요.");
                                    location.href = "/";
                                }
                            });
                        } else {
                            alert("<spring:message code="FM_USER_ID_VALIDATION_MESSAGE" text="사용자ID가 중복되었습니다. 수정 후 진행해 주세요."/>")
                        }
                    });
                }
            }
        </script>
    </div>
</div>
