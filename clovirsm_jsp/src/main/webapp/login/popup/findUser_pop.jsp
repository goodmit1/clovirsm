<%--
  Created by IntelliJ IDEA.
  User: gmpark
  Date: 2021-06-16
  Time: 오후 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--사용자 등록: ID, 이름, PW, 이메일, 부서--%>
<style>
    .modal-dialog {
        width: 450px;
        top:20%;
    }

    .find-item {
        margin-bottom: 15px;
    }

    .form-label {
        font-weight: bold;
    }

    h2 {
        font-weight: bold;
    }

    .submit-block .submit-button {
        width: 100%;
        height: 30px;
        display: flex;
        align-content: center;
        justify-content: center;
        flex-wrap: wrap;
        font-size: 16px;
        outline: none;
        border: none;
        border-radius: 4px;
        color: white;
        background-color: #00AC4F;
        font-weight: 600;
        cursor: pointer;
        padding-left: 1rem;
        padding-right: 1rem;
    }

    label[required]:after {
        content: " *";
        color: red;
    }
</style>
<div class="modal fade" id="findUser_Modal" tabindex="-1" aria-labelledby="findUserModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <input type="hidden" id="find_type" value="">
                <h4 class="modal-title" id="findUserModalLabel"></h4>
                <div style="border-bottom: #BEBEBE solid 1px; margin: 10px 0 0 0;"></div>
            </div>
            <div class="modal-body">
                <form action="" id="findUser-form" method="POST">
                    <div class="mb-3 find-item findID">
                        <label for="userId" class="form-label" required><spring:message code="label_userId" text="아이디"/></label>
                        <input type="text" class="form-control" name="LOGIN_ID" id="userId"
                               placeholder="<spring:message code="label_userId" text="아이디"/>">
                    </div>

                    <div class="mb-3 find-item">
                        <label for="name" class="form-label" required><spring:message code="label_userName" text="이름"/></label>
                        <input type="text" class="form-control" name="USER_NAME" id="name"
                               placeholder="<spring:message code="label_userName" text="이름"/>">
                    </div>

                    <div class="mb-3 find-item">
                        <label for="email" class="form-label"><spring:message code="label_notice_receive_email" text="이메일"/></label>
                        <input type="email" class="form-control" id="email" name="EMAIL" aria-describedby="emailHelp"
                               placeholder="<spring:message code="label_notice_receive_email" text="이메일"/>">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="submit-block">
                    <button class="submit-button" type="button" onclick="FindUserSubmit();"><spring:message code="" text="FIND"/></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function FindUserSubmit() {
        var formData = $("#findUser-form").serializeObject();
        if (!formData.USER_NAME || !formData.EMAIL ) {
            alert("<spring:message code="msg_jsf_necessary_check" text="필수 값을 체크해주세요."/>");
            return false;
        } else if (formData.EMAIL.indexOf('@') == -1) {
            alert("<spring:message code="" text="@는 필수 값입니다."/>")
            return false;
        }

        ($("#find_type").val() == 'ID' ? findID(formData) : findPWD(formData));
    }

    function findID(param) {
        delete param.LOGIN_ID;
        param.title = "아이디 찾기";
        param.template = "findID";
        post('/api/user_mng/findID', param, function (data) {
            alertType(data);
        });
    }

    function findPWD(param) {
        post('/api/user_mng/findPassword', param, function (data) {
            alertType(data);
        });
    }

    function alertType(data) {
        if(data > 0) {
            alert("메일 전송이 완료되었습니다.")
        }else if(data == -2) {
            alert("등록되지 않은 사용자입니다.")
        }else {
            alert("메일 전송이 실패되었습니다.")
        }
        location.href="/";
    }
</script>