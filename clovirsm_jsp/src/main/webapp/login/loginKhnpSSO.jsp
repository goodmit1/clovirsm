<%@page import="com.fliconz.fm.security.SSOHelper"%>
<%@page import="com.clovirsm.service.site.Crypt"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<%
    request.setCharacterEncoding("UTF-8");
    String USER_ID = request.getParameter("USER_ID");
//    USER_ID = "fliconz";
    Crypt c = new Crypt();

    if (USER_ID != null) {
        String decode = c.encryptAndEncode(USER_ID);
        String LOGIN_ID = c.decodeAndDecrypt(decode);

        System.out.println("decode : "+decode);
        System.out.println(c.decodeAndDecrypt(decode));
        SSOHelper.ssoLogin(LOGIN_ID, "", request, response);

    } else if (USER_ID == null) {
        System.out.println("USER_ID is null ? : " + USER_ID);
%>
<script>
    alert("인트라넷을 링크를 통해 접속해 주시기 바랍니다. ");
    //location.href="/";
</script>
<%
} else {
%>
<script>
    alert("SSO 로그인에 실패하였습니다.\n관리자에게 문의바랍니다.\n");
    //location.href="/";
</script>
<% }
%>