<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
var dateLabel = {};
dateLabel.label_term = "<spring:message code="label_term" text="" />";
dateLabel.year = "<spring:message code="label_year" text="" />";
dateLabel.month = "<spring:message code="label_month" text="" />";
dateLabel.week = "<spring:message code="label_week" text="" />";
dateLabel.date = "<spring:message code="label_date" text="" />";
dateLabel.hour = "<spring:message code="hour" text="" />";
dateLabel.minute = "<spring:message code="minute" text="" />";
dateLabel.second = "<spring:message code="second" text="" />";

dateLabel.day = "<spring:message code="label_day" text="" />";
dateLabel.today = "<spring:message code="dt_today" text="" />";
dateLabel.pre_month = "<spring:message code="label_pre_month" text="" />";
dateLabel.next_month = "<spring:message code="label_next_month" text="" />";

dateLabel.sunday_min = "<spring:message code="label_sunday" text="" />";
dateLabel.monday_min = "<spring:message code="label_monday" text="" />";
dateLabel.tuesday_min = "<spring:message code="label_tuesday" text="" />";
dateLabel.wednesday_min = "<spring:message code="label_wednesday" text="" />";
dateLabel.thursday_min = "<spring:message code="label_thursday" text="" />";
dateLabel.friday_min = "<spring:message code="label_friday" text="" />";
dateLabel.saturday_min = "<spring:message code="label_saturday" text="" />";

dateLabel.sunday    = dateLabel.sunday_min    + dateLabel.day;
dateLabel.monday    = dateLabel.monday_min    + dateLabel.day;
dateLabel.tuesday   = dateLabel.tuesday_min   + dateLabel.day;
dateLabel.wednesday = dateLabel.wednesday_min + dateLabel.day;
dateLabel.thursday  = dateLabel.thursday_min  + dateLabel.day;
dateLabel.friday    = dateLabel.friday_min    + dateLabel.day;
dateLabel.saturday  = dateLabel.saturday_min  + dateLabel.day;
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><spring:message code="msg_password_re_setting_mail_req"/></title>
	<link rel="stylesheet"
			href="/res/css/bootstrap.min.css">
	<script src="/res/js/jquery.min.js"></script>
		<script src="/res/js/common.js"></script>
	<link rel="stylesheet" href="/res/css/menu.css">
	<link rel="stylesheet" href="/res/css/common.css">
	<style>
		body
		{
			background-color:#a3a0af
		}
		.panel-heading h4
		{
			color : #2d062d;
		}
		.loginForm
		{
			 width:400px;
			 margin-left : auto;
			 margin-right : auto;
			 margin-top : 150px;
		}
		.row
		{
			margin-top: 20px;
		}
		.panel-footer
		{
			margin-top: 40px;
			text-align: right;
		}
		</style>
</head>
<body>
	<form action="/j_spring_security_check" id="login-form" method="POST">
			<div class="panel panel-default loginForm" >
				<div class="panel-heading">
					<h4><spring:message code="msg_password_re_setting_mail_req"/></h4>

				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<label class="control-label  grid-title-sm" for="userId"> <spring:message code="label_userId"/></label>
							<input type="text" name="userId" id="userId"
													class="form-control input-lg" >

						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<label class="control-label  grid-title-sm" for="password"> <spring:message code="EMAIL" text="이메일"/></label>
							<input type="email" name="email" id="email"
													class="form-control input-lg" >

						</div>
					</div>

				</div>
				<div class="panel-footer" >
				<div style="float:left"><spring:message code="msg_password_re_setting_mail"/></div><input type="button" onclick="request()" class="btn btn-primary " value="<spring:message code="label_confirm" />" />
				</div>
				</div>

				</form>
				<script>
					$(document).ready(function(){
						$("#userId").focus();
					})
					function request()
					{
						post('/api/guest/reqPwdInitEmail', $("#login-form").serialize(), function(data){
							alert('<spring:message code="msg_send_mail"/>');

						})
					}
				</script>

</body>
</html>
