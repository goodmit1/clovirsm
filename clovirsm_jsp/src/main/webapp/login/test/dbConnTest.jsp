 
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Test</title>
</head>
<style>
body{font-size:11px}</style>
<body>
<%
String query = request.getParameter("query");
StringBuffer sb = new StringBuffer();
if(query != null){
	Connection conn = null;
	try {
	    String user = "goodmit"; 
	    String pw = "git3775*";
	    String url = "jdbc:mysql://10.150.0.4:3306/clovirsm?serverTimezone=UTC&useUnicode=true&characterEncoding=utf8&autoReconnect=true";
	    
	    Class.forName("com.mysql.jdbc.Driver");        
	    conn = DriverManager.getConnection(url, user, pw);
	    if(query.toLowerCase().trim().startsWith("select")){
		    ResultSet rs = conn.prepareStatement(query).executeQuery();
		    java.sql.ResultSetMetaData meta = rs.getMetaData();
		    int cnt = meta.getColumnCount();
		    boolean isFirst = true;
		    
		    sb.append("<table cellpadding=10 cellspacing=0 border=1>");
		    while(rs.next()){
		    	
		    	if(isFirst){
		    		sb.append("<tr>");
			    	for(int i=0; i < cnt; i++){
			    		sb.append("<th>");
		    			sb.append(meta.getColumnLabel(i+1));
		    			sb.append("</th>");	
			    	}
			    	sb.append("</tr>");
			    	isFirst = false;
			    }
		    	sb.append("<tr>");
		    	for(int i=0; i < cnt; i++){
		    		sb.append("<td>");
	    			sb.append(rs.getObject(i+1));
	    			sb.append("</td>");	
		    	}
		    	sb.append("</tr>");
		    	
		    }
		    sb.append("</table>");
	    }
	    else {
	    	out.println(query);
	    	out.println("row =" + conn.prepareStatement(query).executeUpdate());
	    }
	    System.out.println("Database에 연결되었습니다.\n");
	    
	    
	} catch (ClassNotFoundException cnfe) {
	    out.println("DB 드라이버 로딩 실패 :"+cnfe.toString());
	} catch (SQLException sqle) {
	    out.println("에러 : "+sqle.toString());
	} catch (Exception e) {
	    out.println("Unkonwn error");
	    e.printStackTrace();
	} finally{
		conn.close();
	}
}

 
%>
<form method=post>
<textarea name="query" style="width:80%;height:500px"><%=query%></textarea>
<input type="submit">
</form>
<%=sb.toString() %>
</body>
</html>