<%@page import="com.clovirsm.sys.hv.vmware.VROpsAction"%>
<%@page import="com.clovirsm.service.monitor.OverUnderVMService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.batch.ScheduleService"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>
<%
long now = System.currentTimeMillis();
 
  
OverUnderVMService service = (OverUnderVMService)SpringBeanUtil.getBean("overUnderVMService");  
service.insertFromVROps();

VROpsAction opsAction = (VROpsAction)SpringBeanUtil.getBean("VROpsAction");
opsAction.insertDCHealth();
out.println((System.currentTimeMillis()-now)/1000);
  %>

</body>
</html>