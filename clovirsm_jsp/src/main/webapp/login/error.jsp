<%@page import="java.io.PrintStream"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page language="java" contentType="text/html; charset=utf-8"    pageEncoding="utf-8"%>
<%
Throwable exceptions = (Throwable) request.getAttribute("javax.servlet.error.exception");
if(exceptions != null)
{
	//out.println(exceptions.getMessage());
	exceptions.printStackTrace(  );
}
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head id="j_idt2">
 
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalabel=0" />
            <meta name="apple-mobile-web-app-capable" content="yes" />
<style>
.exception-code {
    background-color: #e91e63;
}
.exception-panel {
    width: 550px;
    height: 480px;
    background-color: #ffffff;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -275px;
    margin-top: -240px;
    padding: 0;
    text-align: center;
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -webkit-box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14);
    -moz-box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14);
    box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14);
}
.exception-detail h1
{
margin-top:60px
}

</style>


        <title><spring:message code="msg_page_error"/></title></head><body class="exception-body accessdenied">
        <div class="exception-panel">
            <div class="exception-code"><img id="j_idt7" src="/res/img/500.svg" alt="" />
            </div>

            <div class="exception-detail">
                <h1><spring:message code="msg_page_error"/></h1>
                <p><spring:message code="msg_ask_admin"/></p><button type="button" onclick="window.open('/home/home.jsp','_self')"><span ><spring:message code="msg_go_home"/></span></button>
            </div>
        </div></body>

</html>
