<%@page import="com.fliconz.fm.security.filter.FMUrlSecurityMetadataSource"%>
<%@page import="java.util.List"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="com.fliconz.fm.security.PermissionService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>
<%
LinkedHashMap methodMap = (LinkedHashMap)SpringBeanUtil.getBean("methodMap");

PermissionService permissionService = (PermissionService)SpringBeanUtil.getBean("permissionService");
methodMap.clear();
methodMap.putAll(permissionService.getRoleMethodList());


FMUrlSecurityMetadataSource urlSecurityMetadataSource =(FMUrlSecurityMetadataSource) SpringBeanUtil.getBean("urlSecurityMetadataSource");
urlSecurityMetadataSource.reload();
%>
</body>
</html>