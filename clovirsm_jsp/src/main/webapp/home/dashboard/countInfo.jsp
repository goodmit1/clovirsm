<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-default chart_area " id="rsc_cnt_area">
	<div class="panel-heading"><spring:message code="title_retention_system"/> <spring:message code="label_use_item_status"/></div>
	<div class="panel-body" >
		<div id="cnt_dc_area" class="dc_area">
		</div>
		<div class="sm_panel_content">

			 <div class="sm_panel " id="ds_area">
				<img src="/res/img/dashboard/d_ds.png" />
				<div class="sm_panel_title"><spring:message code="title_storage" /></div>
				<a href="/clovirsm/monitor/ds/index.jsp"><div class="panel_value"  >{{count_data.DS_CNT}}</div></a>
			</div>
			<div class="sm_panel " id="vm_area">
				<img src="/res/img/dashboard/d_vm.png" />
				<div class="sm_panel_title">VM</div>
				<a href="/clovirsm/monitor/vm/index.jsp"><div class="panel_value"  >{{count_data.VM_CNT}}</div></a>
			</div>
			<div class="sm_panel" id="host_area">
				<img src="/res/img/dashboard/d_host.png" />
				<div class="sm_panel_title">HOST</div>
				<a href="/clovirsm/monitor/host/index.jsp"><div class="panel_value"  >{{count_data.HOST_CNT}}</div></a>
			</div>
			<c:if test="${sessionScope.IMG_SHOW }">
				<div class="sm_panel " id="img_area">
					<img src="/res/img/dashboard/d_img.png" />
					<div class="sm_panel_title"><spring:message code="title_img"/></div>
					<a href="/clovirsm/resources/image/index.jsp"><div class="panel_value" >{{count_data.IMG_CNT}}</div></a>
				</div>
			</c:if>
		</div>
	</div>
</div>
<script>
var rsc_cnt_area = new Vue({
  el: '#rsc_cnt_area',
  data: {
	  count_data: {DS_CNT:0, VM_CNT:0, HOST_CNT:0, IMG_CNT:0}
  },
  methods: {
	  getReqInfo: function (DC_ID) {
		var that = this;
		$.get('/api/monitor/list/list_RSC_CNT/?DC_ID=' + DC_ID, function(data)
			{
				that.count_data= data[0];

			}
		)
    }
  }
});
chgDcCallback.cnt_dc_area = "rsc_cnt_area.getReqInfo";
$(document).ready(function(){
})
</script>