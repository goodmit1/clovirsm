<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="expireList">
	<div class="panel-heading"><a href="/admin/notice/index.jsp"><spring:message code="dt_expiredate" text="만료일" /></a></div>
	<div class="panel-body">
		<div class="dashboard_list" style="width:100%">
			<table>
				<colgroup>
					<col width="60%" />
					<col width="30%" />
					<col width="20%" />
				</colgroup>
			 	<thead>
				    <tr>
				      <th><spring:message code="btn_list"/></th>
				      <th><spring:message code="NC_FW_CUD_CD"/></th>
				      <th><spring:message code="dt_expiredate"/></th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		<tr v-for="req in expireList" style="width:100%">
			  			<td class="textleft">
			  				<div class="dashboard_list_nm noti ellipsis">
								<a :href="'javascript:expireClick(\'' + req.KUBUN + '\', \'' + req.NAME + '\', \'' + req.VM_NM + '\')'" class="notice_link">{{req.NAME}}</a>
							</div>
						</td>
						<td><div>{{req.KUBUN}}</div></td>
						<!-- <td><span >{{req.EXPIRE_DATE | date('datetime')}}</span></td> -->
						<td><span >{{formatDate(req.EXPIRE_DATE ,'date')}}</span></td>
			  
			  		</tr>
			  	</tbody>
			</table>
		</div>
	
		<!-- <ul class="dashboard_list">
			<li class="dashboard_list_item" v-for="req in expireList" style="width:100%">
				<div class="dashboard_list_nm noti ellipsis">
					<a :href="'javascript:expireClick(\'' + req.KUBUN + '\', \'' + req.NAME + '\', \'' + req.VM_NM + '\')'" class="notice_link">{{req.NAME}}</a>
				</div>
			</li>
		</ul> -->
	</div>
</div>
<style>
#noticePop_button {
	display: none;
}
</style>
<script>
function expireClick(kubun, name, vm){
	if("IMG" == kubun) {
		location.href = "/clovirsm/resources/image/index.jsp?IMG_NM=" + name;
		return;
	}else {
		location.href = "/clovirsm/resources/vm/index.jsp?VM_NM=" + vm;
	}
}
	var expireList = new Vue({
		el: '#expireList',
		data: {
			expireList: []
		},
		methods: {
			getReqInfo: function () {
				var that = this;
				$.get('/api/monitor/list/list_expire/', function(data){
					that.expireList = data;
				});
			}
		}
	});

	$(document).ready(function(){
		expireList.getReqInfo();
		refCallback.push("expireList.getReqInfo");
	});
</script>
