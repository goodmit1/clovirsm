<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="qnaList">
	<div class="panel-heading"><a href="/admin/inquiry/index.jsp">Q&A</a></div>
	<div class="panel-body">
		<div class="dashboard_list" style="width:100%">
			<table>
				<colgroup>
					<col width="70%" />
					<col width="20%" />
					<col width="10%" />
				</colgroup>
			 	<thead>
				    <tr>
				      <th><spring:message code="label_title"/></th>
				      <th><spring:message code="INS_TMS"/></th>
				      <th><spring:message code="NC_INQ_ANSWER" text="Answer"/></th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		<tr v-for="req in qnaList">
			  			<td class="textleft"><a style="cursor: pointer;" onclick="javascript:showQnAceDetail(this)"  :data-id=" req.INQ_ID"  >{{req.INQ_TITLE}}</a></td>
			  			
			  			<td><span >{{formatDate(req.INS_TMS ,'date')}}</span></td>
			  			<td><img class="req_img" :src="'/res/img/dashboard/req_' + ((req.ANSWER_YN == 'N' )?'D':'A') + '.png'"  ></img></td>
			  		</tr>
			  	</tbody>
			</table>
		</div>


		<!-- <ul class="dashboard_list">
			<li class="dashboard_list_item" v-for="req in qnaList" style="width:100%">
				<div class="dashboard_list_nm qna ellipsis">
					<a :href="'javascript:showQnAceDetail(' + req.INQ_ID + ')'"    >{{req.INQ_TITLE}}</a>
				</div>
				<div class="dashboard_list_icon noti_date">
					<span class="dashboard_noti_date">{{req.INS_TMS | date('yyyy-MM-dd')}}</span>
					<img class="req_img" :src="'/res/img/dashboard/req_' + ((req.ANSWER==null  ||  req.ANSWER=='')?'D':'A') + '.png'"  ></img>
				</div>

			</li>
		</ul> -->
	</div>
</div>
<script>

	function adminCheck(){
		var val = ADMIN_YN;
		if(val === 'Y')
		{
			return val;
		}
		else{
			return '';
		}
		
		
	}

	var qnaList = new Vue({
		el: '#qnaList',
		data: {
			qnaList: []
		},
		methods: {
			getReqInfo: function () {
				var that = this;
				$.get('/api/monitor/get_qna_req?ALL_YN='+adminCheck(), function(data){
					 
					that.qnaList= data.QNA_CNT;
				});
			}
		}
	});

	$(document).ready(function(){
		qnaList.getReqInfo();
		refCallback.push("qnaList.getReqInfo");
	});
</script>
