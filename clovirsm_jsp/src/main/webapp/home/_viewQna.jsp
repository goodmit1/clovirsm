<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/popup_main">
<layout:put block="content">
<style>
  body{
	background-color : white
  }
  .panel-body{
  	height : 99vh
  }
  hr{
  	margin:0px;
  }
</style>
<div id="input_area"  >

	<div class="form-panel detail-panel panel panel-default padding0">
		<div class="panel-body" style="overflow-y: auto; word-break: break-word;">
			<div>
				<div class="col col-sm-9" style="border-bottom:none;padding-left:15px;">
					<h3>{{form_data.INQ_TITLE}}</h3>
				</div>
				<div class="col col-sm-3 small text-right" style="border-bottom:none; margin-top: 20px;">
					<span style="padding-right:10px;">{{form_data.TEAM_NM}}/{{form_data.INS_NAME}}</span>
					<div style="padding-right:10px;">{{formatDate(form_data.INS_TMS,'date')}}</div>
				</div>
			</div>
			<hr />
			<div class="col col-sm-12" style="border-bottom:none; padding: 5px;padding-left: 25px;">
				<span style="font-size: 30px;font-weight: 700;color: #005daf; display: inline-block; float: left;">Q.</span>
				<div v-html="form_data.INQUIRY" style="min-height:100px; padding: 5px; display: inline-block; margin-top: 7px;">
				
				
				</div>
				<hr />
				<span style="font-size: 30px;font-weight: 700;color: #de2121;display: inline-block; float: left;">A.</span>
				<div v-html="form_data.ANSWER" style="min-height:100px;padding: 5px; display: inline-block; margin-top: 7px;">
				</div>
			</div>
		</div>
	</div>
</div>
<script>

var req = new Req();
$(document).ready(function(){
	req.getInfo("/api/qna/info?INQ_ID=${param.INQ_ID}", function(d){
		parent.hideLoading();
	});
})

</script>
</layout:put>
</layout:extends>
