<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="reqExpireList">
	<div class="panel-heading"><spring:message code="dt_expiredate_due"/> <span></span></div>
	<div class="panel-body">
		<div class="dashboard_list" style="width:100%">
			<table>
				<colgroup>
					<col width="30%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
				</colgroup>
				<thead v-if="reqExpireList.length != 0">
					<tr >
						<th><spring:message code="OVERUNDER_VM_NM" text="서버명"/></th>
						<th>IP</th>
						<th><spring:message code="CATEGORY_NM" text="서비스명"/></th>
						<th><spring:message code="FM_USER_USER_NAME" text="사용자명"/></th>
						<th><spring:message code="dt_expiredate" text="만료일"/></th>
					</tr>
				</thead>
			  	<tbody>
			  		<tr v-for="req in reqExpireList" v-if="reqExpireList.length != 0">
			  			<td class="textleft"><a style="cursor: pointer;"   :data-id=" req.SVC_ID"  >{{req.VM_NM}}</a></td>
			  			<td><span >{{req.IP}}</span></td>
			  			<td><span >{{req.CATEGORY_NM}}</span></td>
			  			<td><span >{{req.USER_NAME}}</span></td>
			  			<td><span >{{formatDate(req.EXPIRE ,'date')}}</span></td>
			  		</tr>
			  		<tr v-if="reqExpireList.length == 0" style="border-bottom: none;">
			  			<td><spring:message code="search_result_empty" text="조회된 결과가 없습니다."/></td>
			  		</tr>
			  	</tbody>
			</table>
		</div>


	</div>
</div>
<script>

	function adminCheck(){
		var val = ADMIN_YN;
		if(val === 'Y')
		{
			return val;
		}
		else{
			return '';
		}
		
		
	}

	var reqExpireList = new Vue({
		el: '#reqExpireList',
		data: {
			reqExpireList: []
		},
		methods: {
			getVmReqInfo: function () {
				var that = this;
				$.get('/api//monitor/list/list_expire_vm/', function(data){
					 
					that.reqExpireList= data;
				});
			}
		}
	});

	$(document).ready(function(){
		reqExpireList.getVmReqInfo();
		refCallback.push("reqExpireList.getVmReqInfo");
	});
</script>
