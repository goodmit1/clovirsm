<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<div class="reqDisplay green">
	<div class="reqImg">
		<div class="vmCircleArea"><div class="reqCircle"></div></div>
		<div class="reqImgText"><spring:message code="req_state" text="요청 현황"/></div>
	</div>
	<div class="reqText">
		<div class="req_info">
			<span><spring:message code="new_service" text="신규 신청"/> : </span><span><a href="/clovirsm/workflow/requestItemsHistory/index.jsp?SVCCUD=CC&APPR_STATUS_CD=R">{{req_data.CC}}</a></span>
		</div>
		<div class="req_info">
			<span><spring:message code="NEW_VM_TITLE_3" text="자원 변경"/> : </span><span><a href="/clovirsm/workflow/requestItemsHistory/index.jsp?SVCCUD=SU&APPR_STATUS_CD=R">{{req_data.SU}}</a></span>
		</div>
		<div class="req_info">
			<span><spring:message code="NEW_VM_TITLE_4" text="삭제 요청"/> : </span><span><a href="/clovirsm/workflow/requestItemsHistory/index.jsp?SVCCUD=SD&APPR_STATUS_CD=R">{{req_data.SD}}</a></span>
		</div>
		<div class="req_info">
			<span><spring:message code="NEW_VM_TITLE_5" text="기간 변경"/> : </span><span><a href="/clovirsm/workflow/requestItemsHistory/index.jsp?SVCCUD=EU&APPR_STATUS_CD=R">{{req_data.EU}}</a></span>
		</div>
		<div class="req_info">
			<span><spring:message code="title_server_reuse" text="서버복원"/> : </span><span><a href="/clovirsm/workflow/requestItemsHistory/index.jsp?SVCCUD=VU&APPR_STATUS_CD=R">{{req_data.VU}}</a></span>
		</div>
		<div class="req_info">
			<span><spring:message code="recent_approval" text="최근 승인"/> : </span><span><a href="/clovirsm/workflow/requestItemsHistory/index.jsp?APPR_STATUS_CD=A">{{req_data.A}}</a> <span class="small">(7일간)</span> </span>
		</div>
		<div class="req_info">
			<span><spring:message code="recent_deny" text="최근 반려"/> : </span><span><a href="/clovirsm/workflow/requestItemsHistory/index.jsp?APPR_STATUS_CD=D">{{req_data.D}}</a> <span class="small">(7일간)</span></span>
		</div>
	</div>
</div>
<script>
var req_cnt = new Vue({
  el: '.reqDisplay',
  data: {
	  req_data: {CC:0, SD:0, SU:0, EU:0, VU:0, A:0 , D:0}
  },
  methods: {
	  getReqInfo: function () {
		var that = this;
		$.get('/api/monitor/list/list_req_cnt/?dd=7', function(data)
			{
				for(var i = 0; i < data.length; i++){
					var cd = data[i].CD;
					that.req_data[cd] = data[i].NUM;
				}
			}
		)
    },
  	  date_change: function(date){
	  	  var year = date.getFullYear();
	  	  var month = date.getMonth()+1;
	  	  month = (month < 10) ? '0' + month : month;
	      var day = date.getDate();
	      day = (day < 10) ? '0' + day : day;
	      return year+""+month;
  	  }
  }
});

$(document).ready(function(){
	req_cnt.getReqInfo();
	refCallback.push("req_cnt.getReqInfo");
})

</script>