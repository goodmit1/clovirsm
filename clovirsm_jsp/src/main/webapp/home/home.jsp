<%@page import="com.fliconz.fm.mvc.service.DashboardMngService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<%
	DashboardMngService service = (DashboardMngService)SpringBeanUtil.getBean("dashboardMngService");

	MonitorService monitorService = (MonitorService)SpringBeanUtil.getBean("monitorService");
	
	NextApproverService nextApproverService = (NextApproverService)SpringBeanUtil.getBean("nextApproverService");

	Map param=new HashMap();
	param.put("_IS_ADMIN_", monitorService.isAdmin() ? "Y" : "N");
	session.setAttribute("ADMIN_YN", monitorService.isAdmin()?"Y":"N");
	session.setAttribute("APPROVER_YN", nextApproverService.isApprover()?"Y":"N");
	List<Map> list = (List<Map>)service.selectListByQueryKey("user_dashboard_item",param );
	request.setAttribute("list", list);

session.setAttribute("FW_SHOW", PropertyManager.getBoolean("clovirsm.display.fw", false));
session.setAttribute("IMG_SHOW", PropertyManager.getBoolean("clovirsm.display.img", false));
session.setAttribute("SNAPSHOT", PropertyManager.getBoolean("clovirsm.display.snapshot", false));
%>
<layout:extends name="base/index">
 <layout:put block="navi" type="REPLACE">
 </layout:put>
<layout:put block="content">
	<script>

	
 	var chgDcCallback = {};

 	var refCallback = [];
 	
 	var cookieId;

	function makeDC(){
		$.get('/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC', function(list){
			var DCSelect = '';
			if(list && list.length>1) {
				DCSelect = '<select id="DC_ID" onchange="">';
				for( var key in list ) {
					if(!list[key].DC_ID) continue;
					DCSelect += '<option value="' + list[key].DC_ID + '">' + list[key].DC_NM + '</option>';
				}
				DCSelect += '</select>';
				$("div.dc_area").each(function(){
					var _that = this;
					if(chgDcCallback[this.id] != null) {
						$(this).append(DCSelect.replace('onchange=""', 'onchange="' + chgDcCallback[this.id] + '($(this).val())"'))
						setTimeout(function(){
							 
							eval(chgDcCallback[_that.id] + "('" + $(_that).find("#DC_ID").val() + "')");
						}, 100);
					}
				})
			} else {
				$("div.dc_area").each(function(){
					var _that = this;
					var html = '<input type="hidden" value="' + list[0].DC_ID + '" id="DC_ID">'
					$(_that).append(html);
					if(chgDcCallback[this.id] != null) {
						eval(chgDcCallback[_that.id] + "('" + list[0].DC_ID + "')");
					}
				})
			}
		})
	}

	function refresh() {
		showLoading();
		$("div.dc_area").each(function(){
			var _that = this;
			if(chgDcCallback[this.id] != null) {
				eval(chgDcCallback[_that.id] + "('" + $(_that).find("#DC_ID").val() + "')");
			}
		})
		for(var i=0; i<refCallback.length; i++){
			var obj = refCallback[i];
			if(obj instanceof Object) {
				eval(obj.callback + "(" + obj.param ? obj.param : "" + ")");
			} else {
				eval(obj + "()");
			}
		}
		//for(var i in refCallback) {
		//}
		setTimeout(function(){hideLoading();}, 100);
	}
	</script>
	<link rel="stylesheet" href="/res/css/dashboard.css">
	<script src="/res/js/perf-chart.js"></script>
	<fmtags:include page="dashboard.jsp"/>
	<script>
		var selectDashboardData = {};
		var indexMap = {};
	 	var from = null;
	 	
		$(function(){
			if(ADMIN_YN=="Y") makeDC();
			
			
			$(".dashboardArea").sortable({items: "> div.dashboardItem", start: function(event, ui){
				ui.item.show().addClass('ghost')
		    	ui.placeholder.height(ui.item.height());
		    	ui.placeholder.width(ui.item.width());
		    	from = ui.item.index() - 1;
		    }, stop: function(event, ui){
		    	var to = ui.item.index() - 1;
		    	 
		    	ui.item.show().removeClass('ghost');
		    	if(to == from){
		    		return;
		    	}
		    	for(var ori in indexMap){
		    		var i = indexMap[ori];
		    		if(from > to){
		    			if(i >= to && i < from){
		    				indexMap[ori] = ++i;
		    			}else if(i == from){
		    				indexMap[ori] = to;
		    			}
		    		}else{
		    			if(i <= to && i > from){
		    				indexMap[ori] = --i;
		    			}else if(i == from){
		    				indexMap[ori] = to;
		    			}
		    		}
		    	}
		    	var tmp = [];
		    	for(var f in indexMap){
		    		var t = indexMap[f];
		    		var o = {};
		    		o.FROM = f;
		    		o.TO = t;
		    		tmp.push(o);
		    	}
		    	var str = JSON.stringify({param: tmp});
				var param = selectDashboardData.checkedNames.join("\x2C");
				$.get("/api/dashboard_mng/update/dashboard/" + param,function(){
		    		$.get("/api/dashboard_mng/update/dashboardSeq/" + str,function(){});
				})
		    },
		    forcePlaceholderSize: true,
		    placeholder :'drag-place-holder',
		    autoHide: false,
	        animate: true,
	        animateDuration: "fast",
	        ghost: true,
	        resize: function (event, ui) {
	            var outerWidth = $("#dashboardArea").width();
	            var zoneWidthInPercent = Math.round(ui.size.width * 10000 / outerWidth) / 100;
	            var zonesPerRow = Math.abs(zoneWidthInPercent - 50) < 10 ? 2 : 3;
	            
	            var zonePixels = Math.round(outerWidth * 2 / zonesPerRow) / 2;
	            
	            var width = Math.max(Math.round(ui.size.width / zonePixels), 1) * zonePixels;
	            ui.size.width = width;
	            ui.size.height = Math.max(Math.round(ui.size.height / 100), 1) * 100 + 2;
	        },
			});
			$("#dashboardRefresh").click(refresh);
			$("#openDashboardEdit").click(function(){
				$(".selectDashboard").show();
				$("#cancelDashboardItems").click(function(){
					$(".selectDashboard").hide();
				});
				$("#saveDashboardItems").click(function(){
					var param = selectDashboardData.checkedNames.join("\x2C");
					$.get("/api/dashboard_mng/update/dashboard/" + param,function(){
						location.reload();
					})
				});
			});
			var allItem = {};
			selectDashboardData.items = [];
			selectDashboardData.checkedNames = []
			$.get('/api/dashboard_mng/list/select_dashboard_item/?_IS_ADMIN_=' + ADMIN_YN,function(data){
				if(data){
					
					for(var i=0; i<data.length; i++){
						var row = data[i];
						var item = {};
						item.value = row.ITEM_ID;
						item.name = row.ITEM_NM;
						allItem[item.value] = item.name
						selectDashboardData.items.push(item);
						
					}
					//for(var i in data){
					//}
					$.get('/api/dashboard_mng/list/user_dashboard_item/?_IS_ADMIN_=' + ADMIN_YN,function(list){
						if(list){
							for(var i=0; i<list.length; i++){
								var row = list[i];
								selectDashboardData.checkedNames.push(row.ITEM_ID);
								if(allItem[ row.ITEM_ID] == null){
									var item = {};
									item.value = row.ITEM_ID;
									item.name = row.ITEM_NM;
									selectDashboardData.items.push(item);
								}
							} 
							//for(var i in list){
							//}
							dashboardReady();
						}
					});
				}
			});
			
			$( ".dashboardArea > div.dashboardItem" ).each(function(){
		    	var i = $(this).index() - 1;
		    	indexMap[this.id] = i;
		    })
		})
		function goWork(kubun)
		{
			location.href="/clovirsm/workflow/work/index.jsp?click=" + kubun;
		}
		function dashboardReady(){
			 
				selectDashboard = new Vue({
					el: '.selectDashboard',
					data: selectDashboardData
				});
			 
		}
		function fillCountData(xhr, status, args) {
			var prop;

			var val = args.LIST[0];

			for (prop in val) {
				$("#" + prop  ).text(val[prop]);

			}
		}
		
		function getNotiTopMsg(){
			$.get('/api/monitor/list/list_FM_NOTICE/',function(data){
				for(var i=0; i<data.length; i++){
						var val = data[i]
						if (val.TOP_YN === 'Y')
						{
							var cookie = getCookie('cookie'+val.ID);
							if(cookie !== 'Y')
							{	
								viewNoticeTop(val.ID,'show')
							}
						}
				}
			})
		}
		
		Highcharts.setOptions({    navigation: {
	        buttonOptions: {
	            enabled: false
	        }
	    },
        title :     {align: 'left'}});
	</script>
	
</layout:put>
<layout:put block="popup_area">
   	<jsp:include page="viewNotice.jsp"></jsp:include>
	<jsp:include page="viewNoticeTop.jsp"></jsp:include> 
   	<jsp:include page="viewQna.jsp"></jsp:include>

</layout:put>
</layout:extends>