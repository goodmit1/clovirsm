<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
.panel{
border-width: 1px;
    border-style: solid;
    padding: 10px;
    margin : 10px;
    border-radius: 10px;
    }</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>마이그레이션</title>
</head>
<body>
<h3>마이그레이션</h3>
<ol>
<li>조직
<div class="panel">

<a href="org_sample.xlsx">엑셀 템플릿</a>
<form method="post" action="mig.jsp?kubun=org" target="resultArea" enctype="multipart/form-data">
<input type="file" name="org_file"><input type="submit">
<input type="hidden" name="field" value="COMP_ID" />
<input type="hidden" name="field" value="COMP_NM" />
</form>
</div>


</li>
<li>그룹</li>
<div class="panel">

<a href="dept_sample.xlsx">엑셀 템플릿</a>
<form method="post" action="mig.jsp?kubun=dept&DC_ID=528716005374355" target="resultArea" enctype="multipart/form-data">
<input type="file" name="org_file"><input type="submit">
<input type="hidden" name="field" value="TEAM_CD" />
<input type="hidden" name="field" value="TEAM_NM" />
<input type="hidden" name="field" value="COMP_ID" />
<input type="hidden" name="field" value="PARENT_CD" />
</form>
</div>

<li><spring:message code="FM_USER_USER_NAME" text="사용자" /></li>
<div class="panel">

<a href="user_sample.xlsx">엑셀 템플릿</a>
<form method="post" action="mig.jsp?kubun=user" target="resultArea" enctype="multipart/form-data">
<input type="file" name="org_file"><input type="submit">
<input type="hidden" name="field" value="LOGIN_ID" />
<input type="hidden" name="field" value="USER_NAME" />
<input type="hidden" name="field" value="TEAM_CD" />
<input type="hidden" name="field" value="EMAIL" />
<input type="hidden" name="field" value="TEAM_ADMIN_YN" />
<input type="hidden" name="field" value="COMPANY_ADMIN_YN" />
</form>
</div>
<li>VM</li>

<div class="panel">

<a href="mig.jsp?kubun=vm_sample&DC_ID=19026062427700" target="resultArea">엑셀 템플릿</a>
<form method="post" action="/api/vm/mig" target="resultArea" enctype="multipart/form-data">
<input type="file" name="org_file"><input type="submit">
<input type="hidden" id="DC_ID" name="DC_ID" value="19026062427700"/>
<!--<input type="hidden" name="field" value="VM_HV_ID" />-->
<input type="hidden" name="field" value="VM_NM" />
<!--<input type="hidden" name="field" value="CMT" />
 <input type="hidden" name="field" value="PUBLIC_IP" /> -->
<input type="hidden" name="field" value="LOGIN_ID" />
</form>
</div>
</ol>
<iframe id="resultArea" name="resultArea" style="width:1000px"></iframe>
</body>
</html>