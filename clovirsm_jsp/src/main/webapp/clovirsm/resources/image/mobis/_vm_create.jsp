<span slot="footer">
		<input type="button" class="btn" value="<spring:message code="btn_work" text=""/>" onclick="server_create_to_work()" />
		<input type="button" class="btn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="server_create_request()" />
		<fm-popup-button style="display:none;" popupid="vm_create_search_form_popup" popup="/clovirsm/popup/nas_search_form_popup.jsp" cmd="" param="" callback="vm_create_popup_select_nas"></fm-popup-button>
	</span>
	<style>
	#vm_create_popup .modal-dialog {
		width: 1300px;
	}
	.row-fluid.Editor-container {
		width:calc(100% - 150px);
		float: right;
	}
	</style>
	<div class="form-panel detail-panel">
		<form id="vm_create_popup_form" action="none">
			<div class="panel-body">
				<input type="hidden" name="VM_ID" id="F_VM_ID"  />
				<div class="col col-sm-12">
					<fm-select id="SCP_DC_NM" name="DC_ID"
						url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC"
						keyfield="DC_ID" titlefield="DC_NM"
						title="<spring:message code="NC_DC_DC_NM" text="ë°ì´í° ì¼í°"/>">
					</fm-select>
				</div>
				<div class="col col-sm-12">
					<fm-output id="SCP_IMG_NM" name="IMG_NM" title="<spring:message code="NC_IMG_IMG_NM" text="ííë¦¿ëª"/>"></fm-output>
				</div>
				<div class="col col-sm-12">
					<fm-input id="SCP_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VMëª"/>" v-if="${IS_AUTO_VM_NAME}" disabled="disabled"></fm-input>
					<fm-input id="SCP_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VMëª"/>" v-if="${!IS_AUTO_VM_NAME}"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=PURPOSE" id="SCP_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="ì¬ì©ì©ë"/>"></fm-select>
				</div>
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="SCP_P_KUBUN" emptystr=" " name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="êµ¬ë¶"/>"></fm-select>
				</div>
				<div class="col col-sm-6">
					<fm-ibutton id="SCP_SPEC_NM" name="SPEC_NM" title="<spring:message code="NC_VM_SPEC_NM" text="ì¬ì"/>">
						<fm-popup-button v-show="false" popupid="template_spec_search" popup="../../popup/spec_search_form_popup.jsp" cmd="update" param="${popupid}_getSpecSearchParam()" callback="select_spec"><spring:message code="btn_spec_search" text="ì¬ìê²ì"/></fm-popup-button>
					</fm-ibutton>
				</div>
				<div class="col col-sm-6">
					<fm-output id="SCP_CPU_CNT" name="CPU_CNT" title="<spring:message code="NC_IMG_CPU_CNT" text="CPU"/>"></fm-output>
				</div>
				<div class="col col-sm-6">
					<fm-output id="SCP_DISK_SIZE" name="DISK_SIZE" title="<spring:message code="NC_VM_DISK_SIZE" text="ëì¤í¬"/>"></fm-output>
				</div>
				<div class="col col-sm-6">
					<fm-output id="SCP_RAM_SIZE" name="RAM_SIZE" title="<spring:message code="NC_VM_RAM_SIZE" text="ë©ëª¨ë¦¬"/>"></fm-output>
				</div>
				<c:if test="${sessionScope.NAS_SHOW}">
				<div class="col col-sm-12" style="max-height: 278px;">
					<div class="hastitle">
						<label for="vm_create_NAS"
							class="control-label grid-title value-title"
							style="vertical-align: top;">
							<spring:message code="NC_NAS_NAS" text="NAS" />
							<button type="button" class="btn btn-primary btn" onclick="vm_create_newNas()" title="btn_new">
								<i class="fa fa-plus"></i>
							</button>
						</label>
						<div class="hastitle output" id="vm_create_NAS_TABLE" style="height: 100%;overflow: auto;">
						</div>
					</div>
				</div>
				</c:if>
				<div class="col col-sm-12">
					<div>
						<label for="SCP_CMT" class="control-label grid-title value-title"><spring:message code="NC_VM_CMT" text="ì¤ëª" /></label>
						<textarea id="SCP_CMT" name="CMT" style="width:calc(100% - 150px);"></textarea>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script>
		$(function(){
			setTimeout(function(){
				$("#SCP_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
			},1000)
		});
		var popup_server_create_form = newPopup("vm_create_popup_button", "vm_create_popup");
		var vm_create_popup_param = {};
		var vm_create_popup_vue = new Vue({
			el: '#vm_create_popup',
			data: {
				form_data: vm_create_popup_param
			}
		});
		function vm_create_popup_click(vue, param){
			 
			if(param.IMG_ID){
				vm_create_popup_param = $.extend({
					VM_NM: '',
					CMT: '',
					DC_ID: -1,
					DC_NM: '',
					IMG_ID: -1,
					IMG_NM: '',
					SPEC_ID: -1,
					SPEC_NM: '',
					CPU_CNT: 0,
					DISK_SIZE: 0,
					DISK_UNIT: 'G',
					RAM_SIZE: 0
				}, param);
				if(vm_create_popup_param.NAS) {
					var nasArr = vm_create_popup_param.NAS.split(",");
					nasArr.forEach(function(value,index){
						vm_create_makeNas(nasArr[index], true);
					})
					/* for(var i in nasArr){
						vm_create_makeNas(nasArr[i], true);
					} */
				}
				vm_create_popup_vue.form_data = vm_create_popup_param;
				setTimeout(function(){$("#SCP_CMT").Editor("setText", vm_create_popup_param.CMT ? vm_create_popup_param.CMT:"")},1000);
				return true;
			} else {
				vm_create_popup_param = null;
				alert(msg_select_first);
				return false;
			}
		}

		function vm_create_newNas(){
			$("#vm_create_search_form_popup_button").click();
		}
		function vm_create_popup_select_nas(data, callback){
			 
			if(callback){
				callback.call();
			}
			for(var i=0; i<data.length; i++){
				var row = data[i];
				if(!vm_create_chk_dupl_nas(row.PATH)){
					continue;
				}
				vm_create_makeNas(row.PATH, true);
			}
			/* for(var i in data) {
				var row = data[i];
				if(!vm_create_chk_dupl_nas(row.PATH)){
					continue;
				}
				vm_create_makeNas(row.PATH, true);
			} */
		}

		function vm_create_clearNas(){
			$("#vm_create_NAS_TABLE").html("");
		}

		function vm_create_makeNas(path, delAble) {
			var table = $("#vm_create_NAS_TABLE");
			var html = '<div class="nas_item choice" style="width: fit-content;float:left;">';
			if(delAble == true){
				html += '<span class="choice_remove" onclick="delete_nas(this);">×</span>';
			}
			html += '<span class="nas_text">'
			html += path;
			html += "</span></div>";
			table.append(html);
		}

		function vm_create_chk_dupl_nas(path){
			var list = $("#vm_create_NAS_TABLE .nas_text");
			for(var i=0; i<list.length; i++){
				if(isNaN(i) || i == null) {
					continue;
				}
				var val = $(list[i]).text();
				if(path == val) {
					return false;
				}
			}
			return true;
			/* for(var i in list) {
				if(isNaN(i) || i == null) {
					continue;
				}
				var val = $(list[i]).text();
				if(path == val) {
					return false;
				}
			} */
		}

		function delete_nas(that){
			var _this = $(that);
			_this.parent().remove();
		}

		function vm_create_popup_onAfterOpen(){

		}

		function ${popupid}_getSpecSearchParam(){
			return {
				DC_ID: ${popupid}_vue.form_data.DC_ID,
				SPEC_NM: $('#SCP_SPEC_NM').val()
			}
		}

		function server_create_to_work(){
			before_create_req();
			if(vm_create_popup_param.SPEC_ID && vm_create_popup_param.SPEC_ID > 0){
				vm_create_popup_param.CMT = $("#SCP_CMT").Editor("getText" );
				post('/api/vm/insertReq', vm_create_popup_param,  function(data){
					if(confirm(msg_complete_work_move)){
						location.href="/clovirsm/workflow/work/index.jsp";
						return;
					}
					vmImageSearch();
					$('#vm_create_popup').modal('hide');
				});
			} else {
				alert(msg_select_first);
			}
		}

		function server_create_request(){
			before_create_req();
			if(vm_create_popup_param.SPEC_ID && vm_create_popup_param.SPEC_ID > 0){
				vm_create_popup_param.CMT = $("#SCP_CMT").Editor("getText" );
				vm_create_popup_param.DEPLOY_REQ_YN = 'Y';
				post('/api/vm/insertReq', vm_create_popup_param,  function(data){
					alert(msg_complete_work);
					vmImageSearch();
					$('#vm_create_popup').modal('hide');
				});
			} else {
				alert(msg_select_first);
			}
		}

		function before_create_req() {
			var list = $("#vm_create_NAS_TABLE .nas_text");
			var nasStr = [];
			for(var i=0; i<list.length; i++){
				if(isNaN(i) || i == null) {
					continue;
				}
				var val = $(list[i]).text();
				if(val && val.trim() != "") {
					nasStr.push(val);
				}
			}
			/* for(var i in list) {
				if(isNaN(i) || i == null) {
					continue;
				}
				var val = $(list[i]).text();
				if(val && val.trim() != "") {
					nasStr.push(val);
				}
			} */
			if(nasStr[0] != null) {
				vm_create_popup_param.NAS = nasStr.join(",");
			}

		}

		function select_spec(args, callback){
 			vm_create_popup_param = $.extend(vm_create_popup_param, args);
 			vm_create_popup_vue.form_data = vm_create_popup_param;
 			if(callback) callback();
		}

	</script>