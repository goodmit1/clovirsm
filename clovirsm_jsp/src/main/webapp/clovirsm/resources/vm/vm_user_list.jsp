<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px">
	<div id="vmUserTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var vmUserReq = new Req();
	vmUserTable;

	$(function() {
		var
		vmUserTableColumnDefs = [ {
			headerName : '<spring:message code="FM_USER_USER_NAME" text="이름" />',
			field : 'USER_NAME',
			width: 160
		},{
			headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
			field : 'TEAM_NM',
			width: 200
		},{
			headerName : '<spring:message code="FM_USER_POSITION" text="직급" />',
			field : 'POSITION',
			width: 200
		},{
			headerName : '<spring:message code="NC_VM_USER_LOGIN_ID" text="사번" />',
			field : 'LOGIN_ID',
			width: 250
		},{
			headerName : '<spring:message code="NC_VM_USER_USER_IP" text="IP" />',
			field : 'USER_IP',
			width: 200
		},{
			headerName : '<spring:message code="NC_VM_USER_PORT" text="Port" />',
			field : 'PORT',
			width: 130,
			cellStyle:{'text-align':'right'}
		}  ];
		var
		vmUserTableGridOptions = {
			hasNo : true,
			columnDefs : vmUserTableColumnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: false,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		vmUserTable = newGrid("vmUserTable", vmUserTableGridOptions);
	});

	function vmUserSearch(){
		if(vmReq.getData() && vmReq.getData().VM_ID){
			var VM_ID = vmReq.getData().VM_ID;
			vmUserReq.searchSub('/api/vmuser/list', {VM_ID: VM_ID}, function(data) {
				vmUserTable.setData(data);
			});
		}
	}

	// 엑셀 내보내기
	function exportExcel(){
		exportExcelServer("mainForm", '/api/vm/user_list_excel', 'VMUserList',vmUserListTable.gridOptions.columnDefs, vmUserReq.getRunSearchData())
	}

</script>


