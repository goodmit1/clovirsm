<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 입력 Form -->
<form id="vm_detail_form">
	<jsp:include page="vm_detail_wrap_hide.jsp"/>
	<div id="vm_detail_wrap" class="form-panel panel panel-default detail-panel">
		<div class="panel-body">
			<input id="D_VM_ID" name="VM_ID" type="hidden" v-model="form_data.VM_ID" />
			<div class="col col-sm-12" style="border-left-color: transparent;border-right-color: transparent;">
				<div class="fm-output">
					<label class="control-label grid-title value-title" style="background-color:white;">서버정보</label>
				</div>
			</div>
			<div class="col col-sm-6">
				<fm-output id="VM_NM" name="VM_NM"    title="<spring:message code="NC_VM_VM_NM" text="서버명" />"  ></fm-output>
			</div>
			<div class="col col-sm-6">
				<fm-output id="GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명" />"></fm-output>
			</div>

			<div class="col col-sm-6">
				<fm-output id="SPEC_INFO" name="OLD_SPEC_INFO" title="<spring:message code="NC_VM_SPEC_SPEC_INFOM" text="사양" />" :value="getServerSpecinfo(form_data)" :ostyle="getSpecinfoStyle(form_data)"></fm-output>
			</div>
			<div class="col col-sm-6">
				<fm-output   id="P_KUBUN" emptystr=" "  :value="  nvl( form_data.CATEGORY_NM,'') + ' > '   + nvl(form_data.P_KUBUN_NM,'')  + ' '  + nvl(form_data.PURPOSE_NM,'')"
							 name="P_KUBUN_NM" title="<spring:message code="TASK" text="업무"/>">
				</fm-output>
			</div>

			<div class="col col-sm-6">
				<fm-output id="PURPOSE" name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도" />"></fm-output>
			</div>
			<div class="col col-sm-6">
				<fm-output id="P_KUBUN_NM" name="P_KUBUN_NM" title="<spring:message code="NC_VM_P_KUBUN" text="구분" />"></fm-output>
			</div>

			<div class="col col-sm-6">
				<fm-output id="INS_TMS" name="INS_TMS" title="<spring:message code="NC_VM_INS_TMS" text="등록일시" />" :value="formatDate(form_data.INS_TMS, 'datetime')"></fm-output>
			</div>
			<div class="col col-sm-6">
				<fm-output id="INS_ID_NM" name="INS_ID_NM" title="<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />"></fm-output>
			</div>

			<div class="col col-sm-6">
				<fm-output id="TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_CD" text="부서" />"></fm-output>
			</div>

			<div class="col col-sm-6">
				<fm-output id="TEAM_MNGRS" name="TEAM_MNGRS" title="<spring:message code="FM_TEAM_TEAM_MNGRS" text="부서관리자" />"></fm-output>
			</div>

			<div class="col col-sm-12">
				<div style="height: 150px;">
					<label for="CMT" class="control-label grid-title value-title"><spring:message code="NC_VM_CMT" text="설명" /></label>
					<div v-html="cmtValue(form_data.CMT)" class="output hastitle" style="min-height: 150px;overflow-y: auto;word-break: break-word;"></div>
				</div>
			</div>
			<div class="col col-sm-6">
				<fm-output id="PRIVATE_IP" name="PRIVATE_IP" title="<spring:message code="NC_VM_PRIVATE_IP" text="VM IP" />"></fm-output>
			</div>
			<div class="col col-sm-6">
				<fm-output id="MAC_ADDR" name="MAC_ADDR" title="<spring:message code="NC_IP_MAC_ADDR" text="VM MAC" />"></fm-output>
			</div>
			<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">
				<fm-output id="PORT" name="PORT" title="<spring:message code="NC_THIN_CLIENT_PORT" text="접속포트" />"></fm-output>
			</div>
			<div class="col col-sm-12">
				<div>
					<label for="SEC_PRECAUTION_CMT" class="control-label grid-title value-title" style="min-height: 90px; padding-top: 26px;"><spring:message code="NC_OS_TYPE_SEC_PRECAUTION_CMT" text="보안 유의사항 확인" /></label>
<%--					<div v-html="cmtValue(form_data.SEC_PRECAUTION_CMT)" class="output hastitle" style="min-height: 150px;overflow-y: auto;word-break: break-word;"></div>--%>
					<div class="output  hastitle">
<%--						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>보안 유의사항 확인</label><br/>--%>
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/>
						<div v-html="cmtValue(form_data.SEC_PRECAUTION_CMT)" style="overflow-y: auto;word-break: break-word; display: inline; white-space: initial"></div>
					</div>
				</div>
			</div>

			<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'"
				 style="border-left-color: transparent;border-right-color: transparent;">
				<div class="fm-output">
					<label class="control-label grid-title value-title" style="background-color:white;">접속 단말 정보</label>
				</div>
			</div>
			<div class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">
				<fm-output id="MODEL_NM" name="SN" title="<spring:message code="NC_THIN_CLIENT_SN" text="SN" />"></fm-output>
			</div>
			<div class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">
				<fm-output id="MODEL_NM" name="MODEL_NM" title="<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />"></fm-output>
			</div>
			<div class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">
				<fm-output id="MAC_ADR" name="MAC_ADR" title="<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />"></fm-output>
			</div>
			<div class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">
				<fm-output id="IP" name="IP" title="<spring:message code="NC_THIN_CLIENT_IP" text="IP" />"></fm-output>
			</div>
			<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'"
				 style="border-left-color: transparent;border-right-color: transparent;">
				<div class="fm-output">
					<label class="control-label grid-title value-title" style="background-color:white;">접속 단말 보안 예외 신청 항목</label>
				</div>
			</div>
			<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">
				<div class="fm-output">
					<label class="control-label grid-title value-title">NAC 설치</label>
					<div class="output  hastitle">
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right: 5px;"/><label>NAC 설치 예회 신청</label>
					</div>
				</div>
			</div>
			<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">
				<div class="fm-output">
					<label class="control-label grid-title value-title">무결성 검사</label>
					<div class="output  hastitle">
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right: 5px;"/><label>무결성 검사 예외 신청</label>
					</div>
				</div>
			</div>
			<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">
				<div class="fm-output">
					<label class="control-label grid-title value-title" style="height:180px;">무결성 검사 예외<br/> 보안 프로그램</label>
					<div class="output  hastitle">
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>백신(V3)</label><br/>
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>백신(알약)</label><br/>
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>매체제어(nProtect)</label><br/>
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>문서보안</label><br/>
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>출력문보안</label><br/>
						<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>자산관리</label>
					</div>
				</div>
			</div>
			<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">
				<fm-output id="THIN_REQ_CMT" name=THIN_REQ_CMT title="<spring:message code="NC_THIN_CLIENT_THIN_REQ_CMT" text="예외사유" />"></fm-output>
			</div>



		</div>

	</div>
</form>
<script>

	var vm_detail_vue = new Vue({
		el: '#vm_detail_form',
		data: {
			form_data: {

			}
		}
	});

	function recreateVM(){
		if(confirm("<spring:message code="NC_VM_recreate_confirm_msg" text="vcenter에 해당 서버가 없어야 합니다. 있다면 동일 이름으로 추가 생성됩니다. 계속하시겠습니까?" />"))
		{
			post('/api/vm/recreate',{VM_ID:form_data.VM_ID}, function(){
				alert(msg_complete);
				search();
			})
		}
	}

	function cmtValue(cmt){

		var cmtArr = String(cmt).split("\n");

		var str = "";
		if(cmtArr.length > 1){
			for(var i = 0 ; i < cmtArr.length ; i++){
				str += cmtArr[i] + "<br>";
			}
		} else{
			if(cmt != null && cmt != 'undefined')
				str = cmt;
		}
		return str;
	}
</script>
