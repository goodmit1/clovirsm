<%@page import="com.fliconz.fm.mvc.util.MsgUtil"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.clovirsm.service.admin.CategoryMapService"%>
<%@page import="com.clovirsm.service.popup.CategorySearchPopupService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%
CategoryMapService categoryMapService = (CategoryMapService)SpringBeanUtil.getBean("categoryMapService");
Map searchParam = new HashMap();
searchParam.put("ID", session.getAttribute("_USER_ID_"));
searchParam.put("KUBUN","U");
request.setAttribute("MENU_TITLE", MsgUtil.getMsgWithDefault("title_new_vra" + request.getParameter("KUBUN") , new String[]{}, "신규 배포 요청"));
%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm"  >
    	<jsp:include page="list.jsp"></jsp:include>
    	</form>
    </layout:put>
    <layout:put block="popup_area"> 
	</layout:put>
</layout:extends>