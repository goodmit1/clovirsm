<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.clovirsm.service.admin.CategoryMapService"%>
<%@page import="com.clovirsm.service.popup.CategorySearchPopupService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
	#changeTask {
		color: white;
		background-color: #757984;
		margin-left: 10px;
		padding: 9px;
		font-weight: 900;
		display: inline-block;
		font-size: 12px;
		cursor: pointer;
		position: relative;
		top: 0px;
		width: 120px;
		text-align: center;
		height: 32px;
	}
</style>
<div id="input_area" style="border: none;">
	<div class="scrollCategoryTeb" id="scrollCategoryTeb" v-if="${ADMIN_YN eq 'Y'} ">
		<div class="categoryItem select" data-id="0" onclick="clickCategory('0');" id="category0">
			<div class="categoryLabel">All Items </div>
		</div>
		<div class="categoryItems" id="categoryItems" style="display: inline-block;"></div>
		<div class="categoryBut Prv" id="categoryButPrv"><i></i></div>
		<div class="categoryBut Next" id="categoryButNext"><i></i></div>
	</div>
	<div class="selectedCategory" id="selectedCategory" style=" ">
	</div>

	<div class="listSearchBar">
		<div style="display: inline-block; width: 120px; float: left;  margin-right: -3px;  height: 40px;">
			<fm-select id="S_SEARCH"  emptystr="" select_style="width: 100%; height: 32px; padding: 6px; border-width: 1px; border-style: solid; border-color: #a4a7a9; border-radius: 0px; color: #a4a7a9; background-color: white;"></fm-select>
		</div>
		<div style="display: inline-block; width:500px; float: left;  height: 40px;">
			<fm-input id="S_APP_NM" name="APP_NM" input_style="width: 100%; height: 32px; padding: 7px; border-width: 1px; border-style: solid; border-color: #a4a7a9; border-radius: 0px; background-color: white; color: #a4a7a9;" onkeydown="enterCheck();" onkeyup="KeyupSearchItem();"></fm-input>
		</div>
		<div class="search-bar">
		</div>
		<%-- 		<div  id="changeTask" v-if="${ADMIN_YN != 'Y'}"><div  ><spring:message code="CHANGE_REGISTRATION_TASK" text="담당업무 변경"/></div></div> --%>
	</div>
	<div class="template_info_display"><span><spring:message code="NO_TEMPLATES" text="템플릿이 없으십니까?" /></span>
		<span><spring:message code="REQ_MSG_M7" text="자주 사용할 경우는" /></span>
		<a href="/clovirsm/workflow/templateAdd/index.jsp" style="color: black; font-style: oblique; text-decoration: underline; cursor: pointer;"><spring:message code="TEMPLATES_REQ_BTN" text="템플릿 추가 요청" /></a>
		<span><spring:message code="REQ_MSG_M8" text="을 하시면 관리자가 작업하여 추가해 드립니다. " /></span>
	</div>
	<div id="listContext" class="listContext"></div>
	<fm-popup-button style="display: none;" popupid="newVRAPopup" popup="/clovirsm/popup/new_vra_popup.jsp?action=insert" param="info"></fm-popup-button>
	<fm-popup-button style="display: none;" popupid="topology" popup="/clovirsm/popup/vratopology.jsp" param="info"></fm-popup-button>
</div>
<script>

	var infoPram = new Object();
	var vraItem = new Array();
	var vraItemArr = new Array();
	var vraItemSearchArr = new Array();
	var vraSearchObj = new Object();
	var vraSearchArr = new Array();
	var categoryList = new Array();
	var categoryObject = null;
	var start = 0;
	var end = 50;
	var req = new Req();
	var info;
	// -- 검색 Select바 DATA
	vraSearchObj.name = '<spring:message code="name" text="명칭" />';
	vraSearchObj.value = 'name';
	vraSearchArr[0] = vraSearchObj;
	vraSearchObj = new Object();
	vraSearchObj.name = '<spring:message code="NC_VRA_CATALOG_CMT" text="설명" />';
	vraSearchObj.value = 'CMT';
	vraSearchArr[1] = vraSearchObj;
	// --

	$( document ).ready(function() {
		if(ADMIN_YN == "Y"){
			chekLeftScrollWid();
			$(window).resize(function (){
				chekLeftScrollWid();
			});
		}
		categoryListPut();
		searchVra();

		fillOptionByData("S_SEARCH",vraSearchArr,"<spring:message code="NC_LICENSE_LICENSE_TOTAL" text="전체" />","value","name");


		$("#categoryButPrv").on("click",function(){
			var scrollChange = $("#scrollCategoryTeb").scrollLeft()-300;
			$("#scrollCategoryTeb").stop().animate({scrollLeft:scrollChange},600);
			chekLeftScroll();

		});
		$("#categoryButNext").on("click",function(){
			var scrollChange = $("#scrollCategoryTeb").scrollLeft()+300;
			$("#scrollCategoryTeb").stop().animate({scrollLeft:scrollChange},600);
			chekLeftScroll();
		});
		$("#changeTask").on("click",function(){
			location.href="/clovirsm/resources/vra/newTask/index.jsp";
		});

		$("#listSearchBarClear").on("click",function(){
			$('#S_APP_NM').val("");
			KeyupSearchItem();
		});

		$(window).scroll(function(event){
			chekTopScroll();
		});
	});

	function clickCategory(id, sub, that){
		$(".scrollCategoryTeb").children().removeClass("select");
		$(".scrollCategoryTeb").children().children().removeClass("select");
		$("#category"+id).addClass("select");
		start = 0;
		end = 50;
		CATEGORY_ID = id;
		CATEGORY_FULL = $("#category"+id).attr("data-nm");
		CATEGORY_IDS = $("#category"+id).attr("data-ids");
		if(ADMIN_YN == "Y"){
			if(CATEGORY_ID != 0){
				categoryListDraw(CATEGORY_ID);
			} else{
				categoryListDraw(CATEGORY_ID);
			}
			if(sub){
				var category = searchCategory("CATEGORY", id);
				CATEGORY_FULL = category.CATEGORY_FULL;
				CATEGORY_IDS = category.CATEGORY_IDS;
			}
			selectedCategoryList(CATEGORY_FULL,CATEGORY_IDS, sub, that);
			searchVra(CATEGORY_IDS);
		} else{
			searchVra(CATEGORY_IDS);
		}



	};
	function clickCategorySub(that,id){


		clickCategory(id, true, that);
	}
	function selectedCategoryList(arr, idsArr, sub, that){
		if(arr == null && sub == null)
			$("#selectedCategory").empty();
		else{
			if(!sub){
				var list = arr.split(">");
				var listIds = idsArr.split(",");
				for(var i = 0 ; i < list.length ; i++){
					var html ="";
					if(i == 0)
						html += "<div class='categoryItem sub' onclick='clickCategorySub(this,0)'><div class='Item'>"+list[i]+"</div></div>";
					else
						html += "<div class='categoryItem sub' onclick='clickCategorySub(this,"+listIds[i-1]+")'><div class='Item'>"+list[i]+"</div></div>";
				}
				$("#selectedCategory").append(html);
			} else{
				var index = $(that).index();


				$("#selectedCategory").children().eq(index).nextAll().remove();
				$("#selectedCategory").children().eq(index).remove();
			}
		}
	}
	function searchCategory(key, value){
		var searchCategory = new Object();
		for(var i = 0 ; i < categoryList.length ; i++){
			if(categoryList[i][key] == value){
				searchCategory = categoryList[i];
			}
		}
		return searchCategory;
	}
	function categoryListDraw(key){
		$("#categoryItems").empty();
		if(key == null){
			key = 0;
		}
		for(var i = 0 ; i < categoryList.length ; i++){
			if(ADMIN_YN == "Y"){
				if(categoryList[i].PAR_CATEGORY_ID == key){
					var html = "";
					html += '<div class="categoryItem" data-ids="'+categoryList[i].CATEGORY_IDS+'" data-nm="'+categoryList[i].CATEGORY_FULL+'" data-id="'+categoryList[i].CATEGORY+'" onclick="clickCategory('+categoryList[i].CATEGORY+',false);" id="category'+categoryList[i].CATEGORY+'">';
					html += '<div class="categoryLabel">'+categoryList[i].TITLE+'</div>';
					html += '</div>';
					$("#categoryItems").append(html);
				}
			}
// 		else{
// 			var html = "";
// 			html += '<div class="categoryItem" data-ids="'+categoryList[i].CATEGORY_IDS+'" data-nm="'+categoryList[i].CATEGORY_FULL+'" data-id="'+categoryList[i].CATEGORY+'" onclick="clickCategory('+categoryList[i].CATEGORY+',false);" id="category'+categoryList[i].CATEGORY+'">';
// 			html += '<div class="categoryLabel">'+categoryList[i].TITLE+'</div>';
// 			html += '</div>';
// 			$("#categoryItems").append(html);
// 		}
		}
		if(ADMIN_YN == "Y"){
			chekLeftScrollWid();
		}
	}

	function categoryListPut(){



		post("/api/vra_catalog/list/list_NC_CATEGORY_MAP/",search_data,function(data){

			if(data.length > 0){
				for(var i = 0 ; i < data.length ; i++){
					categoryObject = new Object();
					categoryObject = { "CATEGORY" :data[i].CATEGORY_ID, "PAR_CATEGORY_ID" : data[i].PAR_CATEGORY_ID, "TITLE" : data[i].CATEGORY_FULL_NM, "CATEGORY_FULL" : data[i].CATEGORY_FULL_NM, "CATEGORY_IDS": data[i].CATEGORY_IDS};
					categoryList.push(categoryObject);
				}
				categoryListDraw();
			}  else{
// 				 
// 				FMAlert("<spring:message code="task_err" text="담당 업무가 없습니다. HySAC에서 등록하시기 바랍니다." />:",'<spring:message code="msg_jsf_warn" text="경고" />',null, null );
			}
		});

	}


	function drawTopology(obj)
	{
		var id = $(obj).attr("data-id");
		var idx = $(obj).attr("data-index");
		info = vraItemArr[idx];
		$('#topology_button').click();
	}
	function openReq(obj)
	{
		var id = $(obj).attr("data-id");
		var idx = $(obj).attr("data-index");
		if(vraItemSearchArr.length != 0){
			info = vraItemSearchArr[idx];
		} else{
			info = vraItemArr[idx];
		}
		info.CATEGORY_IDS=search_data.PAR_CATEGORY_ID
		$('#newVRAPopup_button').click();

	}
	function enterCheck(){
		if (event.keyCode == 13) {
			event.preventDefault();
		};
	}
	// 검색 input에 keyup이 있을 때 검색 해주는 메소드
	function KeyupSearchItem(e){
		$(".listContext").empty();
		vraItemSearchArr = new Array();
		var searchFilter = $("#S_SEARCH").val();
		var appNm = $("#S_APP_NM").val();
		appNm = appNm.toLowerCase();
		if(searchFilter == 'name'){
			for(var i=0; i<vraItemArr.length; i++){
				var nm = isNull(vraItemArr[i].CATALOG_NM) ? "" : vraItemArr[i].CATALOG_NM;
				nm = nm.toLowerCase();
				if(nm.indexOf(appNm) != -1)
					vraItemSearchArr.push(vraItemArr[i]);
			}
		} else if(searchFilter == 'CMT'){
			for(var i=0; i<vraItemArr.length; i++){
				var cmt = isNull(vraItemArr[i].CATALOG_CMT) ? "" : vraItemArr[i].CATALOG_CMT;
				cmt = cmt.toLowerCase();
				if(cmt.indexOf(appNm) != -1)
					vraItemSearchArr.push(vraItemArr[i]);
			}
		} else{
			for(var i=0; i<vraItemArr.length; i++){
				var cmt = isNull(vraItemArr[i].CATALOG_CMT) ? "" : vraItemArr[i].CATALOG_CMT;
				cmt = cmt.toLowerCase();
				var nm = isNull(vraItemArr[i].CATALOG_NM) ? "" : vraItemArr[i].CATALOG_NM;
				nm = nm.toLowerCase();
				if(cmt.indexOf(appNm) != -1 || nm.indexOf(appNm) != -1)
					vraItemSearchArr.push(vraItemArr[i]);
			}
		}
		if(vraItemSearchArr.length != 0){
			end= 50;

			drawItem(vraItemSearchArr);
			drawItemBackground(vraItemSearchArr);
		}
	}


	// 스크롤바 위치를 체크하여 categoryBut을 보여주거나 안보여주는 메소드
	function chekLeftScroll(){
		setTimeout(function() {
			if($("#scrollCategoryTeb").scrollLeft() != 0){
				$("#categoryButPrv").css("display","inherit");
				$("#categoryButNext").css("display","inherit");
			} else{
				$("#categoryButPrv").css("display","none");
				$("#categoryButNext").css("display","inherit");
			}
		}, 700);
	}



	function chekTopScroll(){
		var elmnt = document.getElementById("html");
		if(vraItem.length > 0){
			if($("#html").scrollTop() > (elmnt.scrollHeight - elmnt.clientHeight )-50){
				start = end;
				if(vraItem.length/50 < 2){
					end = end + vraItem.length % end;
				} else{
					end += 50;
				}
				drawItem(vraItem);
				drawItemBackground(vraItem);
			}
		}
	}


	// 창크기를 확인하여 스크롤이 생기는지 안생기는지 확인하는 메소드
	function chekLeftScrollWid(){
		var elmnt = document.getElementById("scrollCategoryTeb");
		if (elmnt.offsetWidth < elmnt.scrollWidth) {
			chekLeftScroll();
		} else {
			$("#categoryButPrv").css("display","none");
			$("#categoryButNext").css("display","none");
		}
	}



	// ajax로 데이터를 가져오는 메소드
	function searchVra(id){

		if(id == 0 || id == null){
			search_data.PAR_CATEGORY_ID = null;
		} else{
			search_data.PAR_CATEGORY_ID = id ;
		}
		search_data.ORDER_CD = "A"

		vraItemArr = new Array();
		$.ajax({
			type:"post",
			data: search_data,
			url:"/api/vra_catalog/list/list_NC_VRA_CATALOG/",
			success: function(data) {
				$(".listContext").empty();
				vraItemArr = dataSave(data);
				drawItem(data);
				drawItemBackground(data);
			}
		});
	}

	// 데이터를 array에 넣어주는 메소드
	function dataSave(data){
		vraItem = new Array();
		for(var i=0; i< data.length; i++){
			vraItemArr[i] =  data[i];
		}
		vraItem = data;
		return vraItem;
	}



	function drawItemBackground(data){
		if(data.length < end){
			end = data.length;
		}
		for(var i=start; i< end; i++){
			var html = "";
			var bg = data[i].hasOwnProperty("ICON_COLOR") ? data[i].ICON_COLOR : ' ';
			$(".listContext").children(".listContextItem").children().children(".listContext-bar-icon").eq(i).css("background-color", bg);
			$(".listContext").children(".listContextItem").children().children(".listContext-bar-icon").eq(i).css("background-image", "url( \'"+data[i].ICON+"\' )");
		}
	}


	// 데이터를 가져와 Item을 그려주는 메소드
	function drawItem(data){
		if(data.length < end){
			end = data.length;
		}
		for(var i=start; i< end; i++){
			var html = "";
			var DEPLOY_TIME = nvl(data[i].DEPLOY_TIME,10) ;
			var cmt = data[i].CATALOG_CMT == null ? " " : escapeHtml(data[i].CATALOG_CMT);
			var cmtArray = cmt.split("\n");
			var tag = '';
			var speck = '';
			if(data[i].hasOwnProperty("FIXED_JSON")){
				var fixed = JSON.parse(data[i].FIXED_JSON);
				if(fixed.hasOwnProperty("image"))
					tag += '<div class="itemTag image" id="itemTag"> ' + fixed.image.toUpperCase() + '</div>';
				if(fixed.hasOwnProperty("_purpose"))
					tag += '<div class="itemTag purpose" id="itemTag"> ' + fixed._purpose.toUpperCase() + '</div>';
				if(fixed.hasOwnProperty("cpu"))
					speck += '<div class="speck">'+fixed.cpu+'CPU</div>'
				if(fixed.hasOwnProperty("memory"))
					speck += '<div class="speck">'+fixed.memory+'GB</div>'
			}
			tag += '<div class="itemTag deployTime" id="itemTag"> ' + DEPLOY_TIME + '분</div>';
			for(var z = 0 ; z < cmtArray.length ; z++){
				cmtArray[z] = cmtArray[z] == null ? '' : cmtArray[z];
				html += "<div>"+ cmtArray[z]+"</div>";
			}
			$(".listContext").append('<div class="listContextItem" id="itemCode"> '+
					'<div class="listContext-center">'+
					'<div class="listContext-bar-icon" style="background-repeat: no-repeat;background-size: 40px 40px; background-position: center;">'+
					'</div>'+
					'<div class="listContext-subTitle">'+ isEmptyNm(data[i].CATEGORY_NM)  +'</div>'+
					'<div class="listContext-title">'+data[i].CATALOG_NM+'</div>'+
					'<div class="listContext-contents">'+
					'<div class="itemTags" id="itemTags'+i+'"></div>'+
					'<div style="margin-top: 10px;" class="specks">'+speck+'</div>' +
					'<div style="height:100px;">'+html+'</div>'+
					//     									'<div class="listContext-pay" style="text-align: center; max-width: 99%; font-weight: 900;"><spring:message code="label_VRA_FEE" text="" /> : '+formatNumber(data[i].FEE)+'</div>'+
					'</div>'+
					'<div id="listContext-bottom'+i+'"class="listContext-bottom">'+
					'</div></div>');
			$("#listContext-bottom"+i+"").append('<i class="addListBtn" onclick="openReq(this);return false;" data-index="'+ i +'" data-id="'+data[i].CATALOG_ID+'" id="'+data[i].CATALOG_ID+'">요청</i>');

			$("#itemTags"+i).append(tag);
		}

	}







</script>


