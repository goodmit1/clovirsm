<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm"  >
			<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
			<style>

				.Editor-editor {
	    			height: 200px;
	    		}
	    		body {
	    			min-width: 0px;
	    		}
	   		</style>
			<script src="/res/js/editor.js"></script>
			<jsp:include page="list.jsp" />
    	</form>
    </layout:put>
    <layout:put block="popup_area">
	</layout:put>
</layout:extends>