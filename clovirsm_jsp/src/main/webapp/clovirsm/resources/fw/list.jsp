<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style>
	#input_area > div.form-panel.detail-panel.panel.panel-default > div > div.col.col-sm-12 > div > div{
		-ms-overflow-y: auto;
		-ms-word-break: break-all;
	}
</style>
<!--  서버 목록 조회 -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm">
			<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="" />"></fm-select>
		</div>
		<div class="col col-search">
			<fm-input id="keyword_input" class="keyword" name="keyword"
				placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_FW_PUBLIC_IP" text="서버 IP" />/<spring:message code="NC_FW_CIDR" text="방화벽 IP" />/<spring:message code="FM_USER_LOGIN_ID" text="사번" />/<spring:message code="NC_FW_USER_NAME" text="사용자" />/<spring:message code="NC_FW_INS_ID_NM" text="담당자" />"
				title="<spring:message code="keyword_input" text="키워드" />">
			</fm-input>
			<fm-sbutton cmd="search" class="searchBtn" onclick="fwSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		<div class="col btn_group_under">
			<fm-popup-button v-show="false" popupid="fw_req_info_popup" popup="../../popup/fw_detail_form_popup.jsp" cmd="update" class="exeBtn" param="fwReq.getData()"><spring:message code="label_fw_info" text="접근제어 정보"/></fm-popup-button>

			<%-- <fm-popup-button popupid="fw_insert_popup" popup="../../popup/fw_detail_form_popup.jsp?action=insert&callback=fwSearch" cmd="update" class="newBtn" param="{}"><spring:message code="btn_insert_req" text="신규 요청"/></fm-popup-button> --%>

			<fm-popup-button popupid="fw_delete_popup" popup="../../popup/delete_req_form_popup.jsp?svc=fw&key=FW_ID&callback=fwSearch" cmd="delete" class="delBtn" param="fwReq.getData()" disabled="disabled"><spring:message code="btn_delete_req" text="삭제요청"/></fm-popup-button>

			<fm-popup-button popupid="fw_update_popup" popup="../../popup/fw_detail_form_popup.jsp?action=update&callback=fwSearch" cmd="update" class="exeBtn" param="fwReq.getData()" disabled="disabled"><spring:message code="btn_update_req" text="변경 요청"/></fm-popup-button>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:340px">
	<div class="table_title layout">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="fwGrid_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="fwGrid_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="fwGrid" class="ag-theme-fresh" style="height: 400px" ></div>
	</div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
	var fwReq = new Req();
	fwGrid;
	$(function() {
		var
		columnDefs = [ {
			headerName : '',
			hide : true,
			field : 'VM_ID'
		},{
			headerName : '<spring:message code="NC_DC_DC_NM" text="데이터센터명" />',
			field : 'DC_NM',
			maxWidth: 120,
			width: 120,
			headerTooltip:'<spring:message code="NC_DC_DC_NM" text="데이터센터명" />',
		},{
			headerName : '<spring:message code="NC_VM_VM_NM" text="서버명" />',
			field : 'VM_NM',
			tooltip: function(params){
				 if(params.data){
					 return params.data.VM_NM;
          		  }
      		  	return "";
	         },
			width: 180
		},{
			headerName : '<spring:message code="NC_FW_SERVER_IP" text="서버 IP" />',
			field : 'SERVER_IP',

			width: 130
		},{
			headerName : '<spring:message code="NC_FW_USER_IP" text="사용자" />',
			field : 'CIDR',

			width: 130
		},{
			headerName : '<spring:message code="allowedPort" text="허용 포트" />',
			field : 'PORT',
			width: 130,
			cellStyle:{'text-align':'right'}
		},{
			headerName : '<spring:message code="NC_FW_TEAM_NM" text="대상자 팀" />',
			field : 'TEAM_NM',
			maxWidth: 130,
			width: 130
		},{
			headerName : '<spring:message code="NC_FW_INS_NAME" text="대상자" />',
			field : 'USER_NAME',
			maxWidth: 120,
			width: 120,
			tooltipField:"USER_NAME",
		},{
			headerName : "<spring:message code="NC_FW_TASK_STATUS_CD" text="작업상태" />",
			field : "TASK_STATUS_CD",
			width: 120,
			headerTooltip:'<spring:message code="NC_FW_TASK_STATUS_CD" text="작업상태" />',
			valueGetter: function(params) {
				if(params.data && params.data.TASK_STATUS_CD){
					return getStatusMsg(params.data);
          		  }
      		  	return "";
			}
		},
		
		<c:if test="${'Y'.equals(sessionScope.VM_USER_YN)}">{
			headerName : 'OS <spring:message code="NC_FW_VM_USER_YN" text="계정생성여부"/>',
			field : 'VM_USER_YN',
			cellStyle:{'text-align':'left'},
			width: 140,
			headerTooltip:'<spring:message code="NC_FW_VM_USER_YN" text="계정생성여부"/>',
		},</c:if>
		
		{
			headerName : '<spring:message code="NC_FW_USE_MM_M" text="사용 기간(개월)" />',
			field : 'USE_MM',
			width: 120,
			cellStyle:{'text-align':'right'},
			headerTooltip:'<spring:message code="NC_FW_USE_MM_M" text="사용 기간(개월)" />',
			valueFormatter:function(params){
				if(params.value){
					return formatNumber(params.value);
          		  }
      		  	return "";
			}
		},{
			headerName : '<spring:message code="NC_FW_CMT" text="설명" />',
			field : 'CMT',
			hide: true,

			width: 220
		},{
			headerName : '<spring:message code="mgr_team" text="담당자팀" />',
			field : 'INS_TEAM_NM',
			maxWidth: 130,
			width: 130
		},{
			headerName : '<spring:message code="mgr_name" text="담당자" />',
			field : 'INS_NAME',
			maxWidth: 120,
			width: 120,
			tooltipField:"INS_NAME",
		},{
			headerName : '<spring:message code="NC_VM_INS_TMS" text="등록일시"/>',
			field : 'INS_DT',

			width: 180,
			valueGetter: function(params) {
				if(params.data && params.data.INS_DT){
					return formatDate(params.data.INS_DT,'datetime')
          		  }
      		  	return "";
		    	
			}
		}/*,{
			headerName : '<spring:message code="FM_TEAM_INS_TEAM_NM" text="담당자팀" />',
			field : 'INS_TEAM_NM',
			maxWidth: 130,
			width: 130
		},{
			headerName : '<spring:message code="NC_FW_INS_NAME" text="담당자" />',
			field : 'INS_NAME',
			maxWidth: 120,
			width: 120
		}  */];
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: true,
			onSelectionChanged: function(){
				var arr = fwGrid.getSelectedRows();
				fwReq.getInfo('/api/fw/info?FW_ID='+ arr[0].FW_ID, function(data){
					setButtonClickable('fw_save_popup_button', isRequestable(data));
					setButtonClickable('fw_update_popup_button',   isRequestable(data));
					setButtonClickable('fw_delete_popup_button', isDeletable(data) || isRequestable(data));
					setButtonClickable('fw_owner_change_popup_button', isRequestable(data));
				});
			},
			/*onRowDoubleClicked: function(e){
				var data = e.data;
				 
				if(data.CUD_CD){
					if(data.CUD_CD == 'C'){
						$('#fw_insert_save_popup_button').trigger('click');
					} else if(data.CUD_CD == 'U'){
						$('#fw_update_popup_button').trigger('click');
					} else if(data.CUD_CD == 'D'){
						$('#fw_req_info_popup_button').trigger('click');
					} else {
						$('#fw_save_popup_button').trigger('click');
					}
				} else {
					$('#fw_save_popup_button').trigger('click');
				}
			}*/
		}
		fwGrid = newGrid("fwGrid", gridOptions);
		fwSearch();
	});

	function openFWPopup(idx)
	{
		var data = fwGrid.getRowData(idx)
		fwReq.setData(data);
		$('#fw_save_popup_button').trigger('click');

	}
	// 조회
	function fwSearch() {
		
		/* fwReq.search('/api/fw/list', function(data) {
			fwGrid.setData(data);

			setButtonClickable('fw_save_popup_button', false);
			setButtonClickable('fw_update_popup_button', false);
			setButtonClickable('fw_delete_popup_button', false);
			setButtonClickable('fw_owner_change_popup_button', false);
		}); */
		fwReq.searchPaging('/api/fw/list' ,fwGrid, function(data) {
			fwGrid.gridOptions.api.sizeColumnsToFit();
			//fwGrid.setData(data);
			setButtonClickable('fw_save_popup_button', false);
			setButtonClickable('fw_update_popup_button', false);
			setButtonClickable('fw_delete_popup_button', false);
			setButtonClickable('fw_owner_change_popup_button', false);
		});
	}

	//저장
	function fwSave(){
		if(form_data.FW_ID && form_data.FW_ID != null){
			fwReq.save('/api/fw/save', function(){
				alert(msg_complete_work);
				fwSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}


	function onChangedOwner()
	{

	}
	function getDummyFWData(){
		var data = {
			DC_NM: '',
			VM_NM: '',
			PUBLIC_IP: '',
			PRIVATE_IP: ''
		}
		return data;
	}

	// 엑셀 내보내기
	function exportExcel(){
		exportExcelServer("mainForm", '/api/fw/list_excel', 'FireWall',fwGrid.gridOptions.columnDefs, fwReq.getRunSearchData())
	}

</script>


