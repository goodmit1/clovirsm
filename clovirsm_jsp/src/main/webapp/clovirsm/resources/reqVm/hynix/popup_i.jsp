<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<style>
#vm_insert_popup .modal-dialog{
   
	width:972px;

}
</style>
<fm-modal id="${popupid}" title="<spring:message code="btn_server_create" text="서버생성요청"/>" cmd="header-title">
	<span slot="footer" id="${popupid}_footer">
		<input v-if="${isAppr eq 'N'}" type="button" class="btn exeBtn" id="prevBtn" value="<spring:message code="btn_prev" text="이전" />" @click="prev()" />
		<input v-if="${isAppr eq 'N'}" type="button" class="btn exeBtn" id="nextBtn" value="<spring:message code="btn_next" text="다음" />" @click="next()" />
		<input v-if="${isAppr eq 'N'} && ${direct == null && action == 'insert'}  " type="button" class="btn saveBtn" value="<spring:message code="label_confirm" text="확인"/>" onclick="${popupid}_to_work()" />
		<%-- <input v-if="${isAppr == 'N'} && ${direct == null && action == 'insert'}  " type="button" class="btn saveBtn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="${popupid}_request()" /> --%>
		<input v-if="${isAppr eq 'N'} && (${action == 'insert'} && form_data.CUD_CD == 'C') || ${action == 'save'} || ${direct == 'Y'}" type="button" class="btn saveBtn"  value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />
	</span>
	<div class="form-panel detail-panel" >
		<iframe src="/clovirsm/resources/reqVm/_popup.jsp?popupid=${popupid}_iframe&callback=${popupid}_onAfterSave" id="${popupid}Frm" style="width:100%;height:400px;border:none"></iframe>
	</div>
	<fmtags:include page="reqPopupVal.jsp" />
	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param,
				step : 0,
				stepTitle: _popupTitle

			},
			methods:{
				setStepTitle:function ()
				{
					var headerTitle = '<spring:message code="btn_server_create" text="서버생성요청"/>';
					if(this.step > 1) {
						$("#prevBtn").show();
					} else {
						$("#prevBtn").hide();
					}
					$(".saveBtn").hide();
					if(this.step >= 4) {
						$("#nextBtn").hide();
						$(".saveBtn").show();
					} else {
						$("#nextBtn").show();
					}
				},
				next:function() {
					 this.goStep(this.step++, this.step);

				},
				prev:function() {
					 this.goStep(this.step--, this.step);

				},
				goStep: function(hideStep , showStep ) {
					 
					if(hideStep == null && this.step > 0) {
						this.step = 0;
					}
					this.setStepTitle();
					this.step = showStep;
					 
					var obj = document.getElementById("${popupid}Frm");
					if(obj) {
						var objDoc = obj.contentWindow || obj .contentDocument;
						objDoc.showStep(showStep, hideStep);
					}
				}


			}
		});
		var ${popupid}_callback = '${callback}';

		function ${popupid}_click(vue, param){
			 
			if(param && (param.VM_ID || param.IMG_ID)){
				if(param.VM_ID) ${popupid}_vue.form_data.FROM_ID = param.VM_ID;
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;
			}
			return true;
		}

		function ${popupid}_onAfterOpen(){
			${popupid}_vue.goStep(null, 1);
		}

		function ${popupid}_to_work(){
			var obj = document.getElementById("${popupid}Frm");

			var objDoc = obj .contentWindow || obj .contentDocument;


			objDoc.${popupid}_iframe_to_work("#${popupid}");
		}

		function ${popupid}_request(){
			var obj = document.getElementById("${popupid}Frm");

			var objDoc = obj .contentWindow || obj .contentDocument;


			objDoc.${popupid}_iframe_request("#${popupid}");
		}

		function ${popupid}_save(){
			var obj = document.getElementById("${popupid}Frm");

			var objDoc = obj .contentWindow || obj .contentDocument;


			objDoc.${popupid}_iframe_save("#${popupid}");
		}
		function ${popupid}_onAfterSave()
		{
			<c:if test="${callback ne null}">
			${callback}();
			</c:if>
			$('#${popupid}').modal('hide');
		}
	</script>
</fm-modal>