<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fm-modal id="vm_req_popup" title="<spring:message code="label_vm_req" text="서버 생성 요청"/>" >
	<span slot="footer">
		<input type="button" class="btn" style="margin-top: 10px;" value="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="req_to_work()" />
		<input type="button" class="btn" style="margin-top: 10px;" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="req_request()" />
	</span>

	<form id="detailForm" action="none">
		<ul class="nav nav-tabs">
		  	<li class="active">
		        <a  href="#tab1" data-toggle="tab"><spring:message code="label_vm_info" text="VM 정보"/></a>
			</li>
		 	<li >
		        <a  href="#tab2" data-toggle="tab"><spring:message code="label_vm_user" text="VM 사용자"/></a>
			</li>
		</ul>
		<div class="tab-content "  >
			<div style="height:400px" class="form-panel detail-panel tab-pane active" id="tab1" >
				<jsp:include page="vm_input.jsp" />

			</div>
			<div style="height:400px" class="form-panel detail-panel tab-pane" id="tab2" >
				<jsp:include page="user_list.jsp" />

			</div>
		</div>
	</form>
	<script>

	function vm_req_popup_click(vue, param){

	}

	function req_to_work(){
		var param = inputvue.form_data;
		param.NC_VM_USER_LIST = JSON.stringify(reqUserListGrid.getData());
		post('/api/vmReq/insertReq', param,  function(data){
			alert('<spring:message code="saved" text="저장되었습니다"/>');
			if(!confirm(msg_complete_work_continue)){
				location.href="/clovirsm/resources/vm/index.jsp";
				return;
			} else {
				location.href="/clovirsm/workflow/work/index.jsp";
				return;
			}
			return;
		});
	}
	function req_request(){
		var param = inputvue.form_data;
		param.DEPLOY_REQ_YN = 'Y';
		param.NC_VM_USER_LIST = JSON.stringify(reqUserListGrid.getData());
		post('/api/vmReq/insertReq', param, function(data){
			alert('<spring:message code="saved" text="저장되었습니다"/>');
			location.href="/clovirsm/resources/vm/index.jsp";
		});
	}

	</script>
</fm-modal>