<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm" onsubmit="return false">

<div id="taskTable" class="ag-theme-fresh" style="height: 700px" ></div>
<script>
$(document).ready(function(){
	var columnDefs = [{headerName : "<spring:message code="NC_DC_DC_NM" text="데이터센터" />", field : "DC_NM"},
		                {headerName : "<spring:message code="NC_TASK_ING_TASK_ID" text="작업ID" />", field : HypervisorAPI.PARAM_TASK_ID},
		                {headerName : "<spring:message code="NC_TASK_ING_TASK_CD" text="작업 구분" />", field : "TASK_CD_NM"},
		                {headerName : "<spring:message code="NC_TASK_ING_SVC_ID_NM" text="" />", field : "SVC_ID_NM"},
		                {headerName : "<spring:message code="NC_TASK_ING_TASK_TMS" text="작업시작시간" />", field : "TASK_TMS",
		                	valueGetter: function(params) {
					    		return formatDate(params.data.TASK_TMS,'datetime')
							}},
		                {headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="" />", field : "TEAM_NM"}
		                ];
	var gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		//rowModelType: 'infinite',
		//rowSelection : 'single',
		sizeColumnsToFit: true,
		cacheBlockSize: 100,
		rowData : [],
		enableSorting : true,
		enableColResize : true,
		enableServerSideSorting: false,

	}
	mainTable = newGrid("taskTable", gridOptions);
	getTaskInfo()
})

function getTaskInfo() {
	$.get('/api/monitor/task', function(data) {
		data.list=data.LIST;
		mainTable.setData(data);
	});
}
</script>
</form>
</layout:put>
</layout:extends>