<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="termArea table_title">
				<dl>
					<dt class="reload"><input type="text" calss="form-control input hastitle" id="reloadTime"><span><spring:message code="minute" text="" /></span> <img id="isMonitor" onclick="monitorStartAndStop();" src="/res/img/mobis/start_icon.png"> </dt>
					<dt class="title"><spring:message code="dashboard_time_term" text="" /></dt>
					<c:if test='${param.key ne "CLUSTER"}'> <dd id='p_1h' period='1h'>1H</dd></c:if>
					<dd id='p_1d' period='1d'>1D</dd>
					<dd id='p_1w' period='1w'>1W</dd>
					<dd id='p_1m' period='1m'>1M</dd>
					<dd id='p_1y' period='1y'>1Y</dd>
					<dd id="p_select"><spring:message code="select_select" text="" /></dd>

					<span id="hideArea" style="display:none" >
						<table><tr><td>
							<fm-date name="START_DT" id="START_DT" ></fm-date>
						</td>
						<td>
							<select id="START_DT_S">
							<option value="00:00"></option>
								<option>00:00</option>
								<option>01:00</option>
								<option>02:00</option>
								<option>03:00</option>
								<option>04:00</option>
								<option>05:00</option>
								<option>06:00</option>
								<option>07:00</option>
								<option>08:00</option>
								<option>09:00</option>
								<option>10:00</option>
								<option>11:00</option>
								<option>12:00</option>
								<option>13:00</option>
								<option>14:00</option>
								<option>15:00</option>
								<option>16:00</option>
								<option>17:00</option>
								<option>18:00</option>
								<option>19:00</option>
								<option>20:00</option>
								<option>21:00</option>
								<option>22:00</option>
								<option>23:00</option>
							</select>
						</td>
						<td>
							&nbsp~&nbsp
						</td>
						<td>
							<fm-date id="FINISH_DT" name="FINISH_DT" ></fm-date>
						</td>
						<td>
							<select id="FINISH_DT_S">
								<option value="23:59">&nbsp</option>
								<option>00:00</option>
								<option>01:00</option>
								<option>02:00</option>
								<option>03:00</option>
								<option>04:00</option>
								<option>05:00</option>
								<option>06:00</option>
								<option>07:00</option>
								<option>08:00</option>
								<option>09:00</option>
								<option>10:00</option>
								<option>11:00</option>
								<option>12:00</option>
								<option>13:00</option>
								<option>14:00</option>
								<option>15:00</option>
								<option>16:00</option>
								<option>17:00</option>
								<option>18:00</option>
								<option>19:00</option>
								<option>20:00</option>
								<option>21:00</option>
								<option>22:00</option>
								<option>23:00</option>
							</select>
						</td>
						<td>
							<button type="button" title="reload" onclick="reloadVMPerf()" class="btn"><i class="fa fa-search"></i></button>
						</td>
						</tr>
						</table>

					</span>

					<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" style="float: none;" onclick="excelExportAll('<spring:message code="monitor_perf" text="성능"/>');return false" class="layout excelBtn floatRight marginLeft10 right top0" id="excelBtn"></button>

			</dl>

</div>