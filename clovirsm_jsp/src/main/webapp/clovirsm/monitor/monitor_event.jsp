<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="table_title layout name">
				<spring:message code="count" text="건수"/> : <span id="eventTable_total">0</span>
				<div class="btn_group">
				 		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="eventTable.mainTable.exportCSV({fileName:'alarm'})" class="layout excelBtn right" id="excelBtn"></button>
				</div>

</div>

<div id="eventTable" class="ag-theme-fresh" style="height: 600px" ></div>
<script>
				var eventTable = new Vue({
				  el: '#eventTable',
				  data: {
					  eventTable: []
				  },
				  methods: {

				  	  init:function()
				  	  {
				  		var columnDefs = [
					  		{headerName : "<spring:message code="monitor_COMMENT" text="" />",field : "COMMENT", width:600},
					  		{headerName : "<spring:message code="monitor_CATEGORY" text="" />",field : "CATEGORY"},
					  		{headerName : "<spring:message code="monitor_TARGET" text="" />",field : "TARGET"},
				  			{headerName : "<spring:message code="monitor_CRE_TIME" text="" />",field : "CRE_TIME", format: 'datetime'},
				  			];
				  		var gridOptions = {
							hasNo : true,
							columnDefs : columnDefs,
							//rowModelType: 'infinite',
							//rowSelection : 'single',
							sizeColumnsToFit: true,
							cacheBlockSize: 100,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							enableServerSideSorting: false,

						}
						this.mainTable = newGrid("eventTable", gridOptions);

				  	  },
				  	 getReqInfo: function (param) {
							var that = this;
							post('/api/monitor/event/' + param.DC_ID + '/${param.key}/' + param.name  + '/',  param, function(data)
								{
									data.list=data.LIST;
									that.mainTable.setData(data);
								}
							)
					    }
				  }
				});
				$(document).ready(function(){
					eventTable.init();
				})

</script>