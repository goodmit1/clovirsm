<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<script>

var ${popupid}_param = {};


function showLog(param) {
	if(param == null) {
		alert(msg_select_first);
		return;
	}
    $("#paramLBody div").remove("");
    //_logParam = $.extend({},param);
    
	var p = "field=vmw_object_id&target=" + param.HV_ID 
		$("#logFrame").attr("src","/clovirsm/monitor/_log.jsp?" + p );
		$("#log_popup").modal();
	 
}
function ${param.popupid}_click(vue, param){
	 
	if(param != null){
		${popupid}_param = param;
	} else{
		${popupid}_param = {};
	}
	showLog(param)
	$("#log_popup").on("hidden.bs.modal", function(){
		$("#logFrame").attr("src","");
	})
}
//var _logParam; 

 
function closeLog() {
	$("#logCloseBtn").click();
}

function fullScreenClick(type, btn) {
	if(fullScreenClick[type] == null) {
		fullScreenClick[type] = true;
	}
	
	if(type == "L") {
		var p = "field=vmw_object_id&target=" + ${popupid}_param.HV_ID 
		$("#logFullDisplay iframe").attr("src","/clovirsm/monitor/_log.jsp?" + p);
		$("#logFullDisplay").attr("style","display:block;");
		$("#NofullScrennBtn").attr("style","display:inline-block;");
	} else if(type == "T") {
		$("#logFullDisplay").attr("style","display:none;");
	}
}
</script>
<div id="log_popup" class="modal  fade popup_dialog" role="dialog">
	<div class="modal-dialog  ">
		<!-- Modal content-->
		<div class="modal-content  container-fluid">
			<div class="modal-header header-title">
				<button type="button" id="logCloseBtn" class="close" data-dismiss="modal"></button>
				<spring:message code="title_log" text="" />
			</div>

			<div class="modal-body">
				<div class="col col-sm-12" id="paramLBody" style="min-width:700px">
					<button type="button" id="fullScrennBtn" onclick="fullScreenClick('L', this);"
						style="float:right;border: none;box-shadow: none;background: #FFF;color: #b2b2b2;font-size: 15px;">ZOOM IN</button>
				</div>
				<iframe name="logFrame" id="logFrame" scrolling="no"
					style="height: 550px; width: 100%; border: 0;"></iframe>
			</div>
		</div>
	</div>
</div>
<style>
</style>