<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  서버 목록 조회  -->
<script type="text/javascript" src="/res/js/treemap.js"></script>
<style>
 .select2-container {
	width: 300px !important;
	margin-left: 5px;
}
 
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		 
			 
			<div class="col col-sm">
				<fm-select
					url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC"
					id="S_DC_ID" onchange="getSVCHostMonitorInfo()" keyfield="DC_ID" titlefield="DC_NM"
					name="DC_ID"
					title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
				</fm-select>
			</div>
			 
			<div class="col col-sm">
				<fm-select2
					url="/api/category/map/list/list_NC_CATEGORY_MAP_SELECT_TASK/?ALL_USE_YN=Y"
					id="S_CATEGORY" onchange="drawChart()" keyfield="CATEGORY_ID" :titlefield=["CATEGORY_NM","CATEGORY_CODE"]
					name="CATEGORY" emptyStr=""
					title="<spring:message code="TASK"/>">
				</fm-select2>	
			</div>
			<div class="  col col-sm ">
					<fm-select url="/api/code_list?grp=PURPOSE" id="S_PURPOSE"
						name="PURPOSE"  emptyStr=""  onchange="drawChart()"  
						title="<spring:message code="PURPOSE" text="업무"/>"   > </fm-select>
				</div>
			 
			 
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:640px">
<div class="panel-body">
<div id="monitor_svc_host_count"  class="chart-width-subtitle" style="height:500px"></div>
<div id="monitor_host_count"  class="chart-width-subtitle" style="height:130px"></div>
</div>
</div>

<script>
			<jsp:include page="_common.jsp"></jsp:include>
			function drawHostChart( tagValue, title ){
				var DC_ID=$("#S_DC_ID").val();
				$.get('/api/monitor/list/list_vm_host_count_by_tag/?TAG_VAL=' + tagValue +'&' + getTagUrlParam() + 'DC_ID=' + DC_ID , function(data1)
						{
							 
							drawHostChart1( data1, title )
						});
			}
			function drawHostChart1( data, title  ){
				var chart_data=[];
				for(var i=0; i < data .length; i++){
					 
					var data1 = $.extend(data[i],{name: data[i].HOST_NM, value:data[i].CNT, color:getAreaColor(data[i].CNT)});
					chart_data.push(data1);
					 
				}
				Highcharts.chart('monitor_host_count', {
					   legend: {
					        enabled: false 
					    },

					    tooltip: {
					        formatter: function () {
					            return '<spring:message code="TASK"/> : ' + getTagTaskNm(this.point ) + ' <br>' +
					            'Host: ' + this.point.HOST_NM + ' <br>' +					          
					            'VM: '+   this.point.value ;
					        }
					    },
				    series: [{
				        type: 'treemap',
				        layoutAlgorithm: 'squarified',
				        data:  chart_data,
				        dataLabels: {
				            enabled: true,
				            color: '#000000',
				            style: {
			                    textOutline: false 
			                }
				        }
				    }],
				    title: {
				        text: ""
				    }, 
				    plotOptions: {
				    	series: {
		                	events: {
		                    	click: function (e) {
		                    		 
		                    		 
		                    		var taskNm = getTagTaskNm(e.point );
		                    		var host =e.point.HOST_NM;
		                    		var url = '/api/monitor/list/list_vm_by_host_tag/?' + getTagUrlParam(e.point) + 'DC_ID=' + $("#S_DC_ID").val() + '&HOST_NM=' + host   
		                    		$.getJSON(url, function(data){
		                    			var html = '<div style="font-size:14px;text-align:left">Host : ' + host + "<br>";
		                    			html += '<spring:message code="TASK"/> : ' + taskNm + '<br><br>'
		                    			for(var i=0; i<data.length; i++){
		                    				html += '<li><a  tabindex="-1" href="/clovirsm/monitor/admin_vm/index.jsp?DC_ID='+ e.point.DC_ID + '&keyword=' + data[i].VM_NM + '">' + data[i].VM_NM + '</a></li>'
		                    			}
		                    			html += '</div>'
		                    			//message, title, oklabel, okAction , width, height
		                    			FMAlert(html,'VM',false,false,400,250);
		                    		})
		                    	}
		                	}
				    	}
				    },
				});
			}
			function drawChart( ){
				if(!data) return;
				var chart_data=[];
				for(var i=0; i < data.length; i++){
					if(isTarget(data[i])){
					var data1 = $.extend(data[i],{name:getTagKey(data[i]), value:data[i].VM_CNT, color:getAreaColor(data[i].MAX_VM_CNT)});
					chart_data.push(data1);
					}
				}
				Highcharts.chart('monitor_svc_host_count', {
					   legend: {
					        enabled: false 
					    },

					    tooltip: {
					        formatter: function () {
					            return '<spring:message code="TASK"/> : ' + getTagTaskNm(this.point ) + ' <br>' +
					            'Host: ' + this.point.HOST_CNT + ' <br>' +
					            '중복 최대 : ' + this.point.MAX_VM_CNT + ' <br>' +
					            'VM: '+   this.point.value ;
					        }
					    },
					    series: [{
					        type: 'treemap',
					        layoutAlgorithm: 'squarified',
					        data:  chart_data,
					        dataLabels: {
					            enabled: true,
					            color: '#000000',
					            style: {
				                    textOutline: false 
				                }
					        }
					    }],
					    title: {
					        text: ''
					    },
					    plotOptions: {
					    	series: {
			                	events: {
			                    	click: function (e) {
			                    		drawHostChart(e.point.name, getTagTaskNm(e.point ))
			                    	}
					    		}
					    	}
					    }
				});
			
			}
			var data;
			 
			function getSVCHostMonitorInfo()
			{
				var DC_ID=$("#S_DC_ID").val();
				 
					$.get('/api/monitor/list/list_svc_count_by_tag/?' + getTagUrlParam() + 'DC_ID=' + DC_ID, function(data1)
					{
						data = data1;
						drawChart( )
					});
				 
				
			}
			
			 

		 
			$(document).ready(function(){
				 
			})
			 

			</script>