<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }


input[id="INNER_HOUR"] {
       display: none;
}

input[id="INNER_HOUR"] + label {
  display: inline-block;
  cursor: pointer;
  position: relative;
  top:5px;
  font-size: 13px;
}

input[id="INNER_HOUR"]+ label:before {
  content: "";
  display: inline-block;
  margin-right: 10px;
  /* position: absolute; */
  left: 0;
  bottom: 1px;
  width: 18px;
  height: 18px;
  border-radius: 3px;
  border: solid 2px #929594;
  background-color: #ffffff;

}

input[id="INNER_HOUR"]:checked + label:before {
  padding-top: 0px;
  content: url("/res/img/hynix/newHynix/shape-516.png"); /* 체크모양 */
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
  font-weight: 800;
  color: #ffffff;
  background: #929594;
  text-align: center;
  line-height: 18px;
  position: relative;
  top: -3px;
}

.calendar-btn{
	padding: 5px 6px;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->



			<div class="col col-sm">
					<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID"
						keyfield="DC_ID" titlefield="DC_NM"
						name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>"></fm-select>
			</div>


			<div class="col btn_group nomargin">

			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>


		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
				<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
				<div class="btn_group">
					<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
				</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 250px" ></div>
	</div>
	</div>
	<jsp:include page="detail.jsp"></jsp:include>
	
</div>
<script>
					var req = new Req();
					mainTable;


					$(function() {


						var columnDefs = [{headerName : "<spring:message code="NC_DC_DC_NM"   />",field : "DC_NM"},
							
							{headerName : "<spring:message code="monitor_CLUSTER" text="" />",field : "CLUSTER"},
							{headerName : "<spring:message code="monitor_HOST_CNT" text="" />",field : "HOST_CNT", cellStyle:{'text-align':'right'}},
							//{headerName : "<spring:message code="monitor_COMP_CNT" text="" />",field : "COMP_CNT"},
							{headerName : "<spring:message code="monitor_VM_CNT" text="" />",field : "VM_CNT", cellStyle:{'text-align':'right'}},
							{headerName : "<spring:message code="NC_DC_REAL_DC_NM" text="" />",field : "REAL_DC_NM"},
							];
						var
						gridOptions = {
							hasNo : true,
							columnDefs : columnDefs,
							//rowModelType: 'infinite',
							rowSelection : 'single',
							cacheBlockSize: 100,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							 enableServerSideSorting: false,
							 onSelectionChanged : function() {
									var arr = mainTable.getSelectedRows();
									arr[0].name=arr[0].CLUSTER;
									goTabContent(arr[0])

								},
						}
						mainTable = newGrid("mainTable", gridOptions);
						var today = formatDate(new Date(), 'date');

						req.setSearchData({CONNECTTIME_TO: today, CONNECTTIME_FROM:today});
						setTimeout(function(){ search(); }, 100);
						
					});

					// 조회
					function search() {


						req.search('/api/admin_monitor/list/CLUSTER' , function(data) {
							mainTable.setData(data);
						});
					}

					// 엑셀 내보내기
					function exportExcel()
					{
						  mainTable.exportCSV({fileName:'CLUSTER'})
					}

				</script>


