<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!--	서버 목록 조회 -->
<style>
	#S_VM_NM {
		width: 100%;
	}
</style>

<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm" style="width: 267px;">
			<fm-ibutton id="S_VM_NM" name="S_VM_NM" required="required" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>" class="inline">
				<fm-popup-button popupid="s_vm_search" popup="/clovirsm/popup/vm_search_form_popup.jsp?isActiveOnly=Y" cmd="update" param="" callback="s_select_vm"><spring:message code="btn_vm_search" text="서버검색"/></fm-popup-button>
			</fm-ibutton>
		</div>
		
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		<div class="col btn_group_under">
		</div>
		<div id="popup-button-html">
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:340px">
	<div class="box_s">
		<div class="table_title layout name">
			<div class="search_info">
				<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
			</div>
			<div class="btn_group">
				<fm-sbutton cmd="update" class="contentsBtn tabBtnImg new" onclick="ins()"><spring:message code="btn_new" text="신규"/></fm-sbutton>
				<fm-sbutton cmd="update" class="contentsBtn tabBtnImg save" onclick="save()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
				<fm-sbutton cmd="delete" class="contentsBtn tabBtnImg del" onclick="del()"><spring:message code="btn_delete" text="삭제"/></fm-sbutton>
			</div>
		</div>
		<div class="layout background mid">
			<div id="mainTable" class="ag-theme-fresh" style="height: 500px" ></div>
		</div>
		<jsp:include page="detail.jsp"></jsp:include>
	</div>
</div>
<script>
	var req = new Req();
	mainTable;
	var initData = {
			IDU : "I",
			YEAR : (new Date()).getFullYear(),
			MONTH : (new Date()).getMonth() + 1,
			DAY : (new Date()).getDate(),
			HOUR : 1,
			MIN : 0,
			WEEK : "MON",
			SCHEDULE_SYNC : "S",
			SCHEDULE_CYCLE : "D"
		}
	function initFormData(){
		form_data = $.extend(form_data,initData);
		 
	}
	$(function() {
		var columnDefs = [
			{headerName : "<spring:message code="NC_VM_VM_NM" text="서버명" />", field : "VM_NM"},
		    {headerName : "<spring:message code="NC_SCHEDULE_CRON" text="CRON" />", field : "CRON"},
			{headerName : "<spring:message code="NC_SCHEDULE_ACTION" text="Action" />", field : "ACTION"},
			{headerName : "<spring:message code="FM_USER_USER_NAME" text="사용자" />", field : "USER_NAME"},
			{headerName : "<spring:message code="NC_SCHEDULE_LAST_RUN_RESULT" text="마지막 실행 결과" />", field : "LAST_RUN_RESULT_NM"}
		]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			cacheBlockSize: 100,
			rowSelection : 'single',
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				req.getInfo('/api/schedule/info?SCHEDULE_ID='+ arr[0].SCHEDULE_ID, function(data){
					fromCron();
					inputvue.$forceUpdate();
				});
			},
		}
		mainTable = newGrid("mainTable", gridOptions);
		search();
	});

	// 조회
	function search() {
		searchvue.form_data.KUBUN = "V";
		post('/api/schedule/list', searchvue.form_data, function(data) {
			mainTable.setData(data);
			 
		});
	}

	function del(){
		if(chkSelection()){
			req.del("/api/schedule/delete", function(){
				search();
				alert(msg_complete);
			})
		}
	}

	function ins(){
		req.setData(initData);
	}

	function save(){
		if(form_data.IDU == "I" || chkSelection()){
			if(form_data.SCHEDULE_SYNC != 'C') {
				makeCron();
			}
			form_data.KUBUN = "V";
			post("/api/schedule/save", form_data, function(){
				alert("<spring:message code="saved" text="저장되었습니다"/>");
				search();
			})
		}
	}
	
	function s_select_vm(vm, callback){
		searchvue.form_data.VM_NM = vm.VM_NM;
		$("#S_VM_NM").val(vm.VM_NM);
		searchvue.form_data.ID = vm.VM_ID;
		if(callback) callback();
	}

	function chkSelection(){
		var arr = mainTable.getSelectedRows();
		if(arr[0]){
			return true;
		}
		alert(msg_select_first);
		return false
	}

	// 엑셀 내보내기
	function exportExcel(){
		mainTable.exportCSV({fileName:'schedule'})
		//exportExcelServer("mainForm", '/api/schedule/list_excel', 'Schedule',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}

</script>