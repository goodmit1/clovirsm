<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div style="display:flex;">
			<div class="col col-sm-2" style="margin-left:15px; width: 128px;">
				<fm-select id="S_TASK_STATUS_CD" name="UNIT" :options="unitOptions"/></fm-select>
			</div> 
			<div class="col col-sm-8" id="DATE_FORM" style = "margin-left:15px;">
					<table>
						<tr>		
							<td>
								<fm-date id="S_INS_TMS_FROM" name="INS_TMS_FROM"></fm-date>
								<fm-date id="S_APPR_TMS_FROM" name="APPR_TMS_FROM"></fm-date>
							</td>
							<td class="tilt">~</td>
							<td>
								<fm-date id="S_INS_TMS_TO" name="INS_TMS_TO" ></fm-date>
								<fm-date id="S_APPR_TMS_TO" name="APPR_TMS_TO" ></fm-date>
							</td>
						</tr>
					</table>
			</div>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=APP_KIND" id="S_APPR_STATUS_CD" emptystr="<spring:message code="label_all" text="" />"
                  name="APPR_STATUS_CD" title="<spring:message code="NC_REQ_APPR_STATUS_CD" text="결재상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_REQ_NM" name="REQ_NM" title="<spring:message code="label_title" text="서비스" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select2 url="/api/etc/team_list" id="S_TEAM_CD" emptystr="<spring:message code="label_all" text="" />" name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_NM" text="부서" />"></fm-select2>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="NC_REQ_INS_ID_NM" text="요청자" />"></fm-input>
		</div>
		<!-- <div class="col col-sm" id="DATE_UPD_FORM">
				<table>
					<tr>
						<td>
							<fm-date id="S_APPR_TMS_FROM" name="APPR_TMS_FROM"></fm-date>
						</td>
						<td class="tilt">~</td>
						<td>
							<fm-date id="S_APPR_TMS_TO" name="APPR_TMS_TO" ></fm-date>
						</td>
					</tr>
				</table>
		</div> -->
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>

			<fm-popup-button style="display: none;" popupid="approval_req_form_popup" popup="../approval/approval_request_popup.jsp?action=view" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
		</div>
	</div>
</div>
<div class="" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<%-- <spring:message code="total_count" text="총 건수"/>: <span id="mainTable_total">0</span>, <spring:message code="label_criterion_this_mon"/>- [<spring:message code="request_count" text="요청 건수"/>: <span id="request_count">0</span>, <spring:message code="approve_count" text="승인 건수"/>: <span id="approve_count">0</span>, <spring:message code="deny_count" text="반려 건수"/>: <span id="deny_count">0</span>]
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 550px;"></div>
	</div>
<%-- 	<jsp:include page="../../popup/vra_detail_form_popup.jsp?action=view"></jsp:include> --%>
</div>
</div>
<script>
	var req = new Req();
	mainTable;
	var approvalReqParam = null;
	
	var unitOptions = {
			INS: "<spring:message code="" text="요청일자" />",
			APPR: "<spring:message code="NC_REQ_APPR_TMS" text="결재일자" />"
		}

	$(function() {
		status_cd();
		var
		columnDefs = [
			{
				field : "REQ_ID",
				hide: true
			},{
				headerName : "<spring:message code="label_title" text="서비스" />",
				maxWidth: 340,
				width: 340,
				field : "REQ_NM"
			},{
				headerName : "<spring:message code="NC_REQ_INS_ID_NM" text="요청자" />",
				maxWidth: 160,
				width: 160,
				field : "USER_NAME"
			},{
				headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="부서명" />",
				maxWidth: 160,
				width: 160,
				field : "TEAM_NM"
			},{
				headerName : "<spring:message code="" text="요청일자"/>",
				maxWidth: 180,
				width: 180,
				field : "INS_TMS",
				format:'datetime'
			}, {
				headerName : "<spring:message code="NC_REQ_APPR_STATUS_CD" text="결재상태" />",
				maxWidth: 120,
				width: 120,
				field : "APPR_STATUS_CD_NM"
			},{
				headerName : "<spring:message code="NC_REQ_APPR_COMMENT" text="결재의견" />",
				maxWidth: 340,
				width: 340,
				field : "APPR_COMMENT",
				cellRenderer:function(params){
					if(params.value) return escapeHtml(params.value);
				}
				
			},{
				headerName : "<spring:message code="NC_REQ_APPR_TMS" text="결재일자"/>",
				maxWidth: 180,
				width: 180,
				format:'datetime',
				field : "APPR_TMS"
			}
		];
		var
		gridOptions = {
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = mainTable.getSelectedRows();
				if(arr.length > 0){
					var nc_req = arr[0];
			    	approvalReqParam = nc_req;
			    	$('#approval_req_form_popup_button').trigger('click');
				}
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
		search();
		
	});
 
	function initFormData(){
		var now = new Date();
		var firstDate, lastDate;
		 
		firstDate = new Date(now.getFullYear(), now.getMonth(), 1);
		lastDate = new Date(now.getFullYear(), now.getMonth()+1, 0);
		search_data.INS_TMS_FROM = formatDate(firstDate,'date');
		search_data.INS_TMS_TO = formatDate(lastDate,'date');
	}
	 
 
	function status_cd(){
		$('#DATE_FORM > table > tbody > tr > td:nth-child(1) > span:nth-child(2) > div').hide();
		$('#DATE_FORM > table > tbody > tr > td:nth-child(3) > span:nth-child(2) > div').hide();
		$('#S_TASK_STATUS_CD').change( function() {
			var type = $('#S_TASK_STATUS_CD').val();
			 
			 
			
			$('#S_INS_TMS_FROM').val('');
			$('#S_INS_TMS_TO').val('');
			$('#S_APPR_TMS_FROM').val('');
			$('#S_APPR_TMS_TO').val('');
			
			if(type === 'INS'){			
				$('#DATE_FORM > table > tbody > tr > td:nth-child(1) > span:nth-child(1) > div').show();
				$('#DATE_FORM > table > tbody > tr > td:nth-child(3) > span:nth-child(1) > div').show();
				$('#DATE_FORM > table > tbody > tr > td:nth-child(1) > span:nth-child(2) > div').hide();
				$('#DATE_FORM > table > tbody > tr > td:nth-child(3) > span:nth-child(2) > div').hide();
			}
			if(type === 'APPR'){
				$('#DATE_FORM > table > tbody > tr > td:nth-child(1) > span:nth-child(1) > div').hide();
				$('#DATE_FORM > table > tbody > tr > td:nth-child(3) > span:nth-child(1) > div').hide();
				$('#DATE_FORM > table > tbody > tr > td:nth-child(1) > span:nth-child(2) > div').show();
				$('#DATE_FORM > table > tbody > tr > td:nth-child(3) > span:nth-child(2) > div').show();
			}
			
		});
	}

	// 조회
	function search() {
		$.post('/api/approval_history/info/list_NC_REQ_MONTHLY_COUNT/', {}, function (data){
			$('#request_count').text(data.REQUEST_COUNT);
			$('#approve_count').text(data.APPROVE_COUNT);
			$('#deny_count').text(data.DENY_COUNT);
		});
		req.searchPaging('/api/approval_history/list', mainTable);
		// console.log(search_data)
		// req.search('/api/approval_history/list' , function(data) {
		// 	mainTable.setData(data);
		// });
	}

	// 엑셀 내보내기
	function exportExcel() {
		exportExcelServer("mainForm", '/api/approval_history/list_excel', 'ApprovalHistory',mainTable.gridOptions.columnDefs, req.getRunSearchData())
		// mainTable.exportCSV({fileName:'ApprovalHistory List'})
	}
</script>