<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<%
	NextApproverService nextApproverService = (NextApproverService)SpringBeanUtil.getBean("nextApproverService");
	List<String> stepNames = nextApproverService.getAllStepNames();
	request.setAttribute("STEP_NAMES", stepNames);
%>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=APP_KIND" id="S_APPR_STATUS_CD" emptystr=" "
                  name="APPR_STATUS_CD" title="<spring:message code="NC_REQ_APPR_STATUS_CD" text="결재상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_REQ_NM" name="REQ_NM" title="<spring:message code="NC_REQ_REQ_NM" text="제목"/>"></fm-input>
		</div>
		<div class="col">
			<table>
				<tr>
					<td>
						<fm-date id="S_INS_TMS_FROM" name="INS_TMS_FROM" title="<spring:message code="NC_REQ_INS_TMS" text="요청일시"/>"></fm-date>
					</td>
					<td class="tilt">~</td>
					<td>
						<fm-date id="S_INS_TMS_TO" name="INS_TMS_TO" ></fm-date>
					</td>
				</tr>
			</table>
		</div>
		<div class="col btn_group">
			<fm-popup-button popupid="approval_req_form_popup" style="display:none" popup="../approval/mobis/approval_request_popup.jsp?action=cancel" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="appProgressBar">
<%-- 	<c:out value="${fn:length(STEP_NAMES)}"></c:out> --%>

	<div class="bar_progress">
		<div  class="progress-coming-back-i zindex" onClick="approvalWidth(this,'0',0,${fn:length(STEP_NAMES)})">
			<div class="progress-coming-i-in">1</div>
		</div>
		<div  class="progress-done-i zindex" onClick="approvalWidth(this,'100',100,${fn:length(STEP_NAMES)})" >
			<div class="progress-coming-i-normal">${fn:length(STEP_NAMES)+2}</div>
		</div>
		
		<span>
			<c:if test="${fn:length(STEP_NAMES) >0}">
			<c:set var="num" value="${fn:length(STEP_NAMES)+1}" />
				<c:forEach items="${STEP_NAMES}" var="name" varStatus="index">
					<div  class="progress-coming-back-i zindex" onClick="approvalWidth(this,'${100/num * index.count}',${index.index},${fn:length(STEP_NAMES)});" style="left:${((90/num) * index.count)+1.2}%;">
						<div class="progress-coming-i-normal" >${index.count+1}</div>
					</div>
					<div class="approvalName-detail-i" style="left:calc(${90/num * index.count}% + -1px);">
						${name}
					</div>
				</c:forEach>
			</c:if>
			</span>
		<div class="Rounded-Rectangle-Full" style="width:100%;top: 95px;"></div>
		<div class="approvalName-detail-i last"> <spring:message code="finish" text="완료"/></div>
		<div class="approvalName-detail-i in first "> <spring:message code="NC_LICENSE_LICENSE_TOTAL" text="전체"/></div>
		
	</div>
</div>
<div style="margin-left: 12px; margin-top: 10px;  font-size: 12px;">※&nbsp<spring:message code="appr_info" text="결재단계 클릭시 해당 결재단계 조회가능합니다."/></div>
<div class="fullGrid" id="input_area">
	<div class="table_title">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 450px; border-top: 1px solid #eeee;" ></div>

</div>
<script>
	var req = new Req();
	mainTable;
	var approvalReqParam = null;

	$(function() {
		var
		columnDefs = [
			/* {
				headerName : "<spring:message code="NC_REQ_REQ_ID" text="No" />",
				maxWidth: 160,
				width: 160,
				cellStyle:{'text-align':'right'},
				field : "REQ_ID"
			}, */{
				headerName : "<spring:message code="NC_REQ_REQ_NM" text="제목"/>",
				//maxWidth: 480,
				width: 440,
				field : "REQ_NM"
			},{
				headerName : "<spring:message code="NC_REQ_APPR_STEP" text="단계" />",
				//maxWidth: 120,
				width: 120,
				field : "STEP_NM",
				tooltipField : "APPR_USER_NAME",
			},{
				headerName : "<spring:message code="NC_REQ_APPR_STATUS_CD" text="결재상태" />",
				//maxWidth: 120,
				width: 120,
				field : "APPR_STATUS_CD_NM"
			},{
				headerName : "<spring:message code="NC_REQ_APPR_COMMENT" text="결재의견" />",
				//maxWidth:700,
				width: 500,
				field : "APPR_COMMENT"
			},/* {
				headerName : "<spring:message code="NC_REQ_APPR_NM" text="결재자" />",
				//maxWidth: 180,
				width: 180,
				field : "APPR_USER_NAME"
			}, */{
				headerName : "<spring:message code="NC_REQ_INS_TMS" text="요청일시"/>",
				//maxWidth: 200,
				width: 200,
				field : "INS_TMS",
				valueGetter: function(params) {
					if(params.data && params.data.INS_TMS){
						return formatDate(params.data.INS_TMS,'datetime')
	          		  }
          		  	return "";
				}
			},{
				headerName : "<spring:message code="NC_REQ_APPR_TMS" text="결재일자"/>",
				//maxWidth: 200,
				width: 200,
				field : "APPR_TMS",
				valueGetter: function(params) {
					if(params.data && params.data.APPR_TMS){
				    	return formatDate(params.data.APPR_TMS,'datetime')
					}
					return "";
				}
			},{
				field : 'USER_NAME',
				hide : true
			},{
				field : 'STEP',
				hide : true
			},{
				field : 'APPR_STATUS_CD_T',
				hide : true
			}
		];
		var
		gridOptions = {
			hasNo : true,	
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			sizeColumnsToFit: true,
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: true,
			onRowClicked: function(){
				var arr = mainTable.getSelectedRows();
				if(arr.length > 0){
					var nc_req = arr[0];
			    	approvalReqParam = nc_req;
			    	$('#approval_req_form_popup_button').trigger('click');
				}
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
// 		var today = formatDate(new Date(), 'date');
// 		req.setSearchData({INS_TMS_TO: today, INS_TMS_FROM: today});
		search();
	});

	function approvalWidth(that,wid,num,total){
		var width = null;
		$(".progress-coming-i-in").attr("class","progress-coming-i-pre");
		$(".progress-coming-i-normal").attr("class","progress-coming-i-pre");
		$(".approvalName-detail-i").removeClass("in");
		req.setSearchData("APPR_STATUS_CD",null);
		if(wid == 100){
			width = parseFloat(wid);
			$(".progress-done-i > div").attr("class","progress-coming-i-in");
			$(".approvalName-detail-i.last").addClass("in");
			req.setSearchData("STEP",total);
			req.setSearchData("APPR_STATUS_CD","A");
			search();
			return;
		} else if (wid == 0){
			$(".progress-coming-i-pre").attr("class","progress-coming-i-normal");
			that.childNodes[1].setAttribute("class","progress-coming-i-in");
			$(".approvalName-detail-i.first").addClass("in");
			req.setSearchData("STEP",null);
			width = 0;
			search();
			return;
		} else{
			$(".bar_progress > span > div ").eq(num).prevAll("div").children().attr("class","progress-coming-i-pre");
			$(".bar_progress > span > div ").eq(num).nextAll("div").children().attr("class","progress-coming-i-normal");
			$(".bar_progress > span > div > div ").eq(num).attr("class","progress-coming-i-in");
			$(".bar_progress > span > .approvalName-detail-i ").eq(num).addClass("in");
			$(".progress-done div").attr("class","progress-coming-i-normal");
			$(".progress-done-i > div").attr("class","progress-coming-i-normal");
			req.setSearchData("STEP",num+1);
			search();
		}
	}

	function initFormData()
	{
	 
	}
	// 조회
	function search() {
		/* req.search('/api/request_history/list' , function(data) {
			mainTable.setData(data);
		}); */
		req.searchPaging('/api/request_history/list' ,mainTable, function(data) {
    	  	mainTable.gridOptions.api.sizeColumnsToFit();
			//mainTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel() {
		exportExcelServer("mainForm", '/api/request_history/list_excel', 'RequestHistory',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}
</script>