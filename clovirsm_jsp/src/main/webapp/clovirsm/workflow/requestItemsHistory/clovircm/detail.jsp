<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
 
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>

<style>
	th, td, table {    
	border-color: #dddddd;
    font-size: 13px;
    }
</style>
 
<%@include file="/login/mail/reqApprove.jsp" %>
