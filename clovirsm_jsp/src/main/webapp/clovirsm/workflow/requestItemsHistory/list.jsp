<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		
		<div class="col col-sm">
			<fm-input id="S_SVC_NM" name="SVC_NM" title="<spring:message code="REQ_TITLE" text="요청명" />" input_style="width: 175px;"></fm-input>
		</div>
		<c:if test="${ADMIN_YN == 'Y'}">
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="NC_VM_REQ_USER" text="요청자" />"></fm-input>
		</div>
		</c:if>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=SVCCUD" id="S_SVCCUD" emptystr=" "
                  name="SVCCUD" title="<spring:message code="application_classification" text="신청구분"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=APP_KIND" id="S_APPR_STATUS_CD" emptystr=" "
                  name="APPR_STATUS_CD" title="<spring:message code="STATE" text="상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=TASK_STATUS_CD" id="S_TASK_STATUS_CD" emptystr=" "
                  name="TASK_STATUS_CD" title="<spring:message code="deploy_status" text="배포 상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<table>
				<tr>
					<td>
						<fm-date id="S_INS_TMS_FROM" name="INS_TMS_FROM" title="<spring:message code="request_date" text="요청일자"/>"></fm-date>
					</td>
					<td class="tilt">~</td>
					<td>
						<fm-date id="S_INS_TMS_TO" name="INS_TMS_TO" ></fm-date>
					</td>
				</tr>
			</table>
		</div>
		<div class="col btn_group nomargin">
			<fm-popup-button popupid="CC_popup" style="display:none" popup="detail_popup.jsp" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
			<fm-popup-button popupid="ZC_popup" style="display:none" popup="/clovirsm/workflow/templateReq/detail_popup.jsp" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
	<%-- 	<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
		
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 550px;" ></div>
	</div>
	</div>
<%-- 	<jsp:include page="../../popup/vra_detail_form_popup.jsp?action=view"></jsp:include> --%>
</div>
<script>
	var req = new Req();
	mainTable;
	var approvalReqParam = null;

	$(function() {
		var
		columnDefs = [
			{
				headerName : "<spring:message code="application_classification" text="신청 구분" />",
				width: 150,
				field : "SVCCUD_NM" 
			},{
				hide: true,
				width: 150,
				field : "SVC_ID" 
			},{
				headerName : "<spring:message code="REQ_TITLE" text="요청명" />",
				width: 500,
				field : "SVC_NM"
			},{
				headerName : "<spring:message code="NC_VM_REQ_USER" text="요청자" />",
				width: 100,
				field : "USER_NAME"
			},{
				headerName : "<spring:message code="request_date" text="요청일자"/>",
				width: 180,
				field : "INS_TMS",
				format:'datetime'
			},{
				headerName : "<spring:message code="STATE" text="상태" />",
				width: 100,
				field : "APPR_STATUS_CD_NM",
				tooltipField:"APPR_COMMENT"
			},
			{
				headerName : "<spring:message code="deploy_status" text="배포 상태" />",
				width: 100,
				field : "TASK_STATUS_CD_NM"
			}
		];
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = mainTable.getSelectedRows();
				if(arr.length > 0){
				 
			    	approvalReqParam = arr[0];
			    	if(approvalReqParam.SVC_CD=='Z'){
			    		approvalReqParam.TEMPLATE_ID=approvalReqParam.SVC_ID;
			    		$('#ZC_popup_button').trigger('click');
			    	}
			    	else{
			    		$('#CC_popup_button').trigger('click');
			    	}	
			    
				}
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
// 		var today = formatDate(new Date(), 'date');
// 		req.setSearchData({INS_TMS_TO: today, INS_TMS_FROM: today});
		search();
	});

	// 조회
	function search() {
		req.searchPaging('/api/resource/req_history/list', mainTable, function(data) {
			removeOptionByVal("S_APPR_STATUS_CD","C")
			});
	}
	function initFormData()
	{
	 
	}
	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'requestItemHistory'});
	}
</script>