<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px); }
	#INS_TMS_FROM{
		width:100px;
	}
	#INS_TMS_TO{
		width:100px;
	}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-12" style="margin-left: 15px;">
			<fm-date-from-to id="INS_TMS" name="INS_TMS" title="<spring:message code="request_date" text="요청일자"/>"></fm-date-from-to>
		</div>
		<div class="col col-sm">
			<fm-select2 url="/api/etc/team_list" id="S_TEAM_CD" emptystr="<spring:message code="label_all" text="" />" name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_NM" text="부서" />"></fm-select2>
		</div>
		<div class="col col-sm">
			<fm-input id="S_REQ_NM" name="REQ_NM" title="<spring:message code="label_title" text="결재정보" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="요청자" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>

			<fm-popup-button style="display: none;" popupid="approval_req_form_popup" popup="approval_request_popup.jsp" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		<fm-sbutton cmd="update" class="btn contentsBtn tabBtnImg del" onclick="openReasonPopup('deny')"><spring:message code="btn_approval_deny" text=""/></fm-sbutton>
		<fm-sbutton cmd="update" class="btn contentsBtn tabBtnImg save" onclick="openReasonPopup('approval')"><spring:message code="btn_approval_accept" text=""/></fm-sbutton>
	
			<fm-popup-button style="display: none;" popupid="deny_popup" popup="/clovirsm/popup/reason_popup.jsp?title=btn_approval_deny" param="approvalReqParam" callback="denyMulti"></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="approval_popup" popup="/clovirsm/popup/reason_popup.jsp?title=btn_approval_accept" param="approvalReqParam" callback="approveMulti"></fm-popup-button>
	
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 550px;" ></div>
	</div>
	</div>
</div>
<script>
	var req = new Req();
	mainTable;
	var approvalReqParam = null;

	function openReasonPopup(kubun){
		approvalReqParam = mainTable.getSelectedRows();
		if(approvalReqParam.length==0){
			alert(msg_select_first);
			return;
		}
		$("#" + kubun + "_popup_button").click();
	}
	$(function() {
		var
		columnDefs = [{width: 52, minWidth: 52,maxWidth: 52,   checkboxSelection: true, 
		    headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly:true },
			{
				headerName : "<spring:message code="NC_REQ_REQ_ID" text="No" />",
				maxWidth: 170,
				width: 170,
				field : "REQ_ID"
			},{
				headerName : "<spring:message code="label_title" text="결재정보" />",
				field : "REQ_NM",
				width: 980,
				maxWidth: 980,
				cellRenderer:function(params){
					 
					return '<a href="#" onclick="openDetail(' + params.rowIndex + ');return false">' + params.value + '</a>';
				}
			},{
				headerName : "<spring:message code="NC_REQ_CMT" text="설명" />",
				field : "CMT",
				width: 0,
				hide : true
			},{
				headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="부서명" />",
				maxWidth: 160,
				width: 160,
				field : "TEAM_NM"
			},{
				headerName : "<spring:message code="FM_USER_USER_NAME" text="요청자" />",
				maxWidth: 160,
				width: 160,
				field : "USER_NAME"
			},{
				headerName : "<spring:message code="request_date" text="요청일자"/>",
				maxWidth: 180,
				width: 180,
				field : "INS_TMS",
				valueGetter: function(params) {
			    	return formatDate(params.data.INS_TMS,'datetime')
				}
			},{
				headerName : "<spring:message code="NC_REQ_APPR_STEP" text="단계" />",
				//maxWidth: 120,
				hide: true,
				width: 120,
				field : "STEP_NM"
			}
		];
		var
		gridOptions = {
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			rowSelection : 'multiple',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			 
		}
		mainTable = newGrid("mainTable", gridOptions);
// 		var today = formatDate(new Date(), 'date');
// 		req.setSearchData({INS_TMS_TO: today, INS_TMS_FROM: today});
		search();
	});
	function getApprErrMsg(err_list){
		var msg= '';
		for(var i=0; i<err_list.length; i++){
			msg += err_list[i].REQ_ID + "==>" + err_list[i].ERR_MSG + "\n"
		}
		return msg;
	}
	function approveMulti(cd, cmt){
		var param = {};
		param.APPR_COMMENT = cmt;
		param.APPR_CMT_CD = cd;
		param.selected_json= JSON.stringify(mainTable.getSelectedRows());
		post('/api/approval/approve_multi', param, function(data){
			if(data.err_list && data.err_list.length>0){
				alert("에러가 발생하였습니다.\n" + getApprErrMsg(data.err_list));
			}
			search();
		})
	}
	function denyMulti(cd, cmt){
		var param = {};
		param.APPR_COMMENT = cmt;
		param.APPR_CMT_CD = cd;
		param.selected_json= JSON.stringify(mainTable.getSelectedRows());
		post('/api/approval/deny_multi', param, function(data){
			if(data.err_list && data.err_list.length>0){
				alert("에러가 발생하였습니다.\n" + getApprErrMsg(data.err_list));
			}
			search();
		})
	}
	function openDetail(idx){
		approvalReqParam = mainTable.getData()[idx];
		$('#approval_req_form_popup_button').trigger('click');
	}
	// 조회
	function search() {
		req.searchPaging('/api/approval/list', mainTable);
		// req.search('/api/approval/list' , function(data) {
		// 	mainTable.setData(data);
		// });
	}
	/*function initFormData(){
	      var now = new Date();
	      var firstDate, lastDate;
	       
	      firstDate = new Date(now.getFullYear(), now.getMonth(), 1);
	      lastDate = new Date(now.getFullYear(), now.getMonth()+1, 0);
	      search_data.INS_TMS_FROM = formatDate(firstDate,'date');
	      search_data.INS_TMS_TO = formatDate(lastDate,'date');
	   }*/
	// 엑셀 내보내기
	function exportExcel() {
		// mainTable.exportCSV({fileName:'Approval List'})
		exportExcelServer("mainForm", '/api/approval/list_excel', 'User Approval Status List',mainTable.gridOptions.columnDefs, req.getRunSearchData());
	}
</script>