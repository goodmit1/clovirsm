<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px; margin-left: 0;">
	<div id="mappingTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var mappingReq = new Req();
	mappingTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_DC_DC_NM" text="" />", field : "DC_NM", maxWidth:500,width:500},
		                  {headerName : "<spring:message code="" text="데이터 센터 템플릿" />", field : "TMPL_PATH_NAME", maxWidth:600,
							  cellRenderer : CustomCombobox
						  },
		                  ]
		var
		mappingTableGridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			//rowSelection : 'single',
			cacheBlockSize: 100,
			// editable:true,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onCellEditingStarted: function(event) {
				if(event.column.colId=='TMPL_PATH_NAME')
				{
					autocomplete('#mappingTable div.ag-cell-value[col-id="TMPL_PATH_NAME"] input',function(){
						return '/api/code_list?grp=dblist.com.clovirsm.common.Component.selectTemplate&DC_ID=' + event.data.DC_ID
					},'OBJ_NM','OBJ_NM', 1, function (data) {
						event.data.TMPL_PATH = data.OBJ_ID;
						console.log(data);
					});
					 
				}
		    },
		}
		mappingTable = newGrid("mappingTable", mappingTableGridOptions);

		$(document).on('change', '.template_selectBox', function() {
			mappingTable.getData()[this.title].TMPL_PATH = this.value;
			mappingTable.getData()[this.title].IU = 'U';
		})
	});

	function mappingSearch(OS_ID){
		mappingReq.searchSub('/api/ostype/list/list_NC_DC_OS_TYPE/?OS_ID=' + OS_ID, {}, function(data) {
			 
			mappingTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel(){
		mappingListTable.exportCSV({fileName:'mappingList'})
		 
	}

	// combo box 생성 함수
	function CustomCombobox(param) {
		var agSelect = document.createElement("select");

		post('/api/ostype/list/selectByTempate_NC_HV_OBJ/',{DC_ID:param.data.DC_ID}, function (data) {
			agSelect.setAttribute("name", 'template_ID');
			agSelect.setAttribute("class", 'template_selectBox');
			agSelect.setAttribute("title", param.rowIndex);

			var option = document.createElement("option");
			option.text = '';
			agSelect.appendChild(option);

			for(var i = 0; i < data.length; i++) {
				var option = document.createElement("option");
				option.text = data[i].OBJ_NM;
				option.value = data[i].OBJ_ID;
				agSelect.appendChild(option);

				if(data[i].OBJ_ID == data[i].TMPL_PATH) {
					option.selected = true;
				}
			}
		});

		return agSelect;
	}

</script>


