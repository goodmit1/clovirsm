<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %> 

<!--  서버 목록 조회 -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-input id="S_DISK_TYPE_NM" name="DISK_TYPE_NM" title="<spring:message code="label_disk_kubun" text="디스크" />"></fm-input>
		</div>
		
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="diskTypeMngSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col btn_group rt">
			<fm-sbutton id="insert_diskTypeMng" cmd="update" class="newBtn contentsBtn tabBtnImg collection top5"  ><spring:message code="btn_COLLECT"  text="수집"/></fm-sbutton>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:1080px; padding: 10px;">

	<div id="diskItem"></div>
	
</div>
<script>
	var diskTypeMngReq = new Req();
	
	$(document).ready(function(){
		diskTypeMngSearch();
	});
	
	// 조회
	function diskTypeMngSearch() {
		diskTypeMngReq.search('/api/disk_type_mng/list' , function(data) {
			drawDisk(data);
		});
	}
	function drawDisk(data){
		 
		$('#diskItem').empty();
		for(var i = 0 ; i < data.list.length ; i++){
			var html = '<div class="contentsItem">';
			html += '<div class="contentsItemTop">';
			html += '<div class="itemIcon" style=""></div>';
			html += '<div class="itemName">'+ data.list[i].DISK_TYPE_NM +'</div>';
			html += '</div>';
			html += '<div class="contentsItemMid">';
			html += '<div>';
			html += '<div class="contentsItemPay"><span>\</span> ' + formatNumber(data.list[i].HH_FEE) + ' <span style="font-size: 10px;">/ <spring:message code="label_date" text="일"/></span></div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			$('#diskItem').append(html);
		}
	}


</script>


