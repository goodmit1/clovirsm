<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-5">
			<fm-select id="S_TAB2_DC" name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>" emptystr=" " keyfield="DC_ID" titlefield="DC_NM" required="true">
		</div>
		<div class="col col-sm-5">
			<fm-ibutton id="USER_NAME" name="ID"  required="true" title="<spring:message code="admin" text="관리자" />">
				<fm-popup-button v-show="false" popupid="${popupid}_user_search_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#USER_NAME').val()" callback="${popupid}_select_user"  ><spring:message code="admin" text="관리자"/></fm-popup-button>
			</fm-ibutton>
			<input id="USER_ID" name="ID" style="display:none">
		</div>
		<div class=" col col-sm-2">
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save top5" onclick="adminSave('DC_ADMIN');"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	
	 
</div>
