<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=SEPECSETTING" id="S_KUBUN" 
                  name="KUBUN" keyfield="KUBUN" titlefield="KUBUN" title="<spring:message code="application_classification" text="신청구분"/>">
			</fm-select>
		</div>
		<div class="col btn_group rt nomargin">
			<fm-sbutton cmd="search" class="searchBtn"  onclick="searchspecSetting()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
	<%-- 	<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportspecSetting()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportspecSetting()" class="layout excelBtn"></button>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save" class="btn btn-primary" onclick="savespecSetting()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height:450px;"></div>
	</div>
</div>
<script>

var specSetting = new Req();
function exportspecSetting(){
	mainTable.exportCSV({fileName:'specSetting.csv'})
}
function savespecSetting(){
	mainTable.gridOptions.api.stopEditing();
	var arr = mainTable.getData();
	arr.forEach(function(value,i){
		arr[i].IU = "U";
	})
//	for(var i in arr){
//	}
	var selected_json = JSON.stringify(arr);
	post("/api/spec_setting/save_multi", {"selected_json" : selected_json}, function(){
		searchspecSetting();
	})
}
function searchspecSetting(){
	 
	searchvue.form_data.DD_ID = "SPEC_SETTING";
	searchvue.form_data.KUBUN =  $("#S_KUBUN").val();
	specSetting.search('/api/spec_setting/list', function(data){
		mainTable.setData(data);
	});
}

var mainTable;

$(document).ready(function(){
	var columnDefs =[{ headerName: "", field: "ID", hide: true },
	    { headerName: "<spring:message code="NC_SPEC_SETTING_NM" text="구분" />", field: "NM", maxWidth: 200}
		,{
			headerName: "<spring:message code="NC_SPEC_VAL1" text="사용자 선택 값" />",
			headerTooltip:"<spring:message code="tooltip_cpu_mem" text="서버 생성 시 CPU, 메모리의 사용자 선택을 제한하는 값으로 구분해서 여러 개를 입력한다." />",
			field: "VAL1",
			editable: true
		}
	     ];
	var gridOptions = {
		    columnDefs: columnDefs,
		    rowData: [],
			rowSelection : 'single',
		    enableSorting: true,
		    enableColResize: true
		};
	mainTable = newGrid("mainTable", gridOptions)
	setTimeout(function() {
		searchspecSetting();}
	,100);
});
</script>