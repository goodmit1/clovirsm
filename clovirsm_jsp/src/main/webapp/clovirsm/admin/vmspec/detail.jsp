<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 입력 Form -->

<div class="form-panel detail-panel  panel panel-default"  style="margin:0;">

	<div class="panel-body" id="inputForm"  >

		<div class="col col-sm-3">
			<fm-input id="SPEC_NM" name="SPEC_NM" required="true"  title="<spring:message code="NC_VM_SPEC_SPEC_NM" text="사양" />"></fm-input>
		</div>
		<div class="col col-sm-3 btnw13">
			<fm-spin id="CPU_CNT" name="CPU_CNT" required="true"  title="<spring:message code="NC_VM_SPEC_CPU_CNT" text="" />"></fm-spin>
		</div>
		<div class="col col-sm-3 btnw13">
			<fm-spin id="RAM_SIZE" name="RAM_SIZE" required="true"  title="<spring:message code="NC_VM_SPEC_RAM_SIZE" text="" />"></fm-spin>
		</div>
		<div class="col col-sm-3 btnw13">
			<fm-spin id="DISK_SIZE" name="DISK_SIZE" required="true"   title="<spring:message code="NC_VM_SPEC_DISK_SIZE" text="" />"></fm-spin>
		</div>
		<div class="col col-sm-3 btnw13">
			<fm-spin id="DD_FEE" name="DD_FEE"   title="<spring:message code="NC_VM_SPEC_DD_FEE" text="명칭" />"></fm-spin>
		</div>
		<div class="col col-sm-3 btnw13">
			<fm-select id="DISK_TYPE_ID" name="DISK_TYPE_ID" required="true"  url="/api/code_list?grp=db.com.clovirsm.common.Component.selectDiskType"  title="<spring:message code="NC_VM_SPEC_DISK_TYPE_NM" text="명칭" />"></fm-select>
		</div>

		<div class="col col-sm-3">
			<fm-select id="DEL_YN" name="DEL_YN"  url="/api/code_list?grp=sys.yn"  title="<spring:message code="NC_VM_SPEC_DEL_YN" text="DEL_YN" />"></fm-select>
		</div>

	</div>

</div>
