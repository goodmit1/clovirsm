<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px; margin-left: 0;">
	<div id="mappingTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var mappingReq = new Req();
	mappingTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_DC_DC_NM" text="" />", field : "DC_NM", maxWidth:300,width:300},
		                  {headerName : "<spring:message code="" text="매핑 타입" />", field : "DC_SPEC_CD", editable:true},
		                  ]
		var
		mappingTableGridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			//rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onCellEditingStarted: function(event) {
				if(event.column.colId=='DC_SPEC_CD')
				{
					 
					autocomplete('#mappingTable div.ag-cell-value[col-id="DC_SPEC_CD"] input',function(){
						return '/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_HV_OBJ&OBJ_TYPE_NM=instanceType&DC_ID=' + event.data.DC_ID
							
					},'OBJ_NM','OBJ_NM', 1);
					 
				}
		    },
		}
		mappingTable = newGrid("mappingTable", mappingTableGridOptions);
	});

	function mappingSearch(specId){
		mappingReq.searchSub('/api/vm_spec_mng/list/list_NC_DC_SPEC/?SPEC_ID=' + specId, {}, function(data) {
			 
			mappingTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel(){
		mappingListTable.exportCSV({fileName:'mappingList'})
		 
	}

</script>


