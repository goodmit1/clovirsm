<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->
<div id="inputTable" class="ag-theme-fresh" style="height: 400px; width: 100%;" ></div>
<script>
var inputTable;
var license = new Array();
$(function() {
	var columnDefs = [
		{headerName : "<spring:message code="" text="제품명" />",field : "LICENSE_NAME", editable:function(params){
			if(params.data.IU=='I')
				return true;
			else
				return false;
			} ,
		cellEditor: "select",
        cellEditorParams: {
            values: []
        	}
		},
		{headerName : "<spring:message code="" text="구입일" />",field : "LICENSE_DATE", editable:function(params){
			if(params.data.IU=='I')
				return true;
			else
				return false;
			}, cellEditor: 'datePicker'},
		{headerName : "<spring:message code="" text="갯수" />",field : "LICENSE_CNT", editable : true, width: 50,cellEditor: 'numericCellEditor'},
		{headerName : "<spring:message code="" text="PO 번호" />",field : "PO_NUMBER", editable : true},
		{headerName : "<spring:message code="" text="비고" />", field : "REMARK", editable : true},
		
		]
	var
	gridOptions = {
		hasNo : true,
		editable:true,
		columnDefs : columnDefs,
		//rowModelType: 'infinite',
		rowSelection : 'single',
		sizeColumnsToFit: true,
		cacheBlockSize: 100,
		rowData : [],
		enableSorting : true,
		enableColResize : true,
		enableServerSideSorting: false,
		onCellEditingStarted: function(event) {
			if(event.column.colId=='LICENSE_NAME'){
				$("#inputTable div.ag-cell-value[col-id='LICENSE_NAME'] select").select2({

				  data : license,
				  tags: true,

				  createTag: function (params) {
					    return {
					      id: params.term,
					      text: params.term,
					      newOption: true
					    }
					  }})
			}
	    }
	}
	
	gridOptions.components = {
            datePicker: getDatePicker(), // util에서 확인
            numericCellEditor: getNumericCellEditor()
            
        };
	inputTable = newGrid("inputTable", gridOptions);
	
	initlicense();
	inputSearch();
});
function inputSearch() {
	post('/api/license_input/list', {}, function(data) {
		inputTable.setData(data);
	});
}
function initlicense() {
	post('/api/license/list', {}, function(data) {
		 
		for(var i = 0 ; i < data.list.length; i++){
			if(license.indexOf(data.list[i].LICENSE_NAME) == -1)
				license.push(data.list[i].LICENSE_NAME)
		}
	});
	
}
function inputTableAdd(){
	inputTable.insertRow({LICENSE_NAME:'',LICENSE_DATE:'',LICENSE_CNT:'',REMARK:'', IU :'I'});
}
function inputTableSave(){
	inputTable.stopEditing();
	post('/api/license_input/save_multi', {selected_json:JSON.stringify(inputTable.getData())} , function(data) {
		summarySearch();
	});
}
function inputTableDel(){
	var arr = inputTable.getSelectedRows();
	if(arr.length > 0 ){
		post('/api/license_input/delete',{LICENSE_NAME : arr[0].LICENSE_NAME , LICENSE_DATE : arr[0].LICENSE_DATE }, function(data){
			inputSearch();
			summarySearch();
		});
	} else{
		alert("<spring:message code="select_first" text="" />")
	}
}

</script>