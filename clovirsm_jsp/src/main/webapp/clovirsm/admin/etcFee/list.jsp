<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top save" onclick="save()"><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px" ></div>
	</div>
	<br />
</div>
<script>
	var etcFeeReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_ETC_FEE_SVC_NM" text="서비스명" />", field : "SVC_NM"},
		                  {headerName : "<spring:message code="NC_ETC_FEE_DD_FEE" text="일간요금" />", format: "number" , field : "DD_FEE", editable: true},
		                  {headerName : "<spring:message code="NC_ETC_FEE_UPD_TMS" text="수정일시" />", field : "UPD_TMS", format: "datetime"}
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		mainTable = newGrid("mainTable", gridOptions);
		search();
	});

	// 조회
	function search() {
		etcFeeReq.search('/api/etc_fee_mng/list', function(data){mainTable.setData(data)});
	}

	function save() {
		mainTable.gridOptions.api.stopEditing();
		var arr = mainTable.getData();
		for(var i=0; i<arr.length; i++){
			arr[i].IU = "U";
		}
//		for(var i in arr){
//		}
		var selected_json = JSON.stringify(arr);
		post("/api/etc_fee_mng/save_multi", {"selected_json" : selected_json}, function(){
			alert("<spring:message code="saved" text="저장되었습니다"/>");
			search();
		})
		
		 
	}

	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'fee'})
	 
	}

</script>