<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  이용 현황 조회 -->
<style>
	.spinner
	{
		width:98% !important
	}
	.yyyy
	{
		width:120px;
	}
	.mm
	{
		width:120px;
	}
	.search-panel .grid-title{
		min-width:0px;
	}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm btnw13">
			<table><tr>
				<td >
					<fm-spin id="S_YYYY_FROM" class="yyyy"
							 name="S_YYYY_FROM" title=""   ></fm-spin>
				</td>
				<td>
					<label for="S_YYYY_FROM" class="control-label grid-title"><spring:message code="year" text="년"/></label>
				</td>
				<td>
					<fm-spin id="S_MM_FROM" class="mm"
							 name="S_MM_FROM" title="" min="1" max="12"  ></fm-spin>
				</td>
				<td>
					<label for="S_MM_FROM" class="control-label grid-title"><spring:message code="month" text="월"/></label>
				</td>
				<td class="tilt">
					~
				</td>
				<td>
					<fm-spin id="S_YYYY_TO" class="yyyy" name="S_YYYY_TO" title=""></fm-spin>
				</td>
				<td>
					<label for="S_YYYY_TO" class="control-label grid-title"><spring:message code="year" text="년"/></label>
				</td>
				<td>
					<fm-spin id="S_MM_TO" class="mm"
							 name="S_MM_TO" title="" min="1" max="12"  ></fm-spin>
				</td>
				<td>
					<label for="S_MM_TO" class="control-label grid-title"><spring:message code="month" text="월"/></label>
				</td>
			</tr>
			</table>

		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:540px">
	<div class="box_s">
		<div class="table_title layout name">
			<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mmGrid_total" style="color: royalblue;">0</span>&nbsp)
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="excelBtn layout " id="excelBtn"></button>
			</div>
		</div>
		<div id="mmGrid" class="ag-theme-fresh " style="height: 350px" ></div>

		<div class="form-panel panel panel-default">
			<div class="panel-body">
				<jsp:include page="history_bar.jsp"></jsp:include>
				<jsp:include page="fee_chart.jsp"></jsp:include>
			</div>
		</div>
	</div>
</div>
<script>
	var mmReq = new Req();
	mmGrid = null;

	$(function() {
		drawMonitorChart();

		var columnDefs = [{headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="" />", field : "TEAM_NM",cellStyle:{'text-align':'center'}},
			<%--{headerName : "<spring:message code="label_use_fee" text="" />", field : "FEE", cellStyle:{'text-align':'right'},valueFormatter:function(params){--%>
			<%--	return formatNumber(params.value);--%>
			<%--}},--%>
			{headerName : "VM(<spring:message code="cnt" />)",field : "VM_CNT", width:120, cellStyle:{'text-align':'right'},valueFormatter:function(params){
					return formatNumber(params.value);
				}},
			{headerName : "<spring:message code="NC_VM_OS"/>(<spring:message code="cnt" />)",field : "IMG_CNT", width:140, cellStyle:{'text-align':'right'},valueFormatter:function(params){
					return formatNumber(params.value);
				}},
			{headerName : "CPU(<spring:message code="cnt" />)",field : "CPU_CNT", width:120, cellStyle:{'text-align':'right'},valueFormatter:function(params){
					return formatNumber(params.value);
				}},
			{headerName : "Memory(GB)",field : "RAM_SIZE", cellStyle:{'text-align':'right'}, width:140, valueFormatter:function(params){
					return formatNumber(params.value);
				}},
			{headerName : "Disk(GB)",field : "DISK_SIZE", cellStyle:{'text-align':'right'}, width:150,valueFormatter:function(params){
					return formatNumber(params.value);
				}},
			{headerName : "Used Disk(GB)",field : "DISK_USED_SIZE", cellStyle:{'text-align':'right'},width:150,valueFormatter:function(params){
					return formatNumber(params.value);
				}},
			{headerName : "Disk Usage(%)",field : "DISK_USAGE", cellStyle:{'text-align':'right'},width:150,valueFormatter:function(params){
					return formatNumber(params.value);
				}},

		]
		var
				gridOptions = {
					hasNo : true,
					columnDefs : columnDefs,
					//rowModelType: 'infinite',
					rowSelection : 'multiple',
					sizeColumnsToFit: false,
					editable:true,
					cacheBlockSize: 100,
					rowData : [],
					enableSorting : true,
					enableColResize : true,
					enableServerSideSorting: false,
					onRowSelected: function(event){

						toggleHistory(event.data.TEAM_CD,event.data.TEAM_NM,event.rowIndex, event.node.selected);
						toggleFeeHistory(event.data.TEAM_CD,event.data.TEAM_NM,event.rowIndex, event.node.selected);

					}
				}
		mmGrid = newGrid("mmGrid", gridOptions);
		setTimeout(function(){
			search()
		}, 100)
	});
	function initFormData()
	{


		var date = new Date();
		search_data.S_YYYY_FROM = date.getFullYear();
		search_data.S_YYYY_TO = date.getFullYear();
		search_data.S_MM_FROM = date.getMonth()+1;
		search_data.S_MM_TO = date.getMonth()+1;
	}
	// 조회
	function search() {
		setDate();
		mmReq.search('/api/mmpay/list' , function(data) {
			mmGrid.setData(data);
			loadFeeChart();


		});
	}

	function setDate(){
		searchvue.form_data.YYYYMM_FROM = searchvue.form_data.S_YYYY_FROM + ("" + searchvue.form_data.S_MM_FROM).lpad(2,'0');
		searchvue.form_data.YYYYMM_TO =  searchvue.form_data.S_YYYY_TO + ("" + searchvue.form_data.S_MM_TO).lpad(2,'0');
		setHistoryX( searchvue.form_data.S_YYYY_FROM,  searchvue.form_data.S_MM_FROM,  searchvue.form_data.S_YYYY_TO,  searchvue.form_data.S_MM_TO)
	}

	function dateFormat(str){
		var i = new Date(str);
		return i.getFullYear() + (i.getMonth() > 9 ? "" : "0") + (i.getMonth() + 1);
	}

	// 엑셀 내보내기
	function exportExcel(){
		mmGrid.exportCSV({fileName:'MonthlyFee'})

	}

</script>


