<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 입력 Form -->
<div class="tab-buttons">
	<fm-sbutton cmd="update" class="btn contentsBtn tabBtnImg del" onclick="changeApproved('N')"><spring:message code="btn_approval_deny" text="반려"/></fm-sbutton>
	<fm-sbutton cmd="update" class="btn contentsBtn tabBtnImg save" onclick="changeApproved('Y')"><spring:message code="btn_approval_accept" text="승인"/></fm-sbutton>
</div>
<div class="form-panel detail-panel  panel panel-default">
	<div class="panel-body">
		<input id="USER_ID" name="USER_ID" type="hidden" v-model="form_data.USER_ID" />
		<div class="col col-sm-6">
			<fm-output id="TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<div class="fm-output"><label for="USER_NAME" class="control-label grid-title value-title"><spring:message code="FM_USER_USER_NAME" text="사용자명" /></label>
				<div id="USER_NAME" name="USER_NAME" class="output  hastitle">{{form_data.USER_NAME}}({{form_data.LOGIN_ID}})</div></div>
		</div>
		<div class="col col-sm-6">
			<fm-output id="INS_TMS" name="INS_TMS" title="<spring:message code="label_search_date_date" text="가입일자" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="EMAIL" name="EMAIL" title="<spring:message code="FM_USER_EMAIL" text="이메일" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="POSITION" name="POSITION" title="<spring:message code="FM_USER_POSITION" text="직급" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="USE_YN_NM" name="USE_YN_NM" title="<spring:message code="Approved" text="승인 여부"/>"></fm-output>
		</div>


	</div>
	<script>
	function changeApproved(state) {
		var approvalReqParam = mainTable.getSelectedRows();
		var useyn = approvalReqParam[0].USE_YN;
		var text = state === 'Y' ? '이미 승인됐습니다.' : '이미 반려됐습니다.';
		if (state === useyn) {
			return alert(text);
		}
		if(approvalReqParam.length === 0){
			return alert(msg_select_first);
		} else {
			approvalReqParam[0].USE_YN = state
			post('/api/user_approval/save', approvalReqParam[0], function (data) {
				alert(msg_complete);
				userSearch();
			})
		}
	}
	</script>
</div>
