<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<style>
    .height24 {
        height: 24px;
    }
</style>

<!-- 서버 목록 조회 -->
<div id="search_area" class="search-panel panel panel-default">
    <div class="panel-body">
        <!-- 조회의 id는 S_를 붙인다  -->
        <div class="col col-sm">
            <fm-input id="S_USER_NAME" name="USER_NAME"
                      title="<spring:message code="FM_USER_USER_NAME" text="사용자명" />"></fm-input>
        </div>
        <div class="col col-sm">
            <fm-input id="S_LOGIN_ID" name="LOGIN_ID"
                      title="<spring:message code="NC_ETC_FEE_USER_ID" text="사용자 ID" />"></fm-input>
        </div>
        <div class="col col-sm" style="padding-left: 15px;margin:0px;">
            <fm-date-from-to id="DATE_TMS" name="DATE_TMS" title="<spring:message code="label_search_date_date"/>"></fm-date-from-to>
        </div>
        <!--
        <div class="col col-sm">
            <table>
                <tr>
                    <td>
                        <fm-date id="S_DATE_TMS_FROM" name="DATE_TMS_FROM"
                                 title="<spring:message code="label_search_date_date" text="" />"></fm-date>
                    </td>
                    <td class="tilt">~</td>
                    <td>
                        <fm-date id="S_DATE_TMS_TO" name="DATE_TMS_TO"></fm-date>
                    </td>
                </tr>
            </table>
        </div>
        -->
        <div class="col col-sm">
            <fm-select url="/api/code_list?grp=USER_APPROVE" id="USE_YN"
                       keyfield="USE_YN" titlefield="USE_YN_NM" emptystr=" "
                       name="USE_YN" title="<spring:message code="approved" text="승인 여부"/>">
            </fm-select>
        </div>
        <div class="col btn_group nomargin" style="padding-left:5px;">
            <fm-sbutton cmd="search" class="searchBtn" onclick="userSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
        </div>
        <div id="popup-button-html">
        </div>
    </div>
</div>

<div class="fullGrid" id="input_area" style="height:450px">
    <div class="table_box-shadow">
        <div class="table_title layout name">
            <div class="search_info">
                <spring:message code="mainTable_Search_information" text="검색정보"/>
                <span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span
                        id="mainTable_total">0</span>&nbsp)</span>
            </div>
            <div class="btn_group">
                <button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>"
                        onclick="exportExcel()" class="layout excelBtn"></button>
            </div>
        </div>
        <div class="layout background mid">
            <div id="mainTable" class="ag-theme-fresh" style="height: 450px"></div>
        </div>
    </div>

    <div class="tab-content" style="margin-top: 100px;">
        <div class="form-panel detail-panel tab-pane active" id="tab1">
            <jsp:include page="detail.jsp"></jsp:include>
        </div>
    </div>
</div>
<script>
    var userReq = new Req();
    mainTable;

    $(function () {
        var columnDefs = [
            {headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="팀" />", field : "TEAM_NM"},
            {headerName: "<spring:message code="FM_USER_USER_NAME" text="사용자명" />", field: "USER_NAME"},
            {headerName : "<spring:message code="FM_USER_POSITION" text="직급" />", field : "POSITION"},
            {headerName: "<spring:message code="app\roved" text="승인 여부" />", field: "USE_YN_NM"},
            {
                headerName: "<spring:message code="label_search_date_date" text="가입 일자" />", field: "INS_TMS",
                valueGetter: function (params) {
                    if (params.data && params.data.INS_TMS) {
                        console.log('asd',params);
                        return formatDate(params.data.INS_TMS, 'datetime')
                    }
                    return "";
                }
            }
        ]
        var
            gridOptions = {
                hasNo: true,
                columnDefs: columnDefs,
                rowModelType: 'infinite',
                cacheBlockSize: 100,
                rowSelection: 'single',
                rowData: [],
                suppressHorizontalScroll: false,
                rowStyle: {},
                enableSorting: true,
                enableColResize: true,
                enableServerSideSorting: true,
                onSelectionChanged: function () {
                    var arr = mainTable.getSelectedRows();
                    userReq.getInfo('/api/user_approval/info?USER_ID=' + arr[0].USER_ID, function (data) {
                        if (form_data && form_data.INS_TMS) {
                            form_data.INS_TMS = formatDate(form_data.INS_TMS, 'datetime');
                        }
                    });
                },
            }
        mainTable = newGrid("mainTable", gridOptions);
        userSearch();
    });

    // 조회
    function userSearch() {
        userReq.searchPaging('/api/user_approval/list', mainTable, function () {
            mainTable.gridOptions.api.sizeColumnsToFit()
        });
    }

    //
    function passwordReset() {
        if (chkSelection()) {


            post("/api/user_mng/passwordReset", form_data, function (data) {
                userSearch();
            });
        }
    }

    function chkSelection() {
        var arr = mainTable.getSelectedRows();
        if (arr[0]) {
            return true;
        }
        alert(msg_select_first);
        return false
    }

    // 엑셀 내보내기
    function exportExcel() {
        exportExcelServer("mainForm", '/api/user_mng/list_excel', 'Snapshot', mainTable.gridOptions.columnDefs, userReq.getRunSearchData())
    }

</script>


