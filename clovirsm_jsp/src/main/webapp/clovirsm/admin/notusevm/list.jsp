<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-input id="S_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="NC_VRA_CATALOGREQ_USER_NAME" text="" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" class="btn searchBtn" onclick="search()"><spring:message code="btn_search" text="" /></button>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
	</div>
	<br />
</div>
<script>
	var notusevmReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_VM_VM_NM" text="" />", field : "VM_NM"},
		                  {headerName : "<spring:message code="INS_ID" text="" />", field : "USER_NAME"},
		                  {headerName : "<spring:message code="NC_VM_USER_TEAM_NM" text="" />", field : "TEAM_NM"},
		                  {headerName : "IP",field : "PRIVATE_IP"},
		                  {headerName : "<spring:message code="NC_VM_LAST_USE_TMS" text="" />", field : "LAST_USE_TMS",},
		                  {headerName : "<spring:message code="NC_VM_NOT_USE_DAY" text="" />", field : "NOT_USE_DAY",
		                	  valueGetter: function(params) {
		                		  var num = params.data.NOT_USE_DAY;
		                		  var n = Math.floor(num);
					    		  return n;
						  }},
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		mainTable = newGrid("mainTable", gridOptions);
		search();
	});

	// 조회
	function search() {
		notusevmReq.search('/api/not_use_vm/list', function(data){mainTable.setData(data)});
	}

	function setGridValues(key, value){
		var arr = mainTable.getSelectedRows();
		for(var i=0; i<arr.length; i++){
			arr[i][key] = value;
		}
//		for(var i in arr){
//		}
	}

	// 엑셀 내보내기
	function exportExcel() {
		 mainTable.exportCSV({fileName:'notuseVM'})
		  
	}

</script>