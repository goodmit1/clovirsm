<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.clovirsm.service.admin.DdicUserService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
Map param=new HashMap();
DdicUserService ddicUserService = (DdicUserService)SpringBeanUtil.getBean("ddicUserService");
param.put("DD_ID", "PURPOSE");
List<Map> list = ddicUserService.list(param);
request.setAttribute("list", list);
%>
<!--  접근 목록 조회 -->
<style>
.pic{
    margin-left: 15px;
    font-weight: bold;
    color: #00a832;
   }    
.pic .del{
    display: inline-block;
    overflow: hidden;
    width: 9px;
    height: 9px;
    margin: -2px 10px 0 6px;
    background: url(https://ssl.pstatic.net/imgshopping/static/pc-real-2017/img/search/sp_search_v4.png) no-repeat -180px -50px;
    line-height: 9999px;
    vertical-align: middle;
   }     
#filter_area{
 	margin-left: 50px;
 }  
.bar {
    margin: 0 -4px 0 12px;
    padding: 0;
  }
.clear {
    display: inline-block;
    overflow: hidden;
    margin-left: 15px;
    font-size: 12px;
    color: #6b6b6a;
    vertical-align: middle;
	}   
</style>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div id="filter_area" style="float:left"></div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px" ></div>
	</div>
	<br />
</div>
<script>
	var clusterGroupReq = new Req();
	mainTable;
	var	clusterGrop = new Array();
	var columnDefs;
	$(function() {
		columnDefs = [{hide:true, field : "CATEGORY_ID"},
		                 
		          		  <c:forEach items="${ list }" var="i" varStatus="status">
		                  	{headerName : '${i.KO_DD_DESC}', field:'${i.DD_VALUE}', editable : true,cellEditor: "select", cellEditorParams: {values: []}},
		                  </c:forEach>
		                  ]
		var
		gridOptions = {
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onCellClicked : function(params){
				
				if("A"==params.event.srcElement.tagName) return ;
				if(params.colDef.field.indexOf("NAME")<0 || params.value=="" || !params.value) return;
				filter(params.colDef.field, params.value)
			},
		    onCellEditingStarted: function(event) {
		    	 <c:forEach items="${ list }" var="i" varStatus="status">
				if(event.column.colId=='${i.KO_DD_DESC}'){
					$("#mainTable div.ag-cell-value[col-id='${i.DD_VALUE}'] select").select2({

					  data : clusterGrop,
					  tags: true,

					  createTag: function (params) {
						    return {
						      id: params.term,
						      text: params.term,
						      newOption: true
						    }
						  }})
				}
				</c:forEach>
		    },
			onCellValueChanged: function(params) {
				if(params.newValue != params.oldValue)
				{
						var obj = {DC_ID : params.data.CATEGORY_ID, ID: params.colDef.field ,CATEGORY_ID: params.colDef.field ,KUBUN:"CATE_KUBUN_CL",VAL1:params.newValue}
						post("/api/category_conf/save_clusterGroup", obj, function(data){
							mainTable.gridOptions.api.redrawRows();
						});	
				}	
		       
		    },
		}
		mainTable = newGrid("mainTable", gridOptions);
		initClusterGroup();
		search();
	});
	function allClearFilter()
	{
		mainTable.gridOptions.api.setFilterModel(null);
		mainTable.gridOptions.api.onFilterChanged();
		 $("#filter_area").html(""); 
	}
	function clearFilter(obj, field){
		var filter = mainTable.gridOptions.api.getFilterInstance(field);
		 filter.setFilter("");

		 mainTable.gridOptions.api.onFilterChanged();
		 $(obj).remove();
		 if($("#filter_area .pic").length==0)
		 {
			 $("#filter_area").html(""); 
		 }
	}
	function applyAllFilter()
	{
		 $("#filter_area .pic").each(function(){
			 
			 
			 var filter = mainTable.gridOptions.api.getFilterInstance($(this).attr("data-field"));
			 filter.setFilter($(this).attr("data-val"));

			 mainTable.gridOptions.api.onFilterChanged();
		 })
	}
	function filter(field,text)	{
		var filter = mainTable.gridOptions.api.getFilterInstance(field);
		 filter.setFilter(text);

		 mainTable.gridOptions.api.onFilterChanged();
		 if( $("#filter_area").html()=="") {
			 $("#filter_area").append('<a id="_resetFilter" href="#" class="clear" onclick="allClearFilter();return false"><span class="ico_clear"></span><spring:message code="all_clear" text="전체해제"/></a><span class="bar">|</span>')
		 }
		 $("#filter_area").append("<a class='pic' href='#' data-field='" + field + "' data-val='" + text + "' onclick='clearFilter(this, \"" + field + "\");return false'>" + text + "<span class='del'>Clear</span></a>");
	}
	// 조회
	function search() {
		
		clusterGroupReq.search('/api/category_conf/list_cluster_group_setting', function(data){
			 var maxLevel = data.maxLevel;
			 var colArr = [];
			 for(var i=0; i < maxLevel; i++) {
				 colArr.push({	
					 headerName : "<spring:message code="TASK" text="업무" />" + (i+1),   
				 	field : "NAME_" + i})
			 }
			mainTable.gridOptions.api.setColumnDefs($.merge(colArr, columnDefs));
			mainTable.setData(data.list);
		 	applyAllFilter();
				
		});
	}
	
	

	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'fee'})
	 
	}
	function initClusterGroup() {
		post('/api/code_list?grp=CLUSTER_GROUP', {}, function(data) {
			for(i in data){
				clusterGrop.push(data[i])
			}
		});
		
	}
</script>