<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<fm-modal id="template_edit_popup" title="<spring:message code="label_standard_template" text="표준 템플릿"/>" >
	<span slot="footer">
		<input type="button" id="template_delBtn" class="btn delBtn  " value="<spring:message code="btn_delete" text=""/>" onclick="template_delete()" />
		
		<input type="button" class="btn exeBtn" value="<spring:message code="label_confirm" text=""/>" onclick=" template_save()" />
		
		
	</span>
	<div class="form-panel detail-panel">
		<form id="template_edit_popup_form" action="none">
			<div class="panel-body">
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="OS_DC_ID"
						emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
						name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
					</fm-select>
				</div>
					<div class="col col-sm-6">
					<fm-input id="OS_NM"  name="OS_NM" required="true" title="<spring:message code="NC_OS_TYPE_OS_NM" text="템플릿명" />"  ></fm-input>
					
				</div>
				 <div class="col col-sm-12" style="height: 140px;">
					<fm-textarea id="OS_CMT" name="CMT" title="<spring:message code="NC_IMG_CMT" text="설명" />"></fm-textarea>
				</div>
			</div>
		</form>
	</div>
	<script>
		 
		$(document).ready(function(){
			autocomplete('#OS_NM',function(){
				return '/api/code_list?grp=dblist.com.clovirsm.common.Component.selectTemplate&DC_ID=' + $("#OS_DC_ID").val()
					
			},'OBJ_NM','OBJ_NM', 1);
		})
		var template_edit_popup_vue = new Vue({
			el: '#template_edit_popup',
			data: {
				form_data: {}
			}
		});
		function template_edit_popup_click(  param){
			
			if(!param.OS_ID)
			{
				$("#template_delBtn").hide();
				$("#template_edit_popup_title").text("<spring:message code="label_standard_template" text="표준 템플릿"/><spring:message code="label_add" text="추가"/>")
			}
			else
			{
				$("#template_delBtn").show();
				$("#template_edit_popup_title").text("<spring:message code="label_standard_template" text="표준 템플릿"/><spring:message code="label_modify" text="변경"/>")
			}
			 
			template_edit_popup_vue.callback = param.callback;
			template_edit_popup_vue.form_data.OS_NM=param.OS_NM;
			template_edit_popup_vue.form_data.CATEGORY=param.CATEGORY;
			template_edit_popup_vue.form_data.OS_ID=param.OS_ID;
			template_edit_popup_vue.form_data.DC_ID=param.DC_ID;
			template_edit_popup_vue.form_data.P_KUBUN=param.P_KUBUN
			$("#OS_NM").val(param.OS_NM);
			$("#OS_DC_ID").val(param.DC_ID);
			$("#OS_CMT").val(param.CMT);
			return true;
		}
		function template_delete()
		{
			var param =  template_edit_popup_vue.form_data;
			if(confirm(msg_confirm_delete))
			{	
				post('/api/ostype/delete', param, function(data){
					ostypeSearch();
					$("#template_edit_popup").modal("hide");
				})
			}
		}
		function template_save()
		{
			if(!validate("template_edit_popup")) return;
			
			 
				
			var param =  template_edit_popup_vue.form_data;
			param.OS_NM= $("#OS_NM").val();
			param.TMPL_PATH= $("#OS_NM").val();
			param.DC_ID= $("#OS_DC_ID").val();
			param.CMT= $("#OS_CMT").val();
			post('/api/ostype/save', param, function(data){
				ostypeSearch();
				$("#template_edit_popup").modal("hide");
			});
			 
		}
		
	</script>
	<style>
	</style>
</fm-modal>