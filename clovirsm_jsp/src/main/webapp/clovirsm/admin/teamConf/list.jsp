<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
	#S_TEAM_NM{
		width: 100%;
	}
	#TEAM_NM{
		width: 100%;
	}
	.input-group1{
		width:55%
	}
	div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none{
		width: auto;
	}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
					   keyfield="DC_ID" titlefield="DC_NM"
					   name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
			</fm-select>
		</div>
		<div class="col col-sm-3" style=" width: 280px;">
			<fm-ibutton id="S_TEAM_NM"  name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_NM" text=""/>" class="inline">
				<fm-popup-button popupid="team_search" popup="/clovirsm/popup/team_search_form_popup.jsp" cmd="update" class="newBtn" param="" callback="selectSTeam"></fm-popup-button>
			</fm-ibutton>
		</div>
		<div class="col btn_group nomargin">
			<input type="button" id="searchBtn" onclick="searchTeamConf()" class="searchBtn btn" value="<spring:message code="btn_search" text="검색" />">
			<label onclick="searchTeamConf()" for="input"></label>
		</div>

	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportTeamConf()" class="btn" id="excelBtn"></button>
			<fm-sbutton cmd="search" class="btn btn-primary contentsBtn tabBtnImg new" onclick="sync()"  ><spring:message code="btn_sync_ip" text="IP정보수집"/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary contentsBtn tabBtnImg new"  onclick="newTeamConf()"><spring:message code="btn_new" text="신규"/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary saveBtn  contentsBtn tabBtnImg save" onclick="saveTeamConf()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary delBtn  contentsBtn tabBtnImg del"  onclick="deleteTeamConf()"><spring:message code="btn_delete" text="삭제"/></fm-sbutton>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height:450px"></div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
	function selectSTeam(data){
		searchvue.form_data.TEAM_CD = data.TEAM_CD;
		searchvue.form_data.TEAM_NM = data.TEAM_NM;
		$("#S_TEAM_NM").val(data.TEAM_NM);
	}
	function selectTeam(data){
		inputvue.form_data.TEAM_CD = data.TEAM_CD;
		inputvue.form_data.TEAM_NM = data.TEAM_NM;
		$("#TEAM_NM").val(data.TEAM_NM);
	}

	function newTeamConf(){
		$("#TEAM_NM").next().find("button").prop("disabled", false);
		$("#TEAM_NM").val("");
		inputvue.form_data.CLUSTER_IU = "I";
		inputvue.form_data.TEAM_ETC_CD_IU = "I";
		inputvue.form_data.IP_IU = "I";
		inputvue.form_data.DC_ID = "";
		inputvue.form_data.IP = "";
		inputvue.form_data.ORI_IP = "";
		inputvue.form_data.CLUSTER = "";
		inputvue.form_data.TEAM_ETC_CD = "";
		inputvue.form_data.TEAM_CD = "";
		inputvue.form_data.TEAM_NM = "";
		document.getElementById("TEAM_NM").focus();
	}

	var teamConf = new Req();
	function exportTeamConf(){
		mainTable.exportCSV({fileName:'teamConf.csv'})
	}
	function deleteTeamConf(){
		if(confirm(msg_confirm_delete)){
			inputvue.form_data.IP = "";
			inputvue.form_data.CLUSTER = "";
			inputvue.form_data.TEAM_ETC_CD = "";
			inputvue.form_data.CLUSTER_IU = "D";
			inputvue.form_data.TEAM_ETC_CD_IU = "D";
			inputvue.form_data.IP_IU = "D";
			saveTeamConf();
		}
	}
	function saveTeamConf(){
		msg='<spring:message code="team_ip_guide_cidr" text="CIDR 형식으로 입력해주세요 예:10.3.10.0/24,10.230.112.0/24" />'
		var ipVal =  $('#IP').val();
		if(cidrValidate() || ipVal === null || ipVal === ''){
			form_data.DC_ID=search_data.DC_ID;
			teamConf.save("/api/team_conf/save", function(){
				searchTeamConf();
			})
		}else{
			alert(msg)
		}
	}
	function searchTeamConf(){
		teamConf.search('/api/team_conf/list', function(data){
			dcChange();
			mainTable.setData(data);
		});
	}

	var mainTable;

	$(function(){
		var msg='';

		var columnDefs =[
			{ headerName: "", field: "DC_ID", hide: true },
			{ headerName: "", field: "TEAM_CD", hide: true },
			{ headerName: '<spring:message code="FM_TEAM_TEAM_NM" text=""/>', field: "TEAM_NM" },
			{ headerName: '<spring:message code="NC_DC_DC_ID" text=""/>', field: "DC_NM", width:100 },
			{ headerName: '<spring:message code="IP" text="IP"/>', field: "IP"},
			{ headerName: '<spring:message code="NC_TEAM_CONF_CLUSTER" text=""/>', field: "CLUSTER", width:70 },
			{ headerName: '<spring:message code="CODE" text="Code"/>', field: "TEAM_ETC_CD", width:70 },
		];
		var gridOptions = {
			columnDefs: columnDefs,
			rowData: [],
			rowSelection : 'single',
			enableSorting: true,
			enableColResize: true,
			onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				$("#TEAM_NM").next().find("button").prop("disabled", true);
				teamConf.getInfo('/api/team_conf/info?DC_ID='+ arr[0].DC_ID + '&TEAM_CD=' + arr[0].TEAM_CD, function(data){
					inputvue.form_data.ORI_IP = data.IP;
				});
			}
		};
		mainTable = newGrid("mainTable", gridOptions)

		setTimeout(function(){searchTeamConf();}, 100)
	});
	//동기화
	function sync() {
		post('/api/ip/sync',{}, function(data){
			alert("<spring:message code="ip_collect_req_msg" text="IP정보를 수집요청하였습니다.(몇 분 소요)"/>");

		} )
	}
</script>