<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<div class="table_title layout name">
				<spring:message code="NC_DC_DISK_TYPE_TITLE" text="디스크"/> <span> ( <spring:message code="count" text="건수"/> : <span id="diskTable_total">0</span> ) </span>
				
				<div class="btn_group">
						<input id="diskFilter" style="vertical-align: middle;" onkeyup="diskTable.gridOptions.api.setQuickFilter(this.value)" placeholder="Filter.." class="layout postion">
						<button type="button" onclick="diskInsertRow()" style="position: relative;" class="btn icon layout postion"><i class="fa fat add fa-plus-square"></i></button>
						<button type="button" onclick="diskDeleteRow()" style="position: relative;" class="btn icon layout postion"><i class="fa fat fa-minus-square"></i></button>
				 		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel(diskTable, 'DISK')" class="excelBtn layout name" id="excelBtn"></button>
				</div>

	</div>
	<div id="diskTable" class="ag-theme-fresh" style="height: 200px" ></div>
	<script>
	var param;
		function diskInsertRow()
		{
			param = mainTable.getSelectedRows()[0];
			param.oldDisk = diskTable.getData();
// 			diskTable.insertRow({});
			$("#diskMulti_popup_button").click();
		}
		var diskTable;
		var diskTypeObj={ };

		function getDiskData(param)
		{
			post('/api/dc_mng/list/list_NC_DC_DISK_TYPE/', param, function(data){
				diskTable.setData(data);
			})
		}
		function diskDeleteRow()
		{

			var selRowArr = diskTable.getSelectedRows();
			if(selRowArr.length==0 )
			{
				alert(msg_select_first);
				return false;
			}
			else
			{
				if(!confirm(msg_confirm_delete)) return false;
			}
			var svrDelRow=[];
			var delRow=[];
			for(var i=0; i < selRowArr.length; i++)
			{
				if(selRowArr[i].IU=='I')
				{
					delRow.push(selRowArr[i]);
				}
				else
				{
					svrDelRow.push(selRowArr[i]);
				}
			}
			diskTable.deleteRow(delRow);
			if(svrDelRow.length>0)
			{
				var param = {};
				param.deleteRows=JSON.stringify(svrDelRow);
				post('/api/dc_mng/delete/disk_type', param, function(data){
					getDiskData(form_data)
				})
			}
		}
		function initDiskTable()
		{
			var columnDefs = [{headerName : "<spring:message code="NC_DC_DISK_TYPE_DS_NM" text="" />*",field : "DS_NM",
	            required:true
	            },
				{headerName : "<spring:message code="NC_DC_DISK_TYPE_DISK_TYPE_ID" text="" />*",field : "DISK_TYPE_ID", width:100, cellEditor: "select",
		            cellEditorParams: {
		                values: extractKeys(diskTypeObj)
		            	},

		            valueFormatter: function (params) {
		                return lookupValue(diskTypeObj, params.value);
		            	},
		            valueParser: function (params) {
		                return lookupKey(diskTypeObj, params.newValue);
		            	},
		            required:true
				},

				];
			var
			gridOptions = {

				editable:true,
				hasNo : true,
				columnDefs : columnDefs,
				//rowModelType: 'infinite',
				//rowSelection : 'single',
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false,
				onCellEditingStarted: function(event) {
					if(event.column.colId=='DS_NM')
					{
						$("#diskTable div.ag-cell-value[col-id='DS_NM'] select").select2({

						  data : objList.Datastore,
						  tags: true,

						  createTag: function (params) {
							    return {
							      id: params.term,
							      text: params.term,
							      newOption: true
							    }
							  }})
					}
			    },
			}
			diskTable = newGrid("diskTable", gridOptions);

		}
		$(function() {

			$.get('/api/code_list?grp=db.com.clovirsm.common.Component.selectDiskType', function(data){

				Object.keys(data).forEach(function(item, index){
					diskTypeObj[item] = data[item];
				})

				initDiskTable();
			})


		});
	</script>