<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->

	<div class="form-panel detail-panel detail-resize panel panel-default">

			<div class="panel-body" id="inputForm">
					<div class="col col-sm-4">
						<fm-input id="DC_NM" name="DC_NM" required="true"  title="<spring:message code="NC_DC_DC_NM" text="데이터 센터" />"></fm-input>
					</div>
					<div class="col col-sm-4">
						<fm-output name="HV_CD_NM"  title="<spring:message code="NC_DC_HV_CD" text="Hypervisor코드"/>"></fm-output>
						 
					</div>
					<div class="col col-sm-4">
						<fm-select url="/api/code_list?grp=sys.yn" id="DEL_YN"
								name="DEL_YN" title="<spring:message code="NC_DC_DEL_YN" text="삭제 여부"/>" tooltip="<spring:message code="DEL_YN_EX" text="사용자 화면에 표시여부 '예'일시 표시 X"/>"></fm-select>
					</div>
					<div class="col col-sm-4">
						<fm-input id="CONN_URL" name="CONN_URL" required="true"  title="<spring:message code="CONN_URL" text="Region" />"></fm-input>
					</div>
				<div class="col col-sm-4" id="REAL_DC_NM_area">
					<fm-input id="REAL_DC_NM" name="REAL_DC_NM" required="true"  title="<spring:message code="NC_DC_REAL_DC_NM" text="REAL_DC_NM" />" tooltip="<spring:message code="vcenter_dc_nm" text="vCenter에 등록된 데이터 센터명" />"></fm-input>
				</div>
				<div class="col col-sm-4">
					<fm-select url="/api/code_list?grp=sys.yn" id="IP_AUTO_YN"
							   name="IP_AUTO_YN" title="<spring:message code="" text="IP 자동"/>"></fm-select>
				</div>
				<div class="col col-sm-4">
					<fm-select url="/api/code_list?grp=db.com.clovirsm.common.Component.selectDiskType" id="IMG_DISK_TYPE_ID" name="IMG_DISK_TYPE_ID" required="true"  title="<spring:message code="NC_DC_IMG_DISK_TYPE_ID" text="템플릿 디스크종류" />"></fm-select>
				</div>
				<div class="col col-sm-4">
					<fm-input id="INIT_POOL_SIZE" name="INIT_POOL_SIZE" title="<spring:message code="NC_DC_CONNECTION_COUNT" text="초기 연결 갯수" />" tooltip="초기 Connection  Pool 갯수"></fm-input>
				</div>
				<div class="col col-sm-4">
					<fm-input id="CONN_USERID" name="CONN_USERID" required="true"  title="<spring:message code="CONN_USER_ID" text="Client ID" />"></fm-input>
				</div>
				<div id="azureProperties" class="hide">
					<div class="col col-sm-6">
						<fm-input id="tenantId" name="TENANT_ID" title="<spring:message code="" text="TENANT"/>"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-input id="subscriptionId" name="SUBSCRIPTION_ID" title="<spring:message code="" text="SUBSCRIPTION" />"></fm-input>
					</div>
				</div>
				<div class="col col-sm-6" style="width:100%">
					<fm-password id="CONN_PWD" autocomplete="new-password" name="CONN_PWD" required="true" title="<spring:message code="CONN_PWD" text="Client Secret" />"></fm-password>
				</div>
				<div class="col col-sm-6" style="height: 80px; width:100%">
					<fm-textarea id="CMT" name="CMT"   title="<spring:message code="NC_DC_CMT" text="CMT" />"></fm-textarea>
				</div>
	</div>
	* <spring:message code="dc_change_restart" text="IP, 계정정보가 바뀌었을 경우 저장 후 restart해야 합니다. cluster, host, datastore등을 vcenter에 추가하였을 경우  선택 후 수집버튼을 클릭하세요"></spring:message>
</div>
<script>
	$(document).ready(function(){

	})
</script>
