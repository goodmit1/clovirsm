<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">

	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
				  keyfield="DC_ID" titlefield="DC_NM"
				name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
			</fm-select>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" id="searchBtn" onclick="search()" class="searchBtn btn"><spring:message code="btn_search" text="" /></button>
		</div>
<!-- 		<div class="col btn_group_under"> -->
<%-- 			<fm-sbutton cmd="search" class="exeBtn" onclick="sync()"  ><spring:message code="btn_sync_ip" text="IP정보수집"/></fm-sbutton> --%>
<%-- 			<fm-sbutton cmd="update" class="saveBtn" onclick="save()"><spring:message code="btn_save" text="" /></fm-sbutton> --%>
<!-- 		</div> -->
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"><spring:message code="btn_excel_download" text="엑셀다운로드"/></button>
			<fm-sbutton cmd="search" class="exeBtn contentsBtn tabBtnImg collection" onclick="sync()"  ><spring:message code="btn_sync_ip" text="IP정보수집"/></fm-sbutton>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save" onclick="save()"><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 600px" ></div>
	</div>
	<spring:message code="ip_input_msg" text="IP는 여러개 입력할 때는 ,로 구분합니다. 사용예)10.1.2.0/24,10.1.3.0/24" />
	<br />
</div>
<script>
	var portgroupReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [
						  {headerName : "<spring:message code="NC_PORTGROUP_ID" text="Network" />", field : "NM"},
		                  
		                  {headerName : "<spring:message code="NC_PORTGROUP_VAL1" text="IP" />", width:600,  field : "VAL1", editable: true}
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		mainTable = newGrid("mainTable", gridOptions);
		setTimeout(function(){search();},100);
	});

	// 조회
	function search() {
		portgroupReq.search('/api/portgroup/list', function(data){mainTable.setData(data)});
	}
	//동기화
	function sync() {
		post('/api/ip/sync',{}, function(data){
			alert("<spring:message code="ip_collect_req_msg" text="IP정보를 수집요청하였습니다.(몇 분 소요)"/>");
		});
	}
			 
		
	function save() {
		if(confirm(msg_confirm_save)){
			mainTable.stopEditing();
			var arr = mainTable.getData();
			var list = [];
			for(var i=0; i<arr.length; i++){
				list.push(arr[i]);
			}
//			for(var i in arr){
//			}
			post('/api/portgroup/save_multi', {selected_json: JSON.stringify(list)}, function(){
				alert("<spring:message code="saved" text="저장되었습니다"/>");
				search();
			})
		}
	}

	// 엑셀 내보내기
	function exportExcel() {
		  exportExcelServer("mainForm", '/api/portgroup/list_excel', 'vmuser',mainTable.gridOptions.columnDefs, portgroupReq.getRunSearchData())
	}

</script>