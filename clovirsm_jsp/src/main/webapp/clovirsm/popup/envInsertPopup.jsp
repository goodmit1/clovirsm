<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>

#shell_popup .modal-dialog{
	width : 1000px;
}
#${popupid} .modal-footer {
    border-top: none;
}
</style>
<fm-modal id="${popupid}" title='<spring:message code="SW_ENV_POPUP" text="변수"/>' cmd="header-title">
	<span slot="footer" >
		<fm-sbutton cmd="update" class="contentsBtn tabBtnImg save top5" onclick="envSave()"   ><spring:message code="btn_save" text="저장"/></fm-sbutton>
		<!-- <button type="button" class="contentsBtn tabBtnImg save top5" onclick="envSave()">저장</button> -->
	</span>
	<div class="form-panel detail-panel" style="height: 217px; border: 1px solid #e5e5e5;">
			<fm-hidden id="${popupid}_SW_ID" name="SW_ID" />
			<%-- <fm-output id="${popupid}_SW_ID" name="SW_ID" title="<spring:message code="SW_ENV_NAME" text="SW ID"/>"></fm-output> --%>
			<div class="col col-sm-12">
				<fm-input id="${popupid}_NAME" name="NAME" title="<spring:message code="SW_ENV_NAME" text="ENV명"/>" required="required"></fm-input>
			</div>
			<div class="col col-sm-6">
				<fm-input id="${popupid}_TITLE" name="TITLE" title="<spring:message code="SW_ENV_TITLE" text="제목"/>" required="required"></fm-input>
			</div>
			<div class="col col-sm-6">
				<fm-select url="/api/code_list?grp=SW_ENV_TYPE" id="${popupid}_TYPE" onchange="${popupid}_changeType(this)" required="required"
						name="TYPE" title="<spring:message code="SW_ENV_TYPE" text="타입"/>">
				</fm-select>
			</div>
			<div class="col col-sm-6">
				<fm-select-yn id="${popupid}_ENCRYPTED" name="ENCRYPTED" title="<spring:message code="SW_ENV_ENCRYPTED" text="암호여부"/>"></fm-select-yn>
			</div>
			<div class="col col-sm-6">
				<fm-select-yn id="${popupid}_REQUIRED" name="REQUIRED" title="<spring:message code="SW_ENV_REQUIRED" text="필수여부"/>" required="required">
				</fm-select-yn>
			</div>
			<div class="col col-sm-6">
				<fm-input id="${popupid}_MINIMUM" name="MINIMUM" title="<spring:message code="SW_ENV_MIN" text="최소값"/>"></fm-input>
			</div>
			<div class="col col-sm-6">
				<fm-input id="${popupid}_MAXIMUM" name="MAXIMUM" title="<spring:message code="SW_ENV_MAX" text="최대값"/>"></fm-input>
			</div>
			<div class="col col-sm-6">
				<fm-input id="${popupid}_PATTERN" name="PATTERN" title="<spring:message code="SW_ENV_PATTERN" text="정규식"/>"></fm-input>
			</div>
			<div class="col col-sm-6">
				<fm-input id="${popupid}_DEFAULT" name="DEFAULT" title="<spring:message code="SW_ENV_DEFAULT" text="기본값"/>"></fm-input>
			</div>
			<div class="col col-sm-12">
				<fm-input id="${popupid}_DESCRIPTION" name="DESCRIPTION" title="<spring:message code="SW_ENV_DEC" text="설명"/>"></fm-input>
			</div>
	</div>

	<script>
		var editor;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready(function(){
		})

		
		
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
				alert(msg_select_first);
				return false;
			}
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		
		function ${popupid}_changeType(obj){
				var id = $(obj).attr('id');
				var type = $("#"+id).val();
				
				if(type==="string"){
					$("#${popupid}_MINIMUM").parent('div').parent('div').css('display','none')	
					$("#${popupid}_MAXIMUM").parent('div').parent('div').css('display','none')	
					$("#${popupid}_ENCRYPTED").parent('div').parent('div').css('display','inherit')	
					$("#${popupid}_PATTERN").parent('div').parent('div').css('display','inherit')
				}else{
					$("#${popupid}_ENCRYPTED").parent('div').parent('div').css('display','none')	
					$("#${popupid}_PATTERN").parent('div').parent('div').css('display','none')
					$("#${popupid}_MINIMUM").parent('div').parent('div').css('display','inherit')	
					$("#${popupid}_MAXIMUM").parent('div').parent('div').css('display','inherit')
				}
				
		}
		
		function envSave(){
			if(!validate("${popupid}")) return false;
			if(${popupid}_vue.form_data.ENCRYPTED == "N"){
				${popupid}_vue.form_data.ENCRYPTED = null;
			} 
			if(${popupid}_vue.form_data.REQUIRED == "N"){
				${popupid}_vue.form_data.REQUIRED = null;
			} 
			 
			post('/api/sw/env/save',${popupid}_vue.form_data,function(data){
				
				 
				if(data){
					$('#${popupid}').modal('hide');
					envSearch(data.SW_ID);
				} else{
					alert(msg_jsp_error);
				}
			});
		} 
		
		
	</script>
</fm-modal>