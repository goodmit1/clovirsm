<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<style>

#shell_popup .modal-dialog{
	width : 1000px;
}
#shell_popup .modal-dialog .form-panel.detail-panel{
	height : 500px;
}

</style>

<fm-modal id="${popupid}" title='<spring:message code="NEW_ITEM_ADD" text="항목 추가"/>' cmd="header-title">
	<span slot="footer">
		
		<button type="button" class="contentsBtn tabBtnImg save top5" onclick="itemSave()"><spring:message code="btn_save" text="저장"/></button>
	</span>
	<div class="form-panel detail-panel" style="height: 55px;">
		<div class="col col-sm-12" style="text-align: left;">
			<label for="item" style="min-width: 56px; background-color: white;" class="control-label grid-title value-title"><spring:message code="NEW_ITEM" text="새 항목"/></label>
			<input name="item" id="item" style="width: 290px;" class="form-control input hastitle"/>
		</div>
	</div>
	<script>
		var appr = null;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function ${popupid}_click(vue, param, callback){
			 
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_vue.form_data.callback=callback;
			$("#item").focus();
			delete ${popupid}_param.newItem;
			return true;
		}
		function getDdValue(){
		 
			 
			return "appr"+ Object.keys(${popupid}_param).length; 
		}
		function itemSave(){
			var param = new Object();
			param.DD_ID = "APPR_COMMENT";
			param.DD_VALUE = getDdValue();
			param.DD_NM = "결재의견";
			param.KO_DD_DESC = $("#item").val();
			param.USE_YN = 'Y';
			post('/api/ddicUser/save',param,function(data){
				if(data){
					eval(${popupid}_vue.form_data.callback + "(param)");
					$('#${popupid}').modal('hide');
				} else{
					alert(msg_jsp_error);
				}
			});
		}
	</script>
	<style>

	</style>
</fm-modal>