<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	request.setAttribute("action", "update");
%>
<style>
#${popupid} > .modal-dialog {
	width: 60%;
}
</style>
<fm-modal id="${popupid}" title="<spring:message code="title_vm_spec_popup" text="title_vm_spec_popup"/>" cmd="header-title">
	<span slot="footer" id="${popupid}_footer">
		<input type="button" class="btn saveBtn" value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_to_work()" />
	</span>
	<form id="${popupid}_form" action="none">
		<div class="search-panel panel panel-default">
			<div class="panel-body">
				<div class="col col-sm-3">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN" emptystr=" "
						name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
					</fm-select>
				</div>
				<div class="col col-search">
					<fm-input id="keyword_input" class="keyword" name="keyword"
						placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_VM_GUEST_NM" text="OS명" />/<spring:message code="NC_VM_PRIVATE_IP" text="IP" />/<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"
						title="<spring:message code="keyword_input" text="키워드" />">
					</fm-input>
					<fm-sbutton cmd="search" class="searchBtn" onclick="${popupid}_search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
				</div>
			</div>
		</div>
		<div id="${popupid}_grid" style="height:250px" class="ag-theme-fresh"></div>
		<div id="detail" class="panel-default panel detail-panel" style="display:none">
			<div class="panel-body">
				<fmtags:include page="_spec_form.jsp" />
			</div>
		</div>
	</form>

	<script>
		 
		var ${popupid}_vue = makePopupVue("${popupid}", "${popupid}", {
			DISK_SIZE : 0,
			DISK_UNIT : 'G'
		})

		var ${popupid}_CPUOptions = ${popupid}_makeOptions(32, '', 'core');
		var ${popupid}_RAMOptions = ${popupid}_makeOptions(64, '', 'GB');
		function ${popupid}_makeOptions(maxSize, title_prefix, title_postfix){
			title_prefix = title_prefix?title_prefix: '';
			title_postfix = title_postfix?title_postfix: '';
			var ramList = {};
			for(var i = 1; i <= maxSize; i++){
				eval('ramList['+i+']= title_prefix+i+title_postfix;');
			}
			return ramList;
		}

		function ${popupid}_to_work(){
			if(!${popupid}_validationSpecChange()) return;
			action = 'updateReq';
			post('/api/vm/' + action, ${popupid}_vue.form_data,  function(data){
				if(${popupid}_vue.form_data.DISK_UNIT == 'T') ${popupid}_vue.form_data.DISK_SIZE *= 1024;
				${popupid}_tuingSpecInfo();
				if(confirm(msg_complete_work_move)){
					location.href="/clovirsm/workflow/work/index.jsp";
					return;
				}
				if(${popupid}_vue.callback) {
					eval(${popupid}_vue.callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_tuingSpecInfo(){
			var data = ${popupid}_vue.form_data;
			data.SPEC_INFO = getSpecInfo(data.SPEC_NM, data.CPU_CNT, data.RAM_SIZE, data.DISK_SIZE, data.DISK_UNIT, data.DISK_TYPE_NM);
		}

		function ${popupid}_validationSpecChange(){
			
			if(${popupid}_vue.form_data.OLD_SPEC_ID == ${popupid}_vue.form_data.SPEC_ID
				&& ${popupid}_vue.form_data.OLD_CPU_CNT == ${popupid}_vue.form_data.CPU_CNT
				&& ${popupid}_vue.form_data.OLD_RAM_SIZE == ${popupid}_vue.form_data.RAM_SIZE
				&& ${popupid}_vue.form_data.OLD_DISK_SIZE == ${popupid}_vue.form_data.DISK_SIZE
				&& ${popupid}_vue.form_data.OLD_DISK_UNIT== ${popupid}_vue.form_data.DISK_UNIT){
				alert('<spring:message code="msg_change_spec"/>');
				return false;
			}

			if($('#${popupid}_DISK_SIZE').hasClass('invalid-size')){
				alert('<spring:message code="msg_disk_size_re_setting"/>');
				$('#${popupid}_DISK_SIZE').select();
				return false;
			}
			try
			{
				${popupid}_makeDataDiskParam(${popupid}_vue.form_data);
			}
			catch(e)
			{
				console.log(e)
			}
			return true;
		}


		function ${popupid}_click(vue){
			${popupid}_vue.callback = vue.callback;
			${popupid}_search();
			return true;
		}

		function ${popupid}_search(){
			${popupid}_param = { MONITOR:'Y', CHG_SPEC:'Y'	}

			${popupid}_param.P_KUBUN = $("#${popupid} #S_P_KUBUN").val();
			${popupid}_param.keyword = $("#${popupid} #keyword_input").val();
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_req.searchSub('/api/vm/list', ${popupid}_param, function(data) {
				${popupid}_table.setData(data);
				$("#detail").hide();
			});
		}

		var ${popupid}_req = new Req();
		var ${popupid}_grid;
		$(document).ready(function(){
			var ${popupid}_columnDefs =[{
				headerName : '<spring:message code="NC_VM_VM_NM" text="서버명" />',
				field : 'VM_NM',
				maxWidth: 180,
				width: 180,
				minWidth: 150,
				tooltip: function(params){
					if(params.data) return params.data.VM_NM;
				}
			},{
				headerName : '<spring:message code="NC_VM_P_KUBUN" text="구분" />',
				field : 'P_KUBUN_NM',
				maxWidth: 120,
				sort_field: 'P_KUBUN',
				width: 120,
				minWidth: 120
			},{
				headerName : '<spring:message code="label_spec" text="Spec" />',
				field : 'SPEC_INFO',
				maxWidth: 140,
				width: 140,
				minWidth: 140
			},{
				headerName : '<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />',
				field : 'GUEST_NM',
				maxWidth: 400,
				width: 280,
				minWidth: 280,
				tooltip: function(params){
					if(params.data) return params.data.GUEST_NM;
				}
			},{
				headerName : '<spring:message code="NC_VM_PRIVATE_IP" text="IP" />',
				field : 'PRIVATE_IP',
				maxWidth: 140,
				width: 140,
				minWidth: 140
			},{
				headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
				field : 'TEAM_NM',
				maxWidth: 120,
				width: 120,
				minWidth: 120
			},{
				headerName : '<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />',
				field : 'INS_ID_NM',
				maxWidth: 120,
				width: 120,
				minWidth: 100
			} ];
			var ${popupid}_gridOptions = {
				columnDefs: ${popupid}_columnDefs,
				rowData: [],
				sizeColumnsToFit:true,
				enableSorting: true,
				rowSelection:'single',
			    enableColResize: true,
			    onRowClicked: function(){
					var arr = ${popupid}_table.getSelectedRows();
					${popupid}_req.getInfo('/api/vm/info?VM_ID=' + arr[0].VM_ID, function(data){
						${popupid}_vue.form_data = data;
						 
						${popupid}_click_after();
						$("#detail").show();
					} );
			    }
			};
			${popupid}_table = newGrid("${popupid}_grid", ${popupid}_gridOptions);
		});
	</script>
</fm-modal>