<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String isActiveOnly = request.getParameter("isActiveOnly");
	request.setAttribute("isActiveOnly", isActiveOnly == null ? "N": isActiveOnly);

	String isOnlyOwn = request.getParameter("isOnlyOwn");
	request.setAttribute("isOnlyOwn", isOnlyOwn == null ? "N": isOnlyOwn);
%>
<fm-modal id="${popupid}" title="<spring:message code="btn_vm_search"/>" cmd="header-title">
	<form id="${popupid}_form" action="none">
		<div class="panel-body">
			<div class="row search_area">
				<div class="col col-sm-3">
					<fm-select id="${popupid}_vm_keyword_field" emptystr="<spring:message code="label_field" text=""/>" :options="{VM_NM:'VM', INS_ID_NM:'<spring:message code="NC_VRA_CATALOGREQ_USER_NAME" text="" />', PRIVATE_IP:'IP'}" ></fm-select>
				</div>
				<div class="col col-sm-6">
					<fm-input id="${popupid}_vm_keyword" name="keyword" placeholder="<spring:message code="label_keyword" text=""/>" onkeyup="if(event.keyCode == 13) ${popupid}_vm_search();"></fm-input>
					<input type="text" name="DUMMY" style="display:none"/>
				</div>
				<div class="col col-sm-3">
					<input type="button" onclick="${popupid}_vm_search()" class="btn searchBtn" value="<spring:message code="btn_search" text="검색"/>" style="position: absolute; top: -17px;" />
				</div>
			</div>
		</div>
		<div id="${popupid}_vmSearchGrid" style="height:250px" class="ag-theme-fresh"></div>
	</form>

	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_parent_vue = null;
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});

		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			${popupid}_vm_search(param);
			return true;
		}

		function ${popupid}_vm_search(param){
			${popupid}_param = {
				keyword_field: $('#${popupid}_vm_keyword_field').val()
			}

			if(typeof param == 'object'){
				${popupid}_param.DC_ID = param.DC_ID;
				${popupid}_param.keyword = param.VM_NM;
			} else {
				${popupid}_param.keyword = param ? param: $('#${popupid}_vm_keyword').val()
			}
			${popupid}_param.isActiveOnly = "${isActiveOnly}";
			${popupid}_param.isOnlyOwn = "${isOnlyOwn}";
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_vmSearchReq.searchSub('/api/popup/VMSearch/list', ${popupid}_param, function(data) {
				${popupid}_vmSearchGrid.setData(data);
			});
		}

		var ${popupid}_vmSearchReq = new Req();
		var ${popupid}_vmSearchGrid;
		$(document).ready(function(){
			var ${popupid}_vmSearchGridColumnDefs =[
				{headerName: "<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>", field: "DC_NM", width: 120},
				{headerName: "<spring:message code="NC_VM_VM_NM" text="서버명"/>", field: "VM_NM", width: 160},
				{headerName: "IP", field: "PRIVATE_IP", width: 140},
				{headerName: "<spring:message code="NC_VRA_CATALOGREQ_USER_NAME" text="" />", field: "INS_ID_NM", width: 160}
			];
			var ${popupid}_vmSearchGridOptions = {
				columnDefs: ${popupid}_vmSearchGridColumnDefs,
				rowData: [],
				sizeColumnsToFit:true,
				enableSorting: true,
				rowSelection:'single',
			    enableColResize: true,
			    onRowDoubleClicked: function(){
					var arr = ${popupid}_vmSearchGrid.getSelectedRows();
					var callback = ${popupid}_parent_vue ? ${popupid}_parent_vue.callback: null;
					if(callback != null) eval( callback + '(arr[0], function(){ $(\'#${popupid}\').modal(\'hide\'); });');
			    }
			};
			${popupid}_vmSearchGrid = newGrid("${popupid}_vmSearchGrid", ${popupid}_vmSearchGridOptions);
		});
	</script>
</fm-modal>