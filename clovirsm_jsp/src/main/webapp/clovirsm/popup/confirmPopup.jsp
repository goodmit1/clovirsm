<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>
#vmDelCheckList_popup .modal-body{
	min-height: 300px;
}

}
</style>

<fm-modal id="${popupid}" title='<spring:message code="label_req_complete" text=""/>' cmd="header-title">
	<div class="form-panel detail-panel">
		<div id="stepFinish" class="stepFinish" style="padding-top: 70px;">
			<div><spring:message code="REQ_MSG_M1" text=""/></div>
			<div><spring:message code="REQ_MSG_M2" text=""/></div>
			<div><spring:message code="REQ_MSG_M5" text=""/></div>
			<div><spring:message code="REQ_MSG_M6" text=""/></div>
			<div class="btn_group">
				<fm-sbutton  class="exeBtn popupBtn finish stepBtn" cmd="search" onclick="endModal()"><spring:message code="FINISH_NEXT_PAGE" text=""/></fm-sbutton>
			</div>
		</div>
	</div>
	<script>
		var count = 0;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready(function(){
		})
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		function endModal(){
			location.href="/clovirsm/workflow/requestItemsHistory/index.jsp";
			return true;
		}
	</script>
	<style>

	</style>
</fm-modal>