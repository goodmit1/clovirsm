<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<style>
.modal-dialog{
	width:1000px;
}
/* 
.modal-body > form > .ag-theme-fresh{
	height: 450px;
} */
/* form > div.panel-body > div > div.col.col-sm-6{
	margin-left: 16px;
} */ 
#vm_copy_popup_search_form_popup > div{
	width: 1440px;
}
#vm_update_popup_search_form_popup > div{
	width: 1440px;
}
#newVm_iframe_search_form_popup > div{
	width: 1440px;
}
#newVm_iframe_search_form_popup > div > div > div.modal-body{
	height: 750px;
}
#newVm_iframe_search_form_popup_form{
	height: 100%;
}
#newVm_iframe_search_form_popupSearchGrid{
	height: 95%
}
#newVm_iframe_search_form_popup_form > div.panel-body > div{
    margin: 0px 200px;
}
#newVm_iframe_search_form_popup_form > div.panel-body > div > div.col.col-sm-6{
	margin-left: 155px;
}

#vm_update_popup_search_form_popup_form > div.panel-body > div{
    margin: 0px 200px;
}
#vm_update_popup_search_form_popup_form > div.panel-body > div > div.col.col-sm-6{
	margin-left: 155px;
	padding-right: 0px;
}

#vm_copy_popup_search_form_popup_form > div.panel-body > div{
    margin: 0px 200px;
}
#vm_copy_popup_search_form_popup_form > div.panel-body > div > div.col.col-sm-6{
	margin-left: 155px;
	padding-right: 0px;
}
</style>
<fm-modal id="${popupid}" title="<spring:message code="title_nas_select" text="NAS 선택"/>" cmd="header-title">
	<span slot="footer" id="${popupid}_footer">
		<input type="button" class="btn saveBtn" value="<spring:message code="btn_select" text="선택" />" onclick="${popupid}_return();" />
	</span>
	<form id="${popupid}_form" action="none">
		<div class="panel-body">
			<div class="row search_area">
				<div class="col col-sm-6">
					<fm-input id="${popupid}_keyword" name="keyword" onkeyup="if(event.keyCode == 13) ${popupid}_search();"></fm-input>
					<input type="text" name="DUMMY" style="display:none"/>
				</div>
				<div class="col btn_group">
					<input type="button" onclick="${popupid}_search()" class="btn searchBtn" value="<spring:message code="btn_search" text="검색"/>" />
				</div>
			</div>
		</div>
		<div id="${popupid}SearchGrid"  class="ag-theme-fresh" style="height: 695px;"></div>
	</form>

	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_parent_vue = null;
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});

		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			${popupid}_search(param);
			return true;
		}

		function ${popupid}_search(param){
			${popupid}_param = {
				keyword: $('#${popupid}_keyword').val()
			}
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}SearchReq.searchSub('/api/popup/NAS_SEARCH/list', ${popupid}_param, function(data) {
				${popupid}SearchGrid.setData(data);
			});
		}

		var ${popupid}SearchReq = new Req();
		var ${popupid}SearchGrid;
		$(document).ready(function(){
			var ${popupid}SearchGridColumnDefs =[
                {headerName: "",checkboxSelection: true,width:40},
				{headerName: "<spring:message code="PATH" text="Path"/>", field: "PATH", width: 280},
				{headerName: "<spring:message code="FM_TEAM_TEAM_NM" text=""/>", field: "TEAM_NM", width: 120},
				/* {headerName: "<spring:message code="NC_VM_REQ_NM" text="요청자"/>", field: "USER_NAME", width: 140}, */
				{headerName : "<spring:message code="NC_NAS_VOLUME_DATE" text="볼륨생성일" />",field : "INS_TMS",width: 80,
		          	  valueGetter: function(params) {
				    	  	return formatDate(params.data.INS_TMS,'datetime')
				}},
				{headerName: "<spring:message code="NC_NAS_VOLUME_SIZE" text="볼륨용량"/>", field: "SIZE", width: 80},
				{headerName: "<spring:message code="label_note" text="비고"/>", field: "NAS_DESC", width: 100}
			];
			var ${popupid}SearchGridOptions = {
				columnDefs: ${popupid}SearchGridColumnDefs,
				rowData: [],
				sizeColumnsToFit:true,
				enableSorting: true,
				rowSelection:'multiple',
			    enableColResize: true
			};
			${popupid}SearchGrid = newGrid("${popupid}SearchGrid", ${popupid}SearchGridOptions);
		});

		function ${popupid}_return() {
			var arr = ${popupid}SearchGrid.getSelectedRows();
			var callback = ${popupid}_parent_vue ? ${popupid}_parent_vue.callback: null;
			if(callback != null) eval( callback + '(arr, function(){ $(\'#${popupid}\').modal(\'hide\'); });');
		}
	</script>
</fm-modal>