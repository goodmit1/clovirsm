<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>
#vmDelCheckList_popup .modal-body{
	min-height: 300px;
}
.checks {position: relative;}
.etrans{
	margin-bottom:10px;
}
.checks input[type="checkbox"] {  /* 실제 체크박스는 화면에서 숨김 */
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip:rect(0,0,0,0);
  border: 0
}
.checks input[type="checkbox"] + label {
  display: inline-block;
  position: relative;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}
.checks input[type="checkbox"] + label:before {  /* 가짜 체크박스 */
  content: ' ';
  display: inline-block;
  width: 21px;  /* 체크박스의 너비를 지정 */
  height: 21px;  /* 체크박스의 높이를 지정 */
  line-height: 21px; /* 세로정렬을 위해 높이값과 일치 */
  margin: -2px 8px 0 0;
  text-align: center; 
  vertical-align: middle;
  background: #fafafa;
  border: 1px solid #cacece;
  border-radius : 3px;
  box-shadow: 0px 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
}
.checks input[type="checkbox"] + label:active:before,
.checks input[type="checkbox"]:checked + label:active:before {
  box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}

.checks input[type="checkbox"]:checked + label:before {  /* 체크박스를 체크했을때 */ 
  content: '\2714';  /* 체크표시 유니코드 사용 */
  color: #99a1a7;
  text-shadow: 1px 1px #fff;
  background: #e9ecee;
  border-color: #adb8c0;
  box-shadow: 0px 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
}
.checks.etrans input[type="checkbox"] + label {
  padding-left: 30px;
}
.checks.etrans input[type="checkbox"] + label:before {
  position: absolute;
  left: 0;
  top: 0;
  margin-top: 0;
  opacity: .6;
  box-shadow: none;
  border-color: #6cc0e5;
  -webkit-transition: all .12s, border-color .08s;
  transition: all .12s, border-color .08s;
}

.checks.etrans input[type="checkbox"]:checked + label:before {
  position: absolute;
  content: "";
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity:1; 
  background: transparent;
  border-color:transparent #6cc0e5 #6cc0e5 transparent;
  border-top-color:transparent;
  border-left-color:transparent;
  -ms-transform:rotate(45deg);
  -webkit-transform:rotate(45deg);
  transform:rotate(0deg);
}

.checks.etrans input[type="checkbox"]:checked + label:before {
  /*content:"\2713";*/
  content: "\2714";
  top: 0;
  left: 0;
  width: 21px;
  line-height: 21px;
  color: #6cc0e5;
  text-align: center;
  border: 1px solid #6cc0e5;
}
#delVm:disabled{
  background-color: #de9696 !important;
}
</style>

<fm-modal id="${popupid}" title='<spring:message code="" text="JSON 수정"/>' cmd="header-title">
	<span slot="footer" id="${popupid}_footer">
		<button type="button" id="del" class="contentsBtn tabBtnImg del top5 hide"  onclick="jsonDel()"><spring:message code="" text="삭제"/></button>
		<button type="button" id="save" class="contentsBtn tabBtnImg save top5"  onclick="jsonSave()"><spring:message code="" text="저장"/></button>
	</span>
	<div class="form-panel detail-panel">
		<ul class="nav nav-tabs vmDetailTab">
			<li class="active">
				<a href="#tab1" data-toggle="tab"  onclick="chgTab(1)"><spring:message code="" text="값 추가/수정"/></a>
			</li>
			<li>
				<a href="#tab2" data-toggle="tab"  onclick="chgTab(2)"><spring:message code="" text="값 제거"/></a>
			</li>
		</ul>
		
		<div class="tab-content ">
			<div class="form-panel detail-panel tab-pane active" id="tab1" style="height: 340px;">
				<div>
					<div style="font-size: 20px; padding: 10px;">JSON</div>
					<fm-textarea id="DRV_JSON" name="DRV_JSON" sty="height: 120px; width:100%;"></fm-output>
				</div>
			</div>
			<div class="form-panel detail-panel tab-pane" id="tab2" style="height: 340px;">
				<div>
					<fm-sbutton class="contentsBtn tabBtnImg del" onclick="jsonRowRemove();" cmd="update"><spring:message code="btn_delete" text="" /></fm-sbutton>
					<fm-sbutton class="contentsBtn tabBtnImg save" onclick="jsonRowAdd();" cmd="update"><spring:message code="label_add" text="" /></fm-sbutton>
					<div id="jsonTable" class="ag-theme-fresh" style="height: 300px" ></div>
				</div>
			</div>
		</div>
		
	</div>
	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(function() {

			var jsonColumnDefs = [
				{headerName : "<spring:message code=" " text="Key" />",field : "key", width:200 ,editable : true}
				]
			var
			jsongridOptions = {
				editable:true,
				columnDefs : jsonColumnDefs,
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				rowSelection:'single',
				enableColResize : true,
				enableServerSideSorting: false
			}
			jsonTable = newGrid("jsonTable", jsongridOptions);

			
		});
		
		
		function ${popupid}_click(vue, param, callback){
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.callback = callback;
			 
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		
		function jsonSave(){
			var arr = ${popupid}_param;
			var saveArray = new Array();
			if($("#DRV_JSON").val() != null && $("#DRV_JSON").val() != ''){
				try{
					var inputJsonObejct = JSON.parse($("#DRV_JSON").text());	
				}
				catch{
					alert("json 포맷을 다시 설정해주세요");
					return false;
				}
				for(var i = 0 ; i < arr.length ; i++){
					var jsonObject = JSON.parse(arr[i].FIXED_JSON);
					
					for(var key in jsonObject){
						for(var key2 in inputJsonObejct){
							if(key == key2){
								jsonObject[key] = inputJsonObejct[key2];
							} else{
								jsonObject[key2] = inputJsonObejct[key2];
							}
						}
					}
					var saveObject = new Object();
					saveObject.FIXED_JSON = JSON.stringify(jsonObject);
					saveObject.IU = "U";
					saveObject.DRV_ID = arr[i].DRV_ID;
					saveArray.push(saveObject);
				}
				jsonUpdate(saveArray);
			}
		}
		function jsonDel(){
			var arr = ${popupid}_param;
			var jsonArr = jsonTable.getSelectedRows();
			
			var saveArray = new Array();
			jsonTable.stopEditing();
			if(jsonArr.length > 0){
				for(var i = 0 ; i < arr.length ; i++){
					var jsonObject = JSON.parse(arr[i].FIXED_JSON);
					
					for(var key in jsonObject){
						for(var z = 0 ; z < jsonArr.length; z++){
								if(key == jsonArr[z].key){
									delete jsonObject[key];
									
								} 
						}
					}
					var saveObject = new Object();
					saveObject.FIXED_JSON = JSON.stringify(jsonObject);
					saveObject.IU = "U";
					saveObject.DRV_ID = arr[i].DRV_ID;
					saveArray.push(saveObject);
				}
				
				jsonUpdate(saveArray);
			}
		}
		
		function jsonUpdate(param){
			 
			var jsonString = JSON.stringify(param);
			post("/api/vra_drv/save_multi",{"selected_json":jsonString},function(){
				alert("<spring:message code="saved" text="저장되었습니다"/>");
				search();
				$('#${popupid}').modal('hide');
			});
		}
		function jsonRowAdd(){
			 
			jsonTable.insertRow({});

		}
		function jsonRowRemove(){
			var arr = jsonTable.getSelectedRows();
			jsonTable.deleteRow(arr);

		}
		function chgTab(idx)
		{
			 
			if(idx == 1){
				$("#del").addClass("hide");
				$("#save").removeClass("hide");
			} else{
				$("#save").addClass("hide");
				$("#del").removeClass("hide");
			}
		}
	</script>
</fm-modal>