<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <div class="col col-sm-12">
		<fm-ibutton id="${popupid}_SPEC_NM" name="SPEC_NM" title="<spring:message code="NC_VM_SPEC_NM_REQ" text="요청 사양"/>" class="inline" required="true"  >
			<fm-popup-button popupid="${popupid}_spec_search_popup" popup="/clovirsm/popup/spec_search_form_popup.jsp" cmd="update" param="{}" callback="select_${popupid}_spec"><spring:message code="btn_spec_search" text="사양검색"/></fm-popup-button>
		</fm-ibutton>
		<fm-output id="${popupid}_NEW_SPEC_INFO" name="SPEC_INFO" :value="getSpecInfo(null,form_data.CPU_CNT, form_data.RAM_SIZE, form_data.OS_DISK_SIZE,'G')" class="inline white"  ></fm-output>
	</div>
	<div class="col col-sm-6">
		<fm-spin id="${popupid}_DISK_SIZE" input_style="width:200px;"  title="<spring:message code="add" text="추가" /> <spring:message code="NC_IMG_DISK_SIZE" text="디스크사이즈" />" name="DISK_SIZE" required="true"  onchange="${popupid}_validationDisk()" min="0"  >
			<fm-select id="${popupid}_DISK_UNIT" name="DISK_UNIT" class="inline" :options="diskUnitOptions" style="width:50px;display:inline-block;" onchange="${popupid}_validationDisk()"  ></fm-select>
			<fm-select id="${popupid}_DISK_TYPE_ID" style="width:120px;" disabled="true" name="DISK_TYPE_ID" class="inline" url="/api/code_list?grp=dblist.com.clovirsm.common.Component.selectDiskType"></fm-select>
		</fm-spin>
	</div>
	<div class="col col-sm-6">
	<fm-output title="<spring:message code="label_DD_FEE" text="비용"/>" id="${popupid}_DD_FEE" name="DD_FEE" :value="formatNumber(form_data.DD_FEE)" class="inline white"  ></fm-output>
	</div>
<script>
function ${popupid}_calcDiskFee(){
	var obj = ${popupid}_vue.form_data;
	var disk = getDiskSizeGB(Number(obj.DISK_SIZE),obj.DISK_UNIT) + Number(obj.OS_DISK_SIZE);
	getDayFee(obj.DC_ID,obj.CPU_CNT, obj.RAM_SIZE, obj.DISK_TYPE_ID, disk, 'G', obj.SPEC_ID, '${popupid}_DD_FEE')
	${popupid}_vue.form_data.SPEC_INFO = getSimpleSpecInfo(obj);
}
 
function ${popupid}_click_after() {
	${popupid}_calcDiskFee();
}


function ${popupid}_validationDisk(obj){
	if(!${popupid}_vue) {
		return true;
	}
	var extendDiskSizeGB = getDiskSizeGB(${popupid}_vue.form_data.DISK_SIZE,${popupid}_vue.form_data.DISK_UNIT);
	if(extendDiskSizeGB < 0) {
		alert('<spring:message code="msg_disk_size_re_setting"/>');
		return false;
	}
	${popupid}_calcDiskFee();
	return true;
}



function ${popupid}_makeDataDiskParam(param){
	var arr = [];
	
	var extendDiskSizeGB = getDiskSizeGB(${popupid}_vue.form_data.DISK_SIZE,${popupid}_vue.form_data.DISK_UNIT);
	if(extendDiskSizeGB > 0) {
		var flag = true;
		for(var i=0; i<arr.length; i++){
			if(arr[i].CUD_CD == 'C') {
				flag = false;
				arr[i].DISK_SIZE = extendDiskSizeGB;
			}
		}
		if(flag) {
			var o = {DISK_TYPE_ID:${popupid}_vue.form_data.DISK_TYPE_ID, DISK_SIZE:extendDiskSizeGB, DISK_UNIT:'G'};
			 
			
			arr.push(o);
		}
	}
	param.NC_VM_DATA_DISK = JSON.stringify(arr);
	 
}
</script>