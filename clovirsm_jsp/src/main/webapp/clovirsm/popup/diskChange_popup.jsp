<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>
	#vmDelCheckList_popup .modal-body{
		min-height: 300px;
	}
	.menu-layout-3 .form-control {
		width: 89px;
		height: 26px;
	}
</style>

<fm-modal id="${popupid}" title='<spring:message code="NEW_VM_TITLE_3" text="자원 변경"/>' cmd="header-title">
	<span slot="footer">
		<button type="button" class="popupBtn finish top5" id="request" onclick="${popupid}_to_work()"><spring:message code="request" text="요청"/></button>
	</span>
	<div class="form-panel detail-panel">
		<div class="panel-body">
			<fmtags:include page="/clovirsm/popup/_spec_form.jsp" />
		</div>
	</div>
	<script>

		var ${popupid}_vue = makePopupVue("${popupid}", "${popupid}", {
			DISK_SIZE : 0,
			DISK_UNIT : 'G'
		})

		var ${popupid}_param = {CPU_CNT : 0 , RAM_SIZE : 0};
		function ${popupid}_click(vue, param, callback){

			${popupid}_parent_vue = vue;
			${popupid}_vue.callback = callback;
			if(param != null){
				${popupid}_param = param;
				${popupid}_vue.form_data = param;
				${popupid}_click_after();
				${popupid}_chgCPURAM();
			} else{
				${popupid}_param = {};
				alert(msg_select_first);
				return;
			}
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}

		var ${popupid}_CPUOptions = ${popupid}_makeOptions(32, '', 'core');
		var ${popupid}_RAMOptions = ${popupid}_makeOptions(64, '', 'GB');

		function ${popupid}_makeOptions(maxSize, title_prefix, title_postfix){
			title_prefix = title_prefix?title_prefix: '';
			title_postfix = title_postfix?title_postfix: '';
			var ramList = {};
			for(var i = 1; i <= maxSize; i++){
				eval('ramList['+i+']= title_prefix+i+title_postfix;');
			}
			return ramList;
		}

		function ${popupid}_to_work(){
			if(!${popupid}_validationSpecChange()) return;
			action = 'updateReq';


			${popupid}_vue.form_data.DEPLOY_REQ_YN = "Y";
			post('/api/vm/' + action, ${popupid}_vue.form_data,  function(data){
				if(${popupid}_vue.form_data.DISK_UNIT == 'T') ${popupid}_vue.form_data.DISK_SIZE *= 1024;
				${popupid}_tuingSpecInfo();
				$('#${popupid}').modal('hide');
				FMAlert("<img src='/res/img/hynix/confirm.png' style='margin-bottom:20px;margin-top: 20px;'><div style='font-size: 16px; font-weight: 900; margin-bottom:20px;'><spring:message code="REQ_MSG_M1" text=""/></div><div><spring:message code="REQ_MSG_M2" text=""/></div><div><spring:message code="REQ_MSG_M5" text=""/></div><div><spring:message code="REQ_MSG_M6" text=""/></div>",'요청 완료','신청이력페이지로 이동', function(){location.href="/clovirsm/workflow/requestItemsHistory/index.jsp"; } );

				if(${popupid}_vue.callback) {
					eval(${popupid}_vue.callback + '(${popupid}_vue.form_data);');
				}

			});
		}

		function ${popupid}_tuingSpecInfo(){
			var data = ${popupid}_vue.form_data;
			data.SPEC_INFO = getSpecInfo(data.SPEC_NM, data.CPU_CNT, data.RAM_SIZE, data.DISK_SIZE, data.DISK_UNIT, data.DISK_TYPE_NM);
		}

		function ${popupid}_validationSpecChange(){



			if(! ${popupid}_spec_validate()) return;

			try
			{
				${popupid}_makeDataDiskParam(${popupid}_vue.form_data);
			}
			catch(e)
			{

			}
			return true;
		}
		function endModal(){
			location.href="/clovirsm/workflow/requestItemsHistory/index.jsp";
			return true;
		}
		initSpecData();

	</script>
</fm-modal>