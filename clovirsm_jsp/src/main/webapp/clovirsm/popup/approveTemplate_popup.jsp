<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<style>

#${popupid} .modal-dialog{
	width : 800px;
}
#${popupid} .modal-dialog .form-panel.detail-panel{
	height : 500px;
}
/* #reserve_popup .value-title{ */
/*     background-color: white; */
/* } */
</style>

<fm-modal id="${popupid}" title='<spring:message code="btn_task_yes" text="작업완료"/>' cmd="header-title">
	<span slot="footer">
		
		<button type="button" class="popupBtn next top5" onclick="approveTemplateBtn()"><spring:message code="btn_task_yes" text="작업 완료"/></button>
	</span>
	<div class="form-panel detail-panel" style="height: 200px;">
		<div class="col col-sm-12" style="text-align: left;">
			<fm-select id="S_CATALOG_NM" name="CATALOG_ID" onchange="changeCatalog();" title="<spring:message code='CRATE_TEMPLATE' text='생성 템플릿' />"></fm-select>
		</div>
		<div class="col col-sm-6" style="text-align: left;">
			<fm-input id="S_FEE" name="FEE" title="<spring:message code="VRA_CREATE_INFO" text="초기 구성 서비스 비용(원) " />" ></fm-input>
		</div>
		<div class="col col-sm-6" style="text-align: left;">
			<fm-input id="S_DEPLOY_TIME" name="DEPLOY_TIME" title="<spring:message code="DEPLOY_TIME" text="소요시간" />" ></fm-input>
		</div>
		<div class="col col-sm-6" style="text-align: left;">
			<fm-select-yn id="S_ALL_USE_YN" name="ALL_USE_YN" title="<spring:message code="NC_VRA_CATALOG_ALL_USE_YN" text="전체 사용 여부 " />"></fm-select>
		</div>
		<div class="col col-sm-12" id="TEAM_sel" v-if="form_data.ALL_USE_YN=='N'">
				<fm-select2 id="CATEGORY_IDS" keyfield="CATEGORY_ID" titlefield="CATEGORY_NMS"  name="CATEGORY_IDS"  url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listLeafCategory" multiple="true"    title="<spring:message code="TASK" text="업무"/>"></fm-select2>
		</div>
	</div>
	<script>
		var apprReq = new Req();
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function ${popupid}_click(vue, param){
			 
			${popupid}_parent_vue = vue;
			if(param != null){
				if(param.TASK_STATUS_CD == 'S'){
					alert("작업전인 템플릿만 가능합니다.");
					return false;
				}
				${popupid}_param = param;
				putCatalog();
			} else{
				${popupid}_param = {};
				alert(msg_select_first);
				return false;
			}
		 
			return true;
		}
		
		function putCatalog(){
			post('/api/vra_catalog_mng/collect', null, function(){
				post("/api/vra_catalog/list/list_NC_VRA_CATALOG/",null,function(data){
					fillOptionByData("S_CATALOG_NM", data , '', 'CATALOG_ID', 'CATALOG_NM')
				});
			});
		}
		
		function changeCatalog(){
			
		 	var data = $("#S_CATALOG_NM option:selected")[0].data;
		 	${popupid}_vue.form_data = data 				 
							if(data.CATEGORY_IDS && data.CATEGORY_IDS != ''){
								 
								${popupid}_vue.form_data.CATEGORY_IDS = data.CATEGORY_IDS.split(",");
								$("#CATEGORY_IDS").val(${popupid}_vue.form_data.CATEGORY_IDS)
							}
							else{
								${popupid}_vue.form_data.CATEGORY_IDS=[];
								$("#CATEGORY_IDS").val([]);
							}
						 
						 
			 
			
			//${popupid}_vue.form_data = $("#S_CATALOG_NM option:selected")[0].data;
		}
		function approveTemplateBtn(){
			approveTemplate_popup_vue.form_data.CATEGORY_IDS = $("#CATEGORY_IDS").val()
			approveTemplate_popup_vue.form_data.TASK_STATUS_CD = 'S' ;
			approveTemplate_popup_vue.form_data.TEMPLATE_ID = ${popupid}_param.TEMPLATE_ID
			post('/api/template_req/save', approveTemplate_popup_vue.form_data, function(){
				search();
				$('#${popupid}').modal('hide');
			})
		}
		
	</script>
	<style>

	</style>
</fm-modal>