<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmtags" uri="http://fliconz.kr/jsp/tlds/fmtags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html; charset=UTF-8" %>

<script src="/clovirsm/popup/vraInputJs.jsp"></script>

<div class="col col-sm-12">
	<div class="subTitle">PreSet JSON</div>
	<select id="field_select" multiple="multiple"  style="width:100%;"  ></select>
</div>
<div class="col col-sm-7" style="height:300px;overflow: auto; ">
	<div class="step  " id="step2" data-step-name="ADMIN">
	</div>
</div>
<div class="col col-sm-1" style="text-align:center;height:300px">
	<input type="button" value=">" onclick="makeJSON()">
	<br>
	<input type="button" value="<" onclick="makeForm()">
</div>
<div class="col col-sm-4" style="height:300px">
	<textarea id="FIXED_JSON" name="FIXED_JSON" style="width:100%; height:100%" :value="${popupid}_param.FIXED_JSON"></textarea>
</div>
<script>


	function addField(data){
		if(data.name=="_purpose"){
			data.api_url = '/api/code_list?grp=PURPOSE'
		}
		else if(data.name=="_fab"){
			data.api_url = '/api/code_list?grp=FAB'
		}
		else if(data.name=="_p_kubun"){
			data.api_url = '/api/code_list?grp=P_KUBUN'
		}
		contents.push( data);
		displayApp(0, [data]);
		setTimeout(function(){
			select2Orders.onShow();
		}, 300);
	}
	var old_CATALOG_ID = null;;
	function getFieldList(){

		var param = {};

		param.CATALOG_ID=$("#CATALOG_ID").val();
		contents = [];
		if(param.CATALOG_ID=='' || param.CATALOG_ID ==old_CATALOG_ID) return;
		old_CATALOG_ID = param.CATALOG_ID;
		post('/api/vra_drv/list_fields', param, function(data){
			$("#step2").html("");
			var allFields = data ; //[{"type":"string","title":"OS Image","name":"vm_image","fieldName":"image"},{"type":"array","title":"application","name":"application","fieldName":"application"},{"type":"include","title":"OS Param","name":"os_param","fieldName":"os_param"},{"type":"include","title":"Disk","name":"multi_disk","fieldName":"multi_disk"},{"type":"integer","maximum":4,"title":"CPU","default":2,"name":"cpu","fieldName":"cpuCount"},{"type":"integer","maximum":16,"default":4,"title":"Memory(GB)","name":"memory","fieldName":"totalMemoryMB"},{"type":"string","encrypted":true,"default":"password","title":"Login Password","name":"password","fieldName":"cloudConfig"}];

			fillOptionByData("field_select", allFields, "", "name", "title")
			if($("#FIXED_JSON").val() != ''){
				makeForm();
			}

		})
	}
	function initFixed(data){
		console.log(data);
		$("#FIXED_JSON").val(data.FIXED_JSON);
		getAllSwEnv();
		select2Orders.make("field_select",   'Preset field');
		select2Orders.onShow();
		$("#field_select").on("select2:select",function(e){
			var data = e.params.data.element.data;
			addField(data);
		});
		$("#field_select").on("select2:unselect",function(e){
			var data = e.params.data.element.data;
			for(var i=0; i < contents.length; i++){
				if(data.name==contents[i].name){
					$('.' + data.name + '-area').remove();
					contents.splice(i,1)

					break;
				}
			}


		});
		$("#CATALOG_ID").change(function(){
			getFieldList();
		})

		//getFieldList();
	}
	function makeForm(){
		var json = {};
		if($("#FIXED_JSON").val().trim() != ''){
			json = JSON.parse($("#FIXED_JSON").val());
			${popupid}_param.DATA_JSON = json;
		}

		$("#step2").html("");
		//var keys = Object.keys(json);
		var options = $("#field_select option");
		var vals = [];
		var reloadTarget = {};
		for(var i=1; i < options.length; i++){
			if((options[i].data.name in json)){
				options[i].selected=true;

				addField(options[i].data)

				if(options[i].data.fieldName=="application"){
					reloadTarget[ options[i].data.name ]=json[options[i].data.name];
					var arr = json["_" + options[i].data.name + "_param"];
					if(arr){
						for(var j=0; j < arr.length; j++){
							var arr1 = arr[j].split("=");
							reloadTarget[ arr1[0]  ]=arr1[1];
						}
					}


				}
				if(options[i].data.type != "include"){
					reloadTarget[ options[i].data.name ]=json[options[i].data.name];
				}

			}
		}


		setVal4Select(reloadTarget)

	}
	var selValCnt = 0;
	function setVal4Select(json){
		if(selValCnt==5) return;
		selValCnt++;
		setTimeout(function(){
			var newJson = {};
			var keys = Object.keys(json);
			for(var i=0; i < keys.length; i++){



				if($("#" + keys[i]).length==0 || ($("#" + keys[i])[0].tagName=="SELECT" && $("#" + keys[i]   + " option").length==0)){

					newJson[keys[i]] = json[keys[i]];

				}
				else{
					$("#" + keys[i]  ).val(json[keys[i]])
					$("#" + keys[i]  ).trigger("change");

				}
			}
			if(Object.keys(newJson).length>1){

				setVal4Select(newJson);
			}
		},500)
	}
	function getKubunVal(){
		return null;
	}
	function dataJSONSet(name){

	}
	function putDefaultDataJSON(  content){

	}
	function makeJSON(){
		var json = {};
		if($("#FIXED_JSON").val().trim() != ''){
			json = JSON.parse($("#FIXED_JSON").val());
		}

		for(var i=0; i < contents.length; i++){
			var name = contents[i].name;
			var val = $("#" + name).val();
			if(select2Orders.has(name)){
				val =select2Orders.get( name).val();
			}
			if(contents[i].fieldName=='application'){

				var envname = "_"+ name +"_param";
				json[envname] = putEnvDataJSON( "ADMIN");
			}
			if(contents[i].type=="include"){
				try{
					val = eval("get_" + name +  "()");
				}
				catch(e){
					alert(e.message);
					return;
				}
			}
			if(val)
				json[name] = val;
			else
				delete json[name];
		}

		$("#FIXED_JSON").val(JSON.stringify(json))
	}
</script>
</form>

