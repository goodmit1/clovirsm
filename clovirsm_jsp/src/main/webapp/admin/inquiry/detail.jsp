<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>

<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body" id="input_area" style="margin-left: 0;">
			<div id="INQUIRY_N">
				<div class="col col-sm-4">
					<fm-output id="INQ_TITLE" name="INQ_TITLE" disabled="true" ostyle="min-height: auto;" title="<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>" tooltip="INQ_TITLE"></fm-output>
				</div>
				<div class="col col-sm-4">
					<fm-output id="INQ_CD_NM" name="INQ_CD_NM" disabled="true" title="<spring:message code="NC_INQ_INQ" text="문의유형코드"/>"></fm-output>
				</div>
				<div class="col col-sm-4">
					<fm-output id="SVC_CD_NM" name="SVC_CD_NM" disabled="true" title="<spring:message code="NC_INQ_SVC" text="서비스"/>"></fm-output>
				</div>
				<div class="col col-sm-12" style="height: 251px;">
					<div>
						<label class="control-label grid-title value-title inquiry"><spring:message code="NC_INQ_INQUIRY" text="문의" /></label>
						<div style="display: inline-block;" v-html="form_data.INQUIRY"> </div>
					</div>
				</div>
			</div>
			<div id="INQUIRY_Y">
				<div class="col col-sm-4">
					<fm-input id="INQ_TITLE" name="INQ_TITLE" required="true" title="<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>"></fm-input>
				</div>
				<div class="col col-sm-4">
					<fm-select url="/api/code_list?grp=INQ" id="INQ_CD"
						name="INQ_CD" title="<spring:message code="NC_INQ_INQ" text="문의유형코드"/>"></fm-select>
				</div>
				<div class="col col-sm-4">
					<fm-select url="/api/code_list?grp=INQ_SVC" id="SVC_CD"
						name="SVC_CD" title="<spring:message code="NC_INQ_SVC" text="서비스"/>"></fm-select>
				</div>
				<div class="col col-sm-12" style="height: 349px;">
					<fm-textarea id="INQUIRY" name="INQUIRY" required="true" title="<spring:message code="NC_INQ_INQUIRY" text="문의" />"></fm-textarea>
				</div>
			</div>
			<div id="ANSWER_N">
				<jsp:include page="mngDetail.jsp"></jsp:include>
			</div>
			<div id="ANSWER_Y">
				<div class="col col-sm-6">
					<fm-output id="UPD_NAME" name="UPD_NAME" title="<spring:message code="NC_INQ_UPD_NAME" text="답변자"/>"></fm-output>
				</div>
				<div class="col col-sm-6">
					<fm-output id="UPD_TMS" :value="formatDate(form_data.UPD_TMS,'datetime')" name="UPD_TMS" title="<spring:message code="NC_INQ_UPD_TMS" text="답변일시"/>"></fm-output>
				</div>
				<div class="col col-sm-12" style="height: 349px; display: flex;">
					<label class="control-label grid-title value-title inquiry" style="height: 100%; font-weight: bold;"><spring:message code="NC_INQ_ANSWER" text="답변" /></label>
					<div style="display: inline-block;" v-html="form_data.ANSWER"></div>
				</div>
			</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$("#INQUIRY").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false,'source':false});
	})

	function inquiryYNCheck(data) {
		answerCheck(data);
		authorCheck(data);
		inquiryCheck(data);
	}

	function answerCheck(data) {
		if(data.ANSWER !== undefined){
			$('#ANSWER_Y').show();
		}else {
			$('#ANSWER_Y').hide();
		}
	}

	function inquiryCheck(data) {
		if(data.INS_ID === _USER_ID_){
			$('#INQUIRY_Y').show();
			$('#INQUIRY_N').hide();
		}else{
			$('#INQUIRY_Y').hide();
			$('#INQUIRY_N').show();
		}
	}

	function authorCheck(data){
		if(ADMIN_YN === 'Y'){
			$('#ANSWER_Y').hide();
			$('#ANSWER_N').show();
		}
	}

	function newInquiry() {
		$('#ANSWER_N').hide();
		$('#ANSWER_Y').hide();
		$('#INQUIRY_N').hide();
		$('#INQUIRY_Y').show();
	}
</script>