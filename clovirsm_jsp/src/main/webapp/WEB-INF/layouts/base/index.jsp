<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<jsp:include page="_common.jsp" />
	<body class="menu-layout-3">
		<layout:block name="header">
			<fmtags:include page="top.jsp" />
		</layout:block>
		<div class="content-body">
			<layout:block name="menu">
				<fmtags:include page="menu.jsp" />
			</layout:block>
			<div class="main">
				<layout:block name="navi">
					<fmtags:include page="navi.jsp" />
				</layout:block>
				<div id="content" class="container-fluid">
					<layout:block name="content"></layout:block>
				</div>
				<div id="popup_area" class="popup_area">
					<layout:block name="popup_area"></layout:block>
				</div>
			</div>
		</div>
		<div class="mainBottom">
			<layout:block name="bottom">
				<fmtags:include page="bottom.jsp" />
			</layout:block>
		</div>
	 	<div id="loading_dialog" class="loading_dialog">
	 		<div id="loading_dialog_bg" class="loading_dialog_bg">
				<img src="/res/img/loading.gif" />
			</div>
	</body>
</html>