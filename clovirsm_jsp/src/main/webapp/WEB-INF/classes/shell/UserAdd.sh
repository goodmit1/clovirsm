#!/bin/sh
sudo /usr/sbin/useradd -d /home/${USER_NAME} -m -s /bin/bash -k /etc/skel ${USER_NAME}
#echo "${USER_NAME}:${USER_PASSWORD} | sudo chpasswd
#sudo chage -d 0 ${USER_NAME}
sudo mkdir /home/${USER_NAME}/.ssh
sudo touch /home/${USER_NAME}/.ssh/authorized_keys
sudo chmod 700 /home/${USER_NAME}/.ssh
sudo chmod 600 /home/${USER_NAME}/.ssh/authorized_keys
sudo chown -R ${USER_NAME}:${USER_NAME} /home/${USER_NAME}/.ssh
echo "${PUBLIC_KEY}" | sudo tee -a /home/${USER_NAME}/.ssh/authorized_keys /dev/null
