package com.clovirsm_openapi.dto;

import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.obj.VMInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "VM 정보", description = "VM 생성 요청시 필요한 정보 Class")
public class VMDTO {
    @ApiModelProperty(value = "vCenter 아이디")
    @NonNull
    public String DC_ID;

    public String DC_NM;
    public String VM_NM;
    public int CPU_CNT;

    //GB
    public int RAM_SIZE;

    //GB
    public int DISK_SIZE;
    public String IMAGE;
    public String CLUSTER;
    public String ADM_PWD;
    public IPInfo IP_INFO;
    public String DS_NM;

    public String POWER_ON_YN = "N";

    public VMInfo toMap(){
        VMInfo map = new VMInfo();
        map.setREAL_DC_NM(DC_NM);
        map.setVM_NM(  VM_NM);
        map.setDATASTORE_NM_LIST(  new String[]{ DS_NM }); //GB
        map.setCPU(  CPU_CNT);
        map.setCLUSTER( CLUSTER);
        map.setRAM_SIZE(  RAM_SIZE); //GB
        map.setDISK_SIZE(  DISK_SIZE); //GB
        map.setFROM( IMAGE); //GB
        map.setPOWER_ON( "Y".equals(POWER_ON_YN)); //GB
        List ipList = new ArrayList();
        ipList.add(IP_INFO);
        map.setIP_LIST( ipList);

        map.setOS_ADMIN_PWD(  ADM_PWD);
        return map;
    }
}
