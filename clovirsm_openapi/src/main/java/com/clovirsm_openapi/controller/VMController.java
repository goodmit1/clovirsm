package com.clovirsm_openapi.controller;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm_openapi.config.ConnHelper;
import com.clovirsm_openapi.dto.VMDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/vm")
public class VMController {
    private final ConnHelper connHelper;

    public VMController(ConnHelper connProperties) {
        this.connHelper = connProperties;
    }
    @RequestMapping(value = "/{DC_ID}/{VM_NM}", method = RequestMethod.GET)
    public Object getVM(@PathVariable String DC_ID, @PathVariable String VM_NM, HttpServletResponse response) throws Exception {
        try{
            Map param = new HashMap();

            param.put("ALL_YN","Y");
            HypervisorAPI api = connHelper.getConnInfo(DC_ID, param);

            VMInfo vm = new VMInfo();
            vm.setVM_NM( VM_NM) ;
            Map result = api.vmState(new HVParam(param, vm, null, null));
            result.put("result", "success");
            return result;
        }
        catch(Exception e){
            e.printStackTrace();
            Map result = new HashMap();
            result.put("result", "fail");
            result.put("errMsg", e.getMessage());
            return new ResponseEntity<Object>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @RequestMapping(value = "/{DC_ID}/task/{TASK_ID}", method = RequestMethod.GET)
    public Object getTaskInfo(@PathVariable String DC_ID, @PathVariable String TASK_ID, HttpServletResponse response) throws Exception {
        try{
            Map param = new HashMap();

            HypervisorAPI api = connHelper.getConnInfo(DC_ID, param);
            boolean isFinish = api.chkTask(new HVParam(param ), TASK_ID);
            Map result = new HashMap();
            if(isFinish){
                result.put("result","complete");
            }
            else{
                result.put("result","running");
            }
            return result;
        }
        catch(Exception e){
            e.printStackTrace();
            Map result = new HashMap();
            result.put("result", "fail");
            result.put("errMsg", e.getMessage());
            return new ResponseEntity<Object>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @RequestMapping(value = "/{DC_ID}/{VM_NM}", method = RequestMethod.DELETE)
    public Object deleteVM(@PathVariable String DC_ID, @PathVariable String VM_NM, HttpServletResponse response) throws Exception {
        try{
            Map param = new HashMap();
            HypervisorAPI api = connHelper.getConnInfo(DC_ID, param);
            VMInfo vm = new VMInfo();
            vm.setVM_NM( VM_NM ) ;
            Map result = api.deleteVM(new HVParam(param, vm, null, null));
            result.put("result", "success");
            return result;
        }
        catch(Exception e){
            e.printStackTrace();
            Map result = new HashMap();
            result.put("result", "fail");
            result.put("errMsg", e.getMessage());
            return new ResponseEntity<Object>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    public Object makeVM(@RequestBody VMDTO vmVo)  {
        try {
            VMInfo vmInfo = vmVo.toMap();
            Map connMap = new HashMap();
            HypervisorAPI api = connHelper.getConnInfo(vmVo.DC_ID, connMap);



            Map result = new HashMap();
            result = api.createVM(new HVParam(connMap, vmInfo, null, null), true);

            result.put("result", "success");
            return result;
        }
        catch(Exception e){
            e.printStackTrace();
            Map result = new HashMap();
            result.put("result", "fail");
            result.put("errMsg", e.getMessage());
            return new ResponseEntity<Object>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
