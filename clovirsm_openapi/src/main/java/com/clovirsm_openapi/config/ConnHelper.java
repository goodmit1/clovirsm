package com.clovirsm_openapi.config;

import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IConnection;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource("classpath:connection.properties") 		// classpath는 src/main/resource/ 입니다.
public class ConnHelper {

    private final Environment environment;		// 빈 주입을 받습니다.

    public ConnHelper(Environment environment) {
        this.environment = environment;
    }

    public HypervisorAPI getConnInfo(String key, Map param) throws  Exception {
        Map map = new HashMap();

        map.put(HypervisorAPI.PARAM_URL, environment.getProperty(key + ".url" ));
        map.put(HypervisorAPI.PARAM_USERID, environment.getProperty(key + ".userid") );
        map.put(HypervisorAPI.PARAM_PWD, environment.getProperty(key + ".pwd" ));
        map.put("windows_csi", environment.getProperty(key + ".windows_csi" ));
        map.put("linux_csi", environment.getProperty(key + ".linux_csi" ));
        map.put("INIT_POOL_SIZE", environment.getProperty(key + ".linux_csi" ));
        map.put("linux_csi", environment.getProperty(key + ".linux_csi" ));
        map.put("sso", environment.getProperty(key + ".sso" ));
        param.putAll( map);
        return (HypervisorAPI) Class.forName( environment.getProperty(environment.getProperty(key + ".type" ) + ".className")).newInstance();
    }



}
