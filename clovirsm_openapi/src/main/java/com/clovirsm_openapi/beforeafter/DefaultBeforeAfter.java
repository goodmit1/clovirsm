package com.clovirsm_openapi.beforeafter;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.obj.VMInfo;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class DefaultBeforeAfter implements IBeforeAfter {
    @Override
    public List<IPInfo> getNextIp(int firstNicId, VMInfo param) throws Exception {
        return null;
    }

    @Override
    public boolean onBeforeCreateVM(Map param) throws Exception {
        return false;
    }

    @Override
    public void onChangeDCInfo(Map param) throws Exception {

    }

    @Override
    public Runnable getAfterCreateThread(String kubun, Map param) throws Exception {
        return null;
    }

    @Override
    public String getVMName(Map vm) throws Exception {
        return null;
    }

    @Override
    public String[] getVMNames(Map vm, int count) throws Exception {
        return new String[0];
    }

    @Override
    public String getImgName(Map param) throws Exception {
        return null;
    }

    @Override
    public boolean onAfterGetVM(Map result) throws Exception {
        return false;
    }

    @Override
    public int chgNaming(Object categoryId, String naming) throws Exception {
        return 0;
    }

    @Override
    public void onAfterFWDeploy(Map info) throws Exception {

    }

    @Override
    public void onAfterDeleteVM(Map param) {

    }

    @Override
    public boolean reconfigVM(String pkVal, String instDt, Map param) throws Exception {
        return false;
    }

    @Override
    public boolean deleteVM(String pkVal, String instDt, Map param) throws Exception {
        return false;
    }

    @Override
    public String getFolderName(Map param) throws Exception {
        return null;
    }

    @Override
    public void putVraDataJson(JSONObject dataJSON, org.codehaus.jettison.json.JSONObject formJSON, Map param) throws Exception {

    }
    OnAfterVMCreate afterVM;
    @Override
    public IAfterProcess onAfterProcess(String s, String s1, Map param) throws Exception {
        if(afterVM == null){
            afterVM = new OnAfterVMCreate(param);
        }
        return afterVM;
    }

    @Override
    public boolean expire(String s, String s1, boolean b) throws Exception {
        return false;
    }

    @Override
    public void deleteAfterExpire(String s, String s1) throws Exception {

    }

    @Override
    public void reuse(String s, String s1, int i, Date date) throws Exception {

    }

    @Override
    public void onAfterCollect(String s, Map map) throws Exception {

    }
}
