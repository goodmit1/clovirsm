package com.clovirsm_openapi.beforeafter;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;

import java.util.Map;

public class OnAfterVMCreate implements IAfterProcess {

    String taskId;
    Map param ;
    boolean isSuccess = false;
    String errMsg = null;

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    boolean isFinish = false;
    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
        isFinish = true;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }


    public OnAfterVMCreate(Map param){
        this.param = param;
    }
    @Override
    public void onAfterSuccess(String s) throws Exception {
        this.isSuccess = true;
    }

    @Override
    public Map getParam() {
        return param;
    }

    @Override
    public void chkState() {
        try {
            if (this.getState()) {
                try {
                    this.onAfterSuccess(this.taskId);
                } catch (Exception e) {

                    try {
                        e.printStackTrace();
                        this.onAfterFail(this.taskId, e);
                    } catch (Exception var4) {
                        var4.printStackTrace();
                    }
                }


            }
        } catch (Exception e) {


            try {
                e.printStackTrace();
                this.onAfterFail(this.taskId, e);

            } catch (Exception var3) {
                var3.printStackTrace();
            }
        }
    }
    public boolean getState() throws Exception {
        if(taskId == null){
            throw new Exception("TASK ID IS NULL");
        }
        VMWareAPI api = new VMWareAPI();



        return api.chkTask(new HVParam(param), taskId);


    }

    @Override
    public void setTask(String s) {
        this.taskId = s;
    }

    @Override
    public void startTask(String s) {
        setTask(s);
    }

    @Override
    public void onAfterFail(String s, Throwable throwable) throws Exception {
        this.isSuccess = false;
        this.isFinish = true;
        this.errMsg = throwable.getMessage();
    }
}
