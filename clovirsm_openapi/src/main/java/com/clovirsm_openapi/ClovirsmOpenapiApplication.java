package com.clovirsm_openapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClovirsmOpenapiApplication {

	public static void main(String[] args) {

		SpringApplication.run(ClovirsmOpenapiApplication.class, args);

	}


}
