package com.clovirsm_openapi;

import com.clovirsm.hv.RestClient;
import org.json.JSONObject;
import org.junit.Test;

public class VMAPITest {
    @Test
    public void makeVM() throws Exception {
        RestClient client = new RestClient("http://localhost:8080");
        client.setMimeType("application/json");
        JSONObject json = new JSONObject();
        json.put("DC_ID","test");
        json.put("DC_NM","goodcs");
        json.put("VM_NM","syk11");
        json.put("CPU_CNT",4);
        json.put("RAM_SIZE",4);
        json.put("DISK_SIZE",10);
        json.put("IMAGE","goodcs-dc");
        json.put("DS_NM", "datastore-39");
        System.out.println(client.post("/api/vm", json.toString()));
    }
}
