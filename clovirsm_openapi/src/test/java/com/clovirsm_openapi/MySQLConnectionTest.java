package com.clovirsm_openapi;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySQLConnectionTest {
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://10.150.0.4:3306/clovirsm";
    private static final String USER = "";
    private static final String PW = "";

    @Test
    public void 연결테스트() throws Exception {
        Class.forName(DRIVER);
        try (Connection con = DriverManager.getConnection(URL, USER, PW)) {
            System.out.println(con);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
