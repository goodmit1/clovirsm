package com.clovirsm.sys.hv.after;


import com.clovirsm.hv.IAsyncAfter;

import java.util.Map;

public class OnAfterCommonByStateDone extends OnAfterCreateSM implements IAsyncAfter {

    boolean isDone = false;
    Throwable error = null ;

    public OnAfterCommonByStateDone(Map param) {
        super(param);
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public Throwable getErrMsg() {
        return error;
    }

    public void setError(Throwable errMsg) {
        this.error = errMsg;
    }

    @Override
    protected boolean getState() throws Exception {
         if(error != null){
           throw new Exception(  error);
        }
        return isDone;
    }


}
