package com.clovirsm.sys.hv.executor;

import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.PublicKeySshSession;
import com.clovirsm.hv.util.SshUtils;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.resource.VMService;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
public class ShellAddUser extends Common {
    public void createLinuxUser(Map info, SshUtils sshUtils) throws Exception {
        PublicKeySshSession builder = null;
        VMService vmService = (VMService) NCReqService.getService("S");
        try {
            String pwd = (String) ComponentService.getEnv("USER_PASSWORD", "VMware!123123123");
            String key = sshUtils.changeDbPrbKeyToPrbKey((String) info.get("GUEST_CONN_PWD"));
            String ppk = sshUtils.createPublicKey((String) info.get("PRIVATE_KEY"), (String) info.get("SearchKeypair"));
            builder = new PublicKeySshSession.Builder((String) info.get("PUBLIC_DNS_NAME"), (String) info.get("GUEST_NAME"), 22, key.getBytes()).build();

            String[] addusercmd = getAddCmd((String) info.get("SearchKeypair"), pwd, ppk);
            for (String cmd : addusercmd) {
                builder.execute(cmd, false);
            }

            info.put("ACCT_AUTO_CREATE_YN", "Y");

        } catch (Exception e) {
            info.put("ACCT_AUTO_CREATE_YN", "N");
            throw new IllegalArgumentException("계정 생성에 실패하였습니다.");
        } finally {
            builder.disconnect();
        }

        vmService.updateByQueryKey("update_ACCT_AUTO_CREATE_YN", info);
    }

    public void createWindowUser(Map info, SshUtils sshUtils) throws Exception {
        PublicKeySshSession builder = null;
        VMService vmService = (VMService) NCReqService.getService("S");
        try {
            String pwd = (String) ComponentService.getEnv("USER_PASSWORD", "VMware!123123123");
            String key = sshUtils.changeDbPrbKeyToPrbKey((String) info.get("GUEST_CONN_PWD"));
            String ppk = sshUtils.createPublicKey((String) info.get("PRIVATE_KEY"), (String) info.get("SearchKeypair"));
            builder = new PublicKeySshSession.Builder((String) info.get("PUBLIC_DNS_NAME"), (String) info.get("GUEST_NAME"), 22, key.getBytes()).build();

            String[] addusercmd = getAddCmd((String) info.get("SearchKeypair"), pwd, ppk);
            for (String cmd : addusercmd) {
                builder.execute(cmd, true);
            }
            info.put("ACCT_AUTO_CREATE_YN", "Y");

        } catch (Exception e) {
            info.put("ACCT_AUTO_CREATE_YN", "N");
            throw new IllegalArgumentException("계정 생성에 실패하였습니다.");
        } finally {
            builder.disconnect();
        }

        vmService.updateByQueryKey("update_ACCT_AUTO_CREATE_YN", info);
    }

    public String[] getAddCmd(String user, String pwd, String publicKey) throws IOException {
        String inputCmd = CommonUtil.readStream2String(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("UserAdd.sh")), "utf-8");
        Map<String, Object> cmdParam = new HashMap<>();
        cmdParam.put("USER_NAME", user);
        cmdParam.put("USER_PASSWORD", pwd);
        cmdParam.put("PUBLIC_KEY", Objects.requireNonNull(publicKey));
        String shellCMD = CommonUtil.fillContentByVar(inputCmd, cmdParam);
        String[] cmd = shellCMD.split("\n");
        return cmd;
    }


}
