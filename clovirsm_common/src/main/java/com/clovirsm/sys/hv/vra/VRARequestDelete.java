package com.clovirsm.sys.hv.vra;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.fliconz.fw.runtime.util.NumberUtil;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.vmware.vra.VRAAPI; 
import com.clovirsm.sys.hv.DC; 

@Component
public class VRARequestDelete extends VRACommonAction {

	
	@Override
	protected Map run1(VRAAPI api, Map param) throws Exception {
		try
		{
			return api.requestDelete(param );
		}
		catch(Exception e)
		{
			IAfterProcess after =DC.getBefore().onAfterProcess("C", "D", param);;
			after.startTask((String)param.get("CATALOGREQ_ID"));
			return null;
		}
		
	}

}
