package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;
/**
 * 콘솔 열기
 * @author 윤경
 *
 */
@Component
public class OpenConsole extends Common {
	public String run(String vmId) throws Exception
	{
		VMInfo param = dcService.getVMInfo(vmId);
	 	DC dcInfo = dcService.getDC(param.getDC_ID());
		Map result = dcInfo.getAPI().openConsole(newHVParam(dcInfo, param, null , null));
		return (String)result.get(HypervisorAPI.PARAM_CONSOLE_URL);
	}

}
