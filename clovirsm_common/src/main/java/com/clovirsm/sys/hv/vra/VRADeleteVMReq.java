package com.clovirsm.sys.hv.vra;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.service.batch.UsageLogService;
import com.clovirsm.service.resource.VraCatalogService;
import com.clovirsm.sys.hv.DC; 

@Component
public class VRADeleteVMReq extends VRACommonAction {
	@Autowired
	transient UsageLogService usageLogService;
	@Autowired VraCatalogService catalogReqService;
	public Map run(String vmId) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		Map info = catalogReqService.selectOneByQueryKey("selectCatalogReqIdByVM", param);
		if(info != null && !info.isEmpty()) {
			param.putAll(info);
		}
		return run(param);
	}
	@Override
	protected Map run1(VRAAPI api, Map param) throws Exception {
		Map info =  api.deleteVM(param);
		DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		dcInfo.getBefore(dcService).onAfterDeleteVM(param);
		dcService.deleteDiskByVM(param); // 추가 디스크 제거
		// ipService.cancelIp(param); // 랜카드 제거
		usageLogService.useEnd(NCConstant.SVC.S.toString(),(String) param.get("VM_ID"));
		return info;
	}

}
