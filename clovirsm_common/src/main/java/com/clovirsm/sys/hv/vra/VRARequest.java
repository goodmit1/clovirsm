package com.clovirsm.sys.hv.vra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.vmware.vra.VRAAPI;
 
import com.clovirsm.sys.hv.DC; 

@Component
public class VRARequest extends VRACommonAction {

	
	@Override
	protected Map run1(VRAAPI api, Map param) throws Exception {
		
		
		param.put("REQ_TITLE", param.get("REQ_TITLE") + "(" + DateUtil.formatToday("yyyyMMddHHmmss") + ")"); 
		return api.request(param, DC.getBefore().onAfterProcess("C", "C", param));
		
	}

	 
}
