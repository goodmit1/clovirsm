package com.clovirsm.sys.hv.after;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

import java.util.Map;

public abstract class OnAfterCreateSM extends OnAfterCreate{
	public OnAfterCreateSM(  Map param) {
		super(  param);

	}
	@Override
	protected  void onAfterStart(String taskId,Map oldParam)
	{
		oldParam.put("RUN_CD", NCConstant.RUN_CD.W.toString());
		oldParam.put("TASK_STATUS_CD", NCConstant.RUN_CD.W.toString());
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		try {
			dcService.updateByQueryKey("update_" + this.getTableNm(), oldParam) ;
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	@Override
	protected boolean getState()  throws Exception
	{

		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		return dcInfo.getAPI().chkTask(new HVParam(dcInfo.getProp()), taskId );

	}

	@Override
	public void onAfterSuccess(String taskId) throws Exception {
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");


			dcService.updateCreateSuccess(taskId, getTableNm(), param);
			onAfterSuccessExtra();

	}
	@Override
	public void onAfterFail(String taskId, Throwable e) throws Exception {

		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		dcService.updateCreateFail(taskId, getTableNm(), param, e.getMessage());
		onAfterFailExtra();
	}
	protected void onAfterFailExtra()  throws Exception{


	}

	protected void onAfterSuccessExtra() throws Exception {

	}

	/*protected void onAfterPublicSuccess(DCService dcService) throws Exception {
		VMService vmService = (VMService)SpringBeanUtil.getBean("VMService");
		VMInfo vm = dcService.toVMInfo(param);
		Map vmInfo = dcService.getDC((String)param.get("DC_ID")).getAPI().vmState(param, vm);
		vmService.convertMapToIp(param,vmInfo,taskId);
		if(param.get("DATA_DISK_LIST") != null) {
			List<Map> disk = (List<Map>) param.get("DATA_DISK_LIST");
			for(Map m : disk) {
				if(m.get("CUD_CD").equals("C") && m.get("DISK_PATH") != null) {
					int diskSize = Integer.parseInt(String.valueOf(m.get("DISK_SIZE")));
					vm.setDISK_SIZE(diskSize);
					vmService.createPublicCloudDiskMountPath(param, vmInfo,vm);
				}
			}
		}
		dcService.getDC((String)param.get("DC_ID")).getAPI().powerOpVM(param, vm, "powerOn");
		dcService.updateCreateSuccess(taskId, getTableNm(), param);
	}*/

}
