package com.clovirsm.sys.hv.vmware;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.*;
import com.clovirsm.hv.obj.FWInfo;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.util.SshUtils;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.admin.FwMngService;
import com.clovirsm.service.resource.FWService;
import com.clovirsm.service.resource.VraCatalogService;
import com.clovirsm.service.workflow.DefaultNextApprover;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.ISMBeforeAfter;
import com.clovirsm.sys.hv.NextIPHelper;
import com.clovirsm.sys.hv.after.OnAfterCreateDisk;
import com.clovirsm.sys.hv.after.OnAfterCreateImg;
import com.clovirsm.sys.hv.after.OnAfterCreateRevert;
import com.clovirsm.sys.hv.after.OnAfterCreateSnapshot;
import com.clovirsm.sys.hv.executor.*;
import com.clovirsm.sys.hv.vra.VRAOnAfterDelete;
import com.clovirsm.sys.hv.vra.VRAOnAfterRequest;
import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * VM 생성전, 그리고 다른 오브젝트 생성 후 작업 지정
 *
 * @author 윤경
 */
public class BeforeAfterVMWare implements ISMBeforeAfter {


    protected DCService service;
    protected DC dcInfo;
    private final FWService fwService = (FWService) SpringBeanUtil.getBean("fwService");

    public BeforeAfterVMWare() {


    }

    public static String setVarVal(String pattern, Map param) {
        String[] arr = pattern.split("}");
        StringBuffer sb = new StringBuffer();
        for (String a : arr) {
            int pos = a.indexOf("{");
            if (pos >= 0) {
                if (pos > 0) {
                    sb.append(a, 0, pos);
                }

                sb.append(param.get(a.substring(pos + 1)));
            } else {
                sb.append(a);
            }
        }
        return sb.toString();
    }

    private static String[] getIps(String ip) {
        if (ip.indexOf(",") > 0) {
            ip = ip.replace(" ", "");

            return TextHelper.split(ip);
        } else {
            return new String[]{ip};
        }
    }

    private static String[] addIpSubnet(String[] ips) {
        String[] ipSubnet = new String[ips.length];
        for (String ip : ips) {
            int idx = 0;
            if (ip.indexOf("/") != -1) {
                ipSubnet[idx] = ip;
            } else {
                ipSubnet[idx] = ip + "/32";
            }
        }
        return ipSubnet;
    }

    protected String getPoolName(Map param) throws Exception {
        return "RP-ClovirSM";
    }

    @Override
    public String getFolderName(Map param) throws Exception {
        return "ClovirSM-test";
    }

    protected int getVLanId(Map param) throws Exception {
        Map info = service.selectOneByQueryKey("selectMaxLanId", param);
        return NumberHelper.getInt(info.get("VLAN_ID"), 0);
    }

    protected String getPortName(Map param) throws Exception {
        return "dvpg-clovirSM";
    }

    @Override
    public void onChangeDCInfo(Map dcInfo) throws Exception {

    }

    @Override
    public boolean onBeforeCreateVM(Map param) throws Exception {


        Map result = new HashMap();


        if (this.dcInfo.useVLAN()) {
            int vlanId = getVLanId(param);
            result.put(VMWareAPI.PARAM_VLAN_ID, vlanId);
            result.put(VMWareAPI.PARAM_PORT_GROUP_NM, getPortName(param));
        }

        if (this.dcInfo.useRP()) {
            result.put(HypervisorAPI.PARAM_RSC_GROUP_NM, getPoolName(param));
        }


        result.put(VMWareAPI.PARAM_FOLDER_NM, getFolderName(param));


        param.putAll(result);


        return false;

    }

    /*
     *
    @Override
    public IAfterProcess getAfterCreateVM(Map param ) throws Exception {

        return new OnAfterCreateVMVMWare(  param);
    }
    @Override
    public IAfterProcess getAfterCreateDisk( Map param) throws Exception {

        return new OnAfterCreateDisk (  param);
    }
    @Override
    public IAfterProcess getAfterCreateImg(Map param ) throws Exception {

        return new OnAfterCreateImg (  param);
    }
    @Override
    public IAfterProcess getAfterCreateSnapshot(Map param ) throws Exception {

        return new OnAfterCreateSnapshot (  param);
    }

    @Override
    public IAfterProcess getAfterCreateRevert( Map param) throws Exception {

        return new OnAfterCreateRevert (  param);
    }

    @Override
    public IAfterProcess getAfterCreateRevert( Map param) throws Exception {

        return new OnAfterCreateRevert (  param);
    }

    @Override
    public IAfterProcess getAfterUpdateVM(Map param) throws Exception {

        return new OnAfterCreateVMVMWare( param){
            @Override
            public void onAfterSuccess(String taskId) throws Exception {


                //PowerOnVM powerOn = (PowerOnVM)SpringBeanUtil.getBean("powerOnVM");
                //powerOn.runInThread(60000, (String)param.get(Common.PARAM_VM_ID) );
                //GetVMInfos infos = (GetVMInfos)SpringBeanUtil.getBean("getVMInfos");
                //infos.runInThread(5000, (String)param.get(Common.PARAM_VM_ID) ) ;


            }

            @Override
            protected void onAfterSuccessExtra() throws Exception {
                // 메일 보내지 않는다.
            }
        };

    }
    */
    @Override
    public Runnable getAfterCreateThread(String kubun, Map param) throws Exception {
        return null;
    }

    public DCService getService() {
        return service;
    }

    public void setService(DCService service) {
        this.service = service;
    }

    public DC getDcInfo() {
        return dcInfo;
    }

    public void setDcInfo(DC dcInfo) {
        this.dcInfo = dcInfo;
    }

    @Override
    public void reuse(String svcCd, String pkVal, int mm, Date expireDt) throws Exception {
        reuse(svcCd, pkVal, mm, true);
    }

    public void reuse(String svcCd, String pkVal, int mm, boolean sendMail) throws Exception {
        NCReqService service = NCReqService.getService(svcCd);
        Map info = null;

        Map param = service.addTableParam(pkVal, false);
        param.put("ADD_USE_MM", mm);
        service.updateByQueryKey("com.clovirsm.resource.expire", "update_EXPIRED", param);

        DefaultNextApprover approver = (DefaultNextApprover) SpringBeanUtil.getBean("nextApprover");

        if (svcCd.equals("S")) {
            info = service.getAllDetail(pkVal);
            // move folder
            MoveVMIntoFolder move = (MoveVMIntoFolder) SpringBeanUtil.getBean("moveVMIntoFolder");
            info = move.run(pkVal, getOrgFolder(info));

            PowerOnVM powerOn = (PowerOnVM) SpringBeanUtil.getBean("powerOnVM");
            powerOn.run(pkVal);
        } else {
            info = service.getAllDetail(pkVal);
        }

        if (svcCd.equals("S") && info != null) {
            String fromId = (String) info.get("FROM_ID");
            if (fromId != null && fromId.startsWith("C")) {
                VraCatalogService vraservice = (VraCatalogService) SpringBeanUtil.getBean("vraCatalogService");
                reuse("C", fromId, mm, false);

            }
        }

        if (sendMail) {
            Map<String, Map> mailinfo = approver.getDetailList(svcCd, pkVal, (String) info.get("INS_DT"));

            param.put("CMT", approver.getDetailMailContent(null, mailinfo));
            approver.notiAlarm(svcCd, "reuse", pkVal, info);
            //service.sendMail(info.get("INS_ID"), "title_reuse", "expire_reuse", info);
        }

    }

    private String getOrgFolder(Map info) throws Exception {
        DCService dcservice = (DCService) SpringBeanUtil.getBean("DCService");
        return dcservice.getDC((String) info.get("DC_ID")).getBefore(dcservice).getFolderName(info);

    }

    @Override
    public boolean expire(String svcCd, String pkVal, boolean bySchedule) throws Exception {

        NCReqService service = NCReqService.getService(svcCd);
        Map info = service.getAllDetail(pkVal);
        Map param = service.addTableParam(pkVal, false);
        if (info == null) {
            param.put("DEL_YN", "Y");
            service.updateByQueryKey("com.clovirsm.resource.expire", "update_EXPIRED", param);
            return false;
        }

        VraCatalogService vraservice = (VraCatalogService) SpringBeanUtil.getBean("vraCatalogService");

        if (svcCd.equals("S")) {

            try {
                // move folder
                MoveVMIntoFolder move = (MoveVMIntoFolder) SpringBeanUtil.getBean("moveVMIntoFolder");
                move.run(pkVal, PropertyManager.getString("deleteVM.folder", "Deleted"));

                PowerOffVM powerOff = (PowerOffVM) SpringBeanUtil.getBean("powerOffVM");
                powerOff.run(pkVal);

            } catch (NotFoundException e) {
                if (e.getMessage().indexOf("Folder") >= 0) {
                    throw e;
                }
                if ("N".equals(info.get("DEL_YN"))) {
                    param.put("DEL_YN", "Y");
                    service.updateByQueryKey("com.clovirsm.resource.expire", "update_EXPIRED", param);
                }
                String fromId = (String) info.get("FROM_ID");
                if (fromId != null && fromId.startsWith("C")) {
                    vraservice.deleteIfNoVM(fromId);
                }
                return false;
            }

        }

        param.put("EXPIRED_TMS", new Date());
        if (bySchedule) {
            param.put("FAIL_MSG", "EXPIRE_BY_DATE");
        } else {
            param.put("FAIL_MSG", "");
        }
        service.updateByQueryKey("com.clovirsm.resource.expire", "update_EXPIRED", param);
        // 2021.02.22 하이닉스 naming떼문에 카탈로그 만료는 제거함.
			/*if(svcCd.equals("S") && info !=null) {
				String fromId = (String)info.get("FROM_ID");
				if(fromId != null && fromId.startsWith("C")) {

					if(!vraservice.hasVM(fromId)) {
						expire("C", fromId, bySchedule );
					}
				}
			}*/
        return true;

    }

    @Override
    public List<IPInfo> getNextIp(int firstNicId, VMInfo param) throws Exception {
        NextIPHelper ipHelper = new NextIPHelper();

        return ipHelper.getNextIp(firstNicId, param);
    }

    @Override
    public String getVMName(Map vm) throws Exception {
        return null;
    }

    @Override
    public String getImgName(Map param) throws Exception {
        return null;
    }

    @Override
    public boolean onAfterGetVM(Map result) throws Exception {
        result.put("SPEC_ID", "0");
        return false;
    }

    @Override
    public int chgNaming(Object categoryId, String naming) throws Exception {

        return 0;
    }

    @Override
    public void onAfterFWDeploy(Map info) throws Exception {
        if (dcInfo.isPublicCloud()) {
            Map userInfo = this.getService().getUserInfo(info.get("FW_USER_ID"));
            Map vmInfo = this.getService().getVMInfo((String) info.get("VM_ID"));
            try {
                info.putAll(dcInfo.getProp());
                vmInfo(info);

                if ("Y".equals(info.get("VM_USER_YN")) && "N".equals(info.get("DEL_YN"))) {
                    fwUser(info, userInfo, vmInfo);
                }

                fwService.updateFWDeployResult((String) info.get("FW_ID"), "Y");

                Map fwInfo = fwService.selectInfo("NC_FW_org", info);
                if ("N".equals(fwInfo.get("VM_USER_YN")) || "Y".equals(fwInfo.get("ACCT_AUTO_CREATE_YN"))) {
                    FwMngService fwMngService = (FwMngService) SpringBeanUtil.getBean("fwMngService");
                    fwInfo.put("VM_NM", vmInfo.get("VM_NM"));
                    fwInfo.put("SERVER_IP", vmInfo.get("IP"));
                    fwInfo.put("EMAIL", userInfo.get("EMAIL"));
                    fwInfo.put("USER_NAME", userInfo.get("USER_NAME"));
                    fwInfo.put("LOGIN_ID", userInfo.get("LOGIN_ID"));
                    fwInfo.put("TASK_STATUS_CD", "S");
                    fwInfo.put("FAIL_MSG", "");
                    fwMngService.updateApproval(fwInfo);
                }

                fWDeploy(info);
            } catch (Exception e) {
                e.printStackTrace();
                fwService.updateFWDeployResult((String) info.get("FW_ID"), e.getMessage());
            }
        }
    }

    @Override
    public void onAfterDeleteVM(Map param) {
        String fromId = (String) param.get("FROM_ID");
        if (fromId != null && fromId.startsWith("C")) {
            VraCatalogService vraService = (VraCatalogService) SpringBeanUtil.getBean("vraCatalogService");
            try {
                vraService.deleteIfNoVM(fromId);
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }

    @Override
    public String[] getVMNames(Map vm, int count) throws Exception {

        return null;
    }

    @Override
    public boolean reconfigVM(String pkVal, String insDt, Map param) throws Exception {
        ReconfigVM reconfigVM = (ReconfigVM) SpringBeanUtil.getBean("reconfigVM");
        reconfigVM.run(pkVal);
        return true;
    }

    @Override
    public boolean deleteVM(String pkVal, String insDt, Map param) throws Exception {
        DeleteVM deleteVM = (DeleteVM) SpringBeanUtil.getBean("deleteVM");
        deleteVM.run(pkVal);
        return true;
    }

    @Override
    public IAfterProcess onAfterProcess(String svcCd, String cudCd, Map info) throws Exception {


        if (svcCd.equals("C") && cudCd.equals("D")) {
            return new VRAOnAfterDelete(info);
        } else if (svcCd.equals("C")) {
            return new VRAOnAfterRequest(info);
        } else if (svcCd.equals("D") && cudCd.equals("C")) {
            return new OnAfterCreateDisk(info);
        } else if (svcCd.equals("G") && cudCd.equals("C")) {
            return new OnAfterCreateImg(info);
        } else if (svcCd.equals("T") && cudCd.equals("C")) {
            return new OnAfterCreateSnapshot(info);
        } else if (svcCd.equals("R") && cudCd.equals("C")) {
            return new OnAfterCreateRevert(info);
        } else if (svcCd.equals("S") && cudCd.equals("C")) {
            return new OnAfterCreateVMVMWare(info);
        } else if (svcCd.equals("S") && cudCd.equals("U")) {


            return new OnAfterCreateVMVMWare(info) {
                @Override
                public void onAfterSuccess(String taskId) throws Exception {
                    //PowerOnVM powerOn = (PowerOnVM)SpringBeanUtil.getBean("powerOnVM");
                    //powerOn.runInThread(60000, (String)param.get(Common.PARAM_VM_ID) );
                    GetVMInfos infos = (GetVMInfos) SpringBeanUtil.getBean("getVMInfos");
                    infos.runInThread(5000, (String) param.get(Common.PARAM_VM_ID));
                }

                @Override
                protected void onAfterSuccessExtra() throws Exception {
                    // 메일 보내지 않는다.
                }
            };


        } else {
            return null;
        }
    }

    protected void doDeleteAfterExpire(String svcCd, String pkVal) throws Exception {
        if (svcCd.equals(NCConstant.SVC.S.toString())) {
            DeleteVM deleteVM = (DeleteVM) SpringBeanUtil.getBean("deleteVM");
            deleteVM.run(pkVal);
        }
    }

    @Override
    public void deleteAfterExpire(String svcCd, String pkVal) throws Exception {
        doDeleteAfterExpire(svcCd, pkVal);
        NCReqService service = NCReqService.getService(svcCd);
        Map param = service.addTableParam(pkVal, false);

        service.updateByQueryKey("com.clovirsm.resource.expire", "update_EXPIRED", param);

    }

    @Override
    public void onAfterCollect(String table, Map info) throws Exception {


    }

    protected String getVal1EtcConf(String kubun, Object dc, Object id) throws Exception {
        Map param = new HashMap();
        param.put("KUBUN", kubun);
        param.put("DC_ID", dc);
        param.put("ID", id);
        return (String) service.selectOneObjectByQueryKey("selectVal1EtcConf", param);
    }

    @Override
    public void putVraDataJson(JSONObject dataJSON, org.codehaus.jettison.json.JSONObject formJSON, Map param)
            throws Exception {


    }

    private void fWDeploy(Map info) throws Exception {
        String cudCd = (String) info.get("CUD_CD");


        String[] ips = addIpSubnet(getIps((String) info.get("CIDR")));
        String[] ports = TextHelper.isEmpty((String) info.get("PORT")) ? new String[]{} : ((String) info.get("PORT")).split(",");
        FWInfo fwInfo = new FWInfo(TextHelper.nvl((String)info.get("PROTOCOL_CD"), FWInfo.PROTOCOL_TCP), ips, ports);

        if (cudCd.equals(("D"))) {
            DeleteFWRule deleteFWRule = (DeleteFWRule) SpringBeanUtil.getBean("deleteFwRule");
            deleteFWRule.run((String) info.get("VM_ID"), fwInfo);
        } else {
            CreateFWRule createFw = (CreateFWRule) SpringBeanUtil.getBean("createFwRule");
            createFw.run((String) info.get("VM_ID"), fwInfo);
        }


    }

    private void fwUser(Map info, Map userInfo, Map vmInfo) {
        // TODO Auto-generated method stub


        if (!"Y".equals(info.get("ACCT_AUTO_CREATE_YN"))) {
            SshUtils sshUtils = new SshUtils();
            ShellAddUser shellAddUser = (ShellAddUser) SpringBeanUtil.getBean("shellAddUser");
            try {
                Map fwSearchParam = new HashMap();
                fwSearchParam.put("USER_ID", info.get("FW_USER_ID"));
                Map privateKey = fwService.selectOneByQueryKey("selectByPrivateKey", fwSearchParam);
                info.put("SearchKeypair", userInfo.get("LOGIN_ID"));

                if (privateKey == null) {
                    info.putAll(sshUtils.createSsh());
                    fwService.insertByQueryKey("insert_NC_KEYPAIR", info);

                } else {
                    info.put("PRIVATE_KEY", privateKey.get("PRIVATE_KEY"));
                }
                if (info.get("LINUX_YN").equals("Y"))
                    shellAddUser.createLinuxUser(info, sshUtils);
                else if (info.get("LINUX_YN").equals("N"))
                    shellAddUser.createWindowUser(info, sshUtils);

            } catch (Exception e) {
                e.printStackTrace();
                throw new IllegalArgumentException("접근제어 USER 추가 ERROR!");
            }
        }
    }

    private void vmInfo(Map info) {

        try {
            GetVMInfos getInfos = (GetVMInfos) SpringBeanUtil.getBean("getVMInfos");

            info.putAll(getInfos.run((String) info.get("VM_ID")));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // TODO Auto-generated method stub
    }

}
