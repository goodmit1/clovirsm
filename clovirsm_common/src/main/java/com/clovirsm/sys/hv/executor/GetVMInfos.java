package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.*;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.service.resource.DiskService;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class GetVMInfos extends VMCommon  {

    @Autowired
    DiskService diskService;
    @Autowired
    IPService ipService;


    public void runInThread(long delay, String vmId) {
        Thread thread = new Thread() {
            public void run() {
                try {
                    Thread.sleep(delay);

                    GetVMInfos infos = (GetVMInfos) SpringBeanUtil.getBean("getVMInfos");
                    infos.run( vmId);
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public void runInThread(long delay, List<Map> vmInfos) {
        if (vmInfos == null || vmInfos.size() == 0) {
            return;
        }
        try {
            Thread.sleep(delay);
            for (Map m : vmInfos) {
                runInThread(0, m);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void runInThread(long delay, Map vmInfo) {
        Thread thread = new Thread() {
            public void run() {
                try {


                    if (!vmInfo.isEmpty()) {
                        Thread.sleep(delay);
                        GetVMInfos infos = (GetVMInfos) SpringBeanUtil.getBean("getVMInfos");
                        infos.run(vmInfo );
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public Map run(  String vmId) throws Exception {
       return runById(vmId);
    }


    public Map run(Map vmMap)  throws Exception{
        VMInfo vmInfo = dcService.toVMInfo(vmMap);
        DC dcInfo = dcService.getDC(vmInfo.getDC_ID());
        return run1(dcInfo, newHVParam(dcInfo, vmInfo, null, null));
    }
    protected   Map run1(DC dcInfo, HVParam hvParam) throws Exception{
        int idx = 0;
        Map param = new HashMap();




        try {

            VMInfo result = dcInfo.getAPI().vmState(hvParam);
            MapUtil.copy( hvParam.getVmInfo(),result, new String[]{"VM_ID", "DC_ID","TEAM_CD", "INS_ID"});

            String newRunCd = result.getRUN_CD();
            List<DiskInfo> diskList = result.getDISK_LIST();
            if (diskList != null) {
                diskService.syncDiskInfo(result, diskList);
            }
            if (!newRunCd.equals(hvParam.getVmInfo().getRUN_CD())) {

                result.put("FAIL_MSG", "");
                if (newRunCd.equals(HypervisorAPI.RUN_CD_RUN)) {
                    dcService.updateVMStart(result);
                } else {
                    dcService.updateVMStop(result);
                }

            } else {
                dcService.updateByQueryKey("update_NC_VM", result);
            }
            List<IPInfo> ipList = result.getIP_LIST();
            if (ipList != null && ipList.size() > 0) {
                if (dcInfo.isIpAuto()) {
                    for (IPInfo ipInfo : ipList) {


                        if (ipInfo.ip != null && ipInfo.nicId > 0 && !"".equals(ipInfo.ip)) {
                            ipService.updateByQueryKey("update_STATUS_RT_Cancel", param);
                            param.put("IP", ipInfo.ip);
                            param.put("STATUS", "R");
                            param.put("VM_HV_ID", result.getVM_HV_ID());
                            param.put("DC_ID", result.get("DC_ID"));
                            param.put("VM_NM", result.getVM_NM());
                            param.put("MAC_ADDR", ipInfo.macAddress);
                            ipService.insertVMIp(param);
                        }
                    }
                } else {

                    int count = 0;

                    ipService.beforeVMCollect(result);

                    for (IPInfo ipInfo : ipList) {
                        String ip = ipInfo.ip;

                        if (ip != null && ipInfo.nicId > 0 && !"".equals(ip)) {
                            result.put("MAC_ADDR", ipInfo.macAddress);
                            count += ipService.ipUsed(result, ip);


                        }
                    }
                    ipService.afterVMCollect(count > 0, result);
                }


            }
            dcService.getDC(hvParam.getVmInfo().getDC_ID()).getBefore(dcService).onAfterCollect("NC_IP", null);
            return result;
        } catch (NotFoundException e) {
            hvParam.getVmInfo().setRUN_CD("F");
            hvParam.getVmInfo().put("FAIL_MSG", "vm not found");
            dcService.updateVMStop(hvParam.getVmInfo());
            throw e;
        }
    }
 }
