package com.clovirsm.sys.hv;

import com.clovirsm.hv.IBeforeAfter;

public interface ISMBeforeAfter extends IBeforeAfter {

    DCService service = null;
    DC dcInfo = null;
    
    void setService(DCService service) throws Exception;

    void setDcInfo(DC dcInfo) throws Exception;
}
