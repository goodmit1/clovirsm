package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.service.batch.UsageLogService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 디스크 추가
 * @author 윤경
 *
 */
@Component
public class CreateDisk  extends Common{
	
	@Autowired
	DCService dcService;
	@Autowired
	transient UsageLogService usageLogService;
	public void run(  String diskId) throws Exception
	{
	 
		Map diskInfoMap = dcService.getDiskInfo(diskId);
		DiskInfo diskInfo = new DiskInfo(diskInfoMap);
		VMInfo vmInfo = dcService.getVMInfo((String)diskInfo.get("VM_ID"));
		DC dcInfo = dcService.getDC(((String)diskInfo.get("DC_ID")));
		HVParam param = newHVParam(dcInfo, vmInfo, diskInfo, null);
		run1(dcInfo, param);

	}


	protected Map run1(DC dcInfo, HVParam param) throws Exception {
		param.getDiskInfo().setDATASTORE_NM_LIST(  dcService.getDsName( param.getVmInfo().getDC_ID(), (String) param.getDiskInfo().get("DISK_TYPE_ID"), null));

		IBeforeAfter before = dcInfo.getBefore( dcService  );
		param.setAfter(before.onAfterProcess("D","C", param.getDiskInfo()));
		Map result = dcInfo.getAPI().addDisk(param );

		return result;
	}
}
