package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;
/**
 * 디스크 언마운트
 * @author 윤경
 *
 */
@Component
public class UnmountDisk extends CreateDisk{


	@Override
	protected Map run1(DC dcInfo, HVParam param) throws Exception {

		return dcInfo.getAPI().unmount(param);
	}

}