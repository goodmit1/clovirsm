package com.clovirsm.sys.hv.executor;


import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.DateUtil;
import org.springframework.stereotype.Component;

import javax.management.monitor.Monitor;
import java.util.Date;
import java.util.Map;
@Component("insertHostPerf")
public class InsertHostPerf  extends InsertAlarm {

	@Override
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		HVParam hvParam = new HVParam(dcInfo.getProp());
		MonitorParam mParam = new MonitorParam(param);
		hvParam.setMonitorParam(mParam);
		 return  dcInfo.getAPI().getHostAttribute(hvParam);
	}
	protected void onBeforeRun(Map param) throws Exception{
		 
		Date date1 = new Date();
		param.put("REG_DT", DateUtil.format(date1, "yyyyMMdd"));
		param.put("REG_TM", DateUtil.format(date1, "HHmmss"));
	}
	@Override
	protected String getTableNm()
	{
		return "NC_MONITOR_HOST";
	}
	
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		
		 m.put("REG_DT", param.get("REG_DT"));
		 m.put("REG_TM", param.get("REG_TM"));
		 
	}
 
	@Override
	protected void onAfter(BatchService batchService, Map param, Map result) throws Exception{
		super.onAfter(batchService, param, result);
		batchService.updateByQueryKey("delete_real_NC_MONITOR_HOST", param);
	}


}
