package com.clovirsm.sys.hv.after;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.util.IMailSender;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.service.TaskService;
import com.clovirsm.service.resource.DiskService;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.service.workflow.DefaultNextApprover;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.GetVMInfos;

/**
 * VM 생성 후 사후 작업
 * @author 윤경
 *
 */
public class OnAfterCreateVM extends OnAfterCreateSM{
	org.apache.log4j.Logger maillog = LogManager.getLogger("mail");
	public OnAfterCreateVM(Map param)
	{
		super( param);
	}
	
	
	protected void cancelIpReserved()
	{
	 
		IPService ipService = (IPService)SpringBeanUtil.getBean("IPService");
		try {
			ipService.cancelReserveIp((String)param.get("VM_ID"));
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	@Override
	protected void onAfterSuccessExtra() throws Exception {

		try
		{
			DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
			 
			 
			List<IPInfo> ipList = (List)param.get("IP");
			if(ipList != null && ipList.size()>0)
			{
				param.put("PRIVATE_IP",ipList.get(0).ip);
				param.put("GATEWAY",ipList.get(0).gateway);
				param.put("SUBNET_MASK",ipList.get(0).subnetMask);
			}
			
			DefaultNextApprover approver = (DefaultNextApprover)SpringBeanUtil.getBean("nextApprover");
			approver.notiAlarm("S", "vmCreate",  (String)param.get("VM_ID"), param);
			//dcService.sendMail(param.get("INS_ID"), "title_vm_create_mail", "vmCreate", param );
		 
			 
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			maillog.error((new Date()) + "----- mail send error " + e.getMessage());
		}
		
	}
	 
	 
	protected String getTableNm() {
		 
		return "NC_VM";
	}

	protected void onAfterFailExtra() throws Exception
	{
		 
		cancelIpReserved();
	}
	protected String getSvcCd()
	{
		return NCConstant.TASK_CD.S.toString();
	}
	protected String getSvcId()
	{
		return (String)param.get("VM_ID");
	}

	 
}
