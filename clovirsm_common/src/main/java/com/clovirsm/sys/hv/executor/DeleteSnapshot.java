package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 스냅샷 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteSnapshot extends CreateSnapshot{


	@Override
	protected  void doProcess(DC  dcInfo, HVParam param, Map snapshotInfo) throws Exception {

		dcInfo.getAPI().deleteSnapshot(param, (String)snapshotInfo.get("SNAPSHOT_NM"));
	}

}