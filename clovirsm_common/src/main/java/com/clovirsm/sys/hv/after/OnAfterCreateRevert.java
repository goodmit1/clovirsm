package com.clovirsm.sys.hv.after;

import java.util.Map;

import com.clovirsm.common.NCConstant;
import com.clovirsm.service.TaskService;
import com.clovirsm.sys.hv.DCService;

public class OnAfterCreateRevert extends OnAfterCreateSM{
	public OnAfterCreateRevert(  Map param)
	{
		super(param);
	}
	protected String getTableNm() {
		 
		return "NC_REVERT_LOG";
	}

	 
	protected String getSvcCd()
	{
		return NCConstant.TASK_CD.R.toString();
	}
	protected String getSvcId()
	{
		return (String)param.get("VM_ID");
	}
}
