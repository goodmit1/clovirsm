package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.monitor.MonitorService;
import com.clovirsm.sys.hv.DC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 호스트 속성 목록 ( 어드민 > 모니터 > 호스트에서 사용 ) 
 * @author 윤경
 *
 */
@Component
public class ListHostAttribute extends Common {

	@Autowired MonitorService monitorService;
	public List list(String dc,  Map param) throws Exception
	{
		  
		Map r = this.run1(dcService.getDC(dc), param);
		return (List)r.get(VMWareAPI.PARAM_LIST);
	}
	

	protected Map run1(DC dcInfo, Map param) throws Exception {
		List list = monitorService.selectList("NC_MONITOR_HOST", param);
		if(list == null || list.size()>0)
		{	
			return dcInfo.getAPI().getHostAttribute(newHVParam(dcInfo,param));
		}
		else
		{
			Map result = new HashMap();
			result.put(HypervisorAPI.PARAM_LIST, list);
			return result;
		}
	}

}
