package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * VM 시작
 * @author 윤경
 *
 */
@Component
public class PowerOnVM extends VMCommon{

	protected String getOp()
	{
		return VMWareAPI.POWER_ON;
	}
	public String run(String vmId) throws Exception
	{
		Map result = super.runById(vmId);
		return (String)result.get(HypervisorAPI.PARAM_PRIVATE_IP);
	}
	@Override
	protected Map run1(DC dcInfo, HVParam param) throws Exception {
		

		Map result = dcInfo.getAPI().powerOpVM(param,   getOp());
		onAfterProcess(param.getVmInfo() );
		return result;
	}
	protected void onAfterProcess(Map param ) throws Exception
	{
		dcService.updateVMStart(param);
		 
	}
 
	public void runInThread(long delay, String vmId) 
	{
		Thread thread = new Thread()
				{
					public void run()
					{
						try {
							Thread.sleep(delay);
						
							PowerOnVM infos = (PowerOnVM)SpringBeanUtil.getBean("powerOnVM");
							infos.runById(   vmId );
						} catch ( Exception e) {
							 
							e.printStackTrace();
						}
					}
				};
		thread.start();		
	}
		
	 
}
