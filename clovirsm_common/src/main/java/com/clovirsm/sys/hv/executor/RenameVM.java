package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

/**
 * VM명 변경
 * @author 윤경
 *
 */
@Component
public class RenameVM extends Common{

	public void run(String vmId, String newName) throws Exception {
		VMInfo vmInfo = dcService.getVMInfo(vmId);

		DC dcInfo = dcService.getDC(vmInfo.getDC_ID());
		dcInfo.getAPI().renameImg(newHVParam(dcInfo, vmInfo, null, null), newName);
	}

}