package com.clovirsm.sys.hv.vmware;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.sys.hv.AdminAlertException;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.after.OnAfterCreateVM;
import com.clovirsm.sys.hv.executor.Common;
import com.clovirsm.sys.hv.executor.GetVMInfos;
import com.clovirsm.sys.hv.executor.OnFinishCreateVM;
import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

import java.util.List;
import java.util.Map;

public class OnAfterCreateVMVMWare extends OnAfterCreateVM
{

	DCService dcService;
	public OnAfterCreateVMVMWare(  Map param)
	{
		super( param);
		dcService = (DCService)SpringBeanUtil.getBean("DCService");
	}

	@Override
	public void onAfterSuccess(String taskId) throws Exception
	{


		OnFinishCreateVM onFinishCreateVM = (OnFinishCreateVM)SpringBeanUtil.getBean("onFinishCreateVM");
		onFinishCreateVM.run((String)param.get(Common.PARAM_VM_ID) );

		GetVMInfos infos = (GetVMInfos)SpringBeanUtil.getBean("getVMInfos");
		infos.runInThread( 600000, (String)param.get(Common.PARAM_VM_ID) ) ;


		super.onAfterSuccess(taskId);
	}

	/**
	 * 복사 후 추가 디스크 처리
	 * @throws Exception
	 */
	/*
	protected void addExtraDisk() throws Exception
	{
		if(param.get(Common.PARAM_FROM_ID) != null)
		{
			List<Map> extraDiskList = dcService.getImgDisk((String)param.get(Common.PARAM_FROM_ID));
			if(extraDiskList.size()>0)
			{
				List<Map> diskList = dcService.getDC((String)param.get("DC_ID")).getAPI().listVMDisk(param);
				if(diskList.size() != extraDiskList.size()+1)
				{
					throw new AdminAlertException(MsgUtil.getMsg("msg_disk_cnt_not_match", new String[]{param.toString()}));
				}
				for(Map extraDisk : extraDiskList)
				{
					insertDiskInfo((String)param.get(HypervisorAPI.PARAM_VM_NM), extraDisk, diskList);
				}
			}
		}
	}*/
	private void insertDiskInfo(String vmName, Map info, List<Map> diskList) throws Exception
	{
		for(int i=1; i < diskList.size(); i++)
		{
			Map disk = diskList.get(i);
			if(NumberHelper.getLong(disk.get(HypervisorAPI.PARAM_DISK)) == NumberHelper.getLong(info.get(HypervisorAPI.PARAM_DISK)))
			{

				String ds = getDSName(  (String)disk.get(HypervisorAPI.PARAM_DISK_PATH));

				info.put(HypervisorAPI.PARAM_DISK_PATH, disk.get(HypervisorAPI.PARAM_DISK_PATH));
				info.put(HypervisorAPI.PARAM_DATASTORE_NM, ds);
				info.put("DISK_NM", vmName + "_" + info.get("DISK_NM"));

				MapUtil.copy(param, info, new String[]{ "TEAM_CD","DC_ID","VM_ID" });
				int row = dcService.insertDisk(info);
				if(row ==1 ) return;
			}
		}
		throw new AdminAlertException(MsgUtil.getMsg("msg_cant_find_ds", new String[] {info.toString(), diskList.toString()}));

	}
	private String getDSName( String fileName) throws Exception
	{
		int pos = fileName.indexOf("]");
		return fileName.substring(1, pos );

	}






}

