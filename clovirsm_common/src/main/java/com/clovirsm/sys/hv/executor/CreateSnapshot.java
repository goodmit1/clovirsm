package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 스냅샷 추가
 * @author 윤경
 *
 */
@Component
public class CreateSnapshot extends  Common{



	public void run(String vmId, String snapshotId) throws Exception
	{

		VMInfo vmInfo = dcService.getVMInfo(vmId);
		DC dcInfo = dcService.getDC(vmInfo.getDC_ID());
		Map snapshotInfo = dcService.getSnapshotInfo(vmId, snapshotId);


		HVParam param = newHVParam(dcInfo, vmInfo, null, null);
		doProcess(dcInfo, param, snapshotInfo);
	}

	protected  void doProcess(DC  dcInfo, HVParam param, Map snapshotInfo) throws Exception {
		IBeforeAfter before = dcInfo.getBefore( dcService  );
		before.onAfterProcess("T", "C",  snapshotInfo);
		dcInfo.getAPI().createSnapshot(  param, (String) snapshotInfo.get("SNAPSHOT_NM"));
	}


}