package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * VM 스펙 변경
 *
 * @author 윤경
 */
@Component
public class ReconfigVM extends VMCommon {

    public Map run(String vmId) throws Exception {
        VMInfo vmInfo = dcService.getVMCreateInfo(vmId, false);

        return runById(vmInfo);


    }

    @Override
    protected Map run1(DC dcInfo, HVParam   param ) throws Exception {



        IAfterProcess after = dcService.getDC(dcInfo.getProp("DC_ID")).getBefore(dcService).onAfterProcess("S", "U", param.getVmInfo());
        param.setAfter(after);
        return dcInfo.getAPI().reconfigVM( param );

    }

}
