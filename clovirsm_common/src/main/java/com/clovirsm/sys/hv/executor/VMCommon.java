package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;

import java.util.HashMap;
import java.util.Map;

public abstract class VMCommon extends Common {

    protected Map runById(String vmId) throws Exception
    {
        VMInfo vmInfo = dcService.getVMInfo(vmId);
        return runById(vmInfo);

    }
    protected Map runById(VMInfo vmInfo) throws Exception
    {

        Map connInfo = new HashMap();
        String dc = vmInfo.getDC_ID();
        DC dcInfo = dcService.getDC(dc)  ;
        HVParam hvParam = newHVParam(dcInfo, vmInfo, null, null);
        return run1( dcInfo, hvParam);

    }

    protected abstract Map run1(DC dcInfo, HVParam hvParam) throws Exception;


}
