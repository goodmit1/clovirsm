package com.clovirsm.sys.hv.after;

import java.util.Map;

import com.clovirsm.common.NCConstant;
import com.clovirsm.service.TaskService;
import com.clovirsm.sys.hv.DCService;

public class OnAfterCreateImg extends OnAfterCreateDisk{
	public OnAfterCreateImg(  Map param)
	{
		super( param);
	}
	protected String getTableNm() {
		 
		return "NC_IMG";
	}

	 
	protected String getSvcCd()
	{
		return NCConstant.TASK_CD.G.toString();
	}
	protected String getSvcId()
	{
		return (String)param.get("IMG_ID");
	}
}
