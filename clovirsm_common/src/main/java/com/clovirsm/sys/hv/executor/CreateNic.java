package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 랜 카드 추가
 * @author 윤경
 *
 */
@Component
public class CreateNic  extends ReconfigVM
{

	@Override
	protected Map run1(DC dcInfo, HVParam param ) throws Exception {

		int nextNicId = dcService.getNextNicId(param.getVmInfo());

		if(dcInfo.isIpAuto()) {
			List<IPInfo> ipInfo = dcInfo.getBefore(dcService).getNextIp(nextNicId, param.getVmInfo());
			/*for(IPInfo ip : ipInfo)
			{
				dcService.insertNic(ip, param);
			}*/
			param.getVmInfo().setIP_LIST(  ipInfo);
		}
		return dcInfo.getAPI().addNic(param);
	}
 

}
