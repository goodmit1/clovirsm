package com.clovirsm.sys.hv.executor;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.DC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
/**
 * VM 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteVM extends ReconfigVM{
	@Autowired IPService ipService;
	@Override
	protected Map run1(DC dcInfo, HVParam param ) throws Exception {

		try
		{
			Map result = dcInfo.getAPI().deleteVM(param);
		}
		catch(NotFoundException e)
		{
			e.printStackTrace();
		}

		dcInfo.getBefore(dcService).onAfterDeleteVM(param.getVmInfo());
		dcService.deleteDiskByVM(param.getVmInfo()); // 추가 디스크 제거
		ipService.cancelIp(param.getVmInfo()); // 랜카드 제거
		usageLogService.useEnd(NCConstant.SVC.S.toString(),param.getVmInfo().getVM_ID());
		
		return null;
		 
	}
}
