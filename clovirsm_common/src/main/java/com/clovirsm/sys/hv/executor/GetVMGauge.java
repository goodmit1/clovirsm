package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * VM 현재 상태 정보 
 * @author 윤경
 *
 */
@Component
public class GetVMGauge extends VMCommon {

	/**
	 * 
	 * @param vmId
	 * @return disk_totalgb, disk_usedgb, mem_total,disk_usage,cpu_usagemhz,mem_active,cpu_usage,mem_usage
	 * @throws Exception
	 */
	public Map run(String vmId) throws Exception
	{

	 
		return this.runById(vmId);
	}



	@Override
	protected Map run1(DC dcInfo, HVParam hvParam) throws Exception {
		hvParam.getVmInfo().put(VMWareAPI.PARAM_PERIOD, VMWareAPI.PERF_PERIOD_1H);
		return dcInfo.getAPI().getVMPerf(hvParam);
	}
}
