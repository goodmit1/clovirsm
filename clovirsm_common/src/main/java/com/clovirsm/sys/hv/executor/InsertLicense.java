package com.clovirsm.sys.hv.executor;

import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.NumberUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * 라이센스 목록
 * @author user01
 *
 */
@Component("insertLicense")
public class InsertLicense  extends InsertAlarm {

	@Override
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		 return  dcInfo.getAPI().listLicense(newHVParam(dcInfo,param));
	}
	 
	@Override
	protected String getTableNm()
	{
		return "NC_LICENSE";
	}
	@Override
	protected void onBeforeRun(Map param) throws Exception
	{
		batchService.deleteMonitorInfo("NC_LICENSE_HOST",param);
		super.onBeforeRun(param);
		
	}
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		 
		 
	}

	protected void onAfter(BatchService batchService2, Map result) throws Exception{
		List<Map> list = (List)result.get("HOST_LIST");
		for(Map m :list)
		{
			try
			{
				m.put("DC_ID", result.get("DC_ID"));
				batchService.insertMonitorInfo("NC_LICENSE_HOST", m);
				
			}
			catch(Exception ignore)
			{
				
			}
		}
		
	}
	protected void insertInfo(Map m) throws Exception
	{
		try
		{
			if(NumberUtil.getLong(m.get("LICENSE_TOTAL"))>0)
			{	
				batchService.insertMonitorInfo(getTableNm(), m);
			}
			
		}
		catch(Exception ignore)
		{
			
		}	
	}
	@Override
	protected Map run1(DC dcInfo, Map param) throws Exception {
		Map result = super.run1(dcInfo, param);
		
		return result;
		
	}
}
