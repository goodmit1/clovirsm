package com.clovirsm.sys.hv.executor;

import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 모든 VM Sync
 * @author user01
 *
 */
@Component
public class SyncAllVM extends Common{

	@Autowired GetVMInfos vmInfos;
	public void run(String dcId) throws Exception{
		run1(dcService.getDC(dcId)  );
	}
	protected Map run1(DC dcInfo) throws Exception {
		BatchService batchService = (BatchService)SpringBeanUtil.getBean("batchService");
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		Map param = new HashMap();
		List<Map> list = batchService.selectList("NC_VM", param);
		 
		for(Map m : list )
		{
			try
			{
				 
				 
				vmInfos.run(  m);
			}
			catch(Exception ignore)
			{
				
			}
		}
		return null;
	}

}
