package com.clovirsm.sys.hv.executor;


import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.DateUtil;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;
@Component 
public class InsertDSPerf  extends InsertAlarm {

	@Override
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		param.put(HypervisorAPI.PARAM_CRE_TIME, new Date());
		HVParam hvParam = new HVParam(dcInfo.getProp());
		MonitorParam mParam = new MonitorParam(param);
		hvParam.setMonitorParam(mParam);
		 return  dcInfo.getAPI().getDSAttribute(hvParam);
	}
	 
	@Override
	protected String getTableNm()
	{
		return "NC_MONITOR_DS";
	}
	
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		 
		Date date1 = (Date)param.get(HypervisorAPI.PARAM_CRE_TIME);
		m.put("REG_DT", DateUtil.format(date1, "yyyyMMdd"));
		m.put("REG_TM", DateUtil.format(date1, "HHmmss"));
		 
	}
 
	 

}
