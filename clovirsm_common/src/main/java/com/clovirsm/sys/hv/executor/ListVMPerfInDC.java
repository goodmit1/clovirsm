package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 데이터 스토어 속성 (어드민 > 대쉬보드 > 클러스터의 데이터 스토어 탭에서 사용)
 * @author 윤경
 *
 */
@Component
public class ListVMPerfInDC extends ListHostAttribute {

 
	
	@Override
	protected Map run1(DC dcInfo, Map param) throws Exception {
		MonitorParam mParam = new MonitorParam(param);
		mParam.setStartTime( ListPerfHistory.makeDateType(param, HypervisorAPI.PARAM_START_DT));
		mParam.setFinishTime( ListPerfHistory.makeDateType(param, HypervisorAPI.PARAM_FINISH_DT)) ;
		HVParam hvParam = new HVParam(dcInfo.getProp());

		hvParam.setMonitorParam(mParam);
		return dcInfo.getAPI().getVMPerfListInDC(hvParam);
	}

}
