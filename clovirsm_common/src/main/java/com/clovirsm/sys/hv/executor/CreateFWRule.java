package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.FWInfo;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;

import java.util.Map;

public class CreateFWRule extends VMCommon{

    public void run(String vmId, FWInfo fwInfo) throws Exception {
        VMInfo vmInfo =  dcService .getVMInfo(vmId);
        DC dcInfo = dcService.getDC(vmInfo.getDC_ID());
        HVParam hvParam = newHVParam(dcInfo, vmInfo , null, null);
        hvParam.setFwInfo(fwInfo);
        run1(dcInfo, hvParam);
    }

    @Override
    protected Map run1(DC dcInfo, HVParam hvParam) throws Exception {
       return dcInfo.getAPI().createFw(hvParam);
    }
}
