package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.apache.log4j.LogManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RunGuestJob {

	org.apache.log4j.Logger log = LogManager.getLogger(this.getClass());
	final String CMD_TYPE_EXEC = "E" ;
	final String CMD_TYPE_UPLOAD = "U" ;
	final String CMD_TYPE_DOWNLOAD = "D" ;

	DCService service;
	BatchService batchService;

	String linux_guest_id="";
	String linux_guest_pwd="";
	String win_guest_id="";
	String win_guest_pwd="";

	public RunGuestJob()
	{
		service = (DCService)SpringBeanUtil.getBean("DCService");
		batchService = (BatchService)SpringBeanUtil.getBean("batchService");
		linux_guest_id = PropertyManager.getString("linux_guest_id");
		linux_guest_pwd = PropertyManager.getString("linux_guest_pwd");
		win_guest_id = PropertyManager.getString("win_guest_id");
		win_guest_pwd = PropertyManager.getString("win_guest_pwd");
	}
	public String runAll(String jobId) throws Exception
	{
		String runDt = DateUtil.formatToday("yyyyMMddHHmmss");
		Map param = new HashMap();
		List<Map> vmList = batchService.selectList("NC_VM", param);
		param.put("JOB_ID", jobId);
		Map runInfo = batchService.selectInfo("NC_VM_GUEST_JOB", param);
		if(runInfo == null || runInfo.size()==0) throw new Exception("JOB NOT FOUND:" + jobId);
		for(Map vm:vmList)
		{
			VMInfo vmInfo = service.toVMInfo(vm);
			this.runOne(vmInfo, runInfo, runDt);
		}
		return runDt;
	}
	public void runOne(String vmId, String jobId,String runDt) throws Exception
	{
		Map param = new HashMap();

		VMInfo vmInfo = service.getVMInfo(vmId);
		if(vmInfo == null || vmInfo.size()==0) throw new Exception("VM NOT FOUND:" + vmId);
		param.put("JOB_ID", jobId);
		Map runInfo = batchService.selectInfo("NC_VM_GUEST_JOB", param);
		if(runInfo == null || runInfo.size()==0) throw new Exception("JOB NOT FOUND:" + jobId);
		 runOne(vmInfo, runInfo,runDt);
	}
	public void runOne(VMInfo vmInfo, Map runInfo,String runDt)
	{
		if(runDt == null)
		{
			runDt = DateUtil.formatToday("yyyyMMddHHmmss");
		}
		runInfo.put("RUN_DT", runDt);
		runInfo.put("VM_ID", vmInfo.get("VM_ID"));
		logStart(runInfo);

		try
		{
			DC dc = service.getDC((String)vmInfo.get("DC_ID"));
			MapUtil.copyNotExist(dc.getProp(), vmInfo);
			boolean isLinux = true;
			if(((String)vmInfo.get("GUEST_NM")).indexOf("Microsoft")>=0)
			{
				isLinux = false;
				vmInfo.put("GUEST_ID",win_guest_id);
				vmInfo.put("GUEST_PWD",win_guest_pwd);
			}
			else
			{
				vmInfo.put("GUEST_ID",linux_guest_id);
				vmInfo.put("GUEST_PWD",win_guest_pwd);
			}
			String cmdType = (String)runInfo.get("JOB_TYPE");
			if(cmdType.equals(CMD_TYPE_EXEC))
			{
				this.exec(dc, isLinux, vmInfo, runInfo);
			}
			else if(cmdType.equals(CMD_TYPE_UPLOAD))
			{
				this.upload(dc, isLinux, vmInfo, runInfo);
			}
			else if(cmdType.equals(CMD_TYPE_DOWNLOAD))
			{
				this.download(dc, isLinux, vmInfo, runInfo);
			}
			else
			{
				throw new Exception("Invalid CMD==>" + cmdType);
			}
			logSuccess(runInfo);
		}
		catch(Exception e)
		{
			logFail(runInfo, e);
		}
	}
	private void logSuccess(Map runInfo) {
		try {
			runInfo.put("TASK_STATUS_CD", "S");
			runInfo.put("FAIL_MSG", "");

			batchService.updateGuestJobLog(runInfo);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}
	private void logFail(Map runInfo, Exception e) {
		try {
			runInfo.put("TASK_STATUS_CD", "F");
			runInfo.put("FAIL_MSG", e.getMessage());

			log.error("Guest run time error:" + runInfo.get("JOB_NM"),e);
			batchService.updateGuestJobLog(runInfo);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}
	private void logStart(Map runInfo) {

		try {
			batchService.insertGuestJobLog(runInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	protected String getSvrPath(Map runInfo, boolean isLinux)
	{

		String path = (String)runInfo.get("GUEST_LINUX_PATH");
		if(!isLinux)
		{
			path = (String)runInfo.get("GUEST_WIN_PATH");
		}
		return path;
	}
	protected String getLocalPath(Map runInfo, boolean isLinux)
	{

		String path = (String)runInfo.get("LINUX_PATH");
		if(!isLinux)
		{
			path = (String)runInfo.get("WIN_PATH");
		}
		return path;
	}
	protected void exec(DC dc, boolean isLinux , VMInfo vmInfo, Map runInfo) throws Exception
	{

		dc.getAPI().guestRun(Common.newHVParam(dc,vmInfo,null, null), getSvrPath(runInfo, isLinux), (String)runInfo.get("CMD_PARAM"));
	}
	protected void upload(DC dc, boolean isLinux , VMInfo vmInfo, Map runInfo) throws Exception
	{

		dc.getAPI().guestUpload(Common.newHVParam(dc,vmInfo,null, null), getSvrPath(runInfo, isLinux),  getLocalPath(runInfo, isLinux) );
	}
	protected void download(DC dc, boolean isLinux , VMInfo vmInfo, Map runInfo) throws Exception
	{
		String localPath = getLocalPath(runInfo, isLinux);
		localPath = localPath + (localPath.endsWith("/")? "":"/") + runInfo.get("RUN_DT");
		dc.getAPI().guestDownload(Common.newHVParam(dc,vmInfo,null, null), getSvrPath(runInfo, isLinux), localPath);
	}
}
