package com.clovirsm.sys.hv.executor;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 디스크 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteDisk extends CreateDisk{


	@Override
	protected Map run1(DC dcInfo, HVParam param) throws Exception {
			if(param.getVmInfo().getVM_NM()==null)
			{
				param.setVmInfo(null);
			}
			Map result = dcInfo.getAPI().deleteDisk(param);
			usageLogService.useEnd(NCConstant.SVC.S.toString(), param.getDiskInfo().getDS_ID());
			return result;
		}
}
