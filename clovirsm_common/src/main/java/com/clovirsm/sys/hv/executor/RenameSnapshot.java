package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 스냅샷명 변경
 * @author 윤경
 *
 */
@Component
public class RenameSnapshot extends Common{

	public void run(String vmId, String snapshotId, String newName) throws Exception
	{
	 
		Map snapshotInfo = dcService.getSnapshotInfo(vmId, snapshotId);
		snapshotInfo.put(HypervisorAPI.PARAM_OBJECT_NEW_NM, newName);

		VMInfo vmInfo  = dcService.getVMInfo(vmId);
		DC  dcInfo = dcService.getDC(vmInfo.getDC_ID());
		dcInfo.getAPI().renameSnapshot(newHVParam(dcInfo, snapshotInfo), (String) snapshotInfo.get(HypervisorAPI.PARAM_SNAPSHOT_NM), newName );
	}

}