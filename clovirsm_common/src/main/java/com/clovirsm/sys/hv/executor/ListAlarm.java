package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.service.monitor.MonitorService;
import com.clovirsm.sys.hv.DC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 문제 목록
 * @author 윤경
 *
 */
@Component
public class ListAlarm extends   ListEvent{

	@Autowired MonitorService monitorService;
	@Override
	protected Map run1(boolean collect, DC dcInfo, HVParam hvParam) throws Exception {


		return dcInfo.getAPI().listAlarm(hvParam );
	}
	 
}
