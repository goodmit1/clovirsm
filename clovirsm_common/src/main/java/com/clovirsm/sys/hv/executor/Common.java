package com.clovirsm.sys.hv.executor;


import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.service.batch.UsageLogService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public abstract class Common {
	public static final String PARAM_DC_ID = "DC_ID";
	public static final String PARAM_VM_ID = "VM_ID";
	public static final String PARAM_FROM_ID = "FROM_ID";
	public static final String PARAM_TEAM_CD = "TEAM_CD";
	public static final String PARAM_COMP_ID = "COMP_ID";
	protected static final String PARAM_IMG_ID = "IMG_ID";
	public static final String PARAM_DISK_ID = "DISK_ID";
	public static final String PARAM_INS_ID = "INS_ID";
	@Autowired
	DCService dcService;
	
	@Autowired
	transient UsageLogService usageLogService;
	
	Logger log = LoggerFactory.getLogger(Common.class);


	public static HVParam newHVParam(DC dcInfo, VMInfo vmInfo, DiskInfo diskInfo, IAfterProcess afterProcess) {

		return new HVParam(dcInfo.getProp(), vmInfo, diskInfo, afterProcess);
	}

	protected HVParam newHVParam(DC dcInfo, Map param) {
		if(param != null) {
			param.putAll(dcInfo.getProp());
			return new HVParam(param);
		}
		else{
			return new HVParam(dcInfo.getProp());
		}
	}
}
