package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MoveVMIntoFolder extends Common {
	public Map run(String vmId, String folder) throws Exception{
		VMInfo vmInfo = dcService.getVMCreateInfo(vmId, false);

		vmInfo.setFOLDER_NM(  folder);
		DC dcInfo = dcService.getDC(vmInfo.getDC_ID());
		 
		dcInfo.getAPI().moveVMFolder(newHVParam(dcInfo, vmInfo, null , null));
		return null;
	}

	 
}
