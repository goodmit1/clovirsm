package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 이미지 명 변경
 * @author 윤경
 *
 */
@Component
public class RenameImg extends Common{
	public void run(String imgId, String newName) throws Exception
	{
		Map param = new HashMap();
		param.put(PARAM_IMG_ID, imgId);
		Map imgInfo = dcService.getImgInfo(imgId);
		VMInfo vmInfo = new VMInfo();
		vmInfo.setVM_ID(imgId);
		vmInfo.setVM_NM((String) imgInfo.get("IMG_NM"));
		vmInfo.setDC_ID((String) imgInfo.get("DC_ID"));
		DC  dcInfo = dcService.getDC(vmInfo.getDC_ID());
		dcInfo.getAPI().renameImg(newHVParam(dcInfo,vmInfo, null, null), newName);
		
	}

 
	 

}
