package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fm.mvc.util.MsgUtil;
import org.springframework.stereotype.Component;

/**
 * 랜카드 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteNic extends Common {
	public void run(String vmId, int nicId) throws Exception
	{
		VMInfo vmInfo = dcService.getVMInfo(vmId);

		DC dcInfo = dcService.getDC(vmInfo.getDC_ID());

		if(nicId == dcInfo.getAPI().getFirstNicId())
		{
			throw new Exception(MsgUtil.getMsg("msg_cant_del_first_lan", null));
		}
		dcInfo.getAPI().deleteNic(newHVParam(dcInfo , vmInfo, null, null), nicId);

	}
}
