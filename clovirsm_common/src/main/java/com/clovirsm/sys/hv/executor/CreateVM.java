package com.clovirsm.sys.hv.executor;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * VM 생성
 *
 * @author 윤경
 */
@Component
public class CreateVM extends VMCommon  {




    public void run(String vmId) throws Exception {


        VMInfo vmInfo = getCreateInfo(vmId);
        runById(vmInfo);


    }
    protected boolean resetInfo(String vmId) throws Exception {
        Map param = new HashMap();
        param.put("VM_ID", vmId);

        int row = dcService.updateByQueryKey("com.clovirsm.resources.vm.VM", "update_NC_VM_recreate", param);
        if (row > 0) {
            dcService.updateByQueryKey("com.clovirsm.resources.vm.VM", "update_NC_FW_reset_acc", param);
            return true;
        }
        return false;
    }


    @Override
    protected Map run1(DC dcInfo, HVParam hvParam) throws Exception{



        boolean isRecreate = resetInfo(hvParam.getVmInfo().getVM_ID());
        IBeforeAfter before = dcInfo.getBefore(dcService);


        List<IPInfo> ipInfo = dcInfo.getBefore(dcService).getNextIp(dcInfo.getAPI().getFirstNicId(), hvParam.getVmInfo());
		/*for(IPInfo ip : ipInfo)
		{
			dcService.insertNic(ip, param);
		}*/
        VMInfo vmInfo = hvParam.getVmInfo();

        vmInfo.setIP_LIST(ipInfo);
        vmInfo.setPOWER_ON(true);
        return dcInfo.getAPI().createVM(hvParam, isRecreate);

    }

    /**
     * 생성할 VM정보
     *
     * @param vmId
     * @return
     * @throws Exception
     */
    protected VMInfo getCreateInfo(String vmId) throws Exception {

        VMInfo vmInfo = dcService.getVMCreateInfo(vmId, true);
        if (vmInfo == null) {
            throw new NCException(NCException.NOT_FOUND, "VM_ID:", vmId);
        }

        setCopyInfo(vmInfo);
        String[] dsNames = dcService.getDsName(vmInfo.getDC_ID(), null, (String)vmInfo.get("SPEC_ID"));
        vmInfo.setDATASTORE_NM_LIST( dsNames);

        return vmInfo;
    }

    protected void setCopyInfo(VMInfo cinfo) throws Exception {

        String fromId = (String) cinfo.get(Common.PARAM_FROM_ID);
        if (fromId != null && !"".equals(fromId)) {
            Map info = dcService.getVMCopyInfo(fromId);

            if (info == null) {
                throw new NCException(NCException.NOT_FOUND, "VM_ID or IMG_ID", fromId);
            }
            String fromNm = (String) info.get("IMG_NM");
            cinfo.setFROM(fromNm);

        }
    }



}
