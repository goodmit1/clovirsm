package com.clovirsm.sys.hv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.clovirsm.sys.hv.vmware.VMLogHandler;

 

@Configuration
@EnableWebSocket
public class WebSocketConfig4SM implements WebSocketConfigurer {
	 
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        
        registry.addHandler(new VMLogHandler( ), "/ws/vm/log").setAllowedOrigins("*").withSockJS();
    }
}