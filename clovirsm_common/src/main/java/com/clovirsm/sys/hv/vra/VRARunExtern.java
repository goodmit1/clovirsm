package com.clovirsm.sys.hv.vra;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.vmware.vra.VRAAPI;

@Component
public class VRARunExtern extends VRACommonAction{

	@Override
	protected Map run1(VRAAPI api, Map param) throws Exception {
		Map result = new HashMap();
		String apiStr = (String)param.get("api");
		result.put("DATA", api.getExternParam(param, new JSONObject(apiStr)));
		return result;
	}

}
