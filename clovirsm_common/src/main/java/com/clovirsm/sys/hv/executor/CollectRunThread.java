package com.clovirsm.sys.hv.executor;

import org.apache.log4j.LogManager;

public class CollectRunThread  extends Thread
{
	String dcid;
	ICollectByDC collectByDC;
	org.apache.log4j.Logger log = LogManager.getLogger(this.getClass());
	public CollectRunThread(ICollectByDC collectByDC, String dcid)
	{
		this.dcid = dcid;
		this.collectByDC = collectByDC;
	}
	public void run()
	{
		try {
			collectByDC.run(this.dcid);
		} catch (Exception e) {
			
			e.printStackTrace();
			log.error(e);
		}
	}
} 