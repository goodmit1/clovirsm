package com.clovirsm.sys.hv.executor;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 이미지 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteImg extends CreateImg{

	@Override
	protected Map run1(DC dcInfo, HVParam param) throws Exception {
		dcInfo.getAPI().deleteImage(param);

		usageLogService.useEnd(NCConstant.SVC.G.toString(),  param.getVmInfo().getVM_ID());
		return null;
	}

	 
}
