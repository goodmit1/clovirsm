package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.DateUtil;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * 성능 이력 
 * @author 윤경
 *
 */
@Component
public class ListPerfHistory extends Common{

	 
	/**
	 * hv.properties에 
	 * @param hvCd
	 * @param stype
	 * @return
	 * @throws Exception
	 */
	public String[] getPerfHistoryTypes(String hvCd, String stype) throws Exception
	{
		HypervisorAPI api = DC.getAPI(hvCd);
		
		
		return api.getPerfHistoryTypes(stype);
	}
	
	public static Date makeDateType(Map param, String key) throws Exception
	{
		Object o = param.get(key);
		if(o==null   ) return null;
		if("".equals(o))
		{
			param.remove(key);
			return null;
		}
		if(o instanceof String)
		{
			String dateStr = (String)o;
			String format =  "yyyy-MM-dd";
			if(dateStr.length()==8)
			{
				format =  "yyyyMMdd";
			}
			else if(dateStr.length()==16 && dateStr.indexOf(":")>0)
			{
				format =  "yyyy-MM-dd HH:mm";
			}
			return DateUtil.toDate(dateStr,format);
		}
		return null;
	}
	/**
	 * 
	 * @param period
	 * @param dc
	 * @param type HypervisorAPI.OBJ_TYPE_xx에 정의 되어 있음.
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public Map run(String period,String dc, String type, String name, Map param) throws Exception{
		 

		DC dcInfo = dcService.getDC(dc);
		HVParam hvParam = new HVParam(dcInfo.getProp());
		MonitorParam mParam = new MonitorParam();
		if(type.equals(HypervisorAPI.OBJ_TYPE_VM)){
			VMInfo  vmInfo = null;
			try	{
				vmInfo = dcService.getVMInfo(name);
			}catch(Exception ignore){
				vmInfo = new VMInfo();
				vmInfo.setVM_NM(name);

			}
			if(vmInfo != null) hvParam.setVmInfo(vmInfo);
		}

		mParam.setStartTime( makeDateType(param, HypervisorAPI.PARAM_START_DT ));
		mParam.setFinishTime( makeDateType(param, HypervisorAPI.PARAM_FINISH_DT ));
		mParam.setTargetObjType(type);
		mParam.setTargetObjNm(name);
		mParam.setPeriod(period);
		hvParam.setMonitorParam(mParam);
		return this.run1(dcInfo, hvParam );
		 
		 
		
	}

	protected Map run1(DC dcInfo,HVParam hvParam) throws Exception {

		return dcInfo.getAPI().listPerfHistory( hvParam );
	}
	 


}
