package com.clovirsm.sys.hv.executor;

import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 데이터 스토어 속성 (어드민 > 대쉬보드 > 클러스터의 데이터 스토어 탭에서 사용)
 * @author 윤경
 *
 */
@Component
public class ListDSAttribute extends ListHostAttribute {

 
	
	@Override
	protected Map run1(DC dcInfo, Map param) throws Exception {
		return dcInfo.getAPI().getDSAttribute(newHVParam(dcInfo,param));
	}

}
