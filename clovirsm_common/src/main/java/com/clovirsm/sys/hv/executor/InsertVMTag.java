package com.clovirsm.sys.hv.executor;

import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * 라이센스 목록
 * @author user01
 *
 */
@Component("insertVMTag")
public class InsertVMTag  extends InsertAlarm {

	@Override
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		 return  dcInfo.getAPI().getVMTags(newHVParam(dcInfo,param));
	}
	 
	@Override
	protected String getTableNm()
	{
		return "NC_VM_TAG";
	}
	 
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		 
		 
	}

	 
}
