package com.clovirsm.sys.hv.after;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.Common;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

import java.util.Map;

public class OnAfterCreateSnapshot extends OnAfterCreateSM{
	public OnAfterCreateSnapshot(  Map param)
	{
		super(param);
	}
	protected String getTableNm() {
		 
		return "NC_SNAPSHOT";
	}

	@Override
	public void onAfterSuccess(String taskId) throws Exception {
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		VMInfo vmnfo = dcService.getVMInfo((String)param.get("VM_ID"));
		float size =  dcInfo.getAPI().getSnapshotSize(Common.newHVParam(dcInfo, vmnfo, null, null) , (String) param.get("SNAPSHOT_NM"));
		param.put(HypervisorAPI.PARAM_DISK, size);
		super.onAfterSuccess(taskId);
	}
	protected String getSvcCd()
	{
		return NCConstant.TASK_CD.T.toString();
	}
	protected String getSvcId()
	{
		return (String)param.get("VM_ID");
	}
}
