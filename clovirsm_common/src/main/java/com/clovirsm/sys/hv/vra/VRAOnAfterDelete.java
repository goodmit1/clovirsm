package com.clovirsm.sys.hv.vra;

import java.util.Map;

import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.after.OnAfterCreate;
import com.clovirsm.sys.hv.after.OnAfterCreateThread;

public class VRAOnAfterDelete extends OnAfterCreate {

	public VRAOnAfterDelete( Map param ) {
		super(  param );
		 
	}
	@Override
	protected boolean getState( ) throws Exception
	{
		try
		{
			
			VRACommonAction.getAPI(param).requestDelete(param );
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	@Override
	protected String getSvcCd() {
		 
		return "C";
	}

	@Override
	protected String getSvcId() {
		 
		return (String)param.get("CATALOGREQ_ID");
	}

	@Override
	protected String getTableNm() {
		 
		return "NC_VRA_CATALOGREQ";
	}
	@Override
	public void onAfterSuccess(String taskId) throws Exception {
		 
		
	}
	@Override
	public void onAfterFail(String taskId, Throwable e) throws Exception {
		 
		
	}
	@Override
	protected void onAfterStart(String taskId, Map param) {
		 
		
	}

}
