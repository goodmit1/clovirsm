package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;
@Component
public class OnFinishCreateVM extends VMCommon{
    public void run(String vmId ) throws Exception
    {
        this.runById(vmId);

    }


    @Override
    protected Map run1(DC dcInfo, HVParam param) throws Exception {
        dcInfo.getAPI().onFinishCreateVM(param);
        return null;
    }
}
