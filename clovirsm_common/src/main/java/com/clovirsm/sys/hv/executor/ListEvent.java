package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.MonitorParam;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 이벤트 목록
 * @author 윤경
 *
 */
@Component
public class ListEvent extends   Common{


	protected boolean isCollect(){
		return true;
	}
	public Map run(String dc, String type, String name) throws Exception
	{
		MonitorParam param = new MonitorParam();


		param.setTargetObjNm(  name);
		
	 
		
		param.setTargetObjType(  type);
		DC dcInfo = dcService.getDC(dc);
		HVParam hvParam = new HVParam(dcInfo.getProp());
		hvParam.setMonitorParam(param);
		return run1(isCollect(), dcInfo, hvParam );
		
	}
 

	protected Map run1(boolean collect, DC dcInfo, HVParam hvParam) throws Exception {


		return dcInfo.getAPI().listEvent( hvParam   );
	}
}
