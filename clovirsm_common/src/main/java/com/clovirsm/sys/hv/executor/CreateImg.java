package com.clovirsm.sys.hv.executor;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
/**
 * 이미지 생성
 * @author 윤경
 *
 */
@Component
public class CreateImg extends  Common
{

	public void run(String imgId) throws Exception
	{
		Map param = new HashMap();
		param.put(PARAM_IMG_ID, imgId);
		Map imgInfo = dcService.getImgInfo(imgId);

	}
	protected void run1(Map imgInfo) throws Exception{



		VMInfo info = dcService.getVMInfo((String)imgInfo.get( PARAM_FROM_ID));
		if(info == null)
		{
			throw new NCException(NCException.NOT_FOUND,"VM", (String)imgInfo.get(PARAM_FROM_ID)  );
		}

		DC dcInfo = dcService.getDC(info.getDC_ID());
		VMInfo newImgInfo = new VMInfo(imgInfo);
		newImgInfo.setVM_ID(imgInfo.get("IMG_ID"));
		newImgInfo.setFROM(info.getVM_NM());
		newImgInfo.setVM_NM((String) imgInfo.get("IMG_NM"));

		String[] dsNames = dcService.getDsName(info.getDC_ID(), dcInfo.getProp("IMG_DISK_TYPE_ID"), null );
		newImgInfo.setDATASTORE_NM_LIST(  dsNames);
		HVParam param = newHVParam(dcInfo, newImgInfo, null,  dcInfo.getBefore( dcService  ).onAfterProcess("G","C", info));
		run1(dcInfo, param);

	}


	protected Map run1(DC dcInfo, HVParam param) throws Exception {
		return dcInfo.getAPI().createImage(param);
	}
}
