package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

/**
 * 데이터 센터 정보 입력 시 입력한 OS템플릿이 존재하는지 체크
 * @author 윤경
 *
 */
@Component
public class CheckTemplatePath extends Common{

	public VMInfo run(String dcId, String templateName) throws Exception
	{
		DC dcInfo = dcService.getDC(dcId);
		if(dcInfo==null) throw new Exception("not found dc");

		return dcInfo.getAPI().chkTemplatePath(newHVParam(dcInfo, null), templateName);
	}



}
