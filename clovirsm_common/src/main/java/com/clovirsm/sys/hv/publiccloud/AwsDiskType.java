package com.clovirsm.sys.hv.publiccloud;
import com.clovirsm.sys.hv.publiccloud.PublicDiskType;

import java.util.Arrays;

public  enum  AwsDiskType implements PublicDiskType{
    HDD("1","st1"),
    SSD("2","gp2");

    String key;
    String value;
    AwsDiskType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public PublicDiskType convertDiskType(String value) {

        for(AwsDiskType diskType : AwsDiskType.values()){
            if(isPublicDiskType(diskType, value)){
                return diskType;
            }
        }
        throw new IllegalArgumentException("Disk 값이 없습니다.");

    }

    private boolean isPublicDiskType(AwsDiskType awsDiskType, String value){
        return awsDiskType.key.equals(value);
    }

}
