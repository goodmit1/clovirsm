package com.clovirsm.sys.hv.publiccloud;

public interface PublicDiskType {
    public PublicDiskType convertDiskType(String value);
}
