package com.clovirsm.sys.hv.executor;

import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.sys.hv.DC;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 스냅샷으로 부터 VM 복구
 * @author 윤경
 *
 */
@Component
public class RevertVM extends CreateSnapshot{

	@Override
	protected  void doProcess(DC  dcInfo, HVParam param, Map snapshotInfo) throws Exception {

		IBeforeAfter before = dcInfo.getBefore( dcService  );
		param.setAfter(before.onAfterProcess("R", "C", snapshotInfo));
		dcInfo.getAPI().revertVM( param, (String) snapshotInfo.get("SNAPSHOT_NM"));

	}

}