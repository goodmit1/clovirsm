package com.clovirsm.sys.hv.executor;

import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 서버 계정 추가/삭제 
 * @author DT042090
 *
 */

@Component
public class AddDelUser extends  Common{


	public void runOne( Map param) throws Exception
	{
		VMInfo vmInfo = dcService.getVMInfo((String)param.get("VM_ID"));
		if(vmInfo == null) throw new Exception("VM not found");
		DC dcInfo = dcService.getDC((String)vmInfo.get("DC_ID"));
		if(dcInfo == null) throw new Exception("DC not found");



			String cmd = getCmd(dcInfo,vmInfo, param);
			Map userInfo = dcService.getUserInfo(param.get("FW_USER_ID"));


			adddeluser((String)param.get("FW_ID"), "N".equals( param.get("DEL_YN")),(String)param.get("FW_USER_ID"),(String) userInfo.get("LOGIN_ID"), dcInfo, vmInfo, cmd);

	}


	protected String getCmd(DC dcInfo,VMInfo vmInfo,Map param) throws Exception
	{
		Map connMap=null;
		if(isWindow(vmInfo))
		{
			connMap= dcInfo.getPropMap("WIN_GUEST");

		}
		else
		{
			connMap= dcInfo.getPropMap("LINUX_GUEST");
		}
		vmInfo.setOS_ADMIN_ID((String)connMap.get("CONN_USERID"));
		vmInfo.setOS_ADMIN_PWD((String) connMap.get("CONN_PWD"));

		return (String)connMap.get("CONN_URL");
	}


	@Autowired @Qualifier("powerOnVM") PowerOnVM poweronVM;

	protected Map run1(DC dcInfo, Map param) throws Exception {

		VMService vmService = (VMService) NCReqService.getService("S");
		VMInfo vmInfo = dcService.getVMInfo(((String)param.get("VM_ID")));


		String cmd = getCmd(dcInfo,vmInfo, param);
		param.put("VM_NM", vmInfo.getVM_NM());

		List<Map> userList = vmService.selectList("NC_FW_4AddDelUser", param);

		for(Map m : userList)
		{
			try
			{
				adddeluser((String)m.get("FW_ID"), "Y".equals(m.get("DEL_YN")),(String) m.get("FW_USER_ID"),(String) m.get("LOGIN_ID"), dcInfo, vmInfo,  cmd);
			}
			catch(Exception e)
			{
				e.printStackTrace();


			}

		}
		System.out.println("################### end add del user ");
		return null;
	}

	protected boolean adddeluser(String fwId, boolean isDel, String fwUserId, String loginId,  DC dcInfo, VMInfo vmInfo, String cmd) throws Exception
	{

		String paramStr = null;
		boolean resultTF = false;
		if(!isDel)
		{
			//addLDAPUser(dcInfo, m);
			paramStr = "adduser " + loginId;
		}
		else
		{
			paramStr = "deluser " + loginId;
		}
		System.out.println(paramStr);
		HVParam param = newHVParam(dcInfo, vmInfo, null, null);
		if(paramStr != null)
		{
			Map result = dcInfo.getAPI().guestRun(param, cmd, paramStr);
			if(NumberUtil.getInt(result.get("EXIT_CODE"),-1)==0) //성공인 경우 
			{
				resultTF  = true;

			}
			else
			{
				if(!isWindow(vmInfo))
				{
					result = dcInfo.getAPI().guestRun(param, "/usr/bin/id",  loginId);
					if(NumberUtil.getInt(result.get("EXIT_CODE"),-1)==0) //성공인 경우 
					{
						resultTF = true;

					}
					else
					{
						resultTF = false;
					}
				}
				else
				{
					resultTF = false;
				}

			}
			VMService vmService = (VMService) NCReqService.getService("S");
			Map updateParam = new HashMap();
			updateParam.put("FW_ID", fwId);
			updateParam.put("FW_USER_ID", fwUserId);
			updateParam.put("ACCT_AUTO_CREATE_YN", resultTF ? "Y":"N");
			vmService.updateByQueryKey("update_ACCT_AUTO_CREATE_YN", updateParam);
		}
		return resultTF;
	}
	protected void uploadFile(DC dcInfo, HVParam param, String cmd, String localPath) throws Exception {
		dcInfo.getAPI().guestUpload(param, cmd, localPath);

	}
	private boolean isWindow(VMInfo vmInfo) throws Exception
	{
		return vmInfo.isWindows();
	}

}
