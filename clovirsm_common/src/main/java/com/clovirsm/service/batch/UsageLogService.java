package com.clovirsm.service.batch;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fw.runtime.util.DateUtil;
import com.ibm.icu.util.Calendar;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;

@Service
public class UsageLogService extends NCDefaultService{

	public void useEnd(String cd, String id) throws Exception
	{
		Map param = new HashMap();
		param.put("SVC_CD", cd);
		param.put("SVC_ID", id);
		super.updateDBTable("NC_USE_LOG", param);
	}
	public void useStop(String cd, String id, String teamCd, String compId , String userId) throws Exception
	{
		 
		useStart(cd, id, teamCd, compId,userId, false);
	}
	private void useStart(String cd, String id, String teamCd, String compId, String userId, boolean isRun) throws Exception
	{
		if(cd.equals(NCConstant.SVC.S.toString())) useEnd(cd, id);
		Map param = new HashMap();
		param.put("SVC_CD", cd);
		param.put("SVC_ID", id);
		if(teamCd==null || "".equals(teamCd))
		{
			param.put("TEAM_CD", this.getUserVO().getTeam().getTeamCd());
		}
		else
		{
			param.put("TEAM_CD", teamCd);
		}
		param.put("COMP_ID", compId);
		param.put("RUN_YN", isRun?"Y":"N");
		if(userId != null) param.put("_USER_ID_", userId);
		super.insertDBTable("NC_USE_LOG", param);
	}
	public void useStart(String cd, String id, String teamCd, String compId, String userId) throws Exception
	{
		useStart(cd, id, teamCd, compId, userId, true);
	}
	@Override
	protected String getNameSpace() {
		return "com.clovirsm.common.log";
	}
	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * 일일 사용량
	 * @param date
	 */
	public void insert_NC_DD_USE(String date)
	{
		try
		{
			Map param = new HashMap();
			param.put("YYYYMMDD", date);
			param.put("_NOW_", new Date());
			insertByQueryKey( "insert_NC_DD_USE", param);
			/*
			List<Map> list = sqlSession.selectList(NS+"selectListDayUse", param);
			String old=null;
			float totalTerm = 0;
			float stopTotalTerm = 0;
			for(Map m : list)
			{
				String key = (String) m.get("SVC_CD") + m.get("SVC_ID");
				if(old != null && !key.equals(old))
				{

					int total = (int) Math.ceil(totalTerm);
					int stopTotal = (int) Math.ceil(stopTotalTerm);
					param.put("USE_AMT", total==0 ? 1:total );
					param.put("STOP_AMT", stopTotal==0 ? 0:stopTotal );
					totalTerm = 0;

					sqlSession.insert(NS+"insert_NC_DD_USE", param);

				}
				float term = getTerm( (String)m.get("START_TIME"),(String)m.get("END_TIME"));
				if("Y".equals(m.get("RUN_YN")))
				{
					totalTerm += term ;
				}
				else
				{
					stopTotalTerm += term ;
				}

				param.putAll(m);
				old = key;
			}
			if(old != null)
			{
				int total = (int) Math.ceil(totalTerm);
				param.put("USE_AMT", total==0 ? 1:total );
				sqlSession.insert(NS+"insert_NC_DD_USE", param);
			}*/
		}
		catch(Exception ignore)
		{
			ignore.printStackTrace();
		}
	}
}
