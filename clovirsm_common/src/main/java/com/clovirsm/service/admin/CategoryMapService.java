package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
@Service
public class CategoryMapService  extends NCDefaultService {

	@Override
	protected String getTableName() {
		 
		return "NC_CATEGORY_MAP";
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return new String[] {"ID", "CATEGORY_ID", "KUBUN"};
	}

	@Override
	protected String getNameSpace() {
	 
		return "com.clovirsm.admin.categoryMap";
	}
	public int insertByCode(String id,  String kubun, String...  code) throws Exception{
		Map param = new HashMap();
		param.put("ID", id);
		param.put("KUBUN", kubun);
		int row = 0;
		for(int i=0; i < code.length; i++) {
			param.put("CATEGORY_CODE", code[i] );
			try
			{
				row+=this.insertByQueryKey("insert_NC_CATEGORY_MAP_byCode",param);
			}
			catch(Exception ignore) {
				
			}
		}
		return row;
			 
	}
	public void insert(String id, Object o, String kubun, String iconYN ) throws Exception{
		
		Map param = new HashMap();
		param.put("ID", id);
		param.put("KUBUN", kubun);
		if(iconYN == null)
			iconYN = "N";
		if(iconYN == "N") {
			this.delete(param);
			String[] categoryIds = null;
			if(o instanceof String[]) {
				categoryIds = (String[])o;
				if(categoryIds != null) {
					for(int i=0; i < categoryIds.length; i++) {
						param.put("CATEGORY_ID", categoryIds[i]);
						this.insert(param);
					}	
				}
			}
		}
		
	}

}
