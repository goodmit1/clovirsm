package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;

@Service
public class DiskTypeMngService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.disktype.DiskTypeMng";
	}

	@Override
	protected String getTableName() {

		return "NC_DISK_TYPE";
	}
	@Override
	public String[] getPks() {
		return new String[]{"DISK_TYPE_ID"};
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("DEL_YN", "sys.yn");
		return config;
	}
}