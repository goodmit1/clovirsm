package com.clovirsm.service.admin;

import com.clovirsm.common.NCDefaultService;
import com.clovirsm.service.resource.IPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CategoryConfService extends NCDefaultService {

	@Autowired IPService ipService;
	protected String getNameSpace() {
		return "com.clovirsm.admin.categoryConf";
	}

	@Override
	protected String getTableName() {
		return "NC_DC_KUBUN_CONF";
	}
	@Override
	public String[] getPks() {
		return new String[]{"ID", "DC_ID", "KUBUN"};
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		return config;
	}

	public int insertOrUpdateOrDelete(Map param) throws Exception {
		int row = 0;
		if("IP".equals(param.get("KUBUN")) && param.get("VAL1").equals(param.get("ORI_IP"))) {
			return row;
		}
		if(param.get("IU") != null && !"".equals(param.get("IU"))){
			if(param.get("IU").equals("I")) {
				row = insert(param);
			}else if(param.get("IU").equals("U")) {
				row = update(param);
			}else if(param.get("IU").equals("D")) {
				row = delete(param);
			}
		}
		if(row > 0 && "IP".equals(param.get("KUBUN")) && !"".equals(param.get("VAL1"))) {
			
			String[] ips = ((String)param.get("VAL1")).split(",");
			for(String ip:ips)
			{	
				ipService.insertAllIp("", ip );
			}
		}
		return row;
	}
}