package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;

@Service
public class LicenseService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.license.License";
	}

	@Override
	protected String getTableName() {

		return "NC_LICENSE";
	}
	@Override
	public String[] getPks() {
		return new String[]{"LICENSE_KEY"};
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		return config;
	}
}