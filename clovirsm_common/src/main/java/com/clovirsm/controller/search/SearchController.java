package com.clovirsm.controller.search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.search.SearchService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/search" )
public class SearchController extends DefaultController{

	private static enum SEARCH_TYPE {equals, like}

	@Autowired SearchService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

	@RequestMapping("/vm.do")
	public Object search_vm(final HttpServletRequest request, HttpServletResponse response){
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				String keyword = (String)param.get("keyword");
				if(keyword != null){
					List list = getService().list(param);
					result.put("list", list);
					result.put("total", getService().list_count(param));
				} else {
					result.put("list", null);
					result.put("total", 0);
				}
				return result;
			}
		}).run(request);
	}


//	@RequestMapping("/vm.do")
//	@Transactional
//	public String search_vm(HttpServletRequest request, HttpServletResponse response) throws Exception {
//		String search_value = request.getParameter("search_value");
//		JSONObject param = makeSearchQueryParam(search_value, SEARCH_TYPE.like);
//		RestClient restClient = new RestClient(PropertyManager.getString("elasticsearch.url", "http://localhost:9200"));
//		restClient.setMimeType("application/json;chatset=utf-8");
//		Object result = restClient.post("/vmlist/_search", param.toJSONString());
//		param.put("result", result);
//		return param.toJSONString();
//	}



	private static JSONObject makeSearchQueryParam(String search_value, SEARCH_TYPE searchType) {
		JSONObject param = new JSONObject();
		JSONObject query = new JSONObject();
		param.put("query", query);

		JSONObject bool = new JSONObject();
		query.put("bool", bool);
		JSONArray should = new JSONArray();
		bool.put("should", should);

		if(SEARCH_TYPE.equals == searchType){
			JSONObject match = new JSONObject();
			should.add(match);
			JSONObject col = new JSONObject();
			match.put("match", col);
			col.put("vm_nm", search_value);

			match = new JSONObject();
			should.add(match);
			col = new JSONObject();
			match.put("match", col);
			col.put("guest_nm", search_value);

			match = new JSONObject();
			should.add(match);
			col = new JSONObject();
			match.put("match", col);
			col.put("private_ip", search_value);
		} else if(SEARCH_TYPE.like == searchType){
			search_value = search_value + "*";

			JSONObject wildcard = new JSONObject();
			should.add(wildcard);
			JSONObject col = new JSONObject();
			wildcard.put("wildcard", col);
			col.put("vm_nm", search_value);

			wildcard = new JSONObject();
			should.add(wildcard);
			col = new JSONObject();
			wildcard.put("match", col);
			col.put("guest_nm", search_value);

			wildcard = new JSONObject();
			should.add(wildcard);
			col = new JSONObject();
			wildcard.put("match", col);
			col.put("private_ip", search_value);
		}
		JSONArray _source = new JSONArray();
		_source.add("dc_id");
		_source.add("dc_nm");
		_source.add("vm_id");
		_source.add("vm_nm");
		_source.add("guest_nm");
		_source.add("private_ip");
		_source.add("purpose");
		_source.add("cmt");
		param.put("_source", _source);

		return param;
	}
}