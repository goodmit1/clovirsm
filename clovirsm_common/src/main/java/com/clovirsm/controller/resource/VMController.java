package com.clovirsm.controller.resource;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCException;
import com.clovirsm.common.NCReqService;
import com.clovirsm.common.RequiredCheckUtil;
import com.clovirsm.controller.ReqController;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.sys.hv.executor.*;
import com.clovirsm.util.JSONUtil;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.mvc.util.ExcelImportUtil;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value =  "/vm" )
public class VMController extends ReqController{

	VMService service;
	@Override
	protected VMService getService() {
		if(service == null) {
			try {
				service = (VMService) NCReqService.getService("S");
			} catch (Exception e) {
				service = (VMService)SpringBeanUtil.getBean("VMService");
			}
		}
		return service;
	}
	List<Map> getExcelData(MultipartFile file, String[] fieldArr) throws Exception
	{

	    
		List fields = new ArrayList();
		for(String f : fieldArr)
		{
			fields.add(f);
		}
		ExcelImportUtil util = new ExcelImportUtil(1, fields);
		System.out.println(file);
		return util.importExcel(file.getOriginalFilename(), file.getInputStream());


	}

	@RequestMapping(value = "/mig", method = RequestMethod.POST)
	public Object mig(MultipartHttpServletRequest request) {

		try
		{
			List<Map> list = getExcelData(request.getFile("org_file"),request.getParameterValues("field"));
			getService().updateExcelImport(list, request.getParameter("DC_ID"), null);
			Map param = new HashMap();
			param.put("MSG", MsgUtil.getMsg("msg_sucess_cnt", new String[] {String.valueOf(list.size())}));
			return ControllerUtil.getSuccessMap(param, "MSG");

		}
		catch(Exception e)
		{
			return ControllerUtil.getErrMap(e.getMessage());

		}



	}



	@RequestMapping(value =  "/info" )
	public Object info(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return getService().getInfo(param);
			}
		}).run(request);
	}

	@RequestMapping(value =  "/info_with_old" )
	public Object info_with_old(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return getService().getInfoWithOld(param);
			}
		}).run(request);
	}

	/**
	 * 수정 요청, DEPLOY_REQ_YN=Y이면 바로 요청
	 * @throws Exception
	 */
	@RequestMapping(value =  "/updateReq" )
	public Object updateReq_request(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				boolean isDeploy="Y".equals(param.get("DEPLOY_REQ_YN"));
				param.put("CUD_CD", NCConstant.CUD_CD.U.toString());
				param.put("APPR_STATUS_CD", NCConstant.APP_KIND.W.toString());
				String[] result = RequiredCheckUtil.requiredCheck(param, new String[]{"VM_NM", "CPU_CNT", "DISK_SIZE", "RAM_SIZE"});
				if(result != null){
					System.out.println(result);
					throw new NCException(NCException.NOT_FOUND, MsgUtil.getMsg( "NC_VM_" + result[0] , null));
				}
				int old_DISK = ("G".equals(param.get("OLD_DISK_UNIT")) ? 1 : 1024) * Integer.parseInt(String.valueOf(param.get("OLD_DISK_SIZE")));
				int new_DISK = ("G".equals(param.get("DISK_UNIT")) ? 1 : 1024) * Integer.parseInt(String.valueOf(param.get("DISK_SIZE")));
				List vmDiskReqList = null;
				 
				if(param.get("NC_VM_DATA_DISK") != null){
					vmDiskReqList = new ArrayList<Map<String, Object>>(JSONUtil.createJSONArray((String)param.get("NC_VM_DATA_DISK")));
				}
				 
				if(NCConstant.RUN_CD.R.toString().equals(param.get("RUN_CD")) || NCConstant.RUN_CD.S.toString().equals(param.get("RUN_CD"))){
					if(vmDiskReqList != null  )
					{
						if(old_DISK != new_DISK)
						{
							getService().chkDiskSize((String)param.get("VM_ID"), vmDiskReqList );
						}
					}
					else if(old_DISK > new_DISK){
						throw new NCException(NCException.LACK_OF_SIZE, String.valueOf(new_DISK), String.valueOf(old_DISK));
					}else if(old_DISK != new_DISK){
						if(getService().selectCountByQueryKey("list_count_NC_SNAPSHOT", param) > 0){
							throw new NCException(NCException.SNAPSHOT_NOT_DEL);
						}
					}
				}
				if(!param.get("VM_NM").equals(param.get("OLD_VM_NM"))){
					if(getService().selectCountByQueryKey("list_count_NC_VM_NAME", param) > 0){
						throw new NCException(NCException.DUPLICATE_NAME, (String)param.get("VM_NM"));
					}
				}
				List vmUserReqList = null;
				if(param.get("NC_VM_USER_LIST") != null){
					vmUserReqList = new ArrayList<Map<String, Object>>(JSONUtil.createJSONArray((String)param.get("NC_VM_USER_LIST")));
				}
				getService().updateVMReq(param, vmUserReqList, vmDiskReqList, isDeploy);
				return ControllerUtil.getSuccessMap(param,  "INS_DT");

			}

			

		}).run(request);
	}
	
	@Autowired
	transient PowerOnVM powerOnVM;

	@Autowired
	transient PowerOffVM powerOffVM;

	@Autowired
	transient RebootVM rebootVM;

	@Autowired
	transient OpenConsole openConsole;

	@Autowired
	transient GetVMGauge vmGauge;

	@Autowired
	transient GetVMInfos vmStatus;


	@RequestMapping(value =  "/powerOn" )
	public Object powerOn(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner(){

			@Override
			protected Object run(Map param) throws Exception {
				if(getService().isOwner(param)) {
					String ip = powerOnVM.run((String) param.get("VM_ID"));
					param.put("PRIVATE_IP", ip);
					param.put("RUN_CD", NCConstant.RUN_CD.R.toString());
					return ControllerUtil.getSuccessMap(param, getService().getPks());
				}
				else {
					throw new Exception("No right");
				}
			}
		}).run(request);
	}

	@RequestMapping(value =  "/powerOff" )
	public Object powerOff(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				if(getService().isOwner(param)) {
					if(!param.get("RUN_CD").equals(NCConstant.RUN_CD.R.toString())){
						throw new NCException("err.SVR_NOT_START");
					}
					powerOffVM.run((String) param.get("VM_ID"));
					param.put("RUN_CD", NCConstant.RUN_CD.S.toString());
					return ControllerUtil.getSuccessMap(param, getService().getPks());
				}
				else {
					throw new Exception("No right");
				}
			}
		}).run(request);
	}

	@RequestMapping(value =  "/reboot" )
	public Object reboot(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				if(getService().isOwner(param)) {
					rebootVM.run((String) param.get("VM_ID"));
					return ControllerUtil.getSuccessMap(param, getService().getPks());
				}
				else {
					throw new Exception("No right");
				}
			}
		}).run(request);
	}

	@RequestMapping(value =  "/sync" )
	public Object sync(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {

				vmStatus.run((String)param.get("VM_ID") );
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);
	}
	@RequestMapping(value =  "/syncAll" )
	public Object syncAll(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				getService().syncAll();
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);
	}
	@RequestMapping(value =  "/openConsole" )
	public Object openConsole(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				String url = openConsole.run((String) param.get("VM_ID"));
				param.put("url", url);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);
	}

	@RequestMapping(value =  "/get_vm" )
	public Object getVm(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				getService().insertVMfromDC(param);
				return new HashMap();
			}
		}).run(request);
	}

	@RequestMapping(value =  "/recreate" )
	public Object recreate(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				if(getService().isAdmin()) {
					getService().updateRecreate(param);
					return new HashMap();
				}
				else {
					throw new Exception("No right");
				}
			}
		}).run(request);
	}
	@RequestMapping(value =  "/guageData" )
	public Object getGuageData(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				if(param.get("VM_ID") == null || "".equals(param.get("VM_ID")))
				{
					return new HashMap();
				}
				Map result = vmGauge.run((String)param.get("VM_ID"));
				return result;
			}
		}).run(request);


	}
}
