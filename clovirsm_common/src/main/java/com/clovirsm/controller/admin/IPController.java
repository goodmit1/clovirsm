package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.batch.ScheduleService;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.executor.InsertAllIp;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/ip" )
public class IPController  extends DefaultController {

	@Autowired IPService service; 
	@Autowired InsertAllIp insertAllIp;
	@Override
	protected DefaultService getService() {
		return service;
	}
	
	
	/**
	 * 동기화
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value =  "/sync" )
	public Map sync(final HttpServletRequest request, HttpServletResponse response) throws Exception {
	 
		insertAllIp.runAll(false);
		
		return ControllerUtil.getSuccessMap(new HashMap());
	}
	
	@RequestMapping(value =  "/insert" )
	public Map insert(final HttpServletRequest request, HttpServletResponse response) throws Exception {
	 
		
		String ips = request.getParameter("ips");
		String[] ipArr = ips.split(",");
		for(String ip:ipArr) {
			service.insertAllIp("", ip );
		}
		insertAllIp.runAll(false);
		return ControllerUtil.getSuccessMap(new HashMap());
	}
}
