package com.clovirsm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.DiskTypeMngService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/disk_type_mng" )
public class DiskTypeMngController extends DefaultController {

	@Autowired DiskTypeMngService service;
	@Override
	protected DiskTypeMngService getService() {
		return service;
	}

}