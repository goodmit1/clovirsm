package com.clovirsm.controller.admin;

import com.clovirsm.cm.sys.git.GitLabAPI;
import com.clovirsm.service.admin.CategoryMapService;
import com.clovirsm.service.admin.EtcConnMngService;
import com.clovirsm.service.admin.SwService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fw.runtime.util.PropertyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;

@RestController
@RequestMapping(value =  "/sw" )
public class SwController extends DefaultController {

	@Autowired SwService service;
	@Override
	protected SwService getService() {
		 
		return service;
	}

	@RequestMapping(value =  "/codeGitImport" )
	public String codeGitImport(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (String)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				GitLabAPI git = getGitApi();
				if(git==null) {
					return "";
				}
				 
				String result = "";
				try {
					result = git.getFileContent(git.getUserId()  , git.getRepo(), "master",  param.get("name").toString());
				} catch (Exception e) {
					 
				}
				return result;
			}
		}).run(request);
	}
	
	@RequestMapping(value =  "/codeLocalImport" )
	public String codeLocalImport(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (String)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				String result = "";
				String[] plusPath = String.valueOf(param.get("name")).split("/");
				String path = "";
				File inFile = null;
				if(plusPath.length == 3)
					path = PropertyManager.getString("local_shell_path")+"/"+plusPath[1];
				else
					path = PropertyManager.getString("local_shell_path");		
				
				if(plusPath.length == 3)
					inFile = new File(path ,plusPath[2]);
				else
					inFile = new File(path,(String) param.get("name"));
				 BufferedReader br = null;
			        try {
			            br = new BufferedReader(new FileReader(inFile));
			            String line;
			            while ((line = br.readLine()) != null) {
			            	result += line + '\n';
			            }
			        } catch (FileNotFoundException e) {
			            e.printStackTrace();
			        } catch (IOException e) {
			            e.printStackTrace();
			        }finally {
			            if(br != null) try {br.close(); } catch (IOException e) {}
			        }

				return result;
			}
		}).run(request);
	}
	
	@Autowired EtcConnMngService connService;
	
	protected GitLabAPI getGitApi() throws Exception {
		Map info = connService.getConnInfo("GIT");
		if(info == null) return null;
		GitLabAPI git = new GitLabAPI();
		git.setUrl((String)info.get("CONN_URL"));
        git.setToken((String)info.get("CONN_PWD"));
        git.setUserId( (String)info.get("CONN_USERID"));
        git.setRepo((String)info.get("CONN_PROP"));
        			
		return git;
	}
	
	@RequestMapping(value =  "/codeGitSave" )
	public String codeGitSave(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (String)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				GitLabAPI git = getGitApi();
				 
				 String result = "0";		
				try {
					if(git != null) {
						git.saveFile( git.getUserId(), git.getRepo(), "master",param.get("name").toString() , param.get("content").toString(),param.get("commitMsg").toString());
					}
					String[] plusPath = String.valueOf(param.get("name")).split("/");
					String path = "";
					if(plusPath.length == 3)
						path = PropertyManager.getString("local_shell_path")+"/"+plusPath[1];
					else
						path = PropertyManager.getString("local_shell_path");
					File Folder = new File(path);
					if (!Folder.exists()) {
						try{
						    Folder.mkdir(); //폴더 생성합니다.
						    System.out.println("폴더가 생성되었습니다.");
					    }
						catch(Exception e){
							    e.getStackTrace();
						}         
					}
					String fileName = path +"/"+ param.get("name");
					if(plusPath.length == 3) {
						fileName = path +"/"+plusPath[2];
					}
					File orgFile = new File(fileName);
					if(git==null && orgFile.exists()) { // git이 없는 경우 file backup
						
						File backupDir = new File(orgFile.getParent(), "backup") ;
						if(!backupDir.exists()) {
							backupDir.mkdir();
						}
						File dest = new File(backupDir.getPath(), orgFile.getName() + "-" + System.nanoTime());
						orgFile.renameTo(dest);
					}
					OutputStream output = null;
					output = new FileOutputStream(fileName);
				    String str =param.get("content").toString();
				    byte[] by=str.getBytes();
				    try {
				    	output.write(by);
				    	result = "1";
					} catch (Exception e) {
						result = "0";
						// TODO: handle exception
					}
					return result;
				} catch (Exception e) {
					result = "0";
					e.printStackTrace();
					// TODO: handle exception
				}
				return result;
			}
		}).run(request);
	}
	
	@Autowired CategoryMapService categoryMapService;
	@RequestMapping(value =  "/swSave" )
	public int swSave(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (int)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				int result = 0;
				if(param.get("SW_ID") == null) {
					param.put("SW_ID", "" + System.nanoTime());
					result = service.insert(param);
				} else {
					result = service.update(param);
					
				}
				categoryMapService.insert((String)param.get("SW_ID"), param.get("CATEGORY_IDS"), "S",null);
				 
				
				return result;
			}
		}).run(request);
	}
}
