-- Object Property JSON, Tag
create table nc_vm_usage_log (
                           VM_NM varchar(200) not null,
                           PRIVATE_IP varchar(40) not null,
                           LOG_USER varchar(50) not null,
                           LOG_IP varchar(40) not null,
                           START_DATE timestamp not null default CURRENT_TIMESTAMP,
                           END_DATE timestamp null,
                           primary key(PRIVATE_IP, START_DATE, LOG_USER)
);
