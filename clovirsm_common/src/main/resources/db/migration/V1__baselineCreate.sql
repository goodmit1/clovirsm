/* 사용방법
local 생성할 때 flyway로 V1 migrate할 경우 시간이 오래걸리기 때문에 해당 쿼리 복사 후 스크립트 붙여넣기 하신다음 쿼리 실행시키시면 됩니다.
그 이후 local에서 baseline 생성 후 V2부터 migrate 하시면 됩니다.
*/
/* User생성 및 권한 추가
   사용자 없는 경우 추가하시면 됩니다.
create user 'goodmit'@'%' identified by 'git3775*';
grant all on *.* to 'goodmit'@'%';
flush privileges;
 */
/***********************************************TABLE생성*****************************************************************************/

-- before_cidr definition

CREATE TABLE `before_cidr` (
                               `login_id` varchar(100) COLLATE utf8_bin DEFAULT NULL,
                               `user_id` int(11) DEFAULT NULL,
                               `team_cd` varchar(100) COLLATE utf8_bin DEFAULT NULL,
                               `user_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
                               `cidr_after` varchar(500) COLLATE utf8_bin DEFAULT NULL,
                               `cidr_before` varchar(500) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- fm_cd definition

CREATE TABLE `fm_cd` (
                         `GRP_ID` varchar(10) NOT NULL,
                         `CD_ID` varchar(45) NOT NULL,
                         `CD_DESC` varchar(100) DEFAULT NULL,
                         `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `INS_PGM` varchar(30) DEFAULT NULL,
                         `INS_ID` varchar(10) DEFAULT NULL,
                         `INS_IP` varchar(40) DEFAULT NULL,
                         `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `UPD_PGM` varchar(30) DEFAULT NULL,
                         `UPD_ID` varchar(10) DEFAULT NULL,
                         `UPD_IP` varchar(40) DEFAULT NULL,
                         `ODR` decimal(22,0) DEFAULT NULL,
                         PRIMARY KEY (`GRP_ID`,`CD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='코드';


-- fm_cd_grp definition

CREATE TABLE `fm_cd_grp` (
                             `GRP_ID` varchar(10) NOT NULL,
                             `GRP_NM` varchar(100) DEFAULT NULL,
                             `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `INS_PGM` varchar(30) DEFAULT NULL,
                             `INS_ID` varchar(10) DEFAULT NULL,
                             `INS_IP` varchar(40) DEFAULT NULL,
                             `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `UPD_PGM` varchar(30) DEFAULT NULL,
                             `UPD_ID` varchar(10) DEFAULT NULL,
                             `UPD_IP` varchar(40) DEFAULT NULL,
                             PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_cd_nl definition

CREATE TABLE `fm_cd_nl` (
                            `GRP_ID` varchar(10) NOT NULL,
                            `CD_ID` varchar(45) NOT NULL,
                            `LANG_TYPE` varchar(10) NOT NULL,
                            `CD_NM` varchar(100) DEFAULT NULL,
                            `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `INS_PGM` varchar(30) DEFAULT NULL,
                            `INS_ID` varchar(10) DEFAULT NULL,
                            `INS_IP` varchar(40) DEFAULT NULL,
                            `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `UPD_PGM` varchar(30) DEFAULT NULL,
                            `UPD_ID` varchar(10) DEFAULT NULL,
                            `UPD_IP` varchar(40) DEFAULT NULL,
                            PRIMARY KEY (`GRP_ID`,`CD_ID`,`LANG_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_company definition

CREATE TABLE `fm_company` (
                              `COMP_ID` varchar(20) NOT NULL,
                              `ADMIN_ID` decimal(22,0) DEFAULT NULL,
                              `COMP_KUBUN_CD` varchar(1) DEFAULT NULL,
                              `COMP_NM` varchar(200) DEFAULT NULL,
                              `CEO_NM` varchar(200) DEFAULT NULL,
                              `COMP_REG_NO` varchar(12) DEFAULT NULL,
                              `COR_NO` varchar(13) DEFAULT NULL,
                              `BIZ_CD` varchar(10) DEFAULT NULL,
                              `ITEM_CD` varchar(10) DEFAULT NULL,
                              `PAY_EMAIL` varchar(100) DEFAULT NULL,
                              `ZIP_NO` varchar(7) DEFAULT NULL,
                              `ADDR1` varchar(200) DEFAULT NULL,
                              `ADDR2` varchar(500) DEFAULT NULL,
                              `MOBILE_NO` varchar(100) DEFAULT NULL,
                              `OFFICE_NO` varchar(15) DEFAULT NULL,
                              `PAY_KUBUN_CD` varchar(1) DEFAULT NULL,
                              `BANK_CARD_CD` varchar(10) DEFAULT NULL,
                              `ACC_CARD_NO` varchar(100) DEFAULT NULL,
                              `ACC_CARD_OWNER` varchar(200) DEFAULT NULL,
                              `EXPIRE_MM` varchar(6) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_ID` varchar(10) DEFAULT NULL,
                              `UPD_PGM` varchar(30) DEFAULT NULL,
                              `UPD_IP` varchar(40) DEFAULT NULL,
                              `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `DISCOUNT_RATE` decimal(22,0) DEFAULT NULL,
                              PRIMARY KEY (`COMP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='회사';


-- fm_connectlog definition

CREATE TABLE `fm_connectlog` (
                                 `PRGM_ID` varchar(12) DEFAULT NULL,
                                 `PRGM_NM` varchar(50) DEFAULT NULL,
                                 `USER_ID` varchar(20) DEFAULT NULL,
                                 `USER_NAME` varchar(50) DEFAULT NULL,
                                 `TEAM_CD` varchar(20) DEFAULT NULL,
                                 `TEAM_NM` varchar(100) DEFAULT NULL,
                                 `CONNECTTIME` date DEFAULT NULL,
                                 `IPADDRESS` varchar(30) DEFAULT NULL,
                                 `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `INS_PGM` varchar(30) DEFAULT NULL,
                                 `INS_ID` varchar(10) DEFAULT NULL,
                                 `INS_IP` varchar(40) DEFAULT NULL,
                                 `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `UPD_PGM` varchar(30) DEFAULT NULL,
                                 `UPD_ID` varchar(10) DEFAULT NULL,
                                 `UPD_IP` varchar(40) DEFAULT NULL,
                                 `SOC_CD` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='접속정보';


-- fm_dashboard definition

CREATE TABLE `fm_dashboard` (
                                `ITEM_ID` decimal(22,0) NOT NULL,
                                `ITEM_NM` varchar(30) DEFAULT NULL,
                                `ITEM_PATH` varchar(100) DEFAULT NULL,
                                `ADMIN_YN` varchar(1) DEFAULT NULL,
                                `SEQ` decimal(22,0) DEFAULT NULL,
                                `COL` decimal(22,0) DEFAULT '4',
                                `ADMIN_SEQ` decimal(3,0) DEFAULT NULL,
                                PRIMARY KEY (`ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='대쉬보드 ';


-- fm_dashboard_mobis definition

CREATE TABLE `fm_dashboard_mobis` (
                                      `ITEM_ID` decimal(22,0) NOT NULL,
                                      `ITEM_NM` varchar(30) DEFAULT NULL,
                                      `ITEM_PATH` varchar(100) DEFAULT NULL,
                                      `ADMIN_YN` varchar(1) DEFAULT NULL,
                                      `SEQ` decimal(22,0) DEFAULT NULL,
                                      `COL` decimal(22,0) DEFAULT '4',
                                      `ADMIN_SEQ` decimal(3,0) DEFAULT NULL,
                                      PRIMARY KEY (`ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_dashboard_user definition

CREATE TABLE `fm_dashboard_user` (
                                     `ITEM_ID` decimal(22,0) NOT NULL,
                                     `USER_ID` decimal(22,0) NOT NULL,
                                     `SEQ` decimal(22,0) DEFAULT NULL,
                                     `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                     `TITLE` varchar(100) DEFAULT NULL,
                                     `PATH` varchar(200) DEFAULT NULL,
                                     `COL` decimal(1,0) DEFAULT NULL,
                                     PRIMARY KEY (`ITEM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_ddic definition

CREATE TABLE `fm_ddic` (
                           `DD_ID` varchar(20) NOT NULL,
                           `DD_NM` varchar(100) DEFAULT NULL,
                           `DD_VALUE` varchar(30) NOT NULL,
                           `KO_DD_DESC` varchar(100) DEFAULT NULL,
                           `EN_DD_DESC` varchar(100) DEFAULT NULL,
                           `SORT` decimal(22,0) DEFAULT NULL,
                           `USE_YN` varchar(1) DEFAULT 'Y',
                           `EXT_DD_ID` varchar(20) DEFAULT NULL,
                           `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `INS_PGM` varchar(30) DEFAULT NULL,
                           `INS_ID` varchar(10) DEFAULT NULL,
                           `INS_IP` varchar(40) DEFAULT NULL,
                           `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `UPD_PGM` varchar(30) DEFAULT NULL,
                           `UPD_ID` varchar(10) DEFAULT NULL,
                           `UPD_IP` varchar(40) DEFAULT NULL,
                           PRIMARY KEY (`DD_ID`,`DD_VALUE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='코드 관리';


-- fm_field definition

CREATE TABLE `fm_field` (
                            `FIELD_ID` decimal(16,0) NOT NULL,
                            `TABLE_NM` varchar(30) NOT NULL,
                            `DATA_TYPE` varchar(10) DEFAULT NULL,
                            `INPUT_TYPE` varchar(30) DEFAULT NULL,
                            `INPUT_PROP` varchar(500) DEFAULT NULL,
                            `FIELD_DESC1` varchar(500) DEFAULT NULL,
                            `DEFAULT_VAL` varchar(45) DEFAULT NULL,
                            `REQUIRED_YN` varchar(1) DEFAULT NULL,
                            `PK_YN` char(1) DEFAULT 'N',
                            `DB_FIELD_NM` varchar(30) DEFAULT 'Y',
                            `PARAM_ID` varchar(45) DEFAULT NULL,
                            `TITLE1` varchar(300) DEFAULT NULL,
                            `TITLE2` varchar(100) DEFAULT NULL,
                            `TITLE3` varchar(100) DEFAULT NULL,
                            `PK_SEQ` decimal(10,0) DEFAULT NULL,
                            `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `INS_PGM` varchar(30) DEFAULT NULL,
                            `INS_ID` varchar(10) DEFAULT NULL,
                            `INS_IP` varchar(40) DEFAULT NULL,
                            `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `UPD_PGM` varchar(30) DEFAULT NULL,
                            `UPD_ID` varchar(10) DEFAULT NULL,
                            `UPD_IP` varchar(40) DEFAULT NULL,
                            `DATA_SIZE` decimal(22,0) DEFAULT NULL,
                            `VALID_REG_EXP` varchar(200) DEFAULT NULL,
                            `FIELD_DESC2` varchar(500) DEFAULT NULL,
                            `FIELD_DESC3` varchar(500) DEFAULT NULL,
                            `DB_DATA_TYPE` varchar(40) DEFAULT NULL,
                            `AUTOCOMPLETE_YN` char(1) DEFAULT 'N',
                            PRIMARY KEY (`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_file_attach definition

CREATE TABLE `fm_file_attach` (
                                  `TABLE_NM` varchar(30) NOT NULL,
                                  `ID` varchar(100) NOT NULL,
                                  `SEQ` decimal(2,0) NOT NULL,
                                  `FILE_NM` varchar(200) DEFAULT NULL,
                                  `FILE_SIZE` decimal(10,0) DEFAULT NULL,
                                  `FILE_CONTENT` longblob,
                                  PRIMARY KEY (`TABLE_NM`,`ID`,`SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='파일첨부';


-- fm_inquiry definition

CREATE TABLE `fm_inquiry` (
                              `INQ_ID` varchar(20) NOT NULL,
                              `INQ_TITLE` varchar(200) DEFAULT NULL,
                              `INQUIRY` longtext,
                              `ANSWER` longtext,
                              `SVC_CD` varchar(10) DEFAULT NULL,
                              `INQ_CD` varchar(10) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_ID` varchar(10) DEFAULT NULL,
                              `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_IP` varchar(40) DEFAULT NULL,
                              `READ_YN` char(1) NOT NULL DEFAULT 'N',
                              `READ_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `ANSWER_YN` char(1) DEFAULT 'N',
                              PRIMARY KEY (`INQ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Q&A';


-- fm_menu definition

CREATE TABLE `fm_menu` (
                           `ID` decimal(16,0) NOT NULL,
                           `PRGM_ID` decimal(16,0) NOT NULL DEFAULT '0',
                           `MENU_NM` varchar(50) DEFAULT NULL,
                           `PARENT_ID` decimal(22,0) DEFAULT NULL,
                           `THEME` varchar(20) DEFAULT NULL,
                           `MENU_DESC` varchar(500) DEFAULT NULL,
                           `MENU_ORDER` decimal(22,0) DEFAULT NULL,
                           `USE_YN` varchar(1) DEFAULT 'Y',
                           `ICON` varchar(200) DEFAULT NULL,
                           `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `INS_PGM` varchar(30) DEFAULT NULL,
                           `INS_ID` varchar(10) DEFAULT NULL,
                           `INS_IP` varchar(40) DEFAULT NULL,
                           `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `UPD_PGM` varchar(30) DEFAULT NULL,
                           `UPD_ID` varchar(10) DEFAULT NULL,
                           `UPD_IP` varchar(40) DEFAULT NULL,
                           `IS_POPUP` varchar(1) DEFAULT 'N',
                           `MENU_NM_EN` varchar(50) DEFAULT NULL,
                           PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='메뉴';


-- fm_menu_mobis definition

CREATE TABLE `fm_menu_mobis` (
                                 `ID` decimal(16,0) NOT NULL,
                                 `PRGM_ID` decimal(16,0) NOT NULL DEFAULT '0',
                                 `MENU_NM` varchar(50) DEFAULT NULL,
                                 `PARENT_ID` decimal(22,0) DEFAULT NULL,
                                 `THEME` varchar(20) DEFAULT NULL,
                                 `MENU_DESC` varchar(500) DEFAULT NULL,
                                 `MENU_ORDER` decimal(22,0) DEFAULT NULL,
                                 `USE_YN` varchar(1) DEFAULT 'Y',
                                 `ICON` varchar(200) DEFAULT NULL,
                                 `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `INS_PGM` varchar(30) DEFAULT NULL,
                                 `INS_ID` varchar(10) DEFAULT NULL,
                                 `INS_IP` varchar(40) DEFAULT NULL,
                                 `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `UPD_PGM` varchar(30) DEFAULT NULL,
                                 `UPD_ID` varchar(10) DEFAULT NULL,
                                 `UPD_IP` varchar(40) DEFAULT NULL,
                                 `IS_POPUP` varchar(1) DEFAULT 'N',
                                 `MENU_NM_EN` varchar(50) DEFAULT NULL,
                                 PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_my_menu definition

CREATE TABLE `fm_my_menu` (
                              `MENU_ID` decimal(16,0) NOT NULL,
                              `USER_ID` varchar(50) NOT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_PGM` varchar(30) DEFAULT NULL,
                              `UPD_ID` varchar(10) DEFAULT NULL,
                              `UPD_IP` varchar(40) DEFAULT NULL,
                              `FOLDER_ID` decimal(15,0) DEFAULT NULL,
                              PRIMARY KEY (`MENU_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='마이메뉴';


-- fm_nl definition

CREATE TABLE `fm_nl` (
                         `TABLE_NM` varchar(30) NOT NULL,
                         `FIELD_NM` varchar(30) NOT NULL,
                         `ID` varchar(100) NOT NULL,
                         `LANG_TYPE` varchar(10) NOT NULL,
                         `NM` varchar(200) DEFAULT NULL,
                         PRIMARY KEY (`TABLE_NM`,`FIELD_NM`,`ID`,`LANG_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='다국어';


-- fm_notice definition

CREATE TABLE `fm_notice` (
                             `ID` decimal(22,0) NOT NULL,
                             `TITLE` varchar(200) NOT NULL,
                             `CONTENTS` longtext,
                             `START_DATE` timestamp NULL DEFAULT NULL,
                             `END_DATE` timestamp NULL DEFAULT NULL,
                             `HITS` decimal(22,0) NOT NULL DEFAULT '0',
                             `PRIORITY` decimal(22,0) DEFAULT NULL,
                             `TOP_YN` char(1) NOT NULL DEFAULT 'N',
                             `DEL_YN` char(1) NOT NULL DEFAULT 'N',
                             `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `INS_PGM` varchar(30) NOT NULL,
                             `INS_ID` varchar(10) NOT NULL,
                             `INS_IP` varchar(40) NOT NULL,
                             `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `UPD_PGM` varchar(30) DEFAULT NULL,
                             `UPD_ID` varchar(30) DEFAULT NULL,
                             `UPD_IP` varchar(40) DEFAULT NULL,
                             `CATEGORY` varchar(10) DEFAULT 'NOTICE',
                             PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='공지사항';


-- fm_notice_trg definition

CREATE TABLE `fm_notice_trg` (
                                 `ID` decimal(22,0) NOT NULL,
                                 `TRG_ID` varchar(20) NOT NULL,
                                 `TRG_TYPE` varchar(1) DEFAULT 'T',
                                 PRIMARY KEY (`ID`,`TRG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_personal definition

CREATE TABLE `fm_personal` (
                               `USER_ID` varchar(10) NOT NULL,
                               `PRGM_ID` decimal(15,0) NOT NULL,
                               `GRID_NM` varchar(50) NOT NULL,
                               `HIDDEN_COLUMNS` varchar(500) DEFAULT NULL,
                               `ROW_CNT` decimal(3,0) DEFAULT NULL,
                               `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                               `INS_IP` varchar(40) DEFAULT NULL,
                               PRIMARY KEY (`USER_ID`,`PRGM_ID`,`GRID_NM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_prgm definition

CREATE TABLE `fm_prgm` (
                           `PRGM_ID` decimal(16,0) NOT NULL,
                           `SYSTEM_CODE` int(11) NOT NULL DEFAULT '0' COMMENT '시스템코드',
                           `PRGM_NM` varchar(50) DEFAULT NULL,
                           `PRGM_DESC` varchar(500) DEFAULT NULL,
                           `USE_YN` varchar(1) DEFAULT NULL,
                           `PRGM_PATH` varchar(200) DEFAULT NULL,
                           `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `INS_PGM` varchar(30) DEFAULT NULL,
                           `INS_ID` varchar(10) DEFAULT NULL,
                           `INS_IP` varchar(40) DEFAULT NULL,
                           `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `UPD_PGM` varchar(30) DEFAULT NULL,
                           `UPD_ID` varchar(10) DEFAULT NULL,
                           `UPD_IP` varchar(40) DEFAULT NULL,
                           `PKG_NM` varchar(60) DEFAULT NULL,
                           `QUERY_GEN_KUBUN` varchar(10) DEFAULT NULL,
                           `PAGE_ID` varchar(50) DEFAULT NULL,
                           `TMPL_ID` decimal(22,0) DEFAULT NULL,
                           `INTERNAL_ONLY` varchar(1) DEFAULT NULL,
                           `MOBILE_YN` varchar(1) DEFAULT NULL,
                           `COMMON_YN` varchar(1) DEFAULT NULL,
                           `MANUAL_PATH` varchar(150) DEFAULT NULL,
                           `PROCEDURE_SCHEMA` varchar(50) DEFAULT NULL,
                           PRIMARY KEY (`PRGM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='프로그램(메뉴)';


-- fm_prgm_mobis definition

CREATE TABLE `fm_prgm_mobis` (
                                 `PRGM_ID` decimal(16,0) NOT NULL,
                                 `SYSTEM_CODE` int(11) NOT NULL DEFAULT '0' COMMENT '시스템코드',
                                 `PRGM_NM` varchar(50) DEFAULT NULL,
                                 `PRGM_DESC` varchar(500) DEFAULT NULL,
                                 `USE_YN` varchar(1) DEFAULT NULL,
                                 `PRGM_PATH` varchar(200) DEFAULT NULL,
                                 `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `INS_PGM` varchar(30) DEFAULT NULL,
                                 `INS_ID` varchar(10) DEFAULT NULL,
                                 `INS_IP` varchar(40) DEFAULT NULL,
                                 `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `UPD_PGM` varchar(30) DEFAULT NULL,
                                 `UPD_ID` varchar(10) DEFAULT NULL,
                                 `UPD_IP` varchar(40) DEFAULT NULL,
                                 `PKG_NM` varchar(60) DEFAULT NULL,
                                 `QUERY_GEN_KUBUN` varchar(10) DEFAULT NULL,
                                 `PAGE_ID` varchar(50) DEFAULT NULL,
                                 `TMPL_ID` decimal(22,0) DEFAULT NULL,
                                 `INTERNAL_ONLY` varchar(1) DEFAULT NULL,
                                 `MOBILE_YN` varchar(1) DEFAULT NULL,
                                 `COMMON_YN` varchar(1) DEFAULT NULL,
                                 `MANUAL_PATH` varchar(150) DEFAULT NULL,
                                 `PROCEDURE_SCHEMA` varchar(50) DEFAULT NULL,
                                 PRIMARY KEY (`PRGM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_role definition

CREATE TABLE `fm_role` (
                           `ROLE_ID` varchar(10) NOT NULL,
                           `ROLE_NM` varchar(30) DEFAULT NULL,
                           `ROLE_DESC` varchar(500) DEFAULT NULL,
                           `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `INS_PGM` varchar(30) DEFAULT NULL,
                           `INS_ID` varchar(100) DEFAULT NULL,
                           `INS_IP` varchar(40) DEFAULT NULL,
                           `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `UPD_PGM` varchar(30) DEFAULT NULL,
                           `UPD_ID` varchar(10) DEFAULT NULL,
                           `UPD_IP` varchar(40) DEFAULT NULL,
                           PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='사용자권한';


-- fm_role_prgm definition

CREATE TABLE `fm_role_prgm` (
                                `PRGM_ID` decimal(16,0) NOT NULL,
                                `ROLE_ID` varchar(10) NOT NULL,
                                `SELECT_YN` varchar(1) DEFAULT NULL,
                                `UPDATE_YN` varchar(1) DEFAULT NULL,
                                `DELETE_YN` varchar(1) DEFAULT NULL,
                                `INSERT_YN` varchar(1) DEFAULT NULL,
                                `PRINT_YN` varchar(1) DEFAULT NULL,
                                `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `INS_PGM` varchar(30) DEFAULT NULL,
                                `INS_ID` varchar(10) DEFAULT NULL,
                                `INS_IP` varchar(40) DEFAULT NULL,
                                `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `UPD_PGM` varchar(30) DEFAULT NULL,
                                `UPD_ID` varchar(10) DEFAULT NULL,
                                `UPD_IP` varchar(40) DEFAULT NULL,
                                PRIMARY KEY (`ROLE_ID`,`PRGM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='메뉴별 권한';


-- fm_role_prgm_mobis definition

CREATE TABLE `fm_role_prgm_mobis` (
                                      `PRGM_ID` decimal(16,0) NOT NULL,
                                      `ROLE_ID` varchar(10) NOT NULL,
                                      `SELECT_YN` varchar(1) DEFAULT NULL,
                                      `UPDATE_YN` varchar(1) DEFAULT NULL,
                                      `DELETE_YN` varchar(1) DEFAULT NULL,
                                      `INSERT_YN` varchar(1) DEFAULT NULL,
                                      `PRINT_YN` varchar(1) DEFAULT NULL,
                                      `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      `INS_PGM` varchar(30) DEFAULT NULL,
                                      `INS_ID` varchar(10) DEFAULT NULL,
                                      `INS_IP` varchar(40) DEFAULT NULL,
                                      `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      `UPD_PGM` varchar(30) DEFAULT NULL,
                                      `UPD_ID` varchar(10) DEFAULT NULL,
                                      `UPD_IP` varchar(40) DEFAULT NULL,
                                      PRIMARY KEY (`ROLE_ID`,`PRGM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_seq definition

CREATE TABLE `fm_seq` (
                          `KUBUN` varchar(20) NOT NULL,
                          `SEQ` decimal(22,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_table definition

CREATE TABLE `fm_table` (
                            `TABLE_NM` varchar(50) NOT NULL,
                            `PAR_TABLE_NM` varchar(50) DEFAULT NULL,
                            `TABLE_TITLE` varchar(100) DEFAULT NULL,
                            `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `INS_PGM` varchar(30) DEFAULT NULL,
                            `INS_ID` varchar(10) DEFAULT NULL,
                            `INS_IP` varchar(40) DEFAULT NULL,
                            `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `UPD_PGM` varchar(30) DEFAULT NULL,
                            `UPD_ID` varchar(10) DEFAULT NULL,
                            `UPD_IP` varchar(40) DEFAULT NULL,
                            `REAL_TABLE_NM` varchar(30) NOT NULL,
                            PRIMARY KEY (`TABLE_NM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- fm_team definition

CREATE TABLE `fm_team` (
                           `TEAM_CD` varchar(10) NOT NULL,
                           `COMP_ID` varchar(20) DEFAULT NULL,
                           `TEAM_NM` varchar(100) DEFAULT NULL,
                           `USE_YN` varchar(1) NOT NULL DEFAULT 'Y',
                           `TEAM_OWNER` varchar(50) DEFAULT NULL,
                           `PARENT_CD` varchar(10) DEFAULT NULL,
                           `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `INS_PGM` varchar(30) DEFAULT NULL,
                           `INS_ID` varchar(10) DEFAULT NULL,
                           `INS_IP` varchar(40) DEFAULT NULL,
                           `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `UPD_PGM` varchar(30) DEFAULT NULL,
                           `UPD_ID` varchar(10) DEFAULT NULL,
                           `UPD_IP` varchar(40) DEFAULT NULL,
                           `ETC_CD` varchar(30) DEFAULT NULL,
                           PRIMARY KEY (`TEAM_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='팀';


-- fm_team_role definition

CREATE TABLE `fm_team_role` (
                                `TEAM_CD` varchar(10) NOT NULL,
                                `ROLE_ID` varchar(10) NOT NULL,
                                `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `INS_PGM` varchar(30) DEFAULT NULL,
                                `INS_ID` varchar(10) DEFAULT NULL,
                                `INS_IP` varchar(40) DEFAULT NULL,
                                `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `UPD_PGM` varchar(30) DEFAULT NULL,
                                `UPD_ID` varchar(10) DEFAULT NULL,
                                `UPD_IP` varchar(40) DEFAULT NULL,
                                PRIMARY KEY (`TEAM_CD`,`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='팀 권한';


-- fm_user definition

CREATE TABLE `fm_user` (
                           `LOGIN_ID` varchar(50) NOT NULL,
                           `USER_ID` decimal(22,0) NOT NULL,
                           `PASSWORD` varchar(150) DEFAULT NULL,
                           `USER_NAME` varchar(50) NOT NULL,
                           `OFFICE_NO` varchar(15) DEFAULT NULL,
                           `MOBILE_NO` varchar(50) DEFAULT NULL,
                           `EMAIL` varchar(100) DEFAULT NULL,
                           `TEAM_CD` varchar(10) DEFAULT NULL,
                           `USE_YN` varchar(1) NOT NULL DEFAULT 'Y',
                           `USER_TYPE` varchar(3) NOT NULL DEFAULT '18',
                           `LOCALE` varchar(5) DEFAULT 'ko',
                           `LAST_ACCESS_TMS` timestamp NULL DEFAULT NULL,
                           `PWD_CHANGE_TMS` timestamp NULL DEFAULT NULL,
                           `LAST_LOGIN_IP` varchar(50) DEFAULT NULL,
                           `ACC_LOGIN_CNT` decimal(22,0) NOT NULL DEFAULT '0',
                           `AUTH_FAIL` decimal(22,0) DEFAULT NULL,
                           `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `INS_PGM` varchar(30) DEFAULT NULL,
                           `INS_ID` varchar(10) DEFAULT NULL,
                           `INS_IP` varchar(40) DEFAULT NULL,
                           `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `UPD_PGM` varchar(30) DEFAULT NULL,
                           `UPD_ID` varchar(10) DEFAULT NULL,
                           `UPD_IP` varchar(40) DEFAULT NULL,
                           `POSITION` varchar(100) DEFAULT NULL,
                           PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='사용자';


-- fm_user_prgm definition

CREATE TABLE `fm_user_prgm` (
                                `PRGM_ID` decimal(16,0) NOT NULL,
                                `USER_ID` varchar(20) NOT NULL,
                                `ROLE_ID` varchar(10) NOT NULL,
                                `SELECT_YN` varchar(1) DEFAULT NULL,
                                `UPDATE_YN` varchar(1) DEFAULT NULL,
                                `DELETE_YN` varchar(1) DEFAULT NULL,
                                `INSERT_YN` varchar(1) DEFAULT NULL,
                                `PRINT_YN` varchar(1) DEFAULT NULL,
                                `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `INS_PGM` varchar(30) DEFAULT NULL,
                                `INS_ID` varchar(10) DEFAULT NULL,
                                `INS_IP` varchar(40) DEFAULT NULL,
                                `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `UPD_PGM` varchar(30) DEFAULT NULL,
                                `UPD_ID` varchar(10) DEFAULT NULL,
                                `UPD_IP` varchar(40) DEFAULT NULL,
                                PRIMARY KEY (`USER_ID`,`PRGM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='사용자별 메뉴';


-- nc_alarm definition

CREATE TABLE `nc_alarm` (
                            `ALARM_ID` varchar(30) NOT NULL,
                            `ALARM_MSG` varchar(4000) DEFAULT NULL,
                            `ALARM_INS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `ALARM_TYPE` varchar(10) DEFAULT NULL,
                            `ALARM_SVC_ID` varchar(500) DEFAULT NULL,
                            `INS_ID` varchar(20) DEFAULT NULL,
                            `ALARM_TMS` timestamp NULL DEFAULT NULL,
                            PRIMARY KEY (`ALARM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_alarm_rcv definition

CREATE TABLE `nc_alarm_rcv` (
                                `ALARM_ID` varchar(30) NOT NULL,
                                `USER_ID` varchar(22) NOT NULL,
                                `VIEW_YN` char(1) DEFAULT 'N',
                                PRIMARY KEY (`ALARM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_biz_cd definition

CREATE TABLE `nc_biz_cd` (
                             `BIZ_CD` varchar(10) NOT NULL,
                             `PARENT_CD` varchar(10) DEFAULT NULL,
                             `BIZ_NM` varchar(100) NOT NULL,
                             `ORD` decimal(22,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_buy definition

CREATE TABLE `nc_buy` (
                          `BUY_ID` varchar(20) NOT NULL,
                          `GOODS_ID` varchar(20) DEFAULT NULL,
                          `OPTION_ID` varchar(20) DEFAULT NULL,
                          `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                          `TEAM_CD` varchar(10) NOT NULL,
                          `DEL_YN` varchar(1) DEFAULT NULL,
                          `INS_ID` varchar(10) DEFAULT NULL,
                          `UPD_ID` varchar(10) DEFAULT NULL,
                          `INS_PGM` varchar(30) DEFAULT NULL,
                          `UPD_PGM` varchar(30) DEFAULT NULL,
                          `INS_IP` varchar(40) DEFAULT NULL,
                          `UPD_IP` varchar(40) DEFAULT NULL,
                          `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `HH_FEE` decimal(22,0) DEFAULT NULL,
                          `DD_FEE` decimal(22,0) DEFAULT NULL,
                          `MM_FEE` decimal(22,0) DEFAULT NULL,
                          `INS_DT` varchar(14) DEFAULT NULL,
                          PRIMARY KEY (`BUY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_buy_req definition

CREATE TABLE `nc_buy_req` (
                              `BUY_ID` varchar(20) NOT NULL,
                              `GOODS_ID` varchar(20) DEFAULT NULL,
                              `OPTION_ID` varchar(20) DEFAULT NULL,
                              `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                              `TEAM_CD` varchar(10) NOT NULL,
                              `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                              `CUD_CD` varchar(1) DEFAULT NULL,
                              `DD_FEE` decimal(22,0) DEFAULT NULL,
                              `MM_FEE` decimal(22,0) DEFAULT NULL,
                              `HH_FEE` decimal(22,0) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `INS_DT` varchar(14) NOT NULL,
                              PRIMARY KEY (`BUY_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_category_map definition

CREATE TABLE `nc_category_map` (
                                   `ID` varchar(20) NOT NULL,
                                   `CATEGORY_ID` varchar(20) NOT NULL,
                                   `KUBUN` char(1) NOT NULL COMMENT 'C:카탈로그, U:사용자, P:용도',
                                   PRIMARY KEY (`ID`,`CATEGORY_ID`,`KUBUN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_comp_dc_nw definition

CREATE TABLE `nc_comp_dc_nw` (
                                 `COMP_ID` varchar(20) NOT NULL,
                                 `DC_ID` varchar(20) NOT NULL,
                                 `START_IP` varchar(40) NOT NULL,
                                 `CURR_IP` varchar(40) DEFAULT NULL,
                                 `TOTAL_CNT` decimal(22,0) DEFAULT NULL,
                                 `USE_CNT` decimal(22,0) DEFAULT NULL,
                                 `TEAM_CD` varchar(10) NOT NULL DEFAULT '0',
                                 `START_IP_NUM` decimal(10,0) DEFAULT NULL,
                                 PRIMARY KEY (`DC_ID`,`COMP_ID`,`TEAM_CD`,`START_IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_comp_dc_prop definition

CREATE TABLE `nc_comp_dc_prop` (
                                   `COMP_ID` varchar(20) NOT NULL,
                                   `DC_ID` varchar(20) NOT NULL,
                                   `PROP_NM` varchar(200) NOT NULL,
                                   `PROP_VAL` varchar(50) DEFAULT NULL,
                                   `TEAM_CD` varchar(10) NOT NULL,
                                   PRIMARY KEY (`COMP_ID`,`TEAM_CD`,`DC_ID`,`PROP_NM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_comp_nw_con definition

CREATE TABLE `nc_comp_nw_con` (
                                  `NW_CON_ID` varchar(20) NOT NULL,
                                  `COMP_ID` varchar(20) DEFAULT NULL,
                                  `DC_ID` varchar(20) DEFAULT NULL,
                                  `TRG_DC_ID` varchar(20) DEFAULT NULL,
                                  `START_IP` varchar(40) DEFAULT NULL,
                                  `TRG_START_IP` varchar(40) DEFAULT NULL,
                                  `INS_ID` varchar(10) DEFAULT NULL,
                                  `INS_PGM` varchar(30) DEFAULT NULL,
                                  `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `INS_IP` varchar(40) DEFAULT NULL,
                                  `UPD_ID` varchar(10) DEFAULT NULL,
                                  `UPD_PGM` varchar(30) DEFAULT NULL,
                                  `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `UPD_IP` varchar(40) DEFAULT NULL,
                                  `TASK_STATUS_CD` char(1) NOT NULL DEFAULT 'W',
                                  `FAIL_MSG` varchar(1000) DEFAULT NULL,
                                  `TASK_FINISH_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `WORKER_ID` varchar(10) DEFAULT NULL,
                                  `DEL_YN` varchar(1) DEFAULT NULL,
                                  `INS_DT` varchar(14) DEFAULT NULL,
                                  PRIMARY KEY (`NW_CON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_comp_nw_con_req definition

CREATE TABLE `nc_comp_nw_con_req` (
                                      `NW_CON_ID` varchar(20) NOT NULL,
                                      `INS_DT` varchar(14) NOT NULL,
                                      `COMP_ID` varchar(20) DEFAULT NULL,
                                      `DC_ID` varchar(20) DEFAULT NULL,
                                      `START_IP` varchar(40) DEFAULT NULL,
                                      `TRG_START_IP` varchar(40) DEFAULT NULL,
                                      `TRG_DC_ID` varchar(20) DEFAULT NULL,
                                      `CUD_CD` varchar(1) DEFAULT NULL,
                                      `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                                      `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      `INS_ID` varchar(10) DEFAULT NULL,
                                      `INS_IP` varchar(40) DEFAULT NULL,
                                      `INS_PGM` varchar(30) DEFAULT NULL,
                                      PRIMARY KEY (`NW_CON_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_dc definition

CREATE TABLE `nc_dc` (
                         `DC_ID` varchar(20) NOT NULL,
                         `DC_NM` varchar(200) DEFAULT NULL,
                         `HV_CD` varchar(10) DEFAULT NULL,
                         `CMT` varchar(1000) DEFAULT NULL,
                         `CONN_URL` varchar(40) DEFAULT NULL,
                         `CONN_USERID` varchar(100) DEFAULT NULL,
                         `CONN_PWD` varchar(4000) DEFAULT NULL,
                         `DEL_YN` varchar(1) DEFAULT NULL,
                         `INS_ID` varchar(10) DEFAULT NULL,
                         `INS_PGM` varchar(30) DEFAULT NULL,
                         `INS_IP` varchar(40) DEFAULT NULL,
                         `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `UPD_ID` varchar(10) DEFAULT NULL,
                         `UPD_PGM` varchar(30) DEFAULT NULL,
                         `UPD_IP` varchar(40) DEFAULT NULL,
                         `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `START_IP` varchar(40) DEFAULT NULL,
                         `CURR_IP` varchar(40) DEFAULT NULL,
                         `NW_AUTO_YN` varchar(1) DEFAULT NULL,
                         `REAL_DC_NM` varchar(200) DEFAULT NULL,
                         `GUEST_CONN_PWD` varchar(100) DEFAULT NULL,
                         `GUEST_CONN_USERID` varchar(100) DEFAULT NULL,
                         `USE_RP_YN` char(1) DEFAULT 'N',
                         `USE_VLAN_YN` char(1) DEFAULT 'N',
                         `IMG_DISK_TYPE_ID` varchar(20) NOT NULL,
                         `INIT_POOL_SIZE` int(11) DEFAULT '0',
                         `IP_AUTO_YN` varchar(1) DEFAULT 'N',
                         `CONN_PROP` varchar(500) DEFAULT NULL,
                         PRIMARY KEY (`DC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='데이터센터 정보';


-- nc_dc_disk_type definition

CREATE TABLE `nc_dc_disk_type` (
                                   `DISK_TYPE_ID` varchar(20) NOT NULL,
                                   `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                                   `DS_NM` varchar(200) NOT NULL,
                                   `DC_ID` varchar(20) NOT NULL,
                                   `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   `INS_PGM` varchar(30) DEFAULT NULL,
                                   `UPD_PGM` varchar(30) DEFAULT NULL,
                                   `INS_IP` varchar(40) DEFAULT NULL,
                                   `UPD_IP` varchar(40) DEFAULT NULL,
                                   `INS_ID` varchar(10) DEFAULT NULL,
                                   `UPD_ID` varchar(10) DEFAULT NULL,
                                   PRIMARY KEY (`DISK_TYPE_ID`,`DC_ID`,`DS_NM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='데이터센터에 등록 디스크 정보';


-- nc_dc_etc_conn definition

CREATE TABLE `nc_dc_etc_conn` (
                                  `DC_ID` varchar(20) NOT NULL,
                                  `CONN_TYPE` varchar(20) NOT NULL,
                                  `CONN_URL` varchar(100) DEFAULT NULL,
                                  `CONN_USERID` varchar(500) DEFAULT NULL,
                                  `CONN_PWD` varchar(100) DEFAULT NULL,
                                  `CONN_PROP` varchar(100) DEFAULT NULL,
                                  PRIMARY KEY (`DC_ID`,`CONN_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='데이터센터 기타 API 연결정보';


-- nc_dc_kubun_conf definition

CREATE TABLE `nc_dc_kubun_conf` (
                                    `DC_ID` varchar(20) NOT NULL,
                                    `ID` varchar(30) NOT NULL,
                                    `KUBUN` varchar(30) NOT NULL,
                                    `VAL1` varchar(100) DEFAULT NULL,
                                    `INS_TMS` timestamp NULL DEFAULT NULL,
                                    `INS_PGM` varchar(30) DEFAULT NULL,
                                    `INS_ID` varchar(10) DEFAULT NULL,
                                    `INS_IP` varchar(40) DEFAULT NULL,
                                    `UPD_TMS` timestamp NULL DEFAULT NULL,
                                    `UPD_PGM` varchar(30) DEFAULT NULL,
                                    `UPD_ID` varchar(10) DEFAULT NULL,
                                    `UPD_IP` varchar(40) DEFAULT NULL,
                                    `VAL2` varchar(50) DEFAULT NULL,
                                    PRIMARY KEY (`DC_ID`,`ID`,`KUBUN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_dc_os_type definition

CREATE TABLE `nc_dc_os_type` (
                                 `DC_ID` varchar(20) NOT NULL,
                                 `OS_ID` varchar(20) NOT NULL,
                                 `TMPL_PATH` varchar(200) DEFAULT NULL,
                                 `INS_ID` varchar(10) DEFAULT NULL,
                                 `UPD_ID` varchar(10) DEFAULT NULL,
                                 `INS_IP` varchar(40) DEFAULT NULL,
                                 `UPD_IP` varchar(40) DEFAULT NULL,
                                 `INS_PGM` varchar(30) DEFAULT NULL,
                                 `UPD_PGM` varchar(30) DEFAULT NULL,
                                 `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `GUEST_NM` varchar(50) DEFAULT NULL,
                                 `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                                 `RAM_SIZE` decimal(22,0) DEFAULT NULL,
                                 `CPU_CNT` decimal(10,0) DEFAULT NULL,
                                 `HOST_HV_ID` varchar(30) DEFAULT NULL,
                                 PRIMARY KEY (`DC_ID`,`OS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='데이터센터 OS 종류';


-- nc_dc_os_type_nw definition

CREATE TABLE `nc_dc_os_type_nw` (
                                    `DC_ID` varchar(20) NOT NULL,
                                    `OS_ID` varchar(20) NOT NULL,
                                    `NW_HV_ID` varchar(30) NOT NULL,
                                    `SEQ` int(11) DEFAULT NULL,
                                    PRIMARY KEY (`DC_ID`,`OS_ID`,`NW_HV_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_dc_prop definition

CREATE TABLE `nc_dc_prop` (
                              `DC_ID` varchar(20) NOT NULL,
                              `PROP_VAL` varchar(20) DEFAULT NULL,
                              `PROP_NM` varchar(200) NOT NULL,
                              `UPD_ID` varchar(10) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `UPD_IP` varchar(40) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `UPD_PGM` varchar(30) DEFAULT NULL,
                              PRIMARY KEY (`DC_ID`,`PROP_NM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='데이터센터 속성';


-- nc_dc_vm_spec definition

CREATE TABLE `nc_dc_vm_spec` (
                                 `DC_ID` varchar(20) NOT NULL,
                                 `SPEC_ID` varchar(20) NOT NULL,
                                 `DC_SPEC_CD` varchar(100) NOT NULL,
                                 PRIMARY KEY (`DC_ID`,`SPEC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='DC별 사양 맵핑';


-- nc_dd_use definition

CREATE TABLE `nc_dd_use` (
                             `TEAM_CD` varchar(10) NOT NULL,
                             `SVC_ID` varchar(20) NOT NULL,
                             `USE_AMT` decimal(22,0) DEFAULT NULL,
                             `YYYYMMDD` varchar(20) NOT NULL,
                             `SVC_CD` varchar(10) NOT NULL,
                             `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `USER_ID` decimal(22,0) DEFAULT NULL,
                             `COMP_ID` varchar(20) DEFAULT NULL,
                             `STOP_AMT` decimal(22,0) DEFAULT NULL,
                             `FEE` decimal(8,0) DEFAULT '0',
                             PRIMARY KEY (`YYYYMMDD`,`SVC_ID`,`SVC_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_deploy_fail definition

CREATE TABLE `nc_deploy_fail` (
                                  `SVC_ID` varchar(20) NOT NULL,
                                  `SVC_CD` varchar(10) NOT NULL,
                                  `INS_DT` varchar(14) NOT NULL,
                                  `FAIL_MSG` varchar(8000) DEFAULT NULL,
                                  `TASK_STATUS_CD` char(1) DEFAULT NULL,
                                  `RSV_DT` varchar(10) DEFAULT NULL,
                                  `DEPLOY_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `ADMIN_CMT` varchar(1000) DEFAULT NULL,
                                  `ADMIN_CMT_CD` varchar(10) DEFAULT NULL,
                                  `INS_IP` varchar(40) DEFAULT NULL,
                                  `UPD_IP` varchar(40) DEFAULT NULL,
                                  `INS_PGM` varchar(30) DEFAULT NULL,
                                  `UPD_PGM` varchar(30) DEFAULT NULL,
                                  `UPD_ID` varchar(10) DEFAULT NULL,
                                  `INS_ID` varchar(10) DEFAULT NULL,
                                  `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `CUD_CD` char(1) DEFAULT NULL,
                                  PRIMARY KEY (`SVC_CD`,`SVC_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_disk definition

CREATE TABLE `nc_disk` (
                           `VM_ID` varchar(20) DEFAULT NULL,
                           `TEAM_CD` varchar(10) NOT NULL,
                           `DC_ID` varchar(20) DEFAULT NULL,
                           `DISK_NM` varchar(200) DEFAULT NULL,
                           `DISK_ID` varchar(20) NOT NULL,
                           `DISK_TYPE_ID` varchar(20) DEFAULT NULL,
                           `CMT` varchar(1000) DEFAULT NULL,
                           `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                           `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                           `DEL_YN` varchar(1) DEFAULT NULL,
                           `INS_ID` varchar(10) DEFAULT NULL,
                           `UPD_ID` varchar(10) DEFAULT NULL,
                           `INS_PGM` varchar(30) DEFAULT NULL,
                           `UPD_PGM` varchar(30) DEFAULT NULL,
                           `INS_IP` varchar(40) DEFAULT NULL,
                           `UPD_IP` varchar(40) DEFAULT NULL,
                           `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           `HH_FEE` decimal(22,0) DEFAULT NULL,
                           `DD_FEE` decimal(22,0) DEFAULT NULL,
                           `MM_FEE` decimal(22,0) DEFAULT NULL,
                           `DISK_PATH` varchar(200) DEFAULT NULL,
                           `DS_NM` varchar(200) DEFAULT NULL,
                           `TASK_STATUS_CD` char(1) NOT NULL DEFAULT 'W',
                           `FAIL_MSG` varchar(1000) DEFAULT NULL,
                           `INS_DT` varchar(14) DEFAULT NULL,
                           PRIMARY KEY (`DISK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='디스크 정보';


-- nc_disk_req definition

CREATE TABLE `nc_disk_req` (
                               `DISK_NM` varchar(200) DEFAULT NULL,
                               `DISK_ID` varchar(20) NOT NULL,
                               `CMT` varchar(1000) DEFAULT NULL,
                               `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                               `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                               `CUD_CD` varchar(1) DEFAULT NULL,
                               `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                               `INS_ID` varchar(10) DEFAULT NULL,
                               `INS_IP` varchar(40) DEFAULT NULL,
                               `INS_PGM` varchar(30) DEFAULT NULL,
                               `DC_ID` varchar(20) DEFAULT NULL,
                               `VM_ID` varchar(20) DEFAULT NULL,
                               `TEAM_CD` varchar(10) NOT NULL,
                               `DISK_TYPE_ID` varchar(20) DEFAULT NULL,
                               `HH_FEE` decimal(22,0) DEFAULT NULL,
                               `DD_FEE` decimal(22,0) DEFAULT NULL,
                               `MM_FEE` decimal(22,0) DEFAULT NULL,
                               `INS_DT` varchar(14) NOT NULL,
                               PRIMARY KEY (`DISK_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='디스크 요청 정보';


-- nc_disk_type definition

CREATE TABLE `nc_disk_type` (
                                `DISK_TYPE_ID` varchar(20) NOT NULL,
                                `HH_FEE` decimal(22,0) DEFAULT NULL,
                                `DD_FEE` decimal(22,0) DEFAULT NULL,
                                `FEE_UNIT` decimal(22,0) DEFAULT NULL,
                                `DISK_TYPE_NM` varchar(200) DEFAULT NULL,
                                `MM_FEE` decimal(22,0) DEFAULT NULL,
                                `INS_ID` varchar(10) DEFAULT NULL,
                                `UPD_ID` varchar(10) DEFAULT NULL,
                                `INS_IP` varchar(40) DEFAULT NULL,
                                `UPD_IP` varchar(40) DEFAULT NULL,
                                `INS_PGM` varchar(30) DEFAULT NULL,
                                `UPD_PGM` varchar(30) DEFAULT NULL,
                                `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `DEL_YN` varchar(1) DEFAULT NULL,
                                PRIMARY KEY (`DISK_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='디스크 타입';


-- nc_etc_fee definition

CREATE TABLE `nc_etc_fee` (
                              `SVC_CD` varchar(10) NOT NULL,
                              `HH_FEE` decimal(22,0) DEFAULT NULL,
                              `DD_FEE` decimal(22,0) DEFAULT NULL,
                              `MM_FEE` decimal(22,0) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `UPD_ID` varchar(10) DEFAULT NULL,
                              `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_PGM` varchar(30) DEFAULT NULL,
                              `UPD_IP` varchar(40) DEFAULT NULL,
                              PRIMARY KEY (`SVC_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='기타요금 정보';


-- nc_extend_period definition

CREATE TABLE `nc_extend_period` (
                                    `SVC_ID` varchar(20) NOT NULL,
                                    `SVC_CD` varchar(1) NOT NULL,
                                    `REASON` varchar(1000) DEFAULT NULL,
                                    `ADD_USE_MM` decimal(10,0) DEFAULT NULL,
                                    `INS_ID` varchar(10) DEFAULT NULL,
                                    `UPD_ID` varchar(10) DEFAULT NULL,
                                    `INS_PGM` varchar(30) DEFAULT NULL,
                                    `UPD_PGM` varchar(30) DEFAULT NULL,
                                    `INS_IP` varchar(40) DEFAULT NULL,
                                    `UPD_IP` varchar(40) DEFAULT NULL,
                                    `INS_TMS` timestamp NULL DEFAULT NULL,
                                    `UPD_TMS` timestamp NULL DEFAULT NULL,
                                    `INS_DT` varchar(14) DEFAULT NULL,
                                    `SVC_TYPE` char(1) NOT NULL DEFAULT 'E',
                                    PRIMARY KEY (`SVC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_extend_period_req definition

CREATE TABLE `nc_extend_period_req` (
                                        `SVC_ID` varchar(20) NOT NULL,
                                        `CUD_CD` varchar(1) DEFAULT NULL,
                                        `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                                        `REASON` varchar(1000) DEFAULT NULL,
                                        `INS_ID` varchar(10) DEFAULT NULL,
                                        `INS_IP` varchar(40) DEFAULT NULL,
                                        `INS_PGM` varchar(30) DEFAULT NULL,
                                        `INS_DT` varchar(14) NOT NULL,
                                        `ADD_USE_MM` decimal(10,0) DEFAULT NULL,
                                        `SVC_CD` varchar(1) DEFAULT NULL,
                                        `SVC_TYPE` char(1) NOT NULL DEFAULT 'E',
                                        PRIMARY KEY (`SVC_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_fw definition

CREATE TABLE `nc_fw` (
                         `PROTOCOL_CD` varchar(10) DEFAULT NULL,
                         `CMT` varchar(1000) DEFAULT NULL,
                         `CIDR` varchar(500) DEFAULT NULL,
                         `PUBLIC_IP_ID` varchar(20) DEFAULT NULL,
                         `FW_ID` varchar(20) NOT NULL,
                         `DEL_YN` varchar(1) DEFAULT NULL,
                         `INS_ID` varchar(10) DEFAULT NULL,
                         `UPD_ID` varchar(10) DEFAULT NULL,
                         `INS_PGM` varchar(30) DEFAULT NULL,
                         `UPD_PGM` varchar(30) DEFAULT NULL,
                         `INS_IP` varchar(40) DEFAULT NULL,
                         `UPD_IP` varchar(40) DEFAULT NULL,
                         `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `TASK_STATUS_CD` char(1) NOT NULL DEFAULT 'W',
                         `FAIL_MSG` varchar(1000) DEFAULT NULL,
                         `TASK_FINISH_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `WORKER_ID` varchar(10) DEFAULT NULL,
                         `INS_DT` varchar(14) DEFAULT NULL,
                         `VM_ID` varchar(20) DEFAULT NULL,
                         `PORT` varchar(100) DEFAULT NULL,
                         `FW_USER_ID` varchar(10) DEFAULT NULL,
                         `USE_MM` decimal(10,0) DEFAULT NULL,
                         `VM_USER_YN` char(1) DEFAULT 'N',
                         `ACCT_AUTO_CREATE_YN` varchar(1) DEFAULT 'N',
                         `FW_DEPLOY_RESULT` varchar(500) DEFAULT NULL,
                         `PURPOSE` varchar(500) DEFAULT NULL,
                         `PRIVATE_KEY` varchar(3000) DEFAULT NULL,
                         PRIMARY KEY (`FW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='접근통제(방화벽) 정보';


-- nc_fw_ip_req definition

CREATE TABLE `nc_fw_ip_req` (
                                `IP_ID` varchar(20) NOT NULL,
                                `INS_DT` varchar(14) NOT NULL,
                                `OLD_IP` varchar(40) NOT NULL,
                                `NEW_IP` varchar(40) NOT NULL,
                                `INS_ID` decimal(22,0) DEFAULT NULL,
                                `INS_PGM` varchar(30) DEFAULT NULL,
                                `INS_IP` varchar(40) DEFAULT NULL,
                                `CUD_CD` varchar(1) DEFAULT NULL,
                                `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                                PRIMARY KEY (`IP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ip 일괄 변경 요청 ( old_ip-> new_ip )';


-- nc_fw_req definition

CREATE TABLE `nc_fw_req` (
                             `PROTOCOL_CD` varchar(10) DEFAULT NULL,
                             `CMT` varchar(1000) DEFAULT NULL,
                             `CIDR` varchar(500) DEFAULT NULL,
                             `FW_ID` varchar(20) NOT NULL,
                             `CUD_CD` varchar(1) DEFAULT NULL,
                             `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                             `INS_ID` varchar(10) DEFAULT NULL,
                             `INS_IP` varchar(40) DEFAULT NULL,
                             `INS_PGM` varchar(30) DEFAULT NULL,
                             `PUBLIC_IP_ID` varchar(20) DEFAULT NULL,
                             `INS_DT` varchar(14) NOT NULL,
                             `VM_ID` varchar(20) DEFAULT NULL,
                             `PORT` varchar(100) DEFAULT NULL,
                             `FW_USER_ID` varchar(10) DEFAULT NULL,
                             `USE_MM` decimal(10,0) DEFAULT '6',
                             `VM_USER_YN` char(1) DEFAULT 'N',
                             PRIMARY KEY (`FW_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='접근통제 요청 정보';


-- nc_hv_alarm definition

CREATE TABLE `nc_hv_alarm` (
                               `ALARM_NM` varchar(200) NOT NULL,
                               `TARGET` varchar(80) NOT NULL,
                               `TARGET_TYPE` varchar(200) DEFAULT NULL,
                               `CATEGORY` varchar(15) DEFAULT NULL,
                               `CRE_TIME` decimal(22,0) NOT NULL,
                               `TARGET_ID` varchar(100) DEFAULT NULL,
                               `DC_ID` varchar(20) DEFAULT NULL,
                               `DEL_YN` char(1) DEFAULT 'N',
                               `READ_YN` char(1) DEFAULT 'N',
                               `MAIL_SEND_YN` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='하이퍼바이저 경고';


-- nc_hv_obj definition

CREATE TABLE `nc_hv_obj` (
                             `OBJ_NM` varchar(200) NOT NULL,
                             `OBJ_TYPE_NM` varchar(200) NOT NULL,
                             `PARENT_OBJ_NM` varchar(200) DEFAULT NULL,
                             `OBJ_ID` varchar(200) DEFAULT NULL,
                             `DC_ID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='하이퍼바이저 오브젝트';


-- nc_img definition

CREATE TABLE `nc_img` (
                          `IMG_ID` varchar(20) NOT NULL,
                          `IMG_NM` varchar(200) DEFAULT NULL,
                          `CMT` varchar(4000) DEFAULT NULL,
                          `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                          `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                          `HH_FEE` decimal(22,0) DEFAULT NULL,
                          `DD_FEE` decimal(22,0) DEFAULT NULL,
                          `MM_FEE` decimal(22,0) DEFAULT NULL,
                          `FROM_ID` varchar(20) DEFAULT NULL,
                          `DEL_YN` varchar(1) DEFAULT NULL,
                          `TEAM_CD` varchar(20) DEFAULT NULL,
                          `DC_ID` varchar(20) DEFAULT NULL,
                          `OS_ID` varchar(20) DEFAULT NULL,
                          `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `INS_PGM` varchar(30) DEFAULT NULL,
                          `INS_IP` varchar(40) DEFAULT NULL,
                          `INS_ID` varchar(10) DEFAULT NULL,
                          `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `UPD_PGM` varchar(30) DEFAULT NULL,
                          `UPD_IP` varchar(40) DEFAULT NULL,
                          `UPD_ID` varchar(10) DEFAULT NULL,
                          `TASK_STATUS_CD` char(1) NOT NULL DEFAULT 'W',
                          `FAIL_MSG` varchar(1000) DEFAULT NULL,
                          `INS_DT` varchar(14) DEFAULT NULL,
                          `P_KUBUN` varchar(1) DEFAULT NULL,
                          `USE_MM` decimal(22,0) DEFAULT NULL,
                          `PURPOSE` varchar(50) DEFAULT NULL,
                          `CATEGORY` varchar(30) DEFAULT NULL,
                          `SPEC_ID` varchar(20) DEFAULT NULL,
                          `COMMON_YN` char(1) DEFAULT 'Y',
                          `EXPIRED_TMS` timestamp NULL DEFAULT NULL,
                          PRIMARY KEY (`IMG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='이미지(템플릿)';


-- nc_img_category definition

CREATE TABLE `nc_img_category` (
                                   `CATEGORY_ID` varchar(30) NOT NULL,
                                   `PAR_CATEGORY_ID` varchar(30) DEFAULT NULL,
                                   `CATEGORY_NM` varchar(200) DEFAULT NULL,
                                   `INS_ID` varchar(10) DEFAULT NULL,
                                   `INS_PGM` varchar(30) DEFAULT NULL,
                                   `INS_IP` varchar(40) DEFAULT NULL,
                                   `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   `UPD_ID` varchar(10) DEFAULT NULL,
                                   `UPD_PGM` varchar(30) DEFAULT NULL,
                                   `UPD_IP` varchar(40) DEFAULT NULL,
                                   `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   `CATEGORY_CODE` varchar(200) DEFAULT NULL,
                                   `CATEGORY_NUM` decimal(22,0) DEFAULT '3',
                                   `KUBUN` varchar(10) DEFAULT NULL,
                                   PRIMARY KEY (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='이미지(템플릿) 카테고리';


-- nc_img_disk definition

CREATE TABLE `nc_img_disk` (
                               `DISK_NM` varchar(200) DEFAULT NULL,
                               `DISK_ID` varchar(20) NOT NULL,
                               `CMT` varchar(1000) DEFAULT NULL,
                               `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                               `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                               `IMG_ID` varchar(20) NOT NULL,
                               PRIMARY KEY (`IMG_ID`,`DISK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_img_req definition

CREATE TABLE `nc_img_req` (
                              `IMG_ID` varchar(20) NOT NULL,
                              `IMG_NM` varchar(200) DEFAULT NULL,
                              `CMT` varchar(4000) DEFAULT NULL,
                              `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                              `CUD_CD` varchar(1) DEFAULT NULL,
                              `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                              `TEAM_CD` varchar(20) DEFAULT NULL,
                              `DC_ID` varchar(20) DEFAULT NULL,
                              `OS_ID` varchar(20) DEFAULT NULL,
                              `FROM_ID` varchar(20) DEFAULT NULL,
                              `HH_FEE` decimal(22,0) DEFAULT NULL,
                              `DD_FEE` decimal(22,0) DEFAULT NULL,
                              `MM_FEE` decimal(22,0) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `INS_DT` varchar(14) NOT NULL,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `P_KUBUN` varchar(1) DEFAULT NULL,
                              `USE_MM` decimal(22,0) DEFAULT NULL,
                              `PURPOSE` varchar(50) DEFAULT NULL,
                              `CATEGORY` varchar(30) DEFAULT NULL,
                              `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                              PRIMARY KEY (`IMG_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_img_user definition

CREATE TABLE `nc_img_user` (
                               `IMG_ID` varchar(20) NOT NULL,
                               `USER_ID` decimal(22,0) NOT NULL,
                               `USER_IP` varchar(40) NOT NULL,
                               `PORT` varchar(100) DEFAULT NULL,
                               `OWNER_YN` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='개인템플릿';


-- nc_ip definition

CREATE TABLE `nc_ip` (
                         `IP` varchar(40) NOT NULL,
                         `DC_ID` varchar(20) DEFAULT NULL,
                         `VM_HV_ID` varchar(30) DEFAULT NULL,
                         `VM_NM` varchar(50) DEFAULT NULL,
                         `STATUS` char(1) DEFAULT 'N',
                         `RSN` varchar(100) DEFAULT NULL,
                         `NW_HV_ID` varchar(30) DEFAULT NULL,
                         `INS_ID` varchar(10) DEFAULT NULL,
                         `UPD_ID` varchar(10) DEFAULT NULL,
                         `INS_IP` varchar(40) DEFAULT NULL,
                         `UPD_IP` varchar(40) DEFAULT NULL,
                         `INS_PGM` varchar(30) DEFAULT NULL,
                         `UPD_PGM` varchar(30) DEFAULT NULL,
                         `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `IP_NUM` decimal(10,0) DEFAULT NULL,
                         `HOST_HV_ID` varchar(50) DEFAULT NULL,
                         `MAC_ADDR` varchar(30) DEFAULT NULL,
                         PRIMARY KEY (`IP`),
                         KEY `nc_ip_DC_ID_IDX` (`DC_ID`,`VM_NM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='아이피';


-- nc_license definition

CREATE TABLE `nc_license` (
                              `LICENSE_KEY` varchar(100) NOT NULL,
                              `DC_ID` varchar(20) NOT NULL,
                              `LICENSE_NAME` varchar(100) DEFAULT NULL,
                              `LICENSE_TOTAL` decimal(22,0) DEFAULT NULL,
                              `LICENSE_USED` decimal(22,0) DEFAULT NULL,
                              `COST_UNIT` varchar(10) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              PRIMARY KEY (`LICENSE_KEY`,`DC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_license_host definition

CREATE TABLE `nc_license_host` (
                                   `LICENSE_KEY` varchar(100) NOT NULL,
                                   `HOST_HV_ID` varchar(20) NOT NULL,
                                   `DC_ID` varchar(20) NOT NULL,
                                   PRIMARY KEY (`LICENSE_KEY`,`DC_ID`,`HOST_HV_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_license_input definition

CREATE TABLE `nc_license_input` (
                                    `license_name` varchar(100) NOT NULL,
                                    `license_cnt` decimal(22,0) DEFAULT NULL,
                                    `remark` varchar(100) DEFAULT NULL,
                                    `license_date` varchar(11) NOT NULL,
                                    `ins_id` varchar(10) DEFAULT NULL,
                                    `upd_id` varchar(10) DEFAULT NULL,
                                    `ins_pgm` varchar(30) DEFAULT NULL,
                                    `upd_pgm` varchar(30) DEFAULT NULL,
                                    `ins_tms` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    `upd_tms` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_mm_detail definition

CREATE TABLE `nc_mm_detail` (
                                `YYYYMM` varchar(6) NOT NULL,
                                `COMP_ID` varchar(20) NOT NULL,
                                `SVC_ID` varchar(20) NOT NULL,
                                `SVC_CD` varchar(10) NOT NULL,
                                `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                                `USE_AMT` decimal(22,0) DEFAULT NULL,
                                `UNIT_FEE` decimal(22,0) DEFAULT NULL,
                                `TEAM_CD` varchar(20) NOT NULL,
                                `USER_ID` decimal(22,0) DEFAULT NULL,
                                `STOP_AMT` decimal(22,0) DEFAULT NULL,
                                `STOP_UNIT_FEE` decimal(22,0) DEFAULT NULL,
                                PRIMARY KEY (`YYYYMM`,`COMP_ID`,`SVC_ID`,`SVC_CD`,`TEAM_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 월별 사용률 상세';


-- nc_mm_pay definition

CREATE TABLE `nc_mm_pay` (
                             `YYYYMM` varchar(6) NOT NULL,
                             `COMP_ID` varchar(20) NOT NULL,
                             `USE_FEE` decimal(22,0) DEFAULT NULL,
                             `PAY_YN` varchar(1) DEFAULT NULL,
                             `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `PAY_DTTM` varchar(14) DEFAULT NULL,
                             PRIMARY KEY (`YYYYMM`,`COMP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 월별 사용요금';


-- nc_mm_stat definition

CREATE TABLE `nc_mm_stat` (
                              `YYYYMM` varchar(8) NOT NULL,
                              `TEAM_CD` varchar(10) NOT NULL,
                              `OBJ_ID` varchar(20) NOT NULL,
                              `CPU_CNT` decimal(22,0) DEFAULT NULL,
                              `CPU_MHZ` decimal(22,0) DEFAULT NULL,
                              `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                              `RAM_SIZE` decimal(22,0) DEFAULT NULL,
                              `DISK_USAGE` decimal(5,1) DEFAULT '0.0',
                              `CPU_USAGE` decimal(5,1) DEFAULT '0.0',
                              `MEM_USAGE` decimal(5,1) DEFAULT '0.0',
                              `CPU_USAGE_MAX` decimal(5,1) DEFAULT '0.0',
                              `MEM_USAGE_MAX` decimal(5,1) DEFAULT '0.0',
                              `OBJ_TYPE` varchar(1) DEFAULT NULL,
                              `USE_AMT` decimal(22,0) DEFAULT NULL,
                              `FEE` decimal(22,0) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              PRIMARY KEY (`YYYYMM`,`TEAM_CD`,`OBJ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 월별 리소스';


-- nc_mm_stat_user definition

CREATE TABLE `nc_mm_stat_user` (
                                   `YYYYMM` varchar(8) NOT NULL,
                                   `OBJ_ID` varchar(20) NOT NULL,
                                   `USER_ID` decimal(22,0) NOT NULL,
                                   PRIMARY KEY (`YYYYMM`,`OBJ_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 월별 사용자별 리소스';


-- nc_monitor_ds definition

CREATE TABLE `nc_monitor_ds` (
                                 `REG_DT` varchar(8) NOT NULL,
                                 `REG_TM` varchar(6) NOT NULL,
                                 `DC_ID` varchar(20) NOT NULL,
                                 `NAME` varchar(80) NOT NULL,
                                 `CAPACITY` decimal(15,0) DEFAULT NULL,
                                 `FREESPACE` decimal(15,0) DEFAULT NULL,
                                 `HV_ID` varchar(30) DEFAULT NULL,
                                 `VM_CNT` decimal(4,0) DEFAULT NULL,
                                 `OVERALLSTATUS` varchar(10) DEFAULT NULL,
                                 `UNCMTD` decimal(15,0) DEFAULT NULL,
                                 `TYPE` varchar(20) DEFAULT NULL COMMENT '종류',
                                 PRIMARY KEY (`REG_DT`,`REG_TM`,`DC_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='데이터 스토어 모니터링';


-- nc_monitor_etc definition

CREATE TABLE `nc_monitor_etc` (
                                  `ID` varchar(20) NOT NULL,
                                  `DATA` text NOT NULL,
                                  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='기타 모니터링';


-- nc_monitor_host definition

CREATE TABLE `nc_monitor_host` (
                                   `REG_DT` varchar(8) NOT NULL,
                                   `REG_TM` varchar(6) NOT NULL,
                                   `DC_ID` varchar(20) NOT NULL,
                                   `NAME` varchar(100) NOT NULL,
                                   `MEMORYSIZE` decimal(12,0) DEFAULT NULL,
                                   `CPUMODEL` varchar(50) DEFAULT NULL,
                                   `NUMCPUCORES` decimal(3,0) DEFAULT NULL,
                                   `HV_ID` varchar(30) DEFAULT NULL,
                                   `VM_CNT` decimal(4,0) DEFAULT NULL,
                                   `CPU_USAGE` decimal(5,1) DEFAULT NULL,
                                   `MEM_USAGE` decimal(5,1) DEFAULT NULL,
                                   `OVERALLSTATUS` varchar(10) DEFAULT NULL,
                                   `NUMCPUPKGS` decimal(22,0) DEFAULT NULL,
                                   `GPU_SIZE` decimal(5,0) DEFAULT NULL,
                                   `GPU_MODEL_MAX` decimal(5,0) DEFAULT NULL,
                                   `GPU_MODEL` varchar(20) DEFAULT NULL,
                                   PRIMARY KEY (`REG_DT`,`REG_TM`,`DC_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='호스트 모니터링';


-- nc_monitor_vm definition

CREATE TABLE `nc_monitor_vm` (
                                 `REG_DT` varchar(8) NOT NULL,
                                 `REG_TM` varchar(6) NOT NULL,
                                 `DC_ID` varchar(20) NOT NULL,
                                 `VM_HV_ID` varchar(20) NOT NULL,
                                 `CPU_USAGE` decimal(5,1) DEFAULT NULL,
                                 `MEM_USAGE` decimal(5,1) DEFAULT NULL,
                                 `DISK_USAGE` decimal(5,1) DEFAULT NULL,
                                 `NET_USAGE` decimal(10,0) DEFAULT NULL,
                                 `VM_NM` varchar(100) DEFAULT NULL,
                                 `HOST_NM` varchar(100) DEFAULT NULL,
                                 `GUEST_NM` varchar(100) DEFAULT NULL,
                                 `CPU_CNT` smallint(6) DEFAULT NULL,
                                 `RAM_SIZE` smallint(6) DEFAULT NULL,
                                 `DISK_SIZE` bigint(20) DEFAULT NULL,
                                 `OLD_HOST_NM` varchar(100) DEFAULT NULL,
                                 `RUN_CD` char(1) CHARACTER SET ucs2 DEFAULT NULL,
                                 `GPU_MODEL` varchar(10) DEFAULT NULL,
                                 `GPU_SIZE` double DEFAULT NULL,
                                 `HOST_CHG_DTTM` varchar(14) DEFAULT NULL,
                                 `FT_YN` char(1) DEFAULT 'N',
                                 PRIMARY KEY (`REG_DT`,`REG_TM`,`DC_ID`,`VM_HV_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 모니터링';


-- nc_nas definition

CREATE TABLE `nc_nas` (
                          `NAS_IP` varchar(40) NOT NULL,
                          `NAS_PATH` varchar(100) NOT NULL,
                          `TEAM_CD` varchar(10) DEFAULT NULL,
                          `INS_ID` decimal(22,0) DEFAULT NULL,
                          `INS_TMS` timestamp NULL DEFAULT NULL,
                          `SIZE` decimal(22,0) NOT NULL DEFAULT '0',
                          `UNIT` varchar(1) NOT NULL DEFAULT 'G',
                          `UPD_TMS` timestamp NULL DEFAULT NULL,
                          `UPD_ID` varchar(40) DEFAULT NULL,
                          `USER_ID` decimal(22,0) DEFAULT NULL,
                          `NAS_DESC` varchar(1000) DEFAULT NULL,
                          `NAS_ID` varchar(20) NOT NULL,
                          `TASK_STATUS_CD` varchar(10) DEFAULT 'W',
                          `DEL_YN` varchar(1) DEFAULT 'N',
                          `INS_PGM` varchar(30) DEFAULT NULL,
                          `UPD_PGM` varchar(30) DEFAULT NULL,
                          `INS_IP` varchar(40) DEFAULT NULL,
                          `UPD_IP` varchar(40) DEFAULT NULL,
                          `FAIL_MSG` varchar(1000) DEFAULT NULL,
                          `INS_DT` varchar(14) DEFAULT NULL,
                          `USE_SIZE_GB` decimal(10,2) DEFAULT NULL COMMENT '사용용량',
                          `NAS_BASE_PATH` varchar(100) DEFAULT '',
                          `EXPIRED_TMS` timestamp NULL DEFAULT NULL,
                          PRIMARY KEY (`NAS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='NAS 정보';


-- nc_nas_req definition

CREATE TABLE `nc_nas_req` (
                              `NAS_ID` varchar(20) NOT NULL,
                              `INS_DT` varchar(14) NOT NULL,
                              `NAS_PATH` varchar(100) NOT NULL,
                              `SIZE` decimal(22,0) NOT NULL DEFAULT '0',
                              `UNIT` varchar(1) NOT NULL DEFAULT 'G',
                              `NAS_DESC` varchar(1000) DEFAULT NULL,
                              `TEAM_CD` varchar(10) DEFAULT NULL,
                              `INS_ID` decimal(22,0) DEFAULT NULL,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `CUD_CD` varchar(1) DEFAULT NULL,
                              `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                              PRIMARY KEY (`NAS_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_os_type definition

CREATE TABLE `nc_os_type` (
                              `OS_ID` varchar(20) NOT NULL,
                              `OS_NM` varchar(200) DEFAULT NULL,
                              `MM_FEE` decimal(22,0) DEFAULT NULL,
                              `DD_FEE` decimal(22,0) DEFAULT NULL,
                              `HH_FEE` decimal(22,0) DEFAULT NULL,
                              `CMT` varchar(1000) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `UPD_ID` varchar(10) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `UPD_IP` varchar(40) DEFAULT NULL,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `UPD_PGM` varchar(30) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `DEL_YN` varchar(1) DEFAULT NULL,
                              `LINUX_YN` varchar(1) DEFAULT NULL,
                              `PART_YN` char(1) DEFAULT 'N',
                              `CATEGORY` varchar(50) DEFAULT NULL,
                              `YEAR` varchar(4) DEFAULT NULL,
                              `KUBUN` varchar(200) DEFAULT 'O',
                              `TEAM_CD` varchar(10) DEFAULT NULL,
                              `USER_NAME` varchar(20) DEFAULT NULL,
                              `TEAM_NM` varchar(100) DEFAULT NULL,
                              `SUB_YN` varchar(1) DEFAULT 'N',
                              `PURPOSE` varchar(30) DEFAULT NULL,
                              `P_KUBUN` varchar(1) DEFAULT NULL,
                              `ICON` blob,
                              `conn_id` varchar(20) DEFAULT NULL,
                              PRIMARY KEY (`OS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OS 타입';


-- nc_overunder_vm definition

CREATE TABLE `nc_overunder_vm` (
                                   `VM_HV_ID` varchar(10) NOT NULL,
                                   `VM_NM` varchar(100) NOT NULL,
                                   `VM_ID` varchar(20) DEFAULT NULL,
                                   `UNDER_CPU` int(11) DEFAULT '0',
                                   `UNDER_MEM` int(11) DEFAULT '0',
                                   `OVER_CPU` int(11) DEFAULT '0',
                                   `OVER_MEM` int(11) DEFAULT '0',
                                   `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   PRIMARY KEY (`VM_HV_ID`,`VM_NM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_req definition

CREATE TABLE `nc_req` (
                          `REQ_ID` varchar(20) NOT NULL,
                          `INS_ID` varchar(10) DEFAULT NULL,
                          `REQ_NM` varchar(200) DEFAULT NULL,
                          `CMT` varchar(1000) DEFAULT NULL,
                          `ATT_FILE_PATH` varchar(200) DEFAULT NULL,
                          `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                          `TEAM_CD` varchar(20) DEFAULT NULL,
                          `APPR_ID` decimal(22,0) DEFAULT NULL,
                          `APPR_COMMENT` varchar(500) DEFAULT NULL,
                          `INS_IP` varchar(40) DEFAULT NULL,
                          `UPD_IP` varchar(40) DEFAULT NULL,
                          `INS_PGM` varchar(30) DEFAULT NULL,
                          `UPD_PGM` varchar(30) DEFAULT NULL,
                          `UPD_ID` varchar(10) DEFAULT NULL,
                          `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `APPR_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `STEP` decimal(22,0) DEFAULT NULL,
                          `NEXT_APPR_ID` varchar(100) DEFAULT NULL,
                          `APPR_CMT_CD` varchar(10) DEFAULT NULL,
                          PRIMARY KEY (`REQ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='결재요청 정보';


-- nc_req_approver definition

CREATE TABLE `nc_req_approver` (
                                   `REQ_ID` varchar(20) NOT NULL,
                                   `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                                   `APPR_ID` decimal(22,0) NOT NULL,
                                   `APPR_COMMENT` varchar(500) DEFAULT NULL,
                                   `APPR_TMS` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
                                   `STEP` decimal(22,0) NOT NULL,
                                   PRIMARY KEY (`REQ_ID`,`APPR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='결재자 정보';


-- nc_req_detail definition

CREATE TABLE `nc_req_detail` (
                                 `REQ_ID` varchar(20) NOT NULL,
                                 `SVC_ID` varchar(20) NOT NULL,
                                 `SVC_CD` varchar(10) NOT NULL,
                                 `INS_DT` varchar(14) DEFAULT NULL,
                                 PRIMARY KEY (`SVC_ID`,`REQ_ID`,`SVC_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='결재항목 ';


-- nc_req_next_approver definition

CREATE TABLE `nc_req_next_approver` (
                                        `REQ_ID` varchar(20) NOT NULL,
                                        `APPROVER` decimal(22,0) NOT NULL,
                                        PRIMARY KEY (`APPROVER`,`REQ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='결재 STEP';


-- nc_revert_log definition

CREATE TABLE `nc_revert_log` (
                                 `VM_ID` varchar(20) NOT NULL,
                                 `SNAPSHOT_ID` varchar(20) NOT NULL,
                                 `INS_IP` varchar(40) DEFAULT NULL,
                                 `TASK_STATUS_CD` varchar(1) DEFAULT NULL,
                                 `INS_ID` varchar(10) DEFAULT NULL,
                                 `INS_PGM` varchar(30) DEFAULT NULL,
                                 `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                 `FAIL_MSG` varchar(1000) DEFAULT NULL,
                                 PRIMARY KEY (`VM_ID`,`SNAPSHOT_ID`,`INS_TMS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_schedule definition

CREATE TABLE `nc_schedule` (
                               `SCHEDULE_ID` int(11) NOT NULL,
                               `ID` varchar(100) NOT NULL,
                               `KUBUN` char(1) DEFAULT 'V',
                               `CRON` varchar(100) NOT NULL,
                               `ACTION` varchar(100) DEFAULT NULL,
                               `LAST_RUN_RESULT` char(1) DEFAULT NULL,
                               `LAST_RUN_TMS` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
                               `FAIL_MSG` varchar(4000) DEFAULT NULL,
                               `INS_ID` varchar(10) DEFAULT NULL,
                               `UPD_ID` varchar(10) DEFAULT NULL,
                               `INS_PGM` varchar(30) DEFAULT NULL,
                               `UPD_PGM` varchar(30) DEFAULT NULL,
                               `INS_IP` varchar(40) DEFAULT NULL,
                               `UPD_IP` varchar(40) DEFAULT NULL,
                               `INS_TMS` timestamp NULL DEFAULT NULL,
                               `UPD_TMS` timestamp NULL DEFAULT NULL,
                               PRIMARY KEY (`SCHEDULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_script definition

CREATE TABLE `nc_script` (
                             `SCRIPT_ID` varchar(20) NOT NULL,
                             `SCRIPT_NM` varchar(100) DEFAULT NULL,
                             `FILE_NM` varchar(100) DEFAULT NULL,
                             `SCRIPT_DESC` varchar(1000) DEFAULT NULL,
                             `LINUX_YN` char(1) DEFAULT NULL,
                             `INS_DT` varchar(14) DEFAULT NULL,
                             `UPD_ID` varchar(10) DEFAULT NULL,
                             `INS_PGM` varchar(30) DEFAULT NULL,
                             `UPD_PGM` varchar(30) DEFAULT NULL,
                             `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `INS_IP` varchar(40) DEFAULT NULL,
                             `UPD_IP` varchar(40) DEFAULT NULL,
                             `INS_ID` varchar(10) DEFAULT NULL,
                             PRIMARY KEY (`SCRIPT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_snapshot definition

CREATE TABLE `nc_snapshot` (
                               `VM_ID` varchar(20) NOT NULL,
                               `SNAPSHOT_NM` varchar(200) DEFAULT NULL,
                               `SNAPSHOT_ID` varchar(20) NOT NULL,
                               `CMT` varchar(1000) DEFAULT NULL,
                               `DISK_SIZE` decimal(10,2) DEFAULT NULL,
                               `DEL_YN` char(1) DEFAULT 'N',
                               `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                               `INS_IP` varchar(40) DEFAULT NULL,
                               `INS_ID` varchar(10) DEFAULT NULL,
                               `INS_PGM` varchar(30) DEFAULT NULL,
                               `TASK_STATUS_CD` char(1) NOT NULL DEFAULT 'W',
                               `FAIL_MSG` varchar(200) DEFAULT NULL,
                               PRIMARY KEY (`VM_ID`,`SNAPSHOT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='스냅샷';


-- nc_sw definition

CREATE TABLE `nc_sw` (
                         `SW_ID` varchar(20) NOT NULL,
                         `SW_NM` varchar(100) DEFAULT NULL,
                         `SW_VER` varchar(50) DEFAULT NULL,
                         `SW_DESC` varchar(1000) DEFAULT NULL,
                         `PURPOSE` varchar(20) DEFAULT NULL COMMENT '목적:WAS, DB, WEB',
                         `INS_DT` varchar(14) DEFAULT NULL,
                         `UPD_ID` varchar(10) DEFAULT NULL,
                         `INS_PGM` varchar(30) DEFAULT NULL,
                         `UPD_PGM` varchar(30) DEFAULT NULL,
                         `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `INS_IP` varchar(40) DEFAULT NULL,
                         `UPD_IP` varchar(40) DEFAULT NULL,
                         `INS_ID` varchar(10) DEFAULT NULL,
                         `ALL_USE_YN` varchar(1) DEFAULT 'N',
                         PRIMARY KEY (`SW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_sw_env definition

CREATE TABLE `nc_sw_env` (
                             `SW_ID` decimal(16,0) NOT NULL,
                             `NAME` varchar(100) NOT NULL,
                             `UPD_ID` varchar(10) DEFAULT NULL,
                             `INS_PGM` varchar(30) DEFAULT NULL,
                             `UPD_PGM` varchar(30) DEFAULT NULL,
                             `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `INS_IP` varchar(40) DEFAULT NULL,
                             `UPD_IP` varchar(40) DEFAULT NULL,
                             `INS_ID` varchar(10) DEFAULT NULL,
                             `TYPE` varchar(10) DEFAULT NULL,
                             `REQUIRED` varchar(10) DEFAULT NULL,
                             `MINIMUM` varchar(5) DEFAULT NULL,
                             `MAXIMUM` varchar(5) DEFAULT NULL,
                             `ENCRYPTED` varchar(10) DEFAULT NULL,
                             `PATTERN` varchar(10) DEFAULT NULL,
                             `TITLE` varchar(100) DEFAULT NULL,
                             `DESCRIPTION` varchar(1000) DEFAULT NULL,
                             `DEFAULT` varchar(100) NOT NULL,
                             `FIELDNAME` varchar(5) DEFAULT NULL,
                             PRIMARY KEY (`NAME`,`SW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_task_ing definition

CREATE TABLE `nc_task_ing` (
                               `TASK_ID` varchar(20) NOT NULL,
                               `INS_ID` varchar(10) DEFAULT NULL,
                               `SVC_ID` varchar(20) DEFAULT NULL,
                               `TASK_CD` varchar(1) DEFAULT NULL,
                               `TASK_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                               PRIMARY KEY (`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='진행중 작업';


-- nc_team_conf definition

CREATE TABLE `nc_team_conf` (
                                `DC_ID` varchar(20) NOT NULL,
                                `TEAM_CD` varchar(10) NOT NULL,
                                `CLUSTER` varchar(30) DEFAULT NULL,
                                `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `INS_PGM` varchar(30) DEFAULT NULL,
                                `INS_ID` varchar(10) DEFAULT NULL,
                                `INS_IP` varchar(40) DEFAULT NULL,
                                `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                `UPD_PGM` varchar(30) DEFAULT NULL,
                                `UPD_ID` varchar(10) DEFAULT NULL,
                                `UPD_IP` varchar(40) DEFAULT NULL,
                                PRIMARY KEY (`DC_ID`,`TEAM_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_template_req definition

CREATE TABLE `nc_template_req` (
                                   `TEMPLATE_ID` varchar(20) NOT NULL,
                                   `TITLE` varchar(500) NOT NULL,
                                   `CATEGORY_ID` varchar(22) DEFAULT NULL,
                                   `OS` varchar(100) DEFAULT NULL,
                                   `OS_VER` varchar(100) DEFAULT NULL,
                                   `CPU_CNT` decimal(10,0) DEFAULT NULL,
                                   `RAM_SIZE` decimal(10,0) DEFAULT NULL,
                                   `DISK_SIZE` decimal(10,0) DEFAULT NULL,
                                   `DISK_PATH` varchar(100) DEFAULT NULL,
                                   `ETC` varchar(1000) DEFAULT NULL,
                                   `CATEGORY_NM` varchar(100) DEFAULT NULL,
                                   `INS_ID` varchar(10) DEFAULT NULL,
                                   `UPD_ID` varchar(10) DEFAULT NULL,
                                   `INS_PGM` varchar(30) DEFAULT NULL,
                                   `UPD_PGM` varchar(30) DEFAULT NULL,
                                   `INS_TMS` timestamp NULL DEFAULT NULL,
                                   `UPD_TMS` timestamp NULL DEFAULT NULL,
                                   `APPR_TMS` timestamp NULL DEFAULT NULL,
                                   `TASK_STATUS_CD` varchar(10) DEFAULT NULL,
                                   `APPR_COMMENT` varchar(1000) DEFAULT NULL,
                                   `APPR_ID` decimal(10,0) DEFAULT NULL,
                                   `CATALOG_ID` varchar(50) DEFAULT NULL,
                                   `INSTAL_SW` blob,
                                   PRIMARY KEY (`TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_thin_client definition

CREATE TABLE `nc_thin_client` (
                                  `SN` varchar(50) NOT NULL,
                                  `MODEL_NM` varchar(100) DEFAULT NULL,
                                  `MAC_ADR` varchar(30) DEFAULT NULL,
                                  `IP` varchar(20) DEFAULT NULL,
                                  `STATUS` char(1) DEFAULT NULL,
                                  `VM_ID` varchar(20) DEFAULT NULL,
                                  `INS_ID` varchar(10) DEFAULT NULL,
                                  `INS_PGM` varchar(30) DEFAULT NULL,
                                  `INS_IP` varchar(40) DEFAULT NULL,
                                  `INS_TMS` timestamp NULL DEFAULT NULL,
                                  `UPD_ID` varchar(10) DEFAULT NULL,
                                  `UPD_PGM` varchar(30) DEFAULT NULL,
                                  `UPD_IP` varchar(40) DEFAULT NULL,
                                  `UPD_TMS` timestamp NULL DEFAULT NULL,
                                  `PORT` varchar(5) DEFAULT NULL COMMENT '접속포트',
                                  PRIMARY KEY (`SN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_use_log definition

CREATE TABLE `nc_use_log` (
                              `SVC_CD` varchar(10) NOT NULL,
                              `SVC_ID` varchar(20) NOT NULL,
                              `USE_START_DTTM` varchar(14) NOT NULL,
                              `USE_END_DTTM` varchar(14) DEFAULT NULL,
                              `COMP_ID` varchar(20) DEFAULT NULL,
                              `TEAM_CD` varchar(20) DEFAULT NULL,
                              `USER_ID` decimal(22,0) DEFAULT NULL,
                              `RUN_YN` varchar(1) NOT NULL,
                              PRIMARY KEY (`SVC_CD`,`SVC_ID`,`USE_START_DTTM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm definition

CREATE TABLE `nc_vm` (
                         `VM_ID` varchar(20) NOT NULL,
                         `VM_NM` varchar(200) DEFAULT NULL,
                         `DC_ID` varchar(20) NOT NULL,
                         `TEAM_CD` varchar(10) NOT NULL,
                         `RUN_CD` varchar(10) DEFAULT NULL,
                         `DEL_YN` varchar(1) DEFAULT NULL,
                         `UPD_ID` varchar(10) DEFAULT NULL,
                         `INS_PGM` varchar(30) DEFAULT NULL,
                         `UPD_PGM` varchar(30) DEFAULT NULL,
                         `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `INS_IP` varchar(40) DEFAULT NULL,
                         `UPD_IP` varchar(40) DEFAULT NULL,
                         `INS_ID` varchar(10) DEFAULT NULL,
                         `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                         `OS_HH_FEE` decimal(22,0) DEFAULT NULL,
                         `SPEC_HH_FEE` decimal(22,0) DEFAULT NULL,
                         `OS_DD_FEE` decimal(22,0) DEFAULT NULL,
                         `SPEC_DD_FEE` decimal(22,0) DEFAULT NULL,
                         `SPEC_ID` varchar(20) DEFAULT '0',
                         `OS_ID` varchar(20) DEFAULT NULL,
                         `OS_MM_FEE` decimal(22,0) DEFAULT NULL,
                         `SPEC_MM_FEE` decimal(22,0) DEFAULT NULL,
                         `FROM_ID` varchar(20) DEFAULT NULL,
                         `DISK_TYPE_ID` varchar(20) DEFAULT NULL,
                         `CPU_CNT` decimal(10,0) DEFAULT NULL,
                         `DISK_SIZE` decimal(10,0) DEFAULT NULL,
                         `RAM_SIZE` decimal(10,0) DEFAULT NULL,
                         `STOP_DD_FEE` decimal(22,0) DEFAULT NULL,
                         `STOP_HH_FEE` decimal(22,0) DEFAULT NULL,
                         `STOP_MM_FEE` decimal(22,0) DEFAULT NULL,
                         `DS_NM` varchar(200) DEFAULT NULL,
                         `FAIL_MSG` varchar(1000) DEFAULT NULL,
                         `INS_DT` varchar(14) DEFAULT NULL,
                         `VM_HV_ID` varchar(100) DEFAULT NULL,
                         `LAST_USE_TMS` timestamp NULL DEFAULT NULL,
                         `PURPOSE` varchar(100) DEFAULT NULL,
                         `GUEST_NM` varchar(50) DEFAULT NULL,
                         `DISK_UNIT` char(1) DEFAULT 'G',
                         `SEND_SHELL_YN` char(1) DEFAULT 'N',
                         `CATEGORY` varchar(50) DEFAULT NULL,
                         `P_KUBUN` varchar(3) DEFAULT NULL,
                         `USE_MM` decimal(22,0) DEFAULT '6',
                         `NAS` varchar(1000) DEFAULT NULL,
                         `CMT` varchar(4000) DEFAULT NULL,
                         `HOST_HV_ID` varchar(30) DEFAULT NULL,
                         `EXPIRED_TMS` timestamp NULL DEFAULT NULL,
                         `GPU_SIZE` decimal(22,0) DEFAULT NULL,
                         `THIN_REQ_YN` varchar(1) DEFAULT 'N' COMMENT '접속단말신청여부',
                         `THIN_REQ_CMT` varchar(500) DEFAULT NULL COMMENT '접속 단말 보안 예외 신청 사유',
                         UNIQUE KEY `nc_vm_vm_id_idx` (`VM_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 정보';


-- nc_vm_guest_job definition

CREATE TABLE `nc_vm_guest_job` (
                                   `JOB_ID` varchar(10) NOT NULL,
                                   `JOB_NM` varchar(100) DEFAULT NULL,
                                   `JOB_TYPE` varchar(1) DEFAULT NULL,
                                   `LINUX_PATH` varchar(100) DEFAULT NULL,
                                   `WIN_PATH` varchar(100) DEFAULT NULL,
                                   `GUEST_LINUX_PATH` varchar(100) DEFAULT NULL,
                                   `GUEST_WIN_PATH` varchar(100) DEFAULT NULL,
                                   `CMD_PARAM` varchar(100) DEFAULT NULL,
                                   PRIMARY KEY (`JOB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm_guest_job_log definition

CREATE TABLE `nc_vm_guest_job_log` (
                                       `JOB_ID` varchar(10) NOT NULL,
                                       `VM_ID` varchar(20) NOT NULL,
                                       `RUN_DT` varchar(14) NOT NULL,
                                       `START_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                       `FINISH_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                       `TASK_STATUS_CD` varchar(1) DEFAULT NULL,
                                       `FAIL_MSG` varchar(500) DEFAULT NULL,
                                       PRIMARY KEY (`JOB_ID`,`VM_ID`,`RUN_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm_nas definition

CREATE TABLE `nc_vm_nas` (
                             `VM_ID` varchar(20) NOT NULL,
                             `NAS_ID` varchar(20) NOT NULL,
                             `INS_DT` varchar(14) DEFAULT NULL,
                             `TASK_STATUS_CD` char(1) DEFAULT 'W',
                             `FAIL_MSG` varchar(1000) DEFAULT NULL,
                             PRIMARY KEY (`VM_ID`,`NAS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm_nas_req definition

CREATE TABLE `nc_vm_nas_req` (
                                 `VM_ID` varchar(20) NOT NULL,
                                 `NAS_ID` varchar(20) NOT NULL,
                                 `INS_DT` varchar(14) NOT NULL,
                                 `CUD_CD` char(1) DEFAULT '',
                                 PRIMARY KEY (`VM_ID`,`NAS_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm_nic definition

CREATE TABLE `nc_vm_nic` (
                             `VM_ID` varchar(20) NOT NULL,
                             `INS_ID` varchar(10) DEFAULT NULL,
                             `UPD_ID` varchar(10) DEFAULT NULL,
                             `NIC_ID` varchar(20) NOT NULL,
                             `PRIVATE_IP` varchar(40) DEFAULT NULL,
                             `INS_IP` varchar(40) DEFAULT NULL,
                             `UPD_IP` varchar(40) DEFAULT NULL,
                             `INS_PGM` varchar(30) DEFAULT NULL,
                             `UPD_PGM` varchar(30) DEFAULT NULL,
                             `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `PRIVATE_IP_NUM` decimal(10,0) DEFAULT NULL,
                             PRIMARY KEY (`VM_ID`,`NIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm_req definition

CREATE TABLE `nc_vm_req` (
                             `VM_ID` varchar(20) NOT NULL,
                             `VM_NM` varchar(200) DEFAULT NULL,
                             `CMT` varchar(4000) DEFAULT NULL,
                             `FEE_TYPE_CD` varchar(1) DEFAULT NULL,
                             `CUD_CD` varchar(1) DEFAULT NULL,
                             `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                             `INS_PGM` varchar(30) DEFAULT NULL,
                             `INS_IP` varchar(40) DEFAULT NULL,
                             `INS_ID` varchar(10) DEFAULT NULL,
                             `DC_ID` varchar(20) NOT NULL,
                             `TEAM_CD` varchar(10) NOT NULL,
                             `OS_ID` varchar(20) DEFAULT NULL,
                             `OS_MM_FEE` decimal(22,0) DEFAULT NULL,
                             `OS_DD_FEE` decimal(22,0) DEFAULT NULL,
                             `OS_HH_FEE` decimal(22,0) DEFAULT NULL,
                             `SPEC_ID` varchar(20) NOT NULL,
                             `SPEC_MM_FEE` decimal(22,0) DEFAULT NULL,
                             `SPEC_DD_FEE` decimal(22,0) DEFAULT NULL,
                             `SPEC_HH_FEE` decimal(22,0) DEFAULT NULL,
                             `FROM_ID` varchar(20) DEFAULT NULL,
                             `INS_DT` varchar(14) NOT NULL,
                             `CPU_CNT` decimal(10,0) NOT NULL,
                             `DISK_SIZE` decimal(10,0) NOT NULL,
                             `RAM_SIZE` decimal(10,0) NOT NULL,
                             `DISK_TYPE_ID` varchar(20) NOT NULL DEFAULT '1',
                             `STOP_DD_FEE` decimal(22,0) DEFAULT NULL,
                             `STOP_HH_FEE` decimal(22,0) DEFAULT NULL,
                             `STOP_MM_FEE` decimal(22,0) DEFAULT NULL,
                             `PURPOSE` varchar(100) DEFAULT NULL,
                             `DISK_UNIT` char(1) NOT NULL DEFAULT 'G',
                             `CATEGORY` varchar(50) DEFAULT NULL,
                             `P_KUBUN` varchar(3) DEFAULT NULL,
                             `USE_MM` decimal(22,0) DEFAULT NULL,
                             `NAS` varchar(1000) DEFAULT NULL,
                             `GPU_SIZE` decimal(22,0) DEFAULT NULL,
                             `THIN_REQ_YN` varchar(1) DEFAULT NULL COMMENT '접속단말신청여부',
                             `THIN_REQ_CMT` varchar(500) DEFAULT NULL COMMENT '접속 단말 보안 예외 신청 사유',
                             PRIMARY KEY (`VM_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버요청 정보';


-- nc_vm_spec definition

CREATE TABLE `nc_vm_spec` (
                              `SPEC_ID` varchar(20) NOT NULL,
                              `SPEC_NM` varchar(200) DEFAULT NULL,
                              `CPU_CNT` decimal(22,0) DEFAULT NULL,
                              `RAM_SIZE` decimal(22,0) DEFAULT NULL,
                              `DISK_TYPE_ID` varchar(20) DEFAULT NULL,
                              `DISK_SIZE` decimal(22,0) DEFAULT NULL,
                              `HH_FEE` decimal(22,0) DEFAULT NULL,
                              `DD_FEE` decimal(22,0) DEFAULT NULL,
                              `MM_FEE` decimal(22,0) DEFAULT NULL,
                              `DEL_YN` varchar(1) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_PGM` varchar(30) DEFAULT NULL,
                              `UPD_IP` varchar(40) DEFAULT NULL,
                              `UPD_ID` varchar(10) DEFAULT NULL,
                              `DISK_UNIT` char(1) DEFAULT 'G',
                              PRIMARY KEY (`SPEC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm_tag definition

CREATE TABLE `nc_vm_tag` (
                             `DC_ID` varchar(20) NOT NULL,
                             `VM_HV_ID` varchar(100) NOT NULL,
                             `TAG_NAME` varchar(100) NOT NULL,
                             `TAG_VAL` varchar(100) DEFAULT NULL,
                             `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             PRIMARY KEY (`DC_ID`,`VM_HV_ID`,`TAG_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm_user definition

CREATE TABLE `nc_vm_user` (
                              `VM_ID` varchar(20) NOT NULL,
                              `USER_ID` varchar(10) NOT NULL,
                              `USER_IP` varchar(40) NOT NULL,
                              `PORT` varchar(100) DEFAULT NULL,
                              `UPD_ID` varchar(10) DEFAULT NULL,
                              `INS_PGM` varchar(30) DEFAULT NULL,
                              `UPD_PGM` varchar(30) DEFAULT NULL,
                              `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `INS_IP` varchar(40) DEFAULT NULL,
                              `UPD_IP` varchar(40) DEFAULT NULL,
                              `INS_ID` varchar(10) DEFAULT NULL,
                              `INS_DT` varchar(14) DEFAULT NULL,
                              `OWNER_YN` char(1) DEFAULT 'N',
                              `TASK_STATUS_CD` char(1) DEFAULT 'W',
                              `FAIL_MSG` varchar(1000) DEFAULT NULL,
                              `TASK_FINISH_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `WORKER_ID` varchar(10) DEFAULT NULL,
                              `CMT` varchar(100) DEFAULT NULL,
                              `ACCT_AUTO_CREATE_YN` varchar(1) DEFAULT 'N',
                              `DEL_YN` char(1) NOT NULL DEFAULT 'N',
                              PRIMARY KEY (`VM_ID`,`USER_ID`,`USER_IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vm_user_req definition

CREATE TABLE `nc_vm_user_req` (
                                  `VM_ID` varchar(20) NOT NULL,
                                  `USER_ID` varchar(10) NOT NULL,
                                  `USER_IP` varchar(40) NOT NULL,
                                  `PORT` varchar(100) DEFAULT NULL,
                                  `CMT` varchar(100) DEFAULT NULL,
                                  `UPD_ID` varchar(10) DEFAULT NULL,
                                  `INS_PGM` varchar(30) DEFAULT NULL,
                                  `UPD_PGM` varchar(30) DEFAULT NULL,
                                  `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `INS_IP` varchar(40) DEFAULT NULL,
                                  `UPD_IP` varchar(40) DEFAULT NULL,
                                  `INS_ID` varchar(10) DEFAULT NULL,
                                  `INS_DT` varchar(14) NOT NULL,
                                  `OWNER_YN` char(1) DEFAULT 'N',
                                  `CUD_CD` varchar(1) DEFAULT NULL,
                                  PRIMARY KEY (`VM_ID`,`INS_DT`,`USER_ID`,`USER_IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vra_catalog definition

CREATE TABLE `nc_vra_catalog` (
                                  `CATALOG_ID` varchar(60) NOT NULL,
                                  `CATALOG_NM` varchar(100) DEFAULT NULL,
                                  `CATALOG_CMT` varchar(500) DEFAULT NULL,
                                  `ICON_TYPE` varchar(30) DEFAULT NULL,
                                  `ALL_USE_YN` varchar(1) DEFAULT NULL,
                                  `UPD_ID` varchar(10) DEFAULT NULL,
                                  `INS_PGM` varchar(30) DEFAULT NULL,
                                  `UPD_PGM` varchar(30) DEFAULT NULL,
                                  `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  `INS_IP` varchar(40) DEFAULT NULL,
                                  `UPD_IP` varchar(40) DEFAULT NULL,
                                  `INS_ID` varchar(10) DEFAULT NULL,
                                  `DEL_YN` char(1) DEFAULT 'N',
                                  `IP_CNT` decimal(22,0) DEFAULT NULL,
                                  `DC_ID` varchar(100) DEFAULT NULL,
                                  `ICON` blob,
                                  `FORM_INFO` blob,
                                  `FEE` decimal(22,0) DEFAULT '0',
                                  `VER` varchar(50) DEFAULT NULL,
                                  `KUBUN` varchar(10) DEFAULT 'S',
                                  `ICON_COLOR` varchar(50) DEFAULT NULL,
                                  `DEPLOY_TIME` int(11) DEFAULT '5',
                                  PRIMARY KEY (`CATALOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vra_catalog_drv definition

CREATE TABLE `nc_vra_catalog_drv` (
                                      `DRV_ID` varchar(20) NOT NULL,
                                      `DRV_NM` varchar(100) DEFAULT NULL,
                                      `DRV_CMT` varchar(4000) DEFAULT NULL,
                                      `CATALOG_ID` varchar(60) DEFAULT NULL,
                                      `FIXED_JSON` varchar(1000) DEFAULT NULL,
                                      `UPD_ID` varchar(10) DEFAULT NULL,
                                      `INS_PGM` varchar(30) DEFAULT NULL,
                                      `UPD_PGM` varchar(30) DEFAULT NULL,
                                      `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      `INS_IP` varchar(40) DEFAULT NULL,
                                      `UPD_IP` varchar(40) DEFAULT NULL,
                                      `INS_ID` varchar(10) DEFAULT NULL,
                                      `DEL_YN` char(1) DEFAULT 'N',
                                      `ICON` blob,
                                      `ICON_COLOR` varchar(50) DEFAULT NULL,
                                      `ALL_USE_YN` varchar(1) DEFAULT 'N',
                                      PRIMARY KEY (`DRV_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vra_catalog_step definition

CREATE TABLE `nc_vra_catalog_step` (
                                       `CATALOG_ID` varchar(60) NOT NULL,
                                       `STEP_SEQ` decimal(2,0) NOT NULL,
                                       `STEP_NM` varchar(200) NOT NULL,
                                       `TAGS` varchar(100) DEFAULT NULL,
                                       `INPUTS` varchar(4000) DEFAULT NULL,
                                       PRIMARY KEY (`CATALOG_ID`,`STEP_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vra_catalog_team definition

CREATE TABLE `nc_vra_catalog_team` (
                                       `CATALOG_ID` varchar(60) NOT NULL,
                                       `TEAM_CD` varchar(15) NOT NULL,
                                       PRIMARY KEY (`CATALOG_ID`,`TEAM_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- nc_vra_catalogreq definition

CREATE TABLE `nc_vra_catalogreq` (
                                     `CATALOGREQ_ID` varchar(20) NOT NULL,
                                     `TEAM_CD` varchar(10) NOT NULL,
                                     `CATALOG_ID` varchar(60) NOT NULL,
                                     `PURPOSE` varchar(100) DEFAULT NULL,
                                     `CMT` varchar(1000) DEFAULT NULL,
                                     `DEL_YN` char(1) DEFAULT 'N',
                                     `TASK_STATUS_CD` char(1) NOT NULL DEFAULT 'W',
                                     `FAIL_MSG` varchar(1000) DEFAULT NULL,
                                     `VRA_REQUEST_ID` varchar(80) DEFAULT NULL,
                                     `INS_DT` varchar(14) DEFAULT NULL,
                                     `UPD_ID` varchar(10) DEFAULT NULL,
                                     `INS_PGM` varchar(30) DEFAULT NULL,
                                     `UPD_PGM` varchar(30) DEFAULT NULL,
                                     `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                     `UPD_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                     `INS_IP` varchar(40) DEFAULT NULL,
                                     `UPD_IP` varchar(40) DEFAULT NULL,
                                     `INS_ID` varchar(10) DEFAULT NULL,
                                     `DATA_JSON` varchar(1000) DEFAULT NULL,
                                     `REQ_TITLE` varchar(200) DEFAULT NULL,
                                     `P_KUBUN` varchar(10) DEFAULT NULL,
                                     `USE_MM` decimal(22,0) DEFAULT NULL,
                                     `CATEGORY` varchar(50) DEFAULT NULL,
                                     `FAB` varchar(10) DEFAULT NULL,
                                     PRIMARY KEY (`CATALOGREQ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='vra 카탈로그 결재요청';


-- nc_vra_catalogreq_req definition

CREATE TABLE `nc_vra_catalogreq_req` (
                                         `CATALOGREQ_ID` varchar(20) NOT NULL,
                                         `TEAM_CD` varchar(10) NOT NULL,
                                         `CATALOG_ID` varchar(60) NOT NULL,
                                         `PURPOSE` varchar(100) DEFAULT NULL,
                                         `CMT` varchar(1000) DEFAULT NULL,
                                         `CUD_CD` varchar(1) DEFAULT NULL,
                                         `APPR_STATUS_CD` varchar(1) DEFAULT NULL,
                                         `INS_DT` varchar(14) NOT NULL,
                                         `INS_PGM` varchar(30) DEFAULT NULL,
                                         `INS_TMS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                         `INS_IP` varchar(40) DEFAULT NULL,
                                         `INS_ID` varchar(10) DEFAULT NULL,
                                         `DATA_JSON` varchar(1000) DEFAULT NULL,
                                         `PREDICT_DD_FEE` decimal(8,0) DEFAULT NULL COMMENT '예상일일금액',
                                         `MONTH` varchar(3) DEFAULT NULL,
                                         `REQ_TITLE` varchar(200) DEFAULT NULL,
                                         `P_KUBUN` varchar(10) DEFAULT NULL,
                                         `USE_MM` decimal(22,0) DEFAULT NULL,
                                         `CATEGORY` varchar(50) DEFAULT NULL,
                                         `FAB` varchar(10) DEFAULT NULL,
                                         PRIMARY KEY (`CATALOGREQ_ID`,`INS_DT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/***********************************************Procedures생성*****************************************************************************/
DELIMITER $$
CREATE FUNCTION `datediff`( date1 TIMESTAMP,  date2 TIMESTAMP) RETURNS decimal(10,0)
BEGIN
RETURN (CAST(date1 AS DATETIME) - CAST(date2 AS DATETIME)) /60/60/24 ;
END$$

CREATE FUNCTION `dept_connect_by_parent`() RETURNS int(11)
                                                      READS SQL DATA
BEGIN

    DECLARE _id INT;

    DECLARE _parent INT;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET @id = NULL;



    SET _parent = @id;

    SET _id = -1;



    IF @id IS NULL THEN

        RETURN NULL;

END IF;



    LOOP

SELECT  MIN(TEAM_CD)

INTO    @id

FROM    FM_TEAM

WHERE   PARENT_CD = _parent

            AND TEAM_CD > _id;



IF @id IS NOT NULL OR _parent = @start_with THEN

            SET @level = @level + 1;

RETURN @id;

END IF;



        SET @level := @level - 1;



SELECT  TEAM_CD, PARENT_CD

INTO    _id, _parent

FROM    FM_TEAM

WHERE   TEAM_CD = _parent;

END LOOP;

END$$

CREATE FUNCTION `fn_date_add`( date1 TIMESTAMP,  dateNum2 integer, kubun varchar(10)) RETURNS datetime
begin
	if kubun = 'DAY' then
		return DATE_ADD(date1, INTERVAL dateNum2 DAY);
	elseif kubun='MONTH' then

		return DATE_ADD(date1, INTERVAL dateNum2 MONTH);
	elseif kubun = 'DAY' then
		return DATE_ADD(date1, INTERVAL dateNum2*365 DAY);
end if;
END$$

CREATE FUNCTION `fn_img_nm_by_lvl`(a_id varchar(30), a_lvl int) RETURNS varchar(30) CHARSET utf8
    READS SQL DATA
BEGIN

DECLARE _parent varchar(100);

select NM  into _parent from (
    SELECT   func._id as ID,  func._nm as NM, @rownum:=@rownum+1 as lvl FROM
    (
    SELECT B._id
    FROM (
    SELECT @r AS _id,  @nm as _nm,
    ( SELECT @r := PAR_CATEGORY_ID, @nm := CATEGORY_NM  FROM nc_img_category  WHERE CATEGORY_ID = _id ) AS parent,
    @l := @l +1 AS lv
    FROM
    (
    SELECT @r := a_id,
    @l := 0
    ) vars,
    nc_img_category d
    WHERE @r <> 0
    ORDER BY lv DESC
    ) B
    ) func Join nc_img_category d
    ON func._id = d.CATEGORY_ID,  (select @rownum:=0) t
    ) t where lvl=a_lvl;

return _parent;


END$$

CREATE FUNCTION `fn_parent_dept_id_by_lvl`(a_id varchar(30), a_lvl int) RETURNS varchar(30) CHARSET utf8
    READS SQL DATA
BEGIN

DECLARE _parent varchar(30);

select ID into _parent from (
    SELECT   func._id as ID,   @rownum:=@rownum+1 as lvl FROM
    (
    SELECT B._id
    FROM (
    SELECT @r AS _id,
    ( SELECT @r := PARENT_CD  FROM FM_TEAM  WHERE TEAM_CD = _id ) AS parent,
    @l := @l +1 AS lv
    FROM
    (
    SELECT @r := a_id,
    @l := 0
    ) vars,
    FM_TEAM d
    WHERE @r <> 0
    ORDER BY lv DESC
    ) B
    ) func Join FM_TEAM d
    ON func._id = d.TEAM_CD,  (select @rownum:=0) t
    ) t where lvl=a_lvl;

return _parent;


END$$

CREATE FUNCTION `fn_to_date`( date1 varchar(14)) RETURNS datetime
BEGIN
RETURN STR_TO_DATE(date1,'%Y%m%d%H%i%s') ;
END$$

CREATE FUNCTION `get_alarm`(DC_ID1 varchar(20), OBJ_ID varchar(30)) RETURNS varchar(400) CHARSET utf8
begin

		declare result1 varchar(400) DEFAULT '';
SELECT group_concat(ALARM_NM SEPARATOR ',') INTO result1 FROM NC_HV_ALARM  WHERE DC_ID=DC_ID1 AND TARGET_ID=OBJ_ID;
RETURN result1;
end$$

CREATE FUNCTION `get_currentTimeMillis`() RETURNS bigint(20) unsigned
begin
return UNIX_TIMESTAMP(CURRENT_TIMESTAMP )*1000;
end$$

CREATE FUNCTION `get_ddic_nm`(DD_ID1 varchar(20), DD_VALUE1 varchar(30), LANG1 VARCHAR(2), KO_NM varchar(500)) RETURNS varchar(500) CHARSET utf8
BEGIN
DECLARE	result1 varchar(500) DEFAULT KO_NM;
if LANG1 IS NULL OR LANG1='ko' THEN
	if KO_NM is null then

SELECT KO_DD_DESC INTO result1 FROM FM_DDIC WHERE DD_ID=DD_ID1 AND DD_VALUE = DD_VALUE1;
end if;
ELSE
SELECT NM INTO result1 FROM FM_NL WHERE TABLE_NM='FM_DDIC' AND FIELD_NM = 'DD_VALUE' AND ID=concat(concat(DD_ID1, '.'), DD_VALUE1) AND LANG_TYPE=LANG1;
end if;
return result1;
END$$

CREATE FUNCTION `get_img_category_ids`(  node varchar(30) ) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
BEGIN
    DECLARE _path varchar(200);
	DECLARE _cpath varchar(200);
    DECLARE _id varchar(30);
	DECLARE _nm varchar(100);
    DECLARE EXIT HANDLER FOR NOT FOUND RETURN substring(_path, 1, length(_path)-1);
    SET _id =   COALESCE(node, @id);
    SET _path = '';
    loop

SELECT   CATEGORY_ID, PAR_CATEGORY_ID
INTO   _nm, _id
FROM    NC_IMG_CATEGORY
WHERE   CATEGORY_ID = _id
  AND COALESCE(CATEGORY_ID <> @start_with, TRUE);
SET _path = CONCAT(_nm, ',',  _path);



END LOOP;
END$$

CREATE FUNCTION `get_img_category_nm`(  node varchar(30) ) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
BEGIN
    DECLARE _path varchar(800);
select substring(name_list,1, CHAR_LENGTH(name_list)-1) into _path from  (
    SELECT  hi.CATEGORY_ID AS id,
    img_category_by_path('>', hi.CATEGORY_ID) AS name_list,
    PAR_CATEGORY_ID
    FROM    (
    SELECT  CATEGORY_ID,
    CAST(@level AS SIGNED) AS level
    FROM    (
    SELECT  @start_with := 0,
    @id := @start_with,
    @level := 0
    ) vars, NC_IMG_CATEGORY
    WHERE   @id IS NOT NULL
    ) ho
    JOIN    NC_IMG_CATEGORY hi
    ON      hi.CATEGORY_ID = ho.CATEGORY_ID
    ) a where id=node;
return _path;
END$$

CREATE FUNCTION `get_next_approver`(`_Id` BIGINT) RETURNS varchar(200) CHARSET utf8
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE result VARCHAR(200) DEFAULT "";
DECLARE nm VARCHAR(50);
DECLARE c1 CURSOR FOR
SELECT USER_NAME
FROM FM_USER a, NC_REQ_NEXT_APPROVER b
WHERE a.USER_ID=b.APPROVER and b.REQ_ID=_Id ;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;



OPEN c1;
get_nm: LOOP
   FETCH c1 INTO nm;


	IF done = TRUE THEN
		 LEAVE get_nm;
END IF;
 -- build email list
   if result != '' then
   	set result  = concat (result , ",");
end if;
 	SET result = CONCAT(result,nm);
END LOOP get_nm;

CLOSE c1;

RETURN result;
END$$

CREATE FUNCTION `get_old_spec_nm`( vmId VARCHAR(20), intDt varchar(14)) RETURNS varchar(200) CHARSET utf8
begin

declare 	result1 VARCHAR(200) DEFAULT '';


SELECT  get_spec_nm(SPEC_ID, CPU_CNT, RAM_SIZE,  DISK_SIZE, DISK_UNIT) SPEC_INFO into result1
FROM NC_VM_REQ WHERE VM_ID=vmId AND INS_DT = ( SELECT max(INS_DT) FROM NC_VM_REQ WHERE  VM_ID=vmId AND INS_DT < intDT ) and APPR_STATUS_CD='A';

if result1 is null or result1 = '' then
SELECT  get_spec_nm(SPEC_ID, CPU_CNT, RAM_SIZE,  DISK_SIZE, DISK_UNIT) into result1  from NC_VM WHERE VM_ID=vmId;
end if;

RETURN result1;


end$$

CREATE FUNCTION `get_org_nm`( _team_cd  varchar(20)) RETURNS varchar(200) CHARSET utf8
BEGIN
DECLARE result1 VARCHAR(200) DEFAULT '';
select comp_nm into result1 from FM_COMPANY a, FM_TEAM b where a.COMP_ID=b.COMP_ID and b.TEAM_CD=_team_cd;
RETURN result1;
END$$

CREATE FUNCTION `get_spec_nm`( specId VARCHAR(20) , cpuCnt INT, ramSize INT, diskSize INT ,  unit VARCHAR(1) , gpu_size INT) RETURNS varchar(600) CHARSET utf8
BEGIN
     declare result1 VARCHAR(200) DEFAULT '';
    IF specId = '0' then
       return concat( cpuCnt,  'vCore ', ramSize , 'GB RAM, ', CASE WHEN MOD(diskSize,1024)=0 THEN  concat(format(diskSize/1024,0), 'TB')  ELSE  concat(format(diskSize,0),'GB') end, 'Disk');
else
SELECT concat(SPEC_NM, '(', ifnull(cpuCnt, CPU_CNT), 'vCore, ', ifnull(ramSize,RAM_SIZE), 'GB Mem, ',
              CASE WHEN diskSize>0 and MOD(diskSize,1024)=0 THEN  concat(format(diskSize/1024,0), 'TB ',  DISK_TYPE_NM ,'Disk')
                   when diskSize>0 then concat(format(diskSize,0),'GB ',  DISK_TYPE_NM ,'Disk')  ELSE concat(NC_VM_SPEC.DD_FEE, '원/day')  end
           ,')')
INTO result1 FROM NC_VM_SPEC,  NC_DISK_TYPE d
WHERE d.DISK_TYPE_ID=NC_VM_SPEC.DISK_TYPE_ID  AND SPEC_ID=specId;
end if;
RETURN result1;


END$$

CREATE FUNCTION `GET_TASK_STATUS_CD`(svc_cd1 varchar(100), svc_id1 varchar(100), ins_dt1 varchar(100)) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
BEGIN
	declare result1 char(1) DEFAULT '';
	declare cnt int(2) DEFAULT 0;
SELECT max(TASK_STATUS_CD) INTO result1  FROM NC_DEPLOY_FAIL WHERE SVC_CD=svc_cd1 AND SVC_ID = svc_id1 AND INS_DT = ins_dt1  ;
IF result1 IS NULL THEN
	IF svc_cd1 IN ('E','V') THEN
		SET result1 := 'S';
ELSE
SELECT TASK_STATUS_CD INTO result1 FROM V_NC_RSC WHERE SVC_CD=svc_cd1 AND ID = svc_id1;
END IF;
END IF;
IF result1 = 'R' THEN
	set result1 := 'S';
END IF;
return result1;
END$$

CREATE FUNCTION `get_team_mngr`( team_cd1  varchar(20) ) RETURNS varchar(200) CHARSET utf8
BEGIN
DECLARE result1 VARCHAR(200) DEFAULT '';




select  GROUP_CONCAT( USER_NAME SEPARATOR ',')   into result1 from FM_USER TU where  TU.TEAM_CD = team_cd1 and TU.USER_TYPE = '21' ;

RETURN result1;
END$$

CREATE FUNCTION `get_template_nm`(os_id1 varchar(30) , from_id1 VARCHAR(30)) RETURNS varchar(200) CHARSET utf8
BEGIN
 declare result1 VARCHAR(200) DEFAULT '';
if from_id1 IS NULL THEN
	IF os_id1 is NULL THEN
		set result1  = '';
ELSE
SELECT NC_OS_TYPE.OS_NM  into result1 FROM NC_OS_TYPE WHERE NC_OS_TYPE.OS_ID = os_id1;
END IF;
ELSEif from_id1 IS not NULL THEN
	IF SUBSTR(from_id1, 1, 1) = 'S' THEN SELECT NC_VM.VM_NM into result1 FROM NC_VM WHERE NC_VM.VM_ID = from_id1;
ELSEIF SUBSTR(from_id1, 1, 1) = 'G' THEN SELECT NC_IMG.IMG_NM into result1 FROM NC_IMG WHERE NC_IMG.IMG_ID = from_id1;
END IF;
end if;
return result1;
END$$

CREATE FUNCTION `GET_VM_DD_FEE`( ID1 VARCHAR(30)  , DC_ID1 VARCHAR(30)  , CPU_CNT1 integer, RAM_SIZE1 integer, DISK_TYPE_ID1 varchar(20), DISK_SIZE1 integer   , SPEC_ID1 VARCHAR(30)  ) RETURNS int(11)
BEGIN
declare DISK_FEE int default 0;
declare SPEC_FEE int default 0;
declare EXTRA_DISK_SIZE int default DISK_SIZE1;

  IF SPEC_ID1 = '0' THEN
SELECT (SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD='C') * CPU_CNT1 + (SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD='M') *RAM_SIZE1+ (select DD_FEE from NC_DISK_TYPE b  where  b.DISK_TYPE_ID=IFNULL( DISK_TYPE_ID1,'1')) * DISK_SIZE1 INTO  SPEC_FEE FROM DUAL;
RETURN SPEC_FEE;
elseif SPEC_ID1  is not null and SPEC_ID1 != '' then
select b.DD_FEE  , a.DD_FEE  , DISK_SIZE1 -  a.DISK_SIZE
into DISK_FEE, SPEC_FEE,EXTRA_DISK_SIZE from NC_VM_SPEC a, NC_DISK_TYPE b  where SPEC_ID=SPEC_ID1 and a.DISK_TYPE_ID=b.DISK_TYPE_ID	;
else
select DD_FEE into DISK_FEE from NC_DISK_TYPE b, NC_DC a where a.IMG_DISK_TYPE_ID=b.DISK_TYPE_ID and a.DC_ID = DC_ID1;
end if;
 if EXTRA_DISK_SIZE > 0 then
  	return SPEC_FEE + (EXTRA_DISK_SIZE*DISK_FEE);
else
 	return SPEC_FEE;
end if;
END$$

CREATE FUNCTION `get_vm_ip`( dc_id1  varchar(20), vm_nm1 varchar(200),  vm_id1 varchar(20)) RETURNS varchar(200) CHARSET utf8
BEGIN
DECLARE result1 VARCHAR(200) DEFAULT '';


if vm_nm1 is   null then
select VM_NM , DC_ID into vm_nm1, dc_id1 from NC_VM where VM_ID= vm_id1;
end if;

select  GROUP_CONCAT( IP SEPARATOR ',')   into result1 from NC_IP where DC_ID=dc_id1 and VM_NM = vm_nm1  ;

RETURN result1;
END$$

CREATE FUNCTION `img_category_by_path`(delimiter varchar(10), node varchar(30) ) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
BEGIN
     DECLARE _path varchar(200);
	DECLARE _cpath varchar(200);
    DECLARE _id varchar(30);
	DECLARE _nm varchar(100);
    DECLARE EXIT HANDLER FOR NOT FOUND RETURN _path;
    SET _id =   COALESCE(node, @id);
    SET _path = '';
    LOOP
SELECT   CATEGORY_NM, PAR_CATEGORY_ID
INTO   _nm, _id
FROM    NC_IMG_CATEGORY
WHERE   CATEGORY_ID = _id
  AND COALESCE(CATEGORY_ID <> @start_with, TRUE);
SET _path = CONCAT(_nm, delimiter, _path);
END LOOP;
END$$

CREATE FUNCTION `nvl`(s decimal(22), d decimal(22)) RETURNS decimal(22,0)
begin
return ifnull(s,d);
end$$
DELIMITER ;


/***********************************************View생성*****************************************************************************/

-- v_fm_team source

create or replace
algorithm = UNDEFINED view `v_fm_team` as
select
    `t`.`TEAM_CD` as `TEAM_CD`,
    `t`.`COMP_ID` as `COMP_ID`,
    `t`.`TEAM_NM` as `TEAM_NM`,
    `t`.`USE_YN` as `USE_YN`,
    `t`.`TEAM_OWNER` as `TEAM_OWNER`,
    `t`.`PARENT_CD` as `PARENT_CD`,
    `t`.`INS_TMS` as `INS_TMS`,
    `t`.`INS_PGM` as `INS_PGM`,
    `t`.`INS_ID` as `INS_ID`,
    `t`.`INS_IP` as `INS_IP`,
    `t`.`UPD_TMS` as `UPD_TMS`,
    `t`.`UPD_PGM` as `UPD_PGM`,
    `t`.`UPD_ID` as `UPD_ID`,
    `t`.`UPD_IP` as `UPD_IP`,
    `t`.`ETC_CD` as `ETC_CD`
from
    `fm_team` `t`
where
    exists(
            select
                `a`.`TEAM_CD`
            from
                `nc_vm` `a`
            where
                (`a`.`TEAM_CD` = `t`.`TEAM_CD`));


-- v_nc_fw source

create or replace
algorithm = UNDEFINED view `v_nc_fw` as
select
    `fw`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `fw`.`TASK_STATUS_CD` as `TASK_STATUS_CD`,
    `fw`.`CIDR` as `CIDR`,
    `fw`.`PORT` as `PORT`,
    `fw`.`VM_USER_YN` as `VM_USER_YN`,
    `fw`.`OLD_PORT` as `OLD_PORT`,
    `fw`.`OLD_VM_USER_YN` as `OLD_VM_USER_YN`,
    `fw`.`FW_USER_ID` as `FW_USER_ID`,
    `fw`.`VM_ID` as `VM_ID`,
    `fw`.`FW_ID` as `FW_ID`,
    `fw`.`CUD_CD` as `CUD_CD`,
    `fw`.`INS_DT` as `INS_DT`,
    `fw`.`INS_ID` as `INS_ID`,
    `fw`.`FAIL_MSG` as `FAIL_MSG`,
    `fw`.`SERVER_IP` as `SERVER_IP`,
    `fw`.`CMT` as `CMT`,
    `fw`.`USE_MM` as `USE_MM`,
    `fw`.`EXPIRE_DT` as `EXPIRE_DT`,
    `t`.`TEAM_CD` as `TEAM_CD`,
    `t`.`TEAM_NM` as `TEAM_NM`,
    `u`.`POSITION` as `POSITION`,
    `u`.`USER_NAME` as `USER_NAME`,
    `u`.`LOGIN_ID` as `LOGIN_ID`,
    `t2`.`TEAM_CD` as `INS_TEAM_CD`,
    `t2`.`TEAM_NM` as `INS_TEAM_NM`,
    `u2`.`POSITION` as `INS_POSITION`,
    `u2`.`USER_NAME` as `INS_NAME`,
    `u2`.`LOGIN_ID` as `INS_LOGIN_ID`
from
    ((((((
        select
            `r`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
            `nc_fw`.`TASK_STATUS_CD` as `TASK_STATUS_CD`,
            `nc_fw`.`CIDR` as `CIDR`,
            `nc_fw`.`PORT` as `OLD_PORT`,
            `nc_fw`.`PORT` as `PORT`,
            `nc_fw`.`VM_USER_YN` as `VM_USER_YN`,
            `nc_fw`.`VM_USER_YN` as `OLD_VM_USER_YN`,
            `nc_fw`.`FW_USER_ID` as `FW_USER_ID`,
            `nc_fw`.`VM_ID` as `VM_ID`,
            `nc_fw`.`FW_ID` as `FW_ID`,
            `r`.`CUD_CD` as `CUD_CD`,
            `nc_fw`.`INS_DT` as `INS_DT`,
            `nc_vm`.`INS_ID` as `INS_ID`,
            `nc_fw`.`FAIL_MSG` as `FAIL_MSG`,
            `get_vm_ip`(`nc_vm`.`DC_ID`,
                        `nc_vm`.`VM_NM`,
                        `nc_fw`.`VM_ID`) as `SERVER_IP`,
            `nc_fw`.`CMT` as `CMT`,
            `nc_fw`.`USE_MM` as `USE_MM`,
            (case
                 when isnull(`nc_fw`.`USE_MM`) then null
                 else (`nc_fw`.`INS_TMS` + interval `nc_fw`.`USE_MM` month)
                end) as `EXPIRE_DT`
        from
            ((`nc_fw`
                left join `nc_vm` on
                    ((`nc_fw`.`VM_ID` = `nc_vm`.`VM_ID`)))
                left join `nc_fw_req` `r` on
                (((`nc_fw`.`FW_ID` = `r`.`FW_ID`)
                    and (`r`.`APPR_STATUS_CD` in ('R', 'W')))))
        where
            (`nc_fw`.`DEL_YN` = 'N'))) `fw`
        left join `fm_user` `u` on
            ((`u`.`USER_ID` = `fw`.`FW_USER_ID`)))
        left join `fm_user` `u2` on
            ((`u2`.`USER_ID` = `fw`.`INS_ID`)))
        left join `fm_team` `t` on
            ((`t`.`TEAM_CD` = `u`.`TEAM_CD`)))
        left join `fm_team` `t2` on
        ((`t2`.`TEAM_CD` = `u2`.`TEAM_CD`)));


-- v_nc_img source

create or replace
algorithm = UNDEFINED view `v_nc_img` as
select
    `nc_img`.`IMG_ID` as `IMG_ID`,
    `nc_img`.`IMG_NM` as `IMG_NM`,
    `nc_img`.`CMT` as `CMT`,
    `nc_img`.`DISK_SIZE` as `DISK_SIZE`,
    `nc_img`.`FEE_TYPE_CD` as `FEE_TYPE_CD`,
    `nc_img`.`HH_FEE` as `HH_FEE`,
    `nc_img`.`DD_FEE` as `DD_FEE`,
    `nc_img`.`MM_FEE` as `MM_FEE`,
    `nc_img`.`FROM_ID` as `FROM_ID`,
    `nc_img`.`DEL_YN` as `DEL_YN`,
    `nc_img`.`TEAM_CD` as `TEAM_CD`,
    `nc_img`.`DC_ID` as `DC_ID`,
    `nc_img`.`OS_ID` as `OS_ID`,
    `nc_img`.`INS_ID` as `INS_ID`,
    `r`.`CUD_CD` as `CUD_CD`,
    `r`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `nc_img`.`TASK_STATUS_CD` as `TASK_STATUS_CD`,
    `nc_img`.`FAIL_MSG` as `FAIL_MSG`,
    `nc_img`.`USE_MM` as `USE_MM`,
    `nc_img`.`INS_DT` as `INS_DT`,
    `nc_img`.`CATEGORY` as `CATEGORY`,
    `nc_img`.`P_KUBUN` as `P_KUBUN`,
    `nc_img`.`PURPOSE` as `PURPOSE`,
    `nc_img`.`SPEC_ID` as `SPEC_ID`
from
    (`nc_img`
        left join `nc_img_req` `r` on
            (((`nc_img`.`IMG_ID` = `r`.`IMG_ID`)
                and (`r`.`APPR_STATUS_CD` in ('W', 'R')))));

-- v_nc_rsc source

create or replace
algorithm = UNDEFINED view `v_nc_rsc` as
select
    'S' as `SVC_CD`,
    `nc_vm`.`VM_ID` as `ID`,
    `nc_vm`.`VM_NM` as `TITLE`,
    `nc_vm`.`RUN_CD` as `TASK_STATUS_CD`,
    `nc_vm`.`DC_ID` as `DC_ID`,
    `nc_vm`.`TEAM_CD` as `TEAM_CD`,
    `nc_vm`.`DEL_YN` as `DEL_YN`,
    `nc_vm`.`INS_ID` as `INS_ID`,
    `nc_vm`.`CATEGORY` as `CATEGORY`
from
    `nc_vm`
union all
select
    'G' as `SVC_CD`,
    `nc_img`.`IMG_ID` as `ID`,
    `nc_img`.`IMG_NM` as `TITLE`,
    `nc_img`.`TASK_STATUS_CD` as `TASK_STATUS_CD`,
    `nc_img`.`DC_ID` as `DC_ID`,
    `nc_img`.`TEAM_CD` as `TEAM_CD`,
    `nc_img`.`DEL_YN` as `DEL_YN`,
    `nc_img`.`INS_ID` as `INS_ID`,
    `nc_img`.`CATEGORY` as `CATEGORY`
from
    `nc_img`
union all
select
    'F' as `SVC_CD`,
    `a`.`FW_ID` as `FW_ID`,
    concat(concat(concat(concat(`a`.`CIDR`, '->'), `get_vm_ip`(null, null, `a`.`VM_ID`)), ':'), `a`.`PORT`) as `TITLE`,
    `a`.`TASK_STATUS_CD` as `TASK_STATUS_CD`,
    `b`.`DC_ID` as `DC_ID`,
    `b`.`TEAM_CD` as `TEAM_CD`,
    `a`.`DEL_YN` as `DEL_YN`,
    `a`.`INS_ID` as `INS_ID`,
    `b`.`CATEGORY` as `CATEGORY`
from
    (`nc_fw` `a`
        left join `nc_vm` `b` on
        ((`a`.`VM_ID` = `b`.`VM_ID`)));



-- v_nc_req_detail source

create or replace
algorithm = UNDEFINED view `v_nc_req_detail` as
select
    `nc_vm_req`.`VM_ID` as `SVC_ID`,
    'S' as `SVC_CD`,
    `nc_vm_req`.`VM_NM` as `SVC_NM`,
    `nc_vm_req`.`CUD_CD` as `CUD_CD`,
    `nc_vm_req`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `nc_vm_req`.`INS_ID` as `INS_ID`,
    `nc_vm_req`.`INS_DT` as `INS_DT`,
    `nc_vm_req`.`DC_ID` as `DC_ID`,
    `nc_vm_req`.`CATEGORY` as `CATEGORY`
from
    `nc_vm_req`
union all
select
    `nc_fw_req`.`FW_ID` as `SVC_ID`,
    'F' as `SVC_CD`,
    concat(concat(concat(concat(`nc_fw_req`.`CIDR`, '->'), `get_vm_ip`(null, null, `nc_fw_req`.`VM_ID`)), ':'), `nc_fw_req`.`PORT`) as `SVC_NM`,
    `nc_fw_req`.`CUD_CD` as `CUD_CD`,
    `nc_fw_req`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `nc_fw_req`.`INS_ID` as `INS_ID`,
    `nc_fw_req`.`INS_DT` as `INS_DT`,
    null as `DC_ID`,
    null as `CATEGORY`
from
    `nc_fw_req`
where
    exists(
            select
                `nc_vm`.`VM_ID`
            from
                `nc_vm`
            where
                (`nc_vm`.`VM_ID` = `nc_fw_req`.`VM_ID`))
union all
select
    `r`.`SVC_ID` as `SVC_ID`,
    `r`.`SVC_TYPE` as `SVC_CD`,
    concat(concat(concat(concat(`get_ddic_nm`('SVC', `v`.`SVC_CD`, 'ko', null), concat(concat('(', `v`.`TITLE`), ')')), '/'), `r`.`ADD_USE_MM`), '개월 기간연장') as `SVC_NM`,
    `r`.`CUD_CD` as `CUD_CD`,
    `r`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `r`.`INS_ID` as `INS_ID`,
    `r`.`INS_DT` as `INS_DT`,
    `v`.`DC_ID` as `DC_ID`,
    `v`.`CATEGORY` as `CATEGORY`
from
    (`nc_extend_period_req` `r`
        join `v_nc_rsc` `v`)
where
    ((`r`.`SVC_ID` = `v`.`ID`)
        and (`r`.`SVC_TYPE` = 'E'))
union all
select
    `r`.`SVC_ID` as `SVC_ID`,
    `r`.`SVC_TYPE` as `SVC_CD`,
    concat(concat(`get_ddic_nm`('SVC', `v`.`SVC_CD`, 'ko', null), concat(concat('(', `v`.`TITLE`), ')')), ' 복원') as `SVC_NM`,
    `r`.`CUD_CD` as `CUD_CD`,
    `r`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `r`.`INS_ID` as `INS_ID`,
    `r`.`INS_DT` as `INS_DT`,
    `v`.`DC_ID` as `DC_ID`,
    `v`.`CATEGORY` as `CATEGORY`
from
    (`nc_extend_period_req` `r`
        join `v_nc_rsc` `v`)
where
    ((`r`.`SVC_ID` = `v`.`ID`)
        and (`r`.`SVC_TYPE` = 'V'));

-- v_nc_vm source

create or replace
algorithm = UNDEFINED view `v_nc_vm` as
select
    `nc_vm`.`VM_ID` as `VM_ID`,
    `nc_vm`.`VM_NM` as `VM_NM`,
    `nc_vm`.`VM_NM` as `OLD_VM_NM`,
    `nc_vm`.`DC_ID` as `DC_ID`,
    `nc_vm`.`TEAM_CD` as `TEAM_CD`,
    `nc_vm`.`CMT` as `CMT`,
    `nc_vm`.`RUN_CD` as `RUN_CD`,
    `r`.`CUD_CD` as `CUD_CD`,
    `r`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `nc_vm`.`INS_PGM` as `INS_PGM`,
    `nc_vm`.`INS_TMS` as `INS_TMS`,
    `nc_vm`.`INS_IP` as `INS_IP`,
    `nc_vm`.`INS_ID` as `INS_ID`,
    `nc_vm`.`FEE_TYPE_CD` as `FEE_TYPE_CD`,
    `nc_vm`.`OS_HH_FEE` as `OS_HH_FEE`,
    `nc_vm`.`SPEC_HH_FEE` as `SPEC_HH_FEE`,
    `nc_vm`.`OS_DD_FEE` as `OS_DD_FEE`,
    `nc_vm`.`SPEC_DD_FEE` as `SPEC_DD_FEE`,
    `nc_vm`.`SPEC_ID` as `SPEC_ID`,
    `nc_vm`.`SPEC_ID` as `OLD_SPEC_ID`,
    `nc_vm`.`OS_ID` as `OS_ID`,
    `nc_vm`.`OS_MM_FEE` as `OS_MM_FEE`,
    `nc_vm`.`SPEC_MM_FEE` as `SPEC_MM_FEE`,
    `nc_vm`.`FROM_ID` as `FROM_ID`,
    `nc_vm`.`DISK_TYPE_ID` as `DISK_TYPE_ID`,
    `nc_vm`.`DISK_TYPE_ID` as `OLD_DISK_TYPE_ID`,
    `get_template_nm`(`nc_vm`.`OS_ID`,
                      `nc_vm`.`FROM_ID`) as `TMPL_NM`,
    `nc_vm`.`CPU_CNT` as `OLD_CPU_CNT`,
    `nc_vm`.`RAM_SIZE` as `OLD_RAM_SIZE`,
    `nc_vm`.`DISK_SIZE` as `OLD_DISK_SIZE`,
    'G' as `OLD_DISK_UNIT`,
    `nc_vm`.`CPU_CNT` as `CPU_CNT`,
    `nc_vm`.`RAM_SIZE` as `RAM_SIZE`,
    `nc_vm`.`DISK_SIZE` as `DISK_SIZE`,
    'G' as `DISK_UNIT`,
    `nc_vm`.`STOP_DD_FEE` as `STOP_DD_FEE`,
    `nc_vm`.`STOP_HH_FEE` as `STOP_HH_FEE`,
    `nc_vm`.`STOP_MM_FEE` as `STOP_MM_FEE`,
    `nc_vm`.`FAIL_MSG` as `FAIL_MSG`,
    `nc_vm`.`INS_DT` as `INS_DT`,
    `nc_vm`.`VM_HV_ID` as `VM_HV_ID`,
    `nc_vm`.`LAST_USE_TMS` as `LAST_USE_TMS`,
    `nc_vm`.`PURPOSE` as `PURPOSE`,
    `nc_vm`.`GUEST_NM` as `GUEST_NM`,
    `nc_vm`.`CATEGORY` as `CATEGORY`,
    `nc_vm`.`P_KUBUN` as `P_KUBUN`,
    `nc_vm`.`USE_MM` as `USE_MM`,
    `nc_vm`.`NAS` as `NAS`,
    `nc_vm`.`DEL_YN` as `DEL_YN`,
    `nc_vm`.`HOST_HV_ID` as `HOST_HV_ID`,
    `nc_vm`.`EXPIRED_TMS` as `EXPIRED_TMS`,
    `nc_vm`.`THIN_REQ_YN` as `THIN_REQ_YN`,
    `nc_vm`.`THIN_REQ_CMT` as `THIN_REQ_CMT`,
    `nc_vm`.`GPU_SIZE` as `GPU_SIZE`,
    `nc_vm`.`GPU_SIZE` as `OLD_GPU_SIZE`
from
    (`nc_vm`
        left join `nc_vm_req` `r` on
            (((`nc_vm`.`VM_ID` = `r`.`VM_ID`)
                and (`r`.`APPR_STATUS_CD` in ('R', 'W')))));


-- v_nc_vm_user source

create or replace
algorithm = UNDEFINED view `v_nc_vm_user` as
select
    `nc_vm_user`.`VM_ID` as `VM_ID`,
    `nc_vm_user`.`USER_ID` as `USER_ID`,
    `nc_vm_user`.`USER_IP` as `USER_IP`,
    `nc_vm_user`.`PORT` as `PORT`,
    `nc_vm_user`.`UPD_ID` as `UPD_ID`,
    `nc_vm_user`.`INS_PGM` as `INS_PGM`,
    `nc_vm_user`.`UPD_PGM` as `UPD_PGM`,
    `nc_vm_user`.`INS_TMS` as `INS_TMS`,
    `nc_vm_user`.`UPD_TMS` as `UPD_TMS`,
    `nc_vm_user`.`INS_IP` as `INS_IP`,
    `nc_vm_user`.`UPD_IP` as `UPD_IP`,
    `nc_vm_user`.`INS_ID` as `INS_ID`,
    `nc_vm_user`.`INS_DT` as `INS_DT`,
    `nc_vm_user`.`OWNER_YN` as `OWNER_YN`,
    `nc_vm_user`.`TASK_STATUS_CD` as `TASK_STATUS_CD`,
    `nc_vm_user`.`FAIL_MSG` as `FAIL_MSG`,
    `nc_vm_user`.`TASK_FINISH_TMS` as `TASK_FINISH_TMS`,
    `nc_vm_user`.`WORKER_ID` as `WORKER_ID`
from
    `nc_vm_user`
union all
select
    `a`.`VM_ID` as `VM_ID`,
    `a`.`USER_ID` as `USER_ID`,
    `a`.`USER_IP` as `USER_IP`,
    `a`.`PORT` as `PORT`,
    `a`.`UPD_ID` as `UPD_ID`,
    `a`.`INS_PGM` as `INS_PGM`,
    `a`.`UPD_PGM` as `UPD_PGM`,
    `a`.`INS_TMS` as `INS_TMS`,
    `a`.`UPD_TMS` as `UPD_TMS`,
    `a`.`INS_IP` as `INS_IP`,
    `a`.`UPD_IP` as `UPD_IP`,
    `a`.`INS_ID` as `INS_ID`,
    `a`.`INS_DT` as `INS_DT`,
    `a`.`OWNER_YN` as `OWNER_YN`,
    'W' as `TASK_STATUS_CD`,
    '' as `FAIL_MSG`,
    null as `TASK_FINISH_TMS`,
    null as `WORKER_ID`
from
    (`nc_vm_user_req` `a`
        join `nc_vm_req` `b`)
where
    ((`b`.`APPR_STATUS_CD` in ('W', 'R'))
        and (`b`.`CUD_CD` = 'C')
        and (`a`.`VM_ID` = `b`.`VM_ID`)
        and (`a`.`INS_DT` = `b`.`INS_DT`));


-- v_nc_vra_catalog source

create or replace
algorithm = UNDEFINED view `v_nc_vra_catalog` as
select
    `nc_vra_catalog`.`CATALOG_ID` as `CATALOG_ID`,
    `nc_vra_catalog`.`CATALOG_NM` as `CATALOG_NM`,
    `nc_vra_catalog`.`CATALOG_CMT` as `CATALOG_CMT`,
    `nc_vra_catalog`.`DEL_YN` as `DEL_YN`,
    `nc_vra_catalog`.`ALL_USE_YN` as `ALL_USE_YN`,
    `nc_vra_catalog`.`ICON` as `ICON`,
    `nc_vra_catalog`.`FORM_INFO` as `FORM_INFO`,
    `nc_vra_catalog`.`FEE` as `FEE`,
    `nc_vra_catalog`.`KUBUN` as `KUBUN`,
    `nc_vra_catalog`.`VER` as `VER`,
    `nc_vra_catalog`.`DEPLOY_TIME` as `DEPLOY_TIME`,
    `nc_vra_catalog`.`ICON_COLOR` as `ICON_COLOR`,
    null as `FIXED_JSON`,
    null as `ORG_CATALOG_ID`
from
    `nc_vra_catalog`
union all
select
    `m`.`DRV_ID` as `CATALOG_ID`,
    `m`.`DRV_NM` as `CATALOG_NM`,
    `m`.`DRV_CMT` as `CATALOG_CMT`,
    `m`.`DEL_YN` as `DEL_YN`,
    `m`.`ALL_USE_YN` as `ALL_USE_YN`,
    `m`.`ICON` as `ICON`,
    `c`.`FORM_INFO` as `FORM_INFO`,
    `c`.`FEE` as `FEE`,
    `c`.`KUBUN` as `KUBUN`,
    `c`.`VER` as `VER`,
    `c`.`DEPLOY_TIME` as `DEPLOY_TIME`,
    `m`.`ICON_COLOR` as `ICON_COLOR`,
    `m`.`FIXED_JSON` as `FIXED_JSON`,
    `c`.`CATALOG_ID` as `ORG_CATALOG_ID`
from
    (`nc_vra_catalog_drv` `m`
        join `nc_vra_catalog` `c`)
where
    (`m`.`CATALOG_ID` = `c`.`CATALOG_ID`);


-- v_nc_vra_catalogreq source

create or replace
algorithm = UNDEFINED view `v_nc_vra_catalogreq` as
select
    `req`.`CATALOGREQ_ID` as `CATALOGREQ_ID`,
    `req`.`CATALOG_ID` as `CATALOG_ID`,
    `req`.`REQ_TITLE` as `REQ_TITLE`,
    `req`.`P_KUBUN` as `P_KUBUN`,
    `req`.`USE_MM` as `USE_MM`,
    '' as `CUD_CD`,
    '' as `APPR_STATUS_CD`,
    `req`.`TASK_STATUS_CD` as `TASK_STATUS_CD`,
    `req`.`FAIL_MSG` as `FAIL_MSG`,
    `req`.`TEAM_CD` as `TEAM_CD`,
    `req`.`INS_TMS` as `INS_TMS`,
    `req`.`INS_ID` as `INS_ID`,
    `req`.`DEL_YN` as `DEL_YN`
from
    (`nc_vra_catalogreq` `req`
        left join `nc_vra_catalogreq_req` `reqreq` on
        (((`reqreq`.`CATALOGREQ_ID` = `req`.`CATALOGREQ_ID`)
            and (`reqreq`.`APPR_STATUS_CD` in ('R', 'W')))))
where
    isnull(`reqreq`.`CATALOGREQ_ID`)
union all
select
    `reqreq`.`CATALOGREQ_ID` as `CATALOGREQ_ID`,
    `reqreq`.`CATALOG_ID` as `CATALOG_ID`,
    `reqreq`.`REQ_TITLE` as `REQ_TITLE`,
    `req`.`P_KUBUN` as `P_KUBUN`,
    `req`.`USE_MM` as `USE_MM`,
    `reqreq`.`CUD_CD` as `CUD_CD`,
    `reqreq`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    '' as `TASK_STATUS_CD`,
    '' as `FAIL_MSG`,
    `reqreq`.`TEAM_CD` as `TEAM_CD`,
    `reqreq`.`INS_TMS` as `INS_TMS`,
    `reqreq`.`INS_ID` as `INS_ID`,
    'N' as `DEL_YN`
from
    (`nc_vra_catalogreq_req` `reqreq`
        left join `nc_vra_catalogreq` `req` on
        ((`reqreq`.`CATALOGREQ_ID` = `req`.`CATALOGREQ_ID`)))
where
    (`reqreq`.`APPR_STATUS_CD` in ('R', 'W'));


-- v_nc_work source

create or replace
algorithm = UNDEFINED view `v_nc_work` as
select
    `nc_vm_req`.`VM_ID` as `SVC_ID`,
    'S' as `SVC_CD`,
    `nc_vm_req`.`VM_NM` as `SVC_NM`,
    `nc_vm_req`.`CUD_CD` as `CUD_CD`,
    `nc_vm_req`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `nc_vm_req`.`INS_ID` as `INS_ID`,
    `nc_vm_req`.`INS_DT` as `INS_DT`,
    (case
         when (`nc_vm_req`.`CUD_CD` = 'D') then 0
         else `GET_VM_DD_FEE`(`nc_vm_req`.`VM_ID`,
                              `nc_vm_req`.`DC_ID`,
                              `nc_vm_req`.`CPU_CNT`,
                              `nc_vm_req`.`RAM_SIZE`,
                              `nc_vm_req`.`DISK_TYPE_ID`,
                              `nc_vm_req`.`DISK_SIZE`,
                              `nc_vm_req`.`SPEC_ID`)
        end) as `FEE`,
    `nc_vm_req`.`FEE_TYPE_CD` as `FEE_TYPE_CD`
from
    `nc_vm_req`
where
    (`nc_vm_req`.`APPR_STATUS_CD` = 'W')
union all
select
    `nc_img_req`.`IMG_ID` as `SVC_ID`,
    'G' as `SVC_CD`,
    `nc_img_req`.`IMG_NM` as `SVC_NM`,
    `nc_img_req`.`CUD_CD` as `CUD_CD`,
    `nc_img_req`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `nc_img_req`.`INS_ID` as `INS_ID`,
    `nc_img_req`.`INS_DT` as `INS_DT`,
    (case
         when (`nc_img_req`.`CUD_CD` = 'D') then 0
         else `GET_VM_DD_FEE`(`nc_img_req`.`IMG_ID`,
                              `nc_img_req`.`DC_ID`,
                              0,
                              0,
                              (
                                  select
                                      `b`.`IMG_DISK_TYPE_ID`
                                  from
                                      `nc_dc` `b`
                                  where
                                      (`b`.`DC_ID` = `nc_img_req`.`DC_ID`)),
                              `nc_img_req`.`DISK_SIZE`,
                              null)
        end) as `FEE`,
    `nc_img_req`.`FEE_TYPE_CD` as `FEE_TYPE_CD`
from
    `nc_img_req`
where
    (`nc_img_req`.`APPR_STATUS_CD` = 'W')
union all
select
    `nc_fw_req`.`FW_ID` as `SVC_ID`,
    'F' as `SVC_CD`,
    concat(concat(concat(concat(`nc_fw_req`.`CIDR`, '->'), `nc_vm`.`VM_NM`), ':'), `nc_fw_req`.`PORT`) as `SVC_NM`,
    `nc_fw_req`.`CUD_CD` as `CUD_CD`,
    `nc_fw_req`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `nc_fw_req`.`INS_ID` as `INS_ID`,
    `nc_fw_req`.`INS_DT` as `INS_DT`,
    0 as `FEE`,
    'F' as `FEE_TYPE_CD`
from
    (`nc_fw_req`
        join `nc_vm`)
where
    ((`nc_fw_req`.`VM_ID` = `nc_vm`.`VM_ID`)
        and (`nc_fw_req`.`APPR_STATUS_CD` = 'W'))
union all
select
    `nc_vra_catalogreq_req`.`CATALOGREQ_ID` as `SVC_ID`,
    'C' as `SVC_CD`,
    concat(concat(`r`.`CATALOG_NM`, '/'), `nc_vra_catalogreq_req`.`REQ_TITLE`) as `SVC_NM`,
    `nc_vra_catalogreq_req`.`CUD_CD` as `CUD_CD`,
    `nc_vra_catalogreq_req`.`APPR_STATUS_CD` as `APPR_STATUS_CD`,
    `nc_vra_catalogreq_req`.`INS_ID` as `INS_ID`,
    `nc_vra_catalogreq_req`.`INS_DT` as `INS_DT`,
    (case
         when (`nc_vra_catalogreq_req`.`CUD_CD` = 'D') then 0
         else `nc_vra_catalogreq_req`.`PREDICT_DD_FEE`
        end) as `FEE`,
    'D' as `D`
from
    (`nc_vra_catalogreq_req`
        join `v_nc_vra_catalog` `r`)
where
    ((`r`.`CATALOG_ID` = `nc_vra_catalogreq_req`.`CATALOG_ID`)
        and (`nc_vra_catalogreq_req`.`APPR_STATUS_CD` = 'W'));



/*
 * DML 데이터 테이블 목록
fm_cd
fm_cd_grp
fm_cd_nl
fm_dashboard
fm_ddic
fm_menu
fm_nl
fm_prgm
fm_role
fm_role_prgm
*/

INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('biz', 'developement', '개발', '2015-12-11 17:52:02', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('biz', 'service', '서비스', '2015-12-11 17:00:21', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 0);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('dt', 'Date', '날짜', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', '3', 'syk', '0:0:0:0:0:0:0:1', 3);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('dt', 'Number', 'Number', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', '3', 'syk', '0:0:0:0:0:0:0:1', 2);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('dt', 'Text', 'Text', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', '3', 'syk', '0:0:0:0:0:0:0:1', 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('email', 'daum.net', '다음', '2015-12-17 17:33:28', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('email', 'naver.com', '네이버', '2015-12-17 17:33:21', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('ft', 'form', 'form', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('ft', 'grid', 'grid', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'calendar', 'calendar', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', '3', 'syk', '0:0:0:0:0:0:0:1', NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'calendarFromTo', 'calendarFromTo', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'editor', 'editor', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'inputFile', 'file', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', 'table', 'syk', '0:0:0:0:0:0:0:1', NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'inputMask', 'inputMask', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'inputText', 'inputText', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '25', 'syk', '0:0:0:0:0:0:0:1', 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'inputTextarea', 'textarea', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'outputText', 'outputText', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'popup', 'popup', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'popupSel', 'PopupSel', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'selectMany', 'selectMany', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'selectManyCheckbox', 'checkbox', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'selectOneMenu', 'select', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'selectOneRadio', 'radio', '2015-05-15 15:43:33', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('it', 'selectYN', 'selectYN', '2015-05-13 15:53:58', NULL, NULL, NULL, '2015-05-13 15:53:58', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('item_d', 'dtest1', '개발테스트1', '2015-12-14 11:45:19', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('item_d', 'dtest2', '개발테스트2', '2015-12-14 11:45:47', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('item_s', 'test1', '테스트1', '2015-12-14 11:15:07', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 0);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('item_s', 'test2', '테스트2', '2015-12-14 11:15:17', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('iu', 'I', 'insertOnly', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('iu', 'U', 'updateOnly', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('lang', 'en', 'English', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', 2);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('lang', 'ge', 'Deutsch', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('lang', 'ko', '한국어', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('log_level', 'DEBUG', '디버그', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('log_level', 'ERROR', '오류', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('log_level', 'FATAL', '치명적인 오류', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('log_level', 'INFO', '정보', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('log_level', 'TRACE', '추적', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('log_level', 'WARN', '경고', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('mobile', '010', '010', '2015-12-16 15:57:03', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 0);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('mobile', '011', '011', '2015-12-16 15:57:10', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('mobile', '0130', '0130', '2015-12-16 15:57:48', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 2);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('mobile', '016', '016', '2015-12-16 15:57:18', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 3);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('mobile', '017', '017', '2015-12-16 15:57:30', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 4);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('mobile', '018', '018', '2015-12-16 15:57:35', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 5);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('mobile', '019', '019', '2015-12-16 15:57:42', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 6);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('notice', 'TOP', '탑공지', '2016-02-22 10:37:16', NULL, NULL, NULL, '2019-03-18 10:30:01', NULL, NULL, NULL, 0);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('qg', 'G', '일반', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('qt', 'delete', 'delete', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('qt', 'insert', 'insert', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('qt', 'list', 'list', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('qt', 'list_count', 'list_count', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('qt', 'selectByPrimaryKey', 'selectByPrimaryKey', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('qt', 'update', 'update', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('sqlCond', 'eq', '조회값과 같은 자료만 조회(=''값'')', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, 6);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('sqlCond', 'gt', '조회값보다 크거나 같은 자료만 조회(>=''값'')', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, 5);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('sqlCond', 'like', '조회값이 포함된 자료만 조회(like ''%값%'')', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('sqlCond', 'like_post', '조회값이 뒤에 포함된 자료만 조회(like ''%값'')', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, 2);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('sqlCond', 'like_pre', '조회값이 앞에 포함된 자료만 조회(like ''값%'')', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, 3);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('sqlCond', 'lt', '조회값보다 작거나 같은 자료만 조회(<=''값'')', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, 4);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('st', '0', '사용가능', '2015-05-19 19:42:27', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('st', '1', '사용불가', '2015-05-19 19:42:42', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, NULL);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('system', '0', 'ntreec', '2015-12-01 00:00:00', NULL, NULL, NULL, '2015-12-01 00:00:00', NULL, 'secrain', '192.168.0.1', 0);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '02', '서울', '2015-12-16 12:15:44', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 0);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '0303', '(구)인터넷 전화', '2015-12-16 12:15:57', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '031', '경기', '2015-12-16 12:16:05', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 2);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '032', '인천', '2015-12-16 12:16:09', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 3);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '033', '강원', '2015-12-16 12:16:14', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 4);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '041', '충남', '2015-12-16 12:16:24', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 5);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '042', '대전', '2015-12-16 12:16:29', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 6);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '043', '충북', '2015-12-16 12:16:32', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 7);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '044', '세종', '2015-12-16 12:16:37', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 8);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '0502', '평생번호 - KT', '2015-12-16 12:16:45', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 9);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '0504', '평생번호 - 온세', '2015-12-16 12:16:54', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 10);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '0505', '평생번호 - U+', '2015-12-16 12:16:58', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 11);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '0506', '평생번호 - SK BroadBand(Hanaro)', '2015-12-16 12:17:03', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 12);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '0507', '평생번호', '2015-12-16 12:17:09', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 13);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '051', '부산', '2015-12-16 12:17:14', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 14);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '052', '울산', '2015-12-16 12:17:17', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 15);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel ', '053', '대구', '2015-12-16 12:17:20', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 16);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '054', '경북', '2015-12-16 12:17:24', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 17);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel ', '055', '경남', '2015-12-16 12:17:29', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 18);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '061', '전남', '2015-12-16 12:17:38', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 19);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '062', '광주', '2015-12-16 12:17:40', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 20);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '063', '전북', '2015-12-16 12:18:05', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 21);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '064', '제주', '2015-12-16 12:18:09', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 22);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '070', '인터넷 전화', '2015-12-16 12:18:18', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 23);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('tel', '080', '무료전화', '2015-12-16 12:18:26', NULL, NULL, NULL, '2019-03-18 10:30:02', NULL, NULL, NULL, 24);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('ut', 'C', '외부사용자', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, 2);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('ut', 'I', '내부사용자', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL, 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('yn', 'N', '아니오', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', 2);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('yn', 'Y', '예', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', 1);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('yn01', '0', '아니오', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', 2);
INSERT INTO fm_cd
(GRP_ID, CD_ID, CD_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, ODR)
VALUES('yn01', '1', '예', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', 1);



INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('biz', '업태코드', '2015-12-11 17:47:42', NULL, NULL, NULL, '2019-03-18 10:30:00', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('dt', '데이터타입', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1');
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('email', '이메일 도메인', '2015-12-17 17:28:35', NULL, NULL, NULL, '2019-03-18 10:30:00', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ft', '조각종류', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', '입력방식2', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', 'table', 'syk', '0:0:0:0:0:0:0:1');
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_d', '아이템개발', '2015-12-14 11:18:45', NULL, NULL, NULL, '2019-03-18 10:30:00', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_s', '아이템서비스', '2015-12-14 11:15:46', NULL, NULL, NULL, '2019-03-18 10:30:00', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('iu', 'Insert,Update구분', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', '언어', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1');
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', '로그 레벨', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('mobile', '휴대폰 앞자리', '2015-12-16 15:56:09', NULL, NULL, NULL, '2019-03-18 10:30:00', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('notice', '공지타입', '2016-02-22 10:36:57', NULL, NULL, NULL, '2019-03-18 10:30:00', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qg', '쿼리 자동 생성 종류', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', 'tmpl', 'syk', '211.51.22.222');
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qt', '쿼리종류', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('system', '사용시스템', '2015-12-01 00:00:00', NULL, NULL, NULL, '2015-12-01 00:00:00', NULL, 'secrain', '192.168.0.1');
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '일반전화 지역번호', '2015-12-16 12:14:34', NULL, NULL, NULL, '2019-03-18 10:30:00', NULL, NULL, NULL);
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('yn', 'YN', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1');
INSERT INTO fm_cd_grp
(GRP_ID, GRP_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('yn01', 'YN01', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1');


INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('biz', 'developement', 'en', 'Developement', '2015-12-11 17:52:56', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('biz', 'developement', 'ko', '개발', '2015-12-11 17:52:31', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('biz', 'service', 'en', 'Service', '2015-12-11 17:48:50', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('biz', 'service', 'ko', '서비스', '2015-12-11 17:48:42', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('dt', 'Date', 'en', 'Date', '2015-05-13 15:53:59', '25', 'syk', '0:0:0:0:0:0:0:1', '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('dt', 'Date', 'ko', 'Date', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1');
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('dt', 'Number', 'en', 'Number', '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('dt', 'Number', 'ko', 'Number', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('dt', 'Text', 'en', 'Text', '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('dt', 'Text', 'ko', 'Text', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('email', 'daum.net', 'ko', 'daum.net', '2015-12-17 17:34:40', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('email', 'naver.com', 'ko', 'naver.com', '2015-12-17 17:34:19', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ft', 'form', 'ko', 'form', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ft', 'grid', 'ko', 'grid', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'calendar', 'en', 'calendar', '2015-05-13 15:53:59', '3', 'syk', '0:0:0:0:0:0:0:1', '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'calendar', 'ko', 'calendar', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'calendarFromTo', 'ko', 'calendarFromTo', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'editor', 'ko', 'editor', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'inputFile', 'ko', 'file', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'inputMask', 'ko', 'inputMask', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'inputText', 'en', 'inputText', '2015-05-13 15:53:59', '25', 'syk', '0:0:0:0:0:0:0:1', '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'inputText', 'ko', 'inputText', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'inputTextarea', 'ko', 'textarea', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'outputText', 'ko', 'outputText', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'popup', 'ko', 'popup', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'popupSel', 'ko', 'PopupSel', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'selectMany', 'ko', 'selectMany', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'selectManyCheckbox', 'ko', 'checkbox', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'selectOneMenu', 'ko', 'select', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'selectOneRadio', 'ko', 'radio', '2015-05-15 15:43:34', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('it', 'selectYN', 'ko', 'selectYN', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_d', 'dtest1', 'en', 'dtest1', '2015-12-14 11:17:50', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_d', 'dtest1', 'ko', '개발테스트1', '2015-12-14 11:17:36', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_d', 'dtest2', 'en', 'dtest2', '2015-12-14 11:18:32', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_d', 'dtest2', 'ko', '개발테스트2', '2015-12-14 11:18:22', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_s', 'test1', 'en', 'test1', '2015-12-14 11:16:40', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_s', 'test1', 'ko', '테스트1', '2015-12-14 11:16:28', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_s', 'test2', 'en', 'test2', '2015-12-14 11:17:25', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('item_s', 'test2', 'ko', '테스트2', '2015-12-14 11:17:14', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('iu', 'I', 'ko', 'insertOnly', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('iu', 'U', 'ko', 'updateOnly', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'en', 'en', 'English', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'en', 'ge', 'Englisch', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'en', 'ko', '영어', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'ge', 'en', 'Germany', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'ge', 'ge', 'Deutsch', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'ge', 'ko', '독일어', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'ko', 'en', 'Korean', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'ko', 'ge', 'Koreanisch', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('lang', 'ko', 'ko', '한국어', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'DEBUG', 'en', 'debug', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'DEBUG', 'ko', '디버그', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'ERROR', 'en', 'error', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'ERROR', 'ko', '오류', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'FATAL', 'en', 'fatal', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'FATAL', 'ko', '치명적인 오류', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'INFO', 'en', 'info', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'INFO', 'ko', '정보', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'TRACE', 'en', 'trace', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'TRACE', 'ko', '추적', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'WARN', 'en', 'warn', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('log_level', 'WARN', 'ko', '경고', '2015-05-13 15:53:59', NULL, NULL, NULL, '2015-05-13 15:53:59', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('mobile', '010', 'ko', '010', '2015-12-16 15:58:04', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('mobile', '011', 'ko', '011', '2015-12-16 15:58:08', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('mobile', '0130', 'ko', '0130', '2015-12-16 15:58:32', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('mobile', '016', 'ko', '016', '2015-12-16 15:58:13', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('mobile', '017', 'ko', '017', '2015-12-16 15:58:18', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('mobile', '018', 'ko', '018', '2015-12-16 15:58:23', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('mobile', '019', 'ko', '019', '2015-12-16 15:58:27', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('notice', 'TOP', 'en', 'Top panel', '2016-02-22 10:38:13', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('notice', 'TOP', 'ko', '탑공지', '2016-02-22 10:37:54', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qg', 'G', 'ko', '일반', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qt', 'delete', 'ko', 'delete', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qt', 'insert', 'ko', 'insert', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qt', 'list', 'ko', 'list', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qt', 'list_count', 'ko', 'list_count', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qt', 'selectByPrimaryKey', 'ko', 'selectByPrimaryKey', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('qt', 'update', 'ko', 'update', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('sqlCond', 'eq', 'ko', '조회값과 같은 자료만 조회(=''값'')', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('sqlCond', 'gt', 'ko', '조회값보다 크거나 같은 자료만 조회(>=''값'')', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('sqlCond', 'like', 'ko', '조회값이 포함된 자료만 조회(like ''%값%'')', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('sqlCond', 'like_post', 'ko', '조회값이 뒤에 포함된 자료만 조회(like ''%값'')', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('sqlCond', 'like_pre', 'ko', '조회값이 앞에 포함된 자료만 조회(like ''값%'')', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('sqlCond', 'lt', 'ko', '조회값보다 작거나 같은 자료만 조회(<=''값'')', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('system', '0', 'en', 'ntreec', '2015-12-01 00:00:00', NULL, 'secrian', '192.168.0.1', '2015-12-01 00:00:00', NULL, 'secrain', '192.168.0.1');
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('system', '0', 'ko', 'ntreec', '2015-12-01 00:00:00', NULL, 'secrian', '192.168.0.1', '2015-12-01 00:00:00', NULL, 'secrain', '192.168.0.1');
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '02', 'ko', '02', '2015-12-16 12:21:09', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '0303', 'ko', '0303', '2015-12-16 12:21:26', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '031', 'ko', '031', '2015-12-16 12:21:35', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '032', 'ko', '032', '2015-12-16 12:21:55', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '033', 'ko', '033', '2015-12-16 12:22:54', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '041', 'ko', '041', '2015-12-16 12:23:03', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '042', 'ko', '042', '2015-12-16 12:23:28', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '043', 'ko', '043', '2015-12-16 12:23:39', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '044', 'ko', '044', '2015-12-16 12:23:45', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '051', 'ko', '051', '2015-12-16 12:23:55', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '052', 'ko', '052', '2015-12-16 12:24:01', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '053', 'ko', '053', '2015-12-16 12:24:06', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '054', 'ko', '054', '2015-12-16 12:24:21', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '055', 'ko', '055', '2015-12-16 12:24:25', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '061', 'ko', '061', '2015-12-16 12:24:40', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '062', 'ko', '062', '2015-12-16 12:24:54', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '063', 'ko', '063', '2015-12-16 12:25:05', NULL, NULL, NULL, '2019-03-18 10:30:07', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '064', 'ko', '064', '2015-12-16 12:25:15', NULL, NULL, NULL, '2019-03-18 10:30:08', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '070', 'ko', '070', '2015-12-16 12:25:26', NULL, NULL, NULL, '2019-03-18 10:30:08', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('tel', '080', 'ko', '080', '2015-12-16 12:25:39', NULL, NULL, NULL, '2019-03-18 10:30:08', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ut', 'C', 'ko', '외부사용자', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ut', 'I', 'ko', '내부사용자', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('yn', 'N', 'en', 'No', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('yn', 'N', 'ko', '아니오', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('yn', 'Y', 'en', 'Yes', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('yn', 'Y', 'ko', '예', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('yn01', '0', 'ko', '아니오', '2015-07-14 14:04:44', NULL, NULL, NULL, '2019-03-18 10:30:08', NULL, NULL, NULL);
INSERT INTO fm_cd_nl
(GRP_ID, CD_ID, LANG_TYPE, CD_NM, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('yn01', '1', 'ko', '예', '2015-05-13 15:54:00', NULL, NULL, NULL, '2015-05-13 15:54:00', NULL, NULL, NULL);


INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(0, '결재 현황', 'dashboard/req_cnt.jsp', NULL, -1, 4, -1);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(2, 'Datastore disk 사용율', 'dashboard/ds_pie.jsp', 'Y', NULL, 6, 4);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(3, 'Host성능', 'dashboard/host_perf_bubble.jsp', 'Y', NULL, 6, 3);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(4, 'VM성능', 'dashboard/vm_perf_bubble.jsp', NULL, 3, 6, 5);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(5, '그룹별 서버 개수', 'dashboard/group_vm_cnt_bar.jsp', NULL, 8, 6, 6);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(6, '경고목록', 'dashboard/alarm.jsp', NULL, 7, 6, 2);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(7, '공지 사항', 'dashboard/noti.jsp', NULL, 1, 4, 9);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(8, 'Q&A', 'dashboard/qna.jsp', NULL, 5, 4, 1);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(9, 'VM개수 및 비용 현황', 'dashboard/countInfo4user.jsp', 'N', 0, 4, NULL);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(10, '보유시스템 현황', 'dashboard/countInfo.jsp', 'Y', NULL, 4, 0);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(11, '결재요청목록', 'dashboard/req.jsp', 'N', 2, 6, NULL);
INSERT INTO fm_dashboard
(ITEM_ID, ITEM_NM, ITEM_PATH, ADMIN_YN, SEQ, COL, ADMIN_SEQ)
VALUES(15, '만료 목록', 'dashboard/expire.jsp', NULL, 6, 6, 7);

INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('APPR_COMMENT', '결재의견', 'appr1', '용량이 부족합니다', NULL, NULL, 'Y', NULL, '2020-01-30 23:26:27', NULL, NULL, NULL, '2020-06-03 09:15:43', '66', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('APP_KIND', '결재상태코드', 'A', '승인', 'Approve', 3, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2020-06-02 17:32:22', '66', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('APP_KIND', '결재상태코드', 'C', '취소', 'Cancel', 5, 'Y', NULL, '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:37', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('APP_KIND', '결재상태코드', 'D', '반려', 'Deny', 4, 'Y', NULL, '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:37', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('APP_KIND', '결재상태코드', 'R', '승인대기', 'Request', 2, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:37', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('APP_KIND', '결재상태코드', 'W', '요청중', 'Woking', 1, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:37', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('AZ_REGIONS', 'Azure 리전', 'koreacentral', 'Korea Central', 'Korea Central', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('AZ_REGIONS', 'Azure 리전', 'koreasouth', 'Korea South', 'Korea South', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'ap-northeast-1', '아시아 태평양 (도쿄)', 'Asia Pacific (Tokyo)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'ap-northeast-2', '아시아 태평양 (서울)', 'Asia Pacific (Seoul)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'ap-south-1', '아시아 태평양 (뭄바이)', 'Asia Pacific (Mumbai)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'ap-southeast-1', '아시아 태평양 (싱가포르)', 'Asia Pacific (Singapore)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'ap-southeast-2', '아시아 태평양 (시드니)', 'Asia Pacific (Sydney)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'ca-central-1', '캐나다 (중부)', 'Canada (Central)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'eu-central-1', '유럽 (프랑크푸르트)', 'EU (Frankfurt)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'eu-north-1', '유럽 (스톡홀름)', 'EU (Stockholm)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'eu-south-1', '유럽 (밀라노)', 'EU (Milan)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'eu-west-1', '유럽 (아일랜드)', 'EU (Ireland)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'eu-west-2', '유럽 (런던)', 'EU (London)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'eu-west-3', '유럽 (파리)', 'EU (Paris)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'me-south-1', '중동 (바레인)', 'Middle East (Bahrain)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'sa-east-1', '남아메리카 (상파울루)', 'South America (Sao Paulo)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'us-east-1', '미국 동부 (버지니아 북부)', 'US East (N. Virginia)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'us-east-2', '미국 동부 (오하이오)', 'US East (Ohio)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'us-west-1', '미국 서부 (캘리포니아)', 'US West (N. California)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('A_REGIONS', 'AWS 리전', 'us-west-2', '미국 서부 (오레곤)', 'US West (Oregon)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('BANK', '은행코드', 'S', '신한', 'Shinhan', 0, 'Y', NULL, '2015-12-22 16:52:45', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CARD', '카드코드', 'S', '신한카드', 'Shinhan Card', 0, 'Y', NULL, '2015-12-22 16:53:03', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CLIENT_STATUS', '사용중', 'I', '사용중', 'Using', 1, 'Y', NULL, '2020-09-16 10:08:00', NULL, NULL, NULL, '2020-09-16 10:08:35', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CLIENT_STATUS', '사용대기', 'W', '사용대기', 'Wait', 0, 'Y', NULL, '2020-09-16 10:08:00', NULL, NULL, NULL, '2020-09-16 10:08:35', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CLIENT_STATUS', '폐기', 'X', '폐기', 'Disposal', 2, 'Y', NULL, '2020-09-16 10:08:00', NULL, NULL, NULL, '2020-09-16 10:08:35', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CONTAINER_TYPE', 'Container 종류', 'O', 'Openshift', 'Openshift', 1, 'Y', 'CM', '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CPU_CNT_UNIT', 'CPU 단위', '', 'core', 'core', 1, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CPU_CNT_UNIT', 'CPU 단위', 'm', 'milicore', 'milicore', 2, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CUD', 'CUD코드', 'C', '생성', 'Create', 1, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CUD', 'CUD코드', 'D', '삭제', 'Delete', 3, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('CUD', 'CUD코드', 'U', '변경', 'Update', 2, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('C_ENV_CODE', '템플릿Param코드', 'db.selectModel', '모델', 'Model', 2, 'Y', 'CM', '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('C_P_KUBUN', '개발양산', 'D', '개발', 'Develop', 1, 'Y', 'CM', '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('C_P_KUBUN', '개발양산', 'P', '운영', 'Production', 0, 'Y', 'CM', '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'image.max', '0', 'Private Template Max Count', 90, 'N', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2020-09-14 05:49:41', '160', '8', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'manager', '인프라기술팀 김정일 대리(0093),IT운영지원팀 여종건 사원(0098)', 'Portal Related Inquiry', 5, 'Y', NULL, '2019-06-03 16:19:43', '66', '0', '0:0:0:0:0:0:0:1', '2020-09-14 05:49:41', '160', '8', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'oversize_cpu', '5', '오버사이즈서버:최대CPU(%)', 11, 'N', NULL, '2019-06-03 16:19:43', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'oversize_disk', '6', '오버사이즈서버:최대Disk(%)', 13, 'N', NULL, '2019-06-03 16:19:43', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'oversize_mem', '6', '오버사이즈서버:최대Memory(%)', 12, 'N', NULL, '2019-06-03 16:19:43', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'schedule_svr', 'main', '스케쥴실행서버', 1, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2020-09-14 05:49:41', '160', '8', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'security_chk_msg', '~~~~에 따른 보안 관리사항에 대해 동의하며, ~~~~ 조치를 성실히 이행할 것을 확인합니다.', '보안 체크 문구', 5, 'Y', NULL, '2019-06-03 16:19:43', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'size_term_day', '30', '적정사이즈서버:기준(일)', 10, 'N', NULL, '2019-06-03 16:19:43', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'snapshot.max', '1', 'Snapshot Max Count', 90, 'N', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2020-09-14 05:49:41', '160', '8', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'use_vm.cpu_min', '5', '장기미사용서버:최소 CPU 사용율(%)', 2, 'N', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'use_vm.max_not_use_day', '11', '장기미사용서버:기준(일)', 4, 'N', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'use_vm.net_min', '5', '장기미사용서버:최소 네트웍 사용량(kbps)', 3, 'N', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'use_vm_mem_min', '5', '장기미사용서버:최소 Memory 사용율(%)', 1, 'N', NULL, '2019-06-03 16:19:43', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'use_work_reclaim', '6', '장기미사용서버:기준(일)', 5, 'N', NULL, '2019-06-03 16:19:43', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ETC_SVC', '기타요금', 'C', 'CPU', 'CPU', 2, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-04-17 15:11:29', '1', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ETC_SVC', '서비스종류', 'G', '템플릿', 'Imge', 4, 'Y', NULL, '2016-01-08 18:24:28', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ETC_SVC', '서비스종류', 'I', 'IP', 'IP', 3, 'Y', NULL, '2016-01-08 18:24:28', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ETC_SVC', '기타요금', 'M', 'Memory(GB)', 'Memory(GB)', 3, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-04-17 15:11:37', '1', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FAB', 'FAB', 'm14', 'M14', 'M14', 0, 'Y', NULL, '2020-01-14 13:50:34', NULL, NULL, NULL, '2020-01-14 13:50:34', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FAB', 'FAB', 'm15', 'M15', 'M15', 1, 'Y', NULL, '2020-01-14 13:50:34', NULL, NULL, NULL, '2020-01-14 13:50:34', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FAB', 'FAB', 'm16', 'M16', 'M16', 2, 'Y', NULL, '2020-01-14 13:50:34', NULL, NULL, NULL, '2020-01-14 13:50:34', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE', '요금제', 'D', '원/일', '￦/Day', 2, 'Y', NULL, '2015-12-29 16:42:37', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE', '요금제', 'H', '원/시간', '￦/Hour', 1, 'Y', NULL, '2015-12-29 16:42:37', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE', '요금제', 'M', '원/월', '￦/Month', 3, 'Y', NULL, '2015-12-29 16:42:37', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE_FREE', '무료 요금제', 'F', '무료', 'Free', 0, 'Y', NULL, '2016-01-28 16:27:49', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE_TYPE', '요금제', 'D', '일일요금제', 'Day', 2, 'Y', NULL, '2016-01-12 17:53:49', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE_TYPE', '요금제', 'H', '시간요금제', 'Hour', 1, 'Y', NULL, '2016-01-12 17:53:54', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE_TYPE', '요금제', 'M', '월간요금제', 'Month', 3, 'Y', NULL, '2016-01-12 17:54:28', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE_UNIT', '요금제 단위', 'D', '일', 'Day', 1, 'Y', NULL, '2016-02-25 11:31:41', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE_UNIT', '요금제 단위', 'H', '시간', 'Hour', 0, 'Y', NULL, '2016-02-25 11:31:23', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('FEE_UNIT', '요금제 단위', 'M', '월', 'Month', 2, 'Y', NULL, '2016-02-25 11:32:07', NULL, NULL, NULL, '2019-05-08 16:53:39', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('GOODS_CATEGORY', '상품 분류', '1', '빅데이터', 'BigData', 1, 'Y', NULL, '2016-01-08 17:52:10', NULL, NULL, NULL, '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('G_REGIONS', 'GCP 리전', 'asia-northeast3-a', '서울-A', 'Seoul-A', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('G_REGIONS', 'GCP 리전', 'asia-northeast3-b', '서울-B', 'Seoul-B', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('G_REGIONS', 'GCP 리전', 'asia-northeast3-c', '서울-C', 'Seoul-C', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('HV_ETC_PRODUCT_V', '연결정보 종류', 'FW', 'Firewall', 'Firewall', 2, 'Y', NULL, '2018-11-01 16:45:45', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('HV_ETC_PRODUCT_V', '연결정보 종류', 'LINUX_GUEST', 'Linux', 'Linux', 2, 'Y', NULL, '2018-11-01 16:45:45', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('HV_ETC_PRODUCT_V', '연결정보 종류', 'WIN_GUEST', 'Windows', 'Windows', 2, 'Y', NULL, '2018-11-01 16:45:45', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('HYPERVISOR', 'Hypervisor코드', 'A', 'AWS', 'Amazon(Aws)', 1, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('HYPERVISOR', 'Hypervisor코드', 'AZ', 'Azure', 'Azure', 1, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('HYPERVISOR', 'Hypervisor코드', 'G', 'GCP', 'Google(GCP)', 1, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('HYPERVISOR', 'Hypervisor코드', 'V', 'VMWare', 'VMWare', 1, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ', '문의종류', 'ERR', '오류', 'Error', 3, 'Y', NULL, '2016-02-25 11:39:43', NULL, NULL, NULL, '2019-05-08 16:53:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ', '문의종류', 'ETC', '기타', 'etc', 5, 'Y', NULL, '2015-12-28 11:04:20', NULL, NULL, NULL, '2019-05-08 16:53:41', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ', '문의종류', 'FAIL', '장애', 'Failure', 4, 'Y', NULL, '2016-02-25 11:39:43', NULL, NULL, NULL, '2019-05-08 16:53:41', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ', '문의종류', 'INQ', '문의', 'inquiry', 1, 'Y', NULL, '2016-02-25 11:39:43', NULL, NULL, NULL, '2020-06-02 17:33:27', '183', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ', '문의종류', 'REQ', '요청', 'Request', 2, 'Y', NULL, '2016-02-25 11:39:43', NULL, NULL, NULL, '2019-05-08 16:53:41', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'A', '컨테이너', 'Container', 10, 'Y', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'B', '상품구매', 'Buy Goods', 7, 'N', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'C', 'VRA카탈로그', 'Server', 1, 'Y', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'D', '디스크', 'Disk', 2, 'N', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'F', '접근통제', 'Firewall', 5, 'Y', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'G', '템플릿', 'Imge', 4, 'Y', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'I', 'IP', 'IP', 3, 'N', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'N', '네트워크 연결', 'Network Connection', 8, 'N', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'S', '서버', 'Server', 1, 'Y', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('INQ_SVC', '문의 사항 서비스구분', 'V', 'Volume', 'Volume', 9, 'Y', NULL, '2020-11-27 14:24:18', NULL, NULL, NULL, '2020-11-27 14:24:18', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('IP_STATUS', 'IP상태', 'N', '미사용', '미사용', 2, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('IP_STATUS', 'IP상태', 'R', '예약', '예약', 2, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('IP_STATUS', 'IP상태', 'U', '사용중', '사용중', 2, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('IP_STATUS', 'IP상태', 'V', '사용불가', '사용불가', 2, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MEM_SIZE_UNIT', '메모리 구분', 'Gi', 'Gi', 'Gi', 1, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MEM_SIZE_UNIT', '메모리 구분', 'Mi', 'Mi', 'Mi', 1, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MNG_DDIC', '어드민수정코드', 'INQ', '문의사항-종류', '문의사항-종류', 2, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-11-13 10:27:28', '183', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MNG_DDIC', '어드민수정코드', 'PURPOSE', 'VM-사용용도', 'VM-사용용도', 1, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-11-13 10:27:28', '183', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MNG_DDIC', '어드민수정코드', 'P_KUBUN', 'VM-구분', 'VM-구분', 3, 'Y', NULL, '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-11-13 10:27:28', '183', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MODEL_SKUBUN', '모델 서빙 구분', 'P', '운영', 'Production', 1, 'Y', 'CM', '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MODEL_SKUBUN', '모델 서빙 구분', 'T', '검증', 'Test', 0, 'Y', 'CM', '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MONTHSETTING', '서버세팅', 'MONTH', '연장기간(월)', 'MONTH', 1, 'Y', NULL, '2020-07-22 19:27:07', NULL, NULL, NULL, '2020-07-22 19:27:07', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MOUNT_DEFAULT', 'Linux', '/dev/sdb', 'Linux', 'Linux', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('MOUNT_DEFAULT', 'Window', '/temp', 'Window', 'Window', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('NAS_UNIT_CD', 'NAS 용량단위', 'G', 'GB', NULL, 2, 'Y', NULL, '2019-05-27 14:32:08', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:32:09', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('NAS_UNIT_CD', 'NAS 용량단위', 'M', 'MB', NULL, 1, 'Y', NULL, '2019-07-27 14:32:26', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:32:27', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('NAS_UNIT_CD', 'NAS 용량단위', 'T', 'TB', NULL, 3, 'Y', NULL, '2019-05-27 14:32:26', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:32:27', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('OS_TYPE', 'OS TYPE', 'linux', 'Linux', 'Linux', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('OS_TYPE', 'OS TYPE', 'window', 'Window', 'Window', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('OVERALLSTATUS', '유형', 'GREEN', 'GREEN', 'GREEN', NULL, 'Y', NULL, '2019-05-08 16:53:48', NULL, NULL, NULL, '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('OVERALLSTATUS', '유형', 'RED', 'RED', 'RED', NULL, 'Y', NULL, '2019-05-08 16:53:48', NULL, NULL, NULL, '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('OVERALLSTATUS', '유형', 'YELLOW', 'YELLOW', 'YELLOW', NULL, 'Y', NULL, '2019-05-08 16:53:48', NULL, NULL, NULL, '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PAY_KIND', '결제구분', 'A', '계좌이체', 'account transfer', 1, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PAY_KIND', '결제구분', 'C', '신용카드', 'credit card', 2, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PAY_YN', '납부여부', 'N', '미납', 'No Pay', 0, 'Y', NULL, '2016-02-13 16:21:55', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PAY_YN', '납부여부', 'Y', '납부완료', 'Paid', 0, 'Y', NULL, '2016-02-13 16:21:34', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_KIND', '성능 모니터링 종류', 'cpu_usage', 'CPU 사용률(%)', 'CPU Usage(%)', 1, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_KIND', '성능 모니터링 종류', 'cpu_usagemhz', 'CPU 사용(MHz)', 'CPU Usage(MHz)', 5, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_KIND', '성능 모니터링 종류', 'disk_capacity', '총 용량(KB)', 'Disk Capacity(KB)', 8, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_KIND', '성능 모니터링 종류', 'disk_usage', '디스크 사용(KB/초)', 'Disk Usage(KB/s)', 3, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_KIND', '성능 모니터링 종류', 'disk_used', '사용 용량(KB)', 'Disk Used(KB)', 7, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:42', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_KIND', '성능 모니터링 종류', 'mem_consumed', '사용한 호스트 물리적 메모리 양(KB)', 'Memory Consumed(KB)', 6, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_KIND', '성능 모니터링 종류', 'mem_usage', '메모리 사용률(%)', 'Memory Usage(%)', 2, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_KIND', '성능 모니터링 종류', 'net_usage', '네트워크 활용량(KB/초)', 'Network Usage(KB/s)', 4, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM', '성능 모니터링 주기', '1d', '1일', '1Day', 2, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM', '성능 모니터링 주기', '1h', '1시간', '1Hour', 1, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM', '성능 모니터링 주기', '1m', '1달', '1Month', 4, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM', '성능 모니터링 주기', '1w', '1주일', '1Week', 3, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM', '성능 모니터링 주기', '1y', '1년', '1Year', 5, 'Y', NULL, '2015-12-23 11:15:59', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM1', '성능 모니터링 주기(1시간 제외)', '1d', '1일', '1Day', 2, 'Y', NULL, '2016-01-22 14:39:20', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM1', '성능 모니터링 주기(1시간 제외)', '1m', '1달', '1Month', 4, 'Y', NULL, '2016-01-22 14:39:20', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM1', '성능 모니터링 주기(1시간 제외)', '1w', '1주일', '1Week', 3, 'Y', NULL, '2016-01-22 14:39:20', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PERF_TERM1', '성능 모니터링 주기(1시간 제외)', '1y', '1년', '1Year', 5, 'Y', NULL, '2016-01-22 14:39:21', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('POD_STATUS', 'Pod상태코드', 'Error', 'Error', 'Error', 1, 'Y', '', '2019-08-07 13:33:40', NULL, NULL, NULL, '2019-08-07 13:33:40', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('POD_STATUS', 'Pod상태코드', 'Running', 'Running', 'Running', 0, 'Y', NULL, '2019-08-07 13:33:06', NULL, NULL, NULL, '2019-08-07 13:33:06', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PRJ_ROLE', '프로젝트권한', '0', '요청', 'Request', 1, 'Y', 'CM', '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PRJ_ROLE', '프로젝트권한', '1', 'Deploy', 'Deploy', 2, 'Y', 'CM', '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PRJ_ROLE', '프로젝트권한', '2', 'Build', 'Build', 3, 'Y', 'CM', '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PRJ_ROLE', '프로젝트권한', '3', '사양변경', '사양변경', 4, 'Y', 'CM', '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PRJ_ROLE', '프로젝트권한', 'A', '어드민', 'Admin', 5, 'Y', 'CM', '2018-09-17 18:15:49', '66', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:53:48', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PROTOCOL', '프로토콜', 'ICAMP', 'ICAMP', 'ICAMP', 3, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PROTOCOL', '프로토콜', 'TCP', 'TCP', 'TCP', 1, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PROTOCOL', '프로토콜', 'UDP', 'UDP', 'UDP', 2, 'Y', NULL, '2015-12-16 11:59:26', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PROTOCOL1', '프로토콜', 'TCP', 'TCP', 'TCP', 1, 'Y', NULL, '2016-02-16 13:44:55', NULL, NULL, NULL, '2019-05-08 16:53:43', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PROTOCOL1', '프로토콜', 'UDP', 'UDP', 'UDP', 2, 'Y', NULL, '2016-02-16 13:44:55', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PURPOSE', '사용용도', 'AI', 'AI', NULL, NULL, 'Y', NULL, '2019-05-08 16:53:50', NULL, NULL, NULL, '2019-05-08 16:53:50', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PURPOSE', '사용용도', 'BigData', 'BigData', NULL, NULL, 'Y', NULL, '2019-05-08 16:53:50', NULL, NULL, NULL, '2019-05-08 16:53:50', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PURPOSE', '사용용도', 'DB', 'DB', 'build', 1, 'Y', NULL, '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:50', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PURPOSE', '사용용도', 'WAS', 'WAS', NULL, NULL, 'Y', NULL, '2019-05-08 16:53:50', NULL, NULL, NULL, '2019-05-08 16:53:50', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('PURPOSE', '사용용도', 'WEB', 'WEB', NULL, NULL, 'Y', NULL, '2019-05-08 16:53:49', NULL, NULL, NULL, '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('P_KUBUN', '개발양산', 'D', '개발', 'D', 1, 'Y', NULL, '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('P_KUBUN', '개발양산', 'P', '운영', 'P', 1, 'Y', NULL, '2015-12-16 11:59:27', NULL, NULL, NULL, '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'af-south-1', '아프리카 (케이프타운)', 'Africa (Cape Town)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'ap-east-1', '아시아 태평양 (홍콩)', 'Asia Pacific (Hong Kong)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'ap-northeast-1', '아시아 태평양 (도쿄)', 'Asia Pacific (Tokyo)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'ap-northeast-2', '아시아 태평양 (서울)', 'Asia Pacific (Seoul)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'ap-south-1', '아시아 태평양 (뭄바이)', 'Asia Pacific (Mumbai)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'ap-southeast-1', '아시아 태평양 (싱가포르)', 'Asia Pacific (Singapore)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'ap-southeast-2', '아시아 태평양 (시드니)', 'Asia Pacific (Sydney)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'ca-central-1', '캐나다 (중부)', 'Canada (Central)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'eu-central-1', '유럽 (프랑크푸르트)', 'EU (Frankfurt)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'eu-north-1', '유럽 (스톡홀름)', 'EU (Stockholm)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'eu-south-1', '유럽 (밀라노)', 'EU (Milan)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'eu-west-1', '유럽 (아일랜드)', 'EU (Ireland)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'eu-west-2', '유럽 (런던)', 'EU (London)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'eu-west-3', '유럽 (파리)', 'EU (Paris)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'me-south-1', '중동 (바레인)', 'Middle East (Bahrain)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'sa-east-1', '남아메리카 (상파울루)', 'South America (Sao Paulo)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'us-east-1', '미국 동부 (버지니아 북부)', 'US East (N. Virginia)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'us-east-2', '미국 동부 (오하이오)', 'US East (Ohio)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'us-west-1', '미국 서부 (캘리포니아)', 'US West (N. California)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('REGIONS', '리전', 'us-west-2', '미국 서부 (오레곤)', 'US West (Oregon)', 0, 'Y', NULL, '2019-11-08 16:33:24', NULL, NULL, NULL, '2019-11-08 16:33:24', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('RUN_CD', '서버 상태', 'C', '요청중', 'Requested', 5, 'Y', NULL, '2019-01-15 15:42:40', NULL, NULL, NULL, '2019-05-08 16:53:49', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('RUN_CD', '서버 상태', 'F', '생성 실패', 'Fail', 2, 'Y', NULL, '2016-01-11 17:33:03', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('RUN_CD', '서버 상태', 'R', '시작', 'Run', 3, 'Y', NULL, '2016-01-11 17:33:03', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('RUN_CD', '서버 상태', 'S', '정지', 'Stop', 4, 'Y', NULL, '2016-01-11 17:33:03', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('RUN_CD', '서버 상태', 'W', '생성중', 'InProgress', 1, 'Y', NULL, '2016-01-11 17:33:02', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_ACTION', '스케줄 Action', 'snapshotCreate', 'snapshot 생성', 'snapshot 생성', 4, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_ACTION', '스케줄 Action', 'snapshotDelete', 'snapshot 제거', 'snapshot 제거', 5, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_ACTION', '스케줄 Action', 'vmPowerOff', 'stop', 'stop', 3, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_ACTION', '스케줄 Action', 'vmPowerOn', 'start', 'start', 2, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_ACTION', '스케줄 Action', 'vmReboot', 'restart', 'restart', 1, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_CYCLE', '스케줄 주기', 'D', '매일', '매일', 1, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_CYCLE', '스케줄 주기', 'M', '매달', '매달', 3, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_CYCLE', '스케줄 주기', 'W', '매주', '매주', 2, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_SYNC', '스케줄 주기/비주기', 'A', '비주기', '비주기', 2, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_SYNC', '스케줄 주기/비주기', 'C', '입력', '입력', 1, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SCHEDULE_SYNC', '스케줄 주기/비주기', 'S', '주기', '주기', 1, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SEPECSETTING', '서버세팅', 'CPU', 'CPU', 'CPU', 2, 'Y', NULL, '2020-07-22 19:27:07', NULL, NULL, NULL, '2020-07-22 19:27:07', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SEPECSETTING', '서버세팅', 'DISK', 'DISK(GB)', 'DISK(GB)', 4, 'Y', NULL, '2020-07-22 19:27:07', NULL, NULL, NULL, '2020-07-22 19:27:07', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SEPECSETTING', '서버세팅', 'RAM', 'RAM', 'RAM', 3, 'Y', NULL, '2020-07-22 19:27:07', NULL, NULL, NULL, '2020-07-22 19:27:07', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SVC', '서비스종류', 'A', '컨테이너', 'Container', 10, 'Y', NULL, '2016-02-15 19:19:48', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('SVC', '서비스종류', 'B', '상품구매', 'Buy Goods', 7, 'N', NULL, '2015-12-16 12:11:28', NULL, NULL, NULL, '2019-05-08 16:53:44', NULL, NULL, NULL);

INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(1, 0, 'Root', 0, NULL, '최상위 메뉴', 0, 'Y', NULL, '2015-05-13 15:54:03', NULL, NULL, NULL, '2016-01-28 17:41:16', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'ROOT');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(2, 0, '포탈 관리', 28, NULL, '포탈을 관리합니다.', 10, 'Y', 'ui-icon-portal', '2015-05-13 15:54:03', NULL, NULL, NULL, '2021-08-10 15:46:25', '10', '0', '0:0:0:0:0:0:0:1', 'N', '');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(17, 10, '메뉴관리', 145, NULL, '관리자가 메뉴를 설정 합니다', 5, 'Y', NULL, '2015-05-13 15:54:03', NULL, NULL, NULL, '2016-02-02 14:58:02', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Menu Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(19, 37, '접속로그조회', 145, NULL, '사용자의 서버 접속 기록을 조회합니다. ', 2, 'Y', NULL, '2015-05-13 15:54:03', NULL, NULL, NULL, '2019-01-15 20:07:54', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'ConnectLog');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(20, 42, '프로그램사용빈도', 145, NULL, '사용자의 프로그램 사용빈도를 조회합니다. ', 4, 'Y', NULL, '2015-05-13 15:54:03', NULL, NULL, NULL, '2019-01-15 20:08:02', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'Program Statistics');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(28, 0, '홈', 1, NULL, '사용자', 0, 'Y', 'ui-icon-ntreec-user', '2015-05-13 15:54:03', NULL, NULL, NULL, '2018-08-16 18:06:44', '10', '0', '1.220.213.141', 'N', 'User info');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(35, 66, '코드관리', 145, NULL, '코드관리', 1, 'Y', NULL, '2015-05-13 15:54:03', '10', 'syk', '0:0:0:0:0:0:0:1', '2016-02-02 14:57:20', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Code Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(63, 0, '게시판', 28, NULL, '게시물', 14, 'Y', 'ui-icon-noti', '2015-05-22 13:57:29', 'menu', 'autolink', '0:0:0:0:0:0:0:1', '2018-11-16 11:21:04', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Notice');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(64, 46, '공지사항', 63, NULL, '공지사항', 0, 'Y', NULL, '2015-05-22 15:55:05', 'menu', 'autolink', '0:0:0:0:0:0:0:1', '2016-02-04 14:52:46', '10', '0', '127.0.0.1', 'N', 'Notice');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(65, 96, 'Q&A 답변', 63, NULL, '1:1 문의', 1, 'N', NULL, '2015-05-27 18:17:09', 'menu', 'autolink', '0:0:0:0:0:0:0:1', '2019-01-16 17:41:30', '1', '0', '0:0:0:0:0:0:0:1', 'N', '1:1 Q&A');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(69, 0, '클라우드 서버', 28, NULL, '클라우드 서버', 1, 'Y', 'ui-icon-cloud', '2015-12-17 16:56:24', '10', '0', '0:0:0:0:0:0:0:1', '2021-03-29 08:59:05', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Resources');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(70, 88, '서버현황', 69, NULL, '서버 상태 및 정보를 조회합니다.', 0, 'Y', NULL, '2015-12-17 16:57:07', '10', '0', '0:0:0:0:0:0:0:1', '2020-06-02 17:32:50', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Servers');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(74, 124, '월별서버사용', 116, NULL, '요금 및 이용내역', 5, 'N', NULL, '2015-12-18 10:46:30', '89', '0', '0:0:0:0:0:0:0:1', '2018-10-25 16:02:33', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Monthly Usage');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(75, 94, 'Q&A', 63, NULL, '나의 문의 내역', 5, 'Y', NULL, '2015-12-18 10:46:48', '89', '0', '0:0:0:0:0:0:0:1', '2021-03-30 14:05:08', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Q&A');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(78, 118, '스냅샷', 69, NULL, '스냅샷', 8, 'N', NULL, '2015-12-18 10:49:01', '89', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:58:39', '88', '0', '0:0:0:0:0:0:0:1', 'N', 'Snapshot');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(79, 125, '개인템플릿', 69, NULL, '사용자가 개인템플릿을 이용하여 서버생성요청을 합니다.', 9, 'N', NULL, '2015-12-18 10:51:00', '89', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:58:51', '88', '0', '0:0:0:0:0:0:0:1', 'N', 'Template');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(80, 43, '역할관리', 145, NULL, '역할관리', 6, 'Y', NULL, '2015-12-18 11:38:56', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:50', NULL, NULL, NULL, 'N', 'Role Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(84, 22, '팀 관리', 93, NULL, '팀 관리(신규,삭제,저장,조회)합니다. ', 5, 'Y', NULL, '2015-12-24 14:22:03', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:50', NULL, NULL, NULL, 'N', 'Team Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(85, 0, '결재', 28, NULL, '결재', 3, 'Y', 'ui-icon-approval', '2015-12-28 11:02:07', '10', '0', '0:0:0:0:0:0:0:1', '2021-03-29 08:58:15', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Works');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(86, 93, '결재', 85, NULL, '결재를 승인/반려 합니다.', 2, 'Y', NULL, '2015-12-28 11:03:08', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:51', NULL, NULL, NULL, 'N', 'Request');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(87, 95, '작업함', 85, NULL, '서버, 접근제어등 작업들의 목록, 여러 개를 모아서 결재 요청서를 작성할 수 있다.', 0, 'Y', NULL, '2015-12-28 15:41:36', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-27 16:33:33', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'Works');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(88, 0, '클라우드 관리', 2, NULL, '클라우드 서버 관리', 3, 'Y', NULL, '2016-01-07 10:27:05', '10', '0', '0:0:0:0:0:0:0:1', '2019-01-16 16:49:27', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'Cloud Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(89, 101, '데이터 센터 관리', 88, NULL, '데이터센터를 관리(수집, 신규, 삭제, 저장, 조회) 합니다.  ', 2, 'Y', NULL, '2016-01-07 10:27:39', '10', '0', '0:0:0:0:0:0:0:1', '2016-02-01 15:08:35', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Data Center Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(91, 103, '표준템플릿관리', 88, NULL, '표준템플릿관리', 3, 'Y', NULL, '2016-01-07 16:09:59', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-08 18:08:26', '1', '0', '1.220.213.141', 'N', 'OS Type Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(92, 100, '디스크별 요금 관리', 88, NULL, '디스크별 요금 관리', 6, 'N', NULL, '2016-01-07 16:13:07', '10', '0', '0:0:0:0:0:0:0:1', '2019-01-30 15:22:48', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'Disk Type Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(93, 0, '사용자 관리', 2, NULL, '사용자 관리', 0, 'Y', NULL, '2016-01-07 16:15:16', '10', '0', '0:0:0:0:0:0:0:1', '2016-01-07 16:16:35', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'User Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(94, 92, '사용자 조회', 93, NULL, '사용자 정보를 조회합니다.', 0, 'Y', NULL, '2016-01-07 16:17:13', '10', '0', '0:0:0:0:0:0:0:1', '2020-06-02 16:29:31', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'User Search');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(98, 104, 'VM 사양관리', 88, NULL, '서버의 표준사양(CPU,RAM,DISK)을 관리(신규, 삭제, 저장, 조회) 합니다.', 4, 'N', NULL, '2016-01-07 16:35:51', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-18 14:11:36', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'VM Spec Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(104, 111, '결재이력', 85, NULL, '결재한 이력에 대한 정보를 조회합니다.', 3, 'Y', NULL, '2016-01-12 13:13:11', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-27 16:35:13', '1', '0', '1.220.213.141', 'N', 'Approved History');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(105, 112, '결재함', 85, NULL, '결재 요청 이력을 조회합니다.', 1, 'Y', NULL, '2016-01-12 14:16:13', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-27 16:35:03', '1', '0', '1.220.213.141', 'N', 'Reqeust History');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(110, 122, 'VM 모니터링', 116, NULL, 'VM 모니터링 정보를 제공합니다.', 3, 'Y', NULL, '2016-01-25 09:39:26', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 16:03:14', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Monitor');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(112, 119, '클러스터', 116, NULL, '클러스터에 대한 정보를 제공합니다.', 1, 'Y', NULL, '2016-01-25 09:40:48', '10', '0', '0:0:0:0:0:0:0:1', '2021-03-29 14:14:34', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Cluster');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(113, 123, '호스트', 116, NULL, '호스트에 대한 정보를 제공합니다. ', 2, 'Y', NULL, '2016-01-25 09:41:28', '10', '0', '0:0:0:0:0:0:0:1', '2021-03-29 14:14:41', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Host');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(116, 0, '모니터링', 28, NULL, '모니터링', 5, 'Y', 'ui-icon-monitor', '2016-01-26 14:10:45', '10', '0', '0:0:0:0:0:0:0:1', '2021-03-29 08:58:37', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Monitoring');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(118, 127, '진행중 작업', 116, NULL, '진행중 작업', 4, 'Y', NULL, '2016-01-28 17:29:39', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 16:03:17', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Work in Progress');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(120, 146, '접근통제 작업', 88, NULL, '사용자로부터 요청받은 접근통제 작업들을 관리(작업완료, 작업메시지, 작업거부합니다. ', 6, 'Y', NULL, '2016-02-01 14:00:55', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:48:08', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'FW Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(123, 133, '담당자 변경', 69, NULL, '관리자가 담당자를 일괄 변경할 수 있습니다.(인사이동, 퇴사 등이 발생시)', 16, 'Y', NULL, '2016-02-16 09:07:47', '10', '0', '0:0:0:0:0:0:0:1', '2021-02-02 08:49:19', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Owner All Change');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(124, 0, '통계', 28, NULL, NULL, 4, 'Y', 'ui-icon-stat', '2019-05-08 14:34:04', '1', '0', '0:0:0:0:0:0:0:1', '2021-03-29 08:58:20', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Statistics');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(125, 161, 'VM현황', 124, NULL, 'VM 통계', 1, 'Y', NULL, '2019-05-08 14:34:33', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:36:11', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'VM Status');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(126, 166, '라이센스현황', 124, NULL, '라이센스 현황 통계', 2, 'Y', NULL, '2019-05-09 10:26:37', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:36:07', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'License Status');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(127, 167, '데이터센터현황', 124, NULL, '데이터센터별 리소스 통계', 3, 'Y', NULL, '2019-05-09 10:27:01', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:36:17', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Data Center Status');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(128, 168, '결재현황', 124, NULL, '표준템플릿 통계', 4, 'Y', NULL, '2019-05-09 10:27:22', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:37:37', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Template Status');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(130, 0, '대시보드', 116, NULL, NULL, 0, 'N', NULL, '2018-08-14 18:05:14', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 16:02:41', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Dashboard');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(131, 139, '경고목록', 116, NULL, 'Infra, VM 등에 대한 경고목록을 제공합니다.', 7, 'Y', NULL, '2018-08-14 18:07:16', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:52', NULL, NULL, NULL, 'N', 'Warnings');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(133, 141, '데이터스토어', 116, NULL, '데이터스토어에 대한 정보를 제공합니다.', 3, 'Y', NULL, '2018-09-07 14:05:41', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 13:42:47', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Datastore');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(134, 142, '접근통제', 69, NULL, '서버에 접근할 수 있게 요청 합니다.', 4, 'N', NULL, '2018-09-21 20:19:31', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:58:55', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Firewall');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(135, 143, '서버 사용량 내역', 116, NULL, '월별 서버갯수, 비용등에 대한 정보를 제공합니다.', 8, 'Y', NULL, '2018-10-02 20:38:04', '10', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:52', NULL, NULL, NULL, 'N', 'Server Usage');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(136, 144, '완료함', 85, NULL, '완료함', 3, 'N', NULL, '2018-10-08 17:37:55', '10', '0', '0:0:0:0:0:0:0:1', '2018-11-09 16:47:32', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Completed');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(137, 145, '반려함', 85, NULL, '반려함', 5, 'N', NULL, '2018-10-08 17:38:18', '10', '0', '0:0:0:0:0:0:0:1', '2018-11-09 16:47:47', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Denied');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(139, 148, '장기간 미사용 서버', 88, NULL, '장기간 미사용 서버를 조회합니다.', 10, 'Y', NULL, '2018-10-08 17:51:50', '10', '0', '0:0:0:0:0:0:0:1', '2018-10-12 16:03:05', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Long-term Unused Server');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(140, 149, '월별요금조회', 93, NULL, '사용자들(팀단위)의 월별요금을 조회합니다.', 6, 'Y', NULL, '2018-10-12 16:09:50', '9', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:52', NULL, NULL, NULL, 'N', 'Monthly Rate');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(142, 151, 'VRA카탈로그관리', 88, NULL, 'VRA 카탈로그를 관리(수집, 저장, 조회) 합니다.', 0, 'N', NULL, '2018-11-15 10:26:36', '10', '0', '0:0:0:0:0:0:0:1', '2020-03-05 16:38:52', '10', '0', '10.11.10.85', 'N', 'VRA Catalog Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(143, 152, 'VRA카탈로그요청', 69, NULL, '사용자가 VRA 카탈로그를 요청합니다.', 5, 'N', NULL, '2018-11-15 10:27:44', '10', '0', '0:0:0:0:0:0:0:1', '2020-03-05 16:37:29', '10', '0', '10.11.10.85', 'N', 'VRA Catalog Request');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(144, 153, 'VRA카탈로그 생성요청', 69, NULL, NULL, 6, 'N', NULL, '2018-11-23 13:14:20', '10', '0', '106.241.16.118', '2020-03-05 16:37:37', '10', '0', '10.11.10.85', 'N', 'New VRA Catalog');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(145, 0, '시스템관리', 2, NULL, NULL, 5, 'Y', NULL, '2019-01-16 16:49:15', '1', '0', '0:0:0:0:0:0:0:1', '2019-04-30 10:47:11', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'System Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(146, 106, '요금관리', 88, NULL, 'CPU,메모리,디스크 요금관리', 7, 'Y', NULL, '2019-01-18 21:58:54', '1', '0', '0:0:0:0:0:0:0:1', '2019-06-03 19:09:53', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Rate Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(147, 154, '라이센스조회', 88, NULL, '데이터센터에 존재하는 라이센스를 조회합니다.', 11, 'Y', NULL, '2019-01-22 17:46:48', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:53', NULL, NULL, NULL, 'N', 'License');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(148, 155, '디스크 종류 관리', 88, NULL, '디스크 종류(타입)를 관리(신규, 수정, 저장, 조회)합니다', 10, 'N', NULL, '2019-01-23 09:35:00', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:39:17', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'Disk Type Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(149, 156, 'IP 대역 관리', 88, NULL, '데이터센터의 IP 대역을 관리합니다.', 12, 'N', NULL, '2019-04-02 11:23:40', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:36:27', '1', '0', '0:0:0:0:0:0:0:1', 'N', 'IP Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(150, 157, '사양 제한 설정', 88, NULL, '서버 생성시 사용될 리소스의 사양(CPU, 메모리,디스크)을 설정합니다.', 13, 'Y', NULL, '2019-04-09 10:41:01', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:43:51', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Spec Setting');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(151, 158, 'IP현황', 88, NULL, '서버에서 사용중인 IP의 현황을 조회합니다', 16, 'Y', NULL, '2019-04-15 16:18:08', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:38:46', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'IP Status');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(152, 159, 'IP예약', 88, NULL, '사용될 IP를 예약합니다', 15, 'Y', NULL, '2019-04-15 16:19:50', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:53', NULL, NULL, NULL, 'N', 'IP Reservation');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(153, 160, '환경설정', 145, NULL, '시스템 환경설정을 변경합니다.', 0, 'Y', NULL, '2019-04-30 10:47:01', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-08 09:10:50', NULL, NULL, NULL, 'N', 'Settings');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(155, 162, '부서별 세팅', 88, NULL, '부서별로 클러스터, IP, VM Prefix등을 관리합니다', 17, 'Y', NULL, '2019-05-20 16:12:04', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:02', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'Dept Settings');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(158, 163, 'NAS 경로 관리', 88, NULL, 'NAS의 경로를 관리(신규, 삭제, 저장, 조회)합니다.', 18, 'Y', NULL, '2019-05-22 14:54:21', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:11', '10', '0', '0:0:0:0:0:0:0:1', 'N', 'NAS Route Mng');
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(161, 165, 'IP-네트웍 맵핑', 88, NULL, 'IP대역에 따른 VM Network 맵핑', 19, 'Y', NULL, '2019-07-12 03:38:11', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:19', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(169, 175, '오버사이즈 서버', 88, NULL, NULL, 10, 'Y', NULL, '2019-08-02 01:49:08', '10', '0', '0:0:0:0:0:0:0:1', '2019-08-02 10:49:08', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(176, 181, 'APP 관리', 88, NULL, NULL, 20, 'N', NULL, '2019-11-01 17:55:56', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:27', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(177, 182, 'VM명 Prefix', 88, NULL, NULL, 21, 'N', NULL, '2019-11-05 15:02:19', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:34', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(178, 183, '사용자 코드관리', 145, NULL, '사용자 코드관리', 7, 'Y', NULL, '2019-11-07 17:44:46', '10', '0', '0:0:0:0:0:0:0:1', '2019-11-07 17:44:47', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(179, 184, '씬 클라이언트 관리', 88, NULL, NULL, 22, 'Y', NULL, '2019-11-05 15:02:19', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:41', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(181, 0, '서비스 신청', 28, NULL, NULL, 0, 'Y', 'ui-icon-portal', '2020-12-09 15:54:18', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:46:34', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(182, 164, '서버 신청', 181, NULL, NULL, 1, 'Y', NULL, '2020-12-09 15:54:40', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 10:52:24', '1', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(183, 188, '신규 요청', 182, NULL, NULL, 1, 'Y', NULL, '2020-12-09 15:54:52', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 10:52:35', '1', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(184, 192, '자원 변경 요청', 182, NULL, NULL, 2, 'Y', NULL, '2020-12-09 15:55:13', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:54:19', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(185, 193, '삭제 요청', 182, NULL, NULL, 3, 'Y', NULL, '2020-12-09 15:55:20', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:54:33', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(187, 186, '복원 요청', 182, NULL, NULL, 5, 'Y', NULL, '2020-12-09 15:55:48', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 09:02:45', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(188, 187, 'OS 변경 요청', 182, NULL, NULL, 6, 'N', NULL, '2020-12-09 15:55:57', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:52:10', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(189, 0, '계정 및 접근통제 신청', 181, NULL, NULL, 2, 'N', NULL, '2020-12-09 15:56:07', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:51:08', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(190, 0, '신규 요청', 189, NULL, NULL, 1, 'Y', NULL, '2020-12-09 15:56:15', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:56:15', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(191, 0, '접근제어 변경 요청', 189, NULL, NULL, 2, 'Y', NULL, '2020-12-09 15:56:27', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:56:28', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(192, 0, '삭제 요청', 189, NULL, NULL, 3, 'Y', NULL, '2020-12-09 15:56:34', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:56:34', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(193, 0, 'IP 일괄 변경 요청', 189, NULL, NULL, 4, 'Y', NULL, '2020-12-09 15:56:47', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:56:48', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(198, 0, '개인 템플릿 신청', 181, NULL, NULL, 4, 'N', NULL, '2020-12-09 15:57:38', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:51:44', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(199, 0, '신규 요청', 198, NULL, NULL, 1, 'Y', NULL, '2020-12-09 15:57:45', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:57:45', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(200, 0, '기간 연장 요청', 198, NULL, NULL, 2, 'Y', NULL, '2020-12-09 15:57:52', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:57:52', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(201, 0, '삭제 요청', 198, NULL, NULL, 3, 'Y', NULL, '2020-12-09 15:57:57', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:57:58', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(202, 0, '요청이력', 181, NULL, NULL, 5, 'Y', NULL, '2020-12-09 15:58:06', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:58:07', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(203, 0, '작업함', 181, NULL, NULL, 6, 'Y', NULL, '2020-12-09 15:58:12', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:58:12', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(204, 189, '기간연장 요청', 182, NULL, NULL, 4, 'Y', NULL, '2020-12-10 11:01:50', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 11:01:49', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(205, 185, '신청 이력', 28, NULL, NULL, 2, 'Y', 'ui-icon-approval', '2020-12-10 11:02:32', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 11:02:31', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(206, 191, 'Q&A(prev)', 63, NULL, NULL, 6, 'Y', NULL, '2021-03-30 14:04:42', '10', '0', '0:0:0:0:0:0:0:1', '2021-03-30 14:05:11', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(207, 194, '사용 기간 설정', 88, NULL, NULL, 14, 'Y', NULL, '2021-08-11 15:35:25', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-18 15:22:09', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(208, 195, '비용현황', 124, NULL, NULL, 6, 'Y', NULL, '2021-08-11 12:38:34', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:38:35', NULL, NULL, NULL, 'N', NULL);
INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(209, 196, '비용추이', 124, NULL, NULL, 12, 'Y', NULL, '2021-08-11 12:39:12', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:39:12', NULL, NULL, NULL, 'N', NULL);


INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '0', 'en', 'Approval Status');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '10', 'en', 'Retention System Status');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '11', 'en', 'List Of Requests');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '13', 'en', 'Server Usage by Group');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '14', 'en', 'Approval Status');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '15', 'en', 'Expiration List');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '2', 'en', 'Datastore Usage');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '3', 'en', 'Host Performance');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '4', 'en', 'VM Performance');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '5', 'en', 'Number of Servers by Group');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '6', 'en', 'Warnings');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '7', 'en', 'Announcements');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '8', 'en', 'Q&A');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DASHBOARD', 'ITEM_NM', '9', 'en', 'VM number and cost status');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'APP_KIND.A', 'en', 'Approve');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'APP_KIND.C', 'en', 'Cancel');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'APP_KIND.D', 'en', 'Deny');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'APP_KIND.R', 'en', 'Request');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'APP_KIND.W', 'en', 'Woking');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'BANK.S', 'en', 'Shinhan');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'BIZ_KIND.C', 'en', 'corporation');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'BIZ_KIND.I', 'en', 'individual business?');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'BIZ_KIND.N', 'en', 'non-commercial');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'CARD.S', 'en', 'Shinhan Card');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'CUD.C', 'en', 'Create');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'CUD.D', 'en', 'Delete');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'CUD.U', 'en', 'Update');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'ETC_SVC.G', 'en', 'Imge');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'ETC_SVC.I', 'en', 'IP');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE.D', 'en', '￦/Day');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE.H', 'en', '￦/Hour');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE.M', 'en', '￦/Month');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE_FREE.F', 'en', 'Free');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE_TYPE.D', 'en', 'Day');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE_TYPE.H', 'en', 'Hour');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE_TYPE.M', 'en', 'Month');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE_UNIT.D', 'en', 'Day');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE_UNIT.H', 'en', 'Hour');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'FEE_UNIT.M', 'en', 'Month');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'GOODS_CATEGORY.1', 'en', 'BigData');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'HV_ETC_PRODUCT_V.VRA', 'en', 'vRA');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'HYPERVISOR.V', 'en', 'VMware');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'INQ.ERR', 'en', 'Error');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'INQ.ETC', 'en', 'etc');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'INQ.FAIL', 'en', 'Failure');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'INQ.INQ', 'en', 'Inquiry');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'INQ.REQ', 'en', 'Request');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PAY_KIND.A', 'en', 'account transfer');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PAY_KIND.C', 'en', 'credit card');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PAY_YN.N', 'en', 'No Pay');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PAY_YN.Y', 'en', 'Paid');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_KIND.cpu_usage', 'en', 'CPU Usage(%)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_KIND.cpu_usagemhz', 'en', 'CPU Usage(MHz)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_KIND.disk_capacity', 'en', 'Disk Capacity(KB)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_KIND.disk_usage', 'en', 'Disk Usage(KB/s)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_KIND.disk_used', 'en', 'Disk Used(KB)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_KIND.mem_consumed', 'en', 'Memory Consumed(KB)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_KIND.mem_usage', 'en', 'Memory Usage(%)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_KIND.net_usage', 'en', 'Network Usage(KB/s)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM.1d', 'en', '1Day');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM.1h', 'en', '1Hour');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM.1m', 'en', '1Month');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM.1w', 'en', '1Week');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM.1y', 'en', '1Year');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM1.1d', 'en', '1Day');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM1.1m', 'en', '1Month');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM1.1w', 'en', '1Week');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PERF_TERM1.1y', 'en', '1Year');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PROTOCOL.ICAMP', 'en', 'ICAMP');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PROTOCOL.TCP', 'en', 'TCP');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PROTOCOL.UDP', 'en', 'UDP');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PROTOCOL1.TCP', 'en', 'TCP');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PROTOCOL1.UDP', 'en', 'UDP');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PURPOSE.ALY', 'en', 'Analysis');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PURPOSE.BLA', 'en', 'Build Automation');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PURPOSE.BLD', 'en', 'Build');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PURPOSE.GIT', 'en', 'Configuration Mng');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PURPOSE.LGC', 'en', 'Logic');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PURPOSE.PJT', 'en', 'Project Mng');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PURPOSE.SMT', 'en', 'Simulation');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'PURPOSE.TST', 'en', 'Test');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'P_KUBUN.D', 'en', 'Development');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'P_KUBUN.M', 'en', 'Production');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'P_KUBUN.P', 'en', 'Pre Prodiction');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'RUN_CD.C', 'en', 'Requested');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'RUN_CD.F', 'en', 'Fail');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'RUN_CD.R', 'en', 'Run');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'RUN_CD.S', 'en', 'Stop');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'RUN_CD.W', 'en', 'InProgress');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.1', 'en', '1');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.10', 'en', '10');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.11', 'en', '11');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.12', 'en', '12');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.2', 'en', '2');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.3', 'en', '3');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.4', 'en', '4');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.5', 'en', '5');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.6', 'en', '6');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.7', 'en', '7');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.8', 'en', '8');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_MM.9', 'en', '9');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_YYYY.2010', 'en', '2010');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_YYYY.2011', 'en', '2011');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_YYYY.2012', 'en', '2012');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_YYYY.2013', 'en', '2013');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_YYYY.2014', 'en', '2014');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_YYYY.2015', 'en', '2015');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_YYYY.2016', 'en', '2016');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SEARCH_YYYY.2017', 'en', '2017');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.A', 'en', 'NAT');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.B', 'en', 'Buy Goods');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.C', 'en', 'Server');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.D', 'en', 'Disk');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.F', 'en', 'Firewall');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.G', 'en', 'Image');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.I', 'en', 'IP');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.N', 'en', 'Network Connection');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'SVC.S', 'en', 'Server');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.A', 'en', 'NAT');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.C', 'en', 'Catalog');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.D', 'en', 'Disk');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.F', 'en', 'Firewall');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.G', 'en', 'Imge');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.I', 'en', 'IP');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.N', 'en', 'Network Connection');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.R', 'en', 'Revert VM');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.S', 'en', 'Server');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_CD.T', 'en', 'Snapshot');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_STATUS_CD.D', 'en', 'Deny');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_STATUS_CD.F', 'en', 'Fail');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_STATUS_CD.S', 'en', 'Success');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_STATUS_CD.W', 'en', 'Processing');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_STATUS_CD1.F', 'en', 'Success');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_STATUS_CD1.S', 'en', 'Success');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TASK_STATUS_CD1.W', 'en', 'Processing');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TEST.T', 'en', 'TEST');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TMS_TYPE.INS', 'en', 'Request Date/Time');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'TMS_TYPE.UPD', 'en', 'Payment Date');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'USER_TYPE.16', 'en', 'Company Admin');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'USER_TYPE.18', 'en', 'Group Member');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'USER_TYPE.19', 'en', 'Portal Admin');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'DD_VALUE', 'USER_TYPE.21', 'en', 'Group Admin');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_DDIC', 'EN_DD_DESC', 'ENV', 'en', 'Portal Contact Manager');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '1', 'en', 'Top menu');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '105', 'en', 'View the approval request history');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '110', 'en', 'Provides VM monitoring information');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '112', 'en', 'Provides information about the Cluster');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '113', 'en', 'Provides information about the Host');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '116', 'en', 'Monitoring');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '118', 'en', 'Progressing');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '120', 'en', 'Manage Firewall requested by the user (Task Completed, Task Message, Task Denied)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '123', 'en', 'Administrators can change the personnel in bulk (when moving personnel, leaving office, etc.)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '125', 'en', 'VM Statistics');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '126', 'en', 'License Status Statistics');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '127', 'en', 'Resource Statistics by Data Center');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '128', 'en', 'Standard Templates Statistics');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '131', 'en', 'Provides warnings about Infra, VM and so on');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '133', 'en', 'Provides information about the Data Store');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '134', 'en', 'Requests access to the Server');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '135', 'en', 'Provides information about Monthly Number of Servers, Cost, and so on');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '136', 'en', 'Completed');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '137', 'en', 'Rejected');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '139', 'en', 'View Dormant Servers');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '140', 'en', 'View Monthly fee for users (on a Dept basis)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '141', 'en', 'Server Creation Request');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '142', 'en', 'Manage vRA catalog(Collect, Save, View)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '143', 'en', 'Users request Package Servers');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '146', 'en', 'CPU, Memory and Disk Charge Management');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '147', 'en', 'View licenses that exist in the Data Center');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '148', 'en', 'Manage Disk Type(Create, Modify, Save, View)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '149', 'en', 'Manage IP bands in the Data Center');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '150', 'en', 'Sets the specification of the resource(CPU, Memory, Disk) to be used when creating the Server');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '151', 'en', 'View the IP status in use on the Server');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '152', 'en', 'Reserves the IP to be used');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '153', 'en', 'Change System Settings');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '155', 'en', 'Manage Clusters, IPs, and VM prefixes by Dept');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '158', 'en', 'Manage path of NAS(Create, Delete, Save, View)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '161', 'en', 'IP Network Mapping');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '17', 'en', 'The admin sets the menu');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '19', 'en', 'View the user''s Server Connection history');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '2', 'en', 'Manage portal');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '20', 'en', 'View user''s Program Usage Frequency');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '28', 'en', 'User');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '35', 'en', 'Code Management');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '63', 'en', 'Post');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '64', 'en', 'Notice');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '65', 'en', '1:1 Q&A');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '69', 'en', 'Cloud Server');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '70', 'en', 'View Server Status and Information');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '74', 'en', 'Fee and Usage Details');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '75', 'en', 'My Inquiry History');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '76', 'en', 'Disk');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '78', 'en', 'Snapshot');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '79', 'en', 'The user requests to create a Server using a Private Template');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '80', 'en', 'Role Management');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '84', 'en', 'Manage teams(Create, Delete, Save, View)');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '85', 'en', 'Approval');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '86', 'en', 'Approve/Reject Approval');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '87', 'en', 'You can collect a list of tasks such as Server, Firewall, etc., and request an Approval');
INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'DESC', '88', 'en', 'Cloud Server Management');

INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(4, 0, '사용자관리', '사용자관리', 'N', '/admin/user/index.jsp', '2015-05-14 15:06:25', NULL, NULL, NULL, '2015-05-14 15:06:25', 'prgm', 'syk', '127.0.0.1', '/api/user/', 'G', 'user', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(7, 0, '게시판', '게시판', 'N', '/admin/notice/index.jsp', '2015-05-14 15:06:25', NULL, NULL, NULL, '2015-05-14 15:06:25', NULL, NULL, NULL, '/api/notice/', 'G', 'user', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(10, 0, '메뉴관리', '메뉴관리', 'Y', '/admin/menu/index.jsp', '2015-05-14 15:06:25', '123', 'syk', '0:0:0:0:0:0:0:1', '2015-05-14 15:06:25', '10', 'syk', '0:0:0:0:0:0:0:1', '/api/menu/', 'G', 'menu', 3, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(22, 0, '팀관리', '팀관리', 'Y', '/clovirsm/site/team/index.jsp', '2015-05-14 15:06:25', '3', 'syk', '127.0.0.1', '2015-05-14 15:06:25', NULL, NULL, NULL, '/api/site_group/', 'G', 'team', 3, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(37, 0, '접속로그조회', '접속로그조회', 'Y', '/admin/log/index.jsp', '2015-05-14 15:06:26', '3', 'syk', '127.0.0.1', '2019-01-15 20:07:54', '1', '0', '0:0:0:0:0:0:0:1', '/api/log_connect/', 'GL', 'log', 20, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(42, 0, '프로그램사용빈도조회', '프로그램사용빈도조회', 'Y', '/admin/logPrgm/index.jsp', '2015-05-14 15:06:26', '3', 'syk', '211.51.22.223', '2019-01-15 20:08:02', '1', '0', '0:0:0:0:0:0:0:1', '/api/log_prgm/', 'GL', 'logPrgm', 22, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(43, 0, '권한관리', '권한관리', 'Y', '/admin/role/index.jsp', '2015-05-14 15:06:26', '3', 'syk', '127.0.0.1', '2015-05-14 15:06:26', '3', 'syk', '127.0.0.1', '/api/role/', 'G', 'role', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(46, 0, '공지사항', '공지사항(관리자)', 'Y', '/admin/notice/index.jsp', '2015-05-14 15:06:26', '3', 'syk', '211.51.22.223', '2015-05-14 15:06:26', '3', 'hanna', '211.51.22.223', '/api/notice/', 'G', 'notiAdm', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(66, 0, '자료사전', '자료사전', 'Y', '/admin/ddic/index.jsp', '2015-05-14 15:06:26', 'prgm', 'hanna', '211.51.22.223', '2015-05-14 15:06:26', 'ddic', 'hanna', '211.51.22.223', '/api/fm_ddic/', 'G', 'ddic', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(88, 0, '서버현황', '서버', 'Y', '/clovirsm/resources/vm/index.jsp', '2015-12-17 10:52:27', '3', '0', '0:0:0:0:0:0:0:1', '2020-06-02 17:32:50', '10', '0', '0:0:0:0:0:0:0:1', '/api/vm/', 'GL', 'VM', 23, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(92, 0, '사용자 조회', '사용자 조회', 'Y', '/clovirsm/admin/userSearch/index.jsp', '2015-12-23 14:11:53', '3', '0', '0:0:0:0:0:0:0:1', '2020-06-02 16:29:31', '10', '0', '0:0:0:0:0:0:0:1', '/api/user_mng/', 'GL', 'userSearch', 23, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(93, 0, '결재요청', '결재요청', 'Y', '/clovirsm/workflow/approval/index.jsp', '2015-12-28 10:58:23', '3', '0', '0:0:0:0:0:0:0:1', '2016-01-12 14:15:28', '3', '0', '0:0:0:0:0:0:0:1', '/api/approval/', 'G', 'approval', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(94, 0, 'Q&A', '나의문의내역', 'Y', '/admin/inquiry/index.jsp', '2015-12-28 11:10:37', '3', '0', '0:0:0:0:0:0:0:1', '2021-03-30 14:05:08', '10', '0', '0:0:0:0:0:0:0:1', NULL, 'G', 'inquiry', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(95, 0, '작업함', '작업목록', 'Y', '/clovirsm/workflow/work/index.jsp', '2015-12-28 15:40:25', '3', '0', '0:0:0:0:0:0:0:1', '2019-05-27 16:33:33', '1', '0', '0:0:0:0:0:0:0:1', '/api/work/', 'G', 'works', 19, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(101, 0, '데이터 센터 관리', '어드민:데이터 센터 관리', 'Y', '/clovirsm/admin/dcMng/index.jsp', '2016-01-06 14:06:07', '3', '0', '0:0:0:0:0:0:0:1', '2016-01-06 14:54:10', '3', '0', '0:0:0:0:0:0:0:1', '/api/dc_mng/', 'G', 'dcMng', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(102, 0, '표준템플릿 관리', '표준템플릿 관리', 'Y', '/clovirsm/admin/ostypeMng/index.jsp', '2016-01-06 15:12:05', '3', '0', '0:0:0:0:0:0:0:1', '2016-03-18 11:31:03', '999999', '0', '127.0.0.1', '/api/ostype/', 'G', 'osTypeMng', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(103, 0, '표준템플릿관리', '표준템플릿 관리', 'Y', '/clovirsm/admin/ostypeMng/index.jsp', '2016-01-06 15:12:05', '3', '0', '0:0:0:0:0:0:0:1', '2019-05-08 18:08:26', '1', '0', '1.220.213.141', '/api/ostype/', 'G', 'osTypeMng', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(104, 0, 'VM 사양관리', '서버사양별 요금 관리', 'N', '/clovirsm/admin/vmspec/index.jsp', '2016-01-07 16:26:01', '3', '0', '0:0:0:0:0:0:0:1', '2021-08-18 14:11:36', '10', '0', '0:0:0:0:0:0:0:1', '/api/vm_spec_mng/', 'G', 'vmSpecMng', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(106, 0, '요금관리', '기타요금', 'Y', '/clovirsm/admin/etcFee/index.jsp', '2016-01-11 11:13:20', '3', '0', '0:0:0:0:0:0:0:1', '2019-06-03 19:09:53', '10', '0', '0:0:0:0:0:0:0:1', '/api/etc_fee_mng/', 'G', 'etcFeeMng', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(111, 0, '결재이력', '결재이력', 'Y', '/clovirsm/workflow/approvalHistory/index.jsp', '2016-01-12 13:09:04', '3', '0', '0:0:0:0:0:0:0:1', '2019-05-27 16:35:13', '1', '0', '1.220.213.141', '/api/approval_history/', 'G', 'approvalHistory', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(112, 0, '요청이력', '결재 의뢰 이력', 'Y', '/clovirsm/workflow/requestHistory/index.jsp', '2016-01-12 14:15:06', '3', '0', '0:0:0:0:0:0:0:1', '2019-05-27 16:35:03', '1', '0', '1.220.213.141', '/api/request_history/', 'G', 'requestHistory', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(118, 0, '스냅샷', '스냅샷', 'N', '/clovirsm/resources/snapshot/index.jsp', '2016-01-22 17:43:25', '3', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:58:39', '88', '0', '0:0:0:0:0:0:0:1', '/api/snapshot/', 'G', 'snapshot', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(119, 0, '클러스터', NULL, 'Y', '/clovirsm/monitor/cluster/index.jsp', '2016-01-22 17:56:52', '3', '0', '0:0:0:0:0:0:0:1', '2021-03-29 14:14:34', '10', '0', '0:0:0:0:0:0:0:1', '/api/admin_monitor/list/', 'G', 'monitorCluster', 19, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(122, 0, 'VM 모니터링', 'VM모니터링', 'Y', '/clovirsm/monitor/vm/index.jsp', '2016-01-25 09:33:07', '3', '0', '0:0:0:0:0:0:0:1', '2020-12-09 16:03:14', '10', '0', '0:0:0:0:0:0:0:1', '/api/vm/', 'G', 'monitorVM', 19, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(123, 0, '호스트', NULL, 'Y', '/clovirsm/monitor/host/index.jsp', '2016-01-25 09:35:29', '3', '0', '0:0:0:0:0:0:0:1', '2021-03-29 14:14:41', '10', '0', '0:0:0:0:0:0:0:1', '/api/admin_monitor/', 'G', 'monitorHost', 19, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(125, 0, '개인템플릿', '이미지', 'N', '/clovirsm/resources/image/index.jsp', '2016-01-26 13:12:37', '3', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:58:51', '88', '0', '0:0:0:0:0:0:0:1', '/api/image/', 'G', 'image', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(127, 0, '진행중 작업', NULL, 'Y', '/clovirsm/monitor/monitor_task.jsp', '2016-01-28 15:23:11', '3', '0', '0:0:0:0:0:0:0:1', '2020-12-09 16:03:17', '10', '0', '0:0:0:0:0:0:0:1', '/api/monitor/', 'G', 'taskIng', 19, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(133, 0, '담당자 변경', '담당자 일괄 변경', 'Y', '/clovirsm/resources/all_change_owner/index.jsp', '2016-02-16 09:05:52', '3', '0', '0:0:0:0:0:0:0:1', '2021-02-02 08:49:19', '10', '0', '0:0:0:0:0:0:0:1', '/api/all_change_owner/', 'G', 'ownerAllChange', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(139, 0, '경고목록', '경고목록', 'Y', '/clovirsm/monitor/alarm/index.jsp', '2019-05-08 16:46:08', NULL, NULL, NULL, '2019-05-08 16:46:08', NULL, NULL, NULL, '/api/admin_monitor/', 'G', 'alaram', NULL, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(141, 0, '데이터스토어', NULL, 'Y', '/clovirsm/monitor/ds/index.jsp', '2019-05-08 16:46:08', NULL, NULL, NULL, '2020-12-10 13:42:47', '10', '0', '0:0:0:0:0:0:0:1', '/api/admin_monitor/', NULL, 'datastore', NULL, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(142, 0, '접근통제', '방화벽', 'N', '/clovirsm/resources/fw/index.jsp', '2016-01-27 17:19:29', '3', '0', '0:0:0:0:0:0:0:1', '2020-12-09 15:58:55', '10', '0', '0:0:0:0:0:0:0:1', '/api/fw/', 'G', 'fw', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(143, 0, '서버 사용량 내역', '서버 사용량 내역', 'Y', '/clovirsm/monitor/useHistory/index.jsp', '2019-05-08 16:46:08', NULL, NULL, NULL, '2019-05-08 16:46:08', NULL, NULL, NULL, '/api/use_history/', NULL, 'vmHistory', NULL, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(146, 0, '접근통제 작업', '방화벽관리', 'Y', '/clovirsm/admin/fwMng/index.jsp', '2016-01-29 16:37:44', '3', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:48:08', '1', '0', '0:0:0:0:0:0:0:1', '/api/fw_mng/', 'G', 'fwMng4Vm', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(148, 0, '장기간 미사용 서버', '장기간 미사용 서버', 'Y', '/clovirsm/admin/notusevmReal/index.jsp', '2016-01-29 16:37:44', '3', '0', '0:0:0:0:0:0:0:1', '2016-02-05 11:25:02', '3', '0', '0:0:0:0:0:0:0:1', '/api/not_use_vm/', 'G', 'notusevm', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(149, 0, '그룹별월별요금조회', '그룹별월별요금조회', 'Y', '/clovirsm/admin/mmState/index.jsp', '2016-01-29 16:37:44', '3', '0', '0:0:0:0:0:0:0:1', '2016-02-05 11:25:02', '3', '0', '0:0:0:0:0:0:0:1', '/api/mmpay/', 'G', 'mmStat4adm', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(151, 0, 'VRA카탈로그관리', 'VRA카탈로그관리', 'N', '/clovirsm/admin/vraMng/index.jsp', '2016-01-29 16:37:44', '3', '0', '0:0:0:0:0:0:0:1', '2020-03-05 16:38:52', '10', '0', '10.11.10.85', '/api/vra_catalog_mng/', 'G', 'vracatalogMng', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(152, 0, 'VRA카탈로그요청', 'VRA카탈로그목록', 'N', '/clovirsm/resources/vra/index.jsp', '2016-01-29 16:37:44', '3', '0', '0:0:0:0:0:0:0:1', '2020-03-05 16:37:29', '10', '0', '10.11.10.85', '/api/vra_catalog/', 'G', 'vracatalog', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(153, 0, 'VRA카탈로그 생성요청', 'VRA카탈로그요청', 'N', '/clovirsm/resources/vra/newVra/index.jsp', '2016-01-29 16:37:44', '3', '0', '0:0:0:0:0:0:0:1', '2020-03-05 16:37:37', '10', '0', '10.11.10.85', '/api/vra_catalog/', 'G', 'vracatalog', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(154, 0, '라이센스조회', '라이센스조회', 'Y', '/clovirsm/admin/license/index.jsp', '2016-01-29 16:37:44', '3', '0', '0:0:0:0:0:0:0:1', '2019-01-22 17:46:50', '1', '0', '0:0:0:0:0:0:0:1', '/api/license/', 'G', 'license', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(155, 0, '디스크 종류 관리', '디스크 타입관리', 'N', '/clovirsm/admin/diskTypeMng/index.jsp', '2016-01-29 16:37:44', '3', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:39:17', '1', '0', '0:0:0:0:0:0:0:1', '/api/disk_type_mng/', 'G', 'diskTypeMng', 17, 'N', 'N', 'N', NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(156, 0, 'IP 대역 관리', NULL, 'N', '/clovirsm/admin/portgroup/index.jsp', '2019-04-02 11:23:40', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-27 14:36:27', '1', '0', '0:0:0:0:0:0:0:1', '/api/portgroup/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(157, 0, '사양 제한 설정', NULL, 'Y', '/clovirsm/admin/specSetting/index.jsp', '2019-04-09 10:41:01', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:43:51', '10', '0', '0:0:0:0:0:0:0:1', '/api/spec_setting/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(158, 0, 'IP현황', NULL, 'Y', '/clovirsm/admin/ipList/index.jsp', '2019-04-15 16:18:06', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:38:46', '10', '0', '0:0:0:0:0:0:0:1', '/api/ip/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(159, 0, 'IP예약', NULL, 'Y', '/clovirsm/admin/ipReserve/index.jsp', '2019-04-15 16:19:48', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:46:10', NULL, NULL, NULL, '/api/ip/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(160, 0, '환경설정', NULL, 'Y', '/clovirsm/admin/setting/index.jsp', '2019-04-30 10:47:01', '1', '0', '0:0:0:0:0:0:0:1', '2019-05-08 16:46:05', NULL, NULL, NULL, '/api/server_setting/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(161, 0, 'VM현황', NULL, 'Y', '/clovirsm/stat/hi/vm.jsp', '2019-05-08 14:34:34', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:36:11', '10', '0', '0:0:0:0:0:0:0:1', '/clovirsm/stat/hi/vm.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(162, 0, '부서별 세팅', NULL, 'Y', '/clovirsm/admin/teamConf/index.jsp', '2019-05-20 16:12:06', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:01', '10', '0', '0:0:0:0:0:0:0:1', '/api/team_conf/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(163, 0, 'NAS 경로 관리', NULL, 'Y', '/clovirsm/admin/nasMng/index.jsp', '2019-05-22 14:54:22', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:11', '10', '0', '0:0:0:0:0:0:0:1', '/api/nas_mng/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(164, 0, '서버 신청', NULL, 'Y', '/clovirsm/resources/newVm/index.jsp', '2019-05-27 14:27:34', '1', '0', '0:0:0:0:0:0:0:1', '2020-12-10 10:52:24', '1', '0', '0:0:0:0:0:0:0:1', '/api/vmReq/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(165, 0, 'IP-네트웍 맵핑', NULL, 'Y', '/clovirsm/admin/portgroupAll/index.jsp', '2019-07-12 12:38:11', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:19', '10', '0', '0:0:0:0:0:0:0:1', '/api/portgroup/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(166, 0, '라이센스현황', NULL, 'Y', '/clovirsm/stat/hi/license.jsp', '2019-05-09 10:26:39', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:36:07', '10', '0', '0:0:0:0:0:0:0:1', '/clovirsm/stat/hi/license.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(167, 0, '데이터센터현황', NULL, 'Y', '/clovirsm/stat/hi/dc.jsp', '2019-05-09 10:27:03', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:36:17', '10', '0', '0:0:0:0:0:0:0:1', '/clovirsm/stat/hi/dc.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(168, 0, '결재현황', NULL, 'Y', '/clovirsm/stat/hi/approve.jsp', '2019-05-09 10:27:23', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:37:37', '10', '0', '0:0:0:0:0:0:0:1', '/clovirsm/stat/hi/approve.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(169, 0, '템플릿 관리', NULL, 'Y', '/clovircm/admin/tmpl/index.jsp', '2019-07-18 17:34:40', '10', '0', '0:0:0:0:0:0:0:1', '2019-07-19 09:42:25', '10', '0', '0:0:0:0:0:0:0:1', '/api/cm/tmpl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(170, 0, '프로젝트 관리', NULL, 'Y', '/clovircm/admin/prj/index.jsp', '2019-07-19 09:43:19', '10', '0', '0:0:0:0:0:0:0:1', '2019-07-19 09:43:19', NULL, NULL, NULL, '/api/cm/prj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(171, 0, '프로젝트 사용자 관리', NULL, 'Y', '/clovircm/prjMng/index.jsp', '2019-07-19 09:44:03', '10', '0', '0:0:0:0:0:0:0:1', '2019-07-19 09:44:03', NULL, NULL, NULL, '/api/cm/prjUser', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(172, 0, 'Node 모니터링', NULL, 'Y', '/clovircm/monitor/node/index.jsp', '2019-07-24 16:17:26', '10', '0', '0:0:0:0:0:0:0:1', '2019-08-19 10:28:06', '10', '0', '0:0:0:0:0:0:0:1', '/api/cm/node/list', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(173, 0, '컨테이너 현황', NULL, 'Y', '/clovircm/app/index.jsp', '2019-07-24 18:02:03', '10', '0', '0:0:0:0:0:0:0:1', '2019-08-02 09:18:50', '10', '0', '0:0:0:0:0:0:0:1', '/api/cm/container', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(174, 0, '클러스터 관리', NULL, 'Y', '/clovircm/admin/cluster/index.jsp', '2019-07-25 14:24:52', '1', '0', '0:0:0:0:0:0:0:1', '2019-07-25 14:26:40', '10', '0', '0:0:0:0:0:0:0:1', '/api/cm/cluster', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(175, 0, '오버사이즈 서버', NULL, 'Y', '/clovirsm/admin/oversizevm/index.jsp', '2019-08-02 10:49:08', '10', '0', '0:0:0:0:0:0:0:1', '2019-08-02 10:49:08', NULL, NULL, NULL, '/api/not_use_vm/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(176, 0, 'Pod 모니터링', NULL, 'Y', '/clovircm/monitor/pod/index.jsp', '2019-08-07 14:55:08', '10', '0', '10.10.1.134', '2019-08-19 10:33:35', '10', '0', '0:0:0:0:0:0:0:1', '/api/cm/pod/list', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(177, 0, '신규요청', NULL, 'Y', '/clovircm/newApp//index.jsp', '2019-08-19 10:29:01', '10', '0', '0:0:0:0:0:0:0:1', '2019-08-19 10:29:20', '10', '0', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(178, 0, '스토리지 관리', NULL, 'Y', '/clovircm/admin/storage/index.jsp', '2019-08-27 11:18:45', '10', '0', '0:0:0:0:0:0:0:1', '2019-08-27 15:11:48', '10', '0', '0:0:0:0:0:0:0:1', '/api/cm/appvolume/list/list_CM_APP_STORAGE/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(179, 0, 'AI모델관리', NULL, 'Y', '/clovircm/aimodel/index.jsp', '2019-08-27 15:11:34', '10', '0', '0:0:0:0:0:0:0:1', '2019-08-27 15:11:34', NULL, NULL, NULL, '/api/cm/app/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(180, 0, 'AI모델훈련관리', NULL, 'Y', '/clovircm/aimodel/training/index.jsp', '2019-08-27 15:11:34', '10', '0', '0:0:0:0:0:0:0:1', '2019-08-27 15:11:34', NULL, NULL, NULL, '/api/cm/aimodel_training/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(181, 0, 'APP 관리', NULL, 'N', '/clovirsm/admin/vra/appMng/index.jsp', '2019-11-01 17:55:57', '1', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:27', '10', '0', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(182, 0, 'VM명 Prefix', NULL, 'N', '/clovirsm/admin/bulkImport/index.jsp', '2019-11-05 15:02:20', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:34', '10', '0', '0:0:0:0:0:0:0:1', '/api/bulkImport/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(183, 0, '사용자 코드관리', NULL, 'Y', '/admin/ddicUser/index.jsp', '2019-11-07 17:44:47', '10', '0', '0:0:0:0:0:0:0:1', '2019-11-07 17:44:47', NULL, NULL, NULL, '/api/ddicUser', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(184, 0, '씬 클라이언트 관리', NULL, 'Y', '/clovirsm/admin/thinClnt/index.jsp', '2019-11-07 17:44:47', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 15:39:41', '10', '0', '0:0:0:0:0:0:0:1', '/api/thinClnt/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(185, 0, '신청 이력', NULL, 'Y', '/clovirsm/workflow/requestItemsHistory/index.jsp', '2020-12-10 08:53:55', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 11:02:31', '10', '0', '0:0:0:0:0:0:0:1', '/api/vm/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(186, 0, '복원 요청', NULL, 'Y', '/clovirsm/resources/recovery/index.jsp', '2020-12-10 09:02:45', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 09:02:45', NULL, NULL, NULL, '/api/vm/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(187, 0, 'OS 변경 요청', NULL, 'N', '/clovirsm/resources/osChange/index.jsp', '2020-12-10 09:04:58', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:52:10', '10', '0', '0:0:0:0:0:0:0:1', '/api/vm/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(188, 0, '신규 요청', NULL, 'Y', '/clovirsm/resources/vm/newVm/index.jsp', '2020-12-10 10:52:35', '1', '0', '0:0:0:0:0:0:0:1', '2020-12-10 10:52:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(189, 0, '기간연장 요청', NULL, 'Y', '/clovirsm/resources/extendPeriod/index.jsp', '2020-12-10 11:01:49', '10', '0', '0:0:0:0:0:0:0:1', '2020-12-10 11:01:49', NULL, NULL, NULL, '/api/vm/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(190, 0, '신규 신청', NULL, 'Y', '/clovirsm/resources/newVm/mobis/index.jsp', '2021-03-29 08:57:11', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-03 11:01:43', '10', '0', '0:0:0:0:0:0:0:1', '/api/vmReq/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(191, 0, 'Q&A(prev)', NULL, 'Y', '/admin/inquiryPrev/index.jsp', '2021-03-30 14:05:02', '10', '0', '0:0:0:0:0:0:0:1', '2021-03-30 14:05:11', '10', '0', '0:0:0:0:0:0:0:1', '/api/qna/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(192, 0, '자원 변경 요청', NULL, 'Y', '/clovirsm/resources/vm/changeSpec/index.jsp', '2021-08-10 15:54:19', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:54:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(193, 0, '삭제 요청', NULL, 'Y', '/clovirsm/resources/vm/delVm/index.jsp', '2021-08-10 15:54:33', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-10 15:54:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(194, 0, '사용 기간 설정', NULL, 'Y', '/clovirsm/admin/monthSetting/index.jsp', '2021-08-11 15:35:24', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-18 15:22:09', '10', '0', '0:0:0:0:0:0:0:1', '/api/spec_setting/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(195, 0, '비용현황', NULL, 'Y', '/clovirsm/stat/hi/mm_fee.jsp', '2021-08-11 21:38:35', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:38:35', NULL, NULL, NULL, '/clovirsm/stat/hi/mm_fee.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(196, 0, '비용추이', NULL, 'Y', '/clovirsm/stat/hi/mm_fee_transition.jsp', '2021-08-11 21:39:12', '10', '0', '0:0:0:0:0:0:0:1', '2021-08-11 21:39:12', NULL, NULL, NULL, '/clovirsm/stat/hi/mm_fee_transition.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


INSERT INTO fm_role
(ROLE_ID, ROLE_NM, ROLE_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('1', 'ROLE_SUPER_ADMIN', '시스템 관리자', '2015-06-18 13:38:59', NULL, NULL, NULL, '2020-06-03 09:16:01', '43', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_role
(ROLE_ID, ROLE_NM, ROLE_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('15', 'ROLE_ANONYMOUS', '익명 사용자(게스트)', '2015-07-23 14:56:38', NULL, NULL, NULL, '2019-04-15 11:30:39', '1', 'fliconz', '0:0:0:0:0:0:0:1');
INSERT INTO fm_role
(ROLE_ID, ROLE_NM, ROLE_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('18', 'ROLE_GRP_MEMBER', '그룹사용자', '2015-12-18 11:42:20', '43', 'fliconz', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_role
(ROLE_ID, ROLE_NM, ROLE_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('19', 'ROLE_PORTAL_MANAGER', '포탈 관리자', '2016-01-07 10:12:11', '43', 'fliconz', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_role
(ROLE_ID, ROLE_NM, ROLE_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('21', 'ROLE_GRP_MANAGER', '그룹 관리자', '2016-01-08 17:02:50', '43', 'fliconz', '0:0:0:0:0:0:0:1', '2021-04-02 14:54:32', '43', '0', '0:0:0:0:0:0:0:1');
INSERT INTO fm_role
(ROLE_ID, ROLE_NM, ROLE_DESC, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('22', 'ROLE_APPROVER', '중간 관리자', '2019-12-06 00:04:12', NULL, NULL, NULL, '2019-12-06 00:04:12', NULL, NULL, NULL);

INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(46, '18', 'Y', 'N', 'N', 'N', 'N', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(88, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(94, '18', 'Y', 'Y', 'Y', 'Y', 'N', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(95, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(112, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(118, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(122, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(125, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(127, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(139, '18', 'Y', 'N', 'N', 'Y', 'N', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(142, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(143, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(152, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(153, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(164, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(185, '18', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(188, '18', 'Y', 'Y', 'Y', 'Y', 'N', '2021-03-25 12:26:49', '43', '0', '0:0:0:0:0:0:0:1', '2021-03-25 12:26:49', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(37, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(42, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(43, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(46, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(88, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(92, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(93, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(94, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(95, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(101, '19', 'Y', 'Y', 'N', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(102, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(103, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(104, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(106, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(111, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(112, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(118, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(119, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(122, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(123, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(125, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(127, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(133, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(139, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(141, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(142, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(143, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(146, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(148, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(149, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(151, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(152, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(153, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(154, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(157, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(158, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(159, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(160, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(161, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(162, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(163, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(164, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(165, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(166, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(167, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(168, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(169, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(170, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(171, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(172, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(173, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(174, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(175, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(176, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(177, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(178, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(179, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:21', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:21', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(184, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(186, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(188, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(189, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(190, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-08-12 10:16:22', '43', '0', '0:0:0:0:0:0:0:1', '2021-08-12 10:16:22', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(93, '21', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-04-02 14:54:32', '43', '0', '0:0:0:0:0:0:0:1', '2021-04-02 14:54:32', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(133, '21', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-04-02 14:54:32', '43', '0', '0:0:0:0:0:0:0:1', '2021-04-02 14:54:32', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(190, '21', 'Y', 'Y', 'Y', 'Y', 'Y', '2021-04-02 14:54:32', '43', '0', '0:0:0:0:0:0:0:1', '2021-04-02 14:54:32', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(46, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(88, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(93, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(94, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(111, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(122, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(127, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(139, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(143, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);
INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(164, '22', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-16 15:49:04', '43', '8', '0:0:0:0:0:0:0:1', '2019-07-16 15:49:05', NULL, NULL, NULL);



