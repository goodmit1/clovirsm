package com.clovirsm.hv.cloud.aws.impl;

import java.util.Arrays;

public enum InstanceLinuxUserName {
	AMAZON("Amazon", "ec2-user"),
	CENTOS("CentOS", "centos"),
	DEBIAN("Debian", "admin"),
	FEDORA("Fedora", "ec2-user"),
	RHEL("RHEL", "ec2-user"),
	SUSE("SUSE", "ec2-user"),
	UBUNTU("Ubuntu", "ubuntu");
	
	private String os;
	private String name;
	
	InstanceLinuxUserName(String os, String name){
		this.os = os;
		this.name = name;
	}
	
	
	public static InstanceLinuxUserName converterOsToName(String os) {
		return Arrays.asList(InstanceLinuxUserName.values())
				.stream()
				.filter(p -> os.toLowerCase().indexOf(p.getOs().toLowerCase()) != -1)
				.findFirst()
				.get();
	}


	public String getOs() {
		return os;
	}

	public String getName() {
		return name;
	}

	
}
