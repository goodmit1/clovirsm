package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.cloudformation.model.DescribeStacksRequest;
import software.amazon.awssdk.services.cloudformation.model.DescribeStacksResponse;
import software.amazon.awssdk.services.ec2.model.Instance;

import java.util.Map;

public class AwsChkTask extends AwsCommon {
    public AwsChkTask(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }

    public Logger logger = LoggerFactory.getLogger(this.getClass());
    private Instance instance;



    protected String getStacksStatus(  String stackName) throws Exception {

            DescribeStacksRequest req = DescribeStacksRequest.builder().stackName(stackName).build();
            DescribeStacksResponse res = super.getAwsConnection().getCloudFormationClient().describeStacks(req);
            String status =  res.stacks().get(0).stackStatusAsString();
            if(status != null && !isProgress(status)) {
                if (!status.equals("CREATE_COMPLETE")) {
                    String msg = res.stacks().get(0).stackStatusReason();
                    if(msg == null){
                        msg = status;
                    }
                    throw new Exception(msg);
                }
            }
            return status;

    }
    protected String getStacksStatusCd(  String stackName) throws Exception {

        DescribeStacksRequest req = DescribeStacksRequest.builder().stackName(stackName).build();
        DescribeStacksResponse res = super.getAwsConnection().getCloudFormationClient().describeStacks(req);
        String status = res.stacks().get(0).stackStatusAsString();
        return status;
    }
    protected boolean isProgress(String status){
        return status.indexOf("IN_PROGRESS")>0;
    }


    public boolean run(String taskId) throws Exception {
        String status = getStacksStatus(taskId);


        if (status == null || this.isProgress(status)) {
            return false;
        } else {
           return true;
        }

    }
}
