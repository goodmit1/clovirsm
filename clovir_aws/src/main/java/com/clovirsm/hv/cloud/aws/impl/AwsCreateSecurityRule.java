package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.*;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AwsCreateSecurityRule extends AwsCommon {
	public AwsCreateSecurityRule(IConnection conn) {
		super(conn);

	}


	
	@Override
	public String run1(HVParam param, Map result) throws Exception {
		doAction(param);
		return null;
	}
	protected void doAction(HVParam param) throws Exception {
		String securityGroup = param.getVmInfo().getVM_NM();
		SecurityGroup sg = getSecurityGroup(securityGroup);


		if(sg == null) {
			CreateSecurityGroupRequest createSecurityGroupRequest = CreateSecurityGroupRequest.builder().groupName(securityGroup)
					.description("").build();
			CreateSecurityGroupResponse resultSecurity = client.createSecurityGroup(createSecurityGroupRequest);
			sg = getSecurityGroup(securityGroup);
		}
		addFwRule(sg, param.getFwInfo().getProtocol(), param.getFwInfo().getPorts(), param.getFwInfo().getIps());


	}

	private void addFwRule(SecurityGroup sg, String protocol, String[] ports, String[] ips) {


		AuthorizeSecurityGroupIngressRequest req = createIpPermission(sg, protocol, ports, ips);
		if(req != null) {
			client.authorizeSecurityGroupIngress(req);
		}

	}

	protected SecurityGroup getSecurityGroup(String securityGroup) {
		DescribeSecurityGroupsRequest req = DescribeSecurityGroupsRequest.builder().filters(Filter.builder().name("tag:Name").values(securityGroup).build()).build();
		List<SecurityGroup> groups = client.describeSecurityGroups(req).securityGroups();
		if(groups.size()==0){
			return null;
		}
		else{
			return groups.get(0);
		}
	}
	private AuthorizeSecurityGroupIngressRequest createIpPermission(SecurityGroup sg, String protocol, String[] ports, String[]  ips) {
		List<IpPermission> ipPermissions = new ArrayList<>();
		for(String port:ports) {
			addIpPermission(sg, ipPermissions, protocol, CommonUtil.getInt(port), ips);
		}
		if(ipPermissions.size()>0) {
			AuthorizeSecurityGroupIngressRequest.Builder authorizeSecurityGroupIngressRequestBuilder = AuthorizeSecurityGroupIngressRequest.builder()
					.groupName(sg.groupName()).ipPermissions(ipPermissions);

			return authorizeSecurityGroupIngressRequestBuilder.build();
		}
		else{
			return null;
		}
	}
	private void addIpPermission(SecurityGroup sg, List<IpPermission> ipPermissions, String protocol, int port, String[]  ips) {
		  // Allow HTTP and SSH traffic
		List<IpPermission> oldPermissionList = sg.ipPermissions();


		boolean isFind = false;
		if(oldPermissionList != null) {
			for (IpPermission oldIpP : oldPermissionList) {
				if (oldIpP.toPort() == port) {

					IpPermission newIpP = createIpPermission(protocol, port, ips,  oldIpP.ipRanges());

					if(newIpP != null) {
						ipPermissions.add(newIpP);
					}
					isFind = true;
					break;
				}
			}
		}
		if(!isFind){
			IpPermission ipP = createIpPermission(protocol, port, ips, null);
			if(ipP != null){
				ipPermissions.add(ipP);
			}
		}



	}



	private IpPermission createIpPermission(String protocol,int port,String[] ips, List<IpRange> oldIps ) {

		List<IpRange> ipRange = new ArrayList<>();
		for(String ip:ips){

			if(oldIps == null || !exist(oldIps, ip)) {
				ipRange.add(newIpRange(ip));
			}
		}
		if(ipRange.size()==0) {
			return null;

		}
		IpPermission ipPermission1 =   IpPermission.builder().ipRanges( ipRange  )
		          .ipProtocol(protocol)
		          .fromPort(port)
		          .toPort(port).build();
		 
		 return ipPermission1;
		
	}
	private IpRange newIpRange(String ip){
		return IpRange.builder().cidrIp(PublicHVAction.getCIDR(ip)).build();
	}
	private boolean exist(List<IpRange> ipRanges, String ip) {
		if(ipRanges.stream().filter(i -> { return   isInRange(i.cidrIp(),ip); } ).count()==0){
			return false;
		}
		else {
			return true;
		}
	}

}
