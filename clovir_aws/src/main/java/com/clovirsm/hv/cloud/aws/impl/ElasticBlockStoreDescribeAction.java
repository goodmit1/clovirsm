package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.DescribeVolumesRequest;
import software.amazon.awssdk.services.ec2.model.Volume;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ElasticBlockStoreDescribeAction extends AwsCommon {
    List<Volume> volumes = new ArrayList();

    public ElasticBlockStoreDescribeAction(IConnection conn) {
        super(conn);

    }

    @Override
    public String run1(HVParam  param, Map result) throws Exception {

        DescribeVolumesRequest describeVolumesRequest = DescribeVolumesRequest.builder().volumeIds(param.getDiskInfo().getDISK_PATH()).build();


        volumes = client.describeVolumes().volumes()
                .stream()
                .filter(p -> p.volumeId().equals(param.getDiskInfo().getDISK_PATH()))
                .collect(Collectors.toList());
        return null;
    }

    protected  Volume getVolumun(String id){
        DescribeVolumesRequest describeVolumesRequest = DescribeVolumesRequest.builder().volumeIds(id).build();
        return  client.describeVolumes(describeVolumesRequest).volumes().get(0);


    }
    private boolean isAttatch(Volume vol) {
        return vol.attachments().isEmpty();
    }

    public boolean isVolume() {
        List<String> volume = new ArrayList<>();

        for (Volume vol : volumes) {
            if (isAttatch(vol)) {
                volume.add(vol.stateAsString());
            } else {
                throw new IllegalArgumentException("DISK를 찾을 수 없습니다.");
            }
        }

        return volume.stream()
                .filter(state -> state.equals("available"))
                .count() != 0;
    }

    public Map getVolume() {
        Map volumeInfo = new HashMap();
        volumeInfo.put("DISK_SIZE", volumes.get(0).size());
        volumeInfo.put("DISK_STATE", volumes.get(0).stateAsString());
        volumeInfo.put("DISK_PATH", volumes.get(0).volumeId());
        return volumeInfo;
    }

}
