package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.AttachVolumeRequest;
import software.amazon.awssdk.services.ec2.model.AttachVolumeResponse;

import java.util.Map;

public class AwsAttachVolume extends AwsCommon {

	public AwsAttachVolume(IConnection conn) {
		super(conn);

	}

	@Override
	public String run1(HVParam param, Map result) throws Exception {

		return null;
	}

	public   String nextMount(String lastDeviceName){
		// /dev/sdf

		return lastDeviceName.substring(0, lastDeviceName.length()-1) + String.valueOf(  (char)(lastDeviceName.charAt(lastDeviceName.length()-1)+1));
	}
	public void run(String vmId, DiskInfo diskInfo){
		AttachVolumeRequest attachRequest =  AttachVolumeRequest.builder().volumeId(diskInfo.getDISK_PATH()).instanceId(vmId).device(diskInfo.getDISK_NM()).build();
		AttachVolumeResponse attachVolumeResult = client.attachVolume(attachRequest);

	}
}
