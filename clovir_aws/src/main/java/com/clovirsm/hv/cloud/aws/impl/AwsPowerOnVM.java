package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest;
import software.amazon.awssdk.services.ec2.model.StartInstancesRequest;

import java.util.Map;

public class AwsPowerOnVM extends AwsCommon {
	public AwsPowerOnVM(IConnection conn) {
		super(conn);
	}

	public Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public String run1(HVParam param, Map result) throws Exception {
		run(  param.getVmInfo(), false);

		return null;
	}
	public void run(VMInfo vm, boolean isWait) throws Exception{
		StartInstancesRequest startInstancesRequest = StartInstancesRequest.builder()
				.instanceIds(vm.getVM_HV_ID()).build();

		client.startInstances(startInstancesRequest);
		while(true) {
			Thread.sleep(500);

			if (client.describeInstances(
					DescribeInstancesRequest.builder().instanceIds(vm.getVM_HV_ID()).build()
			).reservations().get(0).instances().get(0).state().code() == 16) {
				break;
			}
		}
	}
}
