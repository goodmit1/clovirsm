package com.clovirsm.hv.cloud.aws;

import com.clovirsm.hv.*;
import com.clovirsm.hv.cloud.aws.impl.*;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;

import java.util.List;
import java.util.Map;

public class AwsAPI extends PublicCloudAPI implements HypervisorAPI{


	public static String getHVType() {

		return "A";
	}

	protected  String getConnectionName() {
		return AwsConnection.class.getName();
	}




	@Override
	public Map deleteDisk(HVParam param) throws Exception {
		// TODO Auto-generated method stub
		return null;
		//return this.doProcess(null, param, InstancesInfoAction.class, DetachVolumeAtcion.class ,ElasticBlockStoreDeleteAction.class);
	}

	@Override
	public void onFinishCreateVM(HVParam param) throws Exception {

	}

	@Override
	public Map renameSnapshot(HVParam param, String oldName, String newName) throws Exception {
		return null;
	}

	@Override
	public Map revertVM(HVParam param, String snapshot_nm) throws Exception {
		return null;
	}



	@Override
	public Map mountDisk(HVParam param) throws Exception {


		return this.doProcess(param,  AwsAttachVolume.class);
	}

	@Override
	public Map createVM(HVParam param, boolean isRedeploy) throws Exception {
		if(isRedeploy){
			return this.doProcess(param,   AwsReCreateVM.class);
		}
		else{
			return this.doProcess(param,   AwsCreateVM.class);
		}


	}

	@Override
	public Map moveVMFolder(HVParam param) throws Exception {
		return null;
	}


	@Override
	public Map reconfigVM(HVParam param) throws Exception {

		return this.doProcess(param, AwsReconfigVM.class);
	}



	@Override
	public Map powerOpVM(HVParam param, String op) throws Exception {
		Class cls = null;
		if(op.equals(POWER_ON))
		{
			cls = AwsPowerOnVM.class;
		}
		else if(op.equals(POWER_OFF))
		{
			cls = AwsPowerOffVM.class;
		}
		else if(op.equals(POWER_REBOOT))
		{
			cls = AwsRebootVM.class;
		}

		return doProcess( param, cls);
	}

	@Override
	public Map deleteSnapshot(HVParam param, String snapshot_nm) throws Exception {
		return null;
	}

	@Override
	public void deleteImage(HVParam param) throws Exception {

	}


	@Override
	public Map deleteVM(HVParam param) throws Exception {

		return this.doProcess(param,   AwsDeleteVM.class );
	}

	@Override
	public Map openConsole(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map createSnapshot(HVParam param, String snapshotNm) throws Exception {
		return null;
	}

	@Override
	public void renameVM(HVParam param, String newName) throws Exception {

	}

	@Override
	public void renameImg(HVParam param, String newName) throws Exception {

	}



	@Override
	public Map unmount(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMPerf(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getPerf(String type, HVParam param) throws Exception {
		return null;
	}


	@Override
	public Map addNic(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map deleteNic(HVParam param, int nicId) throws Exception {
		return null;
	}

	@Override
	public int getFirstNicId() throws Exception {
		return 0;
	}

	@Override
	public Map listPerfHistory(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map listEvent(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map listAlarm(HVParam param) throws Exception {
		return null;
	}


	@Override
	public List listObject(HVParam param) throws Exception {
		Map result =  this.doProcess(param, AwsListObject.class);
		return (List)result.get(HypervisorAPI.PARAM_LIST);
	}

	@Override
	public String getHVObjType(String standardType) {
		return null;
	}

	@Override
	public String getStandardObjType(String objType) {
		return null;
	}



	@Override
	public Map getDSAttribute(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getHostAttribute(HVParam param) throws Exception {
		return null;
	}

	@Override
	public String[] getPerfHistoryTypes(String objType) throws Exception {
		return new String[0];
	}


	@Override
	public VMInfo chkTemplatePath(HVParam param, String tmplPath) throws Exception {
		IConnection connectionMgr = null;
		try {
			connectionMgr = connect(param.getConnParam());
			AwsChkTemplate action = new AwsChkTemplate(connectionMgr);
			return action.run(tmplPath);

		}
		finally
		{
			disconnect(connectionMgr);
		}

	}


	@Override
	public List<Map> listVMDisk(HVParam param) throws Exception {
		// TODO Auto-generated method stub
		Map result = doProcess(param, AwsGetVM.class);
		return (List) result.get("DISK_LIST");
	}



	@Override
	public Map createImage(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Float getSnapshotSize(HVParam param, String snapshotName) throws Exception {
		return null;
	}

	@Override
	public Map addDisk(HVParam param ) throws Exception {

		return this.doProcess(param, AwsCreateDisk.class);
	}



	@Override
	public VMInfo vmState(HVParam param) throws Exception {

		IConnection connectionMgr = null;
		try {
			connectionMgr = connect(param.getConnParam());
			AwsGetVM getVM = new AwsGetVM(connectionMgr);
			VMInfo result = new VMInfo();
			getVM.run1(param  , result);
			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}

	}

	@Override
	public Map getVMPerfListInDC(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMListInDC(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map listAllAlarm(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map guestRun(HVParam param, String cmd, String param2) throws Exception {
		return null;
	}

	@Override
	public void guestUpload(HVParam param, String svrPath, String localPath) throws Exception {

	}

	@Override
	public void guestDownload(HVParam param, String svrPath, String localPath) throws Exception {

	}

	@Override
	public Map listLicense(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMIpListInDC(HVParam param) throws Exception {
		return null;
	}

	@Override
	public void connectTest(Map param) throws Exception {

	}




	@Override
	public boolean chkTask(HVParam param, String taskId) throws Exception {
		IConnection connectionMgr = null;
		try {
			connectionMgr = connect(param.getConnParam());
			AwsChkTask action = new AwsChkTask(connectionMgr);
			return action.run(taskId);
		}
		finally
		{
			disconnect(connectionMgr);
		}

	}

	@Override
	public Map listAllVMMaxPerf(HVParam param) throws Exception {
		return null;
	}

	@Override
	public Map getVMTags(HVParam param) throws Exception {
		return null;
	}


	@Override
	public Map createFw(HVParam param) throws Exception {

		return this.doProcess(  param, AwsCreateSecurityRule.class);
	}

	@Override
	public Map deleteFwSourceIp(HVParam param) throws Exception {
		return this.doProcess(  param, AwsDeleteSecurityRule.class);
	}



}
