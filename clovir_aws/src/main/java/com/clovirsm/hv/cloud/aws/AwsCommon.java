package com.clovirsm.hv.cloud.aws;

import com.clovirsm.hv.*;
import software.amazon.awssdk.services.cloudformation.model.AlreadyExistsException;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest;
import software.amazon.awssdk.services.ec2.model.Filter;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.Reservation;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AwsCommon extends PublicHVAction {



	protected Ec2Client client;
	protected String REGION;
	public AwsCommon(IConnection conn)
	{
		super( conn);
		REGION = conn.getURL();
		if(conn != null) {
			this.client = ((AwsConnection)conn).getClient();
		}
	}

	public AwsConnection getAwsConnection(){
		return (AwsConnection)super.getConnect();
	}
	protected String getConnectionClassName()
	{
		return AwsConnection.class.getName();
	}
	protected String getVMIdByName(String VM_NM)  {
		 try {
			 DescribeInstancesRequest req = DescribeInstancesRequest.builder().filters(Filter.builder().name("tag:Name").values(VM_NM).build()).build();
			 String id = null;
			 List<Reservation> reservations = client.describeInstances(req).reservations();
			 for( Reservation r : reservations){
			 	try {
					id = r.instances().stream().filter(
							i -> i.state().code() != 48
					).findFirst().map(Instance::instanceId).get();
					if (id != null) {
						return id;
					}
				}catch(Exception ignore){

				}
			 } ;

		 }
		 catch(Exception e){
		 	e.printStackTrace();

		 }
		return null;
	}
	protected Exception processErr(Exception e, String obj){
		if(e instanceof AlreadyExistsException){
			return new AlreadyExistException(e.getMessage());
		}

		return e;
	}

}
