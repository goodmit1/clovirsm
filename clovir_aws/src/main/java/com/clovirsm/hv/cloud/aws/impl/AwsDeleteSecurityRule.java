package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.IConnection;
import software.amazon.awssdk.services.ec2.model.*;

public class AwsDeleteSecurityRule extends AwsCreateSecurityRule {
	public AwsDeleteSecurityRule(IConnection conn) {
		super(conn);

	}


	

	protected void doAction(HVParam param) throws Exception {
		String securityGroup = param.getVmInfo().getVM_NM();
		SecurityGroup sg = getSecurityGroup(securityGroup);


		if(sg == null) {
			return;
		}
		deleteFwRule(sg, param.getFwInfo().getProtocol(), param.getFwInfo().getIps(), param.getFwInfo().getPorts());


	}
	protected void deleteFwRule(SecurityGroup sg, String protocol, String[] ips, String[] ports){
		for(String ip:ips) {
			for(String port:ports) {
				RevokeSecurityGroupIngressRequest req = RevokeSecurityGroupIngressRequest.builder()
						.groupId(sg.groupId()).ipProtocol(protocol)
						.toPort(CommonUtil.getInt(port.trim()))
						.fromPort(CommonUtil.getInt(port.trim())).cidrIp(getCIDR(ip)).build();
				client.revokeSecurityGroupIngress(req);
			}
		}
		 
	}

}
