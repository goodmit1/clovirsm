package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.DescribeImagesRequest;
import software.amazon.awssdk.services.ec2.model.Image;

import java.util.List;
import java.util.Map;

public class AwsChkTemplate extends AwsCommon {

	public AwsChkTemplate(IConnection conn) {
	 	super(conn);
	}

	@Override
	public String run1(HVParam param, Map result) throws Exception {

		 return null;
	}


    public VMInfo run(String id) throws NotFoundException {
		DescribeImagesRequest req = DescribeImagesRequest.builder().owners("self").imageIds(id).build();
		List<Image> images = client.describeImages(req).images();
		if(images.size()>0){
			VMInfo result = new VMInfo();

			result.setGUEST_NM( images.get(0).platformDetails());
			return result;
		}
		throw new NotFoundException( "Image " + id + " not found");
    }
}
