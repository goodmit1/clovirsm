package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.DeleteVolumeRequest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ElasticBlockStoreDeleteAction extends AwsCommon {

    public ElasticBlockStoreDeleteAction(IConnection conn) {
        super(conn);
        // TODO Auto-generated constructor stub
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {


        if (param.getVmInfo().getDISK_LIST() != null) {
            delVol(param.getVmInfo() ,   result);
        }


        return null;
    }

    private void delVol(VMInfo vmInfo  , Map result) throws Exception {

        ElasticBlockStoreStateCheckAction checkAction = new ElasticBlockStoreStateCheckAction(getAwsConnection());

        List<String> diskList = vmInfo.getDISK_LIST().stream()
                .map(disk -> disk.getDISK_PATH())
                .map(String::valueOf)
                .collect(Collectors.toList());

        while (true) {
            checkAction.run1(vmInfo );
            if (!checkAction.isVolume()) {
                for (String ebsId : diskList) {
                    DeleteVolumeRequest deleteVolumeRequest = DeleteVolumeRequest.builder().volumeId(ebsId).build();

                    client.deleteVolume(deleteVolumeRequest);
                }
                break;
            }
            Thread.sleep(1000);
        }
    }

}
