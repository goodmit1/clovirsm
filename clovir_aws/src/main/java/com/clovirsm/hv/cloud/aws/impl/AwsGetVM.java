package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.*;
import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import software.amazon.awssdk.services.ec2.model.*;

import java.util.*;
import java.util.stream.Collectors;

public class AwsGetVM extends AwsCommon {
    public AwsGetVM(IConnection conn) {
        super(conn);

    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {

        state( param.getVmInfo(), (VMInfo) result);
        return null;
    }
    protected VMInfo run(VMInfo param) throws NotFoundException {
        VMInfo result = new VMInfo();
        state(param, result);
        return result;
    }
    Instance instance;
    private void state(VMInfo param, VMInfo result) throws NotFoundException {

        if (param.getVM_HV_ID() == null) {
            param.setVM_HV_ID(  super.getVMIdByName(param.getVM_NM()));
        }
        if (param.getVM_HV_ID() == null) {
            throw new NotFoundException("VM: " + param.getVM_NM());
        }
        DescribeInstancesRequest describeInstancesRequest = DescribeInstancesRequest.builder()
                .instanceIds(param.getVM_HV_ID()).build();

        DescribeInstancesResponse response = client.describeInstances(describeInstancesRequest);

        instance = response.reservations()
                .get(0)
                .instances()
                .get(0);

        if (instance != null) {


            List<IPInfo> ipList = new ArrayList<>();
            result.setPUBLIC_IP(instance.publicIpAddress());
            for (InstanceNetworkInterface nic : instance.networkInterfaces()) {
                IPInfo ipInfo = new IPInfo();
                ipInfo.nicId = nic.hashCode();
                ipInfo.ip = nic.privateIpAddress();
                ipInfo.macAddress = nic.macAddress();
                ipList.add(ipInfo);

            }

            InstanceTypeDescribeAction instanceTypeAction = new InstanceTypeDescribeAction(this.getAwsConnection());
            InstanceTypeInfo instanceType = instanceTypeAction.getInstanceType(instance.instanceType());
            result.setRUN_CD(instance.state().code()==16 ? HypervisorAPI.RUN_CD_RUN : HypervisorAPI.RUN_CD_STOP);
            result.setIP_LIST(ipList);
            result.setVM_NM(param.getVM_NM());
            result.setSPEC_TYPE(instance.instanceType().toString());
            result.setCPU(instanceType.vCpuInfo().defaultVCpus());
            result.setRAM_SIZE((int) (instanceType.memoryInfo().sizeInMiB()/1024));
            result.setGUEST_NM(getPlatForm(instance));
            result.setPUBLIC_IP(instance.publicDnsName());
            int diskSize = setDiskList(result, instance);
            result.setDISK_SIZE(diskSize);
            result.setFROM(instance.imageId());
            result.setVM_HV_ID(instance.instanceId());
        }
        else{
            throw new NotFoundException("VM: " + param.getVM_NM());
        }
    }
    protected String getPlatForm(Instance instance){
        String result = instance.platformAsString();
        if(result == null){
            DescribeImagesRequest req = DescribeImagesRequest.builder().owners("self").imageIds(instance.imageId()).build();
            Image img = client.describeImages(req).images().get(0);
            return img.platformDetails();
        }
        return result.equals("windows") ? "Windows" : result;
    }
    protected  int setDiskList(VMInfo result, Instance instance ){
        List<InstanceBlockDeviceMapping> ebsList =  instance.blockDeviceMappings().stream()
                .filter(ebs -> !ebs.deviceName().equals("/dev/xvda"))
                .collect(Collectors.toList()) ;
        List<DiskInfo> diskList = new ArrayList();
        ElasticBlockStoreDescribeAction blockStoreDescribeAction = new ElasticBlockStoreDescribeAction(getAwsConnection());
        int total = 0;
        for (InstanceBlockDeviceMapping ebs : ebsList) {


            DiskInfo disk = new DiskInfo();
            disk.setDISK_NM( ebs.deviceName());

            disk.setDISK_PATH( ebs.ebs().volumeId());
            Volume vol = blockStoreDescribeAction.getVolumun(ebs.ebs().volumeId());
            disk.setDS_NM(vol.volumeTypeAsString());
            disk.setDISK_SIZE( vol.size());
            diskList.add(disk);
            total += vol.size() ;
        }
        result.setDISK_LIST(diskList);
        return total;
    }
    protected String getAvailabilityZone() throws Exception {
        if(instance == null){
            throw new Exception("instance is null");
        }
       return instance.placement().availabilityZone();
    }

}
