package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.InstanceBlockDeviceMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ElasticBlockStoreStateCheckAction extends AwsCommon {
	List<InstanceBlockDeviceMapping> ebsList = new ArrayList();
	public ElasticBlockStoreStateCheckAction(IConnection conn) {
		super(conn);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String run1(HVParam param, Map result) throws Exception {
		return null;
	}


	public String run1( VMInfo vmInfo ) throws Exception {
		// TODO Auto-generated method stub

		
	    DescribeInstancesRequest describeInstancesRequest =   DescribeInstancesRequest.builder().instanceIds(vmInfo.getVM_HV_ID()).build();

		DescribeInstancesResponse response = client.describeInstances(describeInstancesRequest);
	        
        Instance instance = response.reservations()
                .get(0)
                .instances()
                .get(0);
       
        Map<String, Object> instancesInfo = new HashMap<>();
      
        if(instance != null) {
    	 
    	ebsList = instance.blockDeviceMappings().stream()
    			.filter(ebs -> !ebs.deviceName().equals("/dev/sda1")).filter(ebs -> !ebs.deviceName().equals("/dev/xvda"))
    	  		.collect(Collectors.toList());
		
        }
		return null;
	}
	
	public boolean isVolume() {
		return ebsList.size() != 0 ; 
	}

}
