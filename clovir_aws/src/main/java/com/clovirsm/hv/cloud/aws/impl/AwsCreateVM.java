package com.clovirsm.hv.cloud.aws.impl;

import com.clovirsm.hv.*;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import software.amazon.awssdk.services.cloudformation.model.CreateStackRequest;
import software.amazon.awssdk.services.cloudformation.model.Parameter;
import software.amazon.awssdk.services.cloudformation.model.UpdateStackRequest;
import software.amazon.awssdk.services.ec2.model.DescribeImagesRequest;
import software.amazon.awssdk.services.ec2.model.Image;

import java.io.InputStream;
import java.util.*;

public class AwsCreateVM extends AwsCommon {
    public AwsCreateVM(IConnection conn) {
        super(conn);
    }

    protected String getWinUserData(VMInfo param){
        String userData = "<powershell>. " + getWinStartScript(param) + " </powershell>";
        return CommonUtil.fillContentByVar(userData, param);
    }
    protected String getLinuxUserData(VMInfo param){
        String userData = "#cloud-config\n" +
                "cloud_final_modules:\n" +
                "- [users-groups,always]\n" +
                "users:\n" +
                "  - name: ${ADMIN_ID}\n" +
                "    groups: [ wheel ]\n" +
                "    sudo: [ \"ALL=(ALL) NOPASSWD:ALL\" ]\n" +
                "    shell: /bin/bash\n" +
                "    ssh-authorized-keys: \n" +
                "    - ${ADMIN_SSH_KEY}";
        return CommonUtil.fillContentByVar(userData, param);
    }
    protected boolean isNew(){
        return true;
    }
    @Override
    public String run1(HVParam paramMap, Map result) throws Exception {
        VMInfo param =   paramMap.getVmInfo();

        super.chkVMSetting(param);
        String name = param.getVM_NM();
        String status =  getPrevStatus( name);
        if(isNew() && status != null){
            throw new AlreadyExistException("VM " + param.getVM_NM());
        }
        InputStream stream = getClass().getClassLoader().getResourceAsStream("awsCreateTemplate.yaml");
        String templateBody = CommonUtil.readStream2String(stream, "utf-8");




        List parameters = new ArrayList();
        parameters.add( Parameter.builder().parameterKey("Name").parameterValue(name).build()) ;
        parameters.add( Parameter.builder().parameterKey("DiskType").parameterValue(param.getDISK_LIST().get(0).getDATASTORE_NM_LIST()[0]).build());
        parameters.add( Parameter.builder().parameterKey("ImageId").parameterValue(param.getFROM()).build()) ;
        parameters.add( Parameter.builder().parameterKey("DiskSize").parameterValue("" + param.getDISK_LIST().get(0).getDISK_SIZE()).build());
        parameters.add( Parameter.builder().parameterKey("InstanceType").parameterValue(param.getSPEC_TYPE()).build()) ;
        parameters.add( Parameter.builder().parameterKey("DefaultIP").parameterValue(PublicHVAction.getCIDR(param.getDEFAULT_IP())).build()) ;
        String userData = null;
        if(isWin(param.getFROM())){
            userData = this.getWinUserData(param);
        }
        else{
            userData = this.getLinuxUserData(param);
        }
        parameters.add( Parameter.builder().parameterKey("UserData").parameterValue(Base64.getEncoder().encodeToString( userData.getBytes())).build());

        if("ROLLBACK_COMPLETE".equals(status)){
            AwsDeleteVM deleteVM = new AwsDeleteVM(this.getConnect());
            deleteVM.run1(paramMap, result);
            waitForDelete(paramMap.getVmInfo().getVM_NM());
            status = null;
        }
        try {
            if (isNew() || status == null) {
                CreateStackRequest req = CreateStackRequest.builder().stackName(name).templateBody(templateBody).parameters(parameters).build();

                super.getAwsConnection().getCloudFormationClient().createStack(req);
            } else {
                UpdateStackRequest req = UpdateStackRequest.builder().stackName(name).templateBody(templateBody).parameters(parameters).build();

                super.getAwsConnection().getCloudFormationClient().updateStack(req);
            }
            return name;
        }catch (Exception e){
            throw processErr(e, "VM " + param.getVM_NM());
        }
    }

    private void waitForDelete(String vm_nm) {
        AwsChkTask chkTask = new AwsChkTask(this.getConnect());
        try {
            while (true) {
                Thread.sleep(500);
                chkTask.getStacksStatus(vm_nm); // 다 지워지면 Exception 발생
            }
        }catch (Exception e){

        }

    }


    protected String getPrevStatus(String name) throws Exception{
        AwsChkTask chkTask = new AwsChkTask(this.getConnect());
        try {
            return chkTask.getStacksStatusCd(name);
        }
        catch(Exception e){
           return null;

        }
    }
    protected boolean isWin(String templateId) throws Exception{
        DescribeImagesRequest req = DescribeImagesRequest.builder().imageIds(templateId).build();
        List<Image> images = client.describeImages(req).images();
        for(Image img : images){
            return "windows".equals(  img.platformAsString());
        }
        throw new NotFoundException("Image " + templateId);
    }
}
