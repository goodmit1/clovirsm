package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.cloud.aws.AwsConnection;
import software.amazon.awssdk.services.ec2.model.DescribeInstanceTypesRequest;
import software.amazon.awssdk.services.ec2.model.DescribeInstanceTypesResponse;
import software.amazon.awssdk.services.ec2.model.InstanceType;
import software.amazon.awssdk.services.ec2.model.InstanceTypeInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InstanceTypeDescribeAction   {
    List<InstanceTypeInfo> list = new ArrayList();
    AwsConnection conn  ;
    public InstanceTypeDescribeAction(AwsConnection conn) {
       this.conn = conn;
    }


    public  List<InstanceTypeInfo> list() throws Exception {

        startDescribeInstanceTypes(null);
        return  list;
    }

    protected InstanceTypeInfo getInstanceType(InstanceType id) {
        DescribeInstanceTypesRequest req  = DescribeInstanceTypesRequest.builder().instanceTypes(id).build();
        return conn.getClient().describeInstanceTypes(req).instanceTypes().get(0);

    }
    private DescribeInstanceTypesResponse startDescribeInstanceTypes(String nextToken) {
        DescribeInstanceTypesRequest.Builder reqBuilder = DescribeInstanceTypesRequest.builder();
        if (nextToken != null) {
            reqBuilder.nextToken(nextToken);
        }

        DescribeInstanceTypesResponse reqR = conn.getClient().describeInstanceTypes(reqBuilder.build());
        list.addAll(reqR.instanceTypes());
        if (reqR.nextToken() != null) {
            DescribeInstanceTypesResponse describeInstanceTypesResult = startDescribeInstanceTypes(reqR.nextToken());
            return describeInstanceTypesResult;
        } else {
            return reqR;
        }


    }


}
