package com.clovirsm.hv.cloud.aws.impl;

import com.clovirsm.hv.IConnection;

public class AwsReCreateVM extends AwsCreateVM{
    public AwsReCreateVM(IConnection conn) {
        super(conn);
    }
    protected boolean isNew(){
        return false;
    }
}
