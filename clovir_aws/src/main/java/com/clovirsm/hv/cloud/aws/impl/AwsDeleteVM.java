package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.cloudformation.model.DeleteStackRequest;
import software.amazon.awssdk.services.cloudformation.model.DeleteStackResponse;

import java.util.Map;

public class AwsDeleteVM extends AwsCommon {
	public AwsDeleteVM(IConnection conn) {
		super(conn);
	}

	public Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public String run1(HVParam  param, Map result) throws Exception {
		// TODO Auto-generated method stub
		try {
			DeleteStackRequest req = DeleteStackRequest.builder().stackName(param.getVmInfo().getVM_NM()).build();
			DeleteStackResponse res = super.getAwsConnection().getCloudFormationClient().deleteStack(req);

		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException(e + " 삭제에 실패했습니다.");
		}
		return null;
	}

}
