package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.ModifyVolumeRequest;

import java.util.Map;

public class AwsVolumeModify extends AwsCommon {

    public AwsVolumeModify(IConnection conn) {
        super(conn);

    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }
    public void run(DiskInfo diskInfo){

        int diskSize = Integer.parseInt(String.valueOf(diskInfo.getDISK_SIZE()));
        try {
            ModifyVolumeRequest modifyVolumeRequest = ModifyVolumeRequest.builder().volumeId(diskInfo.getDISK_PATH()).size(diskSize).build();
            client.modifyVolume(modifyVolumeRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
