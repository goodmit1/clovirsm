package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.*;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.cloudformation.model.*;
import software.amazon.awssdk.services.ec2.model.AttributeValue;
import software.amazon.awssdk.services.ec2.model.ModifyInstanceAttributeRequest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class AwsReconfigVM extends AwsCommon {
    public Logger logger = LoggerFactory.getLogger(this.getClass());

    public AwsReconfigVM(IConnection conn) {
        super(conn);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        this.runInThread(param);
        return "" + System.nanoTime();
    }

    public void runInThread(HVParam param) {
        Thread thread = new Thread(() -> {
            try {
                logger.info("Instance Modify Start");
                ModifyInstanceAttributeRequest.Builder requestBuilder = ModifyInstanceAttributeRequest.builder();

                AwsGetVM getVM = new AwsGetVM(super.getConnect());
                VMInfo oldInfo = getVM.run(param.getVmInfo());

                AwsPowerOffVM powerOff = new AwsPowerOffVM(super.getConnect());
                powerOff.run(oldInfo, true);
                if (!oldInfo.getSPEC_TYPE().equals(param.getVmInfo().getSPEC_TYPE())) {


                    AttributeValue t = AttributeValue.builder().value(param.getVmInfo().getSPEC_TYPE()).build();

                    requestBuilder.instanceId(param.getVmInfo().getVM_HV_ID());
                    requestBuilder.instanceType(t);


                    client.modifyInstanceAttribute(requestBuilder.build());
                }


                createVirtualDisk(oldInfo, param.getVmInfo(), getVM.getAvailabilityZone());
                AwsPowerOnVM powerOn = new AwsPowerOnVM(super.getConnect());
                powerOn.run(oldInfo, true);

                if (param.getAfter() != null && param.getAfter() instanceof IAsyncAfter) {
                    ((IAsyncAfter) param.getAfter()).setDone(true);
                }
                getConnect().disconnect();

            } catch (Exception e) {
                if (param.getAfter() != null && param.getAfter() instanceof IAsyncAfter) {
                    ((IAsyncAfter) param.getAfter()).setError(e);
                }
                e.printStackTrace();


            }
        });
        thread.start();

    }

    private void createVirtualDisk(VMInfo oldVm, VMInfo newVM, String availZone) throws Exception {


        List<DiskInfo> dataDisk = newVM.getDISK_LIST();
        List<DiskInfo> oldDataDisk = oldVm.getDISK_LIST();
        String lastMount = oldDataDisk.stream().map(DiskInfo::getDISK_NM).max(Comparator.naturalOrder()).orElse("/dev/sde");
        for (DiskInfo m : dataDisk) {


            if (m.isNew()) {
                AwsCreateDisk blockStoreAction = new AwsCreateDisk(getAwsConnection());
                String id = blockStoreAction.create(availZone, m, true);
                m.setDISK_PATH(id);
                AwsAttachVolume attachVolume = new AwsAttachVolume(getAwsConnection());
                lastMount = attachVolume.nextMount(lastMount);
                m.setDISK_NM(lastMount);

                attachVolume.run(newVM.getVM_HV_ID(), m);


            } else {

                DiskInfo oldDisk = oldDataDisk.stream()
                        .filter(disk -> disk.getDISK_PATH().equals(m.getDISK_PATH()))
                        .findAny().orElseGet(DiskInfo::new);
                int changeDiskSize = m.getDISK_SIZE();
                int oldDiskSize = oldDisk.getDISK_SIZE();

                if (changeDiskSize > oldDiskSize) {
                    AwsVolumeModify blockStoreModifyAction = new AwsVolumeModify(getAwsConnection());

                    blockStoreModifyAction.run(m);
                } else if (changeDiskSize < oldDiskSize) {
                    throw new Exception("size problem");
                }
            }
        }


    }

    /**
     * vm을 지우고 다시 만듬..
     *
     * @param vm
     * @throws Exception
     */
    public void runByTemplate(VMInfo vm) throws Exception {

        DescribeStacksRequest req = DescribeStacksRequest.builder().stackName(vm.getVM_NM()).build();
        Stack template = super.getAwsConnection().getCloudFormationClient().describeStacks(req).stacks().get(0);
        List<Parameter> parameters = template.parameters();
        parameters = setSpecType(parameters, vm.getSPEC_TYPE());


        GetTemplateRequest treq = GetTemplateRequest.builder().stackName(vm.getVM_NM()).build();
        String templateBody = super.getAwsConnection().getCloudFormationClient().getTemplate(treq).templateBody();
        JSONObject templateJson = YamlUtil.yamlToJson(templateBody);
        JSONArray disk = templateJson.getJSONObject("Resources").getJSONObject("EC2Instance").getJSONObject("Properties").getJSONArray("BlockDeviceMappings");
        createVirtualDisk(disk, vm.getDISK_LIST());
        String chgNm = "c" + System.nanoTime();
        CreateChangeSetRequest updateReq = CreateChangeSetRequest.builder().stackName(vm.getVM_NM()).changeSetName(chgNm).changeSetType(ChangeSetType.UPDATE).parameters(parameters).templateBody(templateJson.toString()).build();
        super.getAwsConnection().getCloudFormationClient().createChangeSet(updateReq);
        DescribeChangeSetResponse chgSet;
        while (true) {
            chgSet = super.getAwsConnection().getCloudFormationClient().describeChangeSet(DescribeChangeSetRequest.builder().stackName(vm.getVM_NM()).changeSetName(chgNm).build());
            if (chgSet.status().equals(ChangeSetStatus.CREATE_COMPLETE) || chgSet.status().equals(ChangeSetStatus.FAILED)) {
                break;
            }
            Thread.sleep(500);
        }
        if (chgSet.status().equals(ChangeSetStatus.FAILED)) {
            throw new Exception(chgSet.statusReason());
        }
        ExecuteChangeSetRequest exeReq = ExecuteChangeSetRequest.builder().stackName(vm.getVM_NM()).changeSetName(chgNm).build();
        super.getAwsConnection().getCloudFormationClient().executeChangeSet(exeReq);

        //UpdateStackRequest updateReq = UpdateStackRequest.builder().stackName(vm.getVM_NM()).parameters(parameters).templateBody(templateJson.toString()).build();
        //super.getAwsConnection().getCloudFormationClient().updateStack(updateReq);
    }

    private void createVirtualDisk(JSONArray disk, List<DiskInfo> dataDisk) throws Exception {
        String lastMount = dataDisk.stream().map(DiskInfo::getDISK_NM).max(Comparator.naturalOrder()).orElse("/dev/sde");

        AwsAttachVolume volume = new AwsAttachVolume(this.getConnect());
        for (DiskInfo m : dataDisk) {


            if (m.isNew()) {
                lastMount = volume.nextMount(lastMount);
                disk.put((new JSONObject()).put("DeviceName", lastMount).put("Ebs", (new JSONObject()).put("VolumeType", m.getDATASTORE_NM_LIST()[0]).put("VolumeSize", m.getDISK_SIZE())));
            } else {

                for (int i = 0; i < disk.length(); i++) {
                    if (disk.getJSONObject(i).getString("DeviceName").equals(m.getDISK_NM())) {
                        disk.getJSONObject(i).getJSONObject("Ebs").put("VolumeSize", m.getDISK_SIZE());
                    }
                }
            }
        }
    }

    private List<Parameter> setSpecType(List<Parameter> parameters, String spec_type) {
        List<Parameter> result = new ArrayList<>();
        result.add(Parameter.builder().parameterKey("InstanceType").parameterValue(spec_type).build());
        for (Parameter p : parameters) {
            if (!p.parameterKey().equals("InstanceType")) {
                result.add(p);
            }
        }
        return result;
    }

}
