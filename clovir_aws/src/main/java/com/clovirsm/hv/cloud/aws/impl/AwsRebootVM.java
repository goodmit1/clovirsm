package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.ec2.model.RebootInstancesRequest;

import java.util.Map;

public class AwsRebootVM extends AwsCommon {
	public AwsRebootVM(IConnection conn) {
		super(conn);
	}

	public Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public String run1(HVParam param, Map result) throws Exception {
		run(  param.getVmInfo());

		return null;
	}
	public void run(VMInfo vm) throws Exception{
        RebootInstancesRequest rebootInstancesRequest = RebootInstancesRequest.builder()
            .instanceIds(vm.getVM_HV_ID()).build();

        client.rebootInstances(rebootInstancesRequest);

	}

}
