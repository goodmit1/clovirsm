package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import org.json.JSONArray;
import org.json.JSONObject;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.model.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AwsListObject extends AwsCommon {


	public AwsListObject(IConnection conn) {
		super(conn);

	}
	@Override
	public String run1(HVParam param, Map result) throws Exception {
		List list = new ArrayList();
		String parent = Region.of(super.getAwsConnection().getURL()).toString();
		add(list, REGION, parent, OBJ_TYPE_REGIONS, null, null);
		addChild(list, parent);
		result.put(HypervisorAPI.PARAM_LIST, list);
		return null;
	}

	private void add(List result,String id, String name, String type, String parent )
	{
		add(result, id, name, type, parent, null);
	}
	private void add(List result,String id, String name, String type, String parent, JSONObject prop)
	{
		Map map = new HashMap();
		map.put("OBJ_ID", id);
		map.put("OBJ_NM", name);
		map.put("OBJ_TYPE_NM", type);
		map.put("PARENT_OBJ_NM", parent);
		if(prop !=null){
			map.put("OBJ_PROP", prop.toString());
		}
		result.add(map);
	}

	private void addChild(List list, String parent) throws Exception
	{
		getInstanceType(list, parent);
		getTemplate(list,parent);
		getDiskType(list, parent);
//		getKeypair(list, parent);
	}
	private void getInstanceType(List list, String parent) throws Exception {
		List<InstanceTypeInfo> typeList = null;
		Map result = new HashMap();
		InstanceTypeDescribeAction action = new InstanceTypeDescribeAction(getAwsConnection());
		typeList = action.list();

		for(InstanceTypeInfo instance : typeList) {
			add(list , instance.instanceType().name(),getInstanceName( instance ) , OBJ_TYPE_INSTANCE_TYPE, parent, getInstanceProp(instance) );
		}
	}

	private String getInstanceName(InstanceTypeInfo machineType){
		return machineType.instanceType().name()  ;
	}

	private JSONObject getInstanceProp(InstanceTypeInfo machineType) {
		JSONObject json = new JSONObject();
		json.put(HypervisorAPI.PARAM_CPU, machineType.vCpuInfo().defaultVCpus());
		json.put(HypervisorAPI.PARAM_MEM,  machineType.memoryInfo().sizeInMiB()/1024)  ;

		return json;
	}
	
	private void getTemplate(List list, String parent){

		DescribeImagesRequest req = DescribeImagesRequest.builder().owners("self").build();
		List<Image> images = client.describeImages(req).images();
		for(Image img : images){
			add(list ,img.imageId(), img.name(), OBJ_TYPE_TEMPLATE, parent);
		}


	}
	private void getDiskType(List list, String parent ) throws IOException {
		InputStream stream = getClass().getClassLoader().getResourceAsStream("awsDiskTypes.json");
		JSONObject json  =  new JSONObject( CommonUtil.readStream2String(stream, "utf-8"));
		JSONArray diskTypes = json.getJSONArray("volumes");
		for(int i=0; i < diskTypes.length(); i++){
			JSONObject diskType = diskTypes.getJSONObject(i);
			add(list ,diskType.getString("volumeType"),diskType.getString("volumeTitle"), OBJ_TYPE_DISK_TYPE, parent, getDiskTypeProp(diskType));
		}

	}

	private JSONObject getDiskTypeProp(JSONObject diskType){
		JSONObject result = new JSONObject();
		if(diskType.has("minVolumeSize")){
			result.put("min", diskType.get("minVolumeSize"));
		}
		if(diskType.has("maxVolumeSize")){
			result.put("max", diskType.get("maxVolumeSize"));
		}
		return result;
	}
	

}
