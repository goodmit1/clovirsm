package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.ec2.model.DeleteSecurityGroupRequest;

import java.util.Map;

public class SecurityGroupDeleteAction extends AwsCommon {
    public Logger logger = LoggerFactory.getLogger(this.getClass());

    public SecurityGroupDeleteAction(IConnection conn) {
        super(conn);
        // TODO Auto-generated constructor stub
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
		doAction(param.getVmInfo().getVM_NM());
        return null;
    }

    public void doAction(String name) throws Exception {
        logger.info("SecurityGroup Delete Start");

        DeleteSecurityGroupRequest.Builder reqBuilder = DeleteSecurityGroupRequest.builder();


        reqBuilder.groupName(name);


        client.deleteSecurityGroup(reqBuilder.build());


        logger.info("SecurityGroup Delete End");

    }

}
