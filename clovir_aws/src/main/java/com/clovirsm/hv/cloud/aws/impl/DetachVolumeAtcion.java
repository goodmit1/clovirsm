package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.DetachVolumeRequest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DetachVolumeAtcion extends AwsCommon {

	public DetachVolumeAtcion(IConnection conn) {
		super(conn);

	}

	@Override
	public String run1(HVParam param, Map result) throws Exception {

		if(param.getVmInfo().getDISK_LIST() != null) {
			detachVol(param.getVmInfo().getDISK_LIST());
		}


		return null;
	}

	private void detachVol(List<DiskInfo> ebsParam) {

		List<String> ebsIdList = ebsParam.stream()
				.map(ebs -> ebs.get("DISK_PATH"))
				.map(String::valueOf)
				.collect(Collectors.toList());

		for(String ebsId : ebsIdList) {
			DetachVolumeRequest detachVolumeRequest = DetachVolumeRequest.builder().volumeId(ebsId).build();
			client.detachVolume(detachVolumeRequest);
		}
	}


}
