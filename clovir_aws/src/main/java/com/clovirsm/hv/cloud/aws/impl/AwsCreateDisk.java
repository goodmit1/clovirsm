package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import software.amazon.awssdk.services.ec2.model.CreateVolumeRequest;
import software.amazon.awssdk.services.ec2.model.CreateVolumeResponse;
import software.amazon.awssdk.services.ec2.model.DescribeVolumesRequest;
import software.amazon.awssdk.services.ec2.model.VolumeState;

import java.util.Map;

public class AwsCreateDisk extends AwsCommon {

    public AwsCreateDisk(IConnection conn) {
        super(conn);

    }

    @Override
    public String run1(HVParam  param, Map result) throws Exception {
        return null;
    }
    protected String create( String availabilityZone, DiskInfo diskInfo, boolean isWait) throws InterruptedException {
        CreateVolumeRequest createVolumeRequest =   CreateVolumeRequest.builder().size(diskInfo.getDISK_SIZE()).availabilityZone(availabilityZone).volumeType(diskInfo.getDATASTORE_NM_LIST()[0]).build();
        CreateVolumeResponse volumeResponse = client.createVolume(createVolumeRequest);
        if(isWait){
            while(true){
                Thread.sleep(500);
                if(client.describeVolumes(
                        DescribeVolumesRequest.builder().volumeIds(volumeResponse.volumeId()).build()
                    ).volumes().get(0).state().compareTo(VolumeState.AVAILABLE)==0){
                    break;
                }

            }
          }
        return volumeResponse.volumeId();
    }

}
