package com.clovirsm.hv.cloud.aws;


import com.clovirsm.hv.IConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudformation.CloudFormationClient;
import software.amazon.awssdk.services.ec2.Ec2Client;

import java.util.Map;

public class AwsConnection implements IConnection{
    protected Ec2Client ec2Client = null;
    private String url;
    private Region regions;
  	private AwsBasicCredentials credentials;
    public Logger logger = LoggerFactory.getLogger(this.getClass());
	CloudFormationClient cfClient;



	public Ec2Client getClient()
	{
		return ec2Client;
	}

	public CloudFormationClient getCloudFormationClient(){ return cfClient; }
	

	@Override
	public String getURL() {
		// TODO Auto-generated method stub
		return url;
	}


	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		credentials =  AwsBasicCredentials.create(userId,pwd);
		regions = Region.of(url);
	  	ec2Client =Ec2Client.builder()
				.credentialsProvider(StaticCredentialsProvider.create(credentials))
                .region(regions)
                .build();
		cfClient = CloudFormationClient.builder()
				.region(regions)
				.credentialsProvider(StaticCredentialsProvider.create(credentials))
				.build();
	  	this.url = url;
	}


	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		ec2Client = null;
	}


	@Override
	public boolean isConnected() {
		// TODO Auto-generated method stub
		return ec2Client != null;
	}

	public Region getRegions() {
		return regions;
	}

	public void setRegions(Region regions) {
		this.regions = regions;
	} 
	
	

}
