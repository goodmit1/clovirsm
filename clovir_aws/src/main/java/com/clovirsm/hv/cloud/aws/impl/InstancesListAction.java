package com.clovirsm.hv.cloud.aws.impl;


import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.aws.AwsCommon;
import com.clovirsm.hv.obj.HVParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse;

import java.util.Map;

public class InstancesListAction extends AwsCommon {
	DescribeInstancesResponse describeInstancesResult;
	
	public InstancesListAction(IConnection conn) {
		super(conn);
	}

	public Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public String run1(HVParam param, Map result) throws Exception {

		logger.info ("Instance Search Start");

		DescribeInstancesRequest.Builder requestBuilder = DescribeInstancesRequest.builder();
    	

    	
    	describeInstancesResult = client.describeInstances(requestBuilder.build());
    

    		result.put("instanceList", describeInstancesResult.reservations().get(0).instances());

		return null;
	}
	
	public boolean isInstance(String instanceName) {
		Long count = describeInstancesResult.reservations().get(0).instances()
		.stream()
		.filter(predicate -> predicate.instanceId().equals(instanceName))
		.count();
		return count > 0 ? true : false;
	}

}
