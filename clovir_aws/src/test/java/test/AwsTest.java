package test;


import com.clovirsm.hv.CommonUtil;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudformation.CloudFormationClient;
import software.amazon.awssdk.services.cloudformation.model.*;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.Tag;
import software.amazon.awssdk.services.ec2.model.*;


import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Ignore
public class AwsTest {
    Ec2Client client;
    Region region;
    AwsBasicCredentials credentials;

    @Before
    public void login() throws Exception {
        String userId = "AKIAZLHBBKFJZGURYBHO";
        String pwd = "dQXYdyeZ80tTd6ccG7XNEMn3Gz56yhid4KmTo+YZ";
        String url = "ap-northeast-2";
        credentials = AwsBasicCredentials.create(
                userId,
                pwd);

        region = Region.of(url);
        client = Ec2Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(credentials))
                .region(region)
                .build();
    }

    private String getTagVal(List<Tag> tags, String key) {
        for (Tag tag : tags) {
            if (tag.key().equals(key)) {
                return tag.value();
            }
        }
        return null;
    }

    private int getStorageSize(String volumnId) {
        DescribeVolumesRequest request = DescribeVolumesRequest.builder().volumeIds(volumnId).build();

        DescribeVolumesResponse result = client.describeVolumes(request);
        List<Volume> volumes = result.volumes();
        if (volumes.size() > 0) {

            return volumes.get(0).size();
        }
        return 0;
    }

    private Map getDiskMap(String id, String name, int size) {
        Map result = new HashMap();
        result.put("DISK_PATH", id);
        result.put("DISK_NM", name);
        result.put("DISK_SIZE", size);
        return result;
    }

    @Test
    public void deleteVMByTemplate() throws Exception {
        CloudFormationClient cfClient = CloudFormationClient.builder()
                .region(region)
                .credentialsProvider(StaticCredentialsProvider.create(credentials))
                .build();
        this.deleteStack(cfClient, "test1");
    }

    @Test
    public void createVMByTemplate() throws Exception {
        CloudFormationClient cfClient = CloudFormationClient.builder()
                .region(region)
                .credentialsProvider(StaticCredentialsProvider.create(credentials))
                .build();


        File file = new File("C:\\work\\clovir\\clovirsm\\clovir_aws\\src\\test\\java\\com\\clovirsm\\hv\\cloud\\aws\\template.yaml");

        String name = "test1";
        List parameters = new ArrayList();
        parameters.add(Parameter.builder().parameterKey("Name").parameterValue(name).build());
        parameters.add(Parameter.builder().parameterKey("KeyName").parameterValue("test").build());
        parameters.add(Parameter.builder().parameterKey("InstanceType").parameterValue("t2.nano").build());
        String templateBody =  CommonUtil.readStream2String( new FileInputStream(file) , "utf-8");
        Map yamlParam = new HashMap();
        String adminId = "clovirsm";
        String adminPublicKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIphOVY65SoYVm3MHUiTBSPjOflFXsCUSspcy6sYQOms6MDRJLxSyThV6uzIq37mlVKaIJ6+yQ9SCOmEeGe9iA2BfxlwYQJz236Xf9zWGvJey/+tUSXZvwEmacMDfFdJpP+f22F6B/cCD6WICLJLf4KSmYAmwFX7hM2V9hLvOc66T8JG2V6xiy2efw53hcTYM8Z/HZI2vX5zcC+ppJz62IhorVkO0ESBpIs2TyfwMJCu0LcILEvdYRvJt8jroGpe4NLle7eNLMUNs82AwcAXaSQDs2MWWGJkrK55X0xr2AislA0yc4LD2ZNQAJvFr7K7t0+c7GNzLK2dpsyp3+Sxh5";
        String addUserShellFile = "UserAdd.sh";
        String adminPwd = "QWER43212!@#$";

        yamlParam.put("ADMIN_ID", adminId);
        yamlParam.put("ADMIN_PWD", adminPwd);
        yamlParam.put("ADMIN_SSH_KEY", adminPwd);
        templateBody = CommonUtil.fillContentByVar(templateBody, yamlParam);
        //ValidateTemplateRequest validReq = ValidateTemplateRequest.builder().templateBody(templateBody).build();
        //System.out.println(cfClient.validateTemplate(validReq).);
        String status = getStacksStatus(cfClient, name);
        if ("ROLLBACK_COMPLETE".equals(status)) {
            deleteStack(cfClient, name);
            Thread.sleep(10000);
            status = null;
        }
        if (status == null) {
            CreateStackRequest req = CreateStackRequest.builder().stackName(name).templateBody(templateBody).parameters(parameters).build();

            cfClient.createStack(req);
        } else {
            UpdateStackRequest req = UpdateStackRequest.builder().stackName(name).templateBody(templateBody).parameters(parameters).build();

            cfClient.updateStack(req);
        }
    }

    @Test
    public void listStacks() {
        CloudFormationClient cfClient = CloudFormationClient.builder()
                .region(region)
                .credentialsProvider(StaticCredentialsProvider.create(credentials))
                .build();
        DescribeStacksRequest req = DescribeStacksRequest.builder().stackName("test").build();
        DescribeStacksResponse res = cfClient.describeStacks(req);
        System.out.println(res.stacks().get(0).stackStatusAsString());
    }

    private void deleteStack(CloudFormationClient cfClient, String stackName) {
        try {
            DeleteStackRequest req = DeleteStackRequest.builder().stackName(stackName).build();
            DeleteStackResponse res = cfClient.deleteStack(req);

        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }

    private String getStacksStatus(CloudFormationClient cfClient, String stackName) {
        try {
            DescribeStacksRequest req = DescribeStacksRequest.builder().stackName(stackName).build();
            DescribeStacksResponse res = cfClient.describeStacks(req);
            return res.stacks().get(0).stackStatusAsString();
        } catch (Exception e) {
            return null;
        }
    }

    @Test
    public void getVM() {
        DescribeInstancesRequest req = DescribeInstancesRequest.builder().filters(Filter.builder().name("tag:Name").values("clovirsm-linux").build()).build();
        List<Instance> instances = client.describeInstances(req).reservations().get(0).instances();
        for (Instance img : instances) {
            System.out.println(img.instanceId() + ":" + img.platformAsString());
        }
    }

    @Test
    public void listTemplate() {
        DescribeImagesRequest req = DescribeImagesRequest.builder().imageIds("ami-0d610d52f21b16389").build();
        List<Image> images = client.describeImages(req).images();
        for (Image img : images) {
            System.out.println(img.name() + ":" + img.platformAsString());
        }


        req = DescribeImagesRequest.builder().owners("self").build();
        images = client.describeImages(req).images();
        for (Image img : images) {
            System.out.println(img.name() + ":" + img.platformAsString());
        }
    }

    @Test
    public void listVM() {
        boolean done = false;
        String nextToken = null;

        List result = new ArrayList();

        do {
            DescribeInstancesRequest request = DescribeInstancesRequest.builder().maxResults(6).nextToken(nextToken).build();
            DescribeInstancesResponse response = client.describeInstances(request);

            for (Reservation reservation : response.reservations()) {
                for (Instance vm : reservation.instances()) {

                    List<Tag> tags = vm.tags();
                    Map result1 = new HashMap();
                    result1.put("VM_NM", getTagVal(tags, "Name"));
                    result1.put("VM_HV_ID", vm.instanceId());
                    result1.put("SPEC_TYPE", vm.instanceType());
                    result1.put("PRIVATE_IP", vm.privateIpAddress());
                    result1.put("PUBLIC_DNS", vm.publicDnsName());
                    result1.put("CPU_CNT", vm.cpuOptions().coreCount());
                    result1.put("RUN_CD", vm.state().code() == 80 ? "S" : (vm.state().code() == 16 ? "R" : "F"));
                    List<InstanceBlockDeviceMapping> blockMappings = vm.blockDeviceMappings();
                    List disks = new ArrayList();
                    for (InstanceBlockDeviceMapping m : blockMappings) {
                        int size = getStorageSize(m.ebs().volumeId());
                        if (size > 0) {
                            disks.add(this.getDiskMap(m.ebs().volumeId(), m.deviceName(), size));
                        }
                    }
                    result1.put("DISK_LIST", disks);
                    result.add(result1);


                }
            }
            nextToken = response.nextToken();
        } while (nextToken != null);
        System.out.println(result);
    }

    @Test
    public void listDiskType() {
        DescribeVolumeAttributeRequest req = DescribeVolumeAttributeRequest.builder().build();
        DescribeVolumeAttributeResponse res = client.describeVolumeAttribute(req);
        System.out.println(res);
    }


}
