package com.clovirsm.hv.cloud.aws;

import org.junit.jupiter.api.BeforeEach;

public class AwsWinVMTest extends AwsLinuxVMAPITest{

    @BeforeEach
    public void init(){
        super.init();
        from = "ami-0d610d52f21b16389";
        vmName="clovirsm-win";

        addUserShellFile = "UserAdd.ps1";
        osDiskSize = 30;
        isLinux = false;
    }

}
