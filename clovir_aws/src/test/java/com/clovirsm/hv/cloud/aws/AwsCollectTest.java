package com.clovirsm.hv.cloud.aws;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.HVParam;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;
import java.util.Map;
import java.util.Set;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AwsCollectTest extends AwsAPITest {
    @Test
    public void listHVObj() throws Exception {

        List<Map> list =api.listObject(new HVParam( connInfo) );

         
        Set types = CommonUtil.getDistinctValues(list,"OBJ_TYPE_NM");
        System.out.println(types);
        System.out.println(list);
        Assert.assertNotNull(list);
        Assert.assertNotEquals(list.size(),0);
        Assert.assertEquals(types.size(),4); // Datastore, Template, InstanceType,Datastore
        int instanceTypeCnt = 0;
        for(Map m:list){
            if("instanceType".equals(m.get("OBJ_TYPE_NM"))) {
                instanceTypeCnt++;
                String prop = (String) m.get("OBJ_PROP");
                JSONObject json  =new JSONObject( prop );
                System.out.println("cpu:" + json.get(HypervisorAPI.PARAM_CPU) + ",memory:" + json.get(HypervisorAPI.PARAM_MEM));

            }
        }
        Assert.assertNotEquals(instanceTypeCnt, 0);

    }
}
