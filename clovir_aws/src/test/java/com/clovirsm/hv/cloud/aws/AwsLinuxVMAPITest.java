package com.clovirsm.hv.cloud.aws;

import com.clovirsm.hv.cloud.aws.impl.AwsAttachVolume;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AwsLinuxVMAPITest extends AwsAPITest {
    @BeforeEach
    public void init(){
        from = "ami-01dfc1f3cfb99226f";
        vmName="clovirsm-linux";

        addUserShellFile = "UserAdd.ps1";
        osDiskSize = 127;
        isLinux = true;
        dataDiskSize = 10;
        osDiskSize = 8;
        addUserShellFile = "UserAdd.sh";
        specType = "t2.medium";
        diskType = "gp2";
        super.new_specType = "t2.small";
    }

    @Test
    public void nextMountNm(){
        String lastMount = "/dev/sdf";
        AwsAttachVolume action = new AwsAttachVolume(null);

        String res = action.nextMount(lastMount);
        assertEquals("/dev/sdg", res);
    }

}
