package com.vmware.sso.client.samples;

import com.rsa.names._2009._12.product.riat.wsdl.STSService;
import com.rsa.names._2009._12.product.riat.wsdl.STSServicePortType;
import com.vmware.sso.client.soaphandlers.HeaderHandlerResolver;
import com.vmware.sso.client.soaphandlers.SamlTokenExtractionHandler;
import com.vmware.sso.client.soaphandlers.TimeStampHandler;
import com.vmware.sso.client.soaphandlers.UserCredentialHandler;
import com.vmware.sso.client.soaphandlers.WsSecurityUserCertificateSignatureHandler;
import com.vmware.sso.client.utils.SecurityUtil;
import com.vmware.sso.client.utils.Utils;
import java.io.PrintStream;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.TimeZone;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import org.oasis_open.docs.ws_sx.ws_trust._200512.LifetimeType;
import org.oasis_open.docs.ws_sx.ws_trust._200512.RenewingType;
import org.oasis_open.docs.ws_sx.ws_trust._200512.RequestSecurityTokenType;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_utility_1_0.AttributedDateTime;
import org.w3c.dom.Element;

public class AcquireHoKTokenByUserCredentialSample
{
  public static Element getToken(String[] paramArrayOfString, PrivateKey paramPrivateKey, X509Certificate paramX509Certificate)
    throws DatatypeConfigurationException
  {
    STSService localSTSService = new STSService();

    HeaderHandlerResolver localHeaderHandlerResolver = new HeaderHandlerResolver();

    localHeaderHandlerResolver.addHandler(new TimeStampHandler());

    UserCredentialHandler localUserCredentialHandler = new UserCredentialHandler(paramArrayOfString[1], paramArrayOfString[2]);

    localHeaderHandlerResolver.addHandler(localUserCredentialHandler);

    WsSecurityUserCertificateSignatureHandler localWsSecurityUserCertificateSignatureHandler = new WsSecurityUserCertificateSignatureHandler(paramPrivateKey, paramX509Certificate);

    localHeaderHandlerResolver.addHandler(localWsSecurityUserCertificateSignatureHandler);
    SamlTokenExtractionHandler localSamlTokenExtractionHandler = new SamlTokenExtractionHandler();
    localHeaderHandlerResolver.addHandler(localSamlTokenExtractionHandler);

    localSTSService.setHandlerResolver(localHeaderHandlerResolver);

    STSServicePortType localSTSServicePortType = localSTSService.getSTSServicePort();

    RequestSecurityTokenType localRequestSecurityTokenType = new RequestSecurityTokenType();

    LifetimeType localLifetimeType = new LifetimeType();
    DatatypeFactory localDatatypeFactory = DatatypeFactory.newInstance();
    GregorianCalendar localGregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));

    XMLGregorianCalendar localXMLGregorianCalendar = localDatatypeFactory.newXMLGregorianCalendar(localGregorianCalendar);

    AttributedDateTime localAttributedDateTime1 = new AttributedDateTime();
    localAttributedDateTime1.setValue(localXMLGregorianCalendar.toXMLFormat());

    AttributedDateTime localAttributedDateTime2 = new AttributedDateTime();
    localXMLGregorianCalendar.add(localDatatypeFactory.newDuration(1800000L));
    localAttributedDateTime2.setValue(localXMLGregorianCalendar.toXMLFormat());

    localLifetimeType.setCreated(localAttributedDateTime1);
    localLifetimeType.setExpires(localAttributedDateTime2);

    localRequestSecurityTokenType.setTokenType("urn:oasis:names:tc:SAML:2.0:assertion");
    localRequestSecurityTokenType.setRequestType("http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue");

    localRequestSecurityTokenType.setLifetime(localLifetimeType);
    localRequestSecurityTokenType.setKeyType("http://docs.oasis-open.org/ws-sx/ws-trust/200512/PublicKey");

    localRequestSecurityTokenType.setSignatureAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");

    localRequestSecurityTokenType.setDelegatable(Boolean.valueOf(true));

    RenewingType localRenewingType = new RenewingType();
    localRenewingType.setAllow(Boolean.TRUE);
    localRenewingType.setOK(Boolean.FALSE);
    localRequestSecurityTokenType.setRenewing(localRenewingType);

    Map localMap = ((BindingProvider)localSTSServicePortType).getRequestContext();

    localMap.put("javax.xml.ws.service.endpoint.address", paramArrayOfString[0]);

    localSTSServicePortType.issue(localRequestSecurityTokenType);

    return localSamlTokenExtractionHandler.getToken();
  }

 
}