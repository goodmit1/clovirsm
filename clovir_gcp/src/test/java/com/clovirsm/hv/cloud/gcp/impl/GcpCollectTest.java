package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.obj.HVParam;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;
import java.util.Map;
import java.util.Set;
@Ignore
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GcpCollectTest extends GcpTest {

    @Test
    public void listHVObj() throws Exception {

        List<Map> list  = api.listObject(new HVParam(connInfo));


        Set types = CommonUtil.getDistinctValues(list,"OBJ_TYPE_NM");
        System.out.println(types);
        System.out.println(list);
        Assert.assertNotNull(list);

        Assert.assertEquals(types.size(),4); // Datastore, Template, InstanceType,
        int instanceTypeCnt = 0;
        for(Map m:list){
            if("instanceType".equals(m.get("OBJ_TYPE_NM"))) {
                instanceTypeCnt++;
                String prop = (String) m.get("OBJ_PROP");
                JSONObject json  =new JSONObject( prop );
                System.out.println("cpu:" + json.get(HypervisorAPI.PARAM_CPU) + ",memory:" + json.get(HypervisorAPI.PARAM_MEM));

            }
        }
        Assert.assertNotEquals(instanceTypeCnt, 0);

    }
}
