package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMTestCommon;
import com.clovirsm.hv.cloud.gcp.GcpAPI;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;


@Ignore
public class GcpTest extends VMTestCommon {


    @BeforeEach
    public void login() throws Exception {

        String pwdJson = "{\n" +
                "  \"type\": \"service_account\",\n" +
                "  \"project_id\": \"propane-abbey-318208\",\n" +
                "  \"private_key_id\": \"18d39a9ca030d547e2061171ae2b4ed4514ab071\",\n" +
                "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCOMKR2mZBwX/i6\\nIZ6JdkJb/P4jgrfJDxnCOvqgNROspNIqt9Mv7884lbHAbrHx7dIO0o5x7jNfT/JG\\nIqRR7AJ4DHFX5lmTP3iE3VdNimu9JxTIdhXJjjD509OQawLsO6G8X64aI9dqDaYx\\nrBVBqdPsaa9yayW8rdTO1DdE1Gl4K1mjQByDS/BR6JPnqkUZhLLi6mtgLa7ZBv3J\\n0n7Xd9YjVeWKmyG/DBzvUJWD3687brJzv39Gf/NYOkpD76uLIzp1mWGxfichVXJH\\nvwlQnhDL7C3AnxyOOuxfjv5aNZfhuj4pvGQ4kzZhLs1qLLX3I/oZ2myJpwXlmREw\\nqqCLgGBNAgMBAAECggEAEWSI0YsSPHS414fCPT1Oe7MUISLduUXUGEqfmpYzfGRC\\n4v/OYOg8GEdTUV8Y/CdjkPWXf/8roIaGc6bMI8szOB0Jhj+CiHz0kIgEpFMAv8f2\\nhljPbHKssfqDwAAfAOeAbqrjgIYO1QWrZRsQFJKh/iKlV+EcYhp/U/dodU5oQk8Y\\nclOIDdln6zqvb7wHLTOoTLJ7GB0IJIHlbRdoDOKtl1HhbNLHjvftUFHp1iJ9DaSH\\ntRMurI0VuTQntkFgsNzmUXOOmCv2TXfURdhjTLXvRqyQk6BPvp3UIajhaE5sjNJn\\n15hcwoPRCOgvMaBVRo/yI80eEzSbIK5+0J7ejwtxEQKBgQDISLocxYGXQBAepPo1\\nrraJVUCI7l2wfMHhDbB5OMySPFCbV7ksMTD5HFbjCylj/zWl1wdQzyXRAWQxT6pB\\noBLbsMcgTMQ7YuTh6qEii9PIqB2rtQRq/0q4ZlEfts3SpBC6p2x6CeJ5vpPw5vaH\\nJLsS254OhMGn1g9/WA67DJ9vZQKBgQC1vrvDkLd3XvJ6pNq6ITfpg5yCxFOFqgMr\\nndlFfqXSO6GNPOteGTum37nO09d7wUYVS3+eQprGqb3AGa3bJiOi/CKdwXH5XbT9\\nt3QOG8FbTAA4r7/SNpsro6A5hFKmj25RjE0PsWT0n+Iu3FrcxT7vdVBgwVFLTmE8\\nrJDfm0SiyQKBgAKHyLIsXKLIkGuSsgaTmoLJrPMN7+kW6mwpfB1L9mmykWIlDZXd\\nWT0MepyLu44j302lFdaTh2rIlbd5xjDmENbtuNZTofcOHM4t5LNHCPn0BcvxkYi4\\niP0jbcr0yEzs/bkd/GTeUxouqjfU8zBp6mMmmyybyMaSEcgOGMGMO0dxAoGAcxmt\\nTkh7eLT/+d4ny9M31sKMP2DYyIEIavoX3OQ9Xix3vrwDD+AJIED2Kt+My4p2uQvM\\ncc0aupBLZLqemXjI/vKRdblKVjQbaMdjT4ASb6BtT3k2kYoVsYh585MyyNtzcV/a\\negLNvgL3utfBD6o3lm2EjxI/SuMDv+pGAecSi8kCgYEArTgyKkk7S9ar4FwtGlKm\\nSdVK/IKIhWmn6mN5tH+9Do0Dh2yrdTwR/5a/g48cE3VthhuNzfg4CB9ocrjiJ9RU\\ncRJjcxDyj7bR8oOFo4FjPnkZ+Y0gnNno3vV2NyAvZB8ggl/lWInMl8KsA7yGJqj9\\n0VSbn1n8W/CMLmj1Xn8LUKA=\\n-----END PRIVATE KEY-----\\n\",\n" +
                "  \"client_email\": \"api-209@propane-abbey-318208.iam.gserviceaccount.com\",\n" +
                "  \"client_id\": \"107317225883486958393\",\n" +
                "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/api-209%40propane-abbey-318208.iam.gserviceaccount.com\"\n" +
                "}\n";
        String region = "asia-northeast3-a";
        String prjId = "propane-abbey-318208";


        connInfo.put(HypervisorAPI.PARAM_URL, region);
        connInfo.put(HypervisorAPI.PARAM_USERID, prjId);
        connInfo.put(HypervisorAPI.PARAM_PWD, pwdJson);

        api = new GcpAPI();
    }
}
