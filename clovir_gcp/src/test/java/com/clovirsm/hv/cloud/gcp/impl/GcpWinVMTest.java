package com.clovirsm.hv.cloud.gcp.impl;


import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;

@Ignore
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GcpWinVMTest extends GcpLinuxVMTest{
    @BeforeEach
    public void init(){
        super.init();
        from = "projects/propane-abbey-318208/global/images/win2012-ssh";
        vmName="clovirsm-win";

        addUserShellFile = "UserAdd.ps1";
        osDiskSize = 50;
        isLinux = false;
    }

}
