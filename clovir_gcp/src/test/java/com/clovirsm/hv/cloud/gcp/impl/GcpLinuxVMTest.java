package com.clovirsm.hv.cloud.gcp.impl;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

@Ignore
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GcpLinuxVMTest extends GcpTest {

    @BeforeEach
    public void init(){
        from = "projects/propane-abbey-318208/global/images/image-1";
        vmName="clovirsm-linux";

        addUserShellFile = "UserAdd.ps1";
        osDiskSize = 127;
        isLinux = true;
        dataDiskSize = 10;
        osDiskSize = 11;
        addUserShellFile = "UserAdd.sh";
        specType = "projects/propane-abbey-318208/zones/asia-northeast3-a/machineTypes/e2-medium";
        diskType = "projects/propane-abbey-318208/zones/asia-northeast3-a/diskTypes/pd-standard";
        super.new_specType = "projects/propane-abbey-318208/zones/asia-northeast3-a/machineTypes/e2-small";
        super.new_cpu = 2;
        super.new_mem = 2;
    }



}