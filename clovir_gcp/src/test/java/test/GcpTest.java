package test;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.RestClient;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.*;
import com.google.api.services.compute.model.Operation;
import com.google.api.services.deploymentmanager.DeploymentManager;
import com.google.api.services.deploymentmanager.model.*;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.awt.print.PrinterJob;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Ignore
public class GcpTest {
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    String applicationName = "gcpDemo";
    Compute compute;
    JSONObject pwd;
    NetHttpTransport httpTransport;
    HttpRequestInitializer requestInitializer;
    String PROJECT_ID = null;
    String ZONE_NAME = "asia-northeast3-a";
    @Before
    public void login() throws  Exception {
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        String json = "{\n" +
                "  \"type\": \"service_account\",\n" +
                "  \"project_id\": \"propane-abbey-318208\",\n" +
                "  \"private_key_id\": \"18d39a9ca030d547e2061171ae2b4ed4514ab071\",\n" +
                "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCOMKR2mZBwX/i6\\nIZ6JdkJb/P4jgrfJDxnCOvqgNROspNIqt9Mv7884lbHAbrHx7dIO0o5x7jNfT/JG\\nIqRR7AJ4DHFX5lmTP3iE3VdNimu9JxTIdhXJjjD509OQawLsO6G8X64aI9dqDaYx\\nrBVBqdPsaa9yayW8rdTO1DdE1Gl4K1mjQByDS/BR6JPnqkUZhLLi6mtgLa7ZBv3J\\n0n7Xd9YjVeWKmyG/DBzvUJWD3687brJzv39Gf/NYOkpD76uLIzp1mWGxfichVXJH\\nvwlQnhDL7C3AnxyOOuxfjv5aNZfhuj4pvGQ4kzZhLs1qLLX3I/oZ2myJpwXlmREw\\nqqCLgGBNAgMBAAECggEAEWSI0YsSPHS414fCPT1Oe7MUISLduUXUGEqfmpYzfGRC\\n4v/OYOg8GEdTUV8Y/CdjkPWXf/8roIaGc6bMI8szOB0Jhj+CiHz0kIgEpFMAv8f2\\nhljPbHKssfqDwAAfAOeAbqrjgIYO1QWrZRsQFJKh/iKlV+EcYhp/U/dodU5oQk8Y\\nclOIDdln6zqvb7wHLTOoTLJ7GB0IJIHlbRdoDOKtl1HhbNLHjvftUFHp1iJ9DaSH\\ntRMurI0VuTQntkFgsNzmUXOOmCv2TXfURdhjTLXvRqyQk6BPvp3UIajhaE5sjNJn\\n15hcwoPRCOgvMaBVRo/yI80eEzSbIK5+0J7ejwtxEQKBgQDISLocxYGXQBAepPo1\\nrraJVUCI7l2wfMHhDbB5OMySPFCbV7ksMTD5HFbjCylj/zWl1wdQzyXRAWQxT6pB\\noBLbsMcgTMQ7YuTh6qEii9PIqB2rtQRq/0q4ZlEfts3SpBC6p2x6CeJ5vpPw5vaH\\nJLsS254OhMGn1g9/WA67DJ9vZQKBgQC1vrvDkLd3XvJ6pNq6ITfpg5yCxFOFqgMr\\nndlFfqXSO6GNPOteGTum37nO09d7wUYVS3+eQprGqb3AGa3bJiOi/CKdwXH5XbT9\\nt3QOG8FbTAA4r7/SNpsro6A5hFKmj25RjE0PsWT0n+Iu3FrcxT7vdVBgwVFLTmE8\\nrJDfm0SiyQKBgAKHyLIsXKLIkGuSsgaTmoLJrPMN7+kW6mwpfB1L9mmykWIlDZXd\\nWT0MepyLu44j302lFdaTh2rIlbd5xjDmENbtuNZTofcOHM4t5LNHCPn0BcvxkYi4\\niP0jbcr0yEzs/bkd/GTeUxouqjfU8zBp6mMmmyybyMaSEcgOGMGMO0dxAoGAcxmt\\nTkh7eLT/+d4ny9M31sKMP2DYyIEIavoX3OQ9Xix3vrwDD+AJIED2Kt+My4p2uQvM\\ncc0aupBLZLqemXjI/vKRdblKVjQbaMdjT4ASb6BtT3k2kYoVsYh585MyyNtzcV/a\\negLNvgL3utfBD6o3lm2EjxI/SuMDv+pGAecSi8kCgYEArTgyKkk7S9ar4FwtGlKm\\nSdVK/IKIhWmn6mN5tH+9Do0Dh2yrdTwR/5a/g48cE3VthhuNzfg4CB9ocrjiJ9RU\\ncRJjcxDyj7bR8oOFo4FjPnkZ+Y0gnNno3vV2NyAvZB8ggl/lWInMl8KsA7yGJqj9\\n0VSbn1n8W/CMLmj1Xn8LUKA=\\n-----END PRIVATE KEY-----\\n\",\n" +
                "  \"client_email\": \"api-209@propane-abbey-318208.iam.gserviceaccount.com\",\n" +
                "  \"client_id\": \"107317225883486958393\",\n" +
                "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/api-209%40propane-abbey-318208.iam.gserviceaccount.com\"\n" +
                "}\n";
        pwd = new JSONObject((json));
        PROJECT_ID =pwd.getString("project_id");
        InputStream stream = new ByteArrayInputStream( json.getBytes(StandardCharsets.UTF_8));
        GoogleCredentials credential = GoogleCredentials.fromStream( stream );
        requestInitializer = new HttpCredentialsAdapter(credential);

        compute = new Compute.Builder(httpTransport, JSON_FACTORY, requestInitializer)
                .setApplicationName(applicationName)
                .build();


    }
    @Test
    public void listVM() throws IOException {
        String PROJECT_ID=pwd.getString("project_id");
        String ZONE_NAME = "asia-northeast3-a";
        Compute.Instances.List list = compute.instances().list(PROJECT_ID, ZONE_NAME);
        System.out.println(list.buildHttpRequest().getUrl());
        System.out.println(list.buildHttpRequest().getHeaders().get("authorization"));

        List<Instance> vms = list.execute().getItems();
        //System.out.println(list.getLastResponseHeaders().get("authorization"));
        for(Instance vm : vms){
            System.out.println(vm.getName());
        }
    }
    public MachineTypeList machineTypeList(Compute compute) throws IOException {

        Compute.MachineTypes.List request = compute.machineTypes().list(PROJECT_ID, ZONE_NAME);
        MachineTypeList response;

        do {
            response = request.execute();
            if (response.getItems() == null) {
                continue;
            }
            request.setPageToken(response.getNextPageToken());
        } while (response.getNextPageToken() != null);


        return response;
    }
    public NetworkList networkList(Compute compute) throws IOException {

        Compute.Networks.List request = compute.networks().list(PROJECT_ID);
        NetworkList response;
        do {
            response = request.execute();
            if (response.getItems() == null) {
                continue;
            }
            request.setPageToken(response.getNextPageToken());
        } while (response.getNextPageToken() != null);

        return response;
    }

    public NetworkInterface networkInterfaceConfig(Compute compute) throws IOException {
        String NETWORK_INTERFACE_CONFIG = "ONE_TO_ONE_NAT";

        final String NETWORK_ACCESS_CONFIG = "External NAT";

        NetworkInterface networkInterface = new NetworkInterface();
        String networkSelfLink = networkList(compute).getItems()
                .stream()
                .map(Network::getSelfLink)
                .collect(Collectors.joining());
        System.out.println(networkSelfLink);
        networkInterface.setNetwork(networkSelfLink);

        List<AccessConfig> configs = new ArrayList<>();
        AccessConfig config = new AccessConfig();
        config.setType(NETWORK_INTERFACE_CONFIG);
        config.setName(NETWORK_ACCESS_CONFIG);
        configs.add(config);
        networkInterface.setAccessConfigs(configs);
//        networkInterface.setNetwork();

        return networkInterface;
    }
    @Test
    public void listDisks() throws  Exception{
        DiskList disks = compute.disks().list(PROJECT_ID, ZONE_NAME).execute();
        List<Disk> items = disks.getItems();
        for(Disk d : items){
            System.out.println(d.getId());
        }
    }
    @Test
    public void listDiskTypes() throws  Exception{

        List<DiskType> diskTypes = compute.diskTypes().list(PROJECT_ID, ZONE_NAME).execute().getItems();
        System.out.println(diskTypes);
    }
    @Test
    public void listImage() throws  Exception{
        ImageList imageList;

        Compute.Images.List images = compute.images().list(PROJECT_ID)
                .setFilter("status=READY");

        do {
            imageList = images.execute();
            if (imageList.getItems() == null) {
                continue;
            }
            imageList.getItems().forEach(image -> {
                System.out.println(image.getName() + ":"+ image.getId() + ":" + image.getSelfLink());

            });
            images.setPageToken(imageList.getNextPageToken());
        } while (imageList.getNextPageToken() != null);
    }

    @Test
    public void listmMachineImage() throws  Exception{
        String PROJECT_ID=pwd.getString("project_id");
        String ZONE_NAME = "asia-northeast3-a";
        System.out.println(compute.projects().get(PROJECT_ID).execute());
        String authKey = (String) ((List)compute.projects().get(PROJECT_ID).buildHttpRequest().getHeaders().get("authorization")).get(0);
        System.out.println(authKey);

    }
    protected String selectImageName(Compute compute) throws IOException {

        ImageList imageList;
        String imageProjectId = "ubuntu-os-cloud";

        Compute.Images.List images = compute.images().list(imageProjectId)
                .setFilter("status=READY");

        do {
            imageList = images.execute();
            if (imageList.getItems() == null) {
                continue;
            }
            images.setPageToken(imageList.getNextPageToken());
        } while (imageList.getNextPageToken() != null);

        return getOSImage(imageList);
    }

    public String getOSImage(ImageList imageList) {


        return Optional.ofNullable(imageList.getItems())
                .orElseGet(ArrayList::new)
                .stream()
                .map(Image::getSelfLink)
                .limit(1)
                .collect(Collectors.joining());

    }

    public AttachedDisk diskConfig(Compute compute, String instanceName) throws IOException {

        AttachedDisk disk = new AttachedDisk();
        disk.setBoot(true);
        disk.setAutoDelete(true);
        disk.setType("PERSISTENT");

        AttachedDiskInitializeParams initializeParams = new AttachedDiskInitializeParams();
        initializeParams.setDiskName(instanceName);
        initializeParams.setSourceImage(selectImageName(compute));
        initializeParams.setDiskType(
                String.format(
                        "https://www.googleapis.com/compute/v1/projects/%s/zones/%s/diskTypes/pd-standard",
                        PROJECT_ID, ZONE_NAME));
        disk.setInitializeParams(initializeParams);

        return disk;
    }


    public Operation createInstance(Compute compute, String instanceName, String machineTypeSelfLink) throws  Exception {


        //instance 설정
        Instance instance = new Instance();
        instance.setName(instanceName);
        instance.setMachineType(machineTypeSelfLink);

        //NetWork interface 설정
        instance.setNetworkInterfaces(Collections.singletonList(networkInterfaceConfig(compute)));
        //Disk 설정
        instance.setDisks(Collections.singletonList(diskConfig(compute, instanceName)));
        //account 설정
        instance.setServiceAccounts(Collections.singletonList(accountConfig()));
        //mateData 설정 (script, ssh)
        instance.setMetadata(metadataConfig());

        Compute.Instances.Insert insert = compute.instances().insert(PROJECT_ID, ZONE_NAME, instance);

        return  insert.execute();
    }
    public Metadata metadataConfig() {
        Metadata metadata = new Metadata();
        List<Metadata.Items> items = new ArrayList<>();
        Metadata.Items item = new Metadata.Items();
        item.setKey("startup-script");
        item.setValue(createUserScript("gitsjcho"));
        items.add(item);
        items.add(createSshSettings());
        metadata.setItems(items);
        return metadata;
    }

    public String createUserScript(String id) {
        return "sudo useradd -m " + id;
    }
    public Metadata.Items createSshSettings(){
        Metadata.Items item = new Metadata.Items();
        StringBuilder builder = new StringBuilder();
        builder.append("gitsjcho:ssh-rsa ");
        item.setKey("ssh-keys");
        item.setValue(builder.toString());

        return item;

    }

    public ServiceAccount accountConfig() {

        // Initialize the service account to be used by the VM Instance and set the API access scopes.
        ServiceAccount account = new ServiceAccount();
        account.setEmail("default");
        List<String> scopes = new ArrayList<>();
        scopes.add("https://www.googleapis.com/auth/devstorage.full_control");
        scopes.add("https://www.googleapis.com/auth/compute");
        account.setScopes(scopes);

        return account;
    }

    @Test
    public void deleteComputeInstance( ) throws Exception, IOException, GeneralSecurityException {
        String instanceName = "syk-test22";
        compute.instances().delete(PROJECT_ID, ZONE_NAME, instanceName).execute();
    }
    @Test
    public void machineTypeList( ) throws Exception, IOException, GeneralSecurityException {
        List<MachineType> list = machineTypeList(compute).getItems();
        /*list.forEach(m -> {
            System.out.println(m.getName() + ":" + m.getSelfLink() + ":" + m.getGuestCpus());
        });*/
        System.out.println(list.stream().filter(m ->   m.getName().startsWith("e2") ).map(MachineType::getSelfLink).collect(Collectors.joining(",")));
    }
    @Test
    public void createComputeInstance( ) throws Exception, IOException, GeneralSecurityException {

        String instanceName = "syk-test22";

        String e2MachineTypeSelfLink =  machineTypeList(compute).getItems()
                .stream()
                .filter(machineType -> (machineType.getName().contains("e2") && machineType.getName().contains("small")))
                .filter(machineType -> (machineType.getGuestCpus() <= 2 && machineType.getMemoryMb() <= 4096))
                .map(MachineType::getSelfLink)
                .collect(Collectors.joining());

//        MachineType machineType = new MachineType();
//        machineType.setGuestCpus(2);
//        machineType.setMemoryMb(4096);

//        Operation operation = createInstance(compute, instanceName, customMachineTypeSelfLink(machineType));
        Operation operation =  createInstance(compute, instanceName, e2MachineTypeSelfLink);
        Operation.Error error = blockUntilComplete(compute, operation);

        boolean isCreated;
        if (error == null) {

            isCreated = true;
        } else {

            isCreated = false;
        }


    }

    private static final long OPERATION_TIMEOUT_MILLIS = 60 * 1000;


    public   Operation.Error blockUntilComplete(Compute compute, Operation operation) throws Exception {
        long start = System.currentTimeMillis();
        final long pollInterval = 5 * 1000;
        String zone = operation.getZone(); // null for global/regional operations
        if (zone != null) {
            String[] bits = zone.split("/");
            zone = bits[bits.length - 1];
        }
        String status = operation.getStatus();
        String opId = operation.getName();
        while (operation != null && !status.equals("DONE")) {
            Thread.sleep(pollInterval);
            long elapsed = System.currentTimeMillis() - start;
            if (elapsed >= OPERATION_TIMEOUT_MILLIS) {
                throw new InterruptedException("Timed out waiting for operation to complete");
            }
            System.out.println("waiting...");
            com.google.api.services.compute.model.Operation operation1;
            if (zone != null) {
                Compute.ZoneOperations.Get get = compute.zoneOperations().get(PROJECT_ID, zone, opId);
                operation1 = get.execute();
            } else {
                Compute.GlobalOperations.Get get = compute.globalOperations().get(PROJECT_ID, opId);
                operation1 = get.execute();
            }
            if (operation != null) {
                status = operation.getStatus();
            }
        }
        return operation == null ? null : operation.getError();
    }
    @Test
    public void listDeployment() throws IOException {
        DeploymentManager deploymentMng = new DeploymentManager.Builder(httpTransport, JSON_FACTORY, requestInitializer).setApplicationName(applicationName).build();
        System.out.println(deploymentMng.deployments().list(PROJECT_ID));
    }

    @Test
    public void createVMByDM() throws IOException {

        DeploymentManager deploymentMng = new DeploymentManager.Builder(httpTransport, JSON_FACTORY, requestInitializer).setApplicationName(applicationName).build();
        TargetConfiguration configure = new TargetConfiguration();
        ConfigFile configFile = new ConfigFile();
        File file = new File("C:\\work\\clovir\\clovirsm\\clovir_gcp\\src\\test\\java\\test\\template.yaml");
        String content = CommonUtil.readStream2String(new FileInputStream(file),"utf-8");
        configFile.setContent(content);

        configure.setConfig(configFile);
        /*List<ImportFile> importFiles = new ArrayList<>();
        importFiles.add((new ImportFile()).setName("").setContent(""));
        configure.setImports(importFiles);*/

        Deployment deployment1 = new Deployment();
        deployment1.setName("test");
        deployment1.setTarget(configure);
        com.google.api.services.deploymentmanager.model.Operation req = deploymentMng.deployments().insert(PROJECT_ID, deployment1).execute();

    }
}
