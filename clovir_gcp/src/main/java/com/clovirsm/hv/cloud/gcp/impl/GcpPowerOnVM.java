package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;

import java.io.IOException;
import java.util.Map;

public class GcpPowerOnVM extends GcpCommon {
    public GcpPowerOnVM(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        run(param.getVmInfo());
        return null;
    }
    public void run(VMInfo vmInfo) throws IOException {
        compute.instances().start(PROJECT_ID, ZONE_NM, vmInfo.getVM_NM()).execute();

    }
}
