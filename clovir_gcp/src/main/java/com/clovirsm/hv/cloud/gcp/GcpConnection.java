package com.clovirsm.hv.cloud.gcp;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.compute.Compute;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GcpConnection implements IConnection {
    String PROJECT_ID;
    String REGION_NM;
    String ZONE_NM;
    @Override
    public String getURL() {
        return ZONE_NM;
    }
    Compute compute;
    RestClient client;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    /**
     *
     * @param url  zone
     * @param userId project_id
     * @param pwd security_json
     * @param prop
     * @throws Exception
     */
    @Override
    public void connect(String url, String userId, String pwd, Map prop) throws Exception {
        String applicationName = "ClovirSM";
        this.ZONE_NM = url;
        int pos = this.ZONE_NM.lastIndexOf("-");
        this.REGION_NM  = this.ZONE_NM.substring(0, pos);
        this.PROJECT_ID = userId;
        NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        JSONObject json = new JSONObject(pwd);

        InputStream stream = new ByteArrayInputStream( pwd.getBytes(StandardCharsets.UTF_8));
        GoogleCredentials credential = GoogleCredentials.fromStream( stream );
        HttpCredentialsAdapter requestInitializer = new HttpCredentialsAdapter(credential);

        compute = new Compute.Builder(httpTransport, JSON_FACTORY, requestInitializer)
                .setApplicationName(applicationName)
                .build();

        String authKey = (String) ((List)compute.projects().get(PROJECT_ID).buildHttpRequest().getHeaders().get("authorization")).get(0);
        client = new RestClient("");
        Map header = new HashMap();
        header.put("authorization", authKey);
        client.setHeader(header);
    }

    public RestClient getRestClient(){
        return client;
    }
    @Override
    public void disconnect() {

    }

    @Override
    public boolean isConnected() {
        try {
            compute.projects().get(PROJECT_ID).execute();
            return true;
        }
        catch(Exception e){
            return false;
        }

    }
}
