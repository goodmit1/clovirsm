package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.*;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GcpListObject extends GcpCommon {
    public GcpListObject(IConnection conn) {
        super(conn);
    }


    @Override
    public String run1(HVParam  param, Map result) throws Exception {
        List list = new ArrayList();
        String parent = ZONE_NM;
        add(list, ZONE_NM , ZONE_NM, OBJ_TYPE_REGIONS, null);
        addChild(list, parent );
        result.put(HypervisorAPI.PARAM_LIST, list);
        return parent;
    }
    private void add(List result,String id, String name, String type, String parent, JSONObject prop)
    {
        Map map = new HashMap();
        map.put("OBJ_ID", id);
        map.put("OBJ_NM", name);
        map.put("OBJ_TYPE_NM", type);
        map.put("PARENT_OBJ_NM", parent);
        if(prop !=null){
            map.put("OBJ_PROP", prop.toString());
        }
        result.add(map);
    }
    private void add(List result,String id, String name, String type, String parent )
    {
        add(result, id, name, type, parent, null);
    }
    private void addChild(List list, String parent ) throws Exception {
        getInstanceType(list, parent);
        getTemplate(list, parent );
        getDiskType(list,parent );
//		getKeypair(list, parent);
    }
    private void getInstanceType(List list, String parent) throws Exception{
        Compute.MachineTypes.List request = compute.machineTypes().list(PROJECT_ID, ZONE_NM);
        MachineTypeList response;

        do {
            response = request.execute();
            if (response.getItems() == null) {
                continue;
            }
            response.getItems().forEach(machineType -> {
                add(list, getRelativeLink(machineType.getSelfLink()), getInstanceName(machineType), OBJ_TYPE_INSTANCE_TYPE, parent , getInstanceProp(machineType));
            });
            request.setPageToken(response.getNextPageToken());
        } while (response.getNextPageToken() != null);
    }

    private JSONObject getInstanceProp(MachineType machineType) {
        JSONObject json = new JSONObject();
        json.put(HypervisorAPI.PARAM_CPU, machineType.getGuestCpus());
        json.put(HypervisorAPI.PARAM_MEM,   machineType.getMemoryMb()/1024)  ;

        return json;
    }

    private String getInstanceName(MachineType machineType){
        return machineType.getName() ;
    }

    private void getTemplate(List list, String parent ) throws IOException {
        ImageList imageList;


        Compute.Images.List images = compute.images().list(PROJECT_ID)
                .setFilter("status=READY");

        do {
            imageList = images.execute();
            if (imageList.getItems() == null) {
                continue;
            }
            imageList.getItems().forEach(img -> {
                add(list, getRelativeLink(img.getSelfLink()), img.getName(), OBJ_TYPE_TEMPLATE, parent );
            });
            images.setPageToken(imageList.getNextPageToken());
        } while (imageList.getNextPageToken() != null);




    }
    private void getDiskType(List list, String parent ) throws  Exception {
        DiskTypeList diskTypeList;


        Compute.DiskTypes.List diskTypes = compute.diskTypes().list(PROJECT_ID, ZONE_NM);

        do {
            diskTypeList = diskTypes.execute();
            if (diskTypeList.getItems() == null) {
                continue;
            }
            for (DiskType diskType : diskTypeList.getItems()) {
                add(list, getRelativeLink(diskType.getSelfLink()), diskType.getName(), OBJ_TYPE_DISK_TYPE, parent, getDiskTypeProp(diskType));
            }
            diskTypes.setPageToken(diskTypeList.getNextPageToken());
        } while (diskTypeList.getNextPageToken() != null);
    }
    private JSONObject getDiskTypeProp(DiskType diskType) throws Exception {
        JSONObject result = new JSONObject();
        String validDiskSize = diskType.getValidDiskSize();
        if(validDiskSize != null) {
            int pos = validDiskSize.indexOf("-");
            if (pos > 0) {
                String minStr = validDiskSize.substring(0, pos);
                String maxStr = validDiskSize.substring(pos + 1);
                if (minStr.equals("GB")) {
                    throw new Exception(minStr + " Not GB");
                }
                if (maxStr.equals("GB")) {
                    throw new Exception(maxStr + " Not GB");
                }
                result.put("min", minStr.substring(0, minStr.length() - 2));
                result.put("max", maxStr.substring(0, maxStr.length() - 2));
            }
        }
        return result;
    }
    private String getRelativeLink(String link){
        int pos = link.indexOf("projects/" + PROJECT_ID);
        return link.substring(pos);
    }




}
