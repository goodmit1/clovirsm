package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.google.api.services.compute.model.Firewall;

import java.util.Map;

public class GcpDisableFW extends GcpCommon {
    public GcpDisableFW(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        doAction(param.getVmInfo().getVM_NM());
        return null;
    }

    protected boolean disabled(){
        return true;
    }

    public void run(HVParam param, boolean isDefaultFw) throws  Exception{
        if(isDefaultFw){
            doActionDefaultFw(param.getVmInfo().getVM_NM());
        }
        else {
            doAction(param.getVmInfo().getVM_NM());
        }

    }
    public void doActionDefaultFw(String vmName) throws Exception {
        GcpGetVM getVM = new GcpGetVM(getConnect());
        VMInfo vmInfo = getVM.run(vmName);
        String defaultFwName =GCPOnFinishCreateVM.getDefaultFWName(  vmInfo.getIP_LIST().get(0).network );
        doAction(defaultFwName);
    }
    public void doAction(String fwName) throws Exception {
        try{

            Firewall firewall = compute.firewalls().get(PROJECT_ID, fwName).execute();
            firewall.setDisabled(this.disabled());
            compute.firewalls().patch(PROJECT_ID, fwName, firewall ).execute();
        }
        catch(Exception e){
            if(isNotFoundException(e)){
                throw new NotFoundException("FW " + fwName);
            }
            else{
                throw e;
            }
        }
    }

}
