package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import com.google.api.services.compute.model.Disk;
import com.google.api.services.compute.model.Operation;

import java.math.BigInteger;
import java.util.Map;

public class GcpCreateDisk extends GcpCommon {
    public GcpCreateDisk(IConnection connection) {
        super(connection);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }

    public BigInteger create(VMInfo newVM, DiskInfo m, boolean isWait) throws Exception {

        Disk disk = new Disk();
        disk.setName(m.getDISK_NM());
        disk.setSizeGb((long) m.getDISK_SIZE());

        disk.setType(m.getDATASTORE_NM_LIST()[0]);
        try {
            Operation res = compute.disks().insert(PROJECT_ID, ZONE_NM, disk).execute();
            GcpChkTask chkTask = new GcpChkTask(super.getConnect());
            if (isWait) {
                while (true) {
                    Thread.sleep(500);
                    if(chkTask.run("" + res.getId())){
                        break;
                    };
                }

            }
            return res.getId();
        }
        catch(Exception e){

            if(e.getMessage().indexOf("409 Conflict")>=0){
                   return null;
            }
            else{
                throw e;
            }

        }
    }
}
