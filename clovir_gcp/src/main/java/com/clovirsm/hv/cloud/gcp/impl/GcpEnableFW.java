package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.IConnection;

public class GcpEnableFW extends GcpDisableFW {
    public GcpEnableFW(IConnection m) {
        super(m);
    }



    protected boolean disabled(){
        return false;
    }


}
