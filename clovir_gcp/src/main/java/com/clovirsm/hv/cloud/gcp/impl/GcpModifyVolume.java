package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import com.google.api.services.compute.model.DisksResizeRequest;

import java.util.Map;

public class GcpModifyVolume extends GcpCommon {
    public GcpModifyVolume(IConnection connect) {
        super(connect);
    }

    public static String nextDeviceName(String lastDeviceName){
       

        return lastDeviceName.substring(0, lastDeviceName.length()-1) + String.valueOf(  (char)(lastDeviceName.charAt(lastDeviceName.length()-1)+1));
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }
    public void run(  DiskInfo diskInfo) throws Exception{

        DisksResizeRequest req = new DisksResizeRequest();
        req.setSizeGb((long) diskInfo.getDISK_SIZE());
        compute.disks().resize(PROJECT_ID, ZONE_NM, diskInfo.getDISK_PATH(), req).execute();
    }
}
