package com.clovirsm.hv.cloud.gcp;

import com.clovirsm.hv.AlreadyExistException;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.PublicHVAction;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.compute.Compute;
import org.json.JSONObject;

import java.io.IOException;

public abstract class GcpCommon extends PublicHVAction {
    protected Compute compute;
    protected String ZONE_NM;
    protected String REGION_NM;
    protected String PROJECT_ID;
    protected String COMPUTE_BASE_URL="https://www.googleapis.com/compute/v1/";
    public GcpCommon(IConnection m) {
        super(m);
        compute = getGCPConnection().compute;

        ZONE_NM=getGCPConnection().ZONE_NM;
        REGION_NM=getGCPConnection().REGION_NM;
        PROJECT_ID=getGCPConnection().PROJECT_ID;
    }



    public GcpConnection getGCPConnection(){
        return (GcpConnection) super.getConnect();
    }
    @Override
    protected String getConnectionClassName() {
        return GcpConnection.class.getName();
    }

    protected boolean isNotFoundException(Exception e){
        Exception ne = processErr(e, "");
        if(ne instanceof  NotFoundException){
            return true;
        }
        return false;
    }

    protected Exception processErr(Exception e, String obj){
        try{
            if(e.getMessage()==null){
                return e;
            }
            String jsonStr = e.getMessage();
            if(e instanceof GoogleJsonResponseException){
                jsonStr = ((GoogleJsonResponseException)e).getContent();
            }
            JSONObject json = new JSONObject(jsonStr);

            if(json.has("error")){
                json = json.getJSONObject("error");
            }
            int resCode = json.getInt("code");
            String resMsg = json.getString("message");


            if(resCode == 409){
                return new AlreadyExistException(obj + " already exists");
            }
            else if(resCode == 404){
                return new NotFoundException(obj + " not exists");
            }
            return new Exception(resMsg);
        }
        catch(Exception e1){
            if(e.getMessage().startsWith("404 ")){
                return new NotFoundException(obj + " not exists");
            }
            return e;
        }
    }
    protected static String getValByUrl(String url, String key){
        int pos = url.indexOf("/" + key + "/");
        if(pos>=0){
            int pos1 = url.indexOf("/",pos+key.length()+3 );
            if(pos1 < 0){
                pos1 = url.length();
            }
            return url.substring(pos+key.length()+2, pos1);
        }
        return url;
    }
}
