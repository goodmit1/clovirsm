package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.obj.FWInfo;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.gcp.GcpCommon;

import java.util.Map;

public class GCPOnFinishCreateVM extends GcpCommon {
    public GCPOnFinishCreateVM(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        doAction(param);
        return null;
    }


    public static String getDefaultFWName(String network){
        return getValByUrl(network, "networks") + "-clovirsm";
    }

    /**
     *   fw 만들어서 clovirsm서버 ip가 22 port로 접근할 수 있게 함.
     * @param param
     * @throws Exception
     */
    void doAction(HVParam param) throws Exception {
        GCPCreateFW action = new GCPCreateFW(getConnect());
        param.setFwInfo(new FWInfo(FWInfo.PROTOCOL_TCP, new String[]{param.getVmInfo().getDEFAULT_IP()}, new String[]{"22"}));
        action.doAction(param);
    }

    }


