package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;

import java.util.Map;

public class GcpRebootVM extends GcpCommon {
    public GcpRebootVM(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        compute.instances().reset(PROJECT_ID, ZONE_NM, param.getVmInfo().getVM_NM()).execute();
        return null;
    }
}
