package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.google.api.services.compute.model.Firewall;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GcpDeleteFWSourceIp extends GcpCommon {
    public GcpDeleteFWSourceIp(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        doAction(param);
        return null;
    }


    void doAction(HVParam param) throws Exception {
        try{
            Firewall firewall = compute.firewalls().get(PROJECT_ID, param.getVmInfo().getVM_NM()).execute();
                     List<String> ips = firewall.getSourceRanges();
            List<String> removeIps = Arrays.asList(  param.getFwInfo().getIps());
            firewall.setSourceRanges(ips.stream().filter( s-> !removeIps.contains(s)).collect(Collectors.toList()));
            if(firewall.getSourceRanges().size()==0){
                compute.firewalls().delete(PROJECT_ID, param.getVmInfo().getVM_NM()).execute();
            }
            else {
                compute.firewalls().update(PROJECT_ID, param.getVmInfo().getVM_NM(), firewall).execute();
            }
        }
        catch(Exception e){
           throw super.processErr(e, "FW " + param.getVmInfo().getVM_NM());

        }
    }

}
