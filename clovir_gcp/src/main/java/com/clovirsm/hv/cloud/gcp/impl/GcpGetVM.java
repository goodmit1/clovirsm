package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.*;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.obj.VMInfo;
import com.google.api.services.compute.model.AttachedDisk;
import com.google.api.services.compute.model.GuestOsFeature;
import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.NetworkInterface;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GcpGetVM extends GcpCommon {
    public GcpGetVM(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map resultMap) throws Exception {

        return null;
    }
    public VMInfo run(HVParam param) throws Exception {
        return run(param.getVmInfo().getVM_NM());
    }
    public VMInfo run(String vmName) throws Exception{
        VMInfo result = new VMInfo();
        try {
            Instance vm = super.compute.instances().get(PROJECT_ID, ZONE_NM, vmName).execute();
            result.setVM_NM(vm.getName());
            result.setVM_HV_ID("" + vm.getId());
            result.setCMT(vm.getDescription());
            result.setSPEC_TYPE(vm.getMachineType().substring(COMPUTE_BASE_URL.length()));
            result.setRUN_CD("RUNNING".equals(vm.getStatus()) ? HypervisorAPI.RUN_CD_RUN : HypervisorAPI.RUN_CD_STOP);
            result.setGUEST_NM(getGuestNm(vm.getDisks().get(0)));
            getSpec(result, vm.getMachineType());
            getIPList(result, vm.getNetworkInterfaces());
            result.setDISK_SIZE(getDisk(result, vm.getDisks()));
            return result;
        }
        catch(Exception e){
            if(isNotFoundException(e)){
                throw new NotFoundException("VM " + vmName);
            }
            throw e;
        }
    }
    private int getDisk(Map result, List<AttachedDisk> diskList) throws Exception {
        List<Map> list = new ArrayList<>();
        int total = 0 ;
        for(AttachedDisk disk: diskList){
            DiskInfo m = new DiskInfo();
            m.setDISK_NM(disk.getDeviceName());
            m.setDISK_PATH( disk.getDeviceName());
            m.setDS_NM( disk.getType());
            m.setDISK_SIZE(disk.getDiskSizeGb().intValue());
            total += disk.getDiskSizeGb();
            list.add(m);
        }
        result.put("DISK_LIST" , list);
        return total;
    }
    private void getIPList(VMInfo result, List<NetworkInterface> nicList) throws Exception {
        List<IPInfo> ipList = new ArrayList<>();
        String publicIp = null;
        int id = 1;
        for(NetworkInterface nic: nicList){
           try{
               publicIp = nic.getAccessConfigs().get(0).getNatIP();
           } catch(Exception ignore){

           }
           IPInfo ipInfo = new IPInfo();
           ipInfo.network = nic.getNetwork() ;
           ipInfo.ip =   nic.getNetworkIP();
           ipInfo.nicId = id++;
           ipList.add(ipInfo);


        }
        result.setPUBLIC_IP( publicIp);
        result.setIP_LIST(ipList);

    }
    private void getSpec(Map result, String specUrl) throws Exception {
        JSONObject specInfo =  super.getGCPConnection().getRestClient().getJSON(specUrl);
        result.put(HypervisorAPI.PARAM_CPU, specInfo.getInt("guestCpus"));
        result.put(HypervisorAPI.PARAM_MEM, specInfo.getInt("memoryMb")/1024);
    }
    private String getGuestNm(AttachedDisk attachedDisk) throws Exception {

        try {

            List<GuestOsFeature> features = attachedDisk.getGuestOsFeatures();
            for(GuestOsFeature f: features){
                if(f.getType().equals("WINDOWS")){

                    return "Windows";
                }
            }
            return "Linux";
            /*
            JSONObject disk = super.getGCPConnection().getRestClient().getJSON(attachedDisk.getSource());
            JSONObject sourceImage = super.getGCPConnection().getRestClient().getJSON(disk.getString("sourceImage"));
            String description = sourceImage.getString("description");
            String[] arr = description.split(",");
            return arr[0] + arr[1] + arr[2];
            */

        }
        catch(Exception e){
            e.printStackTrace();
            return "Linux";
        }
    }
}
