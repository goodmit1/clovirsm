package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

public class GcpChkTemplate extends GcpCommon {
    public GcpChkTemplate(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }


    public VMInfo run(String id) throws  Exception {
        JSONObject json = super.getGCPConnection().getRestClient().getJSON("https://compute.googleapis.com/compute/v1/" + id);

        if(json != null ){
            VMInfo result = new VMInfo();
            result.setDISK_SIZE(json.getInt("diskSizeGb"));
            result.setGUEST_NM(isWindow(json)?"Windows":"Linux");
            //JSONObject license = super.getGCPConnection().getRestClient().getJSON(json.getJSONArray("licenses").getString(0));
            //System.out.println(license);
            return result;
        }
        throw new NotFoundException( "Image " + id + " not found");
    }
    private boolean isWindow(JSONObject json){
        JSONArray guestOsFeatures = json.getJSONArray("guestOsFeatures");
        for(int i=0; i  < guestOsFeatures.length(); i++){
            if("WINDOWS".equals( guestOsFeatures.getJSONObject(i).getString("type").toUpperCase())){
                return true;
            }
        }
        return false;
    }
}
