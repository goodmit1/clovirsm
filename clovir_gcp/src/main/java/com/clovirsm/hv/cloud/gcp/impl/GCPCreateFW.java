package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.obj.HVParam;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.google.api.services.compute.model.Firewall;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GCPCreateFW extends GcpCommon {
    public GCPCreateFW(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        doAction(param);
        return null;
    }


    void doAction(HVParam param) throws Exception {
        Firewall firewall = null;
        try{
            firewall = compute.firewalls().get(PROJECT_ID, param.getVmInfo().getVM_NM()).execute();
            modifyFw(param, firewall);
        }
        catch (Exception e){
            if(isNotFoundException(e)) {
                createFw(param);
            }
            else {
                throw e;
            }
        }

    }

    private void modifyFw(HVParam param, Firewall firewall) throws IOException {
        List<String> newIps = addIps(firewall.getSourceRanges(), param.getFwInfo().getIps());

        firewall.setSourceRanges( newIps );
        List<Firewall.Allowed> allowed = firewall.getAllowed();
        for(Firewall.Allowed allowed1:allowed){
            if(allowed1.getIPProtocol().equals(param.getFwInfo().getProtocol())){
                allowed1.getPorts().addAll(Arrays.asList(param.getFwInfo().getPorts()));
                allowed1.setPorts( allowed1.getPorts().stream().distinct().collect(Collectors.toList()));
            }
        }
        compute.firewalls().update(PROJECT_ID, param.getVmInfo().getVM_NM(), firewall ).execute();

    }

    protected List addIps(List<String> existIps, String[] newIps){

        List<String> result = new ArrayList<>();
        result.addAll(existIps);
        for(String existIp : existIps){
            for(String newIp: newIps) {
                if (!super.isInRange(existIp, newIp)) {
                    result.add(newIp);
                }
            }
        }
        return result;
    }

    private void createFw(HVParam param) throws Exception {
        GcpGetVM getVm = new GcpGetVM(this.getConnect());
        VMInfo vmInfo = getVm.run(param);
        Firewall fw = new Firewall() ;
        fw.setSourceRanges(Arrays.asList(  param.getFwInfo().getIps()));
        fw.setTargetTags(Arrays.asList( param.getVmInfo().getVM_NM()));

        fw.setName(param.getVmInfo().getVM_NM());
        fw.setDirection("INGRESS");
        fw.setNetwork(vmInfo.getIP_LIST().get(0).network);
        List<Firewall.Allowed> allowed = new ArrayList<>();
        Firewall.Allowed allowed1 = new Firewall.Allowed();
        allowed1.setIPProtocol(param.getFwInfo().getProtocol());
        allowed1.setPorts(Arrays.asList(param.getFwInfo().getPorts()));
        allowed.add(allowed1);
        fw.setAllowed(allowed);
        compute.firewalls().insert(PROJECT_ID, fw).execute();
    }
    }
