package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import com.google.api.services.compute.model.Operation;

import java.util.Map;

public class GcpChkTask extends GcpCommon {
    public GcpChkTask(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }



    public boolean run(String taskId) throws Exception {
        Operation op = compute.zoneOperations().get(PROJECT_ID, ZONE_NM, taskId).execute();
        if(op.getError() != null && op.getError().getErrors().size()>0){
            throw new Exception(op.getError().getErrors().get(0).getMessage());
        }
        if(op.getProgress() == 100){
            return true;
        }
        else {
            return false;
        }
    }
}
