package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IAsyncAfter;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import com.google.api.services.compute.model.InstancesSetMachineTypeRequest;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class GcpReconfigVM extends GcpCommon   {
    public GcpReconfigVM(IConnection m) {
        super(m);
    }


    public void runInThread(HVParam param ){
        Thread thread = new Thread(() -> {
            try {
                GcpGetVM getVM = new GcpGetVM( getConnect());
                VMInfo oldVMInfo = getVM.run(param);
                GcpPowerOffVM powerOffVM = new GcpPowerOffVM( getConnect());
                powerOffVM.run(param.getVmInfo());
                String newSpcType = COMPUTE_BASE_URL + param.getVmInfo().getSPEC_TYPE();
                if (!oldVMInfo.getSPEC_TYPE().equals(newSpcType)) {
                    setMachineType(param.getVmInfo());
                }
                createVirtualDisk(oldVMInfo, param.getVmInfo());
                GcpPowerOnVM powerOnVM = new GcpPowerOnVM( getConnect());
                powerOnVM.run(param.getVmInfo());
                if(param.getAfter() != null && param.getAfter() instanceof IAsyncAfter){
                    ((IAsyncAfter)param.getAfter()).setDone(true);
                }
                getConnect().disconnect();

            }
            catch (Exception e){
                if(param.getAfter() != null && param.getAfter() instanceof IAsyncAfter){
                    ((IAsyncAfter)param.getAfter()).setError(e);
                }
                e.printStackTrace();
            }
        });
        thread.start();
    }
    @Override
    public String run1(HVParam param, Map result) throws Exception {
        runInThread(param );
        return "" + System.nanoTime();
    }

    protected void setMachineType(VMInfo vmInfo) throws Exception{
       /* JSONObject json = new JSONObject();
        json.put("machineType", vmInfo.getSPEC_TYPE());
        super.getGCPConnection().getRestClient().post(COMPUTE_BASE_URL + "projects/" + PROJECT_ID + "/zones/" + ZONE_NM + "/instances/"  + vmInfo.getVM_NM() + "/setMachineType", json.toString());
        */
         InstancesSetMachineTypeRequest req = new InstancesSetMachineTypeRequest();
            req.setMachineType(vmInfo.getSPEC_TYPE());
            compute.instances().setMachineType(PROJECT_ID, ZONE_NM, vmInfo.getVM_NM(), req).execute();

    }
    private void createVirtualDisk(VMInfo oldVm, VMInfo newVM ) throws Exception {
        List<DiskInfo> dataDisk = newVM.getDISK_LIST();
        List<DiskInfo> oldDataDisk = oldVm.getDISK_LIST();
        String lastMount = oldDataDisk.stream().map(DiskInfo::getDISK_NM).max(Comparator.naturalOrder()).orElse(newVM.getVM_NM() + "-disk1");
        for (DiskInfo m : dataDisk) {


            if (m.isNew()) {
                GcpCreateDisk blockStoreAction = new GcpCreateDisk(super.getConnect());
                lastMount = GcpAttachVolume.nextDeviceName(lastMount);
                m.setDISK_NM(lastMount);
                m.setDISK_PATH(m.getDISK_NM());
                blockStoreAction.create(newVM, m, true);

                GcpAttachVolume attachVolume = new GcpAttachVolume(getConnect());


                attachVolume.run(newVM, m);


            } else {

                DiskInfo oldDisk = oldDataDisk.stream()
                        .filter(disk -> disk.getDISK_PATH().equals(m.getDISK_PATH()))
                        .findAny().orElseGet(DiskInfo::new);
                int changeDiskSize = m.getDISK_SIZE();
                int oldDiskSize = oldDisk.getDISK_SIZE();

                if (changeDiskSize > oldDiskSize) {
                    GcpModifyVolume blockStoreModifyAction = new GcpModifyVolume(getConnect());

                    blockStoreModifyAction.run(m);
                } else if (changeDiskSize < oldDiskSize) {
                    throw new Exception("size problem");
                }
            }
        }
    }


}
