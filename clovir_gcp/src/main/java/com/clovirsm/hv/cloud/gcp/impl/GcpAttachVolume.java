package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.DiskInfo;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import com.google.api.services.compute.model.AttachedDisk;

import java.util.Map;

public class GcpAttachVolume extends GcpCommon {
    public GcpAttachVolume(IConnection connect) {
        super(connect);
    }

    public static String nextDeviceName(String lastDeviceName){


        return lastDeviceName.substring(0, lastDeviceName.length()-1) + String.valueOf(  (char)(lastDeviceName.charAt(lastDeviceName.length()-1)+1));
    }

    @Override
    public String run1(HVParam param, Map result) throws Exception {
        return null;
    }
    public void run(VMInfo vmInfo, DiskInfo diskInfo) throws Exception{

        AttachedDisk disk = new AttachedDisk();
        disk.setAutoDelete(true);
        disk.setType("PERSISTENT");
        disk.setMode("READ_WRITE");
        String id =  compute.disks().get(PROJECT_ID, ZONE_NM, diskInfo.getDISK_PATH()).execute().getSelfLink();
        disk.setSource(id);
        disk.setDeviceName(diskInfo.getDISK_NM());
        compute.instances().attachDisk(PROJECT_ID,ZONE_NM, vmInfo.getVM_NM(), disk).execute();
    }
}
