package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.obj.VMInfo;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Map;

public class GcpCreateVM extends GcpCommon {

    public GcpCreateVM(IConnection m) {
        super(m);
    }

    @Override
    public String run1(HVParam paramMap, Map result) throws Exception {

        VMInfo param = paramMap.getVmInfo();
        chkVMSetting(param);

        param.put("PROJECT_ID", PROJECT_ID);
        param.put("ZONE_NM", ZONE_NM);
        param.put("REGION_NM", REGION_NM);
        param.put("DISK_TYPE",param.getDISK_LIST().get(0).getDATASTORE_NM_LIST()[0]);
        param.put("DISK_SIZE",param.getDISK_LIST().get(0).getDISK_SIZE());
        InputStream stream = getClass().getClassLoader().getResourceAsStream("gcpCreateTemplate.json");
        String jsonStr = CommonUtil.readStream2String(stream, "utf-8");
        stream.close();
        jsonStr = CommonUtil.fillContentByVar(jsonStr, param);
        System.out.println(jsonStr);
        //JSONObject json = new JSONObject(jsonStr);
        try {
            JSONObject response = (JSONObject) super.getGCPConnection().getRestClient().post(COMPUTE_BASE_URL + "projects/" + PROJECT_ID + "/zones/" + ZONE_NM + "/instances", jsonStr);
            if (response != null) {
                return response.getString("id");
            } else {
                throw new Exception("Response is Empty");
            }
        }
        catch(Exception e){
            throw super.processErr(e,"VM "  + param.getVM_NM());

        }
    }
}
