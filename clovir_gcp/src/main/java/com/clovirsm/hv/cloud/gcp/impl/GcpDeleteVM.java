package com.clovirsm.hv.cloud.gcp.impl;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.cloud.gcp.GcpCommon;
import com.clovirsm.hv.obj.HVParam;
import com.google.api.services.compute.model.AttachedDisk;
import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.Operation;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class GcpDeleteVM extends GcpCommon {
    public GcpDeleteVM(IConnection m) {
        super(m);
    }

    @Override
    public String  run1(HVParam param, Map result) throws Exception {
        deleteFW(param);
        Instance vm = compute.instances().get(PROJECT_ID, ZONE_NM,  param.getVmInfo().getVM_NM()).execute();
        if(vm != null){
            List<AttachedDisk> disks = vm.getDisks();
            for(AttachedDisk disk:disks){
                String diskName = disk.getSource();
                int pos = diskName.lastIndexOf("/");
                compute.disks().delete(PROJECT_ID, ZONE_NM, diskName.substring(pos+1));
            }
        }
        Operation req = compute.instances().delete(PROJECT_ID, ZONE_NM, param.getVmInfo().getVM_NM()).execute();

        return req.getId().toString();
    }

    protected void deleteFW(HVParam param) throws IOException {
        compute.firewalls().delete(PROJECT_ID, param.getVmInfo().getVM_NM()).execute();
    }
}
